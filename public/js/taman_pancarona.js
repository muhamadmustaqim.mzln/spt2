// import './bootstrap';


    pannellum.viewer('panorama', {
    "type": "equirectangular",
    "panorama": "assets/360_Image_PPj/Taman_Pancarona/Gelanggang_Bola_Jaring_Pancarona.jpg",
    "autoLoad":true,
    "autoRotate": 2,
    "autoRotateInactivityDelay": 1
    });

    pannellum.viewer('panorama1', {
    "type": "equirectangular",
    "panorama": "assets/360_Image_PPj/Taman_Pancarona/Gelanggang_Bola_Tampar_Pancarona.jpg",
    "autoLoad":false,
    "autoRotate": -2,
    "autoRotateInactivityDelay": 1
    });

    pannellum.viewer('panorama2', {
        "type": "equirectangular",
        "panorama": "assets/360_Image_PPj/Taman_Pancarona/Gelanggang_Futsal_Keranjang_Pancarona.jpg",
        "autoLoad":false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 1
    });

    pannellum.viewer('panorama3', {
        "type": "equirectangular",
        "panorama": "assets/360_Image_PPj/Taman_Pancarona/Gelanggang_Futsal_Keranjang_Pancarona_2.jpg",
        "autoLoad":false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 1
    });

    pannellum.viewer('panorama4', {
        "type": "equirectangular",
        "panorama": "assets/360_Image_PPj/Taman_Pancarona/Gelanggang_Takraw_Pancarona.jpg",
        "autoLoad":false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 1
    });

    pannellum.viewer('panorama5', {
        "type": "equirectangular",
        "panorama": "assets/360_Image_PPj/Taman_Pancarona/Padang_Bola_Sepak_Pancarona.jpg",
        "autoLoad":false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 1
    });
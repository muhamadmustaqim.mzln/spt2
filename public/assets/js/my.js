const flashData = $('.flash-data').data('flashdata');
const arrays = ['Success', 'Failed', 'Updated', 'BelumSedia', 'Non-existant'];

if (flashData == 'Success') {
    Swal.fire({
        title: 'Berjaya',
        icon: 'success',
        html:
            '<hr>Data telah berjaya disimpan dalam rekod<hr>',
        showCloseButton: true
    });
} 
else if (flashData == 'Failed') {
    Swal.fire(
        'Harap Maaf!',
        'Data anda tidak berjaya disimpan dalam rekod',
        'error'
    )
} 
else if (flashData == 'Updated') {
    Swal.fire({
        title: 'Berjaya',
        icon: 'success',
        html:
            '<hr>Data telah berjaya dikemaskini<hr>',
        showCloseButton: true
    });
} 
else if (flashData == 'BelumSedia') {
    Swal.fire(
        'Harap Maaf!',
        'Fasiliti belum sedia untuk ditempah',
        'warning'
    )
} 
else if (flashData == 'Non-existant') {
    Swal.fire(
        'Harap Maaf!',
        'Tiada data dalam rekod',
        'error'
    )
} 
// else if (flashData){
//     Swal.fire(
//         'Harap Maaf!',
//         flashData,
//         'error'
//     )
// }
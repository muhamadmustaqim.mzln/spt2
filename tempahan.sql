-- MySQL dump 10.19  Distrib 10.3.39-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tempahan
-- ------------------------------------------------------
-- Server version	10.3.39-MariaDB-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit_trail`
--

DROP TABLE IF EXISTS `audit_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_lkp_task` int(10) unsigned DEFAULT NULL,
  `table_ref_id` int(11) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_trail_fk_users_foreign` (`fk_users`),
  KEY `audit_trail_fk_lkp_task_foreign` (`fk_lkp_task`),
  CONSTRAINT `audit_trail_fk_lkp_task_foreign` FOREIGN KEY (`fk_lkp_task`) REFERENCES `lkp_task` (`id`),
  CONSTRAINT `audit_trail_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1637317 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_trail_back`
--

DROP TABLE IF EXISTS `audit_trail_back`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trail_back` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_lkp_task` int(10) unsigned DEFAULT NULL,
  `table_ref_id` int(11) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_trail_fk_users_foreign` (`fk_users`),
  KEY `audit_trail_fk_lkp_task_foreign` (`fk_lkp_task`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_trail_back2`
--

DROP TABLE IF EXISTS `audit_trail_back2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_trail_back2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_lkp_task` int(10) unsigned DEFAULT NULL,
  `table_ref_id` int(11) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_trail_fk_users_foreign` (`fk_users`),
  KEY `audit_trail_fk_lkp_task_foreign` (`fk_lkp_task`)
) ENGINE=InnoDB AUTO_INCREMENT=1636996 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bb_banner`
--

DROP TABLE IF EXISTS `bb_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bb_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bb_name` varchar(100) DEFAULT NULL,
  `eft_uuid` varchar(100) DEFAULT NULL,
  `bb_filename` varchar(100) DEFAULT NULL,
  `bb_location` varchar(100) DEFAULT NULL,
  `bb_status` int(11) DEFAULT NULL,
  `bb_start_date` date DEFAULT NULL,
  `bb_end_date` date DEFAULT NULL,
  `bb_expired_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bb_status` (`bb_status`),
  KEY `bb_start_date` (`bb_start_date`,`bb_end_date`),
  KEY `bb_expired_date` (`bb_expired_date`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_announcement`
--

DROP TABLE IF EXISTS `bh_announcement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_announcement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ba_name` varchar(100) DEFAULT NULL,
  `ba_detail` varchar(255) DEFAULT NULL,
  `ba_status` int(11) DEFAULT NULL,
  `ba_start_date` date DEFAULT NULL,
  `ba_end_date` date DEFAULT NULL,
  `ba_expired_date` date DEFAULT NULL,
  `ba_link` varchar(255) DEFAULT NULL,
  `ba_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ba_status` (`ba_status`),
  KEY `ba_start_date` (`ba_start_date`,`ba_end_date`),
  KEY `ba_end_date` (`ba_end_date`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_answer`
--

DROP TABLE IF EXISTS `bh_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ba_no` int(11) DEFAULT NULL,
  `ba_description` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_attachment`
--

DROP TABLE IF EXISTS `bh_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `ba_date` date DEFAULT NULL,
  `ba_dir` varchar(155) DEFAULT NULL,
  `ba_full_path` varchar(255) DEFAULT NULL,
  `ba_file_name` varchar(155) DEFAULT NULL,
  `ba_file_ext` varchar(155) DEFAULT NULL,
  `ba_file_size` int(11) DEFAULT NULL,
  `ba_generated_name` varchar(100) DEFAULT NULL,
  `ba_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_attachment_fk_main_booking_foreign` (`fk_main_booking`),
  CONSTRAINT `bh_attachment_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1812 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_book_picc`
--

DROP TABLE IF EXISTS `bh_book_picc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_book_picc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall_picc` int(10) unsigned DEFAULT NULL,
  `bbp_status` int(11) DEFAULT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `layout_name` varchar(155) DEFAULT NULL,
  `pax` int(11) DEFAULT NULL,
  `date_booking_start` date DEFAULT NULL,
  `date_booking_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_book_picc_fk_users_foreign` (`fk_users`),
  KEY `bh_book_picc_fk_bh_hall_picc_foreign` (`fk_bh_hall_picc`),
  CONSTRAINT `bh_book_picc_fk_bh_hall_picc_foreign` FOREIGN KEY (`fk_bh_hall_picc`) REFERENCES `bh_hall_picc` (`id`),
  CONSTRAINT `bh_book_picc_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_booking`
--

DROP TABLE IF EXISTS `bh_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_package` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_lkp_discount` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall_usage` int(10) unsigned DEFAULT NULL,
  `bb_hall_status` int(11) DEFAULT NULL,
  `bb_start_date` datetime DEFAULT NULL,
  `bb_end_date` datetime DEFAULT NULL,
  `hall_price` decimal(11,2) DEFAULT NULL,
  `bb_discount_type_rm` decimal(11,2) DEFAULT NULL,
  `bb_no_of_day` int(11) DEFAULT NULL,
  `special_discount_rm` decimal(11,2) DEFAULT NULL,
  `special_disc` int(11) DEFAULT NULL,
  `baucer_rm` decimal(11,2) DEFAULT NULL,
  `baucer` int(11) DEFAULT NULL,
  `bb_gst` decimal(11,2) DEFAULT NULL,
  `bb_total` decimal(11,2) DEFAULT NULL,
  `bb_subtotal` decimal(11,2) DEFAULT NULL,
  `bb_equipment_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_booking_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_booking_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `bh_booking_fk_bh_package_foreign` (`fk_bh_package`),
  KEY `bh_booking_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_booking_fk_lkp_discount_foreign` (`fk_lkp_discount`),
  KEY `bh_booking_fk_bh_hall_usage_foreign` (`fk_bh_hall_usage`),
  KEY `bb_start_date` (`bb_start_date`,`bb_end_date`),
  KEY `bb_hall_status` (`bb_hall_status`),
  CONSTRAINT `bh_booking_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`),
  CONSTRAINT `bh_booking_fk_bh_hall_usage_foreign` FOREIGN KEY (`fk_bh_hall_usage`) REFERENCES `bh_hall_usage` (`id`),
  CONSTRAINT `bh_booking_fk_bh_package_foreign` FOREIGN KEY (`fk_bh_package`) REFERENCES `bh_package` (`id`),
  CONSTRAINT `bh_booking_fk_lkp_discount_foreign` FOREIGN KEY (`fk_lkp_discount`) REFERENCES `lkp_discount` (`id`),
  CONSTRAINT `bh_booking_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`),
  CONSTRAINT `bh_booking_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3882 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_booking_detail`
--

DROP TABLE IF EXISTS `bh_booking_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_booking_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_booking` int(10) unsigned DEFAULT NULL,
  `fk_lkp_event` int(10) unsigned DEFAULT NULL,
  `fk_lkp_time_session` int(10) DEFAULT NULL,
  `bbd_event_name` varchar(155) DEFAULT NULL,
  `bbd_event_description` varchar(255) DEFAULT NULL,
  `bbd_total_no_of_pax` int(11) DEFAULT NULL,
  `bbd_others` varchar(11) DEFAULT NULL,
  `bbd_unit` int(11) DEFAULT NULL,
  `bbd_user_apply` varchar(100) DEFAULT NULL,
  `bbd_contact_no` varchar(20) DEFAULT NULL,
  `bbd_vvip` int(11) DEFAULT NULL,
  `bbd_vip` int(11) DEFAULT NULL,
  `bbd_participant` int(11) DEFAULT NULL,
  `bbd_start_date` datetime DEFAULT NULL,
  `bbd_end_date` datetime DEFAULT NULL,
  `bbd_start_time` time DEFAULT NULL,
  `bbd_end_time` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_booking_detail_fk_bh_booking_foreign` (`fk_bh_booking`),
  KEY `bh_booking_detail_fk_lkp_event_foreign` (`fk_lkp_event`),
  CONSTRAINT `bh_booking_detail_fk_bh_booking_foreign` FOREIGN KEY (`fk_bh_booking`) REFERENCES `bh_booking` (`id`),
  CONSTRAINT `bh_booking_detail_fk_lkp_event_foreign` FOREIGN KEY (`fk_lkp_event`) REFERENCES `lkp_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3497 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_booking_equipment`
--

DROP TABLE IF EXISTS `bh_booking_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_booking_equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_equipment` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_bh_equipment_price` int(10) unsigned DEFAULT NULL,
  `bbe_booking_date` date DEFAULT NULL,
  `bbe_active` int(1) DEFAULT NULL,
  `bbe_price` decimal(11,2) DEFAULT NULL,
  `bbe_quantity` int(11) DEFAULT NULL,
  `bbe_diskaun_rm` decimal(11,2) DEFAULT NULL,
  `special_discount_rm` decimal(11,2) DEFAULT NULL,
  `special_disc` int(11) DEFAULT NULL,
  `baucer_rm` decimal(11,2) DEFAULT NULL,
  `baucer` int(11) DEFAULT NULL,
  `bbe_gst` decimal(11,2) DEFAULT NULL,
  `bbe_total` decimal(11,2) DEFAULT NULL,
  `bbe_subtotal` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_booking_equipment_fk_bh_booking_foreign` (`fk_bh_booking`),
  KEY `bh_booking_equipment_fk_bh_equipment_foreign` (`fk_bh_equipment`),
  KEY `bh_booking_equipment_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_booking_equipment_fk_bh_equipment_price_foreign` (`fk_bh_equipment_price`),
  CONSTRAINT `bh_booking_equipment_fk_bh_booking_foreign` FOREIGN KEY (`fk_bh_booking`) REFERENCES `bh_booking` (`id`),
  CONSTRAINT `bh_booking_equipment_fk_bh_equipment_foreign` FOREIGN KEY (`fk_bh_equipment`) REFERENCES `bh_equipment` (`id`),
  CONSTRAINT `bh_booking_equipment_fk_bh_equipment_price_foreign` FOREIGN KEY (`fk_bh_equipment_price`) REFERENCES `bh_equipment_price` (`id`),
  CONSTRAINT `bh_booking_equipment_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2040 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_booking_price`
--

DROP TABLE IF EXISTS `bh_booking_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_booking_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall_price` int(10) unsigned DEFAULT NULL,
  `bbp_booking_date` date DEFAULT NULL,
  `bbp_price` decimal(11,2) DEFAULT NULL,
  `bbp_discount` decimal(11,2) DEFAULT NULL,
  `bbp_total` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_booking_price_fk_bh_booking_foreign` (`fk_bh_booking`),
  KEY `bh_booking_price_fk_bh_hall_price_foreign` (`fk_bh_hall_price`),
  CONSTRAINT `bh_booking_price_fk_bh_booking_foreign` FOREIGN KEY (`fk_bh_booking`) REFERENCES `bh_booking` (`id`),
  CONSTRAINT `bh_booking_price_fk_bh_hall_price_foreign` FOREIGN KEY (`fk_bh_hall_price`) REFERENCES `bh_hall_price` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8437 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_confirm_booking`
--

DROP TABLE IF EXISTS `bh_confirm_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_confirm_booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_main_booking` int(10) DEFAULT NULL,
  `date_booking` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_confirm_booking_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `fk_main_booking` (`fk_main_booking`),
  KEY `date_booking` (`date_booking`),
  CONSTRAINT `bh_confirm_booking_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50342 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_counter`
--

DROP TABLE IF EXISTS `bh_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_counter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_location` int(11) DEFAULT NULL,
  `fk_bh_hall` int(11) DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lkp_location` (`fk_lkp_location`),
  KEY `fk_bh_hall` (`fk_bh_hall`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_equipment`
--

DROP TABLE IF EXISTS `bh_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `be_name` varchar(100) DEFAULT NULL,
  `be_code` varchar(20) DEFAULT NULL,
  `be_status` int(11) DEFAULT NULL,
  `be_quantity` int(11) DEFAULT NULL,
  `be_code_equip` varchar(10) DEFAULT NULL,
  `be_feecode` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_equipment_fk_lkp_location_foreign` (`fk_lkp_location`),
  CONSTRAINT `bh_equipment_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_equipment_price`
--

DROP TABLE IF EXISTS `bh_equipment_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_equipment_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_equipment` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_lkp_discount_type` int(10) unsigned DEFAULT NULL,
  `be_day_cat` int(11) DEFAULT NULL,
  `be_price` decimal(11,2) DEFAULT NULL,
  `be_discount` int(11) DEFAULT NULL,
  `be_discount_rm` decimal(11,2) DEFAULT NULL,
  `be_gst_rm` decimal(11,2) DEFAULT NULL,
  `be_total_price` decimal(11,2) DEFAULT NULL,
  `be_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_equipment_price_fk_bh_equipment_foreign` (`fk_bh_equipment`),
  KEY `bh_equipment_price_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_equipment_price_fk_lkp_discount_type_foreign` (`fk_lkp_discount_type`),
  CONSTRAINT `bh_equipment_price_fk_bh_equipment_foreign` FOREIGN KEY (`fk_bh_equipment`) REFERENCES `bh_equipment` (`id`),
  CONSTRAINT `bh_equipment_price_fk_lkp_discount_type_foreign` FOREIGN KEY (`fk_lkp_discount_type`) REFERENCES `lkp_discount_type` (`id`),
  CONSTRAINT `bh_equipment_price_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_event`
--

DROP TABLE IF EXISTS `bh_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bev_name` varchar(100) DEFAULT NULL,
  `bev_location` varchar(100) DEFAULT NULL,
  `bev_start_date` datetime DEFAULT NULL,
  `bev_end_date` datetime DEFAULT NULL,
  `bev_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_fpx_detail`
--

DROP TABLE IF EXISTS `bh_fpx_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_fpx_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_payment_fpx` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall` int(10) DEFAULT NULL,
  `fk_bh_equipment` int(10) DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` datetime DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(255) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_fpx_detail_fk_bh_payment_fpx_foreign` (`fk_bh_payment_fpx`),
  KEY `bh_fpx_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  CONSTRAINT `bh_fpx_detail_fk_bh_payment_fpx_foreign` FOREIGN KEY (`fk_bh_payment_fpx`) REFERENCES `bh_payment_fpx` (`id`),
  CONSTRAINT `bh_fpx_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=940 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_hall`
--

DROP TABLE IF EXISTS `bh_hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_hall` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bh_name` varchar(100) DEFAULT NULL,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `bh_hall_owner` int(11) DEFAULT NULL,
  `bh_status` int(11) DEFAULT NULL,
  `bh_parent_id` int(11) DEFAULT NULL,
  `bh_code` varchar(100) DEFAULT NULL,
  `bh_percint` varchar(100) DEFAULT NULL,
  `bh_description` text DEFAULT NULL,
  `bh_size` int(11) DEFAULT NULL,
  `bh_filename` varchar(255) DEFAULT NULL,
  `bh_file_location` varchar(255) DEFAULT NULL,
  `bh_code_fee` varchar(10) DEFAULT NULL,
  `bh_reason` varchar(155) DEFAULT NULL,
  `bh_code_deposit` varchar(20) DEFAULT NULL,
  `feecode_depo_online` varchar(20) DEFAULT NULL,
  `feecode_online` varchar(20) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_hall_fk_lkp_location_foreign` (`fk_lkp_location`),
  CONSTRAINT `bh_hall_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_hall_detail`
--

DROP TABLE IF EXISTS `bh_hall_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_hall_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `bhd_size` int(11) DEFAULT NULL,
  `bhd_cap_banquet` int(11) DEFAULT NULL,
  `bhd_cap_seminar` int(11) DEFAULT NULL,
  `bhd_pameran` int(11) DEFAULT NULL,
  `bhd_other` text DEFAULT NULL,
  `bhd_wifi` int(11) DEFAULT NULL,
  `bhd_capasity` int(11) DEFAULT NULL,
  `bhd_option` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_hall_detail_fk_bh_hall_foreign` (`fk_bh_hall`),
  CONSTRAINT `bh_hall_detail_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_hall_picc`
--

DROP TABLE IF EXISTS `bh_hall_picc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_hall_picc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bh_hall_name` varchar(155) DEFAULT NULL,
  `bhp_capasity` int(11) DEFAULT NULL,
  `bhp_image` varchar(155) DEFAULT NULL,
  `bhp_description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_hall_price`
--

DROP TABLE IF EXISTS `bh_hall_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_hall_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `bhp_day_cat` varchar(15) DEFAULT NULL,
  `bhp_discount` int(11) DEFAULT NULL,
  `bhp_discount_rm` decimal(11,2) DEFAULT NULL,
  `bhp_gst_rm` decimal(11,2) DEFAULT NULL,
  `bhp_total_price` decimal(11,2) DEFAULT NULL,
  `bhp_status` int(11) DEFAULT NULL,
  `bhp_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_hall_price_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_hall_price_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  CONSTRAINT `bh_hall_price_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`),
  CONSTRAINT `bh_hall_price_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_hall_usage`
--

DROP TABLE IF EXISTS `bh_hall_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_hall_usage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall_detail` int(10) unsigned DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `capasity` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_hall_usage_fk_bh_hall_detail_foreign` (`fk_bh_hall_detail`),
  CONSTRAINT `bh_hall_usage_fk_bh_hall_detail_foreign` FOREIGN KEY (`fk_bh_hall_detail`) REFERENCES `bh_hall_detail` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_holiday_detail`
--

DROP TABLE IF EXISTS `bh_holiday_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_holiday_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_holiday` int(10) unsigned DEFAULT NULL,
  `fk_lkp_state` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_holiday_detail_fk_lkp_holiday_foreign` (`fk_lkp_holiday`),
  KEY `bh_holiday_detail_fk_lkp_state_foreign` (`fk_lkp_state`),
  CONSTRAINT `bh_holiday_detail_fk_lkp_holiday_foreign` FOREIGN KEY (`fk_lkp_holiday`) REFERENCES `lkp_holiday` (`id`),
  CONSTRAINT `bh_holiday_detail_fk_lkp_state_foreign` FOREIGN KEY (`fk_lkp_state`) REFERENCES `lkp_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_package`
--

DROP TABLE IF EXISTS `bh_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `bp_package_name` varchar(150) DEFAULT NULL,
  `bp_price` decimal(11,2) DEFAULT NULL,
  `bp_discount` int(11) DEFAULT NULL,
  `bp_discount_rm` decimal(11,2) DEFAULT NULL,
  `bp_gst_rm` decimal(11,2) DEFAULT NULL,
  `bp_package_price` decimal(11,2) DEFAULT NULL,
  `bp_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_package_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_package_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_package_fk_lkp_location_foreign` (`fk_lkp_location`),
  CONSTRAINT `bh_package_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`),
  CONSTRAINT `bh_package_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`),
  CONSTRAINT `bh_package_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_package_detail`
--

DROP TABLE IF EXISTS `bh_package_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_package_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_package` int(10) unsigned DEFAULT NULL,
  `bpd_description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_package_detail_fk_bh_package_foreign` (`fk_bh_package`),
  CONSTRAINT `bh_package_detail_fk_bh_package_foreign` FOREIGN KEY (`fk_bh_package`) REFERENCES `bh_package` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_payment`
--

DROP TABLE IF EXISTS `bh_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_mode` int(10) unsigned DEFAULT NULL,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_bh_quotation` int(11) NOT NULL,
  `bp_total_amount` decimal(11,2) DEFAULT NULL,
  `bp_deposit` decimal(11,2) DEFAULT NULL,
  `bp_paid_amount` decimal(11,2) DEFAULT NULL,
  `bp_receipt_number` varchar(50) DEFAULT NULL,
  `bp_payment_ref_no` varchar(55) DEFAULT NULL,
  `bp_receipt_date` datetime DEFAULT NULL,
  `no_lopo` varchar(30) DEFAULT NULL,
  `bp_special_discount` decimal(11,2) DEFAULT NULL,
  `amount_received` decimal(11,2) DEFAULT NULL,
  `deposit_type` int(11) DEFAULT NULL,
  `bp_subtotal` decimal(11,2) DEFAULT NULL,
  `no_cek` varchar(30) DEFAULT NULL,
  `nama_bank` varchar(55) DEFAULT NULL,
  `bp_payment_status` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_payment_fk_lkp_payment_type_foreign` (`fk_lkp_payment_type`),
  KEY `bh_payment_fk_lkp_payment_mode_foreign` (`fk_lkp_payment_mode`),
  KEY `bh_payment_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `bh_payment_fk_users_foreign` (`fk_users`),
  KEY `fk_bh_quotation` (`fk_bh_quotation`),
  KEY `bp_payment_status` (`bp_payment_status`),
  KEY `bp_receipt_number` (`bp_receipt_number`),
  CONSTRAINT `bh_payment_fk_lkp_payment_mode_foreign` FOREIGN KEY (`fk_lkp_payment_mode`) REFERENCES `lkp_payment_mode` (`id`),
  CONSTRAINT `bh_payment_fk_lkp_payment_type_foreign` FOREIGN KEY (`fk_lkp_payment_type`) REFERENCES `lkp_payment_type` (`id`),
  CONSTRAINT `bh_payment_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `bh_payment_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_payment_detail`
--

DROP TABLE IF EXISTS `bh_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_payment` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_bh_equipment` int(10) unsigned DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(5) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_payment_detail_fk_bh_payment_foreign` (`fk_bh_payment`),
  KEY `bh_payment_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_payment_detail_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_payment_detail_fk_bh_equipment_foreign` (`fk_bh_equipment`),
  CONSTRAINT `bh_payment_detail_fk_bh_equipment_foreign` FOREIGN KEY (`fk_bh_equipment`) REFERENCES `bh_equipment` (`id`),
  CONSTRAINT `bh_payment_detail_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`),
  CONSTRAINT `bh_payment_detail_fk_bh_payment_foreign` FOREIGN KEY (`fk_bh_payment`) REFERENCES `bh_payment` (`id`),
  CONSTRAINT `bh_payment_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_payment_fpx`
--

DROP TABLE IF EXISTS `bh_payment_fpx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_payment_fpx` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_quotation` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `sap_doc_no` varchar(20) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `fpx_serial_no` varchar(30) DEFAULT NULL,
  `fpx_trans_id` varchar(30) DEFAULT NULL,
  `fpx_date` datetime DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `deposit_amount` decimal(11,2) DEFAULT NULL,
  `amount_paid` decimal(11,2) DEFAULT NULL,
  `fpx_status` int(11) DEFAULT NULL,
  `fpx_trans_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_payment_fpx_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `bh_payment_fpx_fk_bh_quotation_foreign` (`fk_bh_quotation`),
  KEY `bh_payment_fpx_fk_lkp_payment_type_foreign` (`fk_lkp_payment_type`),
  CONSTRAINT `bh_payment_fpx_fk_bh_quotation_foreign` FOREIGN KEY (`fk_bh_quotation`) REFERENCES `bh_quotation` (`id`),
  CONSTRAINT `bh_payment_fpx_fk_lkp_payment_type_foreign` FOREIGN KEY (`fk_lkp_payment_type`) REFERENCES `lkp_payment_type` (`id`),
  CONSTRAINT `bh_payment_fpx_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=613 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_payment_fpx_log`
--

DROP TABLE IF EXISTS `bh_payment_fpx_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_payment_fpx_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_quotation` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `fpx_serial_no` varchar(30) DEFAULT NULL,
  `fpx_trans_id` varchar(30) DEFAULT NULL,
  `fpx_date` datetime DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `deposit_amount` decimal(11,2) DEFAULT NULL,
  `amount_paid` decimal(11,2) DEFAULT NULL,
  `fpx_status` int(11) DEFAULT NULL,
  `fpx_trans_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_payment_fpx_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `bh_payment_fpx_fk_bh_quotation_foreign` (`fk_bh_quotation`),
  KEY `bh_payment_fpx_fk_lkp_payment_type_foreign` (`fk_lkp_payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_question`
--

DROP TABLE IF EXISTS `bh_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_question_cat` int(10) unsigned DEFAULT NULL,
  `bq_question` varchar(155) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_question_fk_lkp_question_cat_foreign` (`fk_lkp_question_cat`),
  CONSTRAINT `bh_question_fk_lkp_question_cat_foreign` FOREIGN KEY (`fk_lkp_question_cat`) REFERENCES `lkp_question_cat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_quotation`
--

DROP TABLE IF EXISTS `bh_quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_quotation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_lkp_discount_type` int(10) unsigned DEFAULT NULL,
  `bq_quotation_no` varchar(50) DEFAULT NULL,
  `bq_quotation_date` date DEFAULT NULL,
  `bq_quotation_status` int(11) DEFAULT NULL,
  `bq_total_amount` decimal(11,2) DEFAULT NULL,
  `bq_deposit` decimal(11,2) DEFAULT NULL,
  `bq_payment_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_quotation_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `bh_quotation_fk_users_foreign` (`fk_users`),
  KEY `bh_quotation_fk_lkp_discount_type_foreign` (`fk_lkp_discount_type`),
  KEY `bq_quotation_status` (`bq_quotation_status`),
  KEY `bq_payment_status` (`bq_payment_status`),
  CONSTRAINT `bh_quotation_fk_lkp_discount_type_foreign` FOREIGN KEY (`fk_lkp_discount_type`) REFERENCES `lkp_discount_type` (`id`),
  CONSTRAINT `bh_quotation_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103331 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_quotation_detail`
--

DROP TABLE IF EXISTS `bh_quotation_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_quotation_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_quotation` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `fk_bh_equipment` int(10) unsigned DEFAULT NULL,
  `fk_bh_booking` int(11) DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(5) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_quotation_detail_fk_bh_quotation_foreign` (`fk_bh_quotation`),
  KEY `bh_quotation_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `bh_quotation_detail_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_quotation_detail_fk_bh_equipment_foreign` (`fk_bh_equipment`),
  CONSTRAINT `bh_quotation_detail_fk_bh_equipment_foreign` FOREIGN KEY (`fk_bh_equipment`) REFERENCES `bh_equipment` (`id`),
  CONSTRAINT `bh_quotation_detail_fk_bh_hall_foreign` FOREIGN KEY (`fk_bh_hall`) REFERENCES `bh_hall` (`id`),
  CONSTRAINT `bh_quotation_detail_fk_bh_quotation_foreign` FOREIGN KEY (`fk_bh_quotation`) REFERENCES `bh_quotation` (`id`),
  CONSTRAINT `bh_quotation_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5923 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_rating`
--

DROP TABLE IF EXISTS `bh_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_rating` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `hall_id` int(11) DEFAULT NULL,
  `et_facility_type_id` int(11) DEFAULT NULL,
  `br_comment` varchar(255) DEFAULT NULL,
  `br_total_rating` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `picc_hall_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_rating_fk_users_foreign` (`fk_users`),
  KEY `bh_rating_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `et_facility_type_id` (`et_facility_type_id`),
  KEY `hall_id` (`hall_id`),
  KEY `br_total_rating` (`br_total_rating`),
  CONSTRAINT `bh_rating_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `bh_rating_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_rating_detail`
--

DROP TABLE IF EXISTS `bh_rating_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_rating_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_rating` int(10) unsigned DEFAULT NULL,
  `fk_bh_question` int(10) unsigned DEFAULT NULL,
  `fk_bh_answer` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_rating_detail_fk_bh_rating_foreign` (`fk_bh_rating`),
  KEY `bh_rating_detail_fk_bh_question_foreign` (`fk_bh_question`),
  KEY `bh_rating_detail_fk_bh_answer_foreign` (`fk_bh_answer`),
  CONSTRAINT `bh_rating_detail_fk_bh_answer_foreign` FOREIGN KEY (`fk_bh_answer`) REFERENCES `bh_answer` (`id`),
  CONSTRAINT `bh_rating_detail_fk_bh_question_foreign` FOREIGN KEY (`fk_bh_question`) REFERENCES `bh_question` (`id`),
  CONSTRAINT `bh_rating_detail_fk_bh_rating_foreign` FOREIGN KEY (`fk_bh_rating`) REFERENCES `bh_rating` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_refund`
--

DROP TABLE IF EXISTS `bh_refund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_refund` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `name` varchar(155) DEFAULT NULL,
  `date_refund` date DEFAULT NULL,
  `reason` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `amount_refund` decimal(11,2) DEFAULT NULL,
  `ic_no` int(11) DEFAULT NULL,
  `phone_no` varchar(10) DEFAULT NULL,
  `reference_no` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_refund_request`
--

DROP TABLE IF EXISTS `bh_refund_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_refund_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `name` varchar(155) DEFAULT NULL,
  `date_refund` date DEFAULT NULL,
  `reason` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `amount_refund` decimal(11,2) DEFAULT NULL,
  `ic_no` int(11) DEFAULT NULL,
  `phone_no` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_reference` varchar(50) DEFAULT NULL,
  `reference_no` varchar(30) DEFAULT NULL,
  `receipt_no` varchar(30) DEFAULT NULL,
  `brr_status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_vt`
--

DROP TABLE IF EXISTS `bh_vt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_vt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vt_id` int(10) DEFAULT NULL,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `vt_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_bh_hall_picc` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_vt_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_vt_fk_bh_hall_picc_foreign` (`fk_bh_hall_picc`),
  KEY `bh_vt_fk_et_facility_detail_foreign` (`fk_et_facility_detail`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_vt_18042016`
--

DROP TABLE IF EXISTS `bh_vt_18042016`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_vt_18042016` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vt_id` int(10) DEFAULT NULL,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `vt_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_bh_hall_picc` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_vt_fk_bh_hall_foreign` (`fk_bh_hall`),
  KEY `bh_vt_fk_bh_hall_picc_foreign` (`fk_bh_hall_picc`),
  KEY `bh_vt_fk_et_facility_detail_foreign` (`fk_et_facility_detail`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bh_vt_old`
--

DROP TABLE IF EXISTS `bh_vt_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bh_vt_old` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vt_id` int(10) DEFAULT NULL,
  `fk_bh_hall` int(10) unsigned DEFAULT NULL,
  `vt_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_bh_hall_picc` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bh_vt_fk_bh_hall_foreign` (`fk_bh_hall`) USING BTREE,
  KEY `bh_vt_fk_bh_hall_picc_foreign` (`fk_bh_hall_picc`) USING BTREE,
  KEY `bh_vt_fk_et_facility_detail_foreign` (`fk_et_facility_detail`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_notification`
--

DROP TABLE IF EXISTS `email_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0-tidak hantar 1- hantar',
  `done` int(11) DEFAULT NULL COMMENT '1-selesai 2-selain selesai',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_booking_facility`
--

DROP TABLE IF EXISTS `et_booking_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_booking_facility` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_lkp_discount` int(10) unsigned DEFAULT NULL,
  `ebf_start_date` date DEFAULT NULL,
  `ebf_end_date` date DEFAULT NULL,
  `ebf_no_of_day` int(11) DEFAULT NULL,
  `ebf_subtotal` decimal(11,2) DEFAULT NULL,
  `ebf_deposit` decimal(11,2) DEFAULT NULL,
  `ebf_facility_indi` int(11) DEFAULT NULL,
  `ebf_total_before_gst` decimal(11,2) DEFAULT NULL,
  `ebf_total_gst` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_booking_facility_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `et_booking_facility_fk_et_facility_type_foreign` (`fk_et_facility_type`),
  KEY `et_booking_facility_fk_et_facility_detail_foreign` (`fk_et_facility_detail`),
  KEY `et_booking_facility_fk_lkp_discount_foreign` (`fk_lkp_discount`),
  KEY `ebf_start_date` (`ebf_start_date`,`ebf_end_date`),
  KEY `ebf_facility_indi` (`ebf_facility_indi`),
  CONSTRAINT `et_booking_facility_fk_et_facility_detail_foreign` FOREIGN KEY (`fk_et_facility_detail`) REFERENCES `et_facility_detail` (`id`),
  CONSTRAINT `et_booking_facility_fk_et_facility_type_foreign` FOREIGN KEY (`fk_et_facility_type`) REFERENCES `et_facility_type` (`id`),
  CONSTRAINT `et_booking_facility_fk_lkp_discount_foreign` FOREIGN KEY (`fk_lkp_discount`) REFERENCES `lkp_discount` (`id`),
  CONSTRAINT `et_booking_facility_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=368734 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_booking_facility_detail`
--

DROP TABLE IF EXISTS `et_booking_facility_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_booking_facility_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `fk_lkp_event` int(10) unsigned DEFAULT NULL,
  `ebfd_event_name` varchar(100) DEFAULT NULL,
  `ebfd_event_desc` varchar(155) DEFAULT NULL,
  `ebfd_others` varchar(155) DEFAULT NULL,
  `ebfd_total_pax` int(11) DEFAULT NULL,
  `ebfd_user_apply` varchar(100) DEFAULT NULL,
  `ebfd_venue` varchar(255) DEFAULT NULL,
  `ebfd_address` varchar(500) DEFAULT NULL,
  `ebfd_contact_no` varchar(20) DEFAULT NULL,
  `ebfd_vvip` int(11) DEFAULT NULL,
  `ebfd_vip` int(11) DEFAULT NULL,
  `ebfd_participant` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_booking_facility_detail_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `et_booking_facility_detail_fk_lkp_event_foreign` (`fk_lkp_event`),
  CONSTRAINT `et_booking_facility_detail_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_booking_facility_detail_fk_lkp_event_foreign` FOREIGN KEY (`fk_lkp_event`) REFERENCES `lkp_event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27973 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_confirm_booking`
--

DROP TABLE IF EXISTS `et_confirm_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_confirm_booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `ecb_date_booking` date DEFAULT NULL,
  `ecb_flag_indicator` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_confirm_booking_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `et_confirm_booking_fk_et_facility_type_foreign` (`fk_et_facility_type`),
  KEY `ecb_date_booking` (`ecb_date_booking`),
  KEY `ecb_flag_indicator` (`ecb_flag_indicator`),
  CONSTRAINT `et_confirm_booking_fk_et_facility_type_foreign` FOREIGN KEY (`fk_et_facility_type`) REFERENCES `et_facility_type` (`id`),
  CONSTRAINT `et_confirm_booking_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301078 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_confirm_booking_detail`
--

DROP TABLE IF EXISTS `et_confirm_booking_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_confirm_booking_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_confirm_booking` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `ecbd_date_booking` date DEFAULT NULL,
  `fk_et_facility_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_confirm_booking_detail_fk_et_facility_detail_foreign` (`fk_et_facility_detail`),
  KEY `et_confirm_booking_detail_fk_et_slot_time_foreign` (`fk_et_slot_time`),
  KEY `et_confirm_booking_detail_fk_et_confirm_booking_foreign` (`fk_et_confirm_booking`),
  KEY `ecbd_date_booking` (`ecbd_date_booking`),
  CONSTRAINT `et_confirm_booking_detail_fk_et_confirm_booking_foreign` FOREIGN KEY (`fk_et_confirm_booking`) REFERENCES `et_confirm_booking` (`id`) ON DELETE CASCADE,
  CONSTRAINT `et_confirm_booking_detail_fk_et_facility_detail_foreign` FOREIGN KEY (`fk_et_facility_detail`) REFERENCES `et_facility_detail` (`id`),
  CONSTRAINT `et_confirm_booking_detail_fk_et_slot_time_foreign` FOREIGN KEY (`fk_et_slot_time`) REFERENCES `et_slot_time` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=801005 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_confirm_booking_detail_log`
--

DROP TABLE IF EXISTS `et_confirm_booking_detail_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_confirm_booking_detail_log` (
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `fk_et_confirm_booking` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `ecbd_date_booking` date DEFAULT NULL,
  `fk_et_facility_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_confirm_booking_log`
--

DROP TABLE IF EXISTS `et_confirm_booking_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_confirm_booking_log` (
  `id` int(10) unsigned NOT NULL DEFAULT 0,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `ecb_date_booking` date DEFAULT NULL,
  `ecb_flag_indicator` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_equipment`
--

DROP TABLE IF EXISTS `et_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `ee_name` varchar(100) DEFAULT NULL,
  `ee_code` varchar(25) DEFAULT NULL,
  `ee_fee_code` varchar(25) DEFAULT NULL,
  `ee_quantity` int(11) DEFAULT NULL,
  `ee_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_equipment_fk_lkp_location_foreign` (`fk_lkp_location`),
  CONSTRAINT `et_equipment_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_equipment_book`
--

DROP TABLE IF EXISTS `et_equipment_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_equipment_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment_price` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `eeb_booking_date` date DEFAULT NULL,
  `eeb_unit_price` decimal(11,2) DEFAULT NULL,
  `eeb_quantity` int(11) DEFAULT NULL,
  `eeb_total_price` decimal(11,2) DEFAULT NULL,
  `eeb_discount_type_rm` decimal(11,2) DEFAULT NULL,
  `eeb_discount_rm` decimal(11,2) DEFAULT NULL,
  `eeb_special_disc` int(11) DEFAULT NULL,
  `eeb_special_disc_rm` decimal(11,2) DEFAULT NULL,
  `eeb_total` decimal(11,2) DEFAULT NULL,
  `eeb_gst_rm` decimal(11,2) DEFAULT NULL,
  `eeb_subtotal` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_equipment_book_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `et_equipment_book_fk_et_function_foreign` (`fk_et_function`),
  KEY `et_equipment_book_fk_et_equipment_foreign` (`fk_et_equipment`),
  KEY `et_equipment_book_fk_et_equipment_price_foreign` (`fk_et_equipment_price`),
  KEY `et_equipment_book_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  CONSTRAINT `et_equipment_book_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_equipment_book_fk_et_equipment_foreign` FOREIGN KEY (`fk_et_equipment`) REFERENCES `et_equipment` (`id`),
  CONSTRAINT `et_equipment_book_fk_et_equipment_price_foreign` FOREIGN KEY (`fk_et_equipment_price`) REFERENCES `et_equipment_price` (`id`),
  CONSTRAINT `et_equipment_book_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`),
  CONSTRAINT `et_equipment_book_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8736 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_equipment_price`
--

DROP TABLE IF EXISTS `et_equipment_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_equipment_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_equipment` int(10) unsigned DEFAULT NULL,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `eep_day_cat` int(11) DEFAULT NULL,
  `eep_unit_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_equipment_price_fk_et_equipment_foreign` (`fk_et_equipment`),
  KEY `et_equipment_price_fk_et_function_foreign` (`fk_et_function`),
  KEY `et_equipment_price_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  CONSTRAINT `et_equipment_price_fk_et_equipment_foreign` FOREIGN KEY (`fk_et_equipment`) REFERENCES `et_equipment` (`id`),
  CONSTRAINT `et_equipment_price_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`),
  CONSTRAINT `et_equipment_price_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility`
--

DROP TABLE IF EXISTS `et_facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ef_type` int(11) DEFAULT NULL,
  `ef_desc` varchar(155) DEFAULT NULL,
  `ef_code` varchar(55) DEFAULT NULL,
  `ef_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ef_type` (`ef_type`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_close`
--

DROP TABLE IF EXISTS `et_facility_close`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_close` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility_type` int(11) DEFAULT NULL,
  `day_num` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_et_facility_type` (`fk_et_facility_type`),
  KEY `day_num` (`day_num`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_detail`
--

DROP TABLE IF EXISTS `et_facility_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `efd_name` varchar(100) DEFAULT NULL,
  `efd_area` int(11) DEFAULT NULL,
  `efd_capasity` int(11) DEFAULT NULL,
  `efd_rent_cat` int(11) DEFAULT NULL,
  `efd_fee_code` varchar(25) DEFAULT NULL,
  `efd_code_deposit` varchar(25) DEFAULT NULL,
  `efd_deposit_percent` int(11) DEFAULT NULL,
  `efd_payment_counter` int(11) DEFAULT NULL,
  `efd_status` int(11) DEFAULT NULL,
  `efd_filename` varchar(155) DEFAULT NULL,
  `efd_file_location` varchar(255) DEFAULT NULL,
  `efd_parent` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_facility_detail_fk_et_facility_type_foreign` (`fk_et_facility_type`),
  CONSTRAINT `et_facility_detail_fk_et_facility_type_foreign` FOREIGN KEY (`fk_et_facility_type`) REFERENCES `et_facility_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_foc`
--

DROP TABLE IF EXISTS `et_facility_foc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_foc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility_type` int(11) DEFAULT NULL,
  `day_num` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_et_facility_type` (`fk_et_facility_type`),
  KEY `day_num` (`day_num`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_price`
--

DROP TABLE IF EXISTS `et_facility_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility` int(10) unsigned DEFAULT NULL,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_et_package` int(10) unsigned DEFAULT NULL,
  `fk_lkp_slot_cat` int(10) DEFAULT NULL,
  `efp_day_cat` int(11) DEFAULT NULL,
  `efp_unit_price` decimal(11,2) DEFAULT NULL,
  `efp_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_facility_price_fk_et_facility_foreign` (`fk_et_facility`),
  KEY `et_facility_price_fk_et_function_foreign` (`fk_et_function`),
  KEY `et_facility_price_fk_et_slot_time_foreign` (`fk_et_slot_time`),
  KEY `et_facility_price_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `et_facility_price_fk_et_package_foreign` (`fk_et_package`),
  CONSTRAINT `et_facility_price_fk_et_facility_foreign` FOREIGN KEY (`fk_et_facility`) REFERENCES `et_facility` (`id`),
  CONSTRAINT `et_facility_price_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`),
  CONSTRAINT `et_facility_price_fk_et_package_foreign` FOREIGN KEY (`fk_et_package`) REFERENCES `et_package` (`id`),
  CONSTRAINT `et_facility_price_fk_et_slot_time_foreign` FOREIGN KEY (`fk_et_slot_time`) REFERENCES `et_slot_time` (`id`),
  CONSTRAINT `et_facility_price_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1684 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_type`
--

DROP TABLE IF EXISTS `et_facility_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `fk_et_facility` int(10) unsigned DEFAULT NULL,
  `eft_type_desc` varchar(100) DEFAULT NULL,
  `eft_parent` int(11) DEFAULT NULL,
  `eft_marry_indi` int(11) DEFAULT NULL,
  `eft_status` int(11) DEFAULT NULL,
  `eft_no_of_slot` int(11) DEFAULT NULL,
  `cover_img` varchar(100) DEFAULT NULL,
  `virtual_img` varchar(100) DEFAULT NULL,
  `eft_uuid` varchar(100) DEFAULT NULL,
  `highlight` text DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_facility_type_fk_lkp_location_foreign` (`fk_lkp_location`),
  KEY `et_facility_type_fk_et_facility_foreign` (`fk_et_facility`),
  KEY `eft_parent` (`eft_parent`),
  CONSTRAINT `et_facility_type_fk_et_facility_foreign` FOREIGN KEY (`fk_et_facility`) REFERENCES `et_facility` (`id`),
  CONSTRAINT `et_facility_type_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_type_image`
--

DROP TABLE IF EXISTS `et_facility_type_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_type_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_et_facility_type` (`fk_et_facility_type`),
  CONSTRAINT `et_facility_type_image_ibfk_1` FOREIGN KEY (`fk_et_facility_type`) REFERENCES `et_facility_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_facility_type_slot_restriction`
--

DROP TABLE IF EXISTS `et_facility_type_slot_restriction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_facility_type_slot_restriction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility_type` int(10) unsigned DEFAULT NULL,
  `max_slot` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `expiry_by_month` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `et_facility_type_slot_restriction_fk_et_facility_type_foreign` (`fk_et_facility_type`),
  CONSTRAINT `et_facility_type_slot_restriction_fk_et_facility_type_foreign` FOREIGN KEY (`fk_et_facility_type`) REFERENCES `et_facility_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_fpx_detail`
--

DROP TABLE IF EXISTS `et_fpx_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_fpx_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_payment_fpx` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment` int(11) DEFAULT NULL,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(5) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_fpx_detail_fk_et_payment_fpx_foreign` (`fk_et_payment_fpx`),
  KEY `et_fpx_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `et_fpx_detail_fk_et_facility_detail_foreign` (`fk_et_facility_detail`),
  KEY `et_fpx_detail_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  CONSTRAINT `et_fpx_detail_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_fpx_detail_fk_et_facility_detail_foreign` FOREIGN KEY (`fk_et_facility_detail`) REFERENCES `et_facility_detail` (`id`),
  CONSTRAINT `et_fpx_detail_fk_et_payment_fpx_foreign` FOREIGN KEY (`fk_et_payment_fpx`) REFERENCES `et_payment_fpx` (`id`),
  CONSTRAINT `et_fpx_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91435 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_fpx_log`
--

DROP TABLE IF EXISTS `et_fpx_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_fpx_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(100) DEFAULT NULL,
  `fpx_order_no` varchar(30) DEFAULT NULL,
  `fpx_txn_reference` varchar(30) DEFAULT NULL,
  `fpx_txn_id` varchar(30) DEFAULT NULL,
  `sap_doc_no` varchar(20) DEFAULT NULL,
  `fpx_date` datetime DEFAULT NULL,
  `fpx_txn_time` varchar(30) DEFAULT NULL,
  `fpx_status` varchar(30) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `test` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_function`
--

DROP TABLE IF EXISTS `et_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_function` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ef_function_code` varchar(25) DEFAULT NULL,
  `ef_desc` varchar(255) DEFAULT NULL,
  `ef_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_function_detail`
--

DROP TABLE IF EXISTS `et_function_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_function_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `fk_et_facility` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_function_detail_fk_et_function_foreign` (`fk_et_function`),
  KEY `et_function_detail_fk_et_facility_foreign` (`fk_et_facility`),
  CONSTRAINT `et_function_detail_fk_et_facility_foreign` FOREIGN KEY (`fk_et_facility`) REFERENCES `et_facility` (`id`),
  CONSTRAINT `et_function_detail_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_hall_book`
--

DROP TABLE IF EXISTS `et_hall_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_hall_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `ehb_booking_date` date DEFAULT NULL,
  `ehb_total_hour` int(11) DEFAULT NULL,
  `ehb_total` decimal(11,2) DEFAULT NULL,
  `ehb_total_before_gst` decimal(11,2) DEFAULT NULL,
  `ehb_total_gst` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_hall_book_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `et_hall_book_fk_et_function_foreign` (`fk_et_function`),
  KEY `ehb_booking_date` (`ehb_booking_date`),
  CONSTRAINT `et_hall_book_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_hall_book_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31188 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_hall_time`
--

DROP TABLE IF EXISTS `et_hall_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_hall_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_hall_book` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_price` int(10) unsigned DEFAULT NULL,
  `eht_price` decimal(11,2) DEFAULT NULL,
  `eht_discount_type_rm` decimal(11,2) DEFAULT NULL,
  `eht_special_disc` int(11) DEFAULT NULL,
  `eht_special_disc_rm` decimal(11,2) DEFAULT NULL,
  `eht_discount_rm` decimal(11,2) DEFAULT NULL,
  `eht_total` decimal(11,2) DEFAULT NULL,
  `eht_gst_code` int(11) DEFAULT NULL,
  `eht_gst_rm` decimal(11,2) DEFAULT NULL,
  `eht_subtotal` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_hall_time_fk_et_hall_book_foreign` (`fk_et_hall_book`),
  KEY `et_hall_time_fk_et_slot_time_foreign` (`fk_et_slot_time`),
  KEY `et_hall_time_fk_et_facility_price_foreign` (`fk_et_facility_price`),
  KEY `eht_gst_code` (`eht_gst_code`),
  CONSTRAINT `et_hall_time_fk_et_facility_price_foreign` FOREIGN KEY (`fk_et_facility_price`) REFERENCES `et_facility_price` (`id`),
  CONSTRAINT `et_hall_time_fk_et_hall_book_foreign` FOREIGN KEY (`fk_et_hall_book`) REFERENCES `et_hall_book` (`id`),
  CONSTRAINT `et_hall_time_fk_et_slot_time_foreign` FOREIGN KEY (`fk_et_slot_time`) REFERENCES `et_slot_time` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91915 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_package`
--

DROP TABLE IF EXISTS `et_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ep_name` varchar(155) DEFAULT NULL,
  `ep_day_cat` int(11) DEFAULT NULL,
  `ep_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_package_detail`
--

DROP TABLE IF EXISTS `et_package_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_package_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_package` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `fk_et_function` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_price` int(10) unsigned DEFAULT NULL,
  `epd_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_package_detail_fk_et_package_foreign` (`fk_et_package`),
  KEY `et_package_detail_fk_et_slot_time_foreign` (`fk_et_slot_time`),
  KEY `et_package_detail_fk_et_function_foreign` (`fk_et_function`),
  KEY `et_package_detail_fk_et_facility_price_foreign` (`fk_et_facility_price`),
  CONSTRAINT `et_package_detail_fk_et_facility_price_foreign` FOREIGN KEY (`fk_et_facility_price`) REFERENCES `et_facility_price` (`id`),
  CONSTRAINT `et_package_detail_fk_et_function_foreign` FOREIGN KEY (`fk_et_function`) REFERENCES `et_function` (`id`),
  CONSTRAINT `et_package_detail_fk_et_package_foreign` FOREIGN KEY (`fk_et_package`) REFERENCES `et_package` (`id`),
  CONSTRAINT `et_package_detail_fk_et_slot_time_foreign` FOREIGN KEY (`fk_et_slot_time`) REFERENCES `et_slot_time` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_payment_detail`
--

DROP TABLE IF EXISTS `et_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_payment` int(10) unsigned DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment` int(10) unsigned DEFAULT NULL,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(5) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_payment_detail_fk_bh_payment_foreign` (`fk_bh_payment`),
  KEY `et_payment_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  KEY `et_payment_detail_fk_et_facility_detail_foreign` (`fk_et_facility_detail`),
  KEY `et_payment_detail_fk_et_equipment_foreign` (`fk_et_equipment`),
  KEY `et_payment_detail_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `product_indicator` (`product_indicator`),
  CONSTRAINT `et_payment_detail_fk_bh_payment_foreign` FOREIGN KEY (`fk_bh_payment`) REFERENCES `bh_payment` (`id`),
  CONSTRAINT `et_payment_detail_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_payment_detail_fk_et_facility_detail_foreign` FOREIGN KEY (`fk_et_facility_detail`) REFERENCES `et_facility_detail` (`id`),
  CONSTRAINT `et_payment_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86550 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_payment_fpx`
--

DROP TABLE IF EXISTS `et_payment_fpx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_payment_fpx` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_bh_quotation` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_mode` int(11) DEFAULT 4,
  `bank` varchar(100) DEFAULT NULL,
  `sap_doc_no` varchar(20) DEFAULT NULL,
  `fpx_serial_no` varchar(30) DEFAULT NULL,
  `fpx_trans_id` varchar(30) DEFAULT NULL,
  `fpx_txn_reference` varchar(30) DEFAULT NULL,
  `fpx_date` datetime DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `deposit_amount` decimal(11,2) DEFAULT NULL,
  `amount_paid` decimal(11,2) DEFAULT NULL,
  `fpx_status` int(11) DEFAULT NULL,
  `fpx_trans_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_payment_fpx_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `et_payment_fpx_fk_bh_quotation_foreign` (`fk_bh_quotation`),
  KEY `et_payment_fpx_fk_lkp_payment_type_foreign` (`fk_lkp_payment_type`),
  KEY `fpx_trans_id` (`fpx_trans_id`),
  KEY `fpx_status` (`fpx_status`),
  CONSTRAINT `et_payment_fpx_fk_bh_quotation_foreign` FOREIGN KEY (`fk_bh_quotation`) REFERENCES `bh_quotation` (`id`),
  CONSTRAINT `et_payment_fpx_fk_lkp_payment_type_foreign` FOREIGN KEY (`fk_lkp_payment_type`) REFERENCES `lkp_payment_type` (`id`),
  CONSTRAINT `et_payment_fpx_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52570 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_payment_fpx_log`
--

DROP TABLE IF EXISTS `et_payment_fpx_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_payment_fpx_log` (
  `fpx_trans_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fpx_serial_no` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fpx_trans_date` datetime DEFAULT NULL,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_bh_quotation` int(11) DEFAULT NULL,
  KEY `fpx_trans_id` (`fpx_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_quotation_detail`
--

DROP TABLE IF EXISTS `et_quotation_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_quotation_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_bh_quotation` int(10) unsigned DEFAULT NULL,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_detail` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment` int(10) unsigned DEFAULT NULL,
  `fk_et_equipment_book` int(10) DEFAULT NULL,
  `fk_lkp_gst_rate` int(10) unsigned DEFAULT NULL,
  `product_indicator` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `unit_price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `code_gst` varchar(5) DEFAULT NULL,
  `gst_amount` decimal(11,2) DEFAULT NULL,
  `total_amount` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_quotation_detail_fk_bh_quotation_foreign` (`fk_bh_quotation`),
  KEY `et_quotation_detail_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `et_quotation_detail_fk_et_facility_detail_foreign` (`fk_et_facility_detail`),
  KEY `et_quotation_detail_fk_et_equipment_foreign` (`fk_et_equipment`),
  KEY `et_quotation_detail_fk_lkp_gst_rate_foreign` (`fk_lkp_gst_rate`),
  CONSTRAINT `et_quotation_detail_fk_bh_quotation_foreign` FOREIGN KEY (`fk_bh_quotation`) REFERENCES `bh_quotation` (`id`),
  CONSTRAINT `et_quotation_detail_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`),
  CONSTRAINT `et_quotation_detail_fk_et_facility_detail_foreign` FOREIGN KEY (`fk_et_facility_detail`) REFERENCES `et_facility_detail` (`id`),
  CONSTRAINT `et_quotation_detail_fk_lkp_gst_rate_foreign` FOREIGN KEY (`fk_lkp_gst_rate`) REFERENCES `lkp_gst_rate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232944 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_reschedule`
--

DROP TABLE IF EXISTS `et_reschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_reschedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `er_initial_start_date` date DEFAULT NULL,
  `er_initial_end_date` date DEFAULT NULL,
  `er_new_start_date` date DEFAULT NULL,
  `er_new_end_date` date DEFAULT NULL,
  `er_reason` varchar(255) DEFAULT NULL,
  `er_initial_slot` int(11) DEFAULT NULL,
  `er_new_slot` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_reschedule_fk_main_booking_foreign` (`fk_main_booking`),
  CONSTRAINT `et_reschedule_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140235 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_slot_price`
--

DROP TABLE IF EXISTS `et_slot_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_slot_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_facility` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_time` int(10) unsigned DEFAULT NULL,
  `fk_et_facility_price` int(10) unsigned DEFAULT NULL,
  `esp_day_cat` int(11) DEFAULT NULL,
  `esp_squash_indi` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_slot_price_fk_et_facility_foreign` (`fk_et_facility`),
  KEY `et_slot_price_fk_et_slot_time_foreign` (`fk_et_slot_time`),
  KEY `et_slot_price_fk_et_facility_price_foreign` (`fk_et_facility_price`),
  KEY `esp_squash_indi` (`esp_squash_indi`),
  CONSTRAINT `et_slot_price_fk_et_facility_foreign` FOREIGN KEY (`fk_et_facility`) REFERENCES `et_facility` (`id`),
  CONSTRAINT `et_slot_price_fk_et_facility_price_foreign` FOREIGN KEY (`fk_et_facility_price`) REFERENCES `et_facility_price` (`id`),
  CONSTRAINT `et_slot_price_fk_et_slot_time_foreign` FOREIGN KEY (`fk_et_slot_time`) REFERENCES `et_slot_time` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=643 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_slot_time`
--

DROP TABLE IF EXISTS `et_slot_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_slot_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_slot_cat` int(10) unsigned DEFAULT NULL,
  `est_rent_cat` int(11) DEFAULT NULL,
  `est_slot_time` varchar(155) DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `display_order` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `et_slot_time_fk_lkp_slot_cat_foreign` (`fk_lkp_slot_cat`),
  CONSTRAINT `et_slot_time_fk_lkp_slot_cat_foreign` FOREIGN KEY (`fk_lkp_slot_cat`) REFERENCES `lkp_slot_cat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_sms`
--

DROP TABLE IF EXISTS `et_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(5) NOT NULL,
  `fk_main_booking` int(11) unsigned NOT NULL,
  `no_permohonan` varchar(255) DEFAULT NULL,
  `nama_fasiliti` varchar(100) DEFAULT NULL,
  `tarikh_guna` datetime DEFAULT NULL,
  `nama_pemohon` varchar(255) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `bayaran` decimal(11,2) DEFAULT NULL,
  `lkp_status` int(11) DEFAULT NULL,
  `sms_flag` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55566 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_sport_book`
--

DROP TABLE IF EXISTS `et_sport_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_sport_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_booking_facility` int(10) unsigned DEFAULT NULL,
  `esb_booking_date` date DEFAULT NULL,
  `esb_total_hour` int(11) DEFAULT NULL,
  `esb_total` decimal(11,2) DEFAULT NULL,
  `esb_total_before_gst` decimal(11,2) DEFAULT NULL,
  `esb_total_gst` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_sport_book_fk_et_booking_facility_foreign` (`fk_et_booking_facility`),
  KEY `esb_booking_date` (`esb_booking_date`),
  CONSTRAINT `et_sport_book_fk_et_booking_facility_foreign` FOREIGN KEY (`fk_et_booking_facility`) REFERENCES `et_booking_facility` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=338512 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `et_sport_time`
--

DROP TABLE IF EXISTS `et_sport_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `et_sport_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_et_sport_book` int(10) unsigned DEFAULT NULL,
  `fk_et_slot_price` int(10) unsigned DEFAULT NULL,
  `est_price` decimal(11,2) DEFAULT NULL,
  `est_discount_type_rm` decimal(11,2) DEFAULT NULL,
  `est_special_disc` int(11) DEFAULT NULL,
  `est_special_disc_rm` decimal(11,2) DEFAULT NULL,
  `est_discount_rm` decimal(11,2) DEFAULT NULL,
  `est_total` decimal(11,2) DEFAULT NULL,
  `est_gst_code` int(11) DEFAULT NULL,
  `est_gst_rm` decimal(11,2) DEFAULT NULL,
  `est_subtotal` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `et_sport_time_fk_et_sport_book_foreign` (`fk_et_sport_book`),
  KEY `et_sport_time_fk_et_slot_price_foreign` (`fk_et_slot_price`),
  CONSTRAINT `et_sport_time_fk_et_slot_price_foreign` FOREIGN KEY (`fk_et_slot_price`) REFERENCES `et_slot_price` (`id`),
  CONSTRAINT `et_sport_time_fk_et_sport_book_foreign` FOREIGN KEY (`fk_et_sport_book`) REFERENCES `et_sport_book` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=319440 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fpxtransaction_remote`
--

DROP TABLE IF EXISTS `fpxtransaction_remote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fpxtransaction_remote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fpxTransID` varchar(50) DEFAULT NULL,
  `fpxTransNo` varchar(50) DEFAULT NULL,
  `fpxTransDate` datetime DEFAULT NULL,
  `fpxTransSAPNo` varchar(50) DEFAULT NULL,
  `fpxTransBank` varchar(45) DEFAULT NULL,
  `fpxTransAmount` double DEFAULT NULL,
  `fpxTransSourceSystem` varchar(10) DEFAULT NULL,
  `fpxTransFPXNo` varchar(15) DEFAULT NULL,
  `fpxSAPStatus` varchar(1) DEFAULT '0' COMMENT '0=belum, 1=Sent to Oracle, 2 = Oracle return resitNo',
  `fpxTransStatus` varchar(45) DEFAULT NULL,
  `fpxFlagSentExternalSys` varchar(45) DEFAULT '0' COMMENT '0=Belum Hantar, 01=Dah hantar',
  `fpxFlagDirectIndirect` varchar(1) DEFAULT '0' COMMENT '0=Direct, 1=Indirect',
  `feeCode` varchar(20) DEFAULT NULL,
  `BR_CUSTOMER_ADDRESS1` varchar(50) DEFAULT NULL,
  `BR_CUSTOMER_ADDRESS2` varchar(50) DEFAULT NULL,
  `BR_CUSTOMER_ADDRESS3` varchar(50) DEFAULT NULL,
  `BR_CUSTOMER_ADDRESS4` varchar(50) DEFAULT NULL,
  `BR_CUSTOMER_POSTCODE` varchar(5) DEFAULT NULL,
  `BR_CUSTOMER_STATE` varchar(50) DEFAULT NULL,
  `CUST_NAME` varchar(500) DEFAULT NULL,
  `STATUOTARY_ID` varchar(45) DEFAULT NULL,
  `DOC_REFERENCE` varchar(45) DEFAULT NULL,
  `COLLECTION_REFERENCE` varchar(500) DEFAULT NULL,
  `SUB_SYS_CODE` varchar(10) DEFAULT NULL,
  `STATUS_DE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132200 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci CONNECTION='fpx_tempahan/fpxtransaction';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_country`
--

DROP TABLE IF EXISTS `lkp_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lc_description` varchar(155) DEFAULT NULL,
  `lc_status` int(11) DEFAULT NULL,
  `lc_code` varchar(55) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lc_status` (`lc_status`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_deposit_rate`
--

DROP TABLE IF EXISTS `lkp_deposit_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_deposit_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `ldr_rate` int(11) DEFAULT NULL,
  `ldr_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lkp_deposit_rate_fk_lkp_location_foreign` (`fk_lkp_location`),
  CONSTRAINT `lkp_deposit_rate_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_discount`
--

DROP TABLE IF EXISTS `lkp_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_discount` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `discount_rate` int(11) DEFAULT NULL,
  `ldt_status` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_discount_type`
--

DROP TABLE IF EXISTS `lkp_discount_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_discount_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ldt_user_cat` varchar(100) DEFAULT NULL,
  `ldt_discount_rate` int(11) DEFAULT NULL,
  `ldt_indicator` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_event`
--

DROP TABLE IF EXISTS `lkp_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `le_description` varchar(255) DEFAULT NULL,
  `le_status` int(11) DEFAULT NULL,
  `le_category` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_filming_type`
--

DROP TABLE IF EXISTS `lkp_filming_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_filming_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_gst_rate`
--

DROP TABLE IF EXISTS `lkp_gst_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_gst_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lgr_description` varchar(255) DEFAULT NULL,
  `lgr_rate` decimal(16,16) DEFAULT NULL,
  `lgr_gst_code` varchar(5) DEFAULT NULL,
  `lgr_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lgr_gst_code` (`lgr_gst_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_holiday`
--

DROP TABLE IF EXISTS `lkp_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_holiday` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lh_name` varchar(155) DEFAULT NULL,
  `lh_start_date` date NOT NULL,
  `lh_end_date` date NOT NULL,
  `lh_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lh_start_date` (`lh_start_date`,`lh_end_date`),
  KEY `lh_status` (`lh_status`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_location`
--

DROP TABLE IF EXISTS `lkp_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lc_description` varchar(100) DEFAULT NULL,
  `lc_type` int(11) DEFAULT NULL,
  `lc_no_of_days` int(11) DEFAULT NULL,
  `lc_contact_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lc_status` int(11) DEFAULT NULL,
  `lc_addr` varchar(255) DEFAULT NULL,
  `lc_addr_town` varchar(155) DEFAULT NULL,
  `lc_addr_postcode` varchar(5) DEFAULT NULL,
  `fk_lkp_state` int(11) DEFAULT NULL,
  `fk_lkp_country` int(11) DEFAULT NULL,
  `lc_fax_no` varchar(15) DEFAULT NULL,
  `lc_ext` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_payment_mode`
--

DROP TABLE IF EXISTS `lkp_payment_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_payment_mode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpm_description` varchar(50) DEFAULT NULL,
  `lpm_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_payment_type`
--

DROP TABLE IF EXISTS `lkp_payment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_payment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lpt_description` varchar(55) DEFAULT NULL,
  `lpt_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_presint`
--

DROP TABLE IF EXISTS `lkp_presint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_presint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(155) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_question_cat`
--

DROP TABLE IF EXISTS `lkp_question_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_question_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_sequence`
--

DROP TABLE IF EXISTS `lkp_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_sequence` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_no` int(11) DEFAULT NULL,
  `quotation_no` int(11) DEFAULT NULL,
  `resit_no` int(11) DEFAULT NULL,
  `fpx_no` int(11) DEFAULT NULL,
  `fpx_serial_no` int(11) DEFAULT NULL,
  `fpx_serial_trans` int(11) DEFAULT NULL,
  `fpx_bil_trans` int(11) DEFAULT NULL,
  `txt_file` int(11) DEFAULT NULL,
  `txt_file_date` date DEFAULT NULL,
  `spa_no` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59739 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_slot_cat`
--

DROP TABLE IF EXISTS `lkp_slot_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_slot_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lsc_slot_desc` varchar(155) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_slot_rent`
--

DROP TABLE IF EXISTS `lkp_slot_rent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_slot_rent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lsc_slot_desc` varchar(155) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_spa`
--

DROP TABLE IF EXISTS `lkp_spa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_spa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_spa_type` int(10) unsigned DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  `seq_by` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lkp_spa_fk_lkp_spa_type_foreign` (`fk_lkp_spa_type`),
  CONSTRAINT `lkp_spa_fk_lkp_spa_type_foreign` FOREIGN KEY (`fk_lkp_spa_type`) REFERENCES `lkp_spa_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_spa_type`
--

DROP TABLE IF EXISTS `lkp_spa_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_spa_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_star`
--

DROP TABLE IF EXISTS `lkp_star`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_star` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ls_min` int(11) DEFAULT NULL,
  `ls_max` int(11) DEFAULT NULL,
  `ls_star` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ls_min` (`ls_min`,`ls_max`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_state`
--

DROP TABLE IF EXISTS `lkp_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_state` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ls_description` varchar(155) DEFAULT NULL,
  `ls_status` int(11) DEFAULT NULL,
  `ls_category` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_status`
--

DROP TABLE IF EXISTS `lkp_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ls_description` varchar(255) DEFAULT NULL,
  `ls_name` varchar(50) DEFAULT NULL,
  `ls_status` int(11) DEFAULT NULL,
  `ls_category` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_task`
--

DROP TABLE IF EXISTS `lkp_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_task` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_description` varchar(255) DEFAULT NULL,
  `task_group` varchar(10) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lkp_time_session`
--

DROP TABLE IF EXISTS `lkp_time_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lkp_time_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(100) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `main_booking`
--

DROP TABLE IF EXISTS `main_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `fk_lkp_status` int(10) unsigned DEFAULT NULL,
  `fk_lkp_deposit_rate` int(10) unsigned DEFAULT NULL,
  `fk_lkp_location` int(10) unsigned DEFAULT NULL,
  `fk_lkp_discount_type` int(10) unsigned DEFAULT NULL,
  `bmb_booking_no` varchar(255) DEFAULT NULL,
  `bmb_booking_date` datetime DEFAULT NULL,
  `bmb_subtotal` decimal(11,2) DEFAULT NULL,
  `bmb_deposit_rm` decimal(11,2) DEFAULT NULL,
  `bmb_staff_id` varchar(10) DEFAULT NULL,
  `bmb_kementerian` varchar(100) DEFAULT NULL,
  `bmb_agensi` varchar(100) DEFAULT NULL,
  `bmb_type_user` int(11) DEFAULT NULL,
  `bmb_total_book_hall` decimal(11,2) DEFAULT NULL,
  `bmb_total_equipment` decimal(11,2) DEFAULT NULL,
  `bmb_rounding` decimal(11,2) DEFAULT NULL,
  `bmb_deposit_rounding` decimal(11,2) DEFAULT NULL,
  `bmb_reason_cancel` varchar(155) DEFAULT NULL,
  `bmb_indicator` int(11) DEFAULT NULL,
  `bmb_reschedule_indi` int(11) DEFAULT NULL,
  `internal_indi` int(11) DEFAULT NULL,
  `bmb_total_word` varchar(100) DEFAULT NULL,
  `sap_no` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sms_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `main_booking_fk_users_foreign` (`fk_users`),
  KEY `main_booking_fk_lkp_status_foreign` (`fk_lkp_status`),
  KEY `main_booking_fk_lkp_deposit_rate_foreign` (`fk_lkp_deposit_rate`),
  KEY `main_booking_fk_lkp_location_foreign` (`fk_lkp_location`),
  KEY `main_booking_fk_lkp_discount_type_foreign` (`fk_lkp_discount_type`),
  KEY `updated_by` (`updated_by`),
  KEY `bmb_booking_no` (`bmb_booking_no`),
  KEY `updated_at` (`updated_at`),
  CONSTRAINT `main_booking_fk_lkp_deposit_rate_foreign` FOREIGN KEY (`fk_lkp_deposit_rate`) REFERENCES `lkp_deposit_rate` (`id`),
  CONSTRAINT `main_booking_fk_lkp_discount_type_foreign` FOREIGN KEY (`fk_lkp_discount_type`) REFERENCES `lkp_discount_type` (`id`),
  CONSTRAINT `main_booking_fk_lkp_location_foreign` FOREIGN KEY (`fk_lkp_location`) REFERENCES `lkp_location` (`id`),
  CONSTRAINT `main_booking_fk_lkp_status_foreign` FOREIGN KEY (`fk_lkp_status`) REFERENCES `lkp_status` (`id`),
  CONSTRAINT `main_booking_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176506 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orchestra_options`
--

DROP TABLE IF EXISTS `orchestra_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orchestra_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orchestra_options_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sap_log`
--

DROP TABLE IF EXISTS `sap_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sap_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `request` text DEFAULT NULL,
  `response` text DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_main_booking` (`fk_main_booking`),
  CONSTRAINT `sap_log_ibfk_1` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_attachment`
--

DROP TABLE IF EXISTS `spa_booking_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_lkp_spa_filename` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `size` varchar(155) DEFAULT NULL,
  `location` varchar(155) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_attachment_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_attachment_fk_lkp_spa_filename_foreign` (`fk_lkp_spa_filename`),
  CONSTRAINT `spa_booking_attachment_fk_lkp_spa_filename_foreign` FOREIGN KEY (`fk_lkp_spa_filename`) REFERENCES `lkp_spa` (`id`),
  CONSTRAINT `spa_booking_attachment_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2067 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_confirm`
--

DROP TABLE IF EXISTS `spa_booking_confirm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_confirm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_spa_location` int(10) unsigned DEFAULT NULL,
  `fk_user` int(10) unsigned DEFAULT NULL,
  `date_booking` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_confirm_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_confirm_fk_spa_location_foreign` (`fk_spa_location`),
  KEY `spa_booking_confirm_fk_user_foreign` (`fk_user`),
  KEY `date_booking` (`date_booking`),
  CONSTRAINT `spa_booking_confirm_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_booking_confirm_fk_spa_location_foreign` FOREIGN KEY (`fk_spa_location`) REFERENCES `spa_location` (`id`),
  CONSTRAINT `spa_booking_confirm_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=774 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_event`
--

DROP TABLE IF EXISTS `spa_booking_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_spa_location` int(10) unsigned DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `other_location` varchar(255) DEFAULT NULL,
  `visitor_amount` int(11) DEFAULT NULL,
  `rank_host` int(11) DEFAULT NULL,
  `other_rank` varchar(155) DEFAULT NULL,
  `enter_date` date DEFAULT NULL,
  `enter_time` time DEFAULT NULL,
  `exit_date` date DEFAULT NULL,
  `exit_time` time DEFAULT NULL,
  `total_day` int(11) DEFAULT NULL,
  `drone` int(11) DEFAULT NULL,
  `vip_name` varchar(155) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `booking_verification` int(11) DEFAULT NULL,
  `event_time_to` time DEFAULT NULL,
  `letter_no_five` text DEFAULT NULL,
  `event_date_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_event_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_event_fk_spa_location_foreign` (`fk_spa_location`),
  CONSTRAINT `spa_booking_event_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_booking_event_fk_spa_location_foreign` FOREIGN KEY (`fk_spa_location`) REFERENCES `spa_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=889 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_organiser`
--

DROP TABLE IF EXISTS `spa_booking_organiser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_organiser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_lkp_state` int(10) unsigned DEFAULT NULL,
  `fk_lkp_country` int(10) unsigned DEFAULT NULL,
  `agency_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(155) DEFAULT NULL,
  `postcode` varchar(5) DEFAULT NULL,
  `telephone_no` varchar(12) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_organiser_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_organiser_fk_lkp_state_foreign` (`fk_lkp_state`),
  KEY `spa_booking_organiser_fk_lkp_country_foreign` (`fk_lkp_country`),
  CONSTRAINT `spa_booking_organiser_fk_lkp_country_foreign` FOREIGN KEY (`fk_lkp_country`) REFERENCES `lkp_country` (`id`),
  CONSTRAINT `spa_booking_organiser_fk_lkp_state_foreign` FOREIGN KEY (`fk_lkp_state`) REFERENCES `lkp_state` (`id`),
  CONSTRAINT `spa_booking_organiser_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_person`
--

DROP TABLE IF EXISTS `spa_booking_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_lkp_state` int(10) unsigned DEFAULT NULL,
  `fk_lkp_country` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(155) DEFAULT NULL,
  `postcode` varchar(5) DEFAULT NULL,
  `ic_no` varchar(12) DEFAULT NULL,
  `hp_no` varchar(12) DEFAULT NULL,
  `office_no` varchar(10) DEFAULT NULL,
  `office_no2` varchar(10) DEFAULT NULL,
  `fax_no` varchar(10) DEFAULT NULL,
  `email` varchar(155) DEFAULT NULL,
  `GST_no` varchar(150) DEFAULT NULL,
  `applicant_status` int(11) DEFAULT NULL,
  `applicant_type` int(11) DEFAULT NULL,
  `contact_person` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_person_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_person_fk_lkp_state_foreign` (`fk_lkp_state`),
  KEY `spa_booking_person_fk_lkp_country_foreign` (`fk_lkp_country`),
  CONSTRAINT `spa_booking_person_fk_lkp_country_foreign` FOREIGN KEY (`fk_lkp_country`) REFERENCES `lkp_country` (`id`),
  CONSTRAINT `spa_booking_person_fk_lkp_state_foreign` FOREIGN KEY (`fk_lkp_state`) REFERENCES `lkp_state` (`id`),
  CONSTRAINT `spa_booking_person_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=890 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_booking_review`
--

DROP TABLE IF EXISTS `spa_booking_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_booking_review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_user` int(10) unsigned DEFAULT NULL,
  `fk_lkp_spa_review` int(10) unsigned DEFAULT NULL,
  `location` varchar(155) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `meeting_chairman` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_booking_review_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_booking_review_fk_user_foreign` (`fk_user`),
  KEY `spa_booking_review_fk_lkp_spa_review_foreign` (`fk_lkp_spa_review`),
  CONSTRAINT `spa_booking_review_fk_lkp_spa_review_foreign` FOREIGN KEY (`fk_lkp_spa_review`) REFERENCES `lkp_spa` (`id`),
  CONSTRAINT `spa_booking_review_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_booking_review_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_location`
--

DROP TABLE IF EXISTS `spa_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `rent_perday` decimal(10,2) DEFAULT NULL,
  `deposit` varchar(155) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remark` varchar(155) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `highlight` text DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT 0,
  `fee_code` varchar(255) DEFAULT NULL,
  `fee_code_penggambaran` varchar(255) DEFAULT NULL,
  `fee_code_sub` varchar(255) DEFAULT NULL,
  `fee_code_sub_penggambaran` varchar(255) DEFAULT NULL,
  `feecode_depo` varchar(255) DEFAULT NULL,
  `feecode_depo_penggambaran` varchar(255) DEFAULT NULL,
  `status_penggambaran` int(11) DEFAULT NULL,
  `category_penggambaran` int(11) DEFAULT NULL,
  `rent_perday_penggambaran` decimal(10,2) DEFAULT NULL,
  `deposit_penggambaran` decimal(10,2) DEFAULT NULL,
  `remark_penggambaran` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_location_fk_users_foreign` (`fk_users`),
  KEY `status` (`status`),
  CONSTRAINT `spa_location_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_location_picture`
--

DROP TABLE IF EXISTS `spa_location_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_location_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_spa_location` int(10) unsigned DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `spa_location_picture_fk_spa_location_foreign` (`fk_spa_location`),
  CONSTRAINT `spa_location_picture_fk_spa_location_foreign` FOREIGN KEY (`fk_spa_location`) REFERENCES `spa_location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_meeting_result`
--

DROP TABLE IF EXISTS `spa_meeting_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_meeting_result` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_user` int(10) unsigned DEFAULT NULL,
  `fk_spa_booking_attachment` int(10) unsigned DEFAULT NULL,
  `date_meeting` date DEFAULT NULL,
  `meeting_no` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `fk_lkp_status_meeting` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_meeting_result_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_meeting_result_fk_user_foreign` (`fk_user`),
  KEY `spa_meeting_result_fk_spa_booking_attachment_foreign` (`fk_spa_booking_attachment`),
  KEY `fk_lkp_status_meeting` (`fk_lkp_status_meeting`),
  KEY `date_meeting` (`date_meeting`),
  CONSTRAINT `spa_meeting_result_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_meeting_result_fk_spa_booking_attachment_foreign` FOREIGN KEY (`fk_spa_booking_attachment`) REFERENCES `spa_booking_attachment` (`id`),
  CONSTRAINT `spa_meeting_result_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_payment`
--

DROP TABLE IF EXISTS `spa_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `fk_users` int(10) unsigned DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `fk_lkp_payment_mode` int(11) DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `application_no` varchar(255) DEFAULT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `booking_location` varchar(255) DEFAULT NULL,
  `booking_title` varchar(255) DEFAULT NULL,
  `booking_start` date DEFAULT NULL,
  `booking_end` date DEFAULT NULL,
  `booking_time` time DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `deposit_amount` decimal(10,2) DEFAULT NULL,
  `paid_amount` decimal(10,2) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `fpx_serial_no` varchar(255) DEFAULT NULL,
  `no_bil` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_payment_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_payment_fk_lkp_payment_type_foreign` (`fk_lkp_payment_type`),
  KEY `spa_payment_fk_users_foreign` (`fk_users`),
  CONSTRAINT `spa_payment_fk_lkp_payment_type_foreign` FOREIGN KEY (`fk_lkp_payment_type`) REFERENCES `lkp_payment_type` (`id`),
  CONSTRAINT `spa_payment_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_payment_fk_users_foreign` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=556 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_payment_detail`
--

DROP TABLE IF EXISTS `spa_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_spa_payment` int(10) unsigned DEFAULT NULL,
  `fk_lkp_payment_type` int(10) unsigned DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fee_code` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_quotation_detail`
--

DROP TABLE IF EXISTS `spa_quotation_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_quotation_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(10) unsigned DEFAULT NULL,
  `fk_user` int(10) unsigned DEFAULT NULL,
  `fk_lkp_spa_payitem` int(10) unsigned DEFAULT NULL,
  `rate` double(10,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `exceptional` int(11) DEFAULT NULL,
  `discount_rm` double(10,2) DEFAULT NULL,
  `discount_percentage` double(10,2) DEFAULT NULL,
  `gst_rm` double(10,2) DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  `total_word` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `spa_quotation_detail_fk_main_booking_foreign` (`fk_main_booking`),
  KEY `spa_quotation_detail_fk_user_foreign` (`fk_user`),
  KEY `spa_quotation_detail_fk_lkp_spa_payitem_foreign` (`fk_lkp_spa_payitem`),
  CONSTRAINT `spa_quotation_detail_fk_lkp_spa_payitem_foreign` FOREIGN KEY (`fk_lkp_spa_payitem`) REFERENCES `lkp_spa` (`id`),
  CONSTRAINT `spa_quotation_detail_fk_main_booking_foreign` FOREIGN KEY (`fk_main_booking`) REFERENCES `main_booking` (`id`),
  CONSTRAINT `spa_quotation_detail_fk_user_foreign` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=825 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spa_sms`
--

DROP TABLE IF EXISTS `spa_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spa_sms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `short_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8451 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_applicant_validation`
--

DROP TABLE IF EXISTS `spp_applicant_validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_applicant_validation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `validation_type` int(11) DEFAULT NULL,
  `applicant_name` varchar(255) DEFAULT NULL,
  `applicant_ic_no` varchar(255) DEFAULT NULL,
  `validation_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_booking_attachement`
--

DROP TABLE IF EXISTS `spp_booking_attachement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_booking_attachement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_lkp_spp_filename` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_client_info`
--

DROP TABLE IF EXISTS `spp_client_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_client_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `postcode` int(11) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `fk_lkp_state` int(11) DEFAULT NULL,
  `fk_lkp_country` int(11) DEFAULT NULL,
  `client_office_no` varchar(255) DEFAULT NULL,
  `client_phone_no` varchar(255) DEFAULT NULL,
  `client_fax_no` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_department_approval`
--

DROP TABLE IF EXISTS `spp_department_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_department_approval` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_lkp_spp_department` int(11) DEFAULT NULL,
  `fk_users` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_filming`
--

DROP TABLE IF EXISTS `spp_filming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_filming` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_lkp_filming_type` int(11) DEFAULT NULL,
  `others_filming` varchar(255) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `no_krew` int(11) DEFAULT NULL,
  `storyline` text DEFAULT NULL,
  `electricity` varchar(255) DEFAULT NULL,
  `water` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_location_picture`
--

DROP TABLE IF EXISTS `spp_location_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_location_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_spa_location` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_meeting`
--

DROP TABLE IF EXISTS `spp_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_meeting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_user` int(11) DEFAULT NULL,
  `fk_lkp_spp_booking_attachment` int(11) DEFAULT NULL,
  `meeting_location` varchar(255) DEFAULT NULL,
  `meeting_date` date DEFAULT NULL,
  `meeting_time` time DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `meeting_chairman` varchar(255) DEFAULT NULL,
  `meeting_no` varchar(255) DEFAULT NULL,
  `meeting_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_payment`
--

DROP TABLE IF EXISTS `spp_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_user` int(11) DEFAULT NULL,
  `fk_lkp_payment_mode` int(11) DEFAULT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `booking_location` varchar(255) DEFAULT NULL,
  `booking_title` varchar(255) DEFAULT NULL,
  `booking_start` date DEFAULT NULL,
  `booking_end` date DEFAULT NULL,
  `booking_time` time DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fpx_serial_no` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `fpx_trans_date` datetime DEFAULT NULL,
  `no_bil` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `deposit_amount` decimal(10,2) DEFAULT NULL,
  `paid_amount` decimal(10,2) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `payment_ref_no` varchar(255) DEFAULT NULL,
  `no_lopo` varchar(255) DEFAULT NULL,
  `special_discount` decimal(10,2) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `cheque_no` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_payment_detail`
--

DROP TABLE IF EXISTS `spp_payment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_payment_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_spp_payment` int(11) DEFAULT NULL,
  `fk_lkp_payment_type` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `fee_code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_props`
--

DROP TABLE IF EXISTS `spp_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_props` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_lkp_spp_filename` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_quotation`
--

DROP TABLE IF EXISTS `spp_quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_quotation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_users` int(11) DEFAULT NULL,
  `quotation_no` varchar(255) DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `quotation_status` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `deposit` decimal(10,2) DEFAULT NULL,
  `exception` int(11) DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_quotation_detail`
--

DROP TABLE IF EXISTS `spp_quotation_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_quotation_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_spp_quotation` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `discount_rm` decimal(10,2) DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT NULL,
  `fk_spp_schedule` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_schedule`
--

DROP TABLE IF EXISTS `spp_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `fk_spa_location` int(11) DEFAULT NULL,
  `date_booking` date DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp_transport`
--

DROP TABLE IF EXISTS `spp_transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp_transport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_main_booking` int(11) DEFAULT NULL,
  `transport_name` varchar(255) DEFAULT NULL,
  `no_of_transport` int(11) DEFAULT NULL,
  `plat_no` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `data` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_meta_user_id_name_unique` (`user_id`,`name`),
  KEY `user_meta_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_users` int(11) DEFAULT NULL,
  `fk_lkp_country` int(11) DEFAULT 146,
  `fk_lkp_state` int(11) DEFAULT 14,
  `bud_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_category` int(11) DEFAULT NULL,
  `bud_type_of_ic` int(11) DEFAULT NULL,
  `bud_reference_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_poscode` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_town` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_phone_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_office_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_email` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_user_indicator` int(11) DEFAULT NULL,
  `bud_fax_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_lkp_location` int(11) DEFAULT NULL,
  `accept_mysms` int(10) DEFAULT NULL,
  `bud_staff_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bud_department` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users` (`fk_users`)
) ENGINE=InnoDB AUTO_INCREMENT=57569 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_role_user_id_role_id_index` (`user_id`,`role_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=58028 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `isAdmin` int(11) NOT NULL DEFAULT 0,
  `fullname` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61929 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_back`
--

DROP TABLE IF EXISTS `users_back`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_back` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `isAdmin` int(11) NOT NULL DEFAULT 0,
  `fullname` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=61894 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-22 15:58:27

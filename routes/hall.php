<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\hall\DashboardController;
use App\Http\Controllers\hall\HallController;
use App\Http\Controllers\hall\LocationController;
use App\Http\Controllers\hall\FacilityController;
use App\Http\Controllers\hall\EquipmentController;
use App\Http\Controllers\hall\PriceFacilityController;
use App\Http\Controllers\hall\PriceEquipmentController;
use App\Http\Controllers\hall\DiscountController;
use App\Http\Controllers\hall\DepositController;
use App\Http\Controllers\hall\InternalController;
use App\Http\Controllers\hall\ExternalController;
use App\Http\Controllers\hall\RescheduleController;
use App\Http\Controllers\hall\ReportController;
use App\Http\Controllers\hall\PdfController;

//Panorama page
Route::get('/panorama', [HallController::class, 'panorama']);

Route::get('/test3hari', [HallController::class, 'test']);

//Hall - Public
Route::get('/', [HallController::class, 'lists']);
Route::post('/', [HallController::class, 'lists']);
Route::get('/details/{id}/{dateStart?}/{dateEnd?}/{capacity?}/{fasility?}', [HallController::class, 'details'])->name('hall.details');
Route::post('/details/{id}/{dateStart?}/{dateEnd?}/{capacity?}/{fasility?}', [HallController::class, 'details']);
Route::get('/reservation/{id}/{dateStart?}/{dateEnd?}/{lkp_location?}/{capacity?}', [HallController::class, 'reservation']);
Route::post('/reservation/{id}/{dateStart?}/{dateEnd?}/{lkp_location?}/{capacity?}', [HallController::class, 'reservation']);
Route::get('/slot/{id}/{date}', [HallController::class, 'slot']);
Route::post('/slotbook/{id}/{date}', [HallController::class, 'slotbook']);
Route::match(['get', 'post'], '/tempahan/{booking}', [HallController::class, 'tempahan']);
Route::get('/tempahan_delete/{booking}/{id}', [HallController::class, 'tempahan_delete']);
Route::get('/rumusan/{booking}', [HallController::class, 'rumusan']);
Route::post('/rumusan/{booking}/{total}/{deposit}', [HallController::class, 'rumusan']);
Route::match(['get', 'post'], '/bayaran/{booking}/{price}', [HallController::class, 'bayaran']);
Route::post('/maklumatbayaran/pemulangandeposit/{id}', [HallController::class, 'pemulangandeposit']);
Route::post('/maklumatbayaran/submitrating', [HallController::class, 'submitrating']);
Route::get('/maklumatbayaran/{booking}/{from?}', [HallController::class, 'maklumatbayaran']);
Route::match(['get', 'post'], '/pemulangandeposit/{booking}', [HallController::class, 'pemulangandeposit']);
Route::get('/my-bookings/{type?}', [HallController::class, 'mybookings']);
Route::get('/generateBooking/{id}/{date?}', [HallController::class, 'generateBooking']);
Route::get('/batal/{id}', [HallController::class, 'batal']);


//Hall - Dashboard
Route::get('/dashboard', [DashboardController::class, 'hall']);
Route::get('/dashboard/top_pengguna', [DashboardController::class, 'top_pengguna']);
Route::get('/dashboard/top_tempahan', [DashboardController::class, 'top_tempahan']);
Route::get('/dashboard/report/{type}', [DashboardController::class, 'report']);
Route::get('/dashboard/report_harian', [DashboardController::class, 'report_harian']);
Route::get('/dashboard/report_keseluruhan', [DashboardController::class, 'report_keseluruhan']);
Route::get('/dashboard/report_bulanan', [DashboardController::class, 'report_bulanan']);
Route::get('/dashboard/report_dalaman', [DashboardController::class, 'report_dalaman']);
Route::get('/dashboard/report_my', [DashboardController::class, 'report_my']);
Route::match(['get', 'post'], '/dashboard/deposit', [DashboardController::class, 'deposit']);
Route::match(['get', 'post'], '/dashboard/depositcancel/{id}', [DashboardController::class, 'depositcancel']);
Route::get('/dashboard/maklumatbayaran/{booking}', [DashboardController::class, 'maklumatbayaran']);
Route::match(['get', 'post'], '/dashboard/maklumatkaunter/{booking}', [DashboardController::class, 'maklumatkaunter']);

Route::match(['get', 'post'], '/dashboard/validation/{booking}', [DashboardController::class, 'pengesahan']);
Route::match(['get', 'post'], '/dashboard/inspection/{booking}', [DashboardController::class, 'pemeriksaan']);


//Hall - Admin - Location
Route::get('/admin/location', [LocationController::class, 'show']);
Route::get('/admin/location/form', [LocationController::class, 'form']);
Route::post('/admin/location/add', [LocationController::class, 'add']);
Route::get('/admin/location/edit/{id}', [LocationController::class, 'edit']);
Route::post('/admin/location/update/{id}', [LocationController::class, 'update']);
Route::get('/admin/location/delete/{id}', [LocationController::class, 'delete']);

//Hall - Admin - Facility
Route::get('/admin/facility', [FacilityController::class, 'show']);
Route::get('/admin/facility/form', [FacilityController::class, 'form']);
Route::post('/admin/facility/add', [FacilityController::class, 'add']);
Route::get('/admin/facility/edit/{id}', [FacilityController::class, 'edit']);
Route::post('/admin/facility/update/{id}', [FacilityController::class, 'update']);
Route::get('/admin/facility/delete/{id}', [FacilityController::class, 'delete']);
Route::get('/admin/facility/image/{id}', [FacilityController::class, 'imageshow']);
Route::get('/admin/facility/image/form/{id}', [FacilityController::class, 'imageform']);
Route::post('/admin/facility/image/add/{id}', [FacilityController::class, 'imageadd']);
Route::get('/admin/facility/image/delete/{id}/{file}', [FacilityController::class, 'imagedelete']);

//Hall - Admin - Equipment
Route::get('/admin/equipment', [EquipmentController::class, 'show']);
Route::get('/admin/equipment/form', [EquipmentController::class, 'form']);
Route::post('/admin/equipment/add', [EquipmentController::class, 'add']);
Route::get('/admin/equipment/edit/{id}', [EquipmentController::class, 'edit']);
Route::post('/admin/equipment/update/{id}', [EquipmentController::class, 'update']);
Route::get('/admin/equipment/delete/{id}', [EquipmentController::class, 'delete']);

//Hall - Admin - Price - Facility
Route::get('/admin/facilityprice', [PriceFacilityController::class, 'show']);
Route::get('/admin/facilityprice/form', [PriceFacilityController::class, 'form']);
Route::post('/admin/facilityprice/add', [PriceFacilityController::class, 'add']);
Route::get('/admin/facilityprice/edit/{id}', [PriceFacilityController::class, 'edit']);
Route::post('/admin/facilityprice/update/{id}', [PriceFacilityController::class, 'update']);
Route::get('/admin/facilityprice/delete/{id}', [PriceFacilityController::class, 'delete']);

Route::get('/resitonline/{id}/{equipment}', [PDFController::class, 'resitonline']);

//Hall - Admin - Price - Equuipment
Route::get('/admin/equipmentprice', [PriceEquipmentController::class, 'show']);
Route::get('/admin/equipmentprice/form', [PriceEquipmentController::class, 'form']);
Route::post('/admin/equipmentprice/add', [PriceEquipmentController::class, 'add']);
Route::get('/admin/equipmentprice/edit/{id}', [PriceEquipmentController::class, 'edit']);
Route::post('/admin/equipmentprice/update/{id}', [PriceEquipmentController::class, 'update']);
Route::get('/admin/equipmentprice/delete/{id}', [PriceEquipmentController::class, 'delete']);

//Hall - Admin - Price - Discount
Route::get('/admin/discount', [DiscountController::class, 'show']);
Route::get('/admin/discount/form', [DiscountController::class, 'form']);
Route::post('/admin/discount/add', [DiscountController::class, 'add']);
Route::get('/admin/discount/edit/{id}', [DiscountController::class, 'edit']);
Route::post('/admin/discount/update/{id}', [DiscountController::class, 'update']);
Route::get('/admin/discount/delete/{id}', [DiscountController::class, 'delete']);

//Hall - Admin - Price - Deposit
Route::get('/admin/deposit', [DepositController::class, 'show']);
Route::get('/admin/deposit/form', [DepositController::class, 'form']);
Route::post('/admin/deposit/add', [DepositController::class, 'add']);
Route::get('/admin/deposit/edit/{id}', [DepositController::class, 'edit']);
Route::post('/admin/deposit/update/{id}', [DepositController::class, 'update']);
Route::get('/admin/deposit/delete/{id}', [DepositController::class, 'delete']);

//Hall - Internal
Route::get('/internal', [InternalController::class, 'index']);
Route::post('/internal', [InternalController::class, 'index']);
Route::get('/internal/book', [InternalController::class, 'book']);
Route::post('/internal/book', [InternalController::class, 'book']);
Route::get('/internal/slot/{id}/{date}', [InternalController::class, 'slot']);
Route::post('/internal/slot/{id}/{date}', [InternalController::class, 'slot']);
Route::post('/internal/slotbook/{id}/{date}', [InternalController::class, 'slotbook']);
Route::match(['get', 'post'], '/internal/tempahan/{booking}', [InternalController::class, 'tempahan']);
Route::get('/internal/tempahan_delete/{booking}/{id}', [InternalController::class, 'tempahan_delete']);
Route::get('/internal/rumusan/{booking}', [InternalController::class, 'rumusan']);
Route::post('/internal/bayaran/{booking}', [InternalController::class, 'bayaran']);

//Hall - External
Route::get('/external', [ExternalController::class, 'index']);
Route::post('/external', [ExternalController::class, 'index']);
Route::get('/external/hall/{id}', [ExternalController::class, 'hall']);
Route::get('/external/sport/{id}', [ExternalController::class, 'sport']);
Route::post('/external/hall/{id}', [ExternalController::class, 'hall']);
Route::post('/external/sport/{id}', [ExternalController::class, 'sport']);
Route::get('/external/user', [ExternalController::class, 'user']);
Route::post('/external/user', [ExternalController::class, 'user']);
Route::get('/external/book/{userId}', [ExternalController::class, 'book']);
Route::post('/external/book/{userId}', [ExternalController::class, 'book']);
Route::get('/external/slot/{id}/{date}/{user}', [ExternalController::class, 'slot']);
Route::post('/external/slot/{id}/{date}/{user}', [ExternalController::class, 'slot']);
Route::post('/external/slotbook/{id}/{date}/{user}', [ExternalController::class, 'slotbook']);
Route::get('/external/tempahan/{booking}', [ExternalController::class, 'tempahan']);
Route::get('/external/tempahan_delete/{booking}/{id}', [ExternalController::class, 'tempahan_delete']);
Route::get('/external/rumusan/{booking}', [ExternalController::class, 'rumusan']);
Route::post('/external/bayaran/{booking}', [ExternalController::class, 'bayaran']);
Route::post('/external/submitbayaran/{booking}', [ExternalController::class, 'submit_bayaran']);
Route::get('/external/equipment/{id}', [ExternalController::class, 'equipment']);

//Hall - Reschedule
Route::get('/reschedule', [RescheduleController::class, 'index']);
Route::post('/reschedule', [RescheduleController::class, 'index']);
Route::get('/reschedule/penjadualan_semula/{booking}', [RescheduleController::class, 'penjadualanSemula']);
Route::post('/reschedule/date/submit/{id}', [RescheduleController::class, 'updateDate']);

//Hall - Report
Route::get('/report', [ReportController::class, 'index']);
Route::post('/report', [ReportController::class, 'index']);
Route::match(['get', 'post'], '/report/tempahan', [ReportController::class, 'tempahan']);
Route::match(['get', 'post'], '/report/deposit', [ReportController::class, 'deposit']);
Route::get('/report/pelanggan', [ReportController::class, 'pelanggan']);
Route::post('/report/pelanggan', [ReportController::class, 'pelanggan']);
Route::get('/report/hasil', [ReportController::class, 'hasil']);
Route::post('/report/hasil', [ReportController::class, 'hasil']);
Route::get('/report/harian', [ReportController::class, 'harian']);
Route::post('/report/harian', [ReportController::class, 'harian']);
Route::get('/report/bayaran', [ReportController::class, 'bayaran']);
Route::post('/report/bayaran', [ReportController::class, 'bayaran']);
Route::get('/report/pemulangan', [ReportController::class, 'pemulangan']);
Route::post('/report/pemulangan', [ReportController::class, 'pemulangan']);
Route::get('/report/auditlog', [ReportController::class, 'audit']);
Route::post('/report/auditlog', [ReportController::class, 'audit']);

Route::get('/eksportpdfmaklumattempahan/{location}/{start}/{end}', [PDFController::class, 'maklumattempahanpdf']);
Route::get('/eksportpdfmaklumatpelanggan/{location}/{start}/{end}', [PDFController::class, 'maklumatpelangganpdf']);
Route::get('/eksportpdfhasil/{location}/{payment}/{start}/{end}', [PDFController::class, 'hasilpdf']);
Route::get('/eksportpdfkutipanharian/{location}/{tarikh}/{typebayaran}', [PDFController::class, 'kutipanharianpdf']);
Route::get('/eksportpdfdeposit/{location}/{payment}/{type}/{start}/{end}', [PDFController::class, 'depositpdf']);
Route::get('/eksportpdfaging/{location}/{payment}/{start}/{end}', [PDFController::class, 'agingpdf']);


//Hall - Audit
Route::match(['get', 'post'], '/report/penggunaan/{id?}', [ReportController::class, 'penggunaan']);
Route::match(['get', 'post'], '/report/auditlog', [ReportController::class, 'audit']);

//Hall - Cancel
Route::get('/cancel', [HallController::class, 'cancel']);
Route::post('/cancel', [HallController::class, 'cancel']);
Route::post('/cancel/submit/{id}', [HallController::class, 'submitcancel']);

//Hall - Duplicate
Route::get('/duplicate', [HallController::class, 'duplicate']);
Route::post('/duplicate', [HallController::class, 'duplicate']);

//Hall - Available
Route::get('/available', [HallController::class, 'available']);
Route::post('/available', [HallController::class, 'available']);

//Hall - Ajax
Route::post('/ajax', [HallController::class, 'ajax']);

//Hall - PDF
Route::get('/borangbpsk/{id}', [PDFController::class, 'index']);
Route::get('/janasebutharga/{id}', [PDFController::class, 'janaSebutHarga']);

//Hall - Refund
Route::get('/refund', [HallController::class, 'refund']);
Route::post('/refund', [HallController::class, 'refund']);
Route::post('/refund/submit/{id}', [HallController::class, 'submitrefund']);
Route::get('/refund/maklumatbayaran/{booking}/{from?}', [HallController::class, 'refundinfo']);

Route::get('/dashboard/removefile/{id}', [DashboardController::class, 'removefile']);
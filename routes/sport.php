<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\sport\LocationController;
use App\Http\Controllers\sport\AnnouncementController;
use App\Http\Controllers\sport\FacilityController;
use App\Http\Controllers\sport\SportController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\sport\TypeFacilityController;
use App\Http\Controllers\sport\DetailsFacilityController;
use App\Http\Controllers\sport\SlotController;
use App\Http\Controllers\sport\EquipmentController;
use App\Http\Controllers\sport\TypeFunctionController;
use App\Http\Controllers\sport\FunctionController;
use App\Http\Controllers\sport\PriceFacilityController;
use App\Http\Controllers\sport\PriceEquipmentController;
use App\Http\Controllers\sport\TypePackageController;
use App\Http\Controllers\sport\PackageController;
use App\Http\Controllers\sport\ClosedController;
use App\Http\Controllers\sport\PolicyController;
use App\Http\Controllers\sport\InternalController;
use App\Http\Controllers\sport\ExternalController;
use App\Http\Controllers\sport\RescheduleController;
use App\Http\Controllers\sport\ReportController;
use App\Http\Controllers\sport\PdfController;

//Sport - Public
Route::get('/', [SportController::class, 'lists']);
Route::post('/', [SportController::class, 'lists']);
Route::match(['get', 'post'], '/my-bookings/{type?}', [SportController::class, 'mybookings']);

Route::get('/details/{id}/{date?}/{fasility?}', [SportController::class, 'details']);
Route::post('/details/{id}/{date?}/{fasility?}', [SportController::class, 'details']);

Route::middleware('authv2')->group(function () {

Route::get('/slot/{id}/{date}', [SportController::class, 'slot']);
Route::get('/slotbook/{id}/{date}', [SportController::class, 'slotbook']);
Route::post('/slotbook/{id}/{date}', [SportController::class, 'slotbook']);
Route::get('/tempahan/{booking}', [SportController::class, 'tempahan']);
Route::get('/tempahan_delete/{booking}/{id}/{esb}', [SportController::class, 'tempahan_delete']);
Route::get('/rumusan/{booking}', [SportController::class, 'rumusan']);
Route::post('/bayaran/{booking}', [SportController::class, 'bayaran']);
Route::get('/maklumatbayaran/{booking}/{type?}', [SportController::class, 'maklumatbayaran']);
Route::match(['get', 'post'], '/booking/validation/{booking}', [SportController::class, 'validation']);
Route::match(['get', 'post'], '/booking/hallpayment/{booking}', [SportController::class, 'hallpayment']);
Route::match(['get', 'post'], '/booking/pelarasan/{booking}', [SportController::class, 'pelarasan']);
Route::get('/status', [SportController::class, 'status']);
Route::post('/ratingtempahan/{id}', [SportController::class, 'ratingtempahan']);

Route::get('/test_slot', [SportController::class, 'test_slot']);
Route::get('/tempahanForm/{id}/{date}', [SportController::class, 'pdgsintetikForm']);
Route::match(['get', 'post'], '/dashboard/validation/{booking}', [DashboardController::class, 'pengesahan']);

//Sport - Dashboard
Route::get('/dashboard', [DashboardController::class, 'sport']);
Route::get('/dashboard/top_pengguna', [DashboardController::class, 'top_pengguna']);
Route::get('/dashboard/top_tempahan', [DashboardController::class, 'top_tempahan']);
Route::match(['get', 'post'], '/dashboard/report_keseluruhan', [DashboardController::class, 'report_keseluruhan']);
Route::match(['get', 'post'], '/dashboard/search', [DashboardController::class, 'search']);
Route::get('/dashboard/report_dalaman', [DashboardController::class, 'report_dalaman']);
Route::get('/dashboard/report_bayaran', [DashboardController::class, 'report_bayaran']);
Route::get('/dashboard/report_kelulusan', [DashboardController::class, 'report_kelulusan']);
Route::get('/dashboard/report_harian', [DashboardController::class, 'report_harian']);
Route::get('/dashboard/report_tindih', [DashboardController::class, 'report_tindih']);
Route::get('/dashboard/report_my', [DashboardController::class, 'report_my']);
Route::match(['get', 'post'], '/dashboard/approval/{id}', [DashboardController::class, 'approval']);
Route::get('/dashboard/removefile/{id}', [DashboardController::class, 'removefile']);

//Sport - Admin - Location
Route::get('/admin/location', [LocationController::class, 'show']);
Route::get('/admin/location/form', [LocationController::class, 'form']);
Route::post('/admin/location/add', [LocationController::class, 'add']);
Route::get('/admin/location/edit/{id}', [LocationController::class, 'edit']);
Route::post('/admin/location/update/{id}', [LocationController::class, 'update']);
Route::get('/admin/location/delete/{id}', [LocationController::class, 'delete']);

//Sport - Admin - Announcement
Route::get('/admin/announcement', [AnnouncementController::class, 'showAnnouncement']);

//Sport - Admin - Facility
Route::get('/admin/facility', [FacilityController::class, 'show']);
Route::get('/admin/facility/form', [FacilityController::class, 'form']);
Route::post('/admin/facility/add', [FacilityController::class, 'add']);
Route::get('/admin/facility/edit/{id}', [FacilityController::class, 'edit']);
Route::post('/admin/facility/update/{id}', [FacilityController::class, 'update']);
Route::get('/admin/facility/delete/{id}', [FacilityController::class, 'delete']);
Route::get('/admin/facility/image/{id}', [FacilityController::class, 'imageshow']);
Route::get('/admin/facility/image/form/{id}', [FacilityController::class, 'imageform']);
Route::post('/admin/facility/image/add/{id}', [FacilityController::class, 'imageadd']);
Route::get('/admin/facility/image/delete/{id}/{file}', [FacilityController::class, 'imagedelete']);

//Sport - Admin - Type - Facility
Route::get('/admin/facilitytype', [TypeFacilityController::class, 'show']);
Route::get('/admin/facilitytype/form', [TypeFacilityController::class, 'form']);
Route::post('/admin/facilitytype/add', [TypeFacilityController::class, 'add']);
Route::get('/admin/facilitytype/edit/{id}', [TypeFacilityController::class, 'edit']);
Route::post('/admin/facilitytype/update/{id}', [TypeFacilityController::class, 'update']);
Route::get('/admin/facilitytype/delete/{id}', [TypeFacilityController::class, 'delete']);

//Sport - Admin - Details - Facility
Route::get('/admin/facilitydetail', [DetailsFacilityController::class, 'show']);
Route::get('/admin/facilitydetail/form', [DetailsFacilityController::class, 'form']);
Route::post('/admin/facilitydetail/add', [DetailsFacilityController::class, 'add']);
Route::get('/admin/facilitydetail/edit/{id}', [DetailsFacilityController::class, 'edit']);
Route::post('/admin/facilitydetail/update/{id}', [DetailsFacilityController::class, 'update']);
Route::get('/admin/facilitydetail/delete/{id}', [DetailsFacilityController::class, 'delete']);

//Sport - Admin - Slot
Route::get('/admin/slot', [SlotController::class, 'show']);
Route::get('/admin/slot/form', [SlotController::class, 'form']);
Route::post('/admin/slot/add', [SlotController::class, 'add']);
Route::get('/admin/slot/edit/{id}', [SlotController::class, 'edit']);
Route::post('/admin/slot/update/{id}', [SlotController::class, 'update']);
Route::get('/admin/slot/delete/{id}', [SlotController::class, 'delete']);

//Sport - Admin - Equipment
Route::get('/admin/equipment', [EquipmentController::class, 'show']);
Route::get('/admin/equipment/form', [EquipmentController::class, 'form']);
Route::post('/admin/equipment/add', [EquipmentController::class, 'add']);
Route::get('/admin/equipment/edit/{id}', [EquipmentController::class, 'edit']);
Route::post('/admin/equipment/update/{id}', [EquipmentController::class, 'update']);
Route::get('/admin/equipment/delete/{id}', [EquipmentController::class, 'delete']);

//Sport - Admin - Type - Function
Route::get('/admin/functiontype', [TypeFunctionController::class, 'show']);
Route::get('/admin/functiontype/form', [TypeFunctionController::class, 'form']);
Route::post('/admin/functiontype/add', [TypeFunctionController::class, 'add']);
Route::get('/admin/functiontype/edit/{id}', [TypeFunctionController::class, 'edit']);
Route::post('/admin/functiontype/update/{id}', [TypeFunctionController::class, 'update']);
Route::get('/admin/functiontype/delete/{id}', [TypeFunctionController::class, 'delete']);

//Sport - Admin - Function
Route::get('/admin/function', [FunctionController::class, 'show']);
Route::get('/admin/function/form', [FunctionController::class, 'form']);
Route::post('/admin/function/add', [FunctionController::class, 'add']);
Route::get('/admin/function/edit/{id}', [FunctionController::class, 'edit']);
Route::post('/admin/function/update/{id}', [FunctionController::class, 'update']);
Route::get('/admin/function/delete/{id}', [FunctionController::class, 'delete']);

//Sport - Admin - Price - Facility
Route::get('/admin/facilityprice', [PriceFacilityController::class, 'show']);
Route::get('/admin/facilityprice/form', [PriceFacilityController::class, 'form']);
Route::post('/admin/facilityprice/add', [PriceFacilityController::class, 'add']);
Route::get('/admin/facilityprice/edit/{id}', [PriceFacilityController::class, 'edit']);
Route::post('/admin/facilityprice/update/{id}', [PriceFacilityController::class, 'update']);
Route::get('/admin/facilityprice/delete/{id}', [PriceFacilityController::class, 'delete']);

//Sport - Admin - Price - Equuipment
Route::get('/admin/equipmentprice', [PriceEquipmentController::class, 'show']);
Route::get('/admin/equipmentprice/form', [PriceEquipmentController::class, 'form']);
Route::post('/admin/equipmentprice/add', [PriceEquipmentController::class, 'add']);
Route::get('/admin/equipmentprice/edit/{id}', [PriceEquipmentController::class, 'edit']);
Route::post('/admin/equipmentprice/update/{id}', [PriceEquipmentController::class, 'update']);
Route::get('/admin/equipmentprice/delete/{id}', [PriceEquipmentController::class, 'delete']);

//Sport - Admin - Type - Package
Route::get('/admin/packagetype', [TypePackageController::class, 'show']);
Route::get('/admin/packagetype/form', [TypePackageController::class, 'form']);
Route::post('/admin/packagetype/add', [TypePackageController::class, 'add']);
Route::get('/admin/packagetype/edit/{id}', [TypePackageController::class, 'edit']);
Route::post('/admin/packagetype/update/{id}', [TypePackageController::class, 'update']);
Route::get('/admin/packagetype/delete/{id}', [TypePackageController::class, 'delete']);

//Sport - Admin - Package
Route::get('/admin/package', [PackageController::class, 'show']);
Route::get('/admin/package/form', [PackageController::class, 'form']);
Route::post('/admin/package/add', [PackageController::class, 'add']);
Route::get('/admin/package/edit/{id}', [PackageController::class, 'edit']);
Route::post('/admin/package/update/{id}', [PackageController::class, 'update']);
Route::get('/admin/package/delete/{id}', [PackageController::class, 'delete']);

//Sport - Admin - Closed
Route::get('/admin/closed', [ClosedController::class, 'show']);
Route::get('/admin/closed/form', [ClosedController::class, 'form']);
Route::post('/admin/closed/add', [ClosedController::class, 'add']);
Route::get('/admin/closed/edit/{id}', [ClosedController::class, 'edit']);
Route::post('/admin/closed/update/{id}', [ClosedController::class, 'update']);
Route::get('/admin/closed/delete/{id}', [ClosedController::class, 'delete']);

//Sport - Admin - Policy
Route::get('/admin/policy', [PolicyController::class, 'show']);
Route::get('/admin/policy/form', [PolicyController::class, 'form']);
Route::post('/admin/policy/add', [PolicyController::class, 'add']);
Route::get('/admin/policy/edit/{id}/{type}', [PolicyController::class, 'edit']);
Route::post('/admin/policy/update/{id}/{type}', [PolicyController::class, 'update']);
Route::get('/admin/policy/delete/{id}', [PolicyController::class, 'delete']);

//Sport - Admin - Policy
Route::get('/admin/tax', [TaxController::class, 'show']);
Route::get('/admin/tax/form', [TaxController::class, 'form']);
Route::get('/admin/tax/edit/{id}', [TaxController::class, 'edit']);
Route::post('/admin/tax/update/{id}', [TaxController::class, 'update']);

//Sport - Internal
Route::get('/internal', [InternalController::class, 'index']);
Route::post('/internal', [InternalController::class, 'index']);
Route::match(['get', 'post'], '/internal/purpose/{id}/{date?}', [InternalController::class, 'purpose']);
// Route::match(['get', 'post'], '/internal/purpose/{id}/{loc}/{sdate}/{edate}', [InternalController::class, 'purpose']);
Route::match(['get', 'post'], '/internal/slot/{bookingid}/{hallid?}/{funcid?}', [InternalController::class, 'slotDewan']);
Route::match(['get', 'post'], '/internal/slotMasa/{bookingid}', [InternalController::class, 'slotMasaDewan']);
Route::match(['get', 'post'], '/internal/rumusandewan/{booking}', [InternalController::class, 'rumusandewan']);
Route::match(['get'], '/internal/delete/slotdewan/{mbid}/{id}', [InternalController::class, 'deleteSlotDewan']);
Route::match(['get', 'post'], '/internal/tambahkegunaandewan/{id}', [InternalController::class, 'tambah_kegunaan_dewan']);

Route::match(['get', 'post'], '/internal/tambahkegunaandewan/{id}', [InternalController::class, 'tambah_kegunaan_dewan']);
Route::match(['get', 'post'], '/internal/slotuse/{id}/{ebfid}', [InternalController::class, 'slotDewan_kegunaan']);
Route::match(['get', 'post'], '/internal/slotMasause/{bookingid}/{ebfid}', [InternalController::class, 'slotMasaDewan_kegunaan']);

Route::post('/internal/slotbook/{id}/{date}/{type}', [InternalController::class, 'slotbook']);
Route::get('/internal/tempahan/{booking}', [InternalController::class, 'tempahan']);
Route::match(['get', 'post'], '/internal/tambahkegunaan/{id}', [InternalController::class, 'tambah_kegunaan']);
Route::get('/internal/tempahan_delete/{booking}/{id}/{esbid}', [InternalController::class, 'tempahan_delete']);
Route::match(['get', 'post'], '/internal/rumusan/{booking}', [InternalController::class, 'rumusan']);
Route::post('/internal/bayaran/{booking}', [InternalController::class, 'bayaran']);


//Sport - External
Route::get('/external', [ExternalController::class, 'index']);
Route::post('/external', [ExternalController::class, 'index']);
Route::match(['get', 'post'], '/external/hall/{id}', [ExternalController::class, 'hall']);
Route::match(['get', 'post'], '/external/hallpurpose/{id}/{date}/{userId}', [ExternalController::class, 'purpose']);
Route::match(['get', 'post'], '/external/slot/{id}', [ExternalController::class, 'slotDewan']);
Route::match(['get', 'post'], '/external/slotMasa/{bookingid}', [ExternalController::class, 'slotMasaDewan']);
Route::match(['get', 'post'], '/external/recalculate_booking/{booking}', [ExternalController::class, 'recalculate_dewan']);
Route::match(['get', 'post'], '/external/rumusandewan/{booking}', [ExternalController::class, 'rumusandewan']);
Route::match(['get'], '/external/delete/slotdewan/{mbid}/{id}', [ExternalController::class, 'deleteSlotDewan']);

Route::match(['get', 'post'], '/external/tambahkegunaandewan/{id}', [ExternalController::class, 'tambah_kegunaan_dewan']);
Route::match(['get', 'post'], '/external/slotuse/{id}/{ebfid}', [ExternalController::class, 'slotDewan_kegunaan']);
Route::match(['get', 'post'], '/external/slotMasause/{bookingid}/{ebfid}', [ExternalController::class, 'slotMasaDewan_kegunaan']);


Route::get('/external/sport/{id}', [ExternalController::class, 'sport']);
Route::post('/external/sport/{id}', [ExternalController::class, 'sport']);
Route::post('/external/slotbook/{id}/{date}/{user}', [ExternalController::class, 'sport_slotbook']);
Route::get('/external/tempahan/{booking}', [ExternalController::class, 'tempahan']);
Route::match(['get', 'post'], '/external/tambahkegunaan/{id}', [ExternalController::class, 'tambah_kegunaan']);
Route::get('/external/tempahan_delete/{booking}/{id}/{esbid}', [ExternalController::class, 'tempahan_delete']);
Route::get('/external/rumusan/{booking}', [ExternalController::class, 'rumusan']);
Route::post('/external/bayaran/{booking}', [ExternalController::class, 'bayaran']);
Route::post('/external/submitbayaran/{booking}', [ExternalController::class, 'submit_bayaran']);
Route::match(['get', 'post'], '/external/equipment/{id}', [ExternalController::class, 'equipment']);
Route::match(['get', 'post'], '/external/equipmentSubmit/{id}/{userid}', [ExternalController::class, 'equipmentSubmit']);
Route::match(['get', 'post'], '/external/equipmentSlot/{id}', [ExternalController::class, 'equipmentSlot']);
Route::match(['get', 'post'], '/external/equipmentsummary/{id}', [ExternalController::class, 'equipmentSummary']);
Route::match(['get', 'post'], '/external/equipmentdelete/{id}/{eqid}', [ExternalController::class, 'equipmentDelete']);
Route::match(['get', 'post'], '/external/equipmentadd/{id}', [ExternalController::class, 'equipmentAdd']);


//Sport - Reschedule
Route::get('/reschedule', [RescheduleController::class, 'index']);
Route::post('/reschedule', [RescheduleController::class, 'index']);
Route::get('/reschedule/sport/{id}', [RescheduleController::class, 'sport']);
Route::get('reschedule/delete/slot/{id}/{esbid}/{mbid}', [RescheduleController::class, 'delete']);
Route::match(['get', 'post'], '/reschedule/slot/{type}/{id}/{date}/{booking}/{estid}', [RescheduleController::class, 'slot']);
Route::post('/reschedule/slotbook/{id}/{date}/{booking}/{estid}', [RescheduleController::class, 'slotbook']);
Route::match(['get', 'post'], '/reschedule/slothall/{type}/{id}/{date}/{booking}/{estid}', [RescheduleController::class, 'slothall']);
Route::post('/reschedule/slotbookhall/{id}/{date}/{booking}/{estid}', [RescheduleController::class, 'slotbookhall']);

//Sport - Report
Route::get('/report', [ReportController::class, 'index']);
Route::post('/report', [ReportController::class, 'index']);
Route::get('/report/pelanggan', [ReportController::class, 'pelanggan']);
Route::post('/report/pelanggan', [ReportController::class, 'pelanggan']);
Route::get('/report/hasil', [ReportController::class, 'hasil']);
Route::post('/report/hasil', [ReportController::class, 'hasil']);
Route::get('/report/bayarandepo', [ReportController::class, 'bayarandepo']);
Route::post('/report/bayarandepo', [ReportController::class, 'bayarandepo']);
Route::get('/report/aging', [ReportController::class, 'aging']);
Route::post('/report/aging', [ReportController::class, 'aging']);
Route::get('/report/refund', [ReportController::class, 'refund']);
Route::post('/report/refund', [ReportController::class, 'refund']);
Route::get('/report/harian', [ReportController::class, 'harian']);
Route::post('/report/harian', [ReportController::class, 'harian']);
Route::get('/report/usage', [ReportController::class, 'usage']);
Route::post('/report/usage', [ReportController::class, 'usage']);

//Sport - Audit
Route::match(['get', 'post'], '/report/penggunaan/{id?}', [ReportController::class, 'penggunaan']);
Route::match(['get', 'post'], '/report/auditlog', [ReportController::class, 'audit']);

//Sport - Cancel
Route::get('/cancel', [SportController::class, 'cancel']);
Route::post('/cancel', [SportController::class, 'cancel']);
Route::get('/cancel/submit/{id}', [SportController::class, 'submitcancelget']);
Route::post('/cancel/submit/{id}', [SportController::class, 'submitcancel']);

//Sport - Duplicate
Route::get('/duplicate', [SportController::class, 'duplicate']);
Route::post('/duplicate', [SportController::class, 'duplicate']);

//Sport - Equipment addition
Route::match(['get', 'post'], '/equipment', [SportController::class, 'equipment']);
Route::match(['get', 'post'], '/equipment_edit/{mbid}/{eebid?}', [SportController::class, 'equipment_edit']);
Route::match(['get', 'post'], '/equipment_summary/{mbid}/{eebid?}', [SportController::class, 'equipment_summary']);
Route::match(['get', 'post'], '/equipment_payment/{mbid}', [SportController::class, 'equipment_payment']);
Route::match(['get', 'post'], '/equipment_delete/{eebid}', [SportController::class, 'equipment_delete']);

//Sport - Available
Route::get('/available', [SportController::class, 'available']);
Route::post('/available', [SportController::class, 'available']);

//Sport - Ajax
Route::post('/ajax', [SportController::class, 'ajax']);
Route::post('/ajaxpublic', [SportController::class, 'ajaxpublic']);
Route::post('/ajaxhall', [SportController::class, 'ajaxhall']);
Route::post('/validateSportSlot', [SportController::class, 'validateSportSlot']);
Route::post('/ajaxPaymentStatus', [SportController::class, 'ajaxPaymentStatus']);
Route::get('/ajax/bannerStatus', [SportController::class, 'bannerStatus']);
Route::post('/ajax/getSportTimeSlot', [SportController::class, 'slotMasaSukan']);

//Sport - PDF
Route::get('/resitonline/{id}/{equipment}', [PDFController::class, 'index']);
Route::get('/janasebutharga/{id}', [PDFController::class, 'janaSebutHarga']);
Route::get('/janabpsk/{id}', [PDFController::class, 'janabpsk']);
Route::get('/eksportpdfkutipanharian/{location}/{tarikh}/{typebayaran}', [PDFController::class, 'kutipanharianpdf']);
// Route::get('/eksportpdfpenggunaan/{location}/{usage_type}/{search_type}/{start}/{end}/{year}', [PDFController::class, 'penggunaanpdf']);


Route::get('/eksportpdfmaklumattempahan/{location}/{type}/{start}/{end}', [PDFController::class, 'maklumattempahanpdf']);
Route::get('/eksportpdfmaklumatpelanggan/{location}/{start}/{end}', [PDFController::class, 'maklumatpelangganpdf']);
Route::get('/eksportpdfhasil/{location}/{payment}/{start}/{end}', [PDFController::class, 'hasilpdf']);
Route::get('/eksportpdfdeposit/{location}/{payment}/{type}/{start}/{end}', [PDFController::class, 'depositpdf']);
Route::get('/eksportpdfaging/{location}/{payment}/{start}/{end}', [PDFController::class, 'agingpdf']);

});
// Route::get('/testfpxuat', [SportController::class, 'testfpxuat']);
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\event\DashboardController;
use App\Http\Controllers\event\EventController;
use App\Http\Controllers\event\LocationController;
use App\Http\Controllers\event\FacilityController;
use App\Http\Controllers\event\EquipmentController;
use App\Http\Controllers\event\PriceFacilityController;
use App\Http\Controllers\event\PriceEquipmentController;
use App\Http\Controllers\event\DiscountController;
use App\Http\Controllers\event\DepositController;
use App\Http\Controllers\event\ReportController;
use App\Http\Controllers\event\ExternalController;
use App\Http\Controllers\event\RescheduleController;
use App\Http\Controllers\event\InternalController;
use App\Http\Controllers\event\PdfController;

//Event - Public
Route::match(['get', 'post'], '/reservation/{id}/{tarikhMula?}/{tarikhAkhir?}', [EventController::class, 'reservation']);
Route::get('/details/{id}/{tarikhMula?}/{tarikhAkhir?}', [EventController::class, 'details'])->name('event.details');
Route::post('/details/{id}/{tarikhMula?}/{tarikhAkhir?}', [EventController::class, 'details']);
Route::get('/summary/{id}/{bookingId}', [EventController::class, 'summary'])->name('event.summary');
Route::get('/slot/{id}/{date}', [EventController::class, 'slot']);
Route::post('/slotbook/{id}/{date}', [EventController::class, 'slotbook']);
Route::match(['get', 'post'], '/my-bookings/{jenis?}', [EventController::class, 'mybookings']);
Route::match(['get', 'post'], '/booking/{jenis?}', [EventController::class, 'tempahan']);
// Route::get('/tempahan/{booking}', [EventController::class, 'maklumattempahan']);
Route::get('/tempahan/delete/{id}', [EventController::class, 'tempahan_delete']);
Route::get('/rumusan/{booking}', [EventController::class, 'rumusan']);
Route::get('/bayaran/{booking}', [DashboardController::class, 'bayaran'])->name('bayaran');
Route::get('/bayaran/acara/{booking}/{jumlah}', [EventController::class, 'bayaran'])->name('bayaran');
Route::get('/maklumatbayaran/{booking}', [EventController::class, 'maklumatbayaran']);

Route::post('/carianSekatan', [EventController::class, 'carianSekatan']);

Route::get('/status', [EventController::class, 'status']);
Route::get('/postpayment/{id}/{receipt}/{status}', [EventController::class, 'postPayment']);

Route::middleware(['authv2'])->group(function () {
    //Event - Dashboard
    Route::get('/dashboard', [DashboardController::class, 'event']);
    Route::get('/dashboard/{permohonan?}', [DashboardController::class, 'event_info']);
    Route::get('/dashboard/reservation/{id}', [DashboardController::class, 'reservation'])->name('reservationdash.get');
    Route::post('/dashboard/reservation/{id}', [DashboardController::class, 'reservation'])->name('reservationdash.post');
    Route::match(['get', 'post'], '/dashboard/updatePayment/{id}', [DashboardController::class, 'updatePayment']);
    Route::get('/cancelbooking/{id}', [EventController::class, 'cancelbooking']);
    // Route::get('/dashboard/top_pengguna', [DashboardController::class, 'top_pengguna']);
    // Route::get('/dashboard/top_tempahan', [DashboardController::class, 'top_tempahan']);
    // Route::get('/dashboard/report_keseluruhan', [DashboardController::class, 'report_keseluruhan']);
    // Route::get('/dashboard/report_dalaman', [DashboardController::class, 'report_dalaman']);
    // Route::get('/dashboard/report_bayaran', [DashboardController::class, 'report_bayaran']);
    // Route::get('/dashboard/report_kelulusan', [DashboardController::class, 'report_kelulusan']);
    // Route::get('/dashboard/report_harian', [DashboardController::class, 'report_harian']);
    // Route::get('/dashboard/report_tindih', [DashboardController::class, 'report_tindih']);
    // Route::get('/dashboard/report_my', [DashboardController::class, 'report_my']);

    //Event - Admin - Location
    Route::get('/admin/location', [LocationController::class, 'show']);
    Route::get('/admin/location/form', [LocationController::class, 'form']);
    Route::post('/admin/location/add', [LocationController::class, 'add']);
    Route::get('/admin/location/edit/{id}', [LocationController::class, 'edit']);
    Route::post('/admin/location/update/{id}', [LocationController::class, 'update']);
    Route::get('/admin/location/delete/{id}', [LocationController::class, 'delete']);

    //Event - Admin - Facility
    Route::get('/admin/facility', [FacilityController::class, 'show']);
    Route::get('/admin/facility/form', [FacilityController::class, 'form']);
    Route::post('/admin/facility/add', [FacilityController::class, 'add']);
    Route::get('/admin/facility/edit/{id}', [FacilityController::class, 'edit']);
    Route::post('/admin/facility/update/{id}', [FacilityController::class, 'update']);
    Route::get('/admin/facility/delete/{id}', [FacilityController::class, 'delete']);
    Route::get('/admin/facility/image/{id}', [FacilityController::class, 'imageshow']);
    Route::get('/admin/facility/image/form/{id}', [FacilityController::class, 'imageform']);
    Route::post('/admin/facility/image/add/{id}', [FacilityController::class, 'imageadd']);
    Route::get('/admin/facility/image/delete/{id}/{file}', [FacilityController::class, 'imagedelete']);


    Route::get('/available', [EventController::class, 'details500']);

    //Sport - Report
    Route::get('/report', [ReportController::class, 'index']);
    Route::post('/report', [ReportController::class, 'index']);
    Route::get('/report/pelanggan', [ReportController::class, 'pelanggan']);
    Route::post('/report/pelanggan', [ReportController::class, 'pelanggan']);
    Route::get('/report/hasil', [ReportController::class, 'hasil']);
    Route::post('/report/hasil', [ReportController::class, 'hasil']);
    Route::match(['get', 'post'], '/report/acara', [ReportController::class, 'acara']);
    Route::match(['get', 'post'], '/report/jumlahpengunjung', [ReportController::class, 'jumlahpengunjung']);
    Route::match(['get', 'post'], '/report/jumlahacara', [ReportController::class, 'jumlahacara']);
    Route::get('/report/harian', [ReportController::class, 'harian']);
    Route::post('/report/harian', [ReportController::class, 'harian']);
    Route::get('/report/penggunaan', [ReportController::class, 'penggunaan']);

    Route::get('/admin/facility/form', [FacilityController::class, 'form']);
    Route::post('/admin/facility/add', [FacilityController::class, 'add']);
    Route::get('/admin/facility/edit/{id}', [FacilityController::class, 'edit']);
    Route::post('/admin/facility/update/{id}', [FacilityController::class, 'update']);
    Route::get('/admin/facility/delete/{id}', [FacilityController::class, 'delete']);
    Route::get('/admin/facility/image/{id}', [FacilityController::class, 'imageshow']);
    Route::get('/admin/facility/image/form/{id}', [FacilityController::class, 'imageform']);
    Route::post('/admin/facility/image/add/{id}', [FacilityController::class, 'imageadd']);
    Route::get('/admin/facility/image/delete/{id}/{file}', [FacilityController::class, 'imagedelete']);

    // Route::get('/internal/form/{id?}', [InternalController::class, 'form']);

    Route::get('/list/reservation', [EventController::class, 'internalexternal']);

    //Event - Internal
    Route::match(['get', 'post'],'/internal/form/{location}/{startDate}/{endDate}', [InternalController::class, 'form']);
    // Route::post('/internal/form/{email?}', [InternalController::class, 'form']);
    Route::get('/internal/internaladd', [InternalController::class, 'internaladd']);
    Route::post('/internal/internaladd', [InternalController::class, 'internaladd']);
    Route::get('/internal/reservation/{id}', [InternalController::class, 'reservation'])->name('reservation.get');
    Route::post('/internal/reservation/{id}', [InternalController::class, 'reservation'])->name('reservation.post');
    Route::get('/internal/{tab?}', [InternalController::class, 'index']);
    Route::post('/internal/{tab?}', [InternalController::class, 'index']);

    //Event - External
    Route::get('/external', [ExternalController::class, 'index']);
    Route::post('/external', [ExternalController::class, 'index']);
    Route::get('/external/addevent/{location}/{datefrom}/{dateuntil}/{email?}', [ExternalController::class, 'addevent']);
    Route::post('/external/addevent', [ExternalController::class, 'addevent']);
    Route::post('/external/addeventsubmit', [ExternalController::class, 'addeventSubmit']);
    Route::get('/external/event/{id}', [ExternalController::class, 'event']);
    Route::post('/external/event/{id}', [ExternalController::class, 'event']);
    Route::get('/external/hall/{id}', [ExternalController::class, 'hall']);
    Route::get('/external/sport/{id}', [ExternalController::class, 'sport']);
    Route::post('/external/hall/{id}', [ExternalController::class, 'hall']);
    Route::post('/external/sport/{id}', [ExternalController::class, 'sport']);
    Route::post('/external/slotbook/{id}/{date}/{user}', [ExternalController::class, 'sport_slotbook']);
    Route::get('/external/tempahan/{booking}', [ExternalController::class, 'tempahan']);
    Route::get('/external/tempahan_delete/{booking}/{id}', [ExternalController::class, 'tempahan_delete']);
    Route::get('/external/rumusan/{booking}', [ExternalController::class, 'rumusan']);
    Route::post('/external/bayaran/{booking}', [ExternalController::class, 'bayaran']);
    Route::post('/external/submitbayaran/{booking}', [ExternalController::class, 'submit_bayaran']);
    Route::get('/external/equipment/{id}', [ExternalController::class, 'equipment']);

    //Event - Reschedule
    Route::get('/reschedule', [RescheduleController::class, 'index']);
    Route::post('/reschedule', [RescheduleController::class, 'index']);
    Route::match(['get', 'post'], '/reschedule/event/{id}', [RescheduleController::class, 'event']);

    Route::match(['get', 'post'], '/cancel', [EventController::class, 'cancel']);
    Route::match(['get', 'post'], '/cancel/reservation/{id}', [EventController::class, 'submitcancel']);
    Route::match(['get', 'post'], '/cancel/bookingreservation/{id}', [EventController::class, 'submitcancel2']);

    //Hall - Duplicate
    Route::get('/duplicate', [EventController::class, 'duplicate']);
    Route::post('/duplicate', [EventController::class, 'duplicate']);
    Route::match(['get', 'post'], '/duplicate/reschedule/{location}/{dateStart}/{dateEnd}', [EventController::class, 'reschedule']);

    /*
    Route::get('/admin/facility', [FacilityController::class, 'show']);
    Route::get('/admin/facility/form', [FacilityController::class, 'form']);
    Route::post('/admin/facility/add', [FacilityController::class, 'add']);
    Route::get('/admin/facility/edit/{id}', [FacilityController::class, 'edit']);
    Route::post('/admin/facility/update/{id}', [FacilityController::class, 'update']);
    Route::get('/admin/facility/delete/{id}', [FacilityController::class, 'delete']);
    Route::get('/admin/facility/image/{id}', [FacilityController::class, 'imageshow']);
    Route::get('/admin/facility/image/form/{id}', [FacilityController::class, 'imageform']);
    Route::post('/admin/facility/image/add/{id}', [FacilityController::class, 'imageadd']);
    Route::get('/admin/facility/image/delete/{id}/{file}', [FacilityController::class, 'imagedelete']);
    */

    //Hall - Admin - Equipment
    Route::get('/admin/equipment', [EquipmentController::class, 'show']);
    Route::get('/admin/equipment/form', [EquipmentController::class, 'form']);
    Route::post('/admin/equipment/add', [EquipmentController::class, 'add']);
    Route::get('/admin/equipment/edit/{id}', [EquipmentController::class, 'edit']);
    Route::post('/admin/equipment/update/{id}', [EquipmentController::class, 'update']);
    Route::get('/admin/equipment/delete/{id}', [EquipmentController::class, 'delete']);

    //Hall - Admin - Price - Facility
    Route::get('/admin/facilityprice', [PriceFacilityController::class, 'show']);
    Route::get('/admin/facilityprice/form', [PriceFacilityController::class, 'form']);
    Route::post('/admin/facilityprice/add', [PriceFacilityController::class, 'add']);
    Route::get('/admin/facilityprice/edit/{id}', [PriceFacilityController::class, 'edit']);
    Route::post('/admin/facilityprice/update/{id}', [PriceFacilityController::class, 'update']);
    Route::get('/admin/facilityprice/delete/{id}', [PriceFacilityController::class, 'delete']);

    //Hall - Admin - Price - Equuipment
    Route::get('/admin/equipmentprice', [PriceEquipmentController::class, 'show']);
    Route::get('/admin/equipmentprice/form', [PriceEquipmentController::class, 'form']);
    Route::post('/admin/equipmentprice/add', [PriceEquipmentController::class, 'add']);
    Route::get('/admin/equipmentprice/edit/{id}', [PriceEquipmentController::class, 'edit']);
    Route::post('/admin/equipmentprice/update/{id}', [PriceEquipmentController::class, 'update']);
    Route::get('/admin/equipmentprice/delete/{id}', [PriceEquipmentController::class, 'delete']);

    //Hall - Admin - Price - Discount
    Route::get('/admin/discount', [DiscountController::class, 'show']);
    Route::get('/admin/discount/form', [DiscountController::class, 'form']);
    Route::post('/admin/discount/add', [DiscountController::class, 'add']);
    Route::get('/admin/discount/edit/{id}', [DiscountController::class, 'edit']);
    Route::post('/admin/discount/update/{id}', [DiscountController::class, 'update']);
    Route::get('/admin/discount/delete/{id}', [DiscountController::class, 'delete']);

    //Hall - Admin - Price - Deposit
    Route::get('/admin/deposit', [DepositController::class, 'show']);
    Route::get('/admin/deposit/form', [DepositController::class, 'form']);
    Route::post('/admin/deposit/add', [DepositController::class, 'add']);
    Route::get('/admin/deposit/edit/{id}', [DepositController::class, 'edit']);
    Route::post('/admin/deposit/update/{id}', [DepositController::class, 'update']);
    Route::get('/admin/deposit/delete/{id}', [DepositController::class, 'delete']);

    //Event - PDF
    Route::get('/muatturunborang/{id}', [PDFController::class, 'rumusanBorangTempahan']);
    Route::get('/janasurat/{noBorang}', [PDFController::class, 'janaSurat']);
    Route::get('/resitonline/{id}/{jenis?}', [PDFController::class, 'index2']);

    Route::get('/', [EventController::class, 'lists']);
    Route::post('/{location?}/{tarikhMula?}/{tarikhAkhir?}', [EventController::class, 'lists']);

    //Event - Audit
    Route::match(['get', 'post'], '/report/penggunaan/{id?}', [ReportController::class, 'penggunaan']);
    Route::match(['get', 'post'], '/report/auditlog', [ReportController::class, 'audit']);
});
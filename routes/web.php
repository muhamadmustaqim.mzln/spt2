<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\FpxController;
use App\Http\Controllers\TakwimController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\BapiController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\TaxController;
use App\Http\Controllers\DepositController;

//Homepage
// Route::get('/', function () { return view('home'); });
Route::get('/', [DashboardController::class, 'home']);

Route::get('/virtualdewan', function () { return view('all'); });

//Footer - content - pautan
Route::get('/terma', function () { return view('pautan.terma'); });
Route::get('/privasi', function () { return view('pautan.privasi'); });
Route::get('/keselamatan', function () { return view('pautan.keselamatan'); });
Route::get('/penafian', function () { return view('pautan.penafian'); });

Route::get('/test', function () { return view('sport.pdf.resitonline'); });

//Email
Route::get('/mail', [MailController::class, 'index']);
Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

//SSO 
Route::get('/auth', [AuthController::class, 'index']);
Route::get('/auth/test', [AuthController::class, 'test']);
Route::get('/logout', [AuthController::class, 'logout']);

//FPX 
Route::post('/fpx/direct', [FpxController::class, 'direct']);
Route::post('/fpx/indirect', [FpxController::class, 'indirect']);
Route::post('/fpx/sps/direct', [FpxController::class, 'directsps']);
Route::match(['get','post'],'/fpx/sps/indirect/{bookingNo?}', [FpxController::class, 'indirectsps']);
Route::post('/fpx/spa/direct', [FpxController::class, 'directevent']);
Route::post('/fpx/spa/indirect', [FpxController::class, 'indirectevent']); 
Route::match(['get', 'post'], '/fpx/query', [FpxController::class, 'query']); 
Route::match(['get', 'post'], '/fpx/handle', [FpxController::class, 'handle']); 
Route::get('/fpx/test', [FpxController::class, 'test']);
Route::get('/fpx/test1', [FpxController::class, 'test1']);

//Upload File
Route::post('/upload/image/{id}', [FileController::class, 'image']);
Route::post('/upload/imageCover/{id}', [FileController::class, 'imageCover']);
Route::post('/upload/imageVirtual/{id}', [FileController::class, 'imageVirtual']);
Route::post('/upload/file/{id}', [FileController::class, 'file']);
Route::post('/upload/fileAcara/{id}', [FileController::class, 'fileAcara']);
Route::post('/upload/filePembayaranAcara/{id}', [FileController::class, 'filePembayaranAcara']);
Route::get('/upload/scan/{id}', [FileController::class, 'scan_file']);
Route::post('/file/{id}', [UploadController::class, 'file']);

//Takwim
Route::get('/takwim', [TakwimController::class, 'index']);
Route::get('/takwim/form', [TakwimController::class, 'form']);
Route::post('/takwim/form/add', [TakwimController::class, 'addtakwim']);
Route::get('/takwim/edit/{id}', [TakwimController::class, 'edittakwim']);
Route::post('/takwim/edit/update/{id}', [TakwimController::class, 'updatetakwim']);
Route::get('/takwim/delete/{id}', [TakwimController::class, 'deletetakwim']);
Route::get('/takwim/call', [TakwimController::class, 'call']);
Route::get('/takwim/sport/{id}', [TakwimController::class, 'sport']);

//Dashboard - My
Route::get('/dashboard', [DashboardController::class, 'my']);

//Bapi
Route::get('/bapi', [BapiController::class, 'adhoc']);
Route::get('/tunggakan', [BapiController::class, 'tunggakan']);
Route::get('/bapi_test', [BapiController::class, 'test']);

//Admin - Pengurusan Sistem - Pengguna
Route::get('/admin/systemconfiguration', [AdminController::class, 'systemconfiguration']);
Route::match(['get', 'post'], '/admin/systemconfiguration/edit/{id}', [AdminController::class, 'systemconfiguration_edit']);

//Admin - Pengurusan Sistem - Pengguna
Route::get('/admin', [AdminController::class, 'index']);
Route::get('/admin/user/list/{id?}', [AdminController::class, 'userlist']);
Route::post('/admin/user/list', [AdminController::class, 'userlist']);
Route::get('/admin/user/search', [AdminController::class, 'usersearch']);
Route::get('/admin/user/form', [AdminController::class, 'formUserlist']);
Route::post('/admin/user/form/add', [AdminController::class, 'addUserlist']);
Route::get('/admin/user/edit/{id}', [AdminController::class, 'editUserlist']);
Route::post('/admin/user/edit/update/{id}', [AdminController::class, 'updateUserlist']);
Route::get('/admin/user/delete/{id}', [AdminController::class, 'deleteUserlist']);

//Admin - Pengurusan Sistem - Peranan
Route::get('/admin/role/list', [AdminController::class, 'rolelist']);
Route::get('/admin/role/form', [AdminController::class, 'roleform']);
Route::post('/admin/role/add', [AdminController::class, 'roleadd']);
Route::get('/admin/role/edit/{id}', [AdminController::class, 'editRole']);
Route::post('/admin/role/update/{id}', [AdminController::class, 'updateRole']);
Route::get('/admin/role/delete/{id}', [AdminController::class, 'deleteRole']);

//Admin - Pengurusan Sistem - Tempahan(Umum)
Route::get('/admin/tempahan_list', [AdminController::class, 'tempahanList']);
Route::get('/admin/tempahan_form', [AdminController::class, 'tempahanForm']);
Route::post('/admin/tempahanAdd', [AdminController::class, 'tempahanAdd']);
Route::get('/admin/tempahanEdit/{id}', [AdminController::class, 'tempahanEdit']);
Route::post('/admin/tempahanUpdate/{id}', [AdminController::class, 'updateTempahan']);
Route::get('/admin/tempahanDelete/{id}', [AdminController::class, 'deleteTempahan']);

//Admin - Pengurusan Sistem - Maklumat Sistem
Route::get('/admin/list_maklumat', [AdminController::class, 'listMaklumat']);
Route::get('/admin/form_maklumat', [AdminController::class, 'formMaklumat']);
Route::post('/admin/addMaklumat', [AdminController::class, 'addMaklumat']);
Route::get('/admin/editMaklumat/{id}', [AdminController::class, 'editMaklumat']);
Route::post('/admin/updateMaklumat/{id}', [AdminController::class, 'updateMaklumat']);
Route::get('/admin/deleteMaklumat/{id}', [AdminController::class, 'deleteMaklumat']);

//Admin - Pengurusan Sistem - Banner
Route::get('/admin/banner/list', [AdminController::class, 'bannerList']);
Route::get('/admin/banner/form', [AdminController::class, 'bannerform']);
Route::post('/admin/banner/form/add', [AdminController::class, 'addbanner']);
Route::get('/admin/banner/edit/{id}', [AdminController::class, 'editbanner']);
Route::post('/admin/banner/edit/update/{id}', [AdminController::class, 'updatebanner']);
Route::get('/admin/banner/delete/{id}', [AdminController::class, 'deletebanner']);

Route::get('/admin/bannerimageform/{id}', [AdminController::class, 'imageform']);
Route::post('/admin/addbannerimage/{id}', [AdminController::class, 'imageadd']);
Route::get('/admin/banner/edit/image/{id}', [AdminController::class, 'imageEdit']);
Route::post('/admin/updatebannerimage/{id}', [AdminController::class, 'imageupdate']);
Route::get('/admin/banner/image/delete/{id}', [AdminController::class, 'imagedelete']);

//Admin - Pengurusan Sistem - Pengumuman
Route::get('/admin/announcement/list', [AdminController::class, 'announcementlist']);
Route::get('/admin/announcement/form', [AdminController::class, 'announcementform']);
Route::post('/admin/announcement/add', [AdminController::class, 'addAnn']);
Route::get('/admin/announcement/edit/{id}', [AdminController::class, 'editAnn']);
Route::post('/admin/announcement/update/{id}', [AdminController::class, 'updateAnn']);
Route::get('/admin/announcement/delete/{id}', [AdminController::class, 'deleteAnn']);

Route::get('/admin/announcement/edit/butiran/{id}', [AdminController::class, 'editAnnButiran']);
Route::post('/admin/announcement/update/butiran/{id}', [AdminController::class, 'updateAnnButiran']);

//Admin - Pengurusan Pakej
Route::get('/admin/package/list', [AdminController::class, 'packageList']);
Route::get('/admin/package/form', [AdminController::class, 'packageForm']);
Route::post('/admin/package/form/add', [AdminController::class, 'addpackage']);
Route::get('/admin/package/form/edit/{id}', [AdminController::class, 'editpackage']);
Route::post('/admin/package/form/update/{id}', [AdminController::class, 'updatepackage']);
Route::get('/admin/package/delete/{id}', [AdminController::class, 'deletepackage']);

//Admin - Pengurusan Cukai
Route::get('/admin/taxmanagement', [TaxController::class, 'index']);
Route::get('/admin/taxmanagement/form', [TaxController::class, 'taxForm']);
Route::post('/admin/taxmanagement/add', [TaxController::class, 'addTax']);
Route::get('/admin/taxmanagement/edit/{id}', [TaxController::class, 'editTax']);
Route::post('/admin/taxmanagement/update/{id}', [TaxController::class, 'updateTax']);
Route::get('/admin/taxmanagement/delete/{id}', [TaxController::class, 'deleteTax']);

//BAPI
Route::match(['get', 'post'],'/adhoc/{user?}/{data?}', [BapiController::class, 'adhoc']);
Route::get('/adhoc1/{user?}/{data?}', [BapiController::class, 'adhoc1']);
Route::get('/adhocdeposit', [BapiController::class, 'adhocdeposit']);
Route::get('/check_bp/{id?}', [BapiController::class, 'check_bp']);
Route::get('/create_bp/{user?}', [BapiController::class, 'create_bp']);
Route::get('/generate_bill', [BapiController::class, 'generate_bill']);


Route::get('/manual-pengguna', [DashboardController::class, 'manualPengguna']);

// Route::get('/open-modal/{id}', [AdminController::class, 'bannershow'])->name('open.modal');


//Panorama
Route::get('/kompleks_futsal', function () {
    return view('panorama_view.kompleks_futsal');
});
Route::get('/padang_awam', function () {
    return view('panorama_view.padang_awam');
});
Route::get('/presint_8', function () {
    return view('panorama_view.presint_8');
});
Route::get('/presint_9', function () {
    return view('panorama_view.presint_9');
});
Route::get('/presint_11', function () {
    return view('panorama_view.presint_11');
});
Route::get('/presint_16', function () {
    return view('panorama_view.presint_16');
});
Route::get('/padang_sintetik', function () {
    return view('panorama_view.padang_sintetik');
});
Route::get('/taman_pancarona', function () {
    return view('panorama_view.taman_pancarona');
});


//Pemulangan Deposit
Route::get('/list_deposit', [DepositController::class, 'listdeposit']);
Route::match(['get', 'post'], '/list_deposit/form/{id}', [DepositController::class, 'formdeposit']);
Route::get('/deposit_payment_info/{id}', [DepositController::class, 'maklumatbayaran']);

Route::get('/eksportdeposit', [DepositController::class, 'resitDeposit']);

Route::get('/list_action_support', [DepositController::class, 'support']);
Route::get('/list_action_support/form/{id}', [DepositController::class, 'supportform']);
Route::post('/list_action_support/form/{id}', [DepositController::class, 'supportform']);

Route::get('/list_action_review', [DepositController::class, 'review']);
Route::get('/list_action_review/form', [DepositController::class, 'reviewform']);
Route::post('/list_action_review/form', [DepositController::class, 'reviewform']);
Route::get('/list_action_review/formOpenItem', [DepositController::class, 'reviewformOpenItem']);
Route::post('/list_action_review/formOpenItem', [DepositController::class, 'reviewformOpenItem']);

Route::get('/list_action_verify', [DepositController::class, 'verify']);
Route::get('/list_action_verify/form', [DepositController::class, 'verifyform']);
Route::post('/list_action_verify/form', [DepositController::class, 'verifyform']);

Route::post('/deposit/kemaskini/pengguna/{id}', [DepositController::class, 'updateuser']);
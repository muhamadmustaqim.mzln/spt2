<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Models\All;
use App\Models\Sport;
use App\Models\Hall;
use DateTime;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Helper
{
    public function get_role($id)
    {
        $data = all::GetRow('user_role', 'user_id', $id);
        if($data){
            return $data->role_id;
        } else {
            return '';
        }
    }

    public function get_role_name($id)
    {
        $data = all::GetRow('roles', 'id', $id);
        if($data){
            return $data->name;
        } else {
            return '';
        }
    }
    public function status($id)
    {
        $data = "";
        if($id == 1){
            $data = "Aktif";
        }else if($id == 2){
            $data = "Tutup Sementara";
        }else if($id == 0){
            $data = "Tidak Aktif";
        }else if($id == 3){
            $data = "Tempahan Kaunter";
        }

        return $data;
    }
    public function statusbayaran($id)
    {
        if($id == 'SUCCESS') {
            $data = 'BERJAYA';
        } else if($id == 'FAILED') {
            $data = 'GAGAL';
        } else {
            $data = 'MENUNGGGU PENGESAHAN BANK';
        }
        
        return $data;
    }
	
	public function getUserType($id)
    {
        $data = "";
        if($id == 1){
            $data = "Administrator";
        }else{
            $data = "User";
        }

        return $data;
    }

    public function audit_task_desc($id)
    {
        $data = all::GetRow('lkp_task', 'id', $id);
        if($data){
            return $data->task_description;
        } else {
            return 'No data';
        }
    }

    public function question_cat($id)
    {
        $data = all::GetRow('lkp_question_cat', 'id', $id);
        return $data->description;
    }

    public function location_data($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data ?? '';
    }
    public function lkp_loc_from_ef($id){
        try {
            $data = all::GetRow('et_facility', 'id', $id);
            $data1 = all::GetRow('et_facility_type', 'fk_et_facility', $data->id);
            $data2 = all::GetRow('lkp_location', 'id', $data1->fk_lkp_location);

            return $data2->lc_description ?? '';
        } catch (\Throwable $th) {
            // return $id;
            return '';
        }
    }
    public function lkp_loc_from_ef2($id){
        try {
            $data = all::GetRow('et_facility', 'id', $id);
            $data1 = all::GetRow('et_facility_type', 'fk_et_facility', $data->id);
            $data2 = all::GetRow('lkp_location', 'id', $data1->fk_lkp_location);

            return $data2;
        } catch (\Throwable $th) {
            // return $id;
            return '';
        }
    }
    public function lkp_loc_from_eft($id){
        try {
            $data1 = all::GetRow('et_facility_type', 'id', $id);
            $data2 = all::GetRow('lkp_location', 'id', $data1->fk_lkp_location);

            return $data2;
        } catch (\Throwable $th) {
            // return $id;
            return '';
        }
    }
    public function role_location($id){
        if ($id == 5){
            return 4;
        } else if ($id == 6){
            return 5;
        } else if ($id == 7){
            return 6;
        } else if ($id == 8){
            return 7;
        } else if ($id == 18){
            return 9;
        } else if ($id == 19){
            return 10;
        } else if ($id == 20){
            return 11;
        } else return $id;
    }
    public function location($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_description ?? '';
    }
    public function location_addr($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_addr ?? '';
    }
    public function location_town($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_addr_town ?? '';
    }
    public function location_postcode($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_addr_postcode ?? '';
    }
    public function location_contact_no($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_contact_no ?? '';
    }
    public function location_fax_no($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_fax_no ?? '';
    }

    public function typeSportFacility($id)
    {
        if($id != null){
            $data = all::GetRow('et_facility', 'id', $id);

            if (isset($data->ef_desc))
            {
                return $data->ef_desc;	
            }
            
            return "";
        }
    }

    public function getHallAll($id)
    {
        $data = all::GetRow('bh_hall', 'id', $id);
        return $data;
    }

    public function getHall($id)
    {
        $data = all::GetRow('bh_hall', 'id', $id);
        return $data->bh_name;
    }

    public function getHallTwo($id)
    {
        $data = All::GetRow('bh_hall', 'id', $id);

        if ($data !== null) {
            return $data->bh_name;
        }

        return null;
    }

    public function get_main_booking($id)
    {
        $data = all::GetRow('main_booking', 'id', $id);
        return $data;
    }

    public function get_noTempahan($id)
    {
        $data = all::GetRow('main_booking', 'id', $id);
        return $data->bmb_booking_no;
    }

    public function get_userTempahan($id)
    {
        $data = all::GetRow('main_booking', 'id', $id);
        return $data->fk_users;
    }

    public function typeFunction($id)
    {
        $data = all::GetRow('et_function', 'id', $id);
		
		if (isset($data->ef_desc))
		{
			return $data->ef_desc;	
		}
		
        return "";
    }

    public function getBhBooking($id){
        $data = all::GetRow('bh_booking', 'id', $id);
        return $data;
    }

    public function getBhBookingFromMBooking($id){
        $data = all::GetRow('bh_booking', 'fk_main_booking', $id);
        return $data;
    }

    public function getBhBookingDetail($id){

        $data = all::GetRow('bh_booking_detail', 'fk_bh_booking', $id);
        return $data;
    }

    public function getBhPayment($id){
        $data = all::GetRow('bh_payment', 'id', $id);
        return $data;
    }

    public function getBhPaymentFpx($id){
        $data = all::GetRow('bh_payment_fpx', 'id', $id);
        return $data;
    }

    public function getBhPaymentReceipt($id){
        $data = all::GetRow('bh_payment', 'id', $id);
        return $data->bp_receipt_date;
    }
    public function getBhPaymentFPXReceipt($id){
        $data = all::GetRow('bh_payment_fpx', 'id', $id);
        return $data->fpx_serial_no;
    }

    public function getDiscountRate($id)
    {
        $data = all::GetAllRow('lkp_discount_type', 'id', $id)->first();
        return $data->ldt_discount_rate;
    }

    public function getDepositRate($id)
    {
        $data = all::GetAllRow('lkp_deposit_rate', 'id', $id)->first();
        return $data->ldr_rate;
    }

    public function getBhCodeDeposit($id) // From bh_hall id
    {
        $data = all::GetAllRow('bh_hall', 'id', $id)->first();
        return $data->bh_code_deposit;
    }

    public function getBhCodeDepositOnline($id) // From bh_hall id
    {
        $data = all::GetAllRow('bh_hall', 'id', $id)->first();
        return $data->feecode_depo_online;
    }

    public function getBhQuotation($id)
    {
        $data = all::GetAllRow('bh_quotation', 'id', $id)->first();
        return $data;
    }

    public function getQuotationFromBhPayment($id)
    {
        $data = all::GetAllRow('bh_payment', 'id', $id)->first();
        return $data->fk_bh_quotation;
    }

    public function getBhQuotationDetail($id)
    {
        $data = all::GetAllRow('bh_quotation_detail', 'fk_bh_quotation', $id)->first();
        return $data;
    }

    public function getBhQuotationDetails($id)
    {
        $data = all::GetAllRow('bh_quotation_detail', 'fk_bh_quotation', $id);
        return $data;
    }

    public function getEtFromMb($id){
        try {
            $data['ebf'] = All::GetRow('et_booking_facility', 'fk_main_booking', $id);
            $data['eft'] = All::GetRow('et_facility_type', 'id', $data['ebf']->fk_et_facility_type);
        } catch (\Throwable $th) {
            return null;
            //throw $th;
        }

        return $data['eft']->fk_et_facility;
    }

    public function getSportFacility($id)
    {
        $data = all::GetRow('et_facility_type', 'id', $id);
        $data = $data->eft_type_desc.", ".Helper::location($data->fk_lkp_location);
        return $data;
    }

    public function getEtPaymentFpx($id){
        $data = all::GetRow('et_payment_fpx', 'fk_main_booking', $id);
        return $data->fpx_trans_id;
    }
    public function getEtPaymentFpxDate($id){
        $data = all::GetRow('et_payment_fpx', 'fk_main_booking', $id);
        return $data->fpx_trans_date;
    }
    public function getEtPaymentFpxRef($id){
        $data = all::GetRow('et_payment_fpx', 'fk_main_booking', $id);
        return $data->fpx_txn_reference;
    }


    public function getBanner($id)
    {
        $data = all::GetRow('bb_banner', 'id', $id);
        $data = $data->bb_name;
        return $data;
    }

    public function getJenisTempahan($id)
    {
        // $data = all::GetRow('main_booking', 'id', $id);
        // $data = $data->fk_lkp_discount_type.", ".Helper::location($data->ldt_user_cat);
        $data = all::GetRow('lkp_discount_type', 'id', $id)->ldt_user_cat;
        return $data;
    }
    public function get_et_booking_facility_laporan($id)
    {
        $data = all::GetRow('et_booking_facility', 'fk_main_booking', $id);
        return $data->fk_main_booking;
    }
    public function get_et_booking_facility($id)
    {
        $data = all::GetRow('et_booking_facility', 'id', $id);
        return $data;
    }
    public function get_kemudahan($id){
        $data = all::GetRow('et_booking_facility', 'fk_main_booking', $id);
        if ($data == null) {
            return null;
        }
        $data = all::GetRow('et_facility_type', 'id', $data->fk_et_facility_type);
        return $data->eft_type_desc;
    }
    public function get_kemudahan_terperinci($id){
        $data = all::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $id);
        return $data;
    }
    public function get_maklumat_tempahan_terperinci($id){
        $data = all::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $id);
        if($data == null){
            $data = '';
        } else {
            $data = $data->ebfd_event_name;
        }
        return $data;
    }

    public function get_kemudahan_terperinci_laporan($id){
        $data = all::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $id);
        return $data ? $data->fk_et_booking_facility : null;
    }

    public function laporan_penggunaan($tarikh, $key){
        if($key == 73){
            // dd($tarikh, $key);
        }
        try {
            $et_booking_facility_hall= DB::table('et_booking_facility')
                ->leftjoin('main_booking','main_booking.id','=','et_booking_facility.fk_main_booking')
                // ->whereBetween('ebf_start_date', $tarikh)
                // ->whereBetween('ebf_end_date', $tarikh)
                ->where('ebf_start_date', '>=', $tarikh[0])
                ->where('ebf_end_date', '<=', $tarikh[1])
                ->where('fk_et_facility_detail','=', $key)
                ->where('ebf_facility_indi','=',1)
                ->where('main_booking.fk_lkp_status','=',5)
                ->get([DB::raw('COUNT(et_booking_facility.id) as bil_hall,ebf_start_date,sum(ebf_subtotal) as amaun')])
                ->first();
            $et_booking_facility_sport= DB::table('et_booking_facility')
                ->leftjoin('main_booking','main_booking.id','=','et_booking_facility.fk_main_booking')
                ->where('ebf_start_date', '>=', $tarikh[0])
                ->where('ebf_end_date', '<=', $tarikh[1])
                // ->whereBetween('ebf_start_date', $tarikh)
                // ->whereBetween('ebf_start_date', $tarikh)
                ->where('fk_et_facility_detail','=', $key)
                ->where('ebf_facility_indi','=',2)
                ->where('main_booking.fk_lkp_status','=',5)
                ->get([DB::raw('COUNT(et_booking_facility.id) as bil_sport,ebf_start_date,sum(ebf_subtotal) as amaun')])
                ->first();
            if($key == 100){
                // dd($tarikh, $key, $et_booking_facility_hall, $et_booking_facility_sport);
            }
        } catch (\Throwable $th) {
            // dd($tarikh[0], $th);
        }

        return [$et_booking_facility_hall, $et_booking_facility_sport];
    }
    public function laporan_penggunaan_peralatan($tarikh){
        $fk_et_booking_facility = DB::table('et_booking_facility')
            ->leftjoin('main_booking','main_booking.id','=','et_booking_facility.fk_main_booking')
            ->where('ebf_start_date', '>=', $tarikh[0])
            ->where('ebf_end_date', '<=', $tarikh[1])
            // ->whereBetween('ebf_start_date', $tarikh)
            // ->whereBetween('ebf_start_date', $tarikh)
            ->where('ebf_facility_indi','=',1)
            ->where('main_booking.fk_lkp_status','=',5)
            ->pluck('et_booking_facility.id');

        return $fk_et_booking_facility;
    }
    public function laporan_penggunaan_peralatan2($tarikh, $key, $ebf){
        $et_equipment_book = DB::table('et_equipment_book')
            // ->where('eeb_booking_date', '>=', $tarikh[0])
            // ->where('eeb_booking_date', '<=', $tarikh[1])
            ->whereBetween('eeb_booking_date', $tarikh)
            // ->whereBetween('eeb_booking_date', $tarikh)
            ->where('fk_et_equipment',$key)
            ->whereIn('fk_et_booking_facility',$ebf)
            ->get([DB::raw('sum(eeb_quantity) as bil_eqp,eeb_booking_date,sum(eeb_subtotal) as amaun')])
            ->first();

            return $et_equipment_book;
    }
    public function laporan_penggunaan_tahun($num, $selectyear, $key){
        $et_booking_facility_hall = DB::table('et_booking_facility')
            ->where(DB::raw('MONTH(ebf_start_date)'),'=',$num)
            ->where(DB::raw('YEAR(ebf_start_date)'),'=',$selectyear)
            ->where('fk_et_facility_detail','=', $key)
            ->where('ebf_facility_indi','=',1)
            ->get([DB::raw('COUNT(id) as bil_hall,ebf_start_date,sum(ebf_subtotal) as amaun')])
            ->first();

        $et_booking_facility_sport = DB::table('et_booking_facility')
            ->where(DB::raw('MONTH(ebf_start_date)'),'=',$num)
            ->where(DB::raw('YEAR(ebf_start_date)'),'=',$selectyear)
            ->where('fk_et_facility_detail','=', $key)
            ->where('ebf_facility_indi','=',2)
            ->get([DB::raw('COUNT(id) as bil_sport,ebf_start_date,sum(ebf_subtotal) as amaun')])
            ->first();

        $et_booking_facility_hall_year = DB::table('et_booking_facility')
            ->where(DB::raw('YEAR(ebf_start_date)'),'=',$selectyear)
            ->where('fk_et_facility_detail','=', $key)
            ->where('ebf_facility_indi','=',1)
            ->get([DB::raw('COUNT(id) as bil_hall_year,ebf_start_date,sum(ebf_subtotal) as amaun_year')])
            ->first();

        $et_booking_facility_sport_year = DB::table('et_booking_facility')
            ->where(DB::raw('YEAR(ebf_start_date)'),'=',$selectyear)
            ->where('fk_et_facility_detail','=', $key)
            ->where('ebf_facility_indi','=',2)
            ->get([DB::raw('COUNT(id) as bil_sport_year,ebf_start_date,sum(ebf_subtotal) as amaun_year')])
            ->first();

        // dd($et_booking_facility_hall, $et_booking_facility_sport, $et_booking_facility_hall_year, $et_booking_facility_sport_year);
        return [$et_booking_facility_hall, $et_booking_facility_sport, $et_booking_facility_hall_year, $et_booking_facility_sport_year];
    }
    public function laporan_penggunaan_peralatan_tahun($num = null, $selectyear = null, $key = null, $ebf = null, $type){
        if($type == 1){
            $data = DB::table('et_booking_facility')->where('ebf_facility_indi',1)->pluck('id');
            return $data;
        } else {
            try {
            $et_equipment_book = DB::table('et_equipment_book')
                ->where(DB::raw('MONTH(eeb_booking_date)'),'=',$num)
                ->where(DB::raw('YEAR(eeb_booking_date)'),'=',$selectyear)
                ->where('fk_et_equipment',$key)
                ->whereIn('fk_et_booking_facility',$ebf)
                ->get([DB::raw('sum(eeb_quantity) as bil_eqp,eeb_booking_date,sum(eeb_subtotal) as amaun')])
                ->first();

            $et_equipment_book_year = DB::table('et_equipment_book')
                ->where(DB::raw('YEAR(eeb_booking_date)'),'=',$selectyear)
                ->where('fk_et_equipment',$key)
                ->whereIn('fk_et_booking_facility',$ebf)
                ->get([DB::raw('sum(eeb_quantity) as bil_eqp_year,eeb_booking_date,sum(eeb_subtotal) as amaun_year')])
                ->first();

            return [$et_equipment_book, $et_equipment_book_year];
        } catch (\Throwable $th) {
            // dd($et_equipment_book, $et_equipment_book_year, $th);
        }
        }
    }

    public function status_color($id)
    {
        $data = "";
        if($id == 1){
            $data = "label-light-success";
        }else if ($id == 2){
            $data = "label-light-warning";
        }else{
            $data = "label-light-danger";
        }

        return $data;
    }

    public function get_harga_data($id)
    {
        $data = all::GetRow('et_facility_price', 'id', $id);
        return $data;
    }
    public function get_harga($id, $date = null)
    {
        if($date != null){
            $date = Carbon::parse($date);
            if ($date->isWeekend()) {
                $day = 2;
            } else {
                $day = 1;
            }
            $data = array_values(all::GetAllRow('et_facility_price', 'fk_et_facility', $id)->where('efp_day_cat', $day)->toArray())[0];
        } else {
            $data = all::GetRow('et_facility_price', 'fk_et_facility', $id);
        }
        if($data != null){
            return $data->efp_unit_price;
        } else {
            return '-';
        }
    }

    public function get_harga_dewan($id)
    {
        $data = all::GetRow('bh_hall_price', 'fk_bh_hall', $id);
        return $data->bhp_total_price;
    }
    public function get_harga_dewan_daynight($id, $date)
    {
        $date = DateTime::createFromFormat('d-m-Y', $date);
        $day_of_week = $date->format('N');
        $is_weekend = ($day_of_week == 6 || $day_of_week == 7) ? 1 : 2;
        $data = all::GetAllRow('bh_hall_price', 'fk_bh_hall', $id)->where('bhp_day_cat', $is_weekend);
        $data = array_values($data->toArray());
        return $data[0]->bhp_total_price;
    }

    public function get_slot_masa($id)
    {
        $data = all::GetRow('et_slot_time', 'id', $id);
        return $data->est_slot_time;
    }

    public function get_equipment($id)
    {
        $data = all::GetRow('et_equipment', 'id', $id);
        return $data->ee_name;
    }
    public function get_equipment_max_quantity($id)
    {
        $data = all::GetRow('et_equipment', 'id', $id);
        return $data->ee_quantity;
    }

    public function get_timeslot($id)
    {
        $data = all::GetRow('et_slot_time', 'id', $id);
        return $data->est_slot_time;
    }

    public function get_equipment_book($id)
    {
        $data = all::JoinTwoTableWithWhere3('et_equipment_book', 'et_equipment', 'fk_et_booking_facility', $id);
        // $data = all::GetAllRow('et_equipment_book', 'id', $id);
        return $data;
    }
    public function report($id){
        if($id->ebf_facility_indi == 1){
            $data_hall_book= DB::table('et_hall_book')
                ->where('fk_et_booking_facility',$id->id)
                ->pluck('id');
            $data= DB::table('et_hall_time')
                ->join('et_slot_time','et_slot_time.id','=','et_hall_time.fk_et_slot_time')
                ->whereIn('et_hall_time.fk_et_hall_book',$data_hall_book)
                ->orderBy('fk_et_slot_time')
                ->get();
        } else {
            $data= DB::table('et_sport_time')
                ->join('et_slot_price','et_slot_price.id','=','et_sport_time.fk_et_slot_price')
                ->join('et_slot_time','et_slot_time.id','=','et_slot_price.fk_et_slot_time')
                ->where('et_sport_time.fk_et_sport_book',$id->hallbookid)
                ->get();
        }
        return $data;
    }

    public function hall_booking($id)
    {
        $data = all::GetRow('et_hall_book', 'id', $id);
        return $data;
    }
    public function sport_booking($id)
    {
        $data = all::GetRow('et_sport_book', 'id', $id);
        return $data;
    }

    public function get_equipment_hall($id)
    {
        $data = all::GetRow('bh_equipment', 'id', $id);
        return $data->be_name;
    }

    public function tempahanSportType($id)
    {
        $data = all::GetRow('et_facility_type', 'id', $id);
        return $data->eft_type_desc;
    }

    public function tempahanSportLocation($id)
    {
        $data = all::GetRow('et_facility_type', 'id', $id);
        return Helper::location($data->fk_lkp_location);
    }

    public function get_lkp_event($id)
    {

        $data = all::GetRow('lkp_event', 'id', $id);
        // dd($data);
        return $data->le_description;
    }

    public function get_lkp_event_string($id)
    {
        $data = all::GetRow('lkp_event', 'id', $id);
        if ($data) {
            return $data->le_description ?? ''; // Return the description if it exists, otherwise return an empty string
        }
        return ''; // Return an empty string if no data is found for the given id
    }
    
    public function get_lkp_event_string_laporan($id)
    {
        if ($id === null) {
            return ''; // or any default value you want to return for null values
        }

        $data = all::GetRow('lkp_event', 'id', $id);
        return $data ? $data->le_description : '';
    }

    public function get_hall_usage_capacity($id)
    {
        $data = all::GetRow('bh_hall_usage', 'id', $id);
        return $data->capasity;
    }
    public function negeri($id)
    {
        // dd($id);
        if (!empty($id)){
            $data = all::GetRow('lkp_state', 'id', $id);
            return ($data->ls_description);
        }
    }
    public function negara($id)
    {
        if (!empty($id)){
            $data = all::GetRow('lkp_country', 'id', $id);
            return ($data->lc_description);
        }
    }

    public function tempahanSportDetail($id)
    {
        $data = all::GetRow('et_facility_detail', 'id', $id);
        return $data->efd_name;
    }

    public function tempahanSportDetailFeeCode($id)
    {
        $data = all::GetRow('et_facility_detail', 'id', $id);
        return $data->efd_fee_code;
    }

    public function tempahanSportTime($id)
    {
        try {
            $data = all::GetRow('et_slot_price', 'id', $id);
            $time = all::GetRow('et_slot_time', 'id', $data->fk_et_slot_time);
            return $time->est_slot_time;
        } catch (\Throwable $th) {
            // return $id;
        }
    }
    public function tempahanHallTime($id)
    {
        try {
            $data = all::GetRow('et_facility_price', 'id', $id);
            $time = all::GetRow('et_slot_time', 'id', $data->fk_et_slot_time);
            return $time->est_slot_time;
        } catch (\Throwable $th) {
            // return $id;
        }
    }

    public function tempahanSportGST($id)
    {
        $data = all::GetRow('lkp_gst_rate', 'id', $id);
        return $data->lgr_gst_code;
    }

    public function get_tweet_count($id)
    {
        $token = 'AAAAAAAAAAAAAAAAAAAAAPL2gwEAAAAAohNIi3R6MVK4mSyHj1jPEPBm02Q%3DyG8Rj1cNIWi6I1xqvsTHxdMMO063YCaIiE9d69GGOCndarhS9u';
		$host = 'https://api.twitter.com/2/tweets/counts/recent?query='.$id;
		$ch = curl_init($host);
		$headers = array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
		);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result, true);
		$tweet = $data['meta']['total_tweet_count'];
        return $tweet;
    }

    public function jenis_tempahan($id)
    {
        $data = "";
        if($id == 1){
            $data = "Dewan";
        }else if ($id == 2){
            $data = "Sukan";
        }
        return $data;
    }

    public function kategori_slot($id)
    {
        $data = all::GetSpecRow('lkp_slot_cat', 'id', $id);
        foreach($data as $d){
            $data = $d->lsc_slot_desc;
        }
        return $data;
    }

    public function kategori_gst($id)
    {
        $data = all::GetSpecRow('lkp_gst_rate', 'id', $id);
        foreach($data as $d){
            $data = $d->lgr_description;
        }
        return $data;
    }

    public function kadar_gst($id)
    {
        $data = all::GetSpecRow('lkp_gst_rate', 'id', $id);
        foreach($data as $d){
            $data = $d->lgr_rate;
        }
        return Helper::moneyhelper($data);
    }

    public function kategori_sewa($id)
    {
        $data = all::GetSpecRow('lkp_slot_rent', 'id', $id);
        foreach($data as $d){
            $data = $d->lsc_slot_desc;
        }
        return $data;
    }

    public function kategori_pakej($id)
    {
        $data = all::GetSpecRow('et_package', 'id', $id);
        foreach($data as $d){
            $data = $d->ep_name;
        }
        return $data;
    }

    public function jenis_fungsi($id)
    {
        $data = "";
        if($id == 1){
            $data = "Dewan / Sukan";
        }else if ($id == 2){
            $data = "Peralatan & Kelengkapan";
        }
        return $data;
    }

    public function kategori_hari($id)
    {
        $data = "";
        if($id == 1){
            $data = "Isnin - Jumaat";
        }else if ($id == 2){
            $data = "Sabtu - Ahad / Cuti Umum";
        }
        return $data;
    }

    public function get_hari($id)
    {
        $data = "";
        if($id == 0){
            $data = "Ahad";
        }else if ($id == 1){
            $data = "Isnin";
        }else if ($id == 2){
            $data = "Selasa";
        }else if ($id == 3){
            $data = "Rabu";
        }else if ($id == 4){
            $data = "Khamis";
        }else if ($id == 5){
            $data = "Jumaat";
        }else if ($id == 6){
            $data = "Sabtu";
        }
        return $data;
    }

    public function get_permohonan($id)
    {
        $data = all::GetSpecRow('lkp_discount_type', 'id', $id);
        foreach($data as $d){
            $data = $d->ldt_user_cat;
        }
        if(!$data){
            return 'Tempahan Biasa';
        }
        return $data;
    }

    public function get_status_tempahan($id)
    {
        $data = all::GetSpecRow('lkp_status', 'id', $id);
        foreach($data as $d){
            $data = $d->ls_description;
        }
        if ($id == 10){ // Change naming convention
            $data = 'Tempahan Dibatalkan';
        }
        return $data;
        // return $data->ls_description;
    }

    public function get_status_tempahan_detail($id){
        $data = all::JoinTwoTable('lkp_status','main_booking');

        $data = all::GetSpecRow('lkp_status', 'id', $data->fk_main_booking);
        foreach($data as $d){
            $data = $d->ls_description;
        }
        return $data;
    }

    public function get_jenis_bayaran($id)
    {
        $data = all::GetSpecRow('lkp_payment_type', 'id', $id);
        foreach($data as $d){
            $data = $d->lpt_description;
        }
        return $data;
    }

    public function get_kutipan_bayaran($id)
    {
        $data = all::GetSpecRow('lkp_payment_mode', 'id', $id);
        foreach($data as $d){
            $data = $d->lpm_description;
        }
        return $data;
    }

    public function date_format($date)
    {
        if (!empty($date)){
            $data = date('d-m-Y', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }
    public function date_format1($date)
    {
        if (!empty($date)){
            $data = date('Y-m-d', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }
    public function date_format2($date)
    {
        if (!empty($date)){
            $data = date('d F Y', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }
    public function date_format_between($date)
    {
        $dates = explode(" / ", $date);
        $dates[0] = date('d-m-Y', strtotime($dates[0]));
        $dates[1] = date('d-m-Y', strtotime($dates[1]));
        $dates = $dates[0] . ' / ' . $dates[1];
        return $dates;
    }
    public function date_format_for_db($date)
    {
        if (!empty($date)){
            $data = date('Y-m-d', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }
    public function time_format($time)
    {
        if (!empty($time)) {
            $formattedTime = date('h:i A', strtotime($time)); // 'h' for 12-hour format, 'i' for minutes, 'A' for AM/PM
            return $formattedTime;
        } else {
            return $time;
        }
    }
    public function datetime_format($date)
    {
        if (!empty($date)){
            $data = date('d-m-Y H:i:s', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }
    public function datetime_format2($date)
    {
        if (!empty($date)){
            $data = date('d-m-Y | H:i:s', strtotime($date));
            return $data;
        } else {
            return $date;
        }
    }


    // Acara::Begin
    public function bmb_type_user($id)
    {
        $data = '';
        if($id == 1) {
            $data = 'Luaran';
        } else if($id == 2) {
            $data = 'Dalaman';
        }
        return $data;
    }

    public function locationspa($id)
    {
        $data = all::GetRow('spa_location', 'id', $id);
        return $data->name;
    }
    public function locationspa_depoRate($id)
    {
        $data = all::GetRow('spa_location', 'id', $id);
        return $data->deposit;
    }
    public function locationspa_rent_perday($id)
    {
        $data = all::GetRow('spa_location', 'id', $id);
        return $data->rent_perday;
    }
    
    public function categoryspa($id)
    {
        if($id == 1){
            $data = 'Platinum';
        }else if ($id == 2){
            $data = 'Emas';
        }else if ($id == 3) {
            $data = 'Perak';
        }
        return $data;
    }

    public function location_cat($id)
    {
        if ($id == 1) {
            return 'Platinum';
        } else if ($id == 2){
            return 'Emas';
        } else if ($id == 3){
            return 'Perak';
        }
    }
    // Acara::End

    public function moneyhelper($id) 
    {
        try {
            $cleaned_id = str_replace(',', '', $id);
            $float_id = (float) $cleaned_id;
        
            $data = number_format($float_id, 2, '.', ',');

            return $data;
        } catch (\Throwable $th) {
            // dd($th, $id, $float_id);
            //throw $th;
        }
    }

    public function get_nama($id)
    {
        $data = all::GetSpecRow('users', 'id', $id);
        foreach($data as $d){
            $data = $d->fullname;
        }
        return $data;
    }

    public function get_email($id)
    {
        $data = all::GetSpecRow('users', 'id', $id);
        foreach($data as $d){
            $data = $d->email;
        }
        return $data;
    }

    public function get_name($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_name;
        }
        return $data;
    }

    public function get_ic_number($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_reference_id;
        }
        return $data;
    }

    public function get_address($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_address;
        }
        return $data;
    }
    public function get_town($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_town;
        }
        return $data;
    }
    public function get_postcode($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_poscode;
        }
        return $data;
    }

    public function get_no($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_phone_no;
        }
        return $data;
    }

    public function get_no_office($id)
    {
        $data = all::GetSpecRow('user_profiles', 'fk_users', $id);
        foreach($data as $d){
            $data = $d->bud_office_no;
        }
        return $data;
    }

    public function fpx_status($id)
    {
        $data = 0;
        if($id == "SUCCESS"){
            $data = 1;
        }else if ($id == "FAILED"){
            $data = 0;
        }else{
            $data = 2;
        }
        return $data;
    }

    public function tempahan($id)
    {
        if($id == 1){
            $query = "ff";
        }

        return $query;
    }

    // Deposit Helper
    public function depo_status($id){
        $data = All::GetRow('deposit_status', 'id', $id)->ds_description;
        
        return $data;
    }
    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }
}
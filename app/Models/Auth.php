<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Auth extends Model
{
    public function Validate($email){
        $query = DB::table('users')
                ->where('email', $email)
                ->where('deleted_at', NULL)
                ->first();
        return $query;
    }
    public function ValidateProfiles($email){
        $query = DB::table('user_profiles')
                ->where('bud_email', $email)
                ->where('deleted_at', NULL)
                ->first();
        return $query;
    }

    public function Sso($email){
        $options = array(
            "ssl" => array(
                "verify_peer"=> false,
                "verify_peer_name"=> false,
            ),
        );  
        $json_data = file_get_contents("https://api.ppj.gov.my/api/call/5?fid=52&token=VEcybXJ5OG5tVmNMdzhlY1RJRnJFVVo3eHk1TDE2V05uQzRpRDA2bw==&ref=".$email , false, stream_context_create($options));
        $response = json_decode($json_data);
        
        return $response;
    }

    public function user($id){
        $query = DB::select(DB::raw("
            SELECT * FROM user_profiles WHERE fk_users IN (
                SELECT id FROM users WHERE (fullname LIKE '%$id%' OR email LIKE '%$id%') AND STATUS = 1 AND deleted_at IS NULL
            )
            OR bud_reference_id LIKE '%$id%'
        "));

        return $query;
    }


    public function getAvailslot($et, $fidd, $idbooking, $date, $fbooking) {
        // dd($fbooking);
        //dd($et);
        $getEtt = EfDetail::where('id', $fidd)->first();
        //dd($fidd);
        $ett = $getEtt->fk_et_facility_type;
        $day = $this->getDayIndicator($date,$ett);
        //dd($ett);
        $getParrent = EfDetail::where('fk_et_facility_type', $ett)->where('efd_parent', '=', 1)->first();
        //$parent = $getParrent->id;
        // dd($getParrent);
        if ($getParrent == NULL) {
            $checkparentluar = EtFacilityType::where('id', $ett)->first();
            $loc = $checkparentluar->fk_lkp_location;
            //dd($loc);
            $parentluar = $checkparentluar->eft_parent;
            if ($parentluar == NULL) {
                $checkson = EtFacilityType::where('eft_parent', $ett)->first();
                if ($checkson == NULL) {
                    if ($et == '11') {
                        //dd($et);
                        if ($loc == 5) {
                            $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                                          FROM et_slot_price a,et_slot_time b
                                          WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day AND a.fk_et_slot_time = b.id and a.esp_squash_indi = 2
                                          AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail =  $fidd )
                                          AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c WHERE c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility AND b.id = a.fk_et_sport_book)
                                          ORDER BY b.display_order"));
                            return $info;
                            //dd($info);
                            
                        } else {
                            $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                                          FROM et_slot_price a,et_slot_time b
                                          WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day AND a.fk_et_slot_time = b.id and a.esp_squash_indi = 1
                                          AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail =  $fidd )
                                          AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c WHERE c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility AND b.id = a.fk_et_sport_book)
                                          ORDER BY b.display_order"));
                            return $info;
                            //dd($info);
                            
                        }
                    } else {
                        // dd("masuk");
                        $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                                  FROM et_slot_price a,et_slot_time b
                                  WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day AND a.fk_et_slot_time = b.id
                                  AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail =  $fidd )
                                  AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c WHERE c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility AND b.id = a.fk_et_sport_book)
                                  ORDER BY b.display_order"));
                        return $info;
                        //dd($info);
                        
                    }
                    //dd("masuk");
                    
                } else {
                    //dd("masuk");
                    // dd($checkson);
                    $sonid = $checkson->id;
                    //dd($sonid);
                    $ar = array();
                    $childlist = Efdetail::where('fk_et_facility_type', $sonid)->select('id')->get();
                    foreach ($childlist as $key => $value) {
                        $ar[] = $value->id;
                    }
                    $ar[] = $fidd;
                    //dd($ar);
                    $s = implode(",", $ar);
                    // dd($s);
                    //  dd($childlist);
                    // dd("jeng");
                    $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                              FROM et_slot_price a,et_slot_time b
                              WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day
                              AND a.fk_et_slot_time = b.id
                              AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = $date AND fk_et_facility_detail =  $fidd )
                              AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = $date AND fk_et_facility_detail IN ($s)
                              AND b.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c WHERE c.fk_main_booking = $idbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility
                              AND b.id = a.fk_et_sport_book)
                              AND b.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c
                              WHERE c.fk_main_booking = $idbooking AND c.fk_et_facility_detail IN (SELECT id FROM et_facility_detail WHERE fk_et_facility_type = $et) AND c.id = b.fk_et_booking_facility
                              AND b.id = a.fk_et_sport_book) ORDER BY id) ORDER BY b.display_order"));
                    return $info;
                    // dd($info);
                    
                }
            } else {
                // $getparentfidd = Efdetail::where('id',$parentluar)->first();
                // //dd($getparentfidd);
                // $parentfidd    = $getparentfidd->id;//log esok
                // dd("masuk");
                /*$info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                  FROM et_slot_price a,et_slot_time b
                  WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day
                  AND a.fk_et_slot_time = b.id
                  AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail =  $fidd )
                  AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c
                  WHERE c.fk_main_booking = $idbooking AND c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility
                  AND b.id = a.fk_et_sport_book) ORDER BY b.display_order"));*/

		//Dzul Buat
                $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                  FROM et_slot_price a,et_slot_time b
                  WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day
                  AND a.fk_et_slot_time = b.id
                  AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail =  $fidd )
		  AND b.id NOT IN (
					SELECT f1.id
					FROM et_sport_time AS a1
					JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
					JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
					JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
					JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
					JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
					WHERE b1.esb_booking_date = '$date' AND d1.fk_lkp_status IN (5, 11) AND c1.fk_et_facility_detail = $fidd
				 )
                  AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c
                  WHERE c.fk_main_booking = $idbooking AND c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility
                  AND b.id = a.fk_et_sport_book) ORDER BY b.display_order"));

                return $info;


            }
        } else {
            $parent = $getParrent->id;
            if ($parent == $fidd) {
                //dd("SAMA");
                $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                            FROM et_slot_price a,et_slot_time b
                            WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day
                            AND a.fk_et_slot_time = b.id
                            AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail = $fidd )
                            ORDER BY b.display_order"));
                //dd($info);
                return $info;
            } else {
                //dd("xsama");
                $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                            FROM et_slot_price a,et_slot_time b
                            WHERE a.fk_et_facility = $et AND a.esp_day_cat = $day
                            AND a.fk_et_slot_time = b.id
                            AND b.id NOT IN (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail = $parent )
                            AND a.id NOT IN (SELECT a.fk_et_slot_price FROM et_sport_time a,et_sport_book b,et_booking_facility c
                            WHERE c.fk_main_booking = $idbooking AND c.fk_et_facility_detail in ($parent,$fidd) 
                            AND c.id = $fbooking
                            AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_sport_book) ORDER BY b.display_order"));
                return $info;
            }
        }
    }

}

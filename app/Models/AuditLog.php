<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\MailController as Mail;
use App\Models\All;

class AuditLog extends Model
{
    // use HasFactory;

    public function log($task, $id, $data = null, $mb_status = null)
    {
        $task = Crypt::decrypt($task);  // fk_lkp_task
        $id = Crypt::decrypt($id);      // table_ref_id

        $auditLog = new AuditLog();
        $taskDetails = $auditLog->lkp_task($task);
        
        if ($id != null) {
            $mainBooking = All::GetRow('main_booking', 'id', $id);
            if ($mainBooking !== null) {
                $bmb_booking_no = $mainBooking->bmb_booking_no;
            } else {
                // Handle the case where no row is found with the specified id
                $bmb_booking_no = null; // or any default value you want to assign
            }
        }

        $log = [
            'fk_users'      => Session('user.id'),
            'fk_lkp_task'   => $task,
            'table_ref_id'  => $id,
            'main_booking_status'   => $mb_status,
            // 'task'          => ($data != null) ? $taskDetails->task_description . ' | ' . $data . ' | ' . $bmb_booking_no : $taskDetails->task_description . ' | ' . $bmb_booking_no ,
            'task'          => $data,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ];
        $query = All::InsertGetID('audit_trail', $log);

        if($task == 120 || $task == 122 || $task == 176){
            Mail::payment_received($id, $task);
        }

        return $query;
    }
    private function lkp_task($id)
    {
        return All::GetRow('lkp_task', 'id', $id);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\All;
use App\Models\MainBooking;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

// if(Session::get('user.id') == '61894'){
// }
// if(Session::get('user.id') == '50554'){
// }


class Sport extends Model
{
    public function fasility(){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE eft_status = 1 AND deleted_at IS NULL AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }
    public function fasilityPublic(){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE eft_status IN (1, 2, 3) AND deleted_at IS NULL AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }
    public function fasilityPublicKomuniti(){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE eft_status IN (1, 2, 3) AND deleted_at IS NULL AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 1 AND ef_status = 1
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }

    public function facility($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE eft_status = 1 AND id = $id AND deleted_at IS NULL AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }

    public function details($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE id = $id AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }

    public function price($id, $date = null){
        $date = Carbon::createFromFormat('Y-m-d', $date);
        if ($date->isWeekend()) {
            $day_cat = 2;
        } else {
            $day_cat = 1;
        }
        $query = DB::select(DB::raw("
            SELECT DISTINCT c.efd_name, d.efp_unit_price, d.fk_lkp_slot_cat, d.efp_day_cat, d.efp_status 
            FROM et_facility_type AS a
            JOIN et_facility AS b ON b.id = a.fk_et_facility
            JOIN et_facility_detail AS c ON c.fk_et_facility_type = a.id
            JOIN et_facility_price AS d ON d.fk_et_facility = b.id
            WHERE a.id = $id AND d.efp_status = 1 AND c.deleted_at IS NULL AND d.deleted_at IS NULL 
            ORDER BY c.efd_name
        "));

        return $query;
    }

    public function onepublic($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE fk_lkp_location = $id AND deleted_at IS NULL AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            )
            ORDER BY eft_type_desc ASC
        "));
        // $query = DB::select(DB::raw("
        //     SELECT * FROM et_facility_type WHERE fk_lkp_location = $id AND deleted_at IS NULL AND eft_status = 1  AND fk_et_facility IN (
        //         SELECT id FROM et_facility WHERE ef_type = 2
        //     )
        // "));

        return $query;
    }
    public function one($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE fk_lkp_location = $id AND deleted_at IS NULL AND eft_status IN (0,1)  AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            )
        "));

        return $query;
    }
    public function one1($id){
        $query = DB::select(DB::raw("
            SELECT et_facility_type.id as id, et_facility.id as efid, et_facility_type.eft_type_desc 
            FROM et_facility_type 
            JOIN et_facility ON et_facility_type.fk_et_facility = et_facility.id
            WHERE et_facility_type.fk_lkp_location = $id 
            AND et_facility_type.deleted_at IS NULL 
            AND et_facility_type.fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 1
            )
        "));
        return $query;
    }
    public function slot2($id, $data){
        $date = Carbon::createFromFormat('Y-m-d', $data);
        $eft = All::GetRow('et_facility_type', 'id', $id);
        $lkp_location = $eft->fk_lkp_location;
        $ef = $eft->fk_et_facility;
        $efo = $ef;
        if ($date->isWeekend()) {
            $day_cat = 2;
        } else {
            $day_cat = 1;
        }

        if(in_array($ef, [1,2])){
            $ef = [1,2];
        } else {
            $ef = [$id]; 
        }
        $ef = implode(',', $ef);
        
        $query = DB::select(DB::raw('
            SELECT DISTINCT 
                a.eft_slot_per_user,
                b.id AS "fidd",
                b.efd_name, 
                c.efp_status, 
                c.efp_unit_price AS "harga", 
                c.fk_lkp_gst_rate AS "gst", 
                c.id AS "cidd", 
                d.est_slot_time, 
                d.id AS "fkst", 
                d.start, 
                d.fk_lkp_slot_cat AS "sesi", 
                b.deleted_at
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = '.$id.' AND b.efd_status = 1 AND c.efp_status = 1 AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL AND a.fk_et_facility = '.$efo.' AND a.fk_lkp_location = '.$lkp_location.'
                AND (d.id) NOT IN (
                    SELECT f1.id AS "aaa"
                    FROM et_sport_time AS a1
                    JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                    JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                    JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                    JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                    JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                    JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                    JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                    WHERE b1.esb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.')  AND d1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_et_facility IN ('.$ef.') AND h1.fk_lkp_location = '.$lkp_location.'
                    UNION
                    SELECT a2.fk_et_slot_time AS "aaa"
                    FROM et_confirm_booking_detail AS a2
                    JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                    JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
                    WHERE a2.ecbd_date_booking = "'.$data.'" AND c2.fk_et_facility IN ('.$ef.') AND c2.fk_lkp_location = '.$lkp_location.'
                )
        '));
        // dd($id, $data, $eft, $lkp_location, $ef, $day_cat, $query);
        // $query = DB::select(DB::raw('
        //     SELECT DISTINCT 
        //         a.eft_slot_per_user,
        //         b.id AS "fidd",
        //         b.efd_name, 
        //         c.efp_status, 
        //         c.efp_unit_price AS "harga", 
        //         c.fk_lkp_gst_rate AS "gst", 
        //         c.id AS "cidd", 
        //         d.est_slot_time, 
        //         d.id AS "fkst", 
        //         d.start, 
        //         d.fk_lkp_slot_cat AS "sesi", 
        //         b.deleted_at
        //     FROM et_facility_type AS a
        //     JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
        //     JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
        //     JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
        //     WHERE b.fk_et_facility_type = '.$id.' AND b.efd_status = 1 AND c.efp_status = 1 AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL
        //         AND (d.id, b.efd_name) NOT IN (
        //             SELECT f1.id AS "aaa", g1.efd_name AS "bbb"
        //             FROM et_sport_time AS a1
        //             JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
        //             JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
        //             JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
        //             JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
        //             JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
        //             JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
        //             WHERE b1.esb_booking_date = "'.$data.'" AND c1.fk_et_facility_type = '.$id.' AND d1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND g1.fk_et_facility_type = '.$id.'
        //             UNION
        //             SELECT a2.fk_et_slot_time AS "aaa", b2.efd_name AS "bbb"
        //             FROM et_confirm_booking_detail AS a2
        //             JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
        //             WHERE a2.ecbd_date_booking = "'.$data.'" AND b2.fk_et_facility_type = '.$id.' 
        //         )
        // '));

        return $query;
    }
    
    public function slot($id, $data, $et_func = null){
        $date = Carbon::createFromFormat('Y-m-d', $data);
        $eft = All::GetRow('et_facility_type', 'id', $id);
        $lkp_location = $eft->fk_lkp_location;
        $ef = $eft->fk_et_facility;
        $efo = $ef;

        if ($date->isWeekend()) {
            $day_cat = 2;
        } else {
            $day_cat = 1;
        }

        $eftrue = 0;

        if(in_array($ef, [1,2])){   // [Semua Presint] Fasiliti Dewan & Badminton bertindih
            $ef = [1,2];            
            $eftrue = 1;
        } else if (in_array($ef, [22,23,24])) {
            if($lkp_location == 6){ // [Presint 11] Fasiliti Bola Tampar, Bola Keranjang & Futsal bertindih
                $ef = [22,23,24];   
                $eftrue = 1;
            } else {                // Bukan Presint 11, tiada tindihan lokasi
                $ef = [$id];        
            }
        } else {
            $ef = [$id]; 
        }
        $ef = implode(',', $ef);

        if($eftrue) {       // Tindihan fasiliti
            $query = DB::select(DB::raw('
                SELECT DISTINCT 
                    a.eft_slot_per_user,
                    b.id AS "fidd",
                    b.efd_name, 
                    c.efp_status, 
                    c.efp_unit_price AS "harga", 
                    c.fk_lkp_gst_rate AS "gst", 
                    c.id AS "cidd", 
                    d.est_slot_time, 
                    d.id AS "fkst", 
                    b.id AS "bbb",
                    d.start, 
                    d.fk_lkp_slot_cat AS "sesi", 
                    b.deleted_at
                FROM et_facility_type AS a
                JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
                JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
                JOIN et_slot_price AS e ON e.fk_et_facility_price = c.id
                JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
                WHERE b.fk_et_facility_type = '.$id.' '.($et_func !== null ? 'AND c.fk_et_function = '.$et_func : '').' AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL AND a.fk_et_facility = '.$efo.' AND a.fk_lkp_location = '.$lkp_location.'
                    AND (d.id) NOT IN (
                        SELECT f1.id AS "aaa"
                        FROM et_sport_time AS a1
                        JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                        JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                        JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                        JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                        JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                        JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                        JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                        WHERE b1.esb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_et_facility IN ('.$ef.') AND h1.fk_lkp_location = '.$lkp_location.'
                        UNION
                        SELECT a2.fk_et_slot_time AS "aaa"
                        FROM et_confirm_booking_detail AS a2
                        JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                        JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
                        WHERE a2.ecbd_date_booking = "'.$data.'" AND c2.fk_et_facility IN ('.$ef.') AND c2.fk_lkp_location = '.$lkp_location.' AND a2.deleted_at IS NULL
                        UNION
                        SELECT a1.fk_et_slot_time AS "aaa"
                        FROM et_hall_time AS a1
                        JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
                        JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                        JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                        JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                        JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                        WHERE b1.ehb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2,4) AND h1.fk_lkp_location = '.$lkp_location.'
                    )
            '));
        } else {            // Tiada tindihan fasiliti
            $query = DB::select(DB::raw('
                SELECT DISTINCT 
                    a.eft_slot_per_user,
                    b.id AS "fidd",
                    b.efd_name, 
                    c.efp_status, 
                    c.efp_unit_price AS "harga", 
                    c.fk_lkp_gst_rate AS "gst", 
                    c.id AS "cidd", 
                    d.est_slot_time, 
                    d.id AS "fkst", 
                    b.id AS "bbb",
                    d.start, 
                    d.fk_lkp_slot_cat AS "sesi", 
                    b.deleted_at
                FROM et_facility_type AS a
                JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
                JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
                JOIN et_slot_price AS e ON e.fk_et_facility_price = c.id
                JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
                WHERE b.fk_et_facility_type = '.$id.' '.($et_func !== null ? 'AND c.fk_et_function = '.$et_func : '').' AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL AND a.fk_et_facility = '.$efo.' AND a.fk_lkp_location = '.$lkp_location.'
                    AND (d.id, b.id) NOT IN (
                        SELECT f1.id AS "aaa", g1.id AS "bbb"
                        FROM et_sport_time AS a1
                        JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                        JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                        JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                        JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                        JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                        JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                        JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                        WHERE b1.esb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_et_facility IN ('.$ef.') AND h1.fk_lkp_location = '.$lkp_location.'
                        UNION
                        SELECT a2.fk_et_slot_time AS "aaa", b2.id AS "bbb"
                        FROM et_confirm_booking_detail AS a2
                        JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                        JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
                        WHERE a2.ecbd_date_booking = "'.$data.'" AND a2.fk_et_facility_type IN ('.$id.') AND c2.fk_lkp_location = '.$lkp_location.' AND a2.deleted_at IS NULL
                        UNION
                        SELECT a2.fk_et_slot_time AS "aaa", b2.id AS "bbb"
                        FROM et_confirm_booking_detail AS a2
                        JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                        JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
                        WHERE a2.ecbd_date_booking = "'.$data.'" AND c2.fk_et_facility IN ('.$ef.') AND c2.fk_lkp_location = '.$lkp_location.' AND a2.deleted_at IS NULL
                        UNION
                        SELECT a1.fk_et_slot_time AS "aaa", g1.id AS "bbb"
                        FROM et_hall_time AS a1
                        JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
                        JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                        JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                        JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                        JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                        WHERE b1.ehb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_lkp_location = '.$lkp_location.'
                    )
            '));
            // dd($query);
        }
        // if(Session::get('user.id') == 61894){
            // $query = DB::select(DB::raw('
            //     SELECT a2.fk_et_slot_time AS "aaa", b2.id AS "bbb"
            //     FROM et_confirm_booking_detail AS a2
            //     JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
            //     JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
            //     WHERE a2.ecbd_date_booking = "'.$data.'" AND c2.fk_et_facility IN ('.$ef.') AND c2.fk_lkp_location = '.$lkp_location.' AND a2.deleted_at IS NULL
            //     '));
            // dd($data, $ef, $id, $lkp_location, $query);
        // }
        // old{
        //     // $query = DB::select(DB::raw('
        //     //     SELECT DISTINCT 
        //     //         a.eft_slot_per_user,
        //     //         b.id AS "fidd",
        //     //         b.efd_name, 
        //     //         c.efp_status, 
        //     //         c.efp_unit_price AS "harga", 
        //     //         c.fk_lkp_gst_rate AS "gst", 
        //     //         c.id AS "cidd", 
        //     //         d.est_slot_time, 
        //     //         d.id AS "fkst", 
        //     //         d.start, 
        //     //         d.fk_lkp_slot_cat AS "sesi", 
        //     //         b.deleted_at
        //     //     FROM et_facility_type AS a
        //     //     JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
        //     //     JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
        //     //     JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
        //     //     WHERE b.fk_et_facility_type = '.$id.' AND b.efd_status = 1 AND c.efp_status = 1 AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL
        //     //         AND (d.id, b.efd_name) NOT IN (
        //     //             SELECT f1.id AS "aaa", g1.efd_name AS "bbb"
        //     //             FROM et_sport_time AS a1
        //     //             JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
        //     //             JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
        //     //             JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
        //     //             JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
        //     //             JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
        //     //             JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
        //     //             WHERE b1.esb_booking_date = "'.$data.'" AND c1.fk_et_facility_type = '.$id.' AND d1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND g1.fk_et_facility_type = '.$id.'
        //     //             UNION
        //     //             SELECT a2.fk_et_slot_time AS "aaa", b2.efd_name AS "bbb"
        //     //             FROM et_confirm_booking_detail AS a2
        //     //             JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
        //     //             WHERE a2.ecbd_date_booking = "'.$data.'" AND b2.fk_et_facility_type = '.$id.' 
        //     //         )
        //     // '));
        // }
        
        return $query;
    }
    
    public function slotByMainBooking($id, $data, $et_func = null, $mbid){
        $date = Carbon::createFromFormat('Y-m-d', $data);
        $eft = All::GetRow('et_facility_type', 'id', $id);
        $lkp_location = $eft->fk_lkp_location;
        $ef = $eft->fk_et_facility;
        $efo = $ef;

        if ($date->isWeekend()) {
            $day_cat = 2;
        } else {
            $day_cat = 1;
        }

        if(in_array($ef, [1,2])){   // [Semua Presint] Fasiliti Dewan & Badminton bertindih
            $ef = [1,2];            
        } else if (in_array($ef, [22,23,24])) {
            if($lkp_location == 6){ // [Presint 11] Fasiliti Bola Tampar, Bola Keranjang & Futsal bertindih
                $ef = [22,23,24];   
            } else {                // Bukan Presint 11, tiada tindihan lokasi
                $ef = [$id];        
            }
        } else {
            $ef = [$id]; 
        }
        $ef = implode(',', $ef);

        $query = DB::select(DB::raw('
            SELECT DISTINCT 
                a.eft_slot_per_user,
                b.id AS "fidd",
                b.efd_name, 
                c.efp_status, 
                c.efp_unit_price AS "harga", 
                c.fk_lkp_gst_rate AS "gst", 
                c.id AS "cidd", 
                d.est_slot_time, 
                d.id AS "fkst", 
                d.start, 
                d.fk_lkp_slot_cat AS "sesi", 
                b.deleted_at
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = '.$id.' '.($et_func !== null ? 'AND c.fk_et_function = '.$et_func : '').' AND c.efp_day_cat = '.$day_cat.' AND b.deleted_at IS NULL AND c.deleted_at IS NULL AND a.fk_et_facility = '.$efo.' AND a.fk_lkp_location = '.$lkp_location.'
                AND (d.id, b.id) NOT IN (
                    SELECT f1.id AS "aaa", g1.id AS "bbb"
                    FROM et_sport_time AS a1
                    JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                    JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                    JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                    JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                    JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                    JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                    JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                    WHERE b1.esb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_et_facility IN ('.$ef.') AND h1.fk_lkp_location = '.$lkp_location.'
                    UNION
                    SELECT a2.fk_et_slot_time AS "aaa", b2.id AS "bbb"
                    FROM et_confirm_booking_detail AS a2
                    JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                    JOIN et_facility_type AS c2 ON c2.id = b2.fk_et_facility_type
                    WHERE a2.ecbd_date_booking = "'.$data.'" AND a2.fk_et_facility_type IN ('.$id.') AND c2.fk_lkp_location = '.$lkp_location.' AND a2.deleted_at IS NULL
                    UNION
                    SELECT a1.fk_et_slot_time AS "aaa", g1.id AS "bbb"
                    FROM et_hall_time AS a1
                    JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
                    JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                    JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                    JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                    JOIN et_facility_type AS h1 ON h1.id = g1.fk_et_facility_type
                    WHERE b1.ehb_booking_date = "'.$data.'" AND h1.fk_et_facility IN ('.$ef.') AND d1.deleted_at IS NULL AND a1.deleted_at IS NULL AND d1.fk_lkp_status IN (5,11,2) AND h1.fk_lkp_location = '.$lkp_location.'
                    UNION
                    SELECT e3.fk_et_slot_time AS "aaa", b3.fk_et_facility_detail AS "bbb"
                    FROM main_booking AS a3
                    JOIN et_booking_facility AS b3 ON b3.fk_main_booking = a3.id
                    JOIN et_sport_book AS c3 ON c3.fk_et_booking_facility = b3.id
                    JOIN et_sport_time AS d3 ON d3.fk_et_sport_book = c3.id
                    JOIN et_slot_price AS e3 ON e3.id = d3.fk_et_slot_price
                    WHERE a3.id = :mbid
                    AND b3.ebf_start_date = :data
                )
        '), ['mbid' => $mbid, 'data' => $data]);
        // dd($query, $data);
        if(Session::get('user.id') == 61894){
        }

        return $query;
    }

    public function SportByEfdDate($id){
        
        // $query = DB::select(DB::raw('
        //     SELECT a.id AS ebf_id, a.fk_et_facility_type, c.*, d.fk_et_sport_book, d.fk_et_slot_price
        //     FROM et_booking_facility AS a
        //     JOIN main_booking AS b ON b.id = a.fk_main_booking
        //     JOIN et_sport_book AS c ON c.fk_et_booking_facility = a.id
        //     JOIN et_sport_time AS d ON d.fk_et_sport_book = c.id
        //     WHERE b.id = :id
        // '), ['id' => $id]);

        // $result = [];
        // foreach ($query as $row) {
        //     $ebf_id = $row->ebf_id;
        //     unset($row->ebf_id); // Remove the ebf_id column
        
        //     if (!isset($result[$ebf_id])) {
        //         $result[$ebf_id] = [];
        //     }
        //     $result[$ebf_id][] = $row;
        // }
        // $query = $result;

        $query = DB::select(DB::raw('
            SELECT a.*, c.*, d.fk_et_sport_book, d.fk_et_slot_price
            FROM et_booking_facility AS a
            JOIN main_booking AS b ON b.id = a.fk_main_booking
            JOIN et_sport_book AS c ON c.fk_et_booking_facility = a.id
            JOIN et_sport_time AS d ON d.fk_et_sport_book = c.id
            WHERE b.id = :id
        '), ['id' => $id]);

        return $query;
    }

    public function slotexist($ebf_ett, $ebf_etd, $esb_booking_date, $est_esp_array){
        $est_esp_array = $est_esp_array->all();
        $est_esp_string = implode(',', $est_esp_array);
        $query = DB::select(DB::raw("
            SELECT *
            FROM main_booking AS a
            JOIN et_booking_facility AS b ON b.fk_main_booking = a.id
            JOIN et_sport_book AS c ON c.fk_et_booking_facility = b.id
            JOIN et_sport_time AS d ON d.fk_et_sport_book = c.id
            WHERE a.fk_lkp_status IN (2, 5)
            AND b.fk_et_facility_type = $ebf_ett
            AND b.fk_et_facility_detail = $ebf_etd
            AND c.esb_booking_date = '$esb_booking_date'
            AND d.fk_et_slot_price IN ($est_esp_string)
        "));
        
        return $query;
    }

    public function bookedPolicy_ThreeOrMore($id, $date){
        $monthBefore = date('Y-m-d', strtotime('-1 month', strtotime($date)));
        $monthAfter = date('Y-m-d', strtotime('+1 month', strtotime($date)));
        $query = DB::select(DB::raw("
            SELECT a.id, a.bmb_booking_no, a.fk_lkp_status, c.esb_booking_date, c.id AS esbid
            FROM main_booking AS a
            JOIN et_booking_facility AS b ON b.fk_main_booking = a.id
            JOIN et_sport_book AS c ON c.fk_et_booking_facility = b.id
            JOIN et_sport_time AS d ON d.fk_et_sport_book = c.id
            WHERE a.fk_users = $id
            AND a.fk_lkp_status = 5
            AND c.esb_booking_date <= '$monthAfter'
            AND c.esb_booking_date >= '$date'
        "));

        return count($query);
        // AND c.esb_booking_date >= '$monthBefore'
        // AND c.esb_booking_date <= '$date'
        // AND c.esb_booking_date <= '$monthAfter'
        // AND c.esb_booking_date >= '$date'
    }

    public function getDayIndicator($date, $fkett){
        $daynum = date("w", strtotime($date));
        if (($daynum == 0) || ($daynum == 6)) {
            $checkaltcutislot = DB::table('et_facility_foc')->where('fk_et_facility_type','=',$fkett)
                                    ->where('day_num','=',$daynum)
                                    ->first();
            if($checkaltcutislot == null){
                $day = 2;
            } else {
                $day = 4;
            }
        } else {
            $checkcuti = DB::table('lkp_holiday')->where('lh_start_date', '=', $date)->first();
            if ($checkcuti == null) {
                $checkaltslot = DB::table('et_facility_foc')->where('fk_et_facility_type','=',$fkett)
                                    ->where('day_num','=',$daynum)
                                    ->first();
                if($checkaltslot == null){
                    $day = 1;
                } else {
                    $day = 3;
                }                
            } else {
                $day = 2;
            }
        }
        return $day;
    }

    public function slotdewan_bertindih($id, $date){
        $date = date("Y-m-d", strtotime($date));
        $fid = $id;
        $day = Sport::getDayIndicator($date,$fid);
        $type = DB::table('et_facility_type')->where('id', $id)->first()->fk_et_facility;
        // dd($id, $date, $day, $type);
        // $efid = DB::table('et_facility_type')
            // ->join('et_facility', 'et_facility_type.fk_et_facility', '=', 'et_facility.id')
            // ->where('et_facility_type.id', $id) // Add any additional conditions if needed
            // ->select('et_facility.id')
            // ->first();
            // $et_function_exist = DB::table('et_function_detail')->where('fk_et_facility', $efid->id)->where('fk_et_function', 4)->get();
            // if($et_function_exist) {
            //     $id = $efid->id;
            // }
            // if ($type == 9 || $type == 14 || $type == 28) {
                
        // }
        $sqlQuery = "SELECT DISTINCT c.efp_day_cat, b.id AS fidd, b.efd_name, d.est_slot_time,d.start, d.id AS fkst, d.fk_lkp_slot_cat AS sesi, c.fk_lkp_gst_rate AS gst
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = $fid AND efd_status = 1 AND c.efp_day_cat = $day
                AND (d.id, b.efd_name) NOT IN (
                SELECT f1.id AS aaa, g1.efd_name AS bbb
                FROM et_hall_time AS a1
                JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
                JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                JOIN et_slot_time AS f1 ON f1.id = a1.fk_et_slot_time
                JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                WHERE b1.ehb_booking_date = '$date' AND c1.fk_et_facility_type = $fid AND d1.fk_lkp_status IN (5,11) AND g1.fk_et_facility_type = $fid
                UNION
                SELECT a2.fk_et_slot_time AS aaa, b2.efd_name AS bbb
                FROM et_confirm_booking_detail AS a2
                JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                WHERE a2.ecbd_date_booking = '$date' AND b2.fk_et_facility_type = $fid
            )
            ORDER BY d.id
            LIMIT 15";

        // Execute the query
        $query = DB::select(DB::raw($sqlQuery));
        
        return $query;
    }

    public function slotdewan($id, $date){
        $date = date("Y-m-d", strtotime($date));
        $fid = $id;
        $day = Sport::getDayIndicator($date,$fid);
        $type = DB::table('et_facility_type')->where('id', $id)->first()->fk_et_facility;
        // dd($id, $date, $day, $type);
        // $efid = DB::table('et_facility_type')
            // ->join('et_facility', 'et_facility_type.fk_et_facility', '=', 'et_facility.id')
            // ->where('et_facility_type.id', $id) // Add any additional conditions if needed
            // ->select('et_facility.id')
            // ->first();
            // $et_function_exist = DB::table('et_function_detail')->where('fk_et_facility', $efid->id)->where('fk_et_function', 4)->get();
            // if($et_function_exist) {
            //     $id = $efid->id;
            // }
            // if ($type == 9 || $type == 14 || $type == 28) {
                
        // }
        $sqlQuery = "SELECT DISTINCT c.efp_day_cat, b.id AS fidd, b.efd_name, d.est_slot_time,d.start, d.id AS fkst, d.fk_lkp_slot_cat AS sesi, c.efp_unit_price AS harga, c.fk_lkp_gst_rate AS gst, c.id AS cidd
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = $fid AND efd_status = 1 AND c.efp_day_cat = $day
                AND (d.id, b.efd_name) NOT IN (
                SELECT f1.id AS aaa, g1.efd_name AS bbb
                FROM et_hall_time AS a1
                JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
                JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                JOIN et_slot_time AS f1 ON f1.id = a1.fk_et_slot_time
                JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                WHERE b1.ehb_booking_date = '$date' AND c1.fk_et_facility_type = $fid AND d1.fk_lkp_status IN (5,11) AND g1.fk_et_facility_type = $fid
                UNION
                SELECT a2.fk_et_slot_time AS aaa, b2.efd_name AS bbb
                FROM et_confirm_booking_detail AS a2
                JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                WHERE a2.ecbd_date_booking = '$date' AND b2.fk_et_facility_type = $fid
            )
            LIMIT 15";

        // Execute the query
        $query = DB::select(DB::raw($sqlQuery));
        
        return $query;
    }

    public function takwim($id){
        $query = DB::select(DB::raw('
        SELECT est_slot_time, efd_name, esb_booking_date
        FROM et_sport_time AS a1
        JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
        JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
        JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
        JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
        JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
        JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
        WHERE c1.fk_et_facility_type = '.$id.' AND d1.fk_lkp_status IN (5,11) AND g1.fk_et_facility_type = '.$id.'
        '));

        return $query;
    }

    public function duplicate($id, $data){
        $query = DB::select(DB::raw('
                SELECT 
                c1.id AS ebfid,
                b1.esb_booking_date, 
                c1.fk_et_facility_type,
                d1.id as bmid,
                d1.fk_users,
                d1.bmb_booking_no, 
                d1.fk_lkp_location,
                est_slot_time, 
                efd_name
            FROM et_sport_time AS a1
            JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
            JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
            JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
            JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
            JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
            JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
            WHERE 
                b1.esb_booking_date = "'.$data.'" 
                AND c1.fk_et_facility_type = '.$id.' 
                AND d1.fk_lkp_status IN (5,11) 
                AND g1.fk_et_facility_type = '.$id.'
                AND (est_slot_time, efd_name) IN (
                    SELECT est_slot_time, efd_name
                    FROM (
                        SELECT 
                            est_slot_time, 
                            efd_name, 
                            COUNT(*) AS esttotal
                        FROM et_sport_time AS a2
                        JOIN et_sport_book AS b2 ON b2.id = a2.fk_et_sport_book
                        JOIN et_booking_facility AS c2 ON c2.id = b2.fk_et_booking_facility
                        JOIN main_booking AS d2 ON d2.id = c2.fk_main_booking
                        JOIN et_slot_price AS e2 ON e2.id = a2.fk_et_slot_price
                        JOIN et_slot_time AS f2 ON f2.id = e2.fk_et_slot_time
                        JOIN et_facility_detail AS g2 ON g2.id = c2.fk_et_facility_detail
                        WHERE
                            b2.esb_booking_date = "'.$data.'" 
                            AND c2.fk_et_facility_type = '.$id.' 
                            AND d2.fk_lkp_status IN (5,11)
                        GROUP BY est_slot_time, efd_name
                        HAVING COUNT(*) > 1
                    ) AS duplicates
                )
        '));
        // dd($query);

        return $query;
    }

    public function cariantempahan($location, $tarikhfrom, $tarikhto, $type = null, $typefacility) {
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month = date('t-m-Y');

        $defaultrange = [date('Y-m-d', strtotime($first_day_this_month)), date('Y-m-d', strtotime($last_day_this_month)) ];
        
        $status = ['4', '5', '11' ,'15', '9'];
        $fk_main_booking_location = MainBooking::whereIn('fk_lkp_status', $status)->where('fk_lkp_location', '=', $location)->pluck('id');
        //$fk_main_booking_all = MainBooking::whereIn('fk_lkp_status', $status)->where('fk_lkp_location', '<>', 1)->pluck('id');
	    $fk_main_booking_all = MainBooking::whereIn('fk_lkp_status', $status)->where('fk_lkp_location', '=', 4)->pluck('id');
        
        if (isset($location) || isset($tarikhfrom) || isset($tarikhto) || isset($typefacility)) {
            if ($location == '' && $tarikhfrom == '' && $tarikhto == ''):
                if ($typefacility == '1') {
                    // dd('1');
                    // et_hall_book.ehb_total as amaun,
                    // $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                    $data = DB::table('main_booking')->join('et_booking_facility', 'et_booking_facility.fk_main_booking', '=', 'main_booking.id')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                        main_booking.bmb_booking_no,
                        main_booking.internal_indi,
                        main_booking.bmb_total_book_hall as amaun,
                        et_facility_detail.efd_name,
                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                        et_booking_facility_detail.ebfd_address as ebfd_address,
                        et_hall_book.ehb_booking_date AS booking_date,
                        et_hall_book.ehb_total_hour AS eqp,
                        et_hall_book.id AS hallbookid,
                        et_hall_book.fk_et_function as function,
                        et_booking_facility.id,
                        et_booking_facility.ebf_facility_indi,
                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_hall_book.ehb_booking_date', $defaultrange)->whereBetween('et_hall_book.ehb_booking_date', $defaultrange)
                        // ->orderBy('bmb_booking_no','asc')
                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                } elseif ($typefacility == '3') {
                    // dd('3');
                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                        main_booking.bmb_booking_no,
                        main_booking.internal_indi,
                        et_facility_detail.efd_name,
                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                        et_booking_facility_detail.ebfd_address as ebfd_address,
                        et_equipment_book.eeb_booking_date AS booking_date,
                        et_equipment_book.deleted_at AS eqp,
                        et_equipment_book.id AS hallbookid,
                        et_equipment_book.eeb_total as amaun,
                        et_equipment_book.fk_et_function as function,
                        et_booking_facility.id,
                        et_booking_facility.ebf_facility_indi,
                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_equipment_book.eeb_booking_date', $defaultrange)->whereBetween('et_equipment_book.eeb_booking_date', $defaultrange)->where('main_booking.bmb_type_user', 4)
                                // ->orderBy('bmb_booking_no','asc')
                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                } else {
                    // dd('2');
                    // dd('d');
                    $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->selectRaw('user_profiles.bud_name,
                        main_booking.bmb_booking_no,
                        main_booking.internal_indi,
                        et_facility_detail.efd_name,
                        et_sport_book.esb_total_hour AS ebfd_event_name,
                        et_sport_book.esb_booking_date AS booking_date,
                        et_sport_book.esb_total_hour AS eqp,
                        et_sport_book.id AS hallbookid,
                        et_sport_book.esb_total as amaun,
                        et_booking_facility.id,
                        et_booking_facility.ebf_facility_indi, user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_sport_book.esb_booking_date', $defaultrange)->whereBetween('et_sport_book.esb_booking_date', $defaultrange)
                        // ->union($et_hall)
                        // ->orderBy('bmb_booking_no','asc')
                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                        // ->get();
                    
                } else:
                    $range = [date('Y-m-d', strtotime($tarikhfrom)), date('Y-m-d', strtotime($tarikhto)) ];
                    if ($tarikhfrom != '' || $tarikhto != '') {
                        // dd('f');
                        if ($location == '') {
                            // et_hall_book.ehb_total as amaun,
                            if ($tarikhfrom == $tarikhto) {
                                if ($typefacility == 1) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        main_booking.bmb_total_book_hall,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_hall_book.ehb_booking_date AS booking_date,
                                        et_hall_book.ehb_total_hour AS eqp,
                                        et_hall_book.id AS hallbookid,
                                        et_hall_book.ehb_total,
                                        et_hall_book.fk_et_function as function,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_facility_type.fk_et_facility,
                                        et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_hall_book.ehb_booking_date', $range)->groupBy('et_booking_facility.id')
                                    // ->orderBy('bmb_booking_no','asc')
                                    ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } elseif ($typefacility == 3) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_equipment_book.eeb_booking_date AS booking_date,
                                        et_equipment_book.deleted_at AS eqp,
                                        et_equipment_book.id AS hallbookid,
                                        et_equipment_book.eeb_total as amaun,
                                        et_equipment_book.fk_et_function as function,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,
                                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_equipment_book.eeb_booking_date', $range)->where('main_booking.bmb_type_user', 4)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } else {
                                    $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_sport_book.esb_total_hour AS ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_sport_book.esb_booking_date AS booking_date,
                                        et_sport_book.esb_total_hour AS eqp,
                                        et_sport_book.id AS hallbookid,
                                        et_sport_book.esb_total as amaun,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_sport_book.esb_booking_date', $range)
                                    // ->union($et_hall)
                                    // ->orderBy('bmb_booking_no','asc')
                                    ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                    // ->get();
                                    
                                }
                            } else {
                                if ($typefacility == 1) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_hall_book.ehb_booking_date AS booking_date,
                                        et_hall_book.ehb_total_hour AS eqp,
                                        et_hall_book.id AS hallbookid,
                                        et_hall_book.ehb_total as amaun,
                                        et_hall_book.fk_et_function as function,
                                        et_hall_book.ehb_total,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_hall_book.ehb_booking_date', $range)->whereBetween('et_hall_book.ehb_booking_date', $range)
                                    // ->orderBy('bmb_booking_no','asc')
                                    ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } elseif ($typefacility == 3) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_equipment_book.eeb_booking_date AS booking_date,
                                        et_equipment_book.deleted_at AS eqp,
                                        et_equipment_book.id AS hallbookid,
                                        et_equipment_book.eeb_total as amaun,
                                        et_equipment_book.fk_et_function as function,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,
                                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_equipment_book.eeb_booking_date', $range)->whereBetween('et_equipment_book.eeb_booking_date', $range)->where('main_booking.bmb_type_user', 4)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } else {
                                    $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_sport_book.esb_total_hour AS ebfd_event_name,
                                        et_sport_book.esb_booking_date AS booking_date,
                                        et_sport_book.esb_total_hour AS eqp,
                                        et_sport_book.id AS hallbookid,
                                        et_sport_book.esb_total as amaun,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_all)->whereBetween('et_sport_book.esb_booking_date', $range)->whereBetween('et_sport_book.esb_booking_date', $range)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                        // ->get();
                                    
                                }
                            }
                        } else {
                            // dd('g');
                            if ($tarikhfrom == $tarikhto) {
                                // $query=EtBookingFacality::whereIn('fk_main_booking',$fk_main_booking_location)
                                //                          ->whereBetween('ebf_start_date', $range)
                                //                          ->orderBy('ebf_start_date','asc')->orderBy('id','asc');
                                if ($typefacility == 1) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_hall_book.ehb_booking_date AS booking_date,
                                        et_hall_book.ehb_total_hour AS eqp,
                                        et_hall_book.id AS hallbookid,
                                        et_hall_book.ehb_total as amaun,
                                        et_hall_book.fk_et_function as function,
                                        et_hall_book.ehb_total,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_hall_book.ehb_booking_date', $range)->groupBy('et_booking_facility.id')
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } elseif ($typefacility == 3) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_equipment_book.eeb_booking_date AS booking_date,
                                        et_equipment_book.deleted_at AS eqp,
                                        et_equipment_book.id AS hallbookid,
                                        et_equipment_book.eeb_total as amaun,
                                        et_equipment_book.fk_et_function as function,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,
                                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_equipment_book.eeb_booking_date', $range)->where('main_booking.bmb_type_user', 4)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } else {
                                    $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_sport_book.esb_total_hour AS ebfd_event_name,
                                        et_sport_book.esb_booking_date AS booking_date,
                                        et_sport_book.esb_total_hour AS eqp,
                                        et_sport_book.id AS hallbookid,
                                        et_sport_book.esb_total as amaun,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_sport_book.esb_booking_date', $range)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                        // ->get();
                                    
                                }
                            } else {
                                if ($typefacility == 1) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        main_booking.bmb_total_book_hall as amaun,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_hall_book.ehb_booking_date as booking_date,
                                        et_hall_book.ehb_total_hour as eqp,
                                        et_hall_book.id AS hallbookid,
                                        et_hall_book.fk_et_function as function,
                                        et_hall_book.ehb_total,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_hall_book.ehb_booking_date', $range)->whereBetween('et_hall_book.ehb_booking_date', $range)->groupBy('et_booking_facility.id')
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } elseif ($typefacility == 3) {
                                    $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                        et_booking_facility_detail.ebfd_address as ebfd_address,
                                        et_equipment_book.eeb_booking_date AS booking_date,
                                        et_equipment_book.deleted_at AS eqp,
                                        et_equipment_book.id AS hallbookid,
                                        et_equipment_book.eeb_total as amaun,
                                        et_equipment_book.fk_et_function as function,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,
                                        user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_equipment_book.eeb_booking_date', $range)->whereBetween('et_equipment_book.eeb_booking_date', $range)->where('main_booking.bmb_type_user', 4)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                } else {
                                    $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->selectRaw('user_profiles.bud_name,
                                        main_booking.bmb_booking_no,
                                        main_booking.internal_indi,
                                        et_facility_detail.efd_name,
                                        et_sport_book.esb_total_hour as ebfd_event_name,
                                        et_sport_book.esb_booking_date as booking_date,
                                        et_sport_book.esb_total_hour as eqp,
                                        et_sport_book.id AS hallbookid,
                                        et_sport_book.esb_total as amaun,
                                        et_booking_facility.id,
                                        et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_sport_book.esb_booking_date', $range)->whereBetween('et_sport_book.esb_booking_date', $range)
                                        // ->union($et_hall)
                                        // ->orderBy('bmb_booking_no','asc')
                                        ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                                        // ->get();
                                    
                                }
                            }
                        }
                    } else {
                        // dd('i');
                        if ($typefacility == 1) {
                            $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_hall_book', 'et_hall_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                                main_booking.bmb_booking_no,
                                main_booking.internal_indi,
                                et_facility_detail.efd_name,
                                et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                                et_booking_facility_detail.ebfd_address as ebfd_address,
                                et_hall_book.ehb_booking_date AS booking_date,
                                et_hall_book.ehb_total_hour AS eqp,
                                et_hall_book.id AS hallbookid,
                                et_hall_book.ehb_total as amaun,
                                et_hall_book.fk_et_function as function,
                                et_hall_book.ehb_total,
                                et_booking_facility.id,
                                et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_hall_book.ehb_booking_date', $defaultrange)->whereBetween('et_hall_book.ehb_booking_date', $defaultrange)->groupBy('et_booking_facility.id')
                                // ->orderBy('bmb_booking_no','asc')
                                ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                        } elseif ($typefacility == 3) {
                            $data = DB::table('et_booking_facility')->join('et_booking_facility_detail', 'et_booking_facility_detail.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('et_equipment_book', 'et_equipment_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->join('et_facility_type', 'et_facility_type.id', '=', 'et_booking_facility.fk_et_facility_type')->selectRaw('user_profiles.bud_name,
                            main_booking.bmb_booking_no,
                            main_booking.internal_indi,
                            et_facility_detail.efd_name,
                            et_booking_facility_detail.ebfd_event_name as ebfd_event_name,
                            et_booking_facility_detail.ebfd_address as ebfd_address,
                            et_equipment_book.eeb_booking_date AS booking_date,
                            et_equipment_book.deleted_at AS eqp,
                            et_equipment_book.id AS hallbookid,
                            et_equipment_book.eeb_total as amaun,
                            et_equipment_book.fk_et_function as function,
                            et_booking_facility.id,
                            et_booking_facility.ebf_facility_indi,
                            user_profiles.bud_phone_no,et_facility_type.fk_et_facility,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_equipment_book.eeb_booking_date', $defaultrange)->whereBetween('et_equipment_book.eeb_booking_date', $defaultrange)->where('main_booking.bmb_type_user', 4)
                            // ->union($et_hall)
                            // ->orderBy('bmb_booking_no','asc')
                            ->groupBy('et_booking_facility.id')->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                        } else {
                            $data = DB::table('et_booking_facility')->join('et_sport_book', 'et_sport_book.fk_et_booking_facility', '=', 'et_booking_facility.id')->join('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')->join('et_facility_detail', 'et_facility_detail.id', '=', 'et_booking_facility.fk_et_facility_detail')->selectRaw('user_profiles.bud_name,main_booking.bmb_booking_no,
                            et_facility_detail.efd_name,
                            et_sport_book.esb_total_hour AS ebfd_event_name,
                            et_sport_book.esb_booking_date AS booking_date,
                            et_sport_book.esb_total_hour AS eqp,
                            et_sport_book.id AS hallbookid,
                            et_sport_book.esb_total as amaun,
                            et_booking_facility.id,
                            et_booking_facility.ebf_facility_indi,user_profiles.bud_phone_no,et_booking_facility.fk_et_facility_detail')->whereIn('et_booking_facility.fk_main_booking', $fk_main_booking_location)->whereBetween('et_sport_book.esb_booking_date', $defaultrange)->whereBetween('et_sport_book.esb_booking_date', $defaultrange)
                            // ->union($et_hall)
                            // ->orderBy('bmb_booking_no','asc')
                            ->orderBy('booking_date', 'asc')->orderBy('fk_et_facility_detail', 'asc');
                            // ->get();
                            
                        }
                    }
                endif;
            }
            // dd($data->get(), $typefacility);
            // dd('0', $data->get());
            //////////////////////////////////////////////
            // if($type==1){
            //   $page = Input::get('page', 1);
            //   $paginate = 20;
            //   $offSet = ($page * $paginate) - $paginate;
            //   $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
            //   $results = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page);
            //         return $results;
            //          }else{
            //           $results = $data;
            //           return $results;
            //         }
            //dd('j');
            if ($type == 1) {
                return $data->paginate(20);
            } else {
                return $data->get();
            }
    }

    public function cariantempahan1($location,$tarikhfrom,$tarikhto,$type){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');

        $defaultrange = [date('Y-m-d',strtotime($first_day_this_month)), date('Y-m-d',strtotime($last_day_this_month))];
     
        $status=['1','2','10'];
        $fk_main_booking=DB::table('main_booking')->whereNotIn('fk_lkp_status',$status)
            ->where('fk_lkp_location','=',1)
            ->pluck('id');

        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location=='' && $tarikhfrom=='' && $tarikhto==''):
                $query=BhBooking::whereIn('fk_main_booking',$fk_main_booking)
                            ->whereBetween('bb_start_date', $defaultrange)
                            ->whereBetween('bb_end_date', $defaultrange)
                            ->orderBy('bb_start_date','asc');
                if($type==1){
                    return $query->paginate(20);
                }else{
                    return $query->get();
                }
            else:
                $range = [date('Y-m-d',strtotime($tarikhfrom)), date('Y-m-d',strtotime($tarikhto))];
        
                $bh_hall=DB::table('bh_hall')->where('fk_lkp_location','=',$location)
                        ->pluck('id');

                if($tarikhfrom!='' ||$tarikhto!=''){
                    if($location==''){
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                        -> whereBetween('bb_start_date', $range)
                                        ->orderBy('bb_start_date','asc');
                            if($type==1){
                                return $query->paginate(20);
                            }else{
                                return $query->get();
                            }
                        }else{
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                        -> whereBetween('bb_start_date', $range)
                                        -> whereBetween('bb_end_date', $range)
                                        ->orderBy('bb_start_date','asc');
                            if($type==1){
                                return $query->paginate(20);
                            }else{
                                return $query->get();
                            }
                        }
                    }else{
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                ->whereIn('fk_bh_hall',$bh_hall)
                                ->whereBetween('bb_start_date', $range)
                                ->orderBy('bb_start_date','asc');    

                            if($type==1){
                                return $query->paginate(20);
                            }else{
                                return $query->get();
                            }
                        }else{
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                                ->whereIn('fk_bh_hall',$bh_hall)
                                                ->whereBetween('bb_start_date', $range)
                                                ->whereBetween('bb_end_date', $range)
                                                ->orderBy('bb_start_date','asc');

                            if($type==1){
                                return $query->paginate(20);
                            }else{
                                return $query->get();
                            }
                        }
                    }
                }else{
                    $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                        ->whereIn('fk_bh_hall',$bh_hall)
                        ->orderBy('bb_start_date','asc') ; 
                    if($type==1){
                        return $query->paginate(20);
                    }else{
                        return $query->get();
                    }
                }
            endif;
        }else{
            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                        ->whereBetween('bb_start_date', $defaultrange)
                        ->whereBetween('bb_end_date', $defaultrange)
                        ->orderBy('bb_start_date','asc');
            if($type==1){
                return $query->paginate(20);
            }else{
                return $query->get();
            }
        }
    }

    public function laporan_sukan($id, $data, $data1){
        $query = DB::select(DB::raw('
            SELECT DISTINCT d1.bmb_booking_no ,c1.*,h1.fullname, h1.email, i1.bud_reference_id, i1.bud_phone_no, g1.efd_name
            FROM et_sport_time AS a1
            JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
            JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
            JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
            JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
            JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
            JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
            JOIN users AS h1 ON h1.id = d1.fk_users
            JOIN user_profiles AS i1 ON i1.fk_users = h1.id
            WHERE d1.fk_lkp_location = '.$id.' AND d1.fk_lkp_status IN (5,11) AND b1.esb_booking_date BETWEEN "'.$data.'" AND "'.$data1.'"
            ORDER BY c1.ebf_start_date DESC
        '));

        return $query;
    }

    public function laporan_umum($id, $data, $data1){
        $query = DB::select(DB::raw('
            SELECT a1.*,b1.*,c1.*,f1.*
            FROM et_sport_time AS a1
            JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
            JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
            JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
            JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
            JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
            JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
            JOIN users AS h1 ON h1.id = d1.fk_users
            JOIN user_profiles AS i1 ON i1.fk_users = h1.id
            WHERE d1.fk_lkp_location = '.$id.' AND d1.fk_lkp_status IN (5,11) AND b1.esb_booking_date BETWEEN "'.$data.'" AND "'.$data1.'"
        '));

        return $query;
    }

    public function duplicate_all(){
        $query = DB::select(DB::raw('
            SELECT d1.* ,COUNT(est_slot_time) AS total, est_slot_time, c1.fk_et_facility_type, b1.esb_booking_date, efd_name, d1.fk_lkp_location
            FROM et_sport_time AS a1
            JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
            JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
            JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
            JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
            JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
            JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
            WHERE d1.fk_lkp_status IN (5,11)
            AND a1.deleted_at IS NULL
            AND b1.deleted_at IS NULL
            AND e1.deleted_at IS NULL
            GROUP BY est_slot_time
            HAVING COUNT(est_slot_time) > 1;
        '));

        // $query = DB::select(DB::raw('
        //     SELECT 
        //         ecbd.fk_et_facility_detail,
        //         ecbd.fk_et_slot_time,
        //         ecbd.fk_et_facility_type,
        //         ecbd.ecbd_date_booking,
        //         COUNT(DISTINCT mb.id) AS duplicate_count,
        //         GROUP_CONCAT(DISTINCT mb.bmb_booking_no ORDER BY mb.bmb_booking_no ASC SEPARATOR ", ") AS duplicate_mb_ids,
        //         GROUP_CONCAT(DISTINCT ecbd.id ORDER BY ecbd.id ASC SEPARATOR ", ") AS duplicate_confirm_booking_ids
        //     FROM 
        //         et_confirm_booking_detail AS ecbd
        //     JOIN 
        //         et_confirm_booking AS ecb ON ecb.id = ecbd.fk_et_confirm_booking
        //     JOIN 
        //         main_booking AS mb ON mb.id = ecb.fk_main_booking
        //     WHERE 
        //         mb.id = ecb.fk_main_booking
        //         AND ecb.id = ecbd.fk_et_confirm_booking
        //         AND mb.fk_lkp_status IN (5,11)
        //         AND ecbd.fk_et_facility_detail IS NOT NULL
        //         AND ecbd.fk_et_slot_time IS NOT NULL
        //         AND ecbd.fk_et_facility_type IS NOT NULL
        //         AND ecbd.ecbd_date_booking IS NOT NULL
        //         AND ecbd.deleted_at IS NULL
        //         AND ecbd.ecbd_date_booking != "1970-01-01"
        //     GROUP BY 
        //         ecbd.fk_et_facility_detail,
        //         ecbd.fk_et_slot_time,
        //         ecbd.fk_et_facility_type,
        //         ecbd.ecbd_date_booking
        //     HAVING 
        //         COUNT(DISTINCT mb.id) > 1
        //     ORDER BY 
        //         ecbd.ecbd_date_booking DESC;
        // '));
        // $results = [];
        // foreach ($query as $row) {
        //     $results[] = $row->duplicate_mb_ids;
        // }
        // $results = array_values(array_unique($results));

        // $query = DB::select(DB::raw('
        //     SELECT 
        //         mb.bmb_booking_no,
        //         ecbd.fk_et_facility_detail,
        //         ecbd.fk_et_slot_time,
        //         ecbd.fk_et_facility_type,
        //         ecbd.ecbd_date_booking,
        //         COUNT(*) AS duplicate_count
        //     FROM 
        //         main_booking AS mb
        //     JOIN 
        //         et_confirm_booking AS ecb ON ecb.fk_main_booking = mb.id
        //     JOIN
        //         et_confirm_booking_detail AS ecbd ON ecbd.fk_et_confirm_booking = ecb.id
        //     WHERE
        //         mb.id = ecb.fk_main_booking
        //         AND ecb.id = ecbd.fk_et_confirm_booking
        //         AND mb.fk_lkp_status IN (5,11)
        //         AND ecbd.fk_et_facility_detail IS NOT NULL
        //         AND ecbd.fk_et_slot_time IS NOT NULL
        //         AND ecbd.fk_et_facility_type IS NOT NULL
        //         AND ecbd.ecbd_date_booking IS NOT NULL
        //         AND ecbd.deleted_at IS NULL
        //         AND ecbd.ecbd_date_booking != "1970-01-01"
        // '));

        // dd($query);

        return $query;
    }

    public function laporan_keseluruhanEloquent(){
        $totalRecords = DB::table('main_booking')
            ->whereIn('fk_lkp_status', [2,3,4,5,6,9,10,12,11])
            ->whereNull('internal_indi')
            ->whereNull('deleted_at')
            ->count();

        $perPage = 10; 

        $totalPages = ceil($totalRecords / $perPage);
        
        $page = request()->query('page', 1); 
        
        $records = DB::table('main_booking')
            ->offset(($page - 1) * $perPage)
            ->limit($perPage) 
            ->get();
            
        return [
            'records' => $records,
            'totalPages' => $totalPages,
            'currentPage' => $page,
            'totalRecords' => $totalRecords,
        ];
    }

    public function laporan_keseluruhan()
    {
        $year = date('Y');
        $month = date('m');
        $yearsAgo = $year - 2;
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status != 11 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));
        $query = DB::select(DB::raw("
            SELECT * 
            FROM main_booking 
            WHERE fk_lkp_status IN (2,3,4,5,6,9,10,11,12)
            AND internal_indi IS NULL
            AND deleted_at IS NULL
            AND created_at >= DATE_SUB(NOW(), INTERVAL 2 YEAR)
            ORDER BY id DESC
        "));
        // AND YEAR(bmb_booking_date) BETWEEN $yearsAgo AND $year
        // $query = DB::table('main_booking')
        //     ->where('fk_lkp_status', '!=', 1)
        //     ->paginate(200);

        return $query;
    }

    public function laporan_top_pengguna()
    {
        $query = DB::select(DB::raw('
            SELECT COUNT(*) AS total, fullname
            FROM users AS a
            JOIN main_booking AS b ON b.fk_users = a.id
            WHERE fk_lkp_status IN (5) 
            GROUP BY fullname ORDER BY total DESC LIMIT 10
        '));

        return $query;
    }

    public function laporan_top_tempahan()
    {
        $query = DB::select(DB::raw('
        SELECT COUNT(*) AS total, eft_type_desc, lc_description
        FROM et_booking_facility AS a
        JOIN main_booking AS b ON b.id = a.fk_main_booking
        JOIN et_facility_type AS c ON c.id = a.fk_et_facility_type
        JOIN lkp_location AS d ON d.id = c.fk_lkp_location
        GROUP BY fk_et_facility_type ORDER BY total DESC LIMIT 10
        '));

        return $query;
    }

    public function laporan_dalaman()
    {
        $year = date('Y');
        $month = date('m');
        $query = DB::select(DB::raw('
            SELECT * FROM main_booking WHERE internal_indi IN (1)
        '));
        // SELECT * FROM main_booking WHERE fk_lkp_status = 11 // Dalaman Selesai

        return $query;
    }

    public function laporan_bayaran()
    {
        $year = date('Y');
        $yearsAgo = $year - 2;
        // SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        $query = DB::select(DB::raw('
            SELECT * FROM main_booking WHERE fk_lkp_status IN (2, 3, 4) AND YEAR(bmb_booking_date) BETWEEN '.$yearsAgo.' AND '. $year .' ORDER BY id DESC
        '));

        return $query;
    }

    public function laporan_bayaran2($id, $data, $data1){
        $query = DB::select(DB::raw('
            SELECT DISTINCT b.id AS "fidd",b.efd_name,d.est_slot_time,d.id AS "fkst", d.fk_lkp_slot_cat AS "sesi", c.efp_unit_price AS "harga", c.fk_lkp_gst_rate AS "gst", c.id AS "cidd", d.start
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            
        '));

        return $query;
    }

    public function laporan_kelulusan()
    {
        $year = date('Y');
        $month = date('m');
        $query = DB::select(DB::raw('
            SELECT * FROM main_booking WHERE fk_lkp_status = 13 AND YEAR(bmb_booking_date) = '.$year.' 
        '));

        return $query;
    }

    public function laporan_harian()
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        $query = DB::table('main_booking')
            ->whereDate('bmb_booking_date', $currentDate)
            ->where('fk_lkp_status', '!=', 1)
            ->get();
        return $query;
    }

    public function laporan_user($id)
    {
        // AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        $type = 'SPS%';
        $year = date('Y');
        $month = date('m');
        $query = DB::select(DB::raw("
            SELECT * FROM main_booking WHERE fk_users = $id AND bmb_booking_no LIKE 'SPS%' AND fk_lkp_status != 1 AND deleted_at IS NULL ORDER BY id DESC
        "));

        return $query;
    }
    public function laporan_penggunaan($id){
       
        if(is_numeric($id)){     // For IC-based
            $id = (int)$id;
            $query = DB::select(DB::raw("
                SELECT * 
                FROM audit_trail a
                JOIN main_booking b ON b.fk_users = a.fk_users
                JOIN users c ON c.id = a.fk_users
                JOIN user_profiles d ON d.fk_users = c.id
                WHERE d.bud_reference_id = $id
                AND a.deleted_at IS NULL
                ORDER BY b.id DESC
                LIMIT 2000
            "));
        } else {                // For main_booking number, name
            $mbid = All::GetRow('main_booking', 'bmb_booking_no', $id);
            if(!$mbid) {        // user name
                $query = DB::select(DB::raw("
                    SELECT * 
                    FROM users a
                    JOIN main_booking b ON b.fk_users = a.id
                    JOIN audit_trail c ON a.id = c.fk_users
                    WHERE a.fullname LIKE '%$id%'
                    OR b.id LIKE '$mbid'
                    LIMIT 1000
                "));
            } else {            // main_booking.id
                $mbid = $mbid->id;
                $query = DB::select(DB::raw("
                    SELECT * 
                    FROM audit_trail a
                    JOIN main_booking b ON b.id = a.table_ref_id
                    JOIN users c ON c.id = b.fk_users
                    WHERE b.id LIKE '$mbid'
                    AND a.deleted_at IS NULL
                "));
            }
        }
        // dd($id, $query);
        return $query;
    }

    public function dewan_sukan_rumusan($id)
    {
        $data = DB::select(DB::raw(
            "SELECT DISTINCT
                    a.id AS 'mbid',
                    b.id AS 'ebfid',
                    -- c.id AS 'ebfdid',
                    d.id AS 'eqbid',
                    e.id AS 'ehbid',
                    f.id AS 'ehtid',
                    a.fk_lkp_location,
                    b.fk_et_facility_detail,
                    b.ebf_start_date AS 'start',
                    b.ebf_end_date AS 'end',
                    e.fk_et_function,
                    f.*
            FROM    main_booking a
                    JOIN et_booking_facility b ON b.fk_main_booking = a.id
                    -- JOIN et_booking_facility_detail c ON c.fk_et_booking_facility = b.id
                    LEFT JOIN et_equipment_book d ON d.fk_et_booking_facility = b.id
                    JOIN et_hall_book e ON e.fk_et_booking_facility = b.id
                    LEFT JOIN et_hall_time f ON f.fk_et_hall_book = e.id
                WHERE   
                    a.deleted_at IS NULL
                    AND b.deleted_at IS NULL
                    -- AND c.deleted_at IS NULL
                    AND d.deleted_at IS NULL
                    AND e.deleted_at IS NULL
                    AND f.deleted_at IS NULL
                    AND a.id = $id
            GROUP BY
                    f.id
        "));
        return $data;
    }

    public function getEquipluar($location) {
        return DB::select(DB::raw(
            "SELECT a.id AS 'eid',
                    b.id AS 'eepid',
                    c.id AS 'efid',
                    a.ee_name AS 'item',
                    a.ee_quantity as 'max',
                    c.ef_desc AS 'kegunaan',
                    b.* 
                    FROM et_equipment a,
                    et_equipment_price b,
                    et_function c 
                    WHERE a.ee_status = '1' 
                    AND a.fk_lkp_location = $location 
                    AND c.id = b.fk_et_function 
                    AND a.deleted_at IS NULL
                    AND b.deleted_at IS NULL
                    AND c.deleted_at IS NULL
                    AND b.eep_day_cat = 1 
                    AND b.fk_et_equipment = a.id 
                    AND b.fk_et_function = 9"));
    }
    public function getEquipdalam($location, $et_function = null, $dayCat = null) {
        $data = DB::select(DB::raw(
            "SELECT a.id AS 'eid',
                    b.id AS 'eepid',
                    c.id AS 'efid',
                    a.ee_name AS 'item',
                    a.ee_quantity as 'max',
                    c.ef_desc AS 'kegunaan',
                    b.* 
                    FROM et_equipment a,
                    et_equipment_price b,
                    et_function c 
                    WHERE a.ee_status = '1' 
                    AND a.fk_lkp_location = $location 
                    AND c.id = b.fk_et_function 
                    AND a.deleted_at IS NULL 
                    AND b.deleted_at IS NULL 
                    AND c.deleted_at IS NULL
                    AND b.eep_day_cat = 1 
                    AND b.fk_et_equipment = a.id 
                    AND b.fk_et_function = 8
                    "));
        return $data;
    }

    public function getAvailableDewanList($sdate, $edate, $loc) {
        // $data = DB::table('et_confirm_booking')
        //     ->whereDate('ecb_date_booking', '>=', $sdate)
        //     ->whereDate('ecb_date_booking', '<=', $edate)
        //     ->whereDate('ecb_flag_indicator', 2)
        //     ->groupBy('fk_et_facility_type')
        //     ->get();
        // return $data;
        
        $sdate1 = date("Y-m-d", strtotime($sdate));
        $edate1 = date("Y-m-d", strtotime($edate));
        $confirm = DB::table('et_confirm_booking')->whereDate('ecb_date_booking', '>=', $sdate1)->whereDate('ecb_date_booking', '<=', $edate1)->where('ecb_flag_indicator', 2)->groupBy('fk_et_facility_type')->where('deleted_at', NULL)->get();
        // manipulate date confirm
        // -------------------------------------------------------------------------
        $arrayconfirm = array();
        if ($confirm->count()) {
            foreach ($confirm as $key => $value) {
                array_push($arrayconfirm, $value->fk_et_facility_type);
            } // end foreach
            
        } else {
            array_push($arrayconfirm, 0);
        }
        // -------------------------------------------------------------------------
        $arrayconfirm = implode(",", $arrayconfirm);
        $data = DB::select(DB::raw("SELECT c.id as 'loc','$sdate' as 'sdate','$edate' as 'edate',b.id AS id, b.eft_type_desc as 'nama',c.lc_description as 'lokasi'
            FROM et_facility a,et_facility_type b,lkp_location c
            WHERE b.fk_et_facility = a.id
            and c.id = b.fk_lkp_location
            AND  a.ef_type = 1
            AND b.fk_lkp_location = $loc
            -- AND b.id NOT IN ($arrayconfirm)
            "));
        
        // dd($data, $confirm, $arrayconfirm, $sdate, $edate, $loc);
        return $data;
    }
    public function getBookeddewanlist($sdate, $edate, $loc) {
        $sdate1 = date("Y-m-d", strtotime($sdate));
        $edate1 = date("Y-m-d", strtotime($edate));
        $data = DB::table('et_confirm_booking')->select('et_confirm_booking.*', 'users.fullname', 'et_facility_type.eft_type_desc', 'lkp_location.lc_description', 'main_booking.bmb_booking_no', 'et_booking_facility.ebf_no_of_day')
            ->join('main_booking', 'main_booking.id', '=', 'et_confirm_booking.fk_main_booking')
            ->join('users', 'users.id', '=', 'main_booking.fk_users')
            ->join('et_facility_type', 'et_facility_type.id', '=', 'et_confirm_booking.fk_et_facility_type')
            ->join('lkp_location', 'lkp_location.id', '=', 'et_facility_type.fk_lkp_location')
            ->join('et_booking_facility', function($join) {
                $join->on('et_booking_facility.fk_main_booking', '=', 'main_booking.id')
                     ->on('et_booking_facility.fk_et_facility_type', '=', 'et_confirm_booking.fk_et_facility_type');
            })
            ->where('et_confirm_booking.ecb_date_booking', '>=', $sdate1)
            ->where('et_confirm_booking.ecb_date_booking', '<=', $edate1)
            ->where('et_confirm_booking.ecb_flag_indicator', 2)
            ->where('et_facility_type.fk_lkp_location', $loc)
            ->where('main_booking.fk_lkp_status', '!=', 10)
            ->groupBy('et_confirm_booking.fk_et_facility_type')
            ->get();
    
        // $data = DB::select(DB::raw("SELECT f.ebf_no_of_day,b.bmb_booking_no,b.id,c.fullname,d.eft_type_desc,e.lc_description,d.id as 'fid'
        //     FROM et_confirm_booking a,main_booking b,users c,et_facility_type d,lkp_location e,et_booking_facility f
        //     WHERE a.ecb_date_booking >= '$sdate1' AND a.ecb_date_booking <= '$edate1'
        //     AND f.fk_main_booking = b.id and f.fk_et_facility_type = a.fk_et_facility_type
        //     AND a.ecb_flag_indicator = 2 AND c.id=b.fk_users AND d.fk_lkp_location = $loc AND e.id=d.fk_lkp_location AND d.id = a.fk_et_facility_type AND b.id = a.fk_main_booking GROUP BY a.fk_et_facility_type"));
        return $data;
    }

    public function getId() {
        // $data = LkpSequence::first();
        $data = All::GetRow('lkp_sequence', 'id', 1);

        $no = $data->resit_no;

        if(($no < 999999)){
            $run_no = $no + 1;

            /*-----------update lkpsequence-----------*/
            // $data->resit_no = $run_no;
            // $data->save();
            $data = array(
                'resit_no'          => $run_no,
                'updated_by'        => Session::get('user.id'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            $query = All::GetUpdate('lkp_sequence', 1, $data);
            /*-----------end update lkpsequence-----------*/

        }else{
            $run_no = 1;
            /*-----------update lkpsequence-----------*/
           // $b = new LkpSequence;
            // $data->resit_no = $run_no;
            // $data->save();
            $data = array(
                'resit_no'          => $run_no,
                'updated_by'        => Session::get('user.id'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            $query = All::GetUpdate('lkp_sequence', 1, $data);
            /*-----------end update lkpsequence-----------*/
        }

        while(strlen($run_no) <= 6){
            $run_no = '0' . $run_no;
        }

        return $run_no;

    }
    public function getRn() {
        $running = All::GetRow('lkp_sequence', 'id', 1);
        $rn = $running->booking_no;
        $newcount1 = $rn + 1;
        $newcount = 0;
        if ($newcount1 > 99999) {
            $newcount = 1;
        } else {
            $newcount = $newcount1;
        }
        $data = array(
            'booking_no'        => $newcount,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_sequence', 1, $data);
        return $rn;
    }

    public function priceSlot($id) {
        $query = DB::select(DB::raw('
            SELECT DISTINCT 
                a.fk_et_facility_type, 
                a.fk_et_facility_detail, 
                a.ebf_start_date, 
                c.* 
            FROM et_booking_facility AS a
            JOIN et_sport_book AS b ON b.fk_et_booking_facility = a.id
            JOIN et_sport_time AS c ON c.fk_et_sport_book = b.id
            WHERE a.fk_main_booking = '.$id.'
            AND c.deleted_at is Null
            ORDER BY ebf_start_date ASC
        '));
        return $query;
    }
    public function priceSlot2($id) {
        $query = DB::select(DB::raw('
            SELECT DISTINCT 
            a.fk_et_facility_type, 
            a.fk_et_facility_detail, 
            a.ebf_start_date, 
            c.* 
            FROM et_booking_facility AS a
            JOIN et_hall_book AS b ON b.fk_et_booking_facility = a.id
            JOIN et_hall_time AS c ON c.fk_et_hall_book = b.id
            LEFT JOIN et_slot_price AS d ON d.fk_et_slot_time = c.fk_et_slot_time 
            WHERE a.fk_main_booking = '.$id.'
            AND c.deleted_at is NULL
        '));

        return $query;
    }

    public function priceSlot3($id) {
        $query = DB::select(DB::raw('
            SELECT DISTINCT 
                a.fk_et_facility_type, 
                a.fk_et_facility_detail, 
                a.ebf_start_date, 
                c.*,
                h.bp_total_amount,
                h.bp_paid_amount,
                h.no_lopo,
                h.bp_deposit
            FROM et_booking_facility AS a
            LEFT JOIN et_sport_book AS b ON b.fk_et_booking_facility = a.id
            LEFT JOIN et_sport_time AS c ON c.fk_et_sport_book = b.id
            LEFT JOIN et_hall_book AS d ON d.fk_et_booking_facility = a.id
            LEFT JOIN et_hall_time AS e ON e.fk_et_hall_book = d.id
            LEFT JOIN et_slot_price AS f ON f.fk_et_slot_time = e.fk_et_slot_time
            RIGHT JOIN bh_payment AS h ON h.fk_main_booking = a.fk_main_booking 
            WHERE a.fk_main_booking = '.$id.'
            AND c.deleted_at IS NULL
            AND e.deleted_at IS NULL
        '));
        return $query;
    }

    public function generatetransid() {
        $data = All::GetRowLast('lkp_sequence');
        $no = $data->fpx_serial_trans;
        if (($no < 9999)) {
            $run_no = $no + 1;
            /*-----------update lkpsequence-----------*/
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        } else {
            $run_no = 1;
            /*-----------update lkpsequence-----------*/
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        }
        while (strlen($run_no) <= 3) {
            $run_no = '0' . $run_no;
        }
        return $run_no;
    }
    
    public function generatequono() {
        $data = All::GetRow('lkp_sequence', 'id', 1);
        // $data = LkpSequence::first();
        $no = $data->quotation_no;
        if (($no < 9999)) {
            $run_no = $no + 1;
            /*-----------update lkpsequence-----------*/
            // $data->quotation_no = $run_no;
            // $data->save();
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        } else {
            $run_no = 1;
            /*-----------update lkpsequence-----------*/
            // $b = new LkpSequence;
            // $b->quotation_no = $run_no;
            // $b->save();
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        }
        while (strlen($run_no) <= 4) {
            $run_no = '0' . $run_no;
        }
        return $run_no;
    }
    public function generatereceipt() {
        $latestReceiptNumber = DB::table('bh_payment')
        ->orderBy('id', 'desc')
        ->limit(1)
        ->value('bp_receipt_number');
        $nextReceiptNumber = $latestReceiptNumber + 1;
        return $nextReceiptNumber;
    }

    public function hasil($location, $mode, $start, $end) {
        $query = DB::table(function ($subquery) use ($location, $start, $end) {
            $subquery->select(
                'b.bmb_booking_no',
                'e.fullname',
                'd.efd_name',
                'd.efd_fee_code',
                'a.bp_receipt_date',
                'a.bp_receipt_number',
                'a.bp_payment_ref_no',
                'f.id as paymode',
                'f.lpm_description',
                // DB::raw('SUM(h.est_total) as ebf_subtotal')
                DB::raw('h.est_total as ebf_subtotal')
            )
            ->from('bh_payment as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('et_booking_facility as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('et_facility_detail as d', 'd.id', '=', 'c.fk_et_facility_detail')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            ->whereDate('a.bp_receipt_date', '>=', $start)
            ->whereDate('a.bp_receipt_date', '<=', $end)
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->groupBy('b.bmb_booking_no', 'd.efd_name')
            ->union(
                DB::table('et_payment_fpx as a1')
                ->select(
                    'b1.bmb_booking_no',
                    'e1.fullname',
                    'd1.efd_name',
                    'd1.efd_fee_code',
                    'a1.fpx_trans_date',
                    'b1.sap_no',
                    'i1.fpx_txn_reference as bp_payment_ref_no',
                    'f1.id as paymode',
                    'f1.lpm_description',
                    // DB::raw('SUM(h1.est_total) as ebf_subtotal')
                    DB::raw('h1.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                ->whereDate('a1.fpx_trans_date', '>=', $start)
                ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
            );
        }, 'subquery')
        ->where('paymode', $mode)
        ->get();
        // dd($location, $mode, $start, $end, $query);

        return $query;
    }

    public function usage($location, $start, $end) {
        $query = DB::table(function ($subquery) use ($location, $start, $end) {
            $subquery->select(
                'd.efd_name',
                'd.efd_fee_code',
                'c.ebf_subtotal',
                // DB::raw('h.est_total as ebf_subtotal')
            )
            ->from('bh_payment as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('et_booking_facility as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('et_facility_detail as d', 'd.id', '=', 'c.fk_et_facility_detail')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            ->whereDate('a.bp_receipt_date', '>=', $start)
            ->whereDate('a.bp_receipt_date', '<=', $end)
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->groupBy('b.bmb_booking_no', 'd.efd_name')
            ->union(
                DB::table('et_payment_fpx as a1')
                ->select(
                    'd1.efd_name',
                    'd1.efd_fee_code',
                    'c1.ebf_subtotal',
                    // DB::raw('h.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                ->whereDate('a1.fpx_trans_date', '>=', $start)
                ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
            );
        }, 'subquery')
        // ->where('search_type', $type)
        ->get();
        // dd($query);
        return $query;
    }

    public function usagetest($location, $year, $start, $end, $search_types, $function_types) {
        $query = DB::table(function ($subquery) use ($location, $year, $start, $end) {
            $subquery->select(
                DB::raw('COUNT(id) as bil_hall'),
                'ebf_start_date',
                DB::raw('SUM(ebf_subtotal) as amaun')
            )
            ->from('et_booking_facility as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('bh_payment_fpx as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('et_hall_book as d', 'd.fk_et_booking_facility', '=', 'a.id')
            ->join('et_sport_book as e', 'e.fk_et_booking_facility', '=', 'a.id')
            ->join('user_profiles as f', 'f.fk_users', '=', 'b.fk_users')
            ->join('lkp_status as g', 'g.id', '=', 'b.fk_lkp_status')
            ->join('lkp_location as h', 'h.id', '=', 'b.fk_lkp_location')
            ->join('et_facility_detail as i', 'i.id', '=', 'a.fk_et_facility_detail')
            // ->whereDate('a.ebf_start_date', '>=', $start)
            // ->whereDate('a.ebf_end_date', '<=', $end)
            ->where(DB::raw('MONTH(ebf_start_date)'),'=',$num)
            ->where(DB::raw('YEAR(ebf_start_date)'),'=',$year)
            ->where('b.fk_lkp_location', $location)
            // ->where('b.fk_lkp_status', 5)
            ->where('a.ebf_facility_indi','=',1)
            ->groupBy('a.ebf_start_date')
            ->union(
                DB::table('et_payment_fpx as a1')
                ->select(
                    'd1.efd_name',
                    'd1.efd_fee_code',
                    'c1.ebf_subtotal',
                    // DB::raw('h.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                ->whereDate('a1.fpx_trans_date', '>=', $start)
                ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
            )
            ->union(
                DB::table('bh_payment_fpx as a1')
                ->select(
                    'd1.efd_name',
                    'd1.efd_fee_code',
                    'c1.ebf_subtotal',
                    // DB::raw('h.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                ->whereDate('a1.fpx_trans_date', '>=', $start)
                ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
            );
        }, 'subquery')
        // ->where('search_type', $type)
        ->get();
        // dd($query);
        return $query;
    }

    // Report Penggunaan::Begin
    public function list_booking_facility($lkplocation, $indi) {
        $query = DB::table('et_booking_facility')
                ->leftjoin('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')
                ->leftJoin('et_facility_detail', 'et_booking_facility.fk_et_facility_detail', '=', 'et_facility_detail.id')
                ->leftJoin('et_facility_type', 'et_facility_detail.fk_et_facility_type', '=', 'et_facility_type.id')
                ->selectRaw('DISTINCT(fk_et_facility_detail),et_facility_detail.efd_name')
                ->where('main_booking.fk_lkp_status', 5)
                ->where('et_facility_detail.deleted_at', NULL)
                ->where('et_facility_type.deleted_at', NULL);

        if ($lkplocation == false) { //admin @ super admin @ multi operasi
            if ($indi == 1) {
                return $query->where('et_facility_type.fk_lkp_location', 0)
                                ->pluck('et_facility_detail.efd_name', 'fk_et_facility_detail');
            } else {
                return $query->where('et_facility_type.fk_lkp_location', 0)
                                ->pluck('fk_et_facility_detail');
            }    
        } else {
            if ($indi == 1) {
                return $query->where('et_facility_type.fk_lkp_location', '=', $lkplocation)
                                ->pluck('et_facility_detail.efd_name', 'fk_et_facility_detail');
            } else {
                return $query->where('et_facility_type.fk_lkp_location', '=', $lkplocation)
                                ->pluck('fk_et_facility_detail');
            }
        }
    }
    public function usage1($num, $selectyear, $key, $et_booking_facility)
    {
        $hallQuery = DB::table(function ($subquery) use ($num, $selectyear, $key, $et_booking_facility) {
            $subquery->select(
                DB::raw('COUNT(id) as bil_hall'),
                'ebf_start_date',
                DB::raw('SUM(ebf_subtotal) as amaun')
            )
                ->from('et_booking_facality')
                ->where(DB::raw('MONTH(ebf_start_date)'), '=', $num)
                ->whereIn('fk_et_facility_detail', $et_booking_facility)
                ->where('ebf_facility_indi', '=', 1)
                ->groupBy('ebf_start_date');
        }, 'subquery')
            ->first();

        $sportQuery = DB::table(function ($subquery) use ($num, $selectyear, $key, $et_booking_facility) {
            $subquery->select(
                DB::raw('COUNT(id) as bil_sport'),
                'ebf_start_date',
                DB::raw('SUM(ebf_subtotal) as amaun')
            )
                ->from('et_booking_facality')
                ->where(DB::raw('MONTH(ebf_start_date)'), '=', $num)
                ->whereIn('fk_et_facility_detail', $et_booking_facility)
                ->where('ebf_facility_indi', '=', 2)
                ->groupBy('ebf_start_date');
        }, 'subquery')
            ->first();

        $hallYearQuery = DB::table(function ($subquery) use ($num, $selectyear, $key, $et_booking_facility) {
            $subquery->select(
                DB::raw('COUNT(id) as bil_hall_year'),
                'ebf_start_date',
                DB::raw('SUM(ebf_subtotal) as amaun_year')
            )
                ->from('et_booking_facality')
                ->where(DB::raw('YEAR(ebf_start_date)'), '=', $selectyear)
                ->whereIn('fk_et_facility_detail', $et_booking_facility)
                ->where('ebf_facility_indi', '=', 1)
                ->groupBy('ebf_start_date');
        }, 'subquery')
            ->first();

        $sportYearQuery = DB::table(function ($subquery) use ($num, $selectyear, $key, $et_booking_facility) {
            $subquery->select(
                DB::raw('COUNT(id) as bil_sport_year'),
                'ebf_start_date',
                DB::raw('SUM(ebf_subtotal) as amaun_year')
            )
                ->from('et_booking_facality')
                ->where(DB::raw('YEAR(ebf_start_date)'), '=', $selectyear)
                ->whereIn('fk_et_facility_detail', $et_booking_facility)
                ->where('ebf_facility_indi', '=', 2)
                ->groupBy('ebf_start_date');
        }, 'subquery')
            ->first();

        return [
            'hall' => $hallQuery,
            'sport' => $sportQuery,
            'hall_year' => $hallYearQuery,
            'sport_year' => $sportYearQuery,
        ];
    }
    public function list_equipment($lkplocation, $indi) {
        $query = DB::table('et_booking_facility')->leftjoin('main_booking', 'main_booking.id', '=', 'et_booking_facility.fk_main_booking')->leftJoin('et_equipment_book', 'et_booking_facility.id', '=', 'et_equipment_book.fk_et_booking_facility')->leftJoin('et_equipment', 'et_equipment.id', '=', 'et_equipment_book.fk_et_equipment')->selectRaw('DISTINCT(fk_et_equipment),et_equipment.ee_name')->where('main_booking.fk_lkp_status', 5);
        if ($lkplocation == false) { //admin @ super admin @ multi operasi
            if ($indi == 1) {
                return $query->where('et_equipment.fk_lkp_location', '<>', 1)
                //->whereIn('et_booking_facility.fk_main_booking',$mainbooking)
                ->pluck('et_equipment.ee_name', 'fk_et_equipment');
            } else {
                return $query->where('et_equipment.fk_lkp_location', '<>', 1)
                //->whereIn('et_booking_facility.fk_main_booking',$mainbooking)
                ->pluck('fk_et_equipment');
            }
        } else {
            if ($indi == 1) {
                return $query->where('et_equipment.fk_lkp_location', '=', $lkplocation)
                // ->whereIn('et_booking_facility.fk_main_booking',$mainbooking)
                ->pluck('et_equipment.ee_name', 'fk_et_equipment');
            } else {
                return $query->where('et_equipment.fk_lkp_location', '=', $lkplocation)
                // ->whereIn('et_booking_facility.fk_main_booking',$mainbooking)
                ->pluck('fk_et_equipment');
            }
        }
    }
    public function location() {
        return LkpLocation::whereIn('id', array(4, 5, 6, 7, 9, 10, 11, 12))->pluck('lc_description', 'id');
    }
    // Report Penggunaan::End


    public function getLocation($userId) {
        $userLkp_location = DB::table('user_profiles')->where('fk_users', $userId)->first()->fk_lkp_location;
        $userRole = DB::table('user_role', $userId)->first();
        $userRole2 = All::GetAllRow('user_role', 'user_id', Session::get('user.id'))->pluck('role_id')->toArray();
        // dd(Session::get('user'), $userRole2);
        if (in_array(1, $userRole2)) {
            $data = All::GetAllRow('lkp_location', 'lc_status', 1)->where('id', '!=', 1)->where('deleted_at', NULL);
            return $data;
        } else if (($userRole->role_id == 14) || ($userRole->role_id == 12) || ($userRole->role_id == 11) || ($userRole->role_id == 1)) {
            $data = DB::table('lkp_location')->where('id', '!=', '1')->where('id', '!=', '2')->where('id', '!=', '3')->where('id', '!=', '8')->get();
            return $data;
        } else {
            if ($userLkp_location == 0 || $userLkp_location == null) {
                $data = DB::table('lkp_location')->whereNotIn('id', [1, 2, 3, 8])->where('deleted_at', null)->get();
                return $data;
            } else {
                $data = DB::table('lkp_location')->where('id', '=', $userLkp_location)->get();
                return $data;
            }
        }
    }
    
    public function getCheckfacilitytype($fid) {
        $data = DB::select(DB::raw("SELECT b.ef_type FROM et_facility b,et_facility_type a WHERE a.fk_et_facility = b.id and a.id = $fid"));
        foreach ($data as $key => $value) {
            $type = $value->ef_type;
        }
        return $type;
    }
    public function getCheckfunction($fid) {
        $ettype = DB::table('et_facility_type')->where('id', $fid)->first();
        $info = DB::table('et_function_detail')->where('fk_et_facility', $ettype->fk_et_facility)->first();

        return $info;
    }
    public function getChildruangcr($fid, $sdate) {
        $date = date("Y-m-d", strtotime($sdate));
        $gettype = DB::table('et_facility_type')->where('id', $fid)->first();
        $type = $gettype->fk_et_facility;
        
        $data2 = array();
        $daynum = date("w", strtotime($date));
        if (($daynum == 0) || ($daynum == 6)) {
            $day = 2;
        } else {
            $day = 1;
        }
        if ($type == 9) {
            $child = DB::select(DB::raw("SELECT a.fk_et_facility as 'fef',b.* 
                  FROM et_facility_type a,et_facility_detail b,et_slot_price c,et_slot_time d 
                  WHERE b.fk_et_facility_type = $fid and efd_status = 1 and a.id = b.fk_et_facility_type 
                  and d.id = c.fk_et_slot_time and c.esp_day_cat = $day and c.fk_et_facility = a.fk_et_facility 
                  and d.id NOT IN(SELECT aa.fk_et_slot_time from et_confirm_booking_detail aa,et_facility_detail bb where aa.fk_et_facility_detail = bb.id and aa.ecbd_date_booking = '$date' and bb.fk_et_facility_type = $fid AND aa.fk_et_facility_detail = b.id)"));
            //dd($child);
            
        } else {
            $child = DB::select(DB::raw("SELECT a.fk_et_facility as 'fef',b.* 
                  FROM et_facility_type a,et_facility_detail b,et_facility_price c,et_slot_time d 
                  WHERE b.fk_et_facility_type = $fid and efd_status = 1 and a.id = b.fk_et_facility_type and c.efp_unit_price !='0.00'
                  and d.id = c.fk_et_slot_time and c.efp_day_cat = $day and c.fk_et_facility = a.fk_et_facility 
                  and d.id NOT IN(SELECT aa.fk_et_slot_time from et_confirm_booking_detail aa,et_facility_detail bb where aa.fk_et_facility_detail = bb.id and aa.ecbd_date_booking = '$date' and bb.fk_et_facility_type = $fid AND aa.fk_et_facility_detail = b.id)"));
            
        }
        // dd($child);
        $data = array();
        $rate = DB::select(DB::raw(
           "SELECT c.efd_name,a.efp_unit_price,a.fk_lkp_slot_cat,a.efp_day_cat
            FROM et_facility_price a,et_facility_type b,et_facility_detail c
            WHERE b.fk_et_facility = a.fk_et_facility 
            AND a.efp_unit_price !=0.00 
            AND c.fk_et_facility_type = b.id 
            AND b.id = $fid 
            GROUP BY 1,3,4"));
        $hargawdp = 'Tiada Slot';
        $hargawdm = 'Tiada Slot';
        $hargawep = 'Tiada Slot';
        $hargawem = 'Tiada Slot';
        foreach ($rate as $key => $value) {
            if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawdp = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawdm = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawep = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawem = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 3) === true) {
                $hargawem = $value->efp_unit_price;
                $hargawep = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 3) === true) {
                $hargawdm = $value->efp_unit_price;
                $hargawdp = $value->efp_unit_price;
            }
        }
        foreach ($child as $key => $value) {
            $data[$value->id] = array("id" => $value->id, "efd_name" => $value->efd_name, "pricewdm" => $hargawdp, "pricewdn" => $hargawdm, "pricewem" => $hargawep, "pricewen" => $hargawem);
        }

        return $data;
    }
    public function getCheckruangfunction($fid) {
        $data = array();
        $data2 = DB::select(DB::raw("SELECT a.id,a.ef_desc,d.* FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d 
            WHERE a.id = b.fk_et_function 
            AND b.fk_et_facility = c.fk_et_facility 
            AND c.id = 40 AND d.fk_et_function = a.id AND d.fk_et_facility = c.fk_et_facility"));
                $data1 = DB::select(DB::raw("SELECT a.id AS 'fidd',a.ef_desc,d.* FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d 
            WHERE a.id = b.fk_et_function 
            AND b.fk_et_facility = c.fk_et_facility 
            AND c.id = 40 AND d.fk_et_function = a.id AND d.fk_et_facility = c.fk_et_facility GROUP BY d.fk_et_function"));
        foreach ($data1 as $key => $value2) {
            foreach ($data2 as $key => $value) {
                if ($value->efp_day_cat == 1 && $value->fk_et_function == $value2->fidd) {
                    $hargawd = $value->efp_unit_price;
                } else if ($value->efp_day_cat == 2 && $value->fk_et_function == $value2->fidd) {
                    $hargawe = $value->efp_unit_price;
                }
            }
            $data[$value2->fidd] = array("id" => $value2->fidd, "efd_name" => $value2->ef_desc, "pricewd" => $hargawd, "pricewe" => $hargawe,);
        }
        return $data;
    }
    public function getCheckroom($fid) {
        return DB::select(DB::raw("SELECT * FROM et_facility_type a WHERE a.id = $fid"));
    }
    // Utk Dewan
    public function getCheckfacilityfunction1($fid, $date = null) {
        $ef = All::GetRow('et_facility_type', 'id', $fid)->fk_et_facility;
        $data = array();
        $checkmarry = DB::table('et_facility_type')->where('id', $fid)->first();
        if ($checkmarry->eft_marry_indi == 2) {
            $data1 = DB::select(DB::raw("SELECT a.ef_desc,a.id as 'functionid' FROM et_function a,et_function_detail b,et_facility_type c WHERE c.id = $fid and c.fk_et_facility = b.fk_et_facility and b.fk_et_function = a.id  and a.id !='6' and a.id !='4' and a.id !='10'"));
        } else {
            $data1 = DB::select(DB::raw("SELECT a.ef_desc,a.id as 'functionid' FROM et_function a,et_function_detail b,et_facility_type c WHERE c.id = $fid and c.fk_et_facility = b.fk_et_facility and b.fk_et_function = a.id  and a.id !='6' and a.id !='4'"));
        }
        $data2 = DB::select(DB::raw("SELECT a.ef_desc,a.id AS 'functionid',d.efp_unit_price,d.efp_day_cat,d.fk_lkp_slot_cat 
            FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d
            WHERE c.id = $fid AND c.fk_et_facility = d.fk_et_facility AND c.fk_et_facility = b.fk_et_facility AND b.fk_et_function = a.id AND a.id = d.fk_et_function and a.id
            GROUP BY efp_day_cat,fk_lkp_slot_cat,efp_unit_price"));
        
        if($ef != 20){
            $avail9to6 = true;
            $info = DB::select(DB::raw("
                SELECT e.id
                FROM et_hall_time a
                JOIN et_hall_book b ON b.id = a.fk_et_hall_book
                JOIN et_booking_facility c ON c.id = b.fk_et_booking_facility
                JOIN et_facility_price d ON d.id = a.fk_et_facility_price
                JOIN et_slot_time e ON e.id = d.fk_et_slot_time
                WHERE c.fk_et_facility_type IN ($fid)
                AND c.ebf_start_date = '$date'
            "));
            $ids = collect($info)->pluck('id')->toArray();
            if (count(array_intersect(range(1, 9), $ids)) > 0) {
                $avail9to6 = false;
            }
            foreach ($data1 as $key => $value) {
                $function = $value->functionid;
                $desc = $value->ef_desc;
                foreach ($data2 as $key => $value) {
                    if ($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 1 && $value->functionid == $function) {
                        $hargawdp = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 2 && $value->functionid == $function) {
                        $hargawdm = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 1 && $value->functionid == $function) {
                        $hargawep = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 2 && $value->functionid == $function) {
                        $hargawem = $value->efp_unit_price;
                    }
                }
                if($avail9to6 || $function != 10){
                    $data[$function] = array("efd_name" => $desc, "id" => $function, "pricewdm" => $hargawdp, "pricewdn" => $hargawdm, "pricewem" => $hargawep, "pricewen" => $hargawem);
                }
            }
        }

        // dd($data, $data1, $data2);
        return $data;
        //       $data = DB::select(DB::raw("SELECT a.ef_desc,a.id AS 'functionid',d.efp_unit_price,d.efp_day_cat,d.fk_lkp_slot_cat
        // FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d
        // WHERE c.id = $fid AND c.fk_et_facility = d.fk_et_facility AND c.fk_et_facility = b.fk_et_facility AND b.fk_et_function = a.id AND a.id = d.fk_et_function
        // GROUP BY efp_day_cat,fk_lkp_slot_cat,efp_unit_price"));
        
    }
    public function getCheckfacilityfunction($fid) {
        $ef = All::GetRow('et_facility_type', 'id', $fid)->fk_et_facility;
        $data = array();
        $data1 = DB::select(DB::raw("SELECT a.ef_desc,a.id as 'functionid' FROM et_function a,et_function_detail b,et_facility_type c WHERE c.id = $fid and c.fk_et_facility = b.fk_et_facility and b.fk_et_function = a.id and a.id !='10'"));
        $data2 = DB::select(DB::raw("SELECT a.ef_desc,a.id AS 'functionid',d.efp_unit_price,d.efp_day_cat,d.fk_lkp_slot_cat 
            FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d
            WHERE c.id = $fid AND c.fk_et_facility = d.fk_et_facility AND c.fk_et_facility = b.fk_et_facility AND b.fk_et_function = a.id AND a.id = d.fk_et_function and a.id
            GROUP BY efp_day_cat,fk_lkp_slot_cat,efp_unit_price"));

        if($ef != 20){
            foreach ($data1 as $key => $value) {
                $function = $value->functionid;
                $desc = $value->ef_desc;
                foreach ($data2 as $key => $value) {
                    if ($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 1 && $value->functionid == $function) {
                        $hargawdp = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 2 && $value->functionid == $function) {
                        $hargawdm = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 1 && $value->functionid == $function) {
                        $hargawep = $value->efp_unit_price;
                    } else if ($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 2 && $value->functionid == $function) {
                        $hargawem = $value->efp_unit_price;
                    } else {
                        
                    }
                }
                $data[$function] = array("efd_name" => $desc, "id" => $function, "pricewdm" => $hargawdp, "pricewdn" => $hargawdm, "pricewem" => $hargawep, "pricewen" => $hargawem);
            }
        }
        
        return $data;
        //       $data = DB::select(DB::raw("SELECT a.ef_desc,a.id AS 'functionid',d.efp_unit_price,d.efp_day_cat,d.fk_lkp_slot_cat
        // FROM et_function a,et_function_detail b,et_facility_type c,et_facility_price d
        // WHERE c.id = $fid AND c.fk_et_facility = d.fk_et_facility AND c.fk_et_facility = b.fk_et_facility AND b.fk_et_function = a.id AND a.id = d.fk_et_function
        // GROUP BY efp_day_cat,fk_lkp_slot_cat,efp_unit_price"));
        
    }
    public function validation_email($id, $type){
        if ($type == 1) {
            $data = DB::select(DB::raw(
                "SELECT     a.fk_lkp_status, e.est_slot_time, f.efd_name, c.ehb_booking_date, g.eft_type_desc, h.lc_description
                    FROM    main_booking a,
                            et_booking_facility b,
                            et_hall_book c,
                            et_hall_time d,
                            et_slot_time e,
                            et_facility_detail f,
                            et_facility_type g,
                            lkp_location h
                    WHERE   b.fk_main_booking = a.id
                    AND     c.fk_et_booking_facility = b.id
                    AND     d.fk_et_hall_book = c.id
                    AND     d.fk_et_slot_time = e.id
                    AND     f.fk_et_facility_type = b.fk_et_facility_type
                    AND     g.id = b.fk_et_facility_type
                    AND     h.id = a.fk_lkp_location
                    AND     a.id = $id"));
        } else {
            $data = DB::select(DB::raw(
                "SELECT     a.id, 
                            a.bmb_booking_no, 
                            a.fk_lkp_status,
                            e.est_slot_time, 
                            i.fk_et_slot_time,
                            f.efd_name,
                            c.esb_booking_date, 
                            g.eft_type_desc,
                            h.lc_description
                    FROM    main_booking a,
                            et_booking_facility b,
                            et_sport_book c,
                            et_sport_time d,
                            et_slot_time e,
                            et_facility_detail f,
                            et_facility_type g,
                            lkp_location h,
                            et_slot_price i
                    WHERE   b.fk_main_booking = a.id
                    AND     c.fk_et_booking_facility = b.id
                    AND     d.fk_et_sport_book = c.id
                    AND     d.fk_et_slot_price = i.id
                    AND     f.id = b.fk_et_facility_detail
                    AND     g.id = b.fk_et_facility_type
                    AND     h.id = a.fk_lkp_location
                    AND     i.fk_et_slot_time = e.id
                    AND     a.id = $id"));
        }
        return $data;
    }
    
    // Admin 
    public function pengurusanFasiliti(){
        if(count(Session::get('user.roles')) && Session::get('user.roles')[0] == 1){
            return All::Show('et_facility_type', 'updated_at', 'DESC');
        } else if(count(Session::get('user.roles')) && in_array(Session::get('user.roles')[0], [4, 5, 6, 7])){
            return All::Show('et_facility_type', 'updated_at', 'DESC')->whereIn('fk_lkp_location', Session::get('user.roles'));
        } else {
            return All::Show('et_facility_type', 'updated_at', 'DESC');
        }
    }
    public function bannerStatus(){
        
    }

    // Report
    public function locationByPresint(){
        if(Session::get('user.roles')[0] == 1){
            return All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        } else if(in_array(Session::get('user.roles')[0], [4, 5, 6, 7])){
            return All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->whereIn('id', Session::get('user.roles'));
        } else {
            return All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        }
    }

    // Utk Bilik
    public function getChild($fid) {
        $child = DB::select(DB::raw("SELECT a.fk_et_facility as 'fef',b.* FROM et_facility_type a,et_facility_detail b WHERE b.fk_et_facility_type = $fid and efd_status = 1 and a.id = b.fk_et_facility_type"));
        $data = array();
        /*$rate = DB::select(DB::raw("SELECT c.efd_name,a.efp_unit_price,a.fk_lkp_slot_cat,a.efp_day_cat
        FROM et_facility_price a,et_facility_type b,et_facility_detail c
        WHERE b.fk_et_facility = a.fk_et_facility AND a.efp_unit_price !=0.00 AND c.fk_et_facility_type = b.id AND b.id = $fid GROUP BY 1,3,4"));*/
        // FOC ada dalam senarai harga
        $rate = DB::select(DB::raw("SELECT c.efd_name,a.efp_unit_price,a.fk_lkp_slot_cat,a.efp_day_cat
                FROM et_facility_price a,et_facility_type b,et_facility_detail c
                WHERE b.fk_et_facility = a.fk_et_facility AND c.fk_et_facility_type = b.id AND b.id = $fid"));
        $hargawdp = 'Tiada Slot';
        $hargawdm = 'Tiada Slot';
        $hargawep = 'Tiada Slot';
        $hargawem = 'Tiada Slot';
        $hargawdap = 'Tiada Slot';
        $hargawdam = 'Tiada Slot';
        $hargawdcp = 'Tiada Slot';
        $hargawdcm = 'Tiada Slot';
        foreach ($rate as $key => $value) {
            if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawdp = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawdm = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawep = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawem = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 3 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawdap = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 3 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawdam = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 4 && $value->fk_lkp_slot_cat == 1) === true) {
                $hargawdcp = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 4 && $value->fk_lkp_slot_cat == 2) === true) {
                $hargawdcm = $value->efp_unit_price;
            }else if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 3) === true) {
                  $hargawdm = $value->efp_unit_price;
                  $hargawdp = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 3) === true) {
                $hargawem = $value->efp_unit_price;
                $hargawep = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 3 && $value->fk_lkp_slot_cat == 3) === true) {
                $hargawdam = $value->efp_unit_price;
                $hargawdap = $value->efp_unit_price;
            } else if (($value->efp_day_cat == 4 && $value->fk_lkp_slot_cat == 3) === true) {
                $hargawdcm = $value->efp_unit_price;
                $hargawdcp = $value->efp_unit_price;
            } 
        }
        if ($hargawdp == '0.00') {
            $hargawdp = 'Percuma';
        } else {
          if ($hargawdp != 'Tiada Slot') {
            $hargawdp = 'RM '.$hargawdp;
          }
        }
        if ($hargawdm == '0.00') {
            $hargawdm = 'Percuma';
        } else {
          if ($hargawdm != 'Tiada Slot') {
            $hargawdm = 'RM '.$hargawdm;
          }
        }
        if ($hargawep == '0.00') {
            $hargawep = 'Percuma';
        } else {
          if ($hargawep != 'Tiada Slot') {
            $hargawep = 'RM '.$hargawep;
          }
        }
        if ($hargawem == '0.00') {
            $hargawem = 'Percuma';
        } else {
          if ($hargawem != 'Tiada Slot') {
            $hargawem = 'RM '.$hargawem;
          }
        }
        if ($hargawdap == '0.00') {
          $hargawdap = 'Percuma';
        } else {
          if ($hargawdap != 'Tiada Slot') {
            $hargawdap = 'RM '.$hargawdap;
          }
        }
        if ($hargawdam == '0.00') {
            $hargawdam = 'Percuma';
        } else {
          if ($hargawdam != 'Tiada Slot') {
            $hargawdam = 'RM '.$hargawdam;
          }
        }
        if ($hargawdcp == '0.00') {
          $hargawdcp = 'Percuma';
        } else {
          if ($hargawdcp != 'Tiada Slot') {
            $hargawdcp = 'RM '.$hargawdcp;
          }
        }
        if ($hargawdcm == '0.00') {
            $hargawdcm = 'Percuma';
        } else {
          if ($hargawdcm != 'Tiada Slot') {
            $hargawdcm = 'RM '.$hargawdcm;
          }
        }
        foreach ($child as $key => $value) {
            $data[$value->id] = array("id" => $value->id, "efd_name" => $value->efd_name, "pricewdm" => $hargawdp, "pricewdn" => $hargawdm, "pricewem" => $hargawep, "pricewen" => $hargawem, "pricewdam" => $hargawdap, "pricewdan" => $hargawdam,"pricewdcm" => $hargawdcp,"pricewdcn" => $hargawdcm);
        }
        return $data;
    }
    public function getChild2($fid, $sdate) {
        $date = date("Y-m-d", strtotime($sdate));
        $checkbooking = DB::select(DB::raw("select count(*) as 'number' from et_confirm_booking where fk_et_facility_type = ".$fid." and ecb_date_booking >= '".$date."' and ecb_date_booking <= '".$date."' and ecb_flag_indicator = 2"));
        $checkfacclose = DB::select(DB::raw("select COUNT(*) as 'number' from (select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) selected_date from
             (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
             (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
             (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
             (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
             (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
            where selected_date between curdate() and curdate() + interval 365 day and dayofweek('$date') in 
            (Select day_num + 1 from et_facility_close where fk_et_facility_type = $fid)"));    
        //$idcheck = $checkbooking[0]->number;
        //$idcheck = $idcheck + $checkfacclose[0]->number;
        $idcheck = 0;
	    //dd($idcheck);
        if ($idcheck > 0) {
            $data = [];
            return $data;
        } else {
            //dd($checkbooking);
            $child = DB::select(DB::raw("SELECT a.fk_et_facility as 'fef',b.* FROM et_facility_type a,et_facility_detail b WHERE b.fk_et_facility_type = $fid and efd_status = 1 and a.id = b.fk_et_facility_type"));
            //dd($child);
            $data = array();
            //$rate = DB::select(DB::raw("SELECT a.efp_unit_price,a.efp_day_cat,a.fk_lkp_slot_cat,c.efd_rent_cat FROM et_facility_price a,et_facility_type b,et_facility_detail c WHERE b.id=c.fk_et_facility_type and a.fk_et_facility = b.fk_et_facility and b.id = $fid GROUP BY a.efp_day_cat,a.fk_lkp_slot_cat,a.efp_unit_price"));
            $rate = DB::select(DB::raw("SELECT a.efp_unit_price,a.efp_day_cat,a.fk_lkp_slot_cat,c.efd_rent_cat FROM et_facility_price a,et_facility_type b,et_facility_detail c WHERE b.id=c.fk_et_facility_type and a.fk_et_facility = b.fk_et_facility and b.id = $fid"));
            //dd($rate);
            $hargawdp = 'Tiada Slot';
            $hargawdm = 'Tiada Slot';
            $hargawep = 'Tiada Slot';
            $hargawem = 'Tiada Slot';
            $hargawdap = 'Tiada Slot';
            $hargawdam = 'Tiada Slot';
            $hargawdcp = 'Tiada Slot';
            $hargawdcm = 'Tiada Slot';
            foreach ($rate as $key => $value) {
                if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 1) === true) {
                    $hargawdp = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 1 && $value->fk_lkp_slot_cat == 2) === true) {
                    $hargawdm = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 1) === true) {
                    $hargawep = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 2 && $value->fk_lkp_slot_cat == 2) === true) {
                    $hargawem = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 3 && $value->fk_lkp_slot_cat == 1) === true) {
                    $hargawdap = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 3 && $value->fk_lkp_slot_cat == 2) === true) {
                    $hargawdam = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 4 && $value->fk_lkp_slot_cat == 1) === true) {
                    $hargawdcp = $value->efp_unit_price;
                } else if (($value->efp_day_cat == 4 && $value->fk_lkp_slot_cat == 2) === true) {
                    $hargawdcm = $value->efp_unit_price;
                }
            }
            foreach ($child as $key => $value) {
                $fef = $value->fef;
                $fefd = $value->id;
                $slotcat = $value->efd_rent_cat;
                //Tiada data insert et_slot_price
                /*$slot = EsPrice::where('fk_et_facility',$fef)
                ->where('esp_day_cat', '=', 1)
                ->count();*/
                $slot = DB::table('et_facility_price')->where('fk_et_facility', '=', $fef)->count();
                //dd($slot);
                $usedslotcount = DB::table('et_confirm_booking_detail')->where('ecbd_date_booking', $date)->where('fk_et_facility_detail', '=', $fefd)->count();
                //dd($usedslotcount);
                //dd($fef);
                if ($usedslotcount != $slot) {
                    $data[$value->id] = array("id" => $value->id, "efd_name" => $value->efd_name, "pricewdm" => $hargawdp, "pricewdn" => $hargawdm, "pricewem" => $hargawep, "pricewen" => $hargawem, "pricewdam" => $hargawdap, "pricewdan" => $hargawdam,"pricewdcm" => $hargawdcp,"pricewdcn" => $hargawdcm);
                } else {
                    //dd("masuk");
  
                }
            }
            //RETURN $rate;
            //dd($data);
            return $data;
        }
    }

    // Internal & External
    public function getAvailslotroom($et, $fidd, $idbooking, $date, $fbooking, $new_et_function = null) {
        $getEtt = DB::table('et_facility_detail')->where('id', $fidd)->first();
        $ett = $getEtt->fk_et_facility_type;
        $day = Sport::getDayIndicator($date,$ett);
        $et_faci_id = All::GetAllRow('et_facility_type', 'id', $ett)->first()->fk_et_facility;
        if($et_faci_id == 1 || $et_faci_id == 2){
            $ett = [1, 2];
            $ett = implode(',', $ett);
        } else {
            $ett = (string)$ett; 
        }

        $getParrent = DB::table('et_facility_detail')->where('fk_et_facility_type', $ett)->where('efd_parent', '=', 1)->first();

        if($ett == 36 || $ett == 35){
            $exist36 = All::GetAllRow('et_confirm_booking', 'fk_et_facility_type', 36)->where('ecb_date_booking', $date);
            $exist35 = All::GetAllRow('et_confirm_booking', 'fk_et_facility_type', 35)->where('ecb_date_booking', $date);

            if($ett == 36 && count($exist35) > 0){
                return [];
            } else if($ett == 35 && count($exist36) > 0) {
                return [];
            } else {
                $data['fk_et_function'] = DB::table('et_hall_book')->where('fk_et_booking_facility', $fbooking)->first();
                
                $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa', a.*
                    FROM et_facility_price a, et_slot_time b
                    WHERE a.fk_et_facility = $et 
                    AND a.efp_day_cat = $day
                    AND a.fk_et_slot_time = b.id 
                    AND a.efp_unit_price != '0'
                    AND b.id NOT IN (
                        SELECT fk_et_slot_time 
                        FROM et_confirm_booking_detail 
                        WHERE ecbd_date_booking = '$date' 
                        AND fk_et_facility_type IN ($ett)
                    )
                    AND a.id NOT IN (
                        SELECT a.fk_et_facility_price 
                        FROM et_hall_time a, et_hall_book b, et_booking_facility c
                        WHERE c.fk_main_booking = $idbooking 
                        AND c.id = $fbooking 
                        AND c.fk_et_facility_type IN ($ett)
                        AND c.id = b.fk_et_booking_facility
                        AND b.id = a.fk_et_hall_book
                        AND c.ebf_start_date = '$date' 
                    )
                    AND a.fk_et_function = :fk_et_function
                    ORDER BY b.display_order"), ['fk_et_function' => $data['fk_et_function']->fk_et_function]);
            }
        // } else if ($et_faci_id == 20){
        //     $info = DB::select(DB::raw(
        //         "SELECT DISTINCT a.fk_et_facility as 'fef',b.* 
        //           FROM et_facility_type a,et_facility_detail b,et_facility_price c,et_slot_time d 
        //           WHERE b.fk_et_facility_type = $ett 
        //           AND efd_status = 1 
        //           AND a.id = b.fk_et_facility_type 
        //           AND c.efp_unit_price !='0.00'
        //           AND d.id = c.fk_et_slot_time 
        //           AND c.efp_day_cat = $day 
        //           AND c.fk_et_facility = a.fk_et_facility 
        //           AND b.id NOT IN(
        //             SELECT a.fk_et_facility_detail
        //             FROM et_booking_facility a
        //             WHERE a.fk_main_booking = $idbooking)
        //         "));
                
        //     return $info;
        } else if ($getParrent == NULL) {
            if($new_et_function == null){
                $data['fk_et_function'] = DB::table('et_hall_book')->where('fk_et_booking_facility', $fbooking)->first();
                
                if($data['fk_et_function'] != null){
                    $data['fk_et_function'] = $data['fk_et_function']->fk_et_function;
                    
                    // $info = DB::select(DB::raw("
                    // "));
                    // dd($info, $date);

                    $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa', a.*
                        FROM et_facility_price a, et_slot_time b
                        WHERE a.fk_et_facility = $et 
                        AND a.efp_day_cat = $day
                        AND a.fk_et_slot_time = b.id 
                        AND a.efp_unit_price != '0'
                        AND b.id NOT IN (
                            SELECT fk_et_slot_time 
                            FROM et_confirm_booking_detail 
                            WHERE ecbd_date_booking = '$date' 
                            AND fk_et_facility_type IN ($ett)
                            UNION
                            SELECT a.fk_et_slot_time 
                            FROM et_hall_time a, et_hall_book b, et_booking_facility c, main_booking d
                            WHERE c.fk_main_booking = $idbooking 
                            AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_hall_book
                            UNION
                            SELECT a.fk_et_slot_time 
                            FROM et_hall_time a, et_hall_book b, et_booking_facility c, et_confirm_booking d
                            WHERE d.fk_main_booking = c.fk_main_booking
                            AND d.ecb_date_booking = '$date'
                            AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_hall_book
                        )
                        AND a.id NOT IN (
                            SELECT a.fk_et_facility_price 
                            FROM et_hall_time a, et_hall_book b, et_booking_facility c
                            WHERE c.fk_main_booking = $idbooking 
                            AND c.fk_et_facility_type IN ($ett)
                            AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_hall_book
                            AND c.ebf_start_date = '$date' 
                        )
                        AND a.fk_et_function = :fk_et_function
                        ORDER BY b.display_order"), ['fk_et_function' => $data['fk_et_function']]);

                    return $info;
                    // $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa', a.*
                    //     FROM et_facility_price a, et_slot_time b
                    //     WHERE a.fk_et_facility = $et 
                    //     AND a.efp_day_cat = $day
                    //     AND a.fk_et_slot_time = b.id 
                    //     AND a.efp_unit_price != '0'
                    //     AND b.id NOT IN (
                    //         SELECT fk_et_slot_time 
                    //         FROM et_confirm_booking_detail 
                    //         WHERE ecbd_date_booking = '$date' 
                    //         AND fk_et_facility_detail = $fidd 
                    //     )
                    //     AND a.id NOT IN (
                    //         SELECT a.fk_et_facility_price 
                    //         FROM et_hall_time a, et_hall_book b, et_booking_facility c
                    //         WHERE c.fk_main_booking = $idbooking 
                    //         AND c.id = $fbooking 
                    //         AND c.fk_et_facility_detail = $fidd 
                    //         AND c.id = b.fk_et_booking_facility
                    //         AND b.id = a.fk_et_hall_book
                    //     )
                    //     AND a.fk_et_function = :fk_et_function
                    //     ORDER BY b.display_order"), ['fk_et_function' => $data['fk_et_function']]
                    // );
                } else {
                    $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                        FROM et_facility_price a,et_slot_time b
                        WHERE a.fk_et_facility = $et 
                        AND a.efp_day_cat = $day
                        AND a.fk_et_slot_time = b.id 
                        AND a.efp_unit_price !='0'
                        AND b.id NOT IN 
                            (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail = $fidd )
                        AND a.id NOT IN 
                            (SELECT a.fk_et_facility_price FROM et_hall_time a,et_hall_book b,et_booking_facility c
                            WHERE c.fk_main_booking = $idbooking AND c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_hall_book) 
                        ORDER BY b.display_order"));
                }
            } else {
                if($new_et_function != null){
                    $data['fk_et_function'] = $new_et_function;
                    // dd($et, $day, $date, $ett, $new_et_function);
                    
                    $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa', a.*
                        FROM et_facility_price a, et_slot_time b
                        WHERE a.fk_et_facility = $et 
                        AND a.efp_day_cat = $day
                        AND a.fk_et_slot_time = b.id 
                        AND a.efp_unit_price != '0'
                        AND b.id NOT IN (
                            SELECT fk_et_slot_time 
                            FROM et_confirm_booking_detail 
                            JOIN et_facility_type b ON b.id = et_confirm_booking_detail.fk_et_facility_type
                            WHERE ecbd_date_booking = '$date' 
                            AND b.fk_et_facility IN ($ett)
                        )
                        AND a.id NOT IN (
                            SELECT a.fk_et_facility_price 
                            FROM et_hall_time a, et_hall_book b, et_booking_facility c
                            WHERE c.fk_main_booking = $idbooking 
                            AND c.id = $fbooking 
                            AND c.fk_et_facility_type IN ($ett)
                            AND c.id = b.fk_et_booking_facility
                            AND b.id = a.fk_et_hall_book
                            AND c.ebf_start_date = '$date' 
                        )
                        AND a.fk_et_function = :fk_et_function
                        ORDER BY b.display_order"), ['fk_et_function' => $data['fk_et_function']]);
                            // SELECT fk_et_slot_time 
                            // FROM et_confirm_booking_detail 
                            // WHERE ecbd_date_booking = '$date' 
                            // AND fk_et_facility_type IN ($ett)
                        // AND a.fk_et_function = :fk_et_function
                        // ORDER BY b.display_order"), ['fk_et_function' => $data['fk_et_function']]);
                } 
            }
        } else {
            $info = DB::select(DB::raw("SELECT b.est_slot_time AS 'masa',a.*
                FROM et_facility_price a,et_slot_time b
                WHERE a.fk_et_facility = $et AND a.efp_day_cat = $day
                AND a.fk_et_slot_time = b.id AND a.efp_unit_price !='0'
                AND b.id NOT IN 
                    (SELECT fk_et_slot_time FROM et_confirm_booking_detail WHERE ecbd_date_booking = '$date' AND fk_et_facility_detail = $fidd )
                AND a.id NOT IN 
                    (SELECT a.fk_et_facility_price FROM et_hall_time a,et_hall_book b,et_booking_facility c
                    WHERE c.fk_main_booking = $idbooking AND c.id = $fbooking AND c.fk_et_facility_detail = $fidd AND c.id = b.fk_et_booking_facility
                    AND b.id = a.fk_et_hall_book) ORDER BY id"));
            return $info;
        }

        return $info;
    }
    public function determineEbfEhbEF($id, $et_func, $date, $ef = null){
        if($ef != 20){
            $result = DB::table('et_booking_facility')
                ->select('et_hall_book.id as ehbid', 'et_booking_facility.id as ebfid', 'et_booking_facility.*', 'et_hall_book.*')
                ->join('et_hall_book', 'et_booking_facility.id', '=', 'et_hall_book.fk_et_booking_facility')
                ->where('et_booking_facility.fk_main_booking', $id)
                ->where('et_hall_book.fk_et_function', $et_func)
                ->where('et_booking_facility.ebf_start_date', $date)
                ->first();
        } else {
            $result = DB::table('et_booking_facility')
            ->select('et_hall_book.id as ehbid', 'et_booking_facility.id as ebfid', 'et_booking_facility.*', 'et_hall_book.*')
            ->join('et_hall_book', 'et_booking_facility.id', '=', 'et_hall_book.fk_et_booking_facility')
            ->where('et_booking_facility.fk_main_booking', $id)
            ->where('et_booking_facility.ebf_start_date', $date)
            ->first();
        }
            
        return $result;
    }
    // Internal
    public function getInternal()
    {
        $data = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->where('main_booking.fk_users',$user->id)
            ->whereIn('main_booking.fk_lkp_status',['11',])
            ->select('main_booking.id','main_booking.bmb_booking_no',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            ->groupBy('bh_confirm_booking.fk_main_booking')
            ->get();

        return $data;
    }
    public function getAvailslotlist($bookingid, $date, $fun) {
        $timestamp = strtotime($date);
        $date = date('Y-m-d', $timestamp);
        $getEtt = DB::table('et_booking_facility')->where('fk_main_booking', $bookingid)->first();
        $eft = $getEtt->fk_et_facility_type; 
        $ett = $getEtt->fk_et_facility_detail;
        // $ett = $bookingid;
        $edtail = DB::table('et_facility_detail')->where('id', $ett)->first();
        $fkett = $edtail->fk_et_facility_type;
        $fket = DB::table('et_facility_type')->where('id', $fkett)->first();
        $fett = $fket->fk_et_facility;
        if($fett == 1 || $fett == 2){
            $ett = [1, 2];
            $ett = implode(',', $ett);
        } else {
            $ett = (string)$ett; 
        }

        $day = Sport::getDayIndicator($date, $fkett);

        if(in_array($eft, [1,2])){
            $eft = [1,2];
        } else {
            $eft = [$eft];
        }
        $eft = implode(',', $eft);

        if($fett != 20){
            $info = DB::select(DB::raw(
                "SELECT a.efp_unit_price,
                        b.id,
                        b.est_slot_time AS 'masa',
                        a.fk_et_slot_time AS 'efpid' 
                FROM    et_facility_price a,
                        et_slot_time b 
                WHERE a.fk_et_function = $fun
                AND a.fk_et_slot_time = b.id 
                and a.efp_day_cat = $day 
                and a.efp_unit_price !='0.00' 
                and a.fk_et_facility = $fett
                AND b.id NOT IN (
                    SELECT  fk_et_slot_time 
                        FROM    et_confirm_booking_detail 
                        WHERE   ecbd_date_booking = '$date' 
                        AND     fk_et_facility_type IN ($eft))
                AND a.fk_et_slot_time NOT IN 
                    (
                    SELECT  a.fk_et_slot_time 
                    FROM    et_hall_time a,
                            et_hall_book b,
                            et_booking_facility c
                    WHERE   a.fk_et_hall_book = b.id 
                    and     b.ehb_booking_date = '$date' 
                    AND     b.fk_et_booking_facility = c.id 
                    AND     c.fk_main_booking = $bookingid)
                ORDER BY b.display_order"));
        } else {
            $info = DB::select(DB::raw(
                "SELECT a.efp_unit_price,
                        b.id,
                        b.est_slot_time AS 'masa',
                        a.fk_et_slot_time AS 'efpid' 
                FROM    et_facility_price a,
                        et_slot_time b 
                WHERE a.fk_et_function = $fun
                AND a.fk_et_slot_time = b.id 
                and a.efp_day_cat = $day 
                and a.efp_unit_price !='0.00' 
                and a.fk_et_facility = $fett
                AND b.id NOT IN (
                    SELECT  fk_et_slot_time 
                        FROM    et_confirm_booking_detail 
                        WHERE   ecbd_date_booking = '$date' 
                        AND     fk_et_facility_type IN ($eft))
                ORDER BY b.display_order"));
        }

        // spt/spt2 previous ver
        // $info = DB::select(DB::raw(
        //     "SELECT a.efp_unit_price,
        //             b.est_slot_time AS 'masa',
        //             a.fk_et_slot_time AS 'efpid' 
        //     FROM    et_facility_price a,
        //             et_slot_time b 
        //     WHERE a.fk_et_function = $fun
        //     AND a.fk_et_slot_time = b.id 
        //     and a.efp_day_cat = $day 
        //     and a.efp_unit_price !='0.00' 
        //     and a.fk_et_facility = $fett
        //     AND b.id NOT IN (
        //         SELECT  fk_et_slot_time 
        //         FROM    et_confirm_booking_detail 
        //         WHERE   ecbd_date_booking = '$date' 
        //         AND     fk_et_facility_detail = '$ett')
        //     AND a.fk_et_slot_time NOT IN 
        //         (
        //         SELECT  a.fk_et_slot_time 
        //         FROM    et_hall_time a,
        //                 et_hall_book b,
        //                 et_booking_facility c
        //         WHERE   a.fk_et_hall_book = b.id 
        //         and     b.ehb_booking_date = '$date' 
        //         AND     b.fk_et_booking_facility = c.id 
        //         AND     c.fk_main_booking = $bookingid)
        //     ORDER BY b.display_order"));
        // dd($bookingid, $date, $fun, $info, $ett);
        
        return $info;
    }

    public function kutipanharianwithoutFPX($location, $tarikh, $typebayaran) {
        $tarikh = date('Y-m-d', strtotime($tarikh));
        // dd($location, $tarikh, $typebayaran);
        $data = DB::table('main_booking')
            ->select('main_booking.id','main_booking.bmb_booking_no',
                'main_booking.fk_lkp_location','bh_payment.bp_receipt_number','bh_payment.bp_total_amount','bh_payment.bp_payment_ref_no',
                'bh_payment.bp_receipt_date','bh_payment.fk_lkp_payment_mode','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','lkp_payment_mode.lpm_description', 'et_booking_facility.fk_et_facility_type',
                'lkp_status.ls_description','lkp_status.id as statusid'
                )
            ->join('et_booking_facility','main_booking.id','=','et_booking_facility.fk_main_booking')
            ->join('bh_payment','bh_payment.fk_main_booking','=','main_booking.id')
            ->join('lkp_payment_mode','lkp_payment_mode.id','=','bh_payment.fk_lkp_payment_mode')
            ->join('user_profiles','user_profiles.fk_users','=','main_booking.fk_users')
            // ->join('et_booking_facility','et_booking_facility.fk_et_facility_type','=','et_facility_type.id')
            // ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            // ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->where('main_booking.fk_users',$user->id)
            ->where('main_booking.fk_lkp_location',$location)
            ->where('bh_payment.fk_lkp_payment_mode',$typebayaran)
            ->whereRaw('DATE(bh_payment.bp_receipt_date) = ?', [$tarikh])
            ->whereIn('main_booking.fk_lkp_status',['5'])
            // ->groupBy('bh_confirm_booking.fk_main_booking')
            
            ->get();

        // dd($data);
        return $data;
    }
    public function kutipanharian($location, $tarikh, $typebayaran) {
        $tarikh = date('Y-m-d', strtotime($tarikh));
        // dd($location, $tarikh, $typebayaran);
        $query = DB::table(function ($subquery) use ($location, $tarikh) {
            $subquery->select(
                'b.id',
                'b.bmb_booking_no',
                'e.fullname',
                'd.efd_name',
                'd.efd_fee_code',
                'a.bp_receipt_date',
                'a.bp_receipt_number',
                'a.bp_payment_ref_no',
                'f.id as paymode',
                'f.lpm_description',
                // DB::raw('SUM(h.est_total) as ebf_subtotal')
                DB::raw('h.est_total as ebf_subtotal')
            )
            ->from('bh_payment as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('et_booking_facility as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('et_facility_detail as d', 'd.id', '=', 'c.fk_et_facility_detail')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            ->whereDate('a.bp_receipt_date', '=', [$tarikh])
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->groupBy('b.bmb_booking_no', 'd.efd_name')
            ->union(
                DB::table('et_payment_fpx as a1')
                ->select(
                    'b1.id',
                    'b1.bmb_booking_no',
                    'e1.fullname',
                    'd1.efd_name',
                    'd1.efd_fee_code',
                    'a1.fpx_trans_date',
                    'b1.sap_no',
                    'i1.fpx_txn_reference as bp_payment_ref_no',
                    'f1.id as paymode',
                    'f1.lpm_description',
                    // DB::raw('SUM(h1.est_total) as ebf_subtotal')
                    DB::raw('h1.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                ->whereDate('a1.fpx_trans_date', '=', [$tarikh])
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
            );
        }, 'subquery')
        ->where('paymode', $typebayaran)
        ->get();
        // dd($query);
        return $query;
    }

    public function laporandeposit($location, $tarikhmula, $tarikhtamat, $typebayaran, $typekutipan) {
        $tarikhmula = date('Y-m-d', strtotime($tarikhmula));
        $tarikhtamat = date('Y-m-d', strtotime($tarikhtamat));
        if ($typekutipan == 1) {
        //refer table bh_payment and et_payment
            $query= DB::table('bh_payment as a')
            ->select(
                'b.id',
                'b.bmb_booking_no',
                'e.fullname',
                'd.efd_name',
                'd.efd_fee_code',
                'a.bp_receipt_date',
                'a.bp_receipt_number',
                'a.bp_payment_ref_no',
                'f.id as paymode',
                'f.lpm_description',
                DB::raw('h.est_total as ebf_subtotal'),
                // 'j.fk_lkp_event'
            )
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('et_booking_facility as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('et_facility_detail as d', 'd.id', '=', 'c.fk_et_facility_detail')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            // ->join('et_booking_facility_detail as j', 'j.fk_et_booking_facility', '=', 'c.id')
            ->whereDate('a.bp_receipt_date', '>=', $tarikhmula)
            ->whereDate('a.bp_receipt_date', '<=', $tarikhtamat)
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->where('f.id', $typebayaran)
            ->groupBy('b.bmb_booking_no', 'd.efd_name')
            ->get();
        } else {
        //refer table et_payment_fpx     
            // $subquery->union(
            $query = DB::table('et_payment_fpx as a1')
                    ->select(
                        'b1.id',
                        'b1.bmb_booking_no',
                        'e1.fullname',
                        'd1.efd_name',
                        'd1.efd_fee_code',
                        'a1.fpx_trans_date',
                        'b1.sap_no',
                        'i1.fpx_txn_reference as bp_payment_ref_no',
                        'f1.id as paymode',
                        'f1.lpm_description',
                        DB::raw('h1.est_total as ebf_subtotal'),
                        // 'j1.fk_lkp_event'
                    )
                    ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                    ->join('et_booking_facility as c1', 'c1.fk_main_booking', '=', 'b1.id')
                    ->join('et_facility_detail as d1', 'd1.id', '=', 'c1.fk_et_facility_detail')
                    ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                    ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_mode')
                    ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                    ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                    ->join('et_fpx_log as i1', 'i1.fpx_txn_id', '=', 'a1.fpx_serial_no')
                    // ->join('et_booking_facility_detail as j1', 'j1.fk_et_booking_facility', '=', 'c1.id')
                    ->whereDate('a1.fpx_trans_date', '>=', $tarikhmula)
                    ->whereDate('a1.fpx_trans_date', '<=', $tarikhtamat)
                    ->where('b1.fk_lkp_location', $location)
                    ->where('b1.fk_lkp_status', 5)
                    ->where('f1.id', $typebayaran)
                    ->groupBy('b1.bmb_booking_no', 'd1.efd_name')
                    ->get();
                // );
        }
        // dd($query);
        return $query;
    }

    public function kutipandeposit($location,$tarikhfrom,$tarikhto,$typebayaran,$typekutipan){
        $fk_main_booking=DB::table('main_booking')
                            ->where('fk_lkp_location','=',$location)
                            ->pluck('id');
        
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');

        $jenisbayaran=DB::table('lkp_payment_mode')
                        ->where('id','=',$typebayaran)
                        ->first();

        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }

        if($typekutipan==false){
            $typebayaran='';
        }else{
            if($typekutipan==1){
                $typebayaran=$typebayaran;
            }else{
                $typebayaran='';
            }
        }


        if(isset($location) || isset($tarikhfrom) || isset($tarikhto) || isset($typebayaran) || isset($typekutipan)){
        //     if($location=='' && $typebayaran==''):
        //         if($typekutipan==false){
        //             $bh_payment=DB::table('bh_payment')
        //                                     ->whereIn('fk_main_booking',$fk_main_booking)
        //                                     ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
        //                                     ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
        //                                     ->where('fk_lkp_payment_type','=',1)//bayaran deposit
        //                                     ->pluck('id');

        //             $query=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)->where('product_indicator','=',1);

        //         }else{
        //             if($typekutipan==1){
        //             $bh_payment=DB::table('bh_payment')
        //                                     ->whereIn('fk_main_booking',$fk_main_booking)
        //                                     ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
        //                                     ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
        //                                     ->where('fk_lkp_payment_type','=',1)//bayaran deposit
        //                                     ->pluck('id');

        //             $query=DB::table('bh_payment_detail')
        //                     ->whereIn('fk_bh_payment',$bh_payment)
        //                     ->where('product_indicator','=',1);
        //             }else{
        //                 $bh_payment=DB::table('bh_payment_fpx')
        //                                         ->whereIn('fk_main_booking',$fk_main_booking)
        //                                         ->where('fk_lkp_payment_type','=',1)
        //                                         ->where('fpx_status','=',1)
        //                                         ->where('fpx_date','>=', array( $tarikh_from))
        //                                         ->where('fpx_date' ,'<', array( $tarikh_to)) 
        //                                         // ->whereIn('fk_bh_quotation',$quotation)
        //                                         ->pluck('id');

        //                 $query=DB::table('bh_fpx_detail')
        //                         ->whereIn('fk_bh_payment_fpx',$bh_payment)
        //                         ->where('product_indicator','=',1);
        //                     }
        //         }
        //     if($type==$typekutipan){
        //         return $query->paginate(20);
        //     }else{
        //         return $query->get();
        //     }
        // else:
            if($location !='' && ($typebayaran=='' ||$typebayaran==4 )){
                if($typekutipan==false){
                    $bh_payment=DB::table('bh_payment')
                                        ->whereIn('fk_main_booking',$fk_main_booking)
                                        ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                        ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                        ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                        ->pluck('id');

                    $query=DB::table('bh_payment_detail')
                                ->whereIn('fk_bh_payment',$bh_payment)
                                ->where('product_indicator','=',1);
                }else{
                    if($typekutipan==1){
                        $bh_payment=DB::table('bh_payment')
                                            ->whereIn('fk_main_booking',$fk_main_booking)
                                            ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                            ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                            ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                            ->pluck('id');

                        $query=DB::table('bh_payment_detail')
                                ->whereIn('fk_bh_payment',$bh_payment)
                                ->where('product_indicator','=',1); 
                    }else{
                        $bh_payment=DB::table('bh_payment_fpx')
                                                ->whereIn('fk_main_booking',$fk_main_booking)
                                                ->where('fk_lkp_payment_type','=',1)
                                                ->where('fpx_status','=',1)
                                                ->where('fpx_date','>=', array( $tarikh_from))
                                                ->where('fpx_date' ,'<', array( $tarikh_to)) 
                                                // ->whereIn('fk_bh_quotation',$quotation)
                                                ->pluck('id');
                        $query=DB::table('bh_fpx_detail')
                                ->whereIn('fk_bh_payment_fpx',$bh_payment)
                                ->where('product_indicator','=',1);
                    }
                }

                if($typebayaran==4){
                    return $query->paginate(20);
                }else{
                    return $query->get();
                }


            }else{
                if(($typebayaran=='' ||$typebayaran==4 )){
                    if($typekutipan==1){
                        $bh_payment=DB::table('bh_payment')
                                        ->whereIn('fk_main_booking',$fk_main_booking)
                                        ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                        ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                        ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                        // ->whereIn('fk_bh_quotation',$quotation)
                                        ->pluck('id');
                    }else{
                        $bh_payment=DB::table('bh_payment_fpx')
                                        ->whereIn('fk_main_booking',$fk_main_booking)
                                        ->where('fk_lkp_payment_type','=',1)
                                        ->where('fpx_status','=',1)
                                        ->where('fpx_date','>=', array( $tarikh_from))
                                        ->where('fpx_date' ,'<', array( $tarikh_to)) 
                                        // ->whereIn('fk_bh_quotation',$quotation)
                                        ->pluck('id');
                    }
                }else{
                    $bh_payment=DB::table('bh_payment')
                                        ->whereIn('fk_main_booking',$fk_main_booking)
                                        ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                        ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                        ->where('fk_lkp_payment_mode','=',$jenisbayaran->id)
                                        ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                        // ->whereIn('fk_bh_quotation',$quotation)
                                        ->pluck('id');
                    }
                    $query= DB::table('bh_payment_detail')
                                ->whereIn('fk_bh_payment',$bh_payment)
                                ->where('product_indicator','=',1);


                if($typebayaran==4){
                    return $query->paginate(20);
                }else{
                    return $query->get();
                }
            }
                    
        // endif;
                    
        }else{
            $bh_payment=DB::table('bh_payment_fpx')
                            ->whereIn('fk_main_booking',$fk_main_booking)
                            ->where('fk_lkp_payment_type','=',1)
                            ->where('fpx_status','=',1)
                            ->where('fpx_date','>=', array( $tarikh_from))
                            ->where('fpx_date' ,'<', array( $tarikh_to)) 
                            // ->whereIn('fk_bh_quotation',$quotation)
                            ->pluck('id');

            $query=DB::table('bh_fpx_detail')
                        ->whereIn('fk_bh_payment_fpx',$bh_payment)
                        ->where('product_indicator','=',1);
                        
            if($typebayaran==4){
                return $query->paginate(20);
            }else{
            return $query->get();
            }
        }
    }

    // begin::External
    public function slot_masa_rumusan($type, $id){
        if($type = 1){  // Dewan
            $query = DB::select(DB::raw('
                SELECT b.fk_et_facility_type, b.ebf_start_date, c.fk_et_function, d.fk_et_slot_time, d.eht_subtotal
                FROM main_booking AS a
                JOIN et_booking_facility as b ON a.id = b.fk_main_booking
                JOIN et_hall_book as c ON b.id = c.fk_et_booking_facility
                JOIN et_hall_time as d ON c.id = d.fk_et_hall_book
                WHERE a.id = '.$id.'
            '));
        } else {        // Sukan
            $query = DB::select(DB::raw('
                SELECT b.fk_et_facility_type, b.ebf_start_date, c.fk_et_function, d.fk_et_slot_time, d.eht_subtotal
                FROM main_booking AS a
                JOIN et_booking_facility as b ON a.id = b.fk_main_booking
                JOIN et_sport_book as c ON b.id = c.fk_et_booking_facility
                JOIN et_sport_time as d ON c.id = d.fk_et_sport_book
                WHERE a.id = '.$id.'
            '));
        }

        return $query;
    }
    public function recalculate_booking($mbid){
        $mbid = Crypt::decrypt($mbid);
        $data['main'] = All::GetRow('main_booking', 'id', $mbid);
        $data['ldt'] = All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type);
        $data['ldt_rate'] = $data['ldt']->ldt_discount_rate / 100;
        // $data['after_ldt_rate'] = 1 - $data['ldt_rate'];     // Use ldt_rate for discounted price
        $data['after_ldt_rate'] = 1;                            // * 0% discount

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $mbid);
        $eft = $data['ebf']->first()->fk_et_facility_type;
        $ef = All::GetRow('et_facility_type', 'id', $eft)->fk_et_facility;

        $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['eht'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['ehb']->pluck('id'));
        
        $data['eeb'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));

        // Calculate hall
        $total_depo = 0;
        $eeb_hall_total = 0;
        foreach ($data['ebf'] as $key => $value) {
            foreach ($data['ehb'] as $key => $value1) {
                if($value->id == $value1->fk_et_booking_facility){
                    $ehbid = $value1->id;
                    foreach ($data['eht'] as $key1 => $value2) {
                        $efp_unit_price = All::GetRow('et_facility_price', 'id', $value2->fk_et_facility_price)->efp_unit_price;

                        if($ehbid == $value2->fk_et_hall_book){
                            $update_eht = [
                                'eht_price'         => $efp_unit_price * $data['after_ldt_rate'],
                                'eht_total'         => $efp_unit_price * $data['after_ldt_rate'],
                                'eht_subtotal'      => $efp_unit_price * $data['after_ldt_rate']
                            ];
                            $eeb_hall_total += $efp_unit_price * $data['after_ldt_rate'];
                            // dd($data, $efp_unit_price, $update_eht, $eeb_hall_total);
                            $query = All::GetUpdate('et_hall_time', $value2->id, $update_eht);
                        }
                    }
        
                    $update_ehb = [
                        'ehb_total'         => $efp_unit_price * $data['after_ldt_rate'],
                    ];
                    $query = All::GetUpdate('et_hall_book', $value1->id, $update_ehb);
                }
                $update_ebf = [
                    'ebf_subtotal'         => $eeb_hall_total,
                ];
                // dd($data, $eeb_hall_total, $update_ehb, $update_ebf);
                $query = All::GetUpdate('et_booking_facility', $value->id, $update_ebf);
            }
        }
        
        // Calculate equipment
        $total_eq = 0;
        foreach ($data['eeb'] as $key => $value) {
            $eep_unit_price = All::GetRow('et_equipment_price', 'id', $value->fk_et_equipment_price)->eep_unit_price;

            $update_eeb = [
                'eeb_total_price'   => number_format((($eep_unit_price * $value->eeb_quantity) * $data['after_ldt_rate']), 2),
                'eeb_total'         => number_format((($eep_unit_price * $value->eeb_quantity) * $data['after_ldt_rate']), 2),
                'eeb_subtotal'      => number_format((($eep_unit_price * $value->eeb_quantity) * $data['after_ldt_rate']), 2),
            ];
            $total_eq += ($eep_unit_price * $value->eeb_quantity) * $data['after_ldt_rate'];
            // dd($eep_unit_price, $value, $update_eeb, $total_eq);
            $query = All::GetUpdate('et_equipment_book', $value->id, $update_eeb);
        };

        if($ef == 1){
            $depo_rate = All::GetRow('et_facility_detail', 'fk_et_facility_type', $eft)->efd_deposit_percent;
            $total_depo = $eeb_hall_total * ($depo_rate / 100);
        }

        // dd($total_depo, $eeb_hall_total, $data['main']->id);
        // Update main_booking
        $update_mb = [
            'bmb_subtotal'              => $eeb_hall_total + $total_eq,
            'bmb_total_book_hall'       => $eeb_hall_total,
            'bmb_deposit_rm'            => $total_depo,
            'bmb_deposit_rounding'      => $total_depo,
            'bmb_total_equipment'       => $total_eq,
            'updated_at'                => date('Y-m-d H:i:s')
        ];
        $query = All::GetUpdate('main_booking', $data['main']->id, $update_mb);

        // dd('s', $data, $eeb_hall_total, $total_eq, $update_mb, $update_ebf, $update_ehb);
    }
    // end::External

    //laporan kutipan bayaran tertunggak
    public function carianaging($location, $tarikhfrom, $tarikhto, $event, $type) {
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month = date('t-m-Y');
        $defaultrange = [date('Y-m-d', strtotime($first_day_this_month)), date('Y-m-d', strtotime($last_day_this_month)) ];
        $range = [date('Y-m-d', strtotime($tarikhfrom)), date('Y-m-d', strtotime($tarikhto)) ];
        $status = ['2', '3', '9', '12'];
        if (isset($location) == 'false' && $location == '') {
            $fk_main_booking = DB::table('main_booking')
                                    ->whereIn('fk_lkp_status', $status)
                                    ->where('fk_lkp_location', '<>', 1)
                                    ->pluck('id');
        } else {
            $fk_main_booking = DB::table('main_booking')
                                    ->whereIn('fk_lkp_status', $status)
                                    ->where('fk_lkp_location', '=', $location)
                                    ->pluck('id');
        }
        $event = DB::table('lkp_event')
                    ->where('id', '=', $event)->first();
        if ($tarikhfrom != '' || $tarikhto != '') {
            $tarikh = $range;
        } else {
            $tarikh = $defaultrange;
        }
        
        if (isset($location) || isset($tarikhfrom) || isset($tarikhto) || isset($event)) {
            if ($event == ''):
                $query = DB::table('et_booking_facility')
                            ->whereIn('fk_main_booking', $fk_main_booking)
                            ->whereBetween('ebf_start_date', $tarikh)
                            ->whereBetween('ebf_end_date', $tarikh)
                            ->orderBy('ebf_start_date', 'asc')
                            ->orderBy('id', 'asc');
                if ($type == 1) {
                    return $query->paginate(20);
                } else {
                    return $query->get();
                } else:
                    if ($event == '') {
                        $query = DB::table('et_booking_facility')
                                    ->whereIn('fk_main_booking', $fk_main_booking)
                                    ->whereBetween('ebf_start_date', $tarikh)
                                    ->whereBetween('ebf_end_date', $tarikh)
                                    ->orderBy('ebf_start_date', 'asc')
                                    ->orderBy('id', 'asc');
                        if ($type == 1) {
                            return $query->paginate(20);
                        } else {
                            return $query->get();
                        }
                    } else {
                        $et_booking_facility_detail = DB::table('et_booking_facility_detail')
                                                            ->where('fk_lkp_event', '=', $event->id)
                                                            ->pluck('fk_et_booking_facility');
                        $query = DB::table('et_booking_facility')
                                    ->whereIn('fk_main_booking', $fk_main_booking)
                                    ->whereIn('id', $et_booking_facility_detail)
                                    ->whereBetween('ebf_start_date', $tarikh)
                                    ->whereBetween('ebf_end_date', $tarikh)
                                    ->orderBy('ebf_start_date', 'asc')
                                    ->orderBy('id', 'asc');
                        if ($type == 1) {
                            return $query->paginate(20);
                        } else {
                            return $query->get();
                        }
                    }
                endif;
            } else {
                $query = DB::table('et_booking_facility')
                ->whereIn('fk_main_booking', $fk_main_booking)
                ->whereBetween('ebf_start_date', $tarikh)
                ->whereBetween('ebf_end_date', $tarikh)
                ->orderBy('ebf_start_date', 'asc')
                ->orderBy('id', 'asc');
                if ($type == 1) {
                    return $query->paginate(20);
                } else {
                    return $query->get();
                }
            }
        }
}

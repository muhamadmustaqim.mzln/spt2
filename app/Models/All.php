<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class All extends Model
{
    // Acara tiada deleted_at column
    public function ShowAll($table, $id = 'id', $order = 'asc'){
        $query = DB::table($table)
                ->orderBy($id, $order)
                ->get();
        return $query;
    }

    public function Show($table, $id = 'id', $order = 'asc'){
        $query = DB::table($table)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query;
    }
	
	public function Show2023($table, $id, $order){
        $query = DB::table($table)
				->whereYear('created_at',2022)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
				->take(100)
                ->get();
        return $query;
    }
	
	public function ShowByYear($table, $id, $order, $year){
        $query = DB::table($table)
				->whereYear('created_at',$year)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)				
                ->get();
        return $query;
    }

	public function ShowAuditTrail(){
        $query = DB::select(DB::raw("
            SELECT (SELECT b.bud_name FROM user_profiles b WHERE b.fk_users = a.fk_users) AS fk_users,
			(SELECT c.task_description FROM lkp_task c WHERE c.id = a.fk_lkp_task) AS fk_lkp_task, a.task, a.created_at, a.updated_at, a.updated_by
			 FROM audit_trail AS a WHERE YEAR(created_at) = '2022' limit 2000
        "));
        return $query;
    }
	
	public function ShowLimit($table, $id, $order, $limit){
        $query = DB::table($table)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
				->take($limit)
                ->get();
        return $query;
    }

    public function One($table, $id, $order){
        $query = DB::table($table)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query;
    }

    public function Insert($table, $data){
        DB::table($table)->insert($data);
        return true;
    }

    public function InsertGetID($table, $data){
        $id = DB::table($table)->insertGetId($data);
        return $id;
    }
	
	public function GetUpdateReschedule($id, $data){
		
		$query = DB::update(DB::raw('
            UPDATE et_sport_time SET fk_et_slot_price = "' . $data . '" WHERE fk_et_sport_book = (SELECT id FROM et_sport_book WHERE fk_et_booking_facility = (SELECT id FROM et_booking_facility WHERE fk_main_booking = (SELECT id FROM main_booking WHERE bmb_booking_no = "' . $id . '")))
        '));

        return true;
    }

    public function GetUpdate($table, $id, $data){
        DB::table($table)
            ->where('id', $id)
            ->update($data);
        return true;
    }

    public function GetUpdateSpec($table, $row, $id, $data){
        DB::table($table)
            ->where($row, $id)
            ->update($data);
        return true;
    }

    public function GetUpdate2SpecColumn($table, $row, $id, $row2, $id2, $data){
        DB::table($table)
            ->where($row, $id)
            ->where($row2, $id2)
            ->update($data);
        return true;
    }

    public function GetAllRowWithoutDeleted_at($table, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->where($row, $rowdata)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query; 
    }
    // Jangan ubah, GetAllRow utk tanpa column deleted_at
    public function GetAllRow($table, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->where($row, $rowdata)
                // ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query; 
    }
    public function GetAllRowLike($table, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->where($row, 'like', '%' . $rowdata . '%')
                // ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query; 
    }
    public function GetAllRowIn($table, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->whereIn($row, $rowdata)
                // ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        return $query; 
    }
    public function GetAllRowArray($table, $row, $rowdata, $referTo, $id = 'id', $order = 'ASC'){
        $query = [];
        foreach ($rowdata as $arr) {
            $query[] = DB::table($table)
                ->where($row, $referTo)
                ->whereNull('deleted_at')
                ->orderBy($id, $order)
                ->get();
        }
        return $query; 
    }
    public function GetAllRow2($table, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->where($row, $rowdata)
                ->orderBy($id, $order)
                ->get();
        return $query; 
    }
    public function GetAllRowJoin2($table, $table2, $row, $rowdata, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->join($table2, $table . '.id', '=', $table2 . '.fk_' . $table)
                ->whereIn($table.'.'.$row, $rowdata)
                // ->whereNull('deleted_at')
                ->orderBy($table . '.' . $id, $order)
                ->get();
        return $query; 
    }
    public function GetAllRowJoin2all($table, $table2, $id = 'id', $order = 'ASC'){
        $query = DB::table($table)
                ->join($table2, $table . '.id', '=', $table2 . '.fk_' . $table)
                // ->whereIn($table.'.'.$row, $rowdata)
                // ->whereNull('deleted_at')
                ->orderBy($table . '.' . $id, $order)
                ->get();
        return $query; 
    }

    public function GetSpecRow($table, $row, $rowdata){
        $query = DB::table($table)
                ->where($row, $rowdata)
                ->get();
        return $query; 
    }
    public function GetNearestRow($table, $row, $rowdata){
        $query = DB::table($table)
                ->where($row, '>=', $rowdata) // Select rows with dates greater than or equal to $rowdata
                ->orderBy($row) // Order by date in ascending order
                ->get(); // Get the first row
        return $query; 
    }

    public function GetRow($table, $id, $data, $select = null){
        $query = DB::table($table)
                ->where($id, $data);
                
        if($select !== null) {
            $query->select($select);
        }
        
        return $query->first();
    }
    public function GetRowSport($table, $id, $data, $select = null){
        $query = DB::table($table)
                ->where($id, $data)
                ->where('deleted_at', null);
                
        if($select !== null) {
            $query->select($select);
        }
        
        return $query->first();
    }
    public function GetRowDeleteNull($table, $id, $data){
        $query = DB::table($table)
                ->where($id, $data)
                ->whereNull('deleted_at')
                ->first();
        return $query;
    }
    public function GetRowLast($table){
        $query = DB::table($table)
                ->orderBy('id', 'DESC')
                ->first();
        return $query;
    }
    
    public function GetRowDateRange($table, $row, $rowData, $startDate = null, $endDate = null){
        $query = DB::table($table)
            ->where($row, $rowData)
            ->where(function ($query) use ($startDate, $endDate) {
                if ($startDate !== null && $endDate !== null) {
                    // Check for overlap
                    $query->where('event_date', '<=', $endDate)
                        ->where('event_date_end', '>=', $startDate);
                } elseif ($startDate !== null) {
                    // Only start date specified
                    $query->where('event_date_end', '>=', $startDate);
                } elseif ($endDate !== null) {
                    // Only end date specified
                    $query->where('event_date', '<=', $endDate);
                }
            });

        $result = $query->get();
        return $result;
    }

    public function GetRowDateRangeCustom($table, $row, $rowData, $param1, $param2, $startDate = null, $endDate = null){
        // dd($table, $row, $rowData, $param1, $param2, $startDate, $endDate);
        $query = DB::table($table)
            ->where($row, $rowData)
            ->where(function ($query) use ($param1, $param2, $startDate, $endDate) { // Added $param1 and $param2 to the use statement
                if ($startDate !== null && $endDate !== null) {
                    // Check for overlap
                    $query->where($param2, '<=', $endDate)
                        ->where($param1, '>=', $startDate);
                } elseif ($startDate !== null) {
                    // Only start date specified
                    $query->where($param1, '>=', $startDate);
                } elseif ($endDate !== null) {
                    // Only end date specified
                    $query->where($param2, '<=', $endDate);
                }
            });

        $result = $query->get();
        return $result;
    }

    public function GetDelete($table, $id){
        DB::table($table)->delete($id);
        return true;
    }
    public function GetDeleteWhere($table, $col, $id){
            return DB::table($table)->where($col, $id)->delete();
        return true;
    }

    public function JoinTwoTable($table1, $table2, $sort = 'asc'){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 .'.fk_' . $table2, '=', $table2 . '.id')
            ->whereNull($table1 . '.deleted_at') 
            // ->where($table2.'.fk_lkp_status', 19)
            ->orderBy($table1 . '.id', $sort)
            ->get();
        return $query;
    }
    public function JoinTwoTable2($table1, $table2){
        $query = DB::table($table1)
            ->join($table2, $table2.'.fk_' . $table1, '=', $table1 . '.id')
            ->get();
        $query = DB::table('et_facility')
            ->join('et_facility_type', 'et_facility_type.fk_et_facility', '=', 'et_facility.id')
            ->get();
        // dd($query);
        return $query;
    }

    public function JoinTwoTableWithWhere($table1, $table2, $column, $data){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 .'.fk_' . $table2, '=', $table2 . '.id')
            // ->whereNull($table1 . '.deleted_at') 
            ->where($table2.'.'.$column, $data)
            ->orderBy($table1 . '.id', 'ASC')
            ->get();
        return $query;
    }
    public function JoinTwoTableWithWhere3($table1, $table2, $column, $data){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 .'.fk_' . $table2, '=', $table2 . '.id')
            // ->whereNull($table1 . '.deleted_at') 
            ->where($table1.'.'.$column, $data)
            ->orderBy($table1 . '.id', 'ASC')
            ->get();
        return $query;
    }

    public function JoinTwoTableWithWhere2($table1, $table2, $column, $data){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 .'.fk_main_booking', '=', $table2 . '.fk_main_booking')
            // ->whereNull($table1 . '.deleted_at') 
            ->where($table2.'.'.$column, $data)
            ->orderBy($table1 . '.id', 'ASC')
            ->get();
        return $query;
    }

    public function JoinThreeTables($table1, $table2, $table3, $sort = 'asc'){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 . '.fk_' . $table2, '=', $table2 . '.id')
            ->leftJoin($table3, $table3 . '.fk_' . $table2, '=', $table2 . '.id')
            ->whereNull($table1 . '.deleted_at') 
            // Additional conditions or where clauses if needed
            ->orderBy($table1 . '.id', $sort)
            ->get();
    
        return $query;
    }
    public function JoinThreeTablesHall($table1, $table2, $table3, $sort = 'asc'){
        $query = DB::table($table1)
        ->select($table1 . '.*', $table2 . '.*', $table3 . '.*', $table2 . '.id as ' . $table2 . '_id', $table2 . '.created_at as ' . $table2 . '_created_at', $table3 . '.created_at as ' . $table3 . '_created_at', $table2 . '.*')
            ->leftJoin($table2, $table1 . '.fk_' . $table2, '=', $table2 . '.id')
            ->leftJoin($table3, $table3 . '.fk_' . $table1, '=', $table1 . '.id')
            ->whereNull($table1 . '.deleted_at') 
            // Additional conditions or where clauses if needed
            ->orderBy($table1 . '.id', $sort)
            ->get();
    
        return $query;
    }
    
    public function JoinThreeTablesWithWhere($table1, $table2, $table3, $column, $data){
        $query = DB::table($table1)
            ->leftJoin($table2, $table1 . '.fk_' . $table2, '=', $table2 . '.id')
            ->leftJoin($table3, $table3 . '.fk_' . $table2, '=', $table2 . '.id')
            ->whereNull($table1 . '.deleted_at') 
            ->where($table2.'.'.$column, $data)
            ->orderBy($table1 . '.id', 'ASC')
            ->get();
    
        return $query;
    }
    public function JoinThreeTablesWithWhereDate($table1, $table2, $table3, $column, $data){
        $query = DB::table($table1)
            ->select($table1 . '.*', $table2 . '.created_at as ' . $table2 . '_created_at', $table3 . '.created_at as ' . $table3 . '_created_at', $table2 . '.*')
            ->leftJoin($table2, $table1 . '.fk_' . $table2, '=', $table2 . '.id')
            ->leftJoin($table3, $table3 . '.fk_' . $table2, '=', $table2 . '.id')
            ->whereNull($table1 . '.deleted_at') 
            ->whereDate($table2 . '.' . $column, '=', $data) // Assuming $column is the date column in $table2
            ->orderBy($table1 . '.id', 'ASC')
            ->get();

        return $query;
    }
    
    public function JoinThreeTablesWithWhere2($table1, $table2, $table3, $column, $data){
        $query = DB::table($table1)
            ->join($table2, $table2 . '.id', '=', $table1 . '.fk_' . $table2)
            ->join($table3, $table3 . '.id', '=', $table2 . '.fk_' . $table3)
            ->whereNull($table1 . '.deleted_at') 
            ->whereNull($table2 . '.deleted_at') 
            ->whereNull($table3 . '.deleted_at') 
            ->where($table1.'.'.$column, $data)
            ->orderBy($table1 . '.id', 'ASC')
            ->get();
    
        return $query;
    }

    public function JoinThreeTablesWithWhereAnd($table1, $table2, $table3, $column, $data, $column2, $data2){
        $query = DB::table($table1)
            ->join($table2, $table1 . '.fk_' . $table2, '=', $table2 . '.id')
            ->join($table3, $table3 . '.fk_' . $table2, '=', $table2 . '.id')
            ->whereNull($table1 . '.deleted_at');

        if (is_array($data)) {
            $query->whereIn($table2 . '.' . $column, $data);
        } else {
            $query->where($table2 . '.' . $column, $data);
        }

        $result = $query->where($table2 . '.' . $column2, $data2)
            ->orderBy($table1 . '.id', 'DESC')
            ->get();
    
        // dd($table1, $table2, $table3, $column, $data, $column2, $data2, $query, $result);
        return $result;
    }
}

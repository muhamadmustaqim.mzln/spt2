<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainBooking extends Model
{
    use HasFactory;

    protected $table = 'main_booking';

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_users');
    }
}
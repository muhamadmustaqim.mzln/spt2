<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\All;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use DateTime;

class Hall extends Model
{
    public function laporan_harian()
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        $query = DB::table('main_booking')
            ->select('*', 'main_booking.id AS id')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->whereDate('bmb_booking_date', $currentDate)
            ->get();
            // dd($query);
        return $query;
    }
    public function laporan_keseluruhan()
    {
        // $year = date('Y');
        // $month = date('m');
        // $type = 'SPT%';
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE bmb_booking_no LIKE '$type'
        // "));

        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            // ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->whereNotIn('main_booking.fk_lkp_status',['11'])
            // ->whereMonth('bh_booking.bb_start_date', '=', date('m'))
            // ->whereYear('bh_booking.bb_end_date', '=', date('Y'))
            ->select('main_booking.id','main_booking.bmb_booking_no','main_booking.bmb_booking_date','main_booking.created_at',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            // ->groupBy('bh_confirm_booking.fk_main_booking')
            ->groupBy('bh_booking.fk_main_booking')
            ->orderBy('id', 'DESC')
            ->get();
        return $query;
    }
    public function laporan_bulanan() // need update
    {
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->where('main_booking.fk_users',$user->id)
            // ->whereIn('main_booking.fk_lkp_status',['3','4','5','9','12'])
            ->whereYear('main_booking.bmb_booking_date', '=', $currentYear)
            ->whereMonth('main_booking.bmb_booking_date', '=', $currentMonth)
            // ->whereBetween(DB::raw('YEAR(main_booking.created_at)'), [$startYear, $currentYear])
            // ->whereYear('date_booking', '=', date('Y'))
            ->select(
                'main_booking.id',
                'main_booking.bmb_booking_no',
                'main_booking.fk_lkp_location',
                'main_booking.bmb_booking_date',
                'bh_booking.fk_bh_hall',
                'bh_booking.bb_start_date',
                'bh_booking.bb_end_date',
                'main_booking.fk_lkp_status',
                'main_booking.fk_users',
                'user_profiles.bud_name',
                'user_profiles.bud_phone_no',
                'bh_hall.bh_name',
                'lkp_status.ls_description',
                'lkp_status.id as statusid',
                'bh_quotation.id as quoteid')
            ->groupBy('bh_confirm_booking.fk_main_booking')
            ->get();
        // $year = date('Y');
        // $month = date('m');
        // $type = 'SPT%';
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE bmb_booking_no LIKE '$type' AND YEAR(bmb_booking_date) = $year AND MONTH(bmb_booking_date) = $month
        // "));

        // $currentDate = Carbon::now()->format('Y-m-d');
        // $query = DB::table('main_booking')
        //     ->whereDate('bmb_booking_date', $currentDate)
        //     ->get();
        return $query;
    }
    public function laporan_dalaman() // need update
    {
        $currentYear = Carbon::now()->year;
        $startYear = $currentYear - 3;
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->where('main_booking.fk_users',$user->id)
            ->whereBetween(DB::raw('YEAR(main_booking.created_at)'), [$startYear, $currentYear])
            // ->whereIn('main_booking.internal_indi',['1'])
            ->select(
                'main_booking.id',
                'main_booking.bmb_booking_no',
                'main_booking.fk_lkp_location',
                'bh_booking.fk_bh_hall',
                'bh_booking.bb_start_date',
                'bh_booking.bb_end_date',
                'main_booking.bmb_booking_date',
                'main_booking.fk_lkp_status',
                'main_booking.fk_users',
                'user_profiles.bud_name',
                'user_profiles.bud_phone_no',
                'bh_hall.bh_name',
                'lkp_status.ls_description',
                'lkp_status.id as statusid',
                'bh_quotation.id as quoteid'
            )
            ->groupBy('bh_confirm_booking.fk_main_booking')
            
            ->get();
            // dd($query);
        // $currentDate = Carbon::now()->format('Y-m-d');
        // $query = DB::table('main_booking')
        //     ->whereDate('bmb_booking_date', $currentDate)
        //     ->get();
        return $query;
    }
    public function laporan_tertunggak()
    {
        // echo $next;exit;
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            // ->where('main_booking.fk_users',$user->id)
            ->whereIn('main_booking.fk_lkp_status',['2','3','4','6','9','12'])
            // ->where('date_booking', '=', $now)
            // ->where('date_booking', '=', $next)
            ->whereDate('bh_booking.bb_start_date','>=', date("Y-m-d"))
            ->whereDate('bh_booking.bb_end_date','<=',date('Y-m-d', strtotime("+21 days")))
            ->select('main_booking.id','main_booking.bmb_booking_no',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            ->groupBy('bh_confirm_booking.fk_main_booking')
            ->get();
        // $currentDate = Carbon::now()->format('Y-m-d');
        // $query = DB::table('main_booking')
        //     ->whereDate('bmb_booking_date', $currentDate)
        //     ->get();
        return $query;
    }
    public function laporan_user($id)
    {
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            // ->join('bh_confirm_booking','bh_confirm_booking.fk_main_booking','=','main_booking.id')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            ->where('main_booking.fk_users',$id)
            // ->whereNotIn('main_booking.fk_lkp_status',['11'])
            // ->whereMonth('bh_booking.bb_start_date', '=', date('m'))
            // ->whereYear('bh_booking.bb_end_date', '=', date('Y'))
            ->select('main_booking.id','main_booking.bmb_booking_no','main_booking.bmb_booking_date','main_booking.created_at',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            // ->groupBy('bh_confirm_booking.fk_main_booking')
            ->groupBy('bh_booking.fk_main_booking')
            ->orderBY('id', 'DESC')
            ->get();
        // $year = date('Y');
        // $month = date('m');
        // $query = DB::select(DB::raw('
        //         SELECT * FROM main_booking WHERE fk_users = '.$id.' AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        //     '));
        return $query;
    }
    public function laporan_pengesahan()
    {
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            ->select('main_booking.id','main_booking.bmb_booking_no','main_booking.bmb_booking_date','main_booking.created_at',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            ->where('bmb_booking_no', 'LIKE', 'SPT%')
            ->where('fk_lkp_status', 19)
            ->groupBy('bh_booking.fk_main_booking')
            ->orderBy('id', 'DESC')
            ->get();
        return $query;
    }
    public function laporan_selesai()
    {
        $query = DB::table('main_booking')
            ->join('bh_booking','main_booking.id','=','bh_booking.fk_main_booking')
            ->join('users','users.id','=','main_booking.fk_users')
            ->join('user_profiles','user_profiles.fk_users','=','users.id')
            ->join('bh_hall','bh_hall.id','=','bh_booking.fk_bh_hall')
            ->join('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->join('bh_quotation','main_booking.id','=','bh_quotation.fk_main_booking')
            ->select('main_booking.id','main_booking.bmb_booking_no','main_booking.bmb_booking_date','main_booking.created_at',
                'main_booking.fk_lkp_location','bh_booking.fk_bh_hall','bh_booking.bb_start_date',
                'bh_booking.bb_end_date','main_booking.fk_lkp_status','main_booking.fk_users',
                'user_profiles.bud_name','user_profiles.bud_phone_no','bh_hall.bh_name',
                'lkp_status.ls_description','lkp_status.id as statusid','bh_quotation.id as quoteid')
            ->where('bmb_booking_no', 'LIKE', 'SPT%')
            ->where('fk_lkp_status', 5)
            ->groupBy('bh_booking.fk_main_booking')
            ->orderBy('id', 'DESC')
            ->get();
        return $query;
    }

    public function refundList($type, $main_booking){
        $refund = DB::table('bh_refund')->pluck('reference_no');

        if($type == 1){
            $query = DB::table('bh_payment')->whereNotIn('bp_receipt_number', $refund)->whereIn('fk_main_booking', $main_booking)->get();
        } else {
            $query = DB::table('bh_payment_fpx')->whereNotIn('fpx_serial_no', $refund)->whereIn('fk_main_booking', $main_booking)->get();
        }
        
        return $query;
    }

    public function sekatanHari($id = null, $date = null){
        $data = DB::table('bh_confirm_booking')
            ->select('main_booking.id', 'bh_confirm_booking.date_booking', 'bh_booking_detail.bbd_start_time', 'bh_booking_detail.bbd_end_time')
            ->join('main_booking', 'main_booking.id', 'bh_confirm_booking.fk_main_booking')
            ->join('bh_booking', 'bh_booking.fk_main_booking', 'main_booking.id')
            ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', 'bh_booking.id')
            ->where('bh_confirm_booking.deleted_at', NULL)
            ->orderBy('bh_confirm_booking.date_booking')
            ->when($id != null && $date != null, function ($query) use ($id, $date) {
                return $query->where('bh_confirm_booking.fk_bh_hall', $id)
                    ->where('bh_confirm_booking.date_booking', '>=', $date);
            })
        ->get();

        foreach ($data as $index => $booking) {
            $endTime = strtotime($booking->bbd_end_time);
            if (date('H', $endTime) >= 19) { // Check if end time is after 7 PM
                // Add one day to the date_booking
                $nextDay = date('Y-m-d', strtotime($booking->date_booking . ' +1 day'));
                // dd($nextDay, $index, $booking);
                
                // Insert a new row after the current index with the modified date
                // array_splice($data, $index + 1, 0, [
                //     (object)[
                //         'id' => $booking->id,
                //         'date_booking' => $nextDay,
                //         'bbd_start_time' => $booking->bbd_start_time,
                //         'bbd_end_time' => $booking->bbd_end_time,
                //     ]
                // ]);
                $temp = (object)[
                        'id' => $booking->id,
                        'date_booking' => $nextDay,
                        'bbd_start_time' => $booking->bbd_start_time,
                        'bbd_end_time' => $booking->bbd_end_time,
                ];
                $data->push($temp);
                
                // Break the loop after inserting the new row
                break;
            }
        }

        return $data;
    }

    public function fasility(){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_hall WHERE bh_status = 1 AND bh_hall_owner = 0
        "));
        return $query;
    }

    public function fasilityActive(){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_hall WHERE bh_status = 1 AND bh_hall_owner = 0  AND fk_lkp_location = 1
        "));
        return $query;
    }

    public function facility($id){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_hall WHERE bh_status = 1 AND id = $id AND bh_hall_owner = 0
        "));
        return $query;
    }

    public function hall(){
        $query = DB::select(DB::raw("
		SELECT * FROM bh_hall WHERE bh_status = 1 ORDER BY bh_name
        "));
        return $query;
    }

    public function hall2($loc, $start_date, $end_date){
        // dd($loc, $start_date, $end_date);
        // Hall - check booking
        if($loc == 4) {
            $confirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = $loc
                    AND b.id=a.fk_lkp_location
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

            $result = array();
            foreach ($confirm as $a => $b) {
                array_push($result, $b->id);
            }

            $data = DB::table('bh_hall')->whereIn('bh_hall.id',$result)
                ->where('bh_hall.bh_status','!=',0)
                ->where('fk_lkp_location',$loc)
                ->get();

            return $data;
        } 
        // Sport - check booking
        else if($loc == 5 || $loc == 6 || $loc == 7 || $loc == 8) {
            $confirm = DB::table('et_confirm_booking')->where('ecb_date_booking','>=',$start_date)
                                        ->where('ecb_date_booking','<=',$end_date)
                                        // ->where('fk_et_slot_time','!=',27)
                                        ->where('ecb_flag_indicator',2)
                                        ->groupBy('fk_et_facility_type')
                                        ->get();
            // manipulate date confirm
            // -------------------------------------------------------------------------
            $arrayconfirm = array();

            if($confirm->count()){

                foreach ($confirm as $key => $value) {

                    array_push($arrayconfirm, $value->fk_et_facility_type);

                }// end foreach
            }else{

                    array_push($arrayconfirm, 0);
            }

            // -------------------------------------------------------------------------
            $arrayconfirm = implode(",", $arrayconfirm);
            // dd($arrayconfirm);

            $data = DB::select
            (DB::raw("SELECT b.id AS id
            FROM et_facility a,et_facility_type b
            WHERE b.fk_et_facility = a.id
            AND  a.ef_type = 1
            AND b.fk_lkp_location = 4
            AND b.id NOT IN ($arrayconfirm)
            "));
            // dd($data);
            return $data;
        }


        if($start_date && $end_date) {
            //check on et_confirm_booking
            $confirm = DB::table('et_confirm_booking')
                ->where('ecb_date_booking','>=',$start_date)
                ->where('ecb_date_booking','<=',$end_date)
                // ->where('fk_et_slot_time','!=',27)
                ->where('ecb_flag_indicator',2)
                ->groupBy('fk_et_facility_type')
                ->get();
            // manipulate date confirm
            // -------------------------------------------------------------------------
            $arrayconfirm = array();
    
            if($confirm->count()){
    
                foreach ($confirm as $key => $value) {
    
                    array_push($arrayconfirm, $value->fk_et_facility_type);
    
                }// end foreach
            }else{
    
                    array_push($arrayconfirm, 0);
            }
    
    
            // -------------------------------------------------------------------------
            $arrayconfirm = implode(",", $arrayconfirm);
            // dd($arrayconfirm);
        }

        $data = DB::select
        (DB::raw("SELECT b.id AS id
        FROM et_facility a,et_facility_type b
        WHERE b.fk_et_facility = a.id
        AND  a.ef_type = 1
        AND b.fk_lkp_location = $loc
        AND b.id NOT IN ($arrayconfirm)
        "));
        // dd($data, $loc);
        $dataPrecint = [];
        foreach ($data as $d) {
            $plchdr = DB::table('et_facility')
                ->join('et_facility_type as type', 'type.fk_et_facility', '=', 'et_facility.id')
                ->where('et_facility.id', $d->id)
                ->select('et_facility.id', 'et_facility.ef_desc', 'type.*')
                ->first();
            if ($plchdr) {
                $dataPrecint[] = $plchdr;
            }
        }
        // dd($dataPrecint);
        return $dataPrecint;

    }
    
    public function hall3($loc = '', $hall = '', $start_date = '', $end_date = '', $pax = '')
    {
        if($pax == '.200'){
            $paxS = 0;
            $paxE = 200;
        } else if($pax == '.400'){
            $paxS = 201;
            $paxE = 400;
        } else if($pax == '.600'){
            $paxS = 601;
            $paxE = 800;
        } else if($pax == '.800'){
            $paxS = 800;
            $paxE = 10000;
        } else {
            $paxS = '';
            $paxE = '';
        }
        // dd($loc, $hall, $start_date, $end_date, $pax, $paxS, $paxE);
        $pagination = 8;
        $now = Carbon::now('Asia/Kuala_Lumpur');
        $dt  = $now->toDateString();

        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));

        $confirm = DB::select(DB::raw("SELECT a.id AS 'id'
            FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = $loc
            AND b.id=a.fk_lkp_location
            AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

        $result = array();
        foreach ($confirm as $a => $b) {
            array_push($result, $b->id);
        }

        $data = DB::table('bh_hall')->select('bh_hall.*', 'bh_hall_detail.bhd_capasity', 'bh_hall_detail.bhd_option')->whereIn('bh_hall.id',$result)
            ->whereNull('bh_hall.deleted_at')
            ->join('bh_hall_detail', 'bh_hall_detail.fk_bh_hall','=','bh_hall.id')
            ->where('bh_hall.bh_status','!=',0)
            ->where('fk_lkp_location',$loc)
            ->get();

        $hasValidCapacity = false;
        if($pax != '') {
            foreach ($data as $c => $d) {
                if($d->bhd_option == 1){
                    $filter = DB::table('bh_hall_usage')->where('fk_bh_hall_detail', $d->id)->get();
                    if($filter){
                        foreach($filter as $e) {
                            if($e->type == 1) {
                                if($e->capasity >= $paxS && $e->capasity <= $paxE){
                                    $data[$c]->bankuet = $e->capasity;
                                    $hasValidCapacity = true;
                                }
                            } else {
                                if($e->capasity >= $paxS && $e->capasity <= $paxE){
                                    $data[$c]->seminar = $e->capasity;
                                    $hasValidCapacity = true;
                                }
                            }
                        }
                    }
                    if (!$hasValidCapacity) {
                        unset($data[$c]);
                    }
                } else {
                    if($d->bhd_capasity >= $paxS && $d->bhd_capasity <= $paxE){
                        $hasValidCapacity = true;
                    } else {
                        $hasValidCapacity = false;
                    }
                    if (!$hasValidCapacity) {
                        unset($data[$c]);
                    }
                }
            }
        }
        if(Session('user.id') == 61894){
            // dd($paxS, $paxE, $data);
        }
        return $data;

        $able = true; // Disable
        // if($able){
            if(($pax == '') && ($loc == '') && ($hall == '') && ($start_date == '') && ($end_date == '')){
                $data = BhConfirmBooking::whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                        ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)->get();
                $dataid = array();

                foreach ($data as $key => $value) {
                    array_push($dataid, $value->fk_bh_hall);
                }

                $dataSearch = BhHall::whereNotIn('bh_hall.id',$dataid)->where('bh_hall.bh_status','!=',0)->paginate($pagination);
                return $dataSearch;
            }elseif(($pax != '')&& ($loc == '') && ($hall == '')){
                switch ($pax) {
                    case '.200':
                        $rst = VwCapasity::where('capasity','<=',200)->get();
                        break;

                    case '.400':
                        $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                        break;

                    case '.600':
                        $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                        break;

                    case '.800':
                        $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                        break;

                    case '.801':
                        $rst = VwCapasity::where('capasity','>=',801)->get();
                        break;

                    default:
                        $rst = VwCapasity::all();
                        break;
                }



                $picc = array(); // list for picc only
                $xpicc = array(); // list for except picc
                foreach ($rst as $p => $q) {
                    if($q->source == 'bh_hall_picc'){
                        $picc[$q->id]["id"] =  $q->id;
                        $picc[$q->id]["source"] =  $q->source;
                        $picc[$q->id]["capasity"] =  $q->capasity;
                    }else{
                        $xpicc[$q->id]["id"] =  $q->id;
                        $xpicc[$q->id]["source"] =  $q->source;
                        $xpicc[$q->id]["capasity"] =  $q->capasity;

                    }
                } // endforeach

                // hall yang dah confirm booking
                $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                            ->whereDate('date_booking','<=',$end_date)
                                            ->get();
                $confid = array();

                foreach ($confirm as $a => $b) {
                    array_push($confid, $b->fk_bh_hall);
                }

                $result = array();
                $detail = array();

                // dd($xpicc);
                // dd($confid);
                if($confid == NULL){

                    foreach ($xpicc as $xxx => $yyy) {
                        $result[$yyy['id']]["id"] =  $yyy['id'];
                        $result[$yyy['id']]["source"] =  $yyy['source'];
                        $result[$yyy['id']]["capasity"] =  $yyy['capasity'];

                        array_push($detail, $yyy['id']);
                    }

                }else{

                    foreach ($xpicc as $xxxx => $yyyy) {
                        // dd($yyyy['id']);
                        foreach ($confid as $x1 => $y1) {
                            if($yyyy['id'] != $y1){
                                // array_push($result, $yyyy['id']);
                                $result[$yyyy['id']]["id"] =  $yyyy['id'];
                                $result[$yyyy['id']]["source"] =  $yyyy['source'];
                                $result[$yyyy['id']]["capasity"] =  $yyyy['capasity'];

                                array_push($detail, $yyyy['id']);
                            }else{

                            }
                        }
                    } //endforeach
                } //end else

                // dd($detail);

                // for picc hall
                $piccarray = array();
                foreach ($picc as $kpicc => $vpicc) {
                    array_push($piccarray, $vpicc['id']);
                }

                // dd($piccarray);

                // $datapicc = BhHallPicc::whereIn('bh_hall_picc.id',$piccarray)->paginate($pagination);
                // dd($datapicc);
                // picc end

                // $dataSearch = BhHall::whereIn('bh_hall.id',$detail)->where('bh_hall.bh_status',1)->paginate($pagination);
                // return $dataSearch;

                $a = DB::table('bh_hall')
                    ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                    ->where('bh_hall.bh_status',1)
                    ->whereIn('id',$detail);


                $b = DB::table('bh_hall_picc')
                    ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                    ->whereIn('id',$piccarray);

                $combined = $b->union($a)->get();
                // dd($combined);
                return $combined;


            }elseif(($loc != '')&& ($pax == '') && ($hall == '')&& ($start_date !='') && ($end_date !='')){
                if(($loc == 1) || ($loc == 2)){
                    $confirm = DB::select(DB::raw("SELECT a.id AS 'id'
                            FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = $loc
                            AND b.id=a.fk_lkp_location
                            AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                    $result = array();
                    foreach ($confirm as $a => $b) {
                        array_push($result, $b->id);
                    }

                    $data = DB::table('bh_hall')->whereIn('bh_hall.id',$result)
                        ->where('bh_hall.bh_status','!=',0)
                        ->where('fk_lkp_location',$loc)
                        ->get();
                    return $data;

                }elseif($loc == 4){
                    $confirm = DB::table('bh_confirm_booking')->where('date_booking','>=',$start_date)
                            ->where('date_booking','<=',$end_date)
                            ->get();
                    $confid = array();

                    if(isset($confirm)){
                        foreach ($confirm as $a => $b) {
                            array_push($confid, $b->fk_bh_hall);
                        }
                    } //end if


                    $a = DB::table('bh_hall')
                    ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                    ->where('bh_hall.bh_status','!=',0)
                    ->whereNotIn('id',$confid);


                    $b = DB::table('bh_hall_picc')
                    ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"));


                    $combined = $a->union($b)->get();


                    // dd($combined);

                    return $combined;

                }elseif($loc == 5){
                    // dd('3.2');
                    // percint 8
                    // dd('percint 8');

                    //check on et_confirm_booking
                    $confirm = DB::table('et_confirm_booking')->where('ecb_date_booking','>=',$start_date)
                                                ->where('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();
                    // manipulate date confirm
                    // dd('confirm', $confirm);
                    // -------------------------------------------------------------------------
                    $arrayconfirm = array();

                    if($confirm->count()){
                        foreach ($confirm as $key => $value) {

                            array_push($arrayconfirm, $value->fk_et_facility_type);

                        }// end foreach
                    }else{

                            array_push($arrayconfirm, 0);
                    }


                    // -------------------------------------------------------------------------
                    $arrayconfirm = implode(",", $arrayconfirm);
                    // dd($arrayconfirm);

                    $data = DB::select
                    (DB::raw("SELECT b.id AS id
                    FROM et_facility a,et_facility_type b
                    WHERE b.fk_et_facility = a.id
                    AND  a.ef_type = 1
                    AND b.fk_lkp_location = 4
                    AND b.id NOT IN ($arrayconfirm)
                    "));
                    // dd($data);
                    return $data;

                }elseif($loc == 6){
                    // percint 9
                    // dd('percint 9');
                    $confirm = DB::table('et_confirm_booking')->where('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                    $arrayconfirm = array();

                    if($confirm->count()){

                        foreach ($confirm as $key => $value) {

                            array_push($arrayconfirm, $value->fk_et_facility_type);

                        }// end foreach
                    }else{

                            array_push($arrayconfirm, 0);
                    }


                    // -------------------------------------------------------------------------
                    $arrayconfirm = implode(",", $arrayconfirm);
                    // dd($arrayconfirm);

                    $data = DB::select
                    (DB::raw("SELECT b.id AS id
                    FROM et_facility a,et_facility_type b
                    WHERE b.fk_et_facility = a.id
                    AND  a.ef_type = 1
                    AND b.fk_lkp_location = 5
                    AND b.id NOT IN ($arrayconfirm)
                    "));

                    return $data;

                }elseif($loc == 7){

                    // percint 11
                    $confirm = DB::table('et_confirm_booking')->where('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                    $arrayconfirm = array();

                    if($confirm->count()){

                        foreach ($confirm as $key => $value) {

                            array_push($arrayconfirm, $value->fk_et_facility_type);

                        }// end foreach
                    }else{

                            array_push($arrayconfirm, 0);
                    }


                    // -------------------------------------------------------------------------
                    $arrayconfirm = implode(",", $arrayconfirm);
                    // dd($arrayconfirm);

                    $data = DB::select
                    (DB::raw("SELECT b.id AS id
                    FROM et_facility a,et_facility_type b
                    WHERE b.fk_et_facility = a.id
                    AND  a.ef_type = 1
                    AND b.fk_lkp_location = 6
                    AND b.id NOT IN ($arrayconfirm)
                    "));

                    return $data;
                    // end percint 11

                }elseif($loc == 8){
                    // percint 16
                    $confirm = DB::table('et_confirm_booking')->where('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                    $arrayconfirm = array();

                    if($confirm->count()){

                        foreach ($confirm as $key => $value) {

                            array_push($arrayconfirm, $value->fk_et_facility_type);

                        }// end foreach
                    }else{

                            array_push($arrayconfirm, 0);
                    }


                    // -------------------------------------------------------------------------
                    $arrayconfirm = implode(",", $arrayconfirm);
                    // dd($arrayconfirm);

                    $data = DB::select
                    (DB::raw("SELECT b.id AS id
                    FROM et_facility a,et_facility_type b
                    WHERE b.fk_et_facility = a.id
                    AND  a.ef_type = 1
                    AND b.fk_lkp_location = 7
                    AND b.id NOT IN ($arrayconfirm)
                    "));
                    // dd($data);
                    return $data;
                    // end percint 16

                }else{

                    // if condition loc = 3
                        // dd($source);
                        $s = date("Y-m-d", strtotime($source['start_date']));
                        $e = date("Y-m-d", strtotime($source['end_date']));

                        $start_date = $s;
                        $end_date = $e;

                        if($source['pax'] == ''){
                            $pac = 1;
                        }else{
                            $pac = 1;
                        }
                        // $pac = $source['pax'];


                        $jsonstring= 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($jsonstring);
                        $data = json_decode($str, TRUE);

                        // $data = BhHallPicc::all();

                        return $data;


                }
            }elseif(($hall != '')&& ($pax == '') && ($loc == '')){
                $data = DB::table('bh_hall')->where('bh_name','like', "%".$hall."%")->where('bh_hall.bh_status','!=',0)->get();
                // dd($data);

                $datahall = array();
                if(isset($data)){
                    foreach ($data as $k => $v) {
                        array_push($datahall, $v->id);
                    }
                }
                // dd($datahall);

                // hall yang dah confirm booking
                $confirm = DB::table('bh_confirm_booking')->where('date_booking','>=',$start_date)
                                            ->where('date_booking','<=',$end_date)
                                            ->get();
                // dd($confirm);
                $confid = array();

                if(isset($confirm)){
                    foreach ($confirm as $a => $b) {
                        array_push($confid, $b->fk_bh_hall);
                    }
                } //end if

                // dd($confid);
                // start
                $result = array();

                if($confid == NULL){

                    foreach ($datahall as $p => $q) {
                        array_push($result,$q);
                    }

                }else{

                    foreach ($datahall as $x => $y) {
                        foreach ($confid as $x1 => $y1) {
                            if($y != $y1){
                                array_push($result, $y);
                            }else{

                            }
                        }
                    } //endforeach


                } //end else
                //end
                // dd($result);

                $a = DB::table('bh_hall')
                ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                ->where('bh_hall.bh_status','!=',0)
                ->whereNotIn('id',$confid)
                ->where('bh_name','like',"%".$hall."%");


                $b = DB::table('bh_hall_picc')
                ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                ->where('bh_hall_name','like',"%".$hall."%");

                $combined = $a->union($b)->get();

                return $combined;

                // $dataSearch = BhHall::whereIn('bh_hall.id',$result)->where('bh_hall.bh_status',1)->paginate($pagination);
                // return $dataSearch;
                // dd($result);

            }elseif(($loc != '')&& ($pax != '') && ($hall == '')&& ($start_date !='') && ($end_date !='')){
                switch ($pax) {
                    case '.200':
                        $rst = VwCapasity::where('capasity','<=',200)->get();
                        break;

                    case '.400':
                        $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                        break;

                    case '.600':
                        $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                        break;

                    case '.800':
                        $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                        break;

                    case '.801':
                        $rst = VwCapasity::where('capasity','>=',801)->get();
                        break;

                    default:
                        $rst = VwCapasity::all();
                        break;
                }

                $picc = array(); // list for picc only
                $ppj = array();
                $marina = array();
                foreach ($rst as $p => $q) {
                    if($q->source == 'bh_hall_picc'){
                        array_push($picc,$q->id);
                    }else{
                        // $xpicc[$q->id]["id"] =  $q->id;
                        // $xpicc[$q->id]["source"] =  $q->source;
                        // $xpicc[$q->id]["capasity"] =  $q->capasity;
                        $loca = BhHall::where('bh_hall.id',$q->id)->where('bh_hall.bh_status','!=',0)->select('fk_lkp_location')->first();
                        if($loca != null){
                                if($loca->fk_lkp_location == 1){
                                // $ppj[$q->id]["id"] =  $q->id;
                                // $ppj[$q->id]["source"] =  $q->source;
                                // $ppj[$q->id]["capasity"] =  $q->capasity;
                                    array_push($ppj, $q->id);
                            }else{
                                // $marina[$q->id]["id"] =  $q->id;
                                // $marina[$q->id]["source"] =  $q->source;
                                // $marina[$q->id]["capasity"] =  $q->capasity;
                                    array_push($marina, $q->id);
                            }
                        }
                    }
                } // endforeach
                // end capasity
                switch ($loc) {
                    case 1:
                        //ppj
                        $ppj = implode(",", $ppj);
                        $ppjlist = array();

                        $ppj = explode(",", $ppj);
                        // dd($ppjconfirm);
                        if(count($ppjconfirm) > 0){
                            foreach ($ppj as $x1 => $y1){
                                foreach ($ppjconfirm as $xxx => $yyy) {
                                    if($yyy->id == $y1){
                                        array_push($ppjlist, $y1);
                                    }
                                }
                                // array_push($ppjlist, $yyy->id);
                            }
                        }else{
                        }
                        $res = BhHall::whereIn('bh_hall.id',$ppjlist)
                            ->where('bh_hall.fk_lkp_location','!=',0)
                            ->get();
                        return $res;
                        break;

                    case 2:
                        if($marina != null ){
                            // echo "ada";exit;
                            $marina = implode(",", $marina);
                        }else{
                            $marina = 0;
                            // echo "takda";exit;
                        }

                        $marinaconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status!= 0 AND a.fk_lkp_location = 2
                        AND b.id=a.fk_lkp_location
                        AND a.id IN ($marina)
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                        // dd($marinaconfirm);



                        $marinalist = array();

                        if($marina != null ){
                            $marina = explode(",", $marina);
                        }else{
                            $marina = 0;
                        }

                        // dd($marina);

                        if(count($marinaconfirm) > 0){

                            foreach ($marina as $xa => $ya){
                                foreach ($marinaconfirm as $xx => $yy) {
                                    if($yy->id == $ya){
                                        array_push($marinalist, $ya);
                                    }
                                }
                                // array_push($ppjlist, $yyy->id);
                            }

                        }else{


                        }

                        // dd($marinalist);

                        $res = BhHall::whereIn('bh_hall.id',$marinalist)
                        ->where('bh_hall.fk_lkp_location',2)
                        ->get();

                        return $res;
                        break;

                    case 3:
                        $s = date("Y-m-d", strtotime($source['start_date']));
                        $e = date("Y-m-d", strtotime($source['end_date']));

                        $start_date = $s;
                        $end_date = $e;


                        if($source['pax'] == '.200'){
                            $pac = 1;
                            $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                            $str = file_get_contents($duaratus);

                            $manipulate = json_decode($str, TRUE);
                            $data = array();
                            $i = 0;
                            foreach ($manipulate as $k => $v) {

                                if($v['FAC_CAPACITY'] <= 200 ){
                                    $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                    $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                    $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                    $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                                $i++;
                                }
                            }
                        }elseif($source['pax'] == '.400'){
                            // $pac = 1;
                            // dd('201 --> 400');
                            $pac = 1;
                            $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                            $str = file_get_contents($duaratus);

                            $manipulate = json_decode($str, TRUE);

                            // dd($manipulate);

                            $data = array();
                            $i = 0;
                            foreach ($manipulate as $k => $v) {

                                if(($v['FAC_CAPACITY'] >= 201 ) AND ($v['FAC_CAPACITY'] <= 400)){
                                    $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                    $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                    $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                    $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                                $i++;
                                }
                            }



                        }elseif($source['pax'] == '.600'){
                            // dd('401 --> 600');
                            $pac = 1;
                            $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                            $str = file_get_contents($duaratus);

                            $manipulate = json_decode($str, TRUE);

                            // dd($manipulate);

                            $data = array();
                            $i = 0;
                            foreach ($manipulate as $k => $v) {

                                if(($v['FAC_CAPACITY'] >= 401 ) AND ($v['FAC_CAPACITY'] <= 600)){
                                    $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                    $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                    $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                    $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                                $i++;
                                }
                            }



                        }elseif($source['pax'] == '.800'){
                            // dd('601 --> 800');

                            $pac = 1;
                            $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                            $str = file_get_contents($duaratus);

                            $manipulate = json_decode($str, TRUE);

                            // dd($manipulate);

                            $data = array();
                            $i = 0;
                            foreach ($manipulate as $k => $v) {

                                if(($v['FAC_CAPACITY'] >= 601 ) AND ($v['FAC_CAPACITY'] <= 800)){
                                    $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                    $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                    $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                    $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                                $i++;
                                }
                            }



                        }elseif($source['pax'] == '.801'){
                            // dd('800 --> infiniti');

                            $pac = 1;
                            $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                            $str = file_get_contents($duaratus);

                            $manipulate = json_decode($str, TRUE);

                            // dd($manipulate);

                            $data = array();
                            $i = 0;
                            foreach ($manipulate as $k => $v) {

                                if($v['FAC_CAPACITY'] >= 801){
                                    $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                    $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                    $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                    $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                                $i++;
                                }
                            }



                        }else{
                            // dd('semua');
                        }
                        return $data;

                        break;
                    case 4:
                        //lokasi semua
                        // dd($rst);
                        // dd($ppj);
                        // dd($marina);
                        $dt = array();
                        foreach ($ppj as $aaa => $bbb) {
                            array_push($dt,$bbb);
                        }
                        foreach ($marina as $aaaa => $bbbb) {
                            array_push($dt,$bbbb);
                        }
                        $dpicc = array();
                        foreach ($picc as $aaaaa => $bbbbb) {
                            array_push($dpicc,$bbbbb);
                        }

                        // dd($dpicc);
                        $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                                                ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                                ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                                                ->where('bh_hall.fk_lkp_location',1)
                                                ->orWhere('bh_hall.fk_lkp_location',2)
                                                ->get();
                        $confid = array();

                        if(isset($confirm)){
                            foreach ($confirm as $a => $b) {
                                array_push($confid, $b->fk_bh_hall);
                            }
                        } //end if
                        // dd($confid);


                        $a = DB::table('bh_hall')
                        ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                        ->where('bh_hall.bh_status','!=',0)
                        ->whereIn('id',$dt)
                        ->whereNotIn('id',$confid);


                        $b = DB::table('bh_hall_picc')
                        ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                        ->whereIn('id',$dpicc);



                        $combined = $a->union($b)->get();
                        return $combined;
                        // dd($combined);
                        // dd('semua');
                        break;


                    case 5 :

                            $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                            $arrayconfirm = array();

                            if($confirm->count()){

                                foreach ($confirm as $key => $value) {

                                    array_push($arrayconfirm, $value->fk_et_facility_type);

                                }// end foreach
                            }else{

                                    array_push($arrayconfirm, 0);
                            }


                            // -------------------------------------------------------------------------
                            $arrayconfirm = implode(",", $arrayconfirm);
                            // dd($arrayconfirm);

                            $data = DB::select
                            (DB::raw("SELECT b.id AS id
                            FROM et_facility a,et_facility_type b
                            WHERE b.fk_et_facility = a.id
                            AND  a.ef_type = 1
                            AND b.fk_lkp_location = 4
                            AND b.id NOT IN ($arrayconfirm)
                            "));

                            return $data;

                        break;

                    case 6 :

                            $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                            $arrayconfirm = array();

                            if($confirm->count()){

                                foreach ($confirm as $key => $value) {

                                    array_push($arrayconfirm, $value->fk_et_facility_type);

                                }// end foreach
                            }else{

                                    array_push($arrayconfirm, 0);
                            }


                            // -------------------------------------------------------------------------
                            $arrayconfirm = implode(",", $arrayconfirm);
                            // dd($arrayconfirm);

                            $data = DB::select
                            (DB::raw("SELECT b.id AS id
                            FROM et_facility a,et_facility_type b
                            WHERE b.fk_et_facility = a.id
                            AND  a.ef_type = 1
                            AND b.fk_lkp_location = 5
                            AND b.id NOT IN ($arrayconfirm)
                            "));

                            return $data;

                        break;

                    case 7 :
                            $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                            $arrayconfirm = array();

                            if($confirm->count()){

                                foreach ($confirm as $key => $value) {

                                    array_push($arrayconfirm, $value->fk_et_facility_type);

                                }// end foreach
                            }else{

                                    array_push($arrayconfirm, 0);
                            }


                            // -------------------------------------------------------------------------
                            $arrayconfirm = implode(",", $arrayconfirm);
                            // dd($arrayconfirm);

                            $data = DB::select
                            (DB::raw("SELECT b.id AS id
                            FROM et_facility a,et_facility_type b
                            WHERE b.fk_et_facility = a.id
                            AND  a.ef_type = 1
                            AND b.fk_lkp_location = 6
                            AND b.id NOT IN ($arrayconfirm)
                            "));

                            return $data;

                        break;

                    case 8 :

                            $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                                ->whereDate('ecb_date_booking','<=',$end_date)
                                                // ->where('fk_et_slot_time','!=',27)
                                                ->where('ecb_flag_indicator',2)
                                                ->groupBy('fk_et_facility_type')
                                                ->get();

                            $arrayconfirm = array();

                            if($confirm->count()){

                                foreach ($confirm as $key => $value) {

                                    array_push($arrayconfirm, $value->fk_et_facility_type);

                                }// end foreach
                            }else{

                                    array_push($arrayconfirm, 0);
                            }


                            // -------------------------------------------------------------------------
                            $arrayconfirm = implode(",", $arrayconfirm);
                            // dd($arrayconfirm);

                            $data = DB::select
                            (DB::raw("SELECT b.id AS id
                            FROM et_facility a,et_facility_type b
                            WHERE b.fk_et_facility = a.id
                            AND  a.ef_type = 1
                            AND b.fk_lkp_location = 7
                            AND b.id NOT IN ($arrayconfirm)
                            "));

                            return $data;

                        break;

                    default:
                        # code...
                        break;
                }
                exit;
            }elseif(($loc != '')&& ($pax != '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
                // for loc !=null,pax!=null and hall !=null
                switch ($pax) {
                    case '.200':
                        $rst = VwCapasity::where('capasity','<=',200)->get();
                        break;

                    case '.400':
                        $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                        break;

                    case '.600':
                        $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                        break;

                    case '.800':
                        $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                        break;

                    case '.801':
                        $rst = VwCapasity::where('capasity','>=',801)->get();
                        break;

                    default:
                        $rst = VwCapasity::all();
                        break;
                }
                // dd($rst);

                $picc = array(); // list for picc only
                // $xpicc = array();
                $ppj = array();
                $marina = array();
                                // list for except picc
                foreach ($rst as $p => $q) {
                    if($q->source == 'bh_hall_picc'){
                        // $picc[$q->id]["id"] =  $q->id;
                        // $picc[$q->id]["source"] =  $q->source;
                        // $picc[$q->id]["capasity"] =  $q->capasity;
                        array_push($picc, $q->id);
                    }else{
                        // $xpicc[$q->id]["id"] =  $q->id;
                        // $xpicc[$q->id]["source"] =  $q->source;
                        // $xpicc[$q->id]["capasity"] =  $q->capasity;
                        $loca = BhHall::where('bh_hall.id',$q->id)->select('fk_lkp_location')->first();
                        if($loca->fk_lkp_location == 1){
                            // $ppj[$q->id]["id"] =  $q->id;
                            // $ppj[$q->id]["source"] =  $q->source;
                            // $ppj[$q->id]["capasity"] =  $q->capasity;
                            array_push($ppj, $q->id);
                        }else{
                            // $marina[$q->id]["id"] =  $q->id;
                            // $marina[$q->id]["source"] =  $q->source;
                            // $marina[$q->id]["capasity"] =  $q->capasity;
                            array_push($marina, $q->id);
                        }

                    }
                } // endforeach
                // end capasity

                // dd($marina);
                // dd($ppj);
                // dd($xpicc);
                // dd($loc);

                // start location
                switch ($loc) {
                    case 1:
                        //ppj
                        // $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                        // ->where('bh_hall.fk_lkp_location',1)
                        // ->where('bh_hall.bh_status',1)
                        // ->select('bh_hall.id')
                        // ->get();

                        // hall yang dah confirm booking
                        // $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                        //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                        //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                        //                         ->where('bh_hall.fk_lkp_location',1)
                        //                         ->get();
                        // dd($ppj);

                        $ppj = implode(",", $ppj);

                        $ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status !=0 AND a.fk_lkp_location = 1
                        AND b.id=a.fk_lkp_location
                        AND a.bh_name like '%$hall%'
                        AND a.id IN ($ppj)
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                        // dd($ppjconfirm);

                        $ppjlist = array();

                        $ppj = explode(",", $ppj);

                        if(count($ppjconfirm) > 0){

                            foreach ($ppj as $x1 => $y1){
                                foreach ($ppjconfirm as $xxx => $yyy) {
                                    if($yyy->id == $y1){
                                        array_push($ppjlist, $y1);
                                    }
                                }
                                // array_push($ppjlist, $yyy->id);
                            }

                        }else{


                        }

                        // $confid = array();

                        // if(isset($confirm)){
                        //     foreach ($confirm as $a => $b) {
                        //         array_push($confid, $b->id);
                        //     }
                        // } //end if
                        // echo "confid";
                        // dd($ppj);
                        // dd($confid);

                        // $result = array();

                        // if($confid == NULL){

                        //     foreach ($ppj as $xx => $yy) {
                        //         array_push($result, $yy['id']);
                        //     }
                        // }else{

                        //     foreach ($ppj as $x => $y) {
                        //         foreach ($confid as $x1 => $y1) {
                        //             if($y['id'] != $y1){
                        //                  array_push($result, $y);
                        //             }else{

                        //             }
                        //         }
                        //     } //endforeach
                        // } //end else
                        // dd($result);
                        $res = BhHall::whereIn('bh_hall.id',$ppjlist)
                        ->where('bh_hall.fk_lkp_location',1)
                        ->get();
                        return $res;
                        break;

                    case 2:
                        // marina
                        // $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                        // ->where('bh_hall.fk_lkp_location',2)
                        // ->where('bh_hall.bh_status',1)
                        // ->select('bh_hall.id')
                        // ->get();

                        // dd($marina);

                        if($marina != null){
                        $marina = implode(",", $marina);
                        }else{
                        // $array[] = 0;
                        // $marina = implode(",", $marina);
                        $marina = 0;
                        }
                        // dd($marina);

                        $marinaconfirm  = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = 2
                        AND a.bh_name like '%$hall%'
                        AND b.id=a.fk_lkp_location
                        AND a.id IN ($marina)
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                        // dd($marinaconfirm);

                        // hall yang dah confirm booking
                        // $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                        //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                        //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                        //                         ->where('bh_hall.fk_lkp_location',2)
                        //                         ->get();
                        // $confid = array();

                        // if(isset($confirm)){
                        //     foreach ($confirm as $a => $b) {
                        //         array_push($confid, $b->id);
                        //     }
                        // } //end if

                        // $result = array();

                        // if($confid == NULL){

                        //     foreach ($marina as $xx => $yy) {
                        //         array_push($result, $yy['id']);
                        //     }
                        // }else{

                        //     foreach ($marina as $x => $y) {
                        //         foreach ($confid as $x1 => $y1) {
                        //             if($y['id'] != $y1){
                        //                  array_push($result, $y);
                        //             }else{

                        //             }
                        //         }
                        //     } //endforeach
                        // } //end else

                        $marinalist = array();

                        if(count($marinaconfirm) > 0){

                            foreach ($marinaconfirm as $t => $y) {
                                array_push($marinalist,$y->id);
                            }
                        }else{


                        }


                        // dd($marinalist);
                        $res = BhHall::whereIn('bh_hall.id',$marinalist)
                        ->where('bh_hall.bh_status','!=',0)
                        ->where('bh_hall.fk_lkp_location',2)
                        ->where('bh_name','like',"%".$hall."%")->get();
                        return $res;


                        break;

                    case 3:
                        // dd($picc);
                        if($picc != null){
                        $picc = implode(",", $picc);
                        }else{
                        // $array[] = 0;
                        // $marina = implode(",", $marina);
                        $picc = 0;
                        }

                        // $piccconfirm = BhHallPicc ::whereIn('bh_hall_picc.id',$picc)
                        //         ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                        //         ->select('bh_hall_picc.id')
                        //         ->get();

                        $piccconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall_picc a
                        WHERE a.bh_hall_name like '%$hall%'
                        AND a.id IN ($picc)"));

                        // dd($piccconfirm);

                        $picclist = array();

                        if(count($piccconfirm) > 0){

                            foreach ($piccconfirm as $t => $y) {
                                array_push($picclist,$y->id);

                            }
                        }else{


                        }

                        // foreach ($picc as $pa => $pb) {
                        //         array_push($result, $pb['id']);
                        //     }

                        $data = BhHallPicc::whereIn('bh_hall_picc.id',$picclist)
                        ->where('bh_hall_name','like',"%".$hall."%")->get();
                        // dd($data);
                        return $data;
                        break;

                    case 4:
                        //lokasi semua
                        // dd($rst);
                        // dd($ppj);
                        // dd($marina);
                        $dt = array();
                        foreach ($ppj as $aaa => $bbb) {
                            array_push($dt,$bbb['id']);
                        }
                        foreach ($marina as $aaaa => $bbbb) {
                            array_push($dt,$bbbb['id']);
                        }
                        $dpicc = array();
                        foreach ($picc as $aaaaa => $bbbbb) {
                            array_push($dpicc,$bbbbb['id']);
                        }

                        // dd($dpicc);

                        $a = DB::table('bh_hall')
                        ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                        ->where('bh_hall.bh_status','!=',0)
                        ->whereIn('id',$dt)
                        ->where('bh_name','like',"%".$hall."%");


                        $b = DB::table('bh_hall_picc')
                        ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                        ->whereIn('id',$dpicc)
                        ->where('bh_hall_name','like',"%".$hall."%");



                        $combined = $a->union($b)->get();
                        return $combined;
                        // dd($combined);
                        // dd('semua');
                        break;

                    default:
                        # code...
                        break;
                }
                // end location



            }elseif(($loc == '')&& ($pax != '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
                // dd('hahah');
                // kapasiti
                switch ($pax) {
                    case '.200':
                        $rst = VwCapasity::where('capasity','<=',200)->get();
                        break;

                    case '.400':
                        $rst = VwCapasity::where('vw_capasity.capasity','>',201)->where('vw_capasity.capasity','<=',400)->get();
                        break;

                    case '.600':
                        $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                        break;

                    case '.800':
                        $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                        break;

                    case '.801':
                        $rst = VwCapasity::where('capasity','>=',801)->get();
                        break;

                    default:
                        $rst = VwCapasity::all();
                        break;
                }
                // end kapasiti
                // dd($rst);
                $picc = array(); // list for picc only
                // $xpicc = array();
                $ppj = array();
                $marina = array();
                                // list for except picc
                foreach ($rst as $p => $q) {
                    if($q->source == 'bh_hall_picc'){
                        // $picc[$q->id]["id"] =  $q->id;
                        // $picc[$q->id]["source"] =  $q->source;
                        // $picc[$q->id]["capasity"] =  $q->capasity;
                        array_push($picc, $q->id);
                    }else{
                        // $xpicc[$q->id]["id"] =  $q->id;
                        // $xpicc[$q->id]["source"] =  $q->source;
                        // $xpicc[$q->id]["capasity"] =  $q->capasity;
                        $loca = BhHall::where('bh_hall.id',$q->id)->select('fk_lkp_location')->first();
                        if($loca->fk_lkp_location == 1){
                            // $ppj[$q->id]["id"] =  $q->id;
                            // $ppj[$q->id]["source"] =  $q->source;
                            // $ppj[$q->id]["capasity"] =  $q->capasity;
                            array_push($ppj, $q->id);
                        }else{
                            // $marina[$q->id]["id"] =  $q->id;
                            // $marina[$q->id]["source"] =  $q->source;
                            // $marina[$q->id]["capasity"] =  $q->capasity;
                            array_push($marina, $q->id);
                        }

                    }
                } // endforeach

                $ppj = implode(",", $ppj);

                $ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status !=0 AND a.fk_lkp_location = 1
                        AND a.bh_name like '%$hall%'
                        AND b.id=a.fk_lkp_location
                        AND a.id NOT IN ($ppj)
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));
                // dd($ppjconfirm);
                // dd($ppj);
                $ppjlist = array();

                $ppj = explode(",", $ppj);
                // dd($ppj);
                if(count($ppjconfirm) > 0){
                    // foreach ($ppjconfirm as $kppj => $vppj) {
                    //     foreach ($ppj as $ppja => $ppjb) {
                    //         if($vppj->id != intval($ppjb)){
                    //             array_push($ppjlist,intval($ppjb));
                    //         }
                    //     }

                    // }
                    foreach ($ppjconfirm as $xxx => $yyy) {
                        array_push($ppjlist, $yyy->id);
                    }

                }else{

                    // foreach ($ppj as $kp => $vp) {
                    //         array_push($ppjlist, intval($vp));
                    //     }
                }

                // dd($ppjlist);

                $a =  BhHall::where('bh_hall.bh_status','!=',0)
                                ->where('bh_hall.fk_lkp_location',1)
                                ->whereIn('bh_hall.id',$ppjlist)
                                ->where('bh_name','like',"%".$hall."%")
                                ->select('bh_hall.id')
                                ->get();
                // dd($a);
                // -------------------------PPJ--------------------------------------------------------------
                    // -------------------------MARINA-----------------------------------------------------------
                // $marinaconfirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                //                         ->where('bh_hall.bh_status',1)
                //                         ->where('bh_hall.fk_lkp_location',2)
                //                         // ->where('bh_name','like',"%".$hall."%")
                //                         ->select('bh_hall.id')
                //                         ->get();

                // dd($marina);
                if($marina > 0){
                    $marina = implode(",", $marina);
                }else{
                    // $marina = 0;
                }


                $marinaconfirm  = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = 2
                        AND a.bh_name like '%$hall%'
                        AND b.id=a.fk_lkp_location
                        -- AND a.id NOT IN ($marina)
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                // dd($marinaconfirm);
                // dd($marina);
                $marinalist = array();

                if(count($marinaconfirm) > 0){
                    // foreach ($marinaconfirm as $kmarina => $vmarina) {
                    //     foreach ($marina as $marinaa => $marinab) {
                    //         if($vmarina->id != $marinab){
                    //             array_push($marinalist,$marinab);
                    //         }
                    //     }

                    // }
                    foreach ($marinaconfirm as $t => $y) {
                        array_push($marinalist,$y->id);
                    }
                }else{
                    // $marina = explode(",", $marina);
                        // foreach ($marina as $kmar => $vmar) {
                        //     array_push($marinalist, $vmar);
                        // }

                }
                // $marinaconfirm = DB::select(DB::raw("select bh_hall.id from bh_confirm_booking
                //              inner join bh_hall on bh_hall.id = bh_confirm_booking.fk_bh_hall
                //              where date(bh_confirm_booking.date_booking) >= $start_date
                //              and date(bh_confirm_booking.date_booking) <= $end_date
                //              and bh_hall.bh_status = 1 and bh_hall.fk_lkp_location = 2 ;"));

                // dd(count($marinaconfirm));

                // dd($marinalist);

                $b =  BhHall::where('bh_hall.bh_status','!=',0)
                                ->where('bh_hall.fk_lkp_location',2)
                                ->whereIn('bh_hall.id',$marinalist)
                                ->where('bh_name','like',"%".$hall."%")
                                ->select('bh_hall.id')
                                ->get();
                // dd($b);
                    //-----------------------------MARINA----------------------------------------------------------------

                $c = BhHallPicc ::whereIn('bh_hall_picc.id',$picc)
                                ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                                ->select('bh_hall_picc.id')
                                ->get();
                // dd($a);

                $hallid = array();
                foreach ($a as $keya => $valuea) {
                    // array_push($hallid, $valuea->id);
                    $hallid[$valuea->id]['id'] = $valuea->id;
                    $hallid[$valuea->id]['source'] = 'ppj';
                }
                foreach ($b as $keyb => $valueb) {
                    //array_push($hallid, $valueb->id);
                    $hallid[$valueb->id]['id'] = $valueb->id;
                    $hallid[$valueb->id]['source'] = 'marina';
                }
                foreach ($c as $keyc => $valuec) {
                    // array_push($hallid,$valuec->id);
                    $hallid[$valuec->id]['id'] = $valuec->id;
                    $hallid[$valuec->id]['source'] = 'picc';
                }

                // dd($hallid);
                return $hallid;


            }elseif(($loc != '')&& ($pax == '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
                // start location
                switch ($loc) {
                    case 1:
                        //ppj
                        $data = DB::select(DB::raw("SELECT a.id AS 'id',a.bh_name AS 'nama', b.lc_description AS 'lokasi'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = $loc
                        AND a.bh_name like '%$hall%'
                        AND b.id=a.fk_lkp_location
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                        $halldata = array();
                        foreach ($data as $d => $e) {
                            array_push($halldata,$e->id);
                        }

                        // dd($halldata);

                        $res = BhHall::whereIn('bh_hall.id',$halldata)
                                        ->where('bh_status','!=',0)
                                        ->get();
                        // dd($res);
                        return $res;
                        break;

                    case 2:
                        // marina
                        $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                        ->where('bh_hall.fk_lkp_location',2)
                        ->where('bh_hall.bh_name','like',"%".$hall."%")
                        ->where('bh_hall.bh_status','!=',0)
                        ->select('bh_hall.id')
                        ->get();


                        // hall yang dah confirm booking
                        $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                                                ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                                ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                                                ->where('bh_hall.fk_lkp_location',2)
                                                ->get();
                        $confid = array();

                        if(count($confirm) > 0){

                            foreach ($data as $vkey => $vvalue) {
                                foreach ($confirm as $ckey => $cvalue) {
                                    if($vvalue->id != $cvalue->id){
                                        array_push($confid, $vvalue->id);
                                    }
                                }
                            }

                        }else{
                            foreach ($data as $k => $v) {
                                array_push($confid, $v->id);
                            }
                        }


                        $res = BhHall::whereIn('bh_hall.id',$confid)
                        ->where('bh_hall.bh_status','!=',0)
                        ->get();
                        return $res;


                        break;

                    case 3:

                        $data = BhHallPicc::where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                        ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                        ->select('bh_hall_picc.id')
                        ->get();


                        return $data;
                        break;

                    case 4:
                        //lokasi semua
                        $data = BhHall::where('bh_name','like', "%".$hall."%")->where('bh_hall.bh_status',1)->get();
                        // dd($data);

                        $datahall = array();
                        if(isset($data)){
                            foreach ($data as $k => $v) {
                                array_push($datahall, $v->id);
                            }
                        }
                        // dd($datahall);

                        // hall yang dah confirm booking
                        $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                                    ->whereDate('date_booking','<=',$end_date)
                                                    ->get();

                        $confid = array();

                        if(isset($confirm)){
                            foreach ($confirm as $a => $b) {
                                array_push($confid, $b->fk_bh_hall);
                            }
                        } //end if

                        // start
                        $result = array();

                        if($confid == NULL){

                            foreach ($datahall as $p => $q) {
                                array_push($result,$q);
                            }

                        }else{

                            foreach ($datahall as $x => $y) {
                                foreach ($confid as $x1 => $y1) {
                                    if($y != $y1){
                                        array_push($result, $y);
                                    }else{

                                    }
                                }
                            } //endforeach


                        } //end else
                        //end

                        $a = DB::table('bh_hall')
                        ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                        ->where('bh_hall.bh_status','!=',0)
                        ->whereNotIn('id',$confid)
                        ->where('bh_name','like',"%".$hall."%");


                        $b = DB::table('bh_hall_picc')
                        ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                        ->where('bh_hall_name','like',"%".$hall."%");

                        $combined = $a->union($b)->get();

                        return $combined;
                        // dd($combined);
                        break;

                    default:
                        # code...
                        break;
                }
                // end location
            }else{
                $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                ->where('bh_hall.bh_status','!=',0)
                ->where('bh_hall.fk_lkp_location',1)
                ->select('bh_hall.*')
                ->get();

                $hallid = array();
                foreach ($data as $key => $value) {

                    array_push($hallid, $value->id);
                }

                $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                            ->whereDate('date_booking','<=',$end_date)
                                            ->get();

                $confid = array();

                foreach ($confirm as $a => $b) {
                    array_push($confid, $b->id);
                }

                $result = array();

                if($confid == NULL){

                    foreach ($hallid as $bb => $cc) {
                        array_push($result,$cc);
                    }

                }else{

                    foreach ($hallid as $x => $y) {
                        foreach ($confid as $x1 => $y1) {
                            if($y != $y1){
                                array_push($result, $y);
                            }else{

                            }
                        }
                    } //endforeach


                } 
                $dataSearch = BhHall::whereIn('bh_hall.id',$result)->where('bh_hall.bh_status','!=',0)->paginate($pagination);
                return $dataSearch;
            }
        // }
    }

    public function hallById($id){
        $query = DB::select(DB::raw("
        SELECT * FROM bh_hall WHERE STATUS = 1 AND id = :id ORDER BY bh_name
        "), ['id' => $id]);
        return isset($query[0]) ? $query[0] : null;
    }

    public function halli($id){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_location WHERE status = 1 AND id = $id
        "));
        return $query;
    }

    public function details($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE id = $id AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }

    public function price($id){
        $query = DB::select(DB::raw("
            SELECT DISTINCT c.efd_name, d.efp_unit_price, d.fk_lkp_slot_cat, d.efp_day_cat  FROM et_facility_type AS a
            JOIN et_facility AS b ON b.id = a.fk_et_facility
            JOIN et_facility_detail AS c ON c.fk_et_facility_type = a.id
            JOIN et_facility_price AS d ON d.fk_et_facility = b.id
            WHERE a.id = $id
        "));

        return $query;
    }

    public function getRn() {
        $running = All::GetRow('lkp_sequence', 'id', 1);
        $rn = $running->booking_no;
        $newcount1 = $rn + 1;
        $newcount = 0;
        if ($newcount1 > 99999) {
            $newcount = 1;
        } else {
            $newcount = $newcount1;
        }
        $data = array(
            'booking_no'        => $newcount,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_sequence', 1, $data);
        return $rn;
    }

    public function laporan_acara($id, $data, $data1){
        $query = DB::select(DB::raw('
            SELECT b1.bmb_booking_no, c1.fullname, c1.email, a1.fk_main_booking, a1.event_date, a1.event_date_end, a1.enter_time, a1.exit_time
            FROM spa_booking_event as a1
            JOIN main_booking AS b1 ON b1.id = a1.fk_main_booking
            JOIN users AS c1 ON c1.id = b1.fk_users
            WHERE a1.fk_bh_hall = "'.$id.'" AND a1.event_date = "'.$data.'" AND a1.event_date_end ="'.$data1.'"
        '));

        return $query;
        // SELECT DISTINCT d1.bmb_booking_no ,c1.*,h1.fullname, h1.email, i1.bud_reference_id, i1.bud_phone_no, g1.efd_name
        // FROM et_sport_time AS a1
        // JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
        // JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
        // JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
        // JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
        // JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
        // JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
        // JOIN users AS h1 ON h1.id = d1.fk_users
        // JOIN user_profiles AS i1 ON i1.fk_users = h1.id
        // WHERE d1.fk_lkp_location = '.$id.' AND d1.fk_lkp_status IN (5,11) AND b1.esb_booking_date BETWEEN "'.$data.'" AND "'.$data1.'"
        // ORDER BY c1.ebf_start_date DESC
    }

    public function priceSlot($id) {
        $query = DB::select(DB::raw('
            SELECT DISTINCT a.fk_bh_hall, a.bb_start_date, c.* FROM bh_booking AS a
            JOIN bh_booking_detail AS b ON b.fk_bh_booking = a.id
            JOIN lkp_time_session AS c ON c.id = b.fk_lkp_time_session
            JOIN bh_hall AS d ON d.id = a.fk_bh_hall
            JOIN bh_hall_detail AS e ON e.fk_bh_hall = d.id
            WHERE a.fk_main_booking = '.$id.'
        '));
        // dd($query);
        return $query;
    }

    public function generatetransid() {
        $data = All::GetRow('lkp_sequence', 'id', 1);
        $no = $data->fpx_serial_trans;
        if (($no < 9999)) {
            $run_no = $no + 1;
            /*-----------update lkpsequence-----------*/
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        } else {
            $run_no = 1;
            /*-----------update lkpsequence-----------*/
            $b = array(
                'fpx_serial_trans' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        }
        while (strlen($run_no) <= 3) {
            $run_no = '0' . $run_no;
        }
        return $run_no;
    }
	
	public function slot($id, $data){
        
		$query = DB::select(DB::raw('
            SELECT DISTINCT b.id AS "fidd",b.efd_name,d.est_slot_time,d.id AS "fkst", d.fk_lkp_slot_cat AS "sesi", c.efp_unit_price AS "harga", c.fk_lkp_gst_rate AS "gst", c.id AS "cidd", d.start
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = '.$id.' AND efd_status = 1 AND c.efp_day_cat = 1
            AND (d.id, b.efd_name) NOT IN (
                SELECT f1.id AS "aaa", g1.efd_name AS "bbb"
                FROM et_sport_time AS a1
                JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                WHERE b1.esb_booking_date = "'.$data.'" AND c1.fk_et_facility_type = '.$id.' AND d1.fk_lkp_status IN (5,11,2) AND g1.fk_et_facility_type = '.$id.'
                UNION
                SELECT a2.fk_et_slot_time AS "aaa", b2.efd_name AS "bbb"
                FROM et_confirm_booking_detail AS a2
                JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                WHERE a2.ecbd_date_booking = "'.$data.'" AND b2.fk_et_facility_type = '.$id.' 
            )
        '));

			// dd($query);
		// $query = DB::select(DB::raw('(SELECT CURDATE() + INTERVAL (units.mul + (10 * tens.mul)) DAY AS bh_date, (SELECT bh_name FROM bh_hall LIMIT 1) AS bh_name
        // FROM       (SELECT 0 AS mul UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS units
        // CROSS JOIN (SELECT 0 AS mul UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS tens)
		// '));

        return $query;
    }
    
    public function slotfasiliti($id, $data){
        
		$query = DB::select(DB::raw('
            SELECT DISTINCT b.id AS "fidd",b.efd_name,d.est_slot_time,d.id AS "fkst", d.fk_lkp_slot_cat AS "sesi", c.efp_unit_price AS "harga", c.fk_lkp_gst_rate AS "gst", c.id AS "cidd", d.start
            FROM et_facility_type AS a
            JOIN et_facility_detail AS b ON a.id = b.fk_et_facility_type
            JOIN et_facility_price AS c ON c.fk_et_facility = a.fk_et_facility
            JOIN et_slot_time AS d ON d.id = c.fk_et_slot_time
            WHERE b.fk_et_facility_type = '.$id.' AND efd_status = 1 AND c.efp_day_cat = 1
            AND (d.id, b.efd_name) NOT IN (
                SELECT f1.id AS "aaa", g1.efd_name AS "bbb"
                FROM et_sport_time AS a1
                JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
                JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
                JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
                JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
                JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
                JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
                WHERE b1.esb_booking_date = "'.$data.'" AND c1.fk_et_facility_type = '.$id.' AND d1.fk_lkp_status IN (5,11,2) AND g1.fk_et_facility_type = '.$id.'
                UNION
                SELECT a2.fk_et_slot_time AS "aaa", b2.efd_name AS "bbb"
                FROM et_confirm_booking_detail AS a2
                JOIN et_facility_detail AS b2 ON b2.id = a2.fk_et_facility_detail
                WHERE a2.ecbd_date_booking = "'.$data.'" AND b2.fk_et_facility_type = '.$id.' 
            )
        '));
		
			
		// $query = DB::select(DB::raw('(SELECT CURDATE() + INTERVAL (units.mul + (10 * tens.mul)) DAY AS bh_date, (SELECT bh_name FROM bh_hall LIMIT 1) AS bh_name
        // FROM       (SELECT 0 AS mul UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS units
        // CROSS JOIN (SELECT 0 AS mul UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS tens)
		// '));

        return $query;
    }

    public function tempahan_saya($id)
    {
        $spa = 'SPA%';
        $query = DB::select(DB::raw("
            SELECT * FROM main_booking 
            WHERE fk_users = $id 
            AND bmb_booking_no LIKE 'SPA%'
            "));

        return $query;
    }

    public function listhall($tarikhfrom,$tarikhto,$locationsearch,$type,$statussearch) {
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');
        
        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }

        $data= DB::table('main_booking')
            ->leftJoin('bh_booking','bh_booking.fk_main_booking','=','main_booking.id')
            ->leftJoin('bh_booking_detail','bh_booking_detail.fk_bh_booking','=','bh_booking.id')
            ->leftJoin('spa_meeting_result','spa_meeting_result.fk_main_booking','=','main_booking.id')
            ->leftJoin('spa_payment','spa_payment.fk_main_booking','=','main_booking.id')
            ->leftJoin('bh_hall','bh_hall.id','=','spa_booking_event.fk_bh_hall')
            ->leftJoin('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->leftJoin('user_role','user_role.user_id','=','main_booking.fk_users')
            ->select('main_booking.id','main_booking.bmb_booking_no','spa_booking_event.event_date',
            'spa_booking_event.event_name','spa_booking_organiser.agency_name','bh_hall.name',
            'spa_booking_event.visitor_amount','spa_booking_event.rank_host','spa_meeting_result.meeting_no',
            DB::raw('SUM(spa_payment.amount) AS bayar'),'spa_booking_event.other_rank',
            'main_booking.bmb_rounding','main_booking.bmb_deposit_rm','bh_hall.id AS locationid','lkp_status.ls_description','main_booking.fk_users','user_role.role_id')
            ->whereRaw('(spa_meeting_result.fk_lkp_status_meeting =30 or (user_role.role_id=16 or user_role.role_id=10 ))')//include internal
            ->where('spa_booking_event.event_date','>=', array( $tarikh_from))
            ->where('spa_booking_event.event_date' ,'<', array( $tarikh_to));

        if($statussearch==''){
            $data->whereIn('main_booking.fk_lkp_status',array(18,19,20,21,22,23,24,25,26,27,28,29,30));
        }else{
            $data->whereIn('main_booking.fk_lkp_status',array($statussearch));
        }
        if($locationsearch==''){
            $data->where('bh_hall.id','<>',0)
                ->groupBy('main_booking.id');
        }else{
            $data->where('bh_hall.id',$locationsearch)
                ->groupBy('main_booking.id');
        }
        return $data->get();
    }

    public function duplicate_all2() {
        $query = DB::select(DB::raw('
            SELECT COUNT(*) AS total,
                a1.fk_spa_location,
                a1.event_date,
                a1.event_date_end
            FROM spa_booking_event AS a1
            WHERE a1.event_date IS NOT NULL
            AND EXISTS (
                SELECT 1
                FROM spa_booking_event AS a2
                WHERE a2.fk_spa_location = a1.fk_spa_location
                    AND a2.event_date IS NOT NULL
                    AND a2.event_date_end IS NOT NULL
                    AND a1.event_date <= a2.event_date_end
                    AND a2.event_date <= a1.event_date_end
                    AND a1.id != a2.id
            )
            GROUP BY a1.fk_spa_location, a1.event_date, a1.event_date_end
            HAVING COUNT(*) > 1;
        '));
        return $query;
    }

    public function duplicateAll(){
        $query = DB::table('bh_confirm_booking')
            ->join('main_booking', 'bh_confirm_booking.fk_main_booking', 'main_booking.id')
            ->select('*', DB::raw('COUNT(date_booking) as count'))
            ->where('main_booking.fk_lkp_status', '!=', 12)
            // ->where('fk_bh_hall', $id)
            // ->where('date_booking', $date)
            ->having('count', '>', 1)
            ->get();
        return $query;
    }
    public function duplicate($id, $date, $post){
        $date = date('Y-m-d', strtotime($date));
        $query = DB::table('bh_confirm_booking')
            ->join('main_booking', 'bh_confirm_booking.fk_main_booking', 'main_booking.id')
            // ->select('*', DB::raw('COUNT(fk_main_booking) as count'))
            ->where('fk_bh_hall', $id)
            ->where('date_booking', $date)
            // ->having('count', '>', 1)
            ->get();
        return $query;
    }

    // public function duplicate($id, $data, $data2){
    // public function duplicate($id, $data){
    //     $query = DB::select(DB::raw('
    //         SELECT COUNT(est_slot_time) AS total, est_slot_time, c1.fk_et_facility_type, b1.esb_booking_date, efd_name, d1.fk_lkp_location
    //         FROM et_hall_time AS a1
    //         JOIN et_hall_book AS b1 ON b1.id = a1.fk_et_hall_book
    //         JOIN bh_hall AS c1 ON c1.id = b1.fk_et_booking_facility
    //         JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
    //         JOIN bh_hall_price AS e1 ON e1.id = a1.fk_et_slot_price
    //         JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
    //         JOIN bh_hall_detail AS g1 ON g1.id = c1.fk_et_facility_detail
    //         WHERE b1.esb_booking_date = "'.$data.'" AND c1.fk_et_facility_type = '.$id.' AND d1.fk_lkp_status IN (5,11) AND g1.fk_et_facility_type = '.$id.'
    //         GROUP BY est_slot_time
    //         HAVING COUNT(est_slot_time) > 1;
    //     '));
    //     // dd($query);

    //     return $query;
    // }

    public function getlistDalaman()
    {
        $users = DB::table('user_role')->whereIn('role_id',[1,16])->select('user_id')->get();
        
        $role = array();

        foreach ($users as $key => $value) {
        array_push($role, $value->user_id);
        }

        $data = DB::table('main_booking')
        ->join('spa_booking_event','main_booking.id','=','spa_booking_event.fk_main_booking')
        ->join('spa_booking_person','main_booking.id','=','spa_booking_person.fk_main_booking')
        ->join('spa_location','spa_booking_event.fk_spa_location','=','spa_location.id')
        ->join('lkp_status','lkp_status.id','=','main_booking.fk_lkp_status')
        ->whereIn('main_booking.fk_users',$role)
        ->where('main_booking.bmb_type_user',2)
        //->where('main_booking.deleted_at','=',NULL)
        ->select('main_booking.bmb_booking_no','main_booking.bmb_booking_date','spa_booking_person.name', 'main_booking.fk_users', 'spa_booking_person.contact_person', 'spa_booking_event.event_name','spa_booking_event.event_date','spa_location.name as locationame','spa_booking_event.total_day','spa_booking_event.fk_spa_location','spa_booking_event.other_location','spa_booking_event.enter_date','spa_booking_event.exit_date','main_booking.fk_lkp_status','main_booking.id','lkp_status.ls_description','main_booking.bmb_type_user','spa_booking_person.contact_person','spa_booking_person.name','spa_booking_event.event_date_end') 
        ->get();
        // dd($data);exit;
        
        return $data;
    }
    public function getsearch($source)
    {
        $pagination = 8;
        $now = Carbon::now('Asia/Kuala_Lumpur');
        $dt  = $now->toDateString();


        // dd(Input::except('_token'));
        if(isset($source['pax'])){
            $pax = $source['pax'];
        }else{
            $pax = '';
        }
        if(isset($source['loc'])){
            $loc = $source['loc'];
        }else{
            $loc = '';
        }
        if(isset($source['hall'])){
            $hall = $source['hall'];
        }else{
            $hall ='';
        }
        if(isset($source['start_date'])){
            $start_date = $source['start_date'];
        }else{
            $start_date = '';
        }
        if(isset($source['end_date'])){
            $end_date = $source['end_date'];
        }else{
            $end_date = '';
        }

        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));

        if(($pax == '') && ($loc == '') && ($hall == '') && ($start_date == '') && ($end_date == '')){


            $data = BhConfirmBooking::whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                    ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)->get();
            // return $data;

            $dataid = array();

            foreach ($data as $key => $value) {
                array_push($dataid, $value->fk_bh_hall);
            }
            // dd($dataid);

            $dataSearch = BhHall::whereNotIn('bh_hall.id',$dataid)->where('bh_hall.bh_status','!=',0)->paginate($pagination);
            return $dataSearch;

        }elseif(($pax != '')&& ($loc == '') && ($hall == '')){


            switch ($pax) {
                case '.200':
                    $rst = VwCapasity::where('capasity','<=',200)->get();
                    break;

                case '.400':
                    $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                    break;

                case '.600':
                    $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                    break;

                case '.800':
                    $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                    break;

                case '.801':
                    $rst = VwCapasity::where('capasity','>=',801)->get();
                    break;

                default:
                    $rst = VwCapasity::all();
                    break;
            }


            // dd($rst);

            $picc = array(); // list for picc only
            $xpicc = array();
                            // list for except picc
            foreach ($rst as $p => $q) {
                if($q->source == 'bh_hall_picc'){
                    $picc[$q->id]["id"] =  $q->id;
                    $picc[$q->id]["source"] =  $q->source;
                    $picc[$q->id]["capasity"] =  $q->capasity;
                }else{
                    $xpicc[$q->id]["id"] =  $q->id;
                    $xpicc[$q->id]["source"] =  $q->source;
                    $xpicc[$q->id]["capasity"] =  $q->capasity;

                }
            } // endforeach
            // dd($picc);

            // hall yang dah confirm booking
            $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                        ->whereDate('date_booking','<=',$end_date)
                                        ->get();
            $confid = array();

            foreach ($confirm as $a => $b) {
                array_push($confid, $b->fk_bh_hall);
            }

            $result = array();
            $detail = array();

            // dd($xpicc);
            // dd($confid);
            if($confid == NULL){

                foreach ($xpicc as $xxx => $yyy) {
                    $result[$yyy['id']]["id"] =  $yyy['id'];
                    $result[$yyy['id']]["source"] =  $yyy['source'];
                    $result[$yyy['id']]["capasity"] =  $yyy['capasity'];

                    array_push($detail, $yyy['id']);
                }

            }else{

                foreach ($xpicc as $xxxx => $yyyy) {
                    // dd($yyyy['id']);
                    foreach ($confid as $x1 => $y1) {
                        if($yyyy['id'] != $y1){
                            // array_push($result, $yyyy['id']);
                            $result[$yyyy['id']]["id"] =  $yyyy['id'];
                            $result[$yyyy['id']]["source"] =  $yyyy['source'];
                            $result[$yyyy['id']]["capasity"] =  $yyyy['capasity'];

                            array_push($detail, $yyyy['id']);
                        }else{

                        }
                    }
                } //endforeach
            } //end else

            // dd($detail);

            // for picc hall
            $piccarray = array();
            foreach ($picc as $kpicc => $vpicc) {
                array_push($piccarray, $vpicc['id']);
            }

            // dd($piccarray);

            // $datapicc = BhHallPicc::whereIn('bh_hall_picc.id',$piccarray)->paginate($pagination);
            // dd($datapicc);
            // picc end

            // $dataSearch = BhHall::whereIn('bh_hall.id',$detail)->where('bh_hall.bh_status',1)->paginate($pagination);
            // return $dataSearch;

            $a = DB::table('bh_hall')
                ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                ->where('bh_hall.bh_status',1)
                ->whereIn('id',$detail);


            $b = DB::table('bh_hall_picc')
                ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                ->whereIn('id',$piccarray);

            $combined = $b->union($a)->get();
            // dd($combined);
            return $combined;


        }elseif(($loc != '')&& ($pax == '') && ($hall == '')&& ($start_date !='') && ($end_date !='')){

            // dd($loc);

            if(($loc == 1) || ($loc == 2)){

                $confirm = DB::select(DB::raw("SELECT a.id AS 'id'
                        FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = $loc
                        AND b.id=a.fk_lkp_location
                        AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                $result = array();
                foreach ($confirm as $a => $b) {
                    array_push($result, $b->id);
                }

                // dd($result);

                $data = BhHall::whereIn('bh_hall.id',$result)
                    ->where('bh_hall.bh_status','!=',0)
                    ->where('fk_lkp_location',$loc)
                    ->get();
                return $data;

            }elseif($loc == 4){

                // hall yang dah confirm booking
                $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                            ->whereDate('date_booking','<=',$end_date)
                                            ->get();
                $confid = array();

                if(isset($confirm)){
                    foreach ($confirm as $a => $b) {
                        array_push($confid, $b->fk_bh_hall);
                    }
                } //end if


                $a = DB::table('bh_hall')
                ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                ->where('bh_hall.bh_status','!=',0)
                ->whereNotIn('id',$confid);


                $b = DB::table('bh_hall_picc')
                ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"));


                $combined = $a->union($b)->get();


                // dd($combined);

                return $combined;

            }elseif($loc == 5){
                // percint 8
                // dd('percint 8');

                //check on et_confirm_booking
                $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();
                // manipulate date confirm
                // -------------------------------------------------------------------------
                $arrayconfirm = array();

                if($confirm->count()){

                    foreach ($confirm as $key => $value) {

                        array_push($arrayconfirm, $value->fk_et_facility_type);

                    }// end foreach
                }else{

                        array_push($arrayconfirm, 0);
                }


                // -------------------------------------------------------------------------
                $arrayconfirm = implode(",", $arrayconfirm);
                // dd($arrayconfirm);

                $data = DB::select
                (DB::raw("SELECT b.id AS id
                FROM et_facility a,et_facility_type b
                WHERE b.fk_et_facility = a.id
                AND  a.ef_type = 1
                AND b.fk_lkp_location = 4
                AND b.id NOT IN ($arrayconfirm)
                "));
                // dd($data);
                return $data;

            }elseif($loc == 6){
                // percint 9
                // dd('percint 9');
                $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                $arrayconfirm = array();

                if($confirm->count()){

                    foreach ($confirm as $key => $value) {

                        array_push($arrayconfirm, $value->fk_et_facility_type);

                    }// end foreach
                }else{

                        array_push($arrayconfirm, 0);
                }


                // -------------------------------------------------------------------------
                $arrayconfirm = implode(",", $arrayconfirm);
                // dd($arrayconfirm);

                $data = DB::select
                (DB::raw("SELECT b.id AS id
                FROM et_facility a,et_facility_type b
                WHERE b.fk_et_facility = a.id
                AND  a.ef_type = 1
                AND b.fk_lkp_location = 5
                AND b.id NOT IN ($arrayconfirm)
                "));

                return $data;

            }elseif($loc == 7){

                // percint 11
                $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                $arrayconfirm = array();

                if($confirm->count()){

                    foreach ($confirm as $key => $value) {

                        array_push($arrayconfirm, $value->fk_et_facility_type);

                    }// end foreach
                }else{

                        array_push($arrayconfirm, 0);
                }


                // -------------------------------------------------------------------------
                $arrayconfirm = implode(",", $arrayconfirm);
                // dd($arrayconfirm);

                $data = DB::select
                (DB::raw("SELECT b.id AS id
                FROM et_facility a,et_facility_type b
                WHERE b.fk_et_facility = a.id
                AND  a.ef_type = 1
                AND b.fk_lkp_location = 6
                AND b.id NOT IN ($arrayconfirm)
                "));

                return $data;
                // end percint 11

            }elseif($loc == 8){
                // percint 16
                $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                $arrayconfirm = array();

                if($confirm->count()){

                    foreach ($confirm as $key => $value) {

                        array_push($arrayconfirm, $value->fk_et_facility_type);

                    }// end foreach
                }else{

                        array_push($arrayconfirm, 0);
                }


                // -------------------------------------------------------------------------
                $arrayconfirm = implode(",", $arrayconfirm);
                // dd($arrayconfirm);

                $data = DB::select
                (DB::raw("SELECT b.id AS id
                FROM et_facility a,et_facility_type b
                WHERE b.fk_et_facility = a.id
                AND  a.ef_type = 1
                AND b.fk_lkp_location = 7
                AND b.id NOT IN ($arrayconfirm)
                "));

                return $data;
                // end percint 16

            }else{

                // if condition loc = 3
                    // dd($source);
                    $s = date("Y-m-d", strtotime($source['start_date']));
                    $e = date("Y-m-d", strtotime($source['end_date']));

                    $start_date = $s;
                    $end_date = $e;

                    if($source['pax'] == ''){
                        $pac = 1;
                    }else{
                        $pac = 1;
                    }
                    // $pac = $source['pax'];


                    $jsonstring= 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                    $str = file_get_contents($jsonstring);
                    $data = json_decode($str, TRUE);

                    // $data = BhHallPicc::all();

                    return $data;


            }



            // dd($result);



            // echo $loc;exit;
        }elseif(($hall != '')&& ($pax == '') && ($loc == '')){
            // echo "apip";exit;
            // echo $hall;exit;
            $data = BhHall::where('bh_name','like', "%".$hall."%")->where('bh_hall.bh_status','!=',0)->get();
            // dd($data);

            $datahall = array();
            if(isset($data)){
                foreach ($data as $k => $v) {
                    array_push($datahall, $v->id);
                }
            }
            // dd($datahall);

            // hall yang dah confirm booking
            $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                        ->whereDate('date_booking','<=',$end_date)
                                        ->get();
            // dd($confirm);
            $confid = array();

            if(isset($confirm)){
                foreach ($confirm as $a => $b) {
                    array_push($confid, $b->fk_bh_hall);
                }
            } //end if

            // dd($confid);
            // start
            $result = array();

            if($confid == NULL){

                foreach ($datahall as $p => $q) {
                    array_push($result,$q);
                }

            }else{

                foreach ($datahall as $x => $y) {
                    foreach ($confid as $x1 => $y1) {
                        if($y != $y1){
                            array_push($result, $y);
                        }else{

                        }
                    }
                } //endforeach


            } //end else
            //end
            // dd($result);

            $a = DB::table('bh_hall')
            ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
            ->where('bh_hall.bh_status','!=',0)
            ->whereNotIn('id',$confid)
            ->where('bh_name','like',"%".$hall."%");


            $b = DB::table('bh_hall_picc')
            ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
            ->where('bh_hall_name','like',"%".$hall."%");

            $combined = $a->union($b)->get();

            return $combined;

            // $dataSearch = BhHall::whereIn('bh_hall.id',$result)->where('bh_hall.bh_status',1)->paginate($pagination);
            // return $dataSearch;
            // dd($result);

        }elseif(($loc != '')&& ($pax != '') && ($hall == '')&& ($start_date !='') && ($end_date !='')){
            // loc != 0, pax != 0
            // get capasity
            // echo $pax;exit;
            switch ($pax) {
                case '.200':
                    $rst = VwCapasity::where('capasity','<=',200)->get();
                    break;

                case '.400':
                    $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                    break;

                case '.600':
                    $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                    break;

                case '.800':
                    $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                    break;

                case '.801':
                    $rst = VwCapasity::where('capasity','>=',801)->get();
                    break;

                default:
                    $rst = VwCapasity::all();
                    break;
            }
            // dd($rst);

            $picc = array(); // list for picc only
            // $xpicc = array();
            $ppj = array();
            $marina = array();
                            // list for except picc
            foreach ($rst as $p => $q) {
                if($q->source == 'bh_hall_picc'){
                    // $picc[$q->id]["id"] =  $q->id;
                    // $picc[$q->id]["source"] =  $q->source;
                    // $picc[$q->id]["capasity"] =  $q->capasity;
                    array_push($picc,$q->id);
                }else{
                    // $xpicc[$q->id]["id"] =  $q->id;
                    // $xpicc[$q->id]["source"] =  $q->source;
                    // $xpicc[$q->id]["capasity"] =  $q->capasity;
                    $loca = BhHall::where('bh_hall.id',$q->id)->where('bh_hall.bh_status','!=',0)->select('fk_lkp_location')->first();
                    if($loca != null){
                            if($loca->fk_lkp_location == 1){
                            // $ppj[$q->id]["id"] =  $q->id;
                            // $ppj[$q->id]["source"] =  $q->source;
                            // $ppj[$q->id]["capasity"] =  $q->capasity;
                                array_push($ppj, $q->id);
                        }else{
                            // $marina[$q->id]["id"] =  $q->id;
                            // $marina[$q->id]["source"] =  $q->source;
                            // $marina[$q->id]["capasity"] =  $q->capasity;
                                array_push($marina, $q->id);
                        }
                    }


                }
            } // endforeach
            // end capasity

            // dd($marina);
            // dd($ppj);
            // dd($picc);
            // dd($loc);

            // start location
            switch ($loc) {
                case 1:
                    //ppj


                // ------------------------------------------------------------------------
                    $ppj = implode(",", $ppj);

                    // dd($ppj);

                    /*$ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = 1
                    AND b.id=a.fk_lkp_location
                    AND a.id IN ($ppj)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));*/

            //Dzul Buat
            $ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status != 0 AND a.fk_lkp_location = 1
                    AND b.id=a.fk_lkp_location
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));


                    // dd($ppjconfirm);
                // -------------------------------------------------------------------

                    $ppjlist = array();

                    $ppj = explode(",", $ppj);
                    // dd($ppjconfirm);
                    if(count($ppjconfirm) > 0){

                        foreach ($ppj as $x1 => $y1){
                            foreach ($ppjconfirm as $xxx => $yyy) {
                                if($yyy->id == $y1){
                                    array_push($ppjlist, $y1);
                                }
                            }
                            // array_push($ppjlist, $yyy->id);
                        }

                    }else{


                    }

                    // dd($ppjlist);

                    $res = BhHall::whereIn('bh_hall.id',$ppjlist)
                    ->where('bh_hall.fk_lkp_location','!=',0)
                    ->get();
                    return $res;
                    break;

                case 2:

                // dd($marina);

                    if($marina != null ){
                        // echo "ada";exit;
                        $marina = implode(",", $marina);
                    }else{
                        $marina = 0;
                        // echo "takda";exit;
                    }

                    // dd($marina);

                    $marinaconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status!= 0 AND a.fk_lkp_location = 2
                    AND b.id=a.fk_lkp_location
                    AND a.id IN ($marina)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                    // dd($marinaconfirm);



                    $marinalist = array();

                    if($marina != null ){
                        $marina = explode(",", $marina);
                    }else{
                        $marina = 0;
                    }

                    // dd($marina);

                    if(count($marinaconfirm) > 0){

                        foreach ($marina as $xa => $ya){
                            foreach ($marinaconfirm as $xx => $yy) {
                                if($yy->id == $ya){
                                    array_push($marinalist, $ya);
                                }
                            }
                            // array_push($ppjlist, $yyy->id);
                        }

                    }else{


                    }

                    // dd($marinalist);

                    $res = BhHall::whereIn('bh_hall.id',$marinalist)
                    ->where('bh_hall.fk_lkp_location',2)
                    ->get();

                    return $res;
                    break;

                case 3:

                    /***********************************************
                     $result = array();

                    foreach ($picc as $pa => $pb) {
                            array_push($result, $pb['id']);
                        }

                    $data = BhHallPicc::whereIn('bh_hall_picc.id',$result)->get();

                    return $data;
                    ************************************************/
                    $s = date("Y-m-d", strtotime($source['start_date']));
                    $e = date("Y-m-d", strtotime($source['end_date']));

                    $start_date = $s;
                    $end_date = $e;


                    if($source['pax'] == '.200'){
                        $pac = 1;
                        // dd('0 --> 200');
                        $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($duaratus);

                        $manipulate = json_decode($str, TRUE);

                        // dd($manipulate);

                        $data = array();
                        $i = 0;
                        foreach ($manipulate as $k => $v) {

                            if($v['FAC_CAPACITY'] <= 200 ){
                                $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                            $i++;
                            }
                        }





                    }elseif($source['pax'] == '.400'){
                        // $pac = 1;
                        // dd('201 --> 400');
                        $pac = 1;
                        $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($duaratus);

                        $manipulate = json_decode($str, TRUE);

                        // dd($manipulate);

                        $data = array();
                        $i = 0;
                        foreach ($manipulate as $k => $v) {

                            if(($v['FAC_CAPACITY'] >= 201 ) AND ($v['FAC_CAPACITY'] <= 400)){
                                $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                            $i++;
                            }
                        }



                    }elseif($source['pax'] == '.600'){
                        // dd('401 --> 600');
                        $pac = 1;
                        $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($duaratus);

                        $manipulate = json_decode($str, TRUE);

                        // dd($manipulate);

                        $data = array();
                        $i = 0;
                        foreach ($manipulate as $k => $v) {

                            if(($v['FAC_CAPACITY'] >= 401 ) AND ($v['FAC_CAPACITY'] <= 600)){
                                $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                            $i++;
                            }
                        }



                    }elseif($source['pax'] == '.800'){
                        // dd('601 --> 800');

                        $pac = 1;
                        $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($duaratus);

                        $manipulate = json_decode($str, TRUE);

                        // dd($manipulate);

                        $data = array();
                        $i = 0;
                        foreach ($manipulate as $k => $v) {

                            if(($v['FAC_CAPACITY'] >= 601 ) AND ($v['FAC_CAPACITY'] <= 800)){
                                $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                            $i++;
                            }
                        }



                    }elseif($source['pax'] == '.801'){
                        // dd('800 --> infiniti');

                        $pac = 1;
                        $duaratus = 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                        $str = file_get_contents($duaratus);

                        $manipulate = json_decode($str, TRUE);

                        // dd($manipulate);

                        $data = array();
                        $i = 0;
                        foreach ($manipulate as $k => $v) {

                            if($v['FAC_CAPACITY'] >= 801){
                                $data[$i]["FAC_ID"] = $v['FAC_ID'];
                                $data[$i]["FAC_NAME"] = $v['FAC_NAME'];
                                $data[$i]["FAC_FILENAME"] = $v['FAC_FILENAME'];
                                $data[$i]["FAC_CAPACITY"] = $v['FAC_CAPACITY'];
                            $i++;
                            }
                        }



                    }else{
                        // dd('semua');
                    }

                    // $pac = $source['pax'];


                    // $jsonstring= 'http://booking.picc.com.my/onlinebooking/vendor/index.php?ID=xGWs44WhqswJP37gVD7466U6Zr8lS4AA&START_DATE='.$s.'&END_DATE='.$e.'&PAX='.$pac.'';

                    // $str = file_get_contents($jsonstring);
                    // $data = json_decode($str, TRUE);

                    // $data = BhHallPicc::all();

                    return $data;

                    break;


                case 4:
                    //lokasi semua
                    // dd($rst);
                    // dd($ppj);
                    // dd($marina);
                    $dt = array();
                    foreach ($ppj as $aaa => $bbb) {
                        array_push($dt,$bbb);
                    }
                    foreach ($marina as $aaaa => $bbbb) {
                        array_push($dt,$bbbb);
                    }
                    $dpicc = array();
                    foreach ($picc as $aaaaa => $bbbbb) {
                        array_push($dpicc,$bbbbb);
                    }

                    // dd($dpicc);
                    $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                                            ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                            ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                                            ->where('bh_hall.fk_lkp_location',1)
                                            ->orWhere('bh_hall.fk_lkp_location',2)
                                            ->get();
                    $confid = array();

                    if(isset($confirm)){
                        foreach ($confirm as $a => $b) {
                            array_push($confid, $b->fk_bh_hall);
                        }
                    } //end if
                    // dd($confid);


                    $a = DB::table('bh_hall')
                    ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                    ->where('bh_hall.bh_status','!=',0)
                    ->whereIn('id',$dt)
                    ->whereNotIn('id',$confid);


                    $b = DB::table('bh_hall_picc')
                    ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                    ->whereIn('id',$dpicc);



                    $combined = $a->union($b)->get();
                    return $combined;
                    // dd($combined);
                    // dd('semua');
                    break;


                case 5 :

                        $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                        $arrayconfirm = array();

                        if($confirm->count()){

                            foreach ($confirm as $key => $value) {

                                array_push($arrayconfirm, $value->fk_et_facility_type);

                            }// end foreach
                        }else{

                                array_push($arrayconfirm, 0);
                        }


                        // -------------------------------------------------------------------------
                        $arrayconfirm = implode(",", $arrayconfirm);
                        // dd($arrayconfirm);

                        $data = DB::select
                        (DB::raw("SELECT b.id AS id
                        FROM et_facility a,et_facility_type b
                        WHERE b.fk_et_facility = a.id
                        AND  a.ef_type = 1
                        AND b.fk_lkp_location = 4
                        AND b.id NOT IN ($arrayconfirm)
                        "));

                        return $data;

                    break;

                case 6 :

                        $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                        $arrayconfirm = array();

                        if($confirm->count()){

                            foreach ($confirm as $key => $value) {

                                array_push($arrayconfirm, $value->fk_et_facility_type);

                            }// end foreach
                        }else{

                                array_push($arrayconfirm, 0);
                        }


                        // -------------------------------------------------------------------------
                        $arrayconfirm = implode(",", $arrayconfirm);
                        // dd($arrayconfirm);

                        $data = DB::select
                        (DB::raw("SELECT b.id AS id
                        FROM et_facility a,et_facility_type b
                        WHERE b.fk_et_facility = a.id
                        AND  a.ef_type = 1
                        AND b.fk_lkp_location = 5
                        AND b.id NOT IN ($arrayconfirm)
                        "));

                        return $data;

                    break;

                case 7 :
                        $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                        $arrayconfirm = array();

                        if($confirm->count()){

                            foreach ($confirm as $key => $value) {

                                array_push($arrayconfirm, $value->fk_et_facility_type);

                            }// end foreach
                        }else{

                                array_push($arrayconfirm, 0);
                        }


                        // -------------------------------------------------------------------------
                        $arrayconfirm = implode(",", $arrayconfirm);
                        // dd($arrayconfirm);

                        $data = DB::select
                        (DB::raw("SELECT b.id AS id
                        FROM et_facility a,et_facility_type b
                        WHERE b.fk_et_facility = a.id
                        AND  a.ef_type = 1
                        AND b.fk_lkp_location = 6
                        AND b.id NOT IN ($arrayconfirm)
                        "));

                        return $data;

                    break;

                case 8 :

                        $confirm = EtConfirmBooking::whereDate('ecb_date_booking','>=',$start_date)
                                            ->whereDate('ecb_date_booking','<=',$end_date)
                                            // ->where('fk_et_slot_time','!=',27)
                                            ->where('ecb_flag_indicator',2)
                                            ->groupBy('fk_et_facility_type')
                                            ->get();

                        $arrayconfirm = array();

                        if($confirm->count()){

                            foreach ($confirm as $key => $value) {

                                array_push($arrayconfirm, $value->fk_et_facility_type);

                            }// end foreach
                        }else{

                                array_push($arrayconfirm, 0);
                        }


                        // -------------------------------------------------------------------------
                        $arrayconfirm = implode(",", $arrayconfirm);
                        // dd($arrayconfirm);

                        $data = DB::select
                        (DB::raw("SELECT b.id AS id
                        FROM et_facility a,et_facility_type b
                        WHERE b.fk_et_facility = a.id
                        AND  a.ef_type = 1
                        AND b.fk_lkp_location = 7
                        AND b.id NOT IN ($arrayconfirm)
                        "));

                        return $data;

                    break;

                default:
                    # code...
                    break;
            }
            // end location
            exit;


        }elseif(($loc != '')&& ($pax != '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
            // dd('loc pac hall');
            // for loc !=null,pax!=null and hall !=null
            switch ($pax) {
                case '.200':
                    $rst = VwCapasity::where('capasity','<=',200)->get();
                    break;

                case '.400':
                    $rst = VwCapasity::where('capasity','>',201)->where('capasity','<=',400)->get();
                    break;

                case '.600':
                    $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                    break;

                case '.800':
                    $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                    break;

                case '.801':
                    $rst = VwCapasity::where('capasity','>=',801)->get();
                    break;

                default:
                    $rst = VwCapasity::all();
                    break;
            }
            // dd($rst);

            $picc = array(); // list for picc only
            // $xpicc = array();
            $ppj = array();
            $marina = array();
                            // list for except picc
            foreach ($rst as $p => $q) {
                if($q->source == 'bh_hall_picc'){
                    // $picc[$q->id]["id"] =  $q->id;
                    // $picc[$q->id]["source"] =  $q->source;
                    // $picc[$q->id]["capasity"] =  $q->capasity;
                    array_push($picc, $q->id);
                }else{
                    // $xpicc[$q->id]["id"] =  $q->id;
                    // $xpicc[$q->id]["source"] =  $q->source;
                    // $xpicc[$q->id]["capasity"] =  $q->capasity;
                    $loca = BhHall::where('bh_hall.id',$q->id)->select('fk_lkp_location')->first();
                    if($loca->fk_lkp_location == 1){
                        // $ppj[$q->id]["id"] =  $q->id;
                        // $ppj[$q->id]["source"] =  $q->source;
                        // $ppj[$q->id]["capasity"] =  $q->capasity;
                        array_push($ppj, $q->id);
                    }else{
                        // $marina[$q->id]["id"] =  $q->id;
                        // $marina[$q->id]["source"] =  $q->source;
                        // $marina[$q->id]["capasity"] =  $q->capasity;
                        array_push($marina, $q->id);
                    }

                }
            } // endforeach
            // end capasity

            // dd($marina);
            // dd($ppj);
            // dd($xpicc);
            // dd($loc);

            // start location
            switch ($loc) {
                case 1:
                    //ppj
                    // $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                    // ->where('bh_hall.fk_lkp_location',1)
                    // ->where('bh_hall.bh_status',1)
                    // ->select('bh_hall.id')
                    // ->get();

                    // hall yang dah confirm booking
                    // $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                    //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                    //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                    //                         ->where('bh_hall.fk_lkp_location',1)
                    //                         ->get();
                    // dd($ppj);

                    $ppj = implode(",", $ppj);

                    $ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status !=0 AND a.fk_lkp_location = 1
                    AND b.id=a.fk_lkp_location
                    AND a.bh_name like '%$hall%'
                    AND a.id IN ($ppj)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                    // dd($ppjconfirm);

                    $ppjlist = array();

                    $ppj = explode(",", $ppj);

                    if(count($ppjconfirm) > 0){

                        foreach ($ppj as $x1 => $y1){
                            foreach ($ppjconfirm as $xxx => $yyy) {
                                if($yyy->id == $y1){
                                    array_push($ppjlist, $y1);
                                }
                            }
                            // array_push($ppjlist, $yyy->id);
                        }

                    }else{


                    }

                    // $confid = array();

                    // if(isset($confirm)){
                    //     foreach ($confirm as $a => $b) {
                    //         array_push($confid, $b->id);
                    //     }
                    // } //end if
                    // echo "confid";
                    // dd($ppj);
                    // dd($confid);

                    // $result = array();

                    // if($confid == NULL){

                    //     foreach ($ppj as $xx => $yy) {
                    //         array_push($result, $yy['id']);
                    //     }
                    // }else{

                    //     foreach ($ppj as $x => $y) {
                    //         foreach ($confid as $x1 => $y1) {
                    //             if($y['id'] != $y1){
                    //                  array_push($result, $y);
                    //             }else{

                    //             }
                    //         }
                    //     } //endforeach
                    // } //end else
                    // dd($result);
                    $res = BhHall::whereIn('bh_hall.id',$ppjlist)
                    ->where('bh_hall.fk_lkp_location',1)
                    ->get();
                    return $res;
                    break;

                case 2:
                    // marina
                    // $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                    // ->where('bh_hall.fk_lkp_location',2)
                    // ->where('bh_hall.bh_status',1)
                    // ->select('bh_hall.id')
                    // ->get();

                    // dd($marina);

                    if($marina != null){
                    $marina = implode(",", $marina);
                    }else{
                    // $array[] = 0;
                    // $marina = implode(",", $marina);
                    $marina = 0;
                    }
                    // dd($marina);

                    $marinaconfirm  = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = 2
                    AND a.bh_name like '%$hall%'
                    AND b.id=a.fk_lkp_location
                    AND a.id IN ($marina)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

                    // dd($marinaconfirm);

                    // hall yang dah confirm booking
                    // $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                    //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                    //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                    //                         ->where('bh_hall.fk_lkp_location',2)
                    //                         ->get();
                    // $confid = array();

                    // if(isset($confirm)){
                    //     foreach ($confirm as $a => $b) {
                    //         array_push($confid, $b->id);
                    //     }
                    // } //end if

                    // $result = array();

                    // if($confid == NULL){

                    //     foreach ($marina as $xx => $yy) {
                    //         array_push($result, $yy['id']);
                    //     }
                    // }else{

                    //     foreach ($marina as $x => $y) {
                    //         foreach ($confid as $x1 => $y1) {
                    //             if($y['id'] != $y1){
                    //                  array_push($result, $y);
                    //             }else{

                    //             }
                    //         }
                    //     } //endforeach
                    // } //end else

                    $marinalist = array();

                    if(count($marinaconfirm) > 0){

                        foreach ($marinaconfirm as $t => $y) {
                            array_push($marinalist,$y->id);
                        }
                    }else{


                    }


                    // dd($marinalist);
                    $res = BhHall::whereIn('bh_hall.id',$marinalist)
                    ->where('bh_hall.bh_status','!=',0)
                    ->where('bh_hall.fk_lkp_location',2)
                    ->where('bh_name','like',"%".$hall."%")->get();
                    return $res;


                    break;

                case 3:
                    // dd($picc);
                    if($picc != null){
                    $picc = implode(",", $picc);
                    }else{
                    // $array[] = 0;
                    // $marina = implode(",", $marina);
                    $picc = 0;
                    }

                    // $piccconfirm = BhHallPicc ::whereIn('bh_hall_picc.id',$picc)
                    //         ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                    //         ->select('bh_hall_picc.id')
                    //         ->get();

                    $piccconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall_picc a
                    WHERE a.bh_hall_name like '%$hall%'
                    AND a.id IN ($picc)"));

                    // dd($piccconfirm);

                    $picclist = array();

                    if(count($piccconfirm) > 0){

                        foreach ($piccconfirm as $t => $y) {
                            array_push($picclist,$y->id);

                        }
                    }else{


                    }

                    // foreach ($picc as $pa => $pb) {
                    //         array_push($result, $pb['id']);
                    //     }

                    $data = BhHallPicc::whereIn('bh_hall_picc.id',$picclist)
                    ->where('bh_hall_name','like',"%".$hall."%")->get();
                    // dd($data);
                    return $data;
                    break;

                case 4:
                    //lokasi semua
                    // dd($rst);
                    // dd($ppj);
                    // dd($marina);
                    $dt = array();
                    foreach ($ppj as $aaa => $bbb) {
                        array_push($dt,$bbb['id']);
                    }
                    foreach ($marina as $aaaa => $bbbb) {
                        array_push($dt,$bbbb['id']);
                    }
                    $dpicc = array();
                    foreach ($picc as $aaaaa => $bbbbb) {
                        array_push($dpicc,$bbbbb['id']);
                    }

                    // dd($dpicc);

                    $a = DB::table('bh_hall')
                    ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                    ->where('bh_hall.bh_status','!=',0)
                    ->whereIn('id',$dt)
                    ->where('bh_name','like',"%".$hall."%");


                    $b = DB::table('bh_hall_picc')
                    ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                    ->whereIn('id',$dpicc)
                    ->where('bh_hall_name','like',"%".$hall."%");



                    $combined = $a->union($b)->get();
                    return $combined;
                    // dd($combined);
                    // dd('semua');
                    break;

                default:
                    # code...
                    break;
            }
            // end location



        }elseif(($loc == '')&& ($pax != '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
            // dd('hahah');
            // kapasiti
            switch ($pax) {
                case '.200':
                    $rst = VwCapasity::where('capasity','<=',200)->get();
                    break;

                case '.400':
                    $rst = VwCapasity::where('vw_capasity.capasity','>',201)->where('vw_capasity.capasity','<=',400)->get();
                    break;

                case '.600':
                    $rst = VwCapasity::where('capasity','>',401)->where('capasity','<=',600)->get();
                    break;

                case '.800':
                    $rst = VwCapasity::where('capasity','>',601)->where('capasity','<=',800)->get();
                    break;

                case '.801':
                    $rst = VwCapasity::where('capasity','>=',801)->get();
                    break;

                default:
                    $rst = VwCapasity::all();
                    break;
            }
            // end kapasiti
            // dd($rst);
            $picc = array(); // list for picc only
            // $xpicc = array();
            $ppj = array();
            $marina = array();
                            // list for except picc
            foreach ($rst as $p => $q) {
                if($q->source == 'bh_hall_picc'){
                    // $picc[$q->id]["id"] =  $q->id;
                    // $picc[$q->id]["source"] =  $q->source;
                    // $picc[$q->id]["capasity"] =  $q->capasity;
                    array_push($picc, $q->id);
                }else{
                    // $xpicc[$q->id]["id"] =  $q->id;
                    // $xpicc[$q->id]["source"] =  $q->source;
                    // $xpicc[$q->id]["capasity"] =  $q->capasity;
                    $loca = BhHall::where('bh_hall.id',$q->id)->select('fk_lkp_location')->first();
                    if($loca->fk_lkp_location == 1){
                        // $ppj[$q->id]["id"] =  $q->id;
                        // $ppj[$q->id]["source"] =  $q->source;
                        // $ppj[$q->id]["capasity"] =  $q->capasity;
                        array_push($ppj, $q->id);
                    }else{
                        // $marina[$q->id]["id"] =  $q->id;
                        // $marina[$q->id]["source"] =  $q->source;
                        // $marina[$q->id]["capasity"] =  $q->capasity;
                        array_push($marina, $q->id);
                    }

                }
            } // endforeach

            // dd($ppj);
            //dd($picc);
            //dd($marina);
                // -------------------------PPJ----------------------------------------------------------------
            // $ppjconfirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
            //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
            //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
            //                         ->where('bh_hall.bh_status',1)
            //                         ->where('bh_hall.fk_lkp_location',1)
            //                         ->where('bh_name','like',"%".$hall."%")
            //                         // ->whereIn('bh_hall.id',$ppj)
            //                         ->select('bh_hall.id')
            //                         ->get();
            // dd(count($ppjconfirm));
            // if(count($ppjconfirm) > 0){
            //     echo 'ada array';
            // }else{
            //     echo 'tiada array';
            // }exit;

            // dd($ppjconfirm);


            // dd($ppj);

            $ppj = implode(",", $ppj);

            $ppjconfirm = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status !=0 AND a.fk_lkp_location = 1
                    AND a.bh_name like '%$hall%'
                    AND b.id=a.fk_lkp_location
                    AND a.id NOT IN ($ppj)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));
            // dd($ppjconfirm);
            // dd($ppj);
            $ppjlist = array();

            $ppj = explode(",", $ppj);
            // dd($ppj);
            if(count($ppjconfirm) > 0){
                // foreach ($ppjconfirm as $kppj => $vppj) {
                //     foreach ($ppj as $ppja => $ppjb) {
                //         if($vppj->id != intval($ppjb)){
                //             array_push($ppjlist,intval($ppjb));
                //         }
                //     }

                // }
                foreach ($ppjconfirm as $xxx => $yyy) {
                    array_push($ppjlist, $yyy->id);
                }

            }else{

                // foreach ($ppj as $kp => $vp) {
                //         array_push($ppjlist, intval($vp));
                //     }
            }

            // dd($ppjlist);

            $a =  BhHall::where('bh_hall.bh_status','!=',0)
                            ->where('bh_hall.fk_lkp_location',1)
                            ->whereIn('bh_hall.id',$ppjlist)
                            ->where('bh_name','like',"%".$hall."%")
                            ->select('bh_hall.id')
                            ->get();
            // dd($a);
            // -------------------------PPJ--------------------------------------------------------------
                // -------------------------MARINA-----------------------------------------------------------
            // $marinaconfirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
            //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
            //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
            //                         ->where('bh_hall.bh_status',1)
            //                         ->where('bh_hall.fk_lkp_location',2)
            //                         // ->where('bh_name','like',"%".$hall."%")
            //                         ->select('bh_hall.id')
            //                         ->get();

            // dd($marina);
            if($marina > 0){
                $marina = implode(",", $marina);
            }else{
                // $marina = 0;
            }


            $marinaconfirm  = DB::select(DB::raw("SELECT a.id AS 'id'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = 2
                    AND a.bh_name like '%$hall%'
                    AND b.id=a.fk_lkp_location
                    -- AND a.id NOT IN ($marina)
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));

            // dd($marinaconfirm);
            // dd($marina);
            $marinalist = array();

            if(count($marinaconfirm) > 0){
                // foreach ($marinaconfirm as $kmarina => $vmarina) {
                //     foreach ($marina as $marinaa => $marinab) {
                //         if($vmarina->id != $marinab){
                //             array_push($marinalist,$marinab);
                //         }
                //     }

                // }
                foreach ($marinaconfirm as $t => $y) {
                    array_push($marinalist,$y->id);
                }
            }else{
                // $marina = explode(",", $marina);
                    // foreach ($marina as $kmar => $vmar) {
                    //     array_push($marinalist, $vmar);
                    // }

            }
            // $marinaconfirm = DB::select(DB::raw("select bh_hall.id from bh_confirm_booking
            //              inner join bh_hall on bh_hall.id = bh_confirm_booking.fk_bh_hall
            //              where date(bh_confirm_booking.date_booking) >= $start_date
            //              and date(bh_confirm_booking.date_booking) <= $end_date
            //              and bh_hall.bh_status = 1 and bh_hall.fk_lkp_location = 2 ;"));

            // dd(count($marinaconfirm));

            // dd($marinalist);

            $b =  BhHall::where('bh_hall.bh_status','!=',0)
                            ->where('bh_hall.fk_lkp_location',2)
                            ->whereIn('bh_hall.id',$marinalist)
                            ->where('bh_name','like',"%".$hall."%")
                            ->select('bh_hall.id')
                            ->get();
            // dd($b);
                //-----------------------------MARINA----------------------------------------------------------------

            $c = BhHallPicc ::whereIn('bh_hall_picc.id',$picc)
                            ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                            ->select('bh_hall_picc.id')
                            ->get();
            // dd($a);

            $hallid = array();
            foreach ($a as $keya => $valuea) {
                // array_push($hallid, $valuea->id);
                $hallid[$valuea->id]['id'] = $valuea->id;
                $hallid[$valuea->id]['source'] = 'ppj';
            }
            foreach ($b as $keyb => $valueb) {
                //array_push($hallid, $valueb->id);
                $hallid[$valueb->id]['id'] = $valueb->id;
                $hallid[$valueb->id]['source'] = 'marina';
            }
            foreach ($c as $keyc => $valuec) {
                // array_push($hallid,$valuec->id);
                $hallid[$valuec->id]['id'] = $valuec->id;
                $hallid[$valuec->id]['source'] = 'picc';
            }

            // dd($hallid);
            return $hallid;


        }elseif(($loc != '')&& ($pax == '') && ($hall != '')&& ($start_date !='') && ($end_date !='')){
            // dd('kedua');
            // start location
            switch ($loc) {
                case 1:
                    //ppj
                    // $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                    // ->where('bh_hall.fk_lkp_location',1)
                    // ->where('bh_hall.bh_name','like',"%".$hall."%")
                    // ->where('bh_hall.bh_status',1)
                    // ->select('bh_hall.id')
                    // ->get();
                    // // dd($data);

                    // $dataid = array();
                    // foreach ($data as $d => $e) {
                    //     array_push($dataid,$e->id);
                    // }
                    // // dd($dataid);
                    // // hall yang dah confirm booking
                    // $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                    //                         ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                    //                         ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                    //                         ->where('bh_hall.fk_lkp_location',1)
                    //                         ->where('bh_hall.bh_name','like',"%".$hall."%")
                    //                         ->select('bh_hall.id')
                    //                         ->get();
                    // // dd($dataid);
                    // $confid = array();

                    // $ada = array();
                    // $takda = array();

                    // // dd(count($confirm));
                    // if(count($confirm) > 0){
                    //     foreach ($dataid as $vkey => $vvalue){
                    //         foreach ($confirm as $ckey => $cvalue){
                    //             if($cvalue->id != $vvalue){
                    //                 array_push($confid, $vvalue);
                    //             }else{
                    //                 // array_push($ada,$vvalue);
                    //             }
                    //         }
                    //     }

                    // }else{
                    //     foreach ($dataid as $k => $v) {
                    //         array_push($confid, $v);
                    //     }
                    // }

                    // dd($confid);
                    // dd($result);

                    $data = DB::select(DB::raw("SELECT a.id AS 'id',a.bh_name AS 'nama', b.lc_description AS 'lokasi'
                    FROM bh_hall a,lkp_location b WHERE a.bh_status!=0 AND a.fk_lkp_location = $loc
                    AND a.bh_name like '%$hall%'
                    AND b.id=a.fk_lkp_location
                    AND a.id NOT IN (SELECT fk_bh_hall FROM bh_confirm_booking WHERE date_booking >= '$start_date' AND date_booking <= '$end_date')"));




                    $halldata = array();
                    foreach ($data as $d => $e) {
                        array_push($halldata,$e->id);
                    }

                    // dd($halldata);

                    $res = BhHall::whereIn('bh_hall.id',$halldata)
                                    ->where('bh_status','!=',0)
                                    ->get();
                    // dd($res);
                    return $res;
                    break;

                case 2:
                    // marina
                    $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
                    ->where('bh_hall.fk_lkp_location',2)
                    ->where('bh_hall.bh_name','like',"%".$hall."%")
                    ->where('bh_hall.bh_status','!=',0)
                    ->select('bh_hall.id')
                    ->get();


                    // hall yang dah confirm booking
                    $confirm = BhConfirmBooking::join('bh_hall','bh_hall.id','=','bh_confirm_booking.fk_bh_hall')
                                            ->whereDate('bh_confirm_booking.date_booking','>=',$start_date)
                                            ->whereDate('bh_confirm_booking.date_booking','<=',$end_date)
                                            ->where('bh_hall.fk_lkp_location',2)
                                            ->get();
                    $confid = array();

                    if(count($confirm) > 0){

                        foreach ($data as $vkey => $vvalue) {
                            foreach ($confirm as $ckey => $cvalue) {
                                if($vvalue->id != $cvalue->id){
                                    array_push($confid, $vvalue->id);
                                }
                            }
                        }

                    }else{
                        foreach ($data as $k => $v) {
                            array_push($confid, $v->id);
                        }
                    }


                    $res = BhHall::whereIn('bh_hall.id',$confid)
                    ->where('bh_hall.bh_status','!=',0)
                    ->get();
                    return $res;


                    break;

                case 3:

                    $data = BhHallPicc::where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                    ->where('bh_hall_picc.bh_hall_name','like',"%".$hall."%")
                    ->select('bh_hall_picc.id')
                    ->get();


                    return $data;
                    break;

                case 4:
                    //lokasi semua
                    $data = BhHall::where('bh_name','like', "%".$hall."%")->where('bh_hall.bh_status',1)->get();
                    // dd($data);

                    $datahall = array();
                    if(isset($data)){
                        foreach ($data as $k => $v) {
                            array_push($datahall, $v->id);
                        }
                    }
                    // dd($datahall);

                    // hall yang dah confirm booking
                    $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                                ->whereDate('date_booking','<=',$end_date)
                                                ->get();

                    $confid = array();

                    if(isset($confirm)){
                        foreach ($confirm as $a => $b) {
                            array_push($confid, $b->fk_bh_hall);
                        }
                    } //end if

                    // start
                    $result = array();

                    if($confid == NULL){

                        foreach ($datahall as $p => $q) {
                            array_push($result,$q);
                        }

                    }else{

                        foreach ($datahall as $x => $y) {
                            foreach ($confid as $x1 => $y1) {
                                if($y != $y1){
                                    array_push($result, $y);
                                }else{

                                }
                            }
                        } //endforeach


                    } //end else
                    //end

                    $a = DB::table('bh_hall')
                    ->select(DB::raw("id, bh_name,'' AS 'bh_hall_name',fk_lkp_location"))
                    ->where('bh_hall.bh_status','!=',0)
                    ->whereNotIn('id',$confid)
                    ->where('bh_name','like',"%".$hall."%");


                    $b = DB::table('bh_hall_picc')
                    ->select(DB::raw("id, '' AS 'bh_name', bh_hall_name, '' AS 'fk_lkp_location'"))
                    ->where('bh_hall_name','like',"%".$hall."%");

                    $combined = $a->union($b)->get();

                    return $combined;
                    // dd($combined);
                    break;

                default:
                    # code...
                    break;
            }
            // end location
        }else{

            // semua filter ada

            // echo "apip";exit;

            $data = BhHall::join('lkp_location','lkp_location.id','=','bh_hall.fk_lkp_location')
            ->where('bh_hall.bh_status','!=',0)
            ->where('bh_hall.fk_lkp_location',1)
            ->select('bh_hall.*')
            ->get();

            $hallid = array();
            foreach ($data as $key => $value) {

                array_push($hallid, $value->id);
            }



            // hall yang dah confirm booking
            $confirm = BhConfirmBooking::whereDate('date_booking','>=',$start_date)
                                        ->whereDate('date_booking','<=',$end_date)
                                        ->get();

            $confid = array();

            foreach ($confirm as $a => $b) {
                array_push($confid, $b->id);
            }


            $result = array();

            if($confid == NULL){

                foreach ($hallid as $bb => $cc) {
                    array_push($result,$cc);
                }

            }else{

                foreach ($hallid as $x => $y) {
                    foreach ($confid as $x1 => $y1) {
                        if($y != $y1){
                            array_push($result, $y);
                        }else{

                        }
                    }
                } //endforeach


            } //end else


            $dataSearch = BhHall::whereIn('bh_hall.id',$result)->where('bh_hall.bh_status','!=',0)->paginate($pagination);
            return $dataSearch;


        } //end of else

    } //end of function

    public function getTempahanInternal() {
        return DB::select(DB::raw( "SELECT d.bb_start_date as 'start',f.id as 'qid',d.bb_end_date as 'end',e.bh_name,c.*,b.id AS 'sid',u.bud_name AS 'nama',u.fk_users AS 'id',u.bud_office_no,u.bud_phone_no,u.bud_email,a.bmb_booking_no AS 'notempahan',b.ls_description AS 'status',a.id AS 'idtempahan',a.internal_indi AS 'jenis'
  
        FROM main_booking a,user_profiles u,lkp_status b,bh_booking_detail c,bh_booking d,bh_hall e,bh_quotation f
        WHERE u.fk_users = a.fk_users and f.fk_main_booking = a.id and c.fk_bh_booking = d.id AND e.id = d.fk_bh_hall And d.fk_main_booking = a.id AND b.id = a.fk_lkp_status AND a.bmb_type_user = 2"));
    }

    public function  getInfoTempahanExternal()
    {
        return DB::select(DB::raw( "SELECT d.bb_start_date as 'start',d.bb_end_date as 'end',e.bh_name,c.*,b.id AS 'sid',u.bud_name AS 'nama',u.fk_users AS 'id',u.bud_office_no,u.bud_phone_no,u.bud_email,a.bmb_booking_no AS 'notempahan',b.ls_description AS 'status',a.id AS 'idtempahan',f.id as 'qid'
  
        FROM main_booking a,user_profiles u,lkp_status b,bh_booking_detail c,bh_booking d,bh_hall e,bh_quotation f
        WHERE u.fk_users = a.fk_users and f.fk_main_booking = a.id and c.fk_bh_booking = d.id AND e.id = d.fk_bh_hall And d.fk_main_booking = a.id AND b.id = a.fk_lkp_status AND a.bmb_type_user = 3 and a.bmb_indicator IS NULL "));
 
    }

    public function cariantempahan($location,$tarikhfrom,$tarikhto,$type){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');

        $defaultrange = [date('Y-m-d',strtotime($first_day_this_month)), date('Y-m-d',strtotime($last_day_this_month))];
        
        $status=['1','2','10'];
        $fk_main_booking=DB::table('main_booking')->whereNotIn('fk_lkp_status', $status)
            ->where('fk_lkp_location',1)
            ->pluck('id');
        
        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location=='' && $tarikhfrom=='' && $tarikhto==''):
                $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                    ->whereBetween('bb_start_date', $defaultrange)
                    ->whereBetween('bb_end_date', $defaultrange)
                    ->orderBy('bb_start_date','asc');
            else:
                $range = [date('Y-m-d',strtotime($tarikhfrom)), date('Y-m-d',strtotime($tarikhto))];
                $bh_hall=DB::table('bh_hall')->where('fk_lkp_location','=',$location)->pluck('id');
                if($tarikhfrom!='' ||$tarikhto!=''){
                    if($location==''){
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                    -> whereBetween('bb_start_date', $range)
                                    ->orderBy('bb_start_date','asc');
                        }else{
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                                -> whereBetween('bb_start_date', $range)
                                                -> whereBetween('bb_end_date', $range)
                                                ->orderBy('bb_start_date','asc');
                        }
                    }else{
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                            ->whereIn('fk_bh_hall',$bh_hall)
                                            ->whereBetween('bb_start_date', $range)
                                            ->orderBy('bb_start_date','asc');    
                        }else{
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                ->whereIn('fk_bh_hall',$bh_hall)
                                ->whereBetween('bb_start_date', $range)
                                ->whereBetween('bb_end_date', $range)
                                ->orderBy('bb_start_date','asc');
                        }
                    }
                }else{
                $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                    ->whereIn('fk_bh_hall',$bh_hall)
                    ->orderBy('bb_start_date','asc') ; 
            }
        endif;
        }else{
            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                ->whereBetween('bb_start_date', $defaultrange)
                ->whereBetween('bb_end_date', $defaultrange)
                ->orderBy('bb_start_date','asc');
        }
        return $query->get();
    }

    public function carianpelanggan($location,$tarikhfrom,$tarikhto,$type){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');

        $defaultrange = [date('Y-m-d',strtotime($first_day_this_month)), date('Y-m-d',strtotime($last_day_this_month))];
     
         //$status=['1','2','3'];
        $fk_main_booking=DB::table('main_booking')->where('fk_lkp_location','=',1)->pluck('id');

        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location=='' && $tarikhfrom=='' && $tarikhto==''):
                $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                    ->whereBetween('bb_start_date', $defaultrange)
                    ->whereBetween('bb_end_date', $defaultrange)
                    ->orderBy('bb_start_date','asc');
            else:
                $range = [date('Y-m-d',strtotime($tarikhfrom)), date('Y-m-d',strtotime($tarikhto))];
                
                $bh_hall=DB::table('bh_hall')->where('fk_lkp_location','=',$location)
                        ->pluck('id');

                if($tarikhfrom!='' ||$tarikhto!=''){
                    if($location==''){
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                                -> whereBetween('bb_start_date', $range)
                                                ->orderBy('bb_start_date','asc');
                    }else{
                            $query= DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                -> whereBetween('bb_start_date', $range)
                                -> whereBetween('bb_end_date', $range)
                                ->orderBy('bb_start_date','asc');
                        }
                    }else{
                        if($tarikhfrom==$tarikhto){
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                                ->whereIn('fk_bh_hall',$bh_hall)
                                                ->whereBetween('bb_start_date', $range)
                                                ->orderBy('bb_start_date','asc');
                        }else{
                            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                                ->whereIn('fk_bh_hall',$bh_hall)
                                ->whereBetween('bb_start_date', $range)
                                ->whereBetween('bb_end_date', $range)
                                ->orderBy('bb_start_date','asc');
                        }
                    }
                }else{
                    $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                            ->whereIn('fk_bh_hall',$bh_hall)
                            ->orderBy('bb_start_date','asc');
                }
            endif;
        }else{
            $query=DB::table('bh_booking')->whereIn('fk_main_booking',$fk_main_booking)
                ->whereBetween('bb_start_date', $defaultrange)
                ->whereBetween('bb_end_date', $defaultrange)
                ->orderBy('bb_start_date','asc');
        }
        return $query->groupBy('fk_main_booking')->get();
    }

    public function kutipandeposit($location,$tarikh_from,$tarikh_to,$typebayaran,$type,$typekutipan){
        // dd($location,$tarikh_from,$tarikh_to,$typebayaran,$type,$typekutipan);

        $fk_main_booking=DB::table('main_booking')->where('fk_lkp_location','=',1)
            ->pluck('id');
        
        $jenisbayaran=DB::table('lkp_payment_mode')->where('id','=',$typebayaran)->first();

        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');

        if($tarikh_from!='' ||$tarikh_to!=''){
            $tarikh_from = $tarikh_from->format('Y-m-d 00:00:00');
            $tarikh_to = $tarikh_to->format('Y-m-d 23:59:59');
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikh_from));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikh_to));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }
        if($typekutipan==false){
            $typebayaran='';
        }else{
            if($typekutipan==1){
                $typebayaran=$typebayaran;
            }else{
                $typebayaran='';
            }
        }
        // dd($location, $typebayaran, $typekutipan, $jenisbayaran);
        if(isset($location) || isset($tarikhfrom) || isset($tarikhto) || isset($typebayaran) || isset($typekutipan)){
            if($location=='' && $typebayaran==''):
                if($typekutipan==false){
                    // dd($fk_main_booking, $tarikh_from, $tarikh_to);
                    $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                        ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                        ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                        ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                        ->pluck('id');

                    $query=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                        ->where('product_indicator','=',1);
                }else{
            // dd('if2');
                    if($typekutipan==1){
                        $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                            ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                            ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                            ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                            ->pluck('id');

                        $query=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                            ->where('product_indicator','=',1);
                    }else{
                // dd('if3');
                        $bh_payment=DB::table('bh_payment_fpx')->whereIn('fk_main_booking',$fk_main_booking)
                            ->where('fk_lkp_payment_type','=',1)
                            ->where('fpx_status','=',1)
                            ->where('fpx_date','>=', array( $tarikh_from))
                            ->where('fpx_date' ,'<', array( $tarikh_to)) 
                            // ->whereIn('fk_bh_quotation',$quotation)
                            ->pluck('id');

                        $query=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment)
                            ->where('product_indicator','=',1);
                    }
                }
            else:
                if($location !='' && ($typebayaran=='' || $typebayaran==7 )){
                    if($typekutipan==false){
                // dd('if4');
                        $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                            ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                            ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                            ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                            ->pluck('id');

                        $query=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                            ->where('product_indicator','=',1);
                    }else{
                        if($typekutipan==1){
                    // dd('if5');
                            $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                                ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                ->pluck('id');

                            $query=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                                ->where('product_indicator','=',1); 
                        }else{
                            // dd('if6', $typekutipan);
                            $bh_payment=DB::table('bh_payment_fpx')->whereIn('fk_main_booking',$fk_main_booking)
                                ->where('fk_lkp_payment_type','=',1)
                                ->where('fpx_status','=',1)
                                ->where('fpx_date','>=', array( $tarikh_from))
                                ->where('fpx_date' ,'<', array( $tarikh_to))
                                // ->whereIn('fk_bh_quotation',$quotation)
                                ->pluck('id');

                            $query=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment)
                                ->where('product_indicator','=',1);
                        }  
                        // dd($query->get());
                    }
                }else{
                    if(($typebayaran=='' || $typebayaran==7 )){
                        if($typekutipan==1){
                    // dd('if7');
                            $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                                ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                                ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                                ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                                // ->whereIn('fk_bh_quotation',$quotation)
                                ->pluck('id');
                        }else{
                    // dd('if8');
                            $bh_payment=DB::table('bh_payment_fpx')->whereIn('fk_main_booking',$fk_main_booking)
                                ->where('fk_lkp_payment_type','=',1)
                                ->where('fpx_status','=',1)
                                ->where('fpx_date','>=', array( $tarikh_from))
                                ->where('fpx_date' ,'<', array( $tarikh_to)) 
                                // ->whereIn('fk_bh_quotation',$quotation)
                                ->pluck('id');
                        }
                    }else{
                        $bh_payment=DB::table('bh_payment')->whereIn('fk_main_booking',$fk_main_booking)
                            ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
                            ->where('bh_payment.bp_receipt_date' ,'<', array( $tarikh_to)) 
                            ->where('fk_lkp_payment_mode','=',$jenisbayaran->id)
                            ->where('fk_lkp_payment_type','=',1)//bayaran deposit
                            // ->whereIn('fk_bh_quotation',$quotation)
                            ->pluck('id');
                    }
                    $query= DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                        ->where('product_indicator','=',1);
                }
            endif;
        }else{
            $bh_payment=DB::table('bh_payment_fpx')->whereIn('fk_main_booking',$fk_main_booking)
                ->where('fk_lkp_payment_type','=',1)
                ->where('fpx_status','=',1)
                ->where('fpx_date','>=', array( $tarikh_from))
                ->where('fpx_date' ,'<', array( $tarikh_to)) 
                // ->whereIn('fk_bh_quotation',$quotation)
                ->pluck('id');

            $query=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment)
                ->where('product_indicator','=',1);
        }
        return $query->get();
    }

    public function kutipanhasil($location,$type,$tarikhfrom,$tarikhto){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');
        // dd($location,$tarikhfrom,$tarikhto,$type);
        $defaultrange = [date('Y-m-d 00:00:00',strtotime($first_day_this_month)), date('Y-m-d 23:59:59',strtotime($last_day_this_month))];

        $range = [date('Y-m-d 00:00:00',strtotime($tarikhfrom)), date('Y-m-d 23:59:59',strtotime($tarikhto))];

        $status=['1','2','3'];
        $fk_main_booking=DB::table('main_booking')->whereNotIn('fk_lkp_status',$status)
            ->where('fk_lkp_location','=',1)
            ->pluck('id');

            // dd($fk_main_booking);
        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }
        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location==''):
                $bh_payment=DB::table('bh_payment')->where('fk_lkp_payment_mode','<>',2)
                    ->where('bp_payment_status','=',1)
                    ->where('bp_receipt_date','>=', array( $tarikh_from))
                    ->where('bp_receipt_date' ,'<', array( $tarikh_to)) 
                    ->whereIn('fk_main_booking',$fk_main_booking)
                    ->pluck('id');

                $bh_hall=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                    ->pluck('fk_bh_hall');
            else:
                if($location !=''){
                    $bh_payment=DB::table('bh_payment')->where('fk_lkp_payment_mode','<>',2)
                        ->where('bp_payment_status','=',1)
                        ->where('bp_receipt_date','>=', array( $tarikh_from))
                        ->where('bp_receipt_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                        ->pluck('fk_bh_hall');
                }else{
                    $bh_payment=DB::table('bh_payment')->where('fk_lkp_payment_mode','<>',2)
                        ->where('bp_payment_status','=',1)
                        ->where('bp_receipt_date','>=', array( $tarikh_from))
                        ->where('bp_receipt_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                        ->pluck('fk_bh_hall');
                }
            endif;
       }else{
            $bh_payment=DB::table('bh_payment')->where('bp_receipt_date','>=', array( $tarikh_from))
                ->where('bp_receipt_date' ,'<', array( $tarikh_to)) 
                ->where('bp_payment_status','=',1)
                ->whereIn('fk_main_booking',$fk_main_booking)
                ->pluck('id');

            $bh_hall=DB::table('bh_payment_detail')->whereIn('fk_bh_payment',$bh_payment)
                ->pluck('fk_bh_hall');
        }
        // dd($bh_payment_fpx, $bh_hall);
        $data= DB::table('bh_payment')
         // Table Selection and Joins
            // ->join('bh_payment_detail', 'bh_payment_detail.fk_bh_payment', '=', 'fk_bh_payment.id')
            ->join('main_booking', 'main_booking.id', '=', 'bh_payment.fk_main_booking')
            ->join('bh_booking', 'bh_booking.fk_main_booking', '=', 'main_booking.id')
            ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', '=', 'bh_booking.id')
            ->join('lkp_gst_rate', 'lkp_gst_rate.id', '=', 'bh_booking.fk_lkp_gst_rate')
            ->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')
            // ->join('bh_hall', 'bh_hall.id', '=', 'bh_payment_detail.fk_bh_hall')
            ->join('lkp_event', 'lkp_event.id', '=', 'bh_booking_detail.fk_lkp_event')
            ->join('lkp_location', 'lkp_location.id', '=', 'main_booking.fk_lkp_location')
            ->join('lkp_payment_mode', 'lkp_payment_mode.id', '=', 'main_booking.fk_lkp_payment_mode')
            // Column Selection
            ->selectRaw('main_booking.id,main_booking.bmb_booking_no,
                        bh_payment.bp_receipt_number,
                        bh_payment.bp_receipt_date,
                        SUM(bh_booking.bb_total) AS Jumlah,
                        SUM(bh_booking.bb_gst) AS gst_rm,
                        SUM(bh_booking.bb_subtotal) AS subtotal,
                        user_profiles.bud_name,
                        lkp_event.le_description AS event_h,
                        lkp_location.lc_description AS location_h,
                        lkp_payment_mode.lpm_description AS payment_h,
                        bh_booking_detail.fk_lkp_event')
            // Conditions
            //  ->whereRaw('bh_payment_detail.fk_bh_equipment is null')
            //  ->whereIn('bh_payment_detail.fk_bh_hall',$bh_hall)
             ->where('bh_payment.fk_lkp_payment_mode',2)
             ->where('bh_payment.bp_receipt_date','>=', array( $tarikh_from))
             ->where('bh_payment.bp_receipt_date','<', array( $tarikh_to))
            //  Grouping and Ordering:
             ->groupBy('bh_payment.bp_receipt_number')
            //  ->groupBy('bh_payment_detail.fk_bh_hall')
             ->groupBy('bh_booking.fk_lkp_gst_rate')
            //  ->groupBy('bh_payment_detail.product_indicator')
             ->orderBy('bh_payment.bp_receipt_date','asc')
             
             ->get();
        //end main query
        $results = $data;
        // dd($results,$bh_payment, $bh_hall,$fk_main_booking);
        return $results;
    }

    public function hasil($location, $mode, $start, $end) {
        $query = DB::table(function ($subquery) use ($location, $start, $end) {
            $subquery->select(
                'b.bmb_booking_no',
                'e.fullname',
                'd.bh_name',
                'd.bh_code',
                'a.bp_receipt_date',
                'a.bp_receipt_number',
                'a.bp_payment_ref_no',
                'a.bp_total_amount',
                'f.id as paymode',
                'f.lpm_description',
                'b.fk_lkp_location',
                'h.fk_lkp_event',
                // DB::raw('SUM(h.est_total) as ebf_subtotal')
                // DB::raw('h.est_total as ebf_subtotal')
            )
            ->from('bh_payment as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('bh_booking as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('bh_hall as d', 'd.id', '=', 'c.fk_bh_hall')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('bh_booking_detail as h', 'h.fk_bh_booking', '=', 'c.id')
            ->join('lkp_event as i', 'i.id', '=', 'h.fk_lkp_event')
            // ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            // ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            ->whereDate('a.bp_receipt_date', '>=', $start)
            ->whereDate('a.bp_receipt_date', '<=', $end)
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->groupBy('b.bmb_booking_no', 'd.bh_name')
            ->union(
                DB::table('bh_payment_fpx as a1')
                ->select(
                    'b1.bmb_booking_no',
                    'e1.fullname',
                    'd1.bh_name',
                    'd1.bh_code',
                    'a1.fpx_trans_date',
                    'a1.total_amount',
                    'b1.sap_no',
                    'i1.fpx_serial_no as bp_payment_ref_no',
                    'f1.id as paymode',
                    'f1.lpm_description',
                    'b1.fk_lkp_location',
                    'g1.fk_lkp_event',
                    // DB::raw('SUM(h1.est_total) as ebf_subtotal')
                    // DB::raw('h1.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('bh_booking as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('bh_hall as d1', 'd1.id', '=', 'c1.fk_bh_hall')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_type')
                // ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                // ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('bh_payment_fpx_log as i1', 'i1.fpx_trans_id', '=', 'a1.fpx_serial_no')
                ->join('bh_booking_detail as g1', 'g1.fk_bh_booking', '=', 'c1.id')
                ->join('lkp_event as h1', 'h1.id', '=', 'g1.fk_lkp_event')
                ->whereDate('a1.fpx_trans_date', '>=', $start)
                ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.bh_name')
            );
        }, 'subquery')
        ->where('paymode', $mode)
        ->get();
        // dd($location, $mode, $start, $end, $query);
        return $query;
    }

    public function harian($location, $mode, $start) {
        $query = DB::table(function ($subquery) use ($location, $start) {
            $subquery->select(
                'b.bmb_booking_no',
                'e.fullname',
                'd.bh_name',
                'd.bh_code',
                'a.bp_receipt_date',
                'a.bp_receipt_number',
                'a.bp_payment_ref_no',
                'a.bp_total_amount',
                'a.bp_subtotal',
                'f.id as paymode',
                'f.lpm_description',
                'b.fk_lkp_location',
                'h.fk_lkp_event',
                // DB::raw('SUM(h.est_total) as ebf_subtotal')
                // DB::raw('h.est_total as ebf_subtotal')
            )
            ->from('bh_payment as a')
            ->join('main_booking as b', 'a.fk_main_booking', '=', 'b.id')
            ->join('bh_booking as c', 'c.fk_main_booking', '=', 'b.id')
            ->join('bh_hall as d', 'd.id', '=', 'c.fk_bh_hall')
            ->join('users as e', 'e.id', '=', 'b.fk_users')
            ->join('lkp_payment_mode as f', 'f.id', '=', 'a.fk_lkp_payment_mode')
            ->join('bh_booking_detail as h', 'h.fk_bh_booking', '=', 'c.id')
            ->join('lkp_event as i', 'i.id', '=', 'h.fk_lkp_event')
            // ->join('et_sport_book as g', 'g.fk_et_booking_facility', '=', 'c.id')
            // ->join('et_sport_time as h', 'h.fk_et_sport_book', '=', 'g.id')
            ->whereDate('a.bp_receipt_date', '=', $start)
            // ->whereDate('a.bp_receipt_date', '<=', $end)
            ->where('b.fk_lkp_location', $location)
            ->where('b.fk_lkp_status', 5)
            ->groupBy('b.bmb_booking_no', 'd.bh_name')
            ->union(
                DB::table('bh_payment_fpx as a1')
                ->select(
                    'b1.bmb_booking_no',
                    'e1.fullname',
                    'd1.bh_name',
                    'd1.bh_code',
                    'a1.fpx_trans_date',
                    'a1.fpx_trans_id',
                    'a1.total_amount',
                    'b1.sap_no',
                    'i1.fpx_serial_no as bp_payment_ref_no',
                    'f1.id as paymode',
                    'f1.lpm_description',
                    'b1.fk_lkp_location',
                    'g1.fk_lkp_event',
                    // DB::raw('SUM(h1.est_total) as ebf_subtotal')
                    // DB::raw('h1.est_total as ebf_subtotal')
                )
                ->join('main_booking as b1', 'a1.fk_main_booking', '=', 'b1.id')
                ->join('bh_booking as c1', 'c1.fk_main_booking', '=', 'b1.id')
                ->join('bh_hall as d1', 'd1.id', '=', 'c1.fk_bh_hall')
                ->join('users as e1', 'e1.id', '=', 'b1.fk_users')
                ->join('lkp_payment_mode as f1', 'f1.id', '=', 'a1.fk_lkp_payment_type')
                // ->join('et_sport_book as g1', 'g1.fk_et_booking_facility', '=', 'c1.id')
                // ->join('et_sport_time as h1', 'h1.fk_et_sport_book', '=', 'g1.id')
                ->join('bh_payment_fpx_log as i1', 'i1.fpx_trans_id', '=', 'a1.fpx_serial_no')
                ->join('bh_booking_detail as g1', 'g1.fk_bh_booking', '=', 'c1.id')
                ->join('lkp_event as h1', 'h1.id', '=', 'g1.fk_lkp_event')
                ->whereDate('a1.fpx_trans_date', '=', $start)
                // ->whereDate('a1.fpx_trans_date', '<=', $end)
                ->where('b1.fk_lkp_location', $location)
                ->where('b1.fk_lkp_status', 5)
                ->groupBy('b1.bmb_booking_no', 'd1.bh_name')
            );
        }, 'subquery')
        ->where('paymode', $mode)
        ->get();
        // dd($location, $mode, $start, $end, $query);
        return $query;
    }

    public function generatequono() {
        $data = All::GetRow('lkp_sequence', 'id', 1);
        // $data = LkpSequence::first();
        $no = $data->quotation_no;
        if (($no < 9999)) {
            $run_no = $no + 1;
            /*-----------update lkpsequence-----------*/
            // $data->quotation_no = $run_no;
            // $data->save();
            $b = array(
                'quotation_no' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        } else {
            $run_no = 1;
            /*-----------update lkpsequence-----------*/
            // $b = new LkpSequence;
            // $b->quotation_no = $run_no;
            // $b->save();
            $b = array(
                'quotation_no' => $run_no,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            All::Insert('lkp_sequence', $b);
            /*-----------end update lkpsequence-----------*/
        }
        while (strlen($run_no) <= 4) {
            $run_no = '0' . $run_no;
        }
        return $run_no;
    }

    public function kutipanharian($location,$type,$tarikhfrom){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');
        // dd($location,$type,$tarikhfrom);

        $defaultrange = [date('Y-m-d 00:00:00',strtotime($first_day_this_month)), date('Y-m-d 23:59:59',strtotime($last_day_this_month))];
        $tarikhto = $tarikhfrom;
        $range = [date('Y-m-d 00:00:00',strtotime($tarikhfrom)), date('Y-m-d 23:59:59',strtotime($tarikhto))];

        $status=['1','2','3'];
        $fk_main_booking=DB::table('main_booking')->whereNotIn('fk_lkp_status',$status)
            ->where('fk_lkp_location','=',1)
            ->pluck('id');

        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }
        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location==''):
                $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                    ->where('fpx_status','=',1)
                    ->where('fpx_trans_date','>=', array( $tarikh_from))
                    ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                    ->whereIn('fk_main_booking',$fk_main_booking)
                    ->pluck('id');

                $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                    ->pluck('fk_bh_hall');
            else:
                if($location !=''){
                    $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                        ->where('fpx_status','=',1)
                        ->where('fpx_trans_date','>=', array( $tarikh_from))
                        ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                        ->pluck('fk_bh_hall');
                }else{
                    $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                        ->where('fpx_status','=',1)
                        ->where('fpx_trans_date','>=', array( $tarikh_from))
                        ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                        ->pluck('fk_bh_hall');
                }
            endif;
       }else{
            $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fpx_trans_date','>=', array( $tarikh_from))
                ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                ->where('fpx_status','=',1)
                ->whereIn('fk_main_booking',$fk_main_booking)
                ->pluck('id');

            $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                ->pluck('fk_bh_hall');
        }
        $data= DB::table('bh_payment_fpx')
            // ->join('bh_fpx_detail', 'bh_fpx_detail.fk_bh_payment_fpx', '=', 'bh_payment_fpx.id')
            ->join('main_booking', 'main_booking.id', '=', 'bh_payment_fpx.fk_main_booking')
            ->join('bh_booking', 'bh_booking.fk_main_booking', '=', 'main_booking.id')
            ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', '=', 'bh_booking.id')
            ->join('lkp_gst_rate', 'lkp_gst_rate.id', '=', 'bh_booking.fk_lkp_gst_rate')
            ->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')
            // ->join('bh_hall', 'bh_hall.id', '=', 'bh_fpx_detail.fk_bh_hall')
            ->join('lkp_event', 'lkp_event.id', '=', 'bh_booking_detail.fk_lkp_event') 
            ->selectRaw('main_booking.id,main_booking.bmb_booking_no,
             bh_payment_fpx.fpx_serial_no,
             bh_payment_fpx.fpx_trans_date,
             SUM(bh_booking.bb_total) AS Jumlah,
             SUM(bh_booking.bb_gst) AS gst_rm,
             SUM(bh_booking.bb_subtotal) AS subtotal,
             user_profiles.bud_name,
             lkp_event.le_description AS event_h,
             bh_booking_detail.fk_lkp_event')
            //  ->whereRaw('bh_fpx_detail.fk_bh_equipment is null')
            //  ->whereIn('bh_fpx_detail.fk_bh_hall',$bh_hall)
             ->where('bh_payment_fpx.fk_lkp_payment_type',2)
             ->where('bh_payment_fpx.fpx_trans_date','>=', array( $tarikh_from))
             ->where('bh_payment_fpx.fpx_trans_date','<', array( $tarikh_to)) 
             ->groupBy('bh_payment_fpx.fpx_serial_no')
            //  ->groupBy('bh_fpx_detail.fk_bh_hall')
             ->groupBy('bh_booking.fk_lkp_gst_rate')
            //  ->groupBy('bh_fpx_detail.product_indicator')
             ->orderBy('bh_payment_fpx.fpx_trans_id','asc')
             ->get();
        // $data= DB::table('bh_payment_fpx')
        //     ->join('bh_fpx_detail', 'bh_fpx_detail.fk_bh_payment_fpx', '=', 'bh_payment_fpx.id')
        //     ->join('main_booking', 'main_booking.id', '=', 'bh_payment_fpx.fk_main_booking')
        //     ->join('bh_booking', 'bh_booking.fk_main_booking', '=', 'main_booking.id')
        //     ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', '=', 'bh_booking.id')
        //     ->join('lkp_gst_rate', 'lkp_gst_rate.id', '=', 'bh_booking.fk_lkp_gst_rate')
        //     ->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')
        //     ->join('bh_hall', 'bh_hall.id', '=', 'bh_fpx_detail.fk_bh_hall')
        //     ->join('lkp_event', 'lkp_event.id', '=', 'bh_booking_detail.fk_lkp_event') 
        //     ->selectRaw('main_booking.id,main_booking.bmb_booking_no,
        //      bh_fpx_detail.fk_bh_hall,
        //      bh_payment_fpx.fpx_serial_no,
        //      bh_payment_fpx.fpx_trans_date,
        //      bh_fpx_detail.product_indicator,
        //      bh_fpx_detail.fk_bh_equipment,
        //      bh_fpx_detail.id,lkp_gst_rate.lgr_gst_code,
        //      SUM(bh_booking.bb_total) AS Jumlah,
        //      SUM(bh_booking.bb_gst) AS gst_rm,
        //      SUM(bh_booking.bb_subtotal) AS subtotal,
        //      user_profiles.bud_name,
        //      bh_hall.bh_name as item,
        //      bh_hall.bh_code_fee AS code_fee,
        //      bh_hall.bh_name AS eqp_name,
        //      bh_hall.bh_name,
        //      lkp_event.le_description AS event_h,
        //      bh_booking_detail.fk_lkp_event')
        //      ->whereRaw('bh_fpx_detail.fk_bh_equipment is null')
        //      ->whereIn('bh_fpx_detail.fk_bh_hall',$bh_hall)
        //      ->where('bh_payment_fpx.fk_lkp_payment_type',2)
        //      ->where('bh_payment_fpx.fpx_trans_date','>=', array( $tarikh_from))
        //      ->where('bh_payment_fpx.fpx_trans_date','<', array( $tarikh_to)) 
        //      ->groupBy('bh_payment_fpx.fpx_serial_no')
        //      ->groupBy('bh_fpx_detail.fk_bh_hall')
        //      ->groupBy('bh_booking.fk_lkp_gst_rate')
        //      ->groupBy('bh_fpx_detail.product_indicator')
        //      ->orderBy('bh_payment_fpx.fpx_trans_id','asc')
        //      ->get();
        //end main query
        $results = $data;
        return $results;
    }

    public function kutipantertunggak($location,$type,$tarikhfrom,$tarikhto){
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');
        // dd($location,$type,$tarikhfrom);

        $defaultrange = [date('Y-m-d 00:00:00',strtotime($first_day_this_month)), date('Y-m-d 23:59:59',strtotime($last_day_this_month))];
        // $tarikhto = $tarikhfrom;
        $range = [date('Y-m-d 00:00:00',strtotime($tarikhfrom)), date('Y-m-d 23:59:59',strtotime($tarikhto))];

        $status=['1','2','3'];
        $fk_main_booking=DB::table('main_booking')->whereNotIn('fk_lkp_status',$status)
            ->where('fk_lkp_location','=',1)
            ->pluck('id');

        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d 00:00:00',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d 23:59:59',strtotime($last_day_this_month));
        }
        if(isset($location) || isset($tarikhfrom) || isset($tarikhto)){
            if($location==''):
                $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                    ->where('fpx_status','=',1)
                    ->where('fpx_trans_date','>=', array( $tarikh_from))
                    ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                    ->whereIn('fk_main_booking',$fk_main_booking)
                    ->pluck('id');

                $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                    ->pluck('fk_bh_hall');
            else:
                if($location !=''){
                    $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                        ->where('fpx_status','=',1)
                        ->where('fpx_trans_date','>=', array( $tarikh_from))
                        ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                        ->pluck('fk_bh_hall');
                }else{
                    $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fk_lkp_payment_type','<>',1)
                        ->where('fpx_status','=',1)
                        ->where('fpx_trans_date','>=', array( $tarikh_from))
                        ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                        ->whereIn('fk_main_booking',$fk_main_booking)
                        ->pluck('id');

                    $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                        ->pluck('fk_bh_hall');
                }
            endif;
       }else{
            $bh_payment_fpx=DB::table('bh_payment_fpx')->where('fpx_trans_date','>=', array( $tarikh_from))
                ->where('fpx_trans_date' ,'<', array( $tarikh_to)) 
                ->where('fpx_status','=',1)
                ->whereIn('fk_main_booking',$fk_main_booking)
                ->pluck('id');

            $bh_hall=DB::table('bh_fpx_detail')->whereIn('fk_bh_payment_fpx',$bh_payment_fpx)
                ->pluck('fk_bh_hall');
        }
        $data= DB::table('bh_payment_fpx')
            // ->join('bh_fpx_detail', 'bh_fpx_detail.fk_bh_payment_fpx', '=', 'bh_payment_fpx.id')
            ->join('main_booking', 'main_booking.id', '=', 'bh_payment_fpx.fk_main_booking')
            ->join('bh_booking', 'bh_booking.fk_main_booking', '=', 'main_booking.id')
            ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', '=', 'bh_booking.id')
            ->join('lkp_gst_rate', 'lkp_gst_rate.id', '=', 'bh_booking.fk_lkp_gst_rate')
            ->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')
            // ->join('bh_hall', 'bh_hall.id', '=', 'bh_fpx_detail.fk_bh_hall')
            ->join('lkp_event', 'lkp_event.id', '=', 'bh_booking_detail.fk_lkp_event') 
            ->selectRaw('main_booking.id,main_booking.bmb_booking_no,
             bh_payment_fpx.fpx_serial_no,
             bh_payment_fpx.fpx_trans_date,
             SUM(bh_booking.bb_total) AS Jumlah,
             SUM(bh_booking.bb_gst) AS gst_rm,
             SUM(bh_booking.bb_subtotal) AS subtotal,
             user_profiles.bud_name,
             lkp_event.le_description AS event_h,
             bh_booking_detail.fk_lkp_event')
            //  ->whereRaw('bh_fpx_detail.fk_bh_equipment is null')
            //  ->whereIn('bh_fpx_detail.fk_bh_hall',$bh_hall)
             ->where('bh_payment_fpx.fk_lkp_payment_type',2)
             ->where('bh_payment_fpx.fpx_trans_date','>=', array( $tarikh_from))
             ->where('bh_payment_fpx.fpx_trans_date','<', array( $tarikh_to)) 
             ->groupBy('bh_payment_fpx.fpx_serial_no')
            //  ->groupBy('bh_fpx_detail.fk_bh_hall')
             ->groupBy('bh_booking.fk_lkp_gst_rate')
            //  ->groupBy('bh_fpx_detail.product_indicator')
             ->orderBy('bh_payment_fpx.fpx_trans_id','asc')
             ->get();
        // $data= DB::table('bh_payment_fpx')
        //     ->join('bh_fpx_detail', 'bh_fpx_detail.fk_bh_payment_fpx', '=', 'bh_payment_fpx.id')
        //     ->join('main_booking', 'main_booking.id', '=', 'bh_payment_fpx.fk_main_booking')
        //     ->join('bh_booking', 'bh_booking.fk_main_booking', '=', 'main_booking.id')
        //     ->join('bh_booking_detail', 'bh_booking_detail.fk_bh_booking', '=', 'bh_booking.id')
        //     ->join('lkp_gst_rate', 'lkp_gst_rate.id', '=', 'bh_booking.fk_lkp_gst_rate')
        //     ->join('user_profiles', 'user_profiles.fk_users', '=', 'main_booking.fk_users')
        //     ->join('bh_hall', 'bh_hall.id', '=', 'bh_fpx_detail.fk_bh_hall')
        //     ->join('lkp_event', 'lkp_event.id', '=', 'bh_booking_detail.fk_lkp_event') 
        //     ->selectRaw('main_booking.id,main_booking.bmb_booking_no,
        //      bh_fpx_detail.fk_bh_hall,
        //      bh_payment_fpx.fpx_serial_no,
        //      bh_payment_fpx.fpx_trans_date,
        //      bh_fpx_detail.product_indicator,
        //      bh_fpx_detail.fk_bh_equipment,
        //      bh_fpx_detail.id,lkp_gst_rate.lgr_gst_code,
        //      SUM(bh_booking.bb_total) AS Jumlah,
        //      SUM(bh_booking.bb_gst) AS gst_rm,
        //      SUM(bh_booking.bb_subtotal) AS subtotal,
        //      user_profiles.bud_name,
        //      bh_hall.bh_name as item,
        //      bh_hall.bh_code_fee AS code_fee,
        //      bh_hall.bh_name AS eqp_name,
        //      bh_hall.bh_name,
        //      lkp_event.le_description AS event_h,
        //      bh_booking_detail.fk_lkp_event')
        //      ->whereRaw('bh_fpx_detail.fk_bh_equipment is null')
        //      ->whereIn('bh_fpx_detail.fk_bh_hall',$bh_hall)
        //      ->where('bh_payment_fpx.fk_lkp_payment_type',2)
        //      ->where('bh_payment_fpx.fpx_trans_date','>=', array( $tarikh_from))
        //      ->where('bh_payment_fpx.fpx_trans_date','<', array( $tarikh_to)) 
        //      ->groupBy('bh_payment_fpx.fpx_serial_no')
        //      ->groupBy('bh_fpx_detail.fk_bh_hall')
        //      ->groupBy('bh_booking.fk_lkp_gst_rate')
        //      ->groupBy('bh_fpx_detail.product_indicator')
        //      ->orderBy('bh_payment_fpx.fpx_trans_id','asc')
        //      ->get();
        //end main query
        $results = $data;
        return $results;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\All;
use Carbon\Carbon;

class Event extends Model
{
	//  public function fasility(){
    //     $query = DB::select(DB::raw("
    //         SELECT * FROM bh_hall WHERE bh_status = 1 AND bh_hall_owner = 0
    //     "));
    //     return $query;
    // }
    public function rateTapak(){
        $query = DB::table('spa_location')
            ->select('name', 'category', 'rent_perday', 'deposit')
            ->where('status', 1)
            ->orderBy('category', 'asc')
            ->get();
        return $query;
    }
    public function kadarTapak()
    {
        $query = DB::table('spa_location')
            ->select('name', 'category', 'rent_perday', 'deposit')
            ->where('status', 1)
            ->orderBy('category', 'asc')
            ->get();
            
        // Group the results by category
        $groupedData = $query->groupBy('category');
        // dd($groupedData);
        // Initialize an array to hold the final result
        $result = [];

        // Iterate over the grouped data
        foreach ($groupedData as $category => $items) {
            // Sort items within each category by rent_perday and deposit
            $sortedItems = $items->sortBy('rent_perday')->sortBy('deposit');
            // Count the number of items in the category
            $count = $sortedItems->count();
            // Initialize a rowspan counter
             $rentPerDayRowspanCounter  = 0;
            // Iterate over the sorted items in the category
            foreach ($sortedItems as $item) {
                    // Increment rowspan counter for each unique rent_perday value
                    $rentPerDayRowspanCounter++;
                    // Initialize a rowspan counter for deposit
                    $depositRowspanCounter = 0;
                    // Check if the current item has the same rent_perday as the previous item
                    if ($rentPerDayRowspanCounter > 1 && $item->rent_perday == $result[count($result) - 1]->rent_perday) {
                        // If rent_perday is the same, use the same deposit rowspan
                        $depositRowspanCounter = $result[count($result) - 1]->deposit_rowspan;
                    }
                    // Increment deposit rowspan counter for each unique deposit value
                    $depositRowspanCounter++;
                    // Add the category count to each item
                    $item->rowspan = $count;
                    // Add the rent_perday rowspan counter to each item
                    $item->rent_perday_rowspan = $rentPerDayRowspanCounter;
                    // Add the deposit rowspan counter to each item
                    $item->deposit_rowspan = $depositRowspanCounter;
                    // Add the item to the final result array
                    $result[] = $item;
            }
        }
        // dd($result);
        return $result;
    }

    public function sekatanHari($lokasi, $tarikh = null){
        $data = DB::table('spa_booking_confirm')
            ->select('date_booking')
            ->where('fk_spa_location', $lokasi)
            ->whereDate('date_booking', '>=', now()->toDateString())
            // ->where('date_booking', '<=', $tarikh->tarikhMula) // Check if 'date_booking' is less than or equal to $tarikh
            // ->where('date_booking', '>=', $tarikh->tarikhAkhir) // Check if 'date_booking_end' is greater than or equal to $tarikh
            ->get();

        // foreach ($data as $index => $booking) {
        //     $endTime = strtotime($booking->date_booking);
        //     $nextDay = date('Y-m-d', strtotime($booking->date_booking . ' +1 day'));
        //     $temp = (object)[
        //             'date_booking' => $nextDay,
        //     ];
        //     $data->push($temp);

        //     break;
        // }
        
        return $data;
    }
    
    public function sekatanHariSemuaLokasi($lokasi = null, $tarikh = null){
        $data = DB::table('spa_booking_confirm')
            ->whereNotNull('fk_spa_location')
            ->whereDate('date_booking', '>=', now()->toDateString())
            // ->groupBy('fk_main_booking')
            ->get();
            
        $groupedData = [];

        foreach ($data as $item) {
            $fk_spa_location = $item->fk_spa_location;
            $dateBooking = $item->date_booking;

            if (!isset($groupedData[$fk_spa_location])) {
                $groupedData[$fk_spa_location] = [];
            }

            $groupedData[$fk_spa_location][] = $dateBooking;
        }

        return $groupedData;
    }

    public function locationPictures($id = null){
        $query = DB::table('spa_location')
            ->leftJoin('spa_location_picture', 'spa_location_picture.fk_spa_location', '=', 'spa_location.id')
            ->selectRaw('spa_location.*, spa_location_picture.id as slpid, spa_location_picture.virtual_img, spa_location_picture.file_name') 
            ->where('status', 1);
    
        if ($id != null) {
            $query->where('spa_location.id', $id);
        }
    
        $result = $query->get();
    
        return $result;
    }

    public function fasilitymap(){
        $query = DB::select(DB::raw("
            SELECT * FROM spa_location WHERE status = 1
        "));
        return $query;
    }

    public function fasility(){
        $query = DB::select(DB::raw("
            SELECT * FROM spa_location WHERE status = 1
        "));
        return $query;
    }

    public function facility($id){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_hall WHERE bh_status = 1 AND id = $id AND bh_hall_owner = 0
        "));
        return $query;
    }
	
    public function event(){
        $query = DB::select(DB::raw("
		SELECT * FROM spa_location WHERE STATUS = 1 ORDER BY NAME
        "));
        return $query;
    }

    public function eventById($id){
        $query = DB::table('spa_location')
            ->where('status', 1)
            ->get();
        // $query = DB::select(DB::raw("
        // SELECT * FROM spa_location WHERE STATUS = 1 AND id = :id ORDER BY NAME
        // "), ['id' => $id]);
        // return isset($query[0]) ? $query[0] : null;
        return $query;
    }

    public function eventi($id){
        $query = DB::select(DB::raw("
            SELECT * FROM bh_location WHERE status = 1 AND id = $id
        "));
        return $query;
    }

    public function details($id){
        $query = DB::select(DB::raw("
            SELECT * FROM et_facility_type WHERE id = $id AND fk_et_facility IN (
                SELECT id FROM et_facility WHERE ef_type = 2
            ) ORDER BY fk_lkp_location ASC, id ASC
        "));
        return $query;
    }

    public function price($id){
        $query = DB::select(DB::raw("
            SELECT DISTINCT c.efd_name, d.efp_unit_price, d.fk_lkp_slot_cat, d.efp_day_cat  FROM et_facility_type AS a
            JOIN et_facility AS b ON b.id = a.fk_et_facility
            JOIN et_facility_detail AS c ON c.fk_et_facility_type = a.id
            JOIN et_facility_price AS d ON d.fk_et_facility = b.id
            WHERE a.id = $id
        "));

        return $query;
    }

    
    public function getRn() {
        $running = All::GetRow('lkp_sequence', 'id', 1);
        $rn = $running->booking_no;
        $newcount1 = $rn + 1;
        $newcount = 0;
        if ($newcount1 > 99999) {
            $newcount = 1;
        } else {
            $newcount = $newcount1;
        }
        $data = array(
            'booking_no'        => $newcount,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_sequence', 1, $data);
        return $rn;
    }

    public function laporan_acara($id, $data, $data1){
        $query = DB::select(DB::raw('
            SELECT b1.bmb_booking_no, c1.fullname, c1.email, a1.fk_main_booking, a1.event_date, a1.event_date_end, a1.enter_time, a1.exit_time
            FROM spa_booking_event as a1
            JOIN main_booking AS b1 ON b1.id = a1.fk_main_booking
            JOIN users AS c1 ON c1.id = b1.fk_users
            WHERE a1.fk_spa_location = "'.$id.'" AND a1.event_date = "'.$data.'" AND a1.event_date_end ="'.$data1.'"
        '));

        return $query;
        // SELECT DISTINCT d1.bmb_booking_no ,c1.*,h1.fullname, h1.email, i1.bud_reference_id, i1.bud_phone_no, g1.efd_name
        // FROM et_sport_time AS a1
        // JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
        // JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
        // JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
        // JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
        // JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
        // JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
        // JOIN users AS h1 ON h1.id = d1.fk_users
        // JOIN user_profiles AS i1 ON i1.fk_users = h1.id
        // WHERE d1.fk_lkp_location = '.$id.' AND d1.fk_lkp_status IN (5,11) AND b1.esb_booking_date BETWEEN "'.$data.'" AND "'.$data1.'"
        // ORDER BY c1.ebf_start_date DESC
    }

    
    public function laporan_keseluruhan() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTables('spa_booking_event', 'main_booking', 'spa_booking_person')->where('fk_lkp_status', '!=', 18);
        // $query = All::JoinTwoTable('spa_booking_event', 'main_booking');
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // SELECT * FROM main_booking WHERE fk_lkp_status != 11 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'

        return $query;
    }

    public function laporan_baharu() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 19);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 19);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'

        return $query;
    }

    public function keputusan_mesyuarat() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 22);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 22);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_pembentangan_semula() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 23);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 23);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_perlu_maklumat_bayaran() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 30);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 30);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_perlu_bayaran_pendahuluan()
    {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 24);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 24);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_pendahuluan_diterima()
    {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 25);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 25);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_perlu_bayaran_penuh() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 26);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 26);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_bayaran_penuh_diterima() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 27);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 27);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 22 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status IN (2,3) AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_kelulusan() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', 22);
        // $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 22);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 23 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // $query = DB::select(DB::raw('
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 13 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        // '));

        return $query;
    }

    public function laporan_harian() {
        $currentDate = Carbon::now()->format('Y-m-d');
        $query = DB::table('main_booking')
            ->where('bmb_booking_no', 'LIKE', 'SPA%')
            ->whereDate('bmb_booking_date', $currentDate)
            ->get();
        return $query;
    }

    public function laporan_keseluruhanId() {
        $year = date('Y');
        $month = date('m');
        $query = DB::select(DB::raw("
            SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND bmb_booking_no LIKE 'SPA%' WHERE fk_users = 
        "));
        // SELECT * FROM main_booking WHERE fk_lkp_status != 11 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'

        return $query;
    }

    public function laporan_dalaman() {
        $year = date('Y');
        $month = date('m');
        $query = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_lkp_status', 19);
        // $query = DB::select(DB::raw("
        //     SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND bmb_booking_no LIKE 'SPA%'
        // "));
        // SELECT * FROM main_booking WHERE fk_lkp_status = 19 AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'

        return $query;
    }

    public function laporan_user($id) {
        $year = date('Y');
        $month = date('m');
        $query = DB::select(DB::raw("
            SELECT * FROM main_booking 
            WHERE fk_users = $id 
            AND bmb_booking_no LIKE 'SPA%'
            "));
            // SELECT * FROM main_booking WHERE fk_users = '.$id.' AND YEAR(bmb_booking_date) = '.$year.' AND MONTH(bmb_booking_date) = '.$month.'
        return $query;
    }

    public function tempahan_saya($id)
    {
        $spa = 'SPA%';
        $query = DB::select(DB::raw("
            SELECT * FROM main_booking 
            WHERE fk_users = $id 
            AND bmb_booking_no LIKE 'SPA%'
            "));

        return $query;
    }

    // Laporan::Mula
    public function listacara($tarikhfrom,$tarikhto,$locationsearch,$type,$statussearch) {
        $first_day_this_month = date('01-m-Y');
        $last_day_this_month  = date('t-m-Y');
        
        if($tarikhfrom!='' ||$tarikhto!=''){
            $tarikh_from=date('Y-m-d',strtotime($tarikhfrom));
            $tarikh_to=date('Y-m-d',strtotime($tarikhto));
        }else{
            $tarikh_from=date('Y-m-d',strtotime($first_day_this_month));
            $tarikh_to=date('Y-m-d',strtotime($last_day_this_month));
        }

        $data= DB::table('main_booking')
            ->leftJoin('spa_booking_organiser','spa_booking_organiser.fk_main_booking','=','main_booking.id')
            ->leftJoin('spa_booking_event','spa_booking_event.fk_main_booking','=','main_booking.id')
            ->leftJoin('spa_meeting_result','spa_meeting_result.fk_main_booking','=','main_booking.id')
            ->leftJoin('spa_payment','spa_payment.fk_main_booking','=','main_booking.id')
            ->leftJoin('spa_location','spa_location.id','=','spa_booking_event.fk_spa_location')
            ->leftJoin('lkp_status','main_booking.fk_lkp_status','=','lkp_status.id')
            ->leftJoin('user_role','user_role.user_id','=','main_booking.fk_users')
            ->select('main_booking.id','main_booking.bmb_booking_no','spa_booking_event.event_date',
                'spa_booking_event.event_name','spa_booking_organiser.agency_name','spa_location.name',
                'spa_booking_event.visitor_amount','spa_booking_event.rank_host','spa_meeting_result.meeting_no',
                DB::raw('SUM(spa_payment.amount) AS bayar'),
                'spa_booking_event.other_rank',
                'main_booking.bmb_rounding','main_booking.bmb_deposit_rm','spa_location.id AS locationid','lkp_status.ls_description','main_booking.fk_users','user_role.role_id')
            ->whereRaw('(spa_meeting_result.fk_lkp_status_meeting =30 or (user_role.role_id=16 or user_role.role_id=1 ))')//include internal
            ->where('spa_booking_event.event_date', '>=', $tarikh_from)
            ->where('spa_booking_event.event_date', '<=', $tarikh_to)
            ->orderBy('event_date', 'DESC')
            ;

        // dd($data->get(), $tarikh_from, $tarikh_to);

        if($statussearch==''){
            $data->whereIn('main_booking.fk_lkp_status',array(5,18,19,20,21,22,23,24,25,26,27,28,29,30));
        }else{
            $data->whereIn('main_booking.fk_lkp_status',array($statussearch));
        }
        if(!$locationsearch==''){
            $data->where('spa_location.id',$locationsearch);
                // ->groupBy('main_booking.id');
        }else{
            // $data->where('spa_location.id','<>',0)
            //     ->groupBy('main_booking.id');
        }
        $data = $data->get();
        // dd($data, $tarikh_from, $tarikh_to, $statussearch, $locationsearch);

        return $data;
    }
    // Laporan::Tamat

    public function duplicate($id, $data, $data2){
        // dd($id, $data, $data2);
        $query = DB::select(DB::raw('
            SELECT COUNT(*) AS total,
               a1.fk_spa_location,
               a1.event_date,
               a1.event_date_end
            FROM spa_booking_event AS a1
            WHERE a1.event_date IS NOT NULL
            AND a1.event_date >= :data
            AND a1.event_date_end <= :data2
            AND a1.fk_spa_location = :id
            GROUP BY a1.fk_spa_location, a1.event_date, a1.event_date_end
            HAVING COUNT(*) > 1
        '), ['id' => $id, 'data' => $data, 'data2' => $data2]);

        return $query;
    }

    public function duplicateData($locationId, $dateStart, $dateEnd){
        $query = DB::select(DB::raw('
            SELECT *
            FROM spa_booking_event
            JOIN main_booking ON main_booking.id = fk_main_booking
            WHERE event_date >= :data
            AND event_date_end <= :data2
            AND fk_spa_location = :id
        '), ['id' => $locationId, 'data' => $dateStart, 'data2' => $dateEnd]);

        return $query;
    }


    public function duplicate_all2() {
        $query = DB::select(DB::raw('
            SELECT COUNT(*) AS total,
                a1.fk_spa_location,
                a1.event_date,
                a1.event_date_end
            FROM spa_booking_event AS a1
            JOIN main_booking ON main_booking.id = a1.fk_main_booking
            WHERE a1.event_date IS NOT NULL
            AND main_booking.fk_lkp_status NOT IN (18, 21)
            AND EXISTS (
                SELECT 1
                FROM spa_booking_event AS a2
                WHERE a2.fk_spa_location = a1.fk_spa_location
                    AND a2.event_date IS NOT NULL
                    AND a2.event_date_end IS NOT NULL
                    AND a1.event_date <= a2.event_date_end
                    AND a2.event_date <= a1.event_date_end
                    AND a1.id != a2.id
            )
            GROUP BY a1.fk_spa_location, a1.event_date, a1.event_date_end
            HAVING COUNT(*) > 1
            ORDER BY main_booking.id DESC;
        '));
        // $query = DB::select(DB::raw('
        //     SELECT COUNT(*) AS total,
        //         a1.fk_spa_location,
        //         a1.event_date,
        //         a1.event_date_end
        //     FROM spa_booking_event AS a1
        //     WHERE a1.event_date IS NOT NULL
        //     AND EXISTS (
        //         SELECT 1
        //         FROM spa_booking_event AS a2
        //         WHERE a2.fk_spa_location = a1.fk_spa_location
        //             AND a2.event_date IS NOT NULL
        //             AND a2.event_date_end IS NOT NULL
        //             AND a1.event_date <= a2.event_date_end
        //             AND a2.event_date <= a1.event_date_end
        //             AND a1.id != a2.id
        //     )
        //     GROUP BY a1.fk_spa_location, a1.event_date, a1.event_date_end
        //     HAVING COUNT(*) > 1;
        // '));
        return $query;
    }

    public function duplicate_all(){
        $query = DB::select(DB::raw('
            SELECT d1.* ,COUNT(est_slot_time) AS total, est_slot_time, c1.fk_et_facility_type, b1.esb_booking_date, efd_name, d1.fk_lkp_location
            FROM et_sport_time AS a1
            JOIN et_sport_book AS b1 ON b1.id = a1.fk_et_sport_book
            JOIN et_booking_facility AS c1 ON c1.id = b1.fk_et_booking_facility
            JOIN main_booking AS d1 ON d1.id = c1.fk_main_booking
            JOIN et_slot_price AS e1 ON e1.id = a1.fk_et_slot_price
            JOIN et_slot_time AS f1 ON f1.id = e1.fk_et_slot_time
            JOIN et_facility_detail AS g1 ON g1.id = c1.fk_et_facility_detail
            WHERE d1.fk_lkp_status IN (5,11)
            GROUP BY est_slot_time
            HAVING COUNT(est_slot_time) > 1;
        '));

        return $query;
    }

    public function spa_quotation_detail_all($mainbooking) {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->Join('lkp_spa','lkp_spa.id','=','spa_quotation_detail.fk_lkp_spa_payitem')
              ->select('spa_quotation_detail.*','lkp_spa.description')
              ->where('main_booking.id',$mainbooking)
              ->orderBy('lkp_spa.seq_by','asc')//created at

              // ->where('spa_quotation_detail.rate','>',0)
              ->get();

          return $data;
    }
    public function sumtotal($mainbooking)
    {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->selectRaw('sum(spa_quotation_detail.total) as total,sum(spa_quotation_detail.gst_rm) as totalgst')
              ->where('main_booking.id',$mainbooking)
              ->where('spa_quotation_detail.exceptional','<>',1)//exclude pengecualian
              //->where('spa_quotation_detail.fk_lkp_spa_payitem','<>',9)//21/11/2017
              ->first();

          return $data;
    }
    public function sumtotal_all($mainbooking)
    {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->selectRaw('sum(spa_quotation_detail.total) as total,sum(spa_quotation_detail.gst_rm) as totalgst')
              ->where('main_booking.id',$mainbooking)
              //->where('spa_quotation_detail.exceptional','<>',1)//exclude pengecualian
              ->first();

          return $data;
    }
    public function spa_quotation_detail_bpsk($mainbooking)
    {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->Join('lkp_spa','lkp_spa.id','=','spa_quotation_detail.fk_lkp_spa_payitem')
              ->select('spa_quotation_detail.*','lkp_spa.description')
              ->where('main_booking.id',$mainbooking)
              ->where('spa_quotation_detail.fk_lkp_spa_payitem','<>',9)
              ->where('spa_quotation_detail.rate','>',0)
              ->get();

          return $data;
    }
    public function spa_deposit_kecuali($mainbooking)
    {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->select('spa_quotation_detail.*')
              ->where('main_booking.id',$mainbooking)
              ->where('spa_quotation_detail.fk_lkp_spa_payitem','=',9)
              ->first();

          return $data;
    }
    public function spa_quotation_detail_all_count($mainbooking)
    {
         $data= DB::table('main_booking')
              ->Join('spa_quotation_detail','spa_quotation_detail.fk_main_booking','=','main_booking.id')
              ->Join('lkp_spa','lkp_spa.id','=','spa_quotation_detail.fk_lkp_spa_payitem')
              ->select('spa_quotation_detail.*','lkp_spa.description')
              ->where('main_booking.id',$mainbooking)
              ->where('spa_quotation_detail.rate','>',0)
              ->count();

          return $data;
    }
    public function location($id)
    {
        $data = all::GetRow('lkp_location', 'id', $id);
        return $data->lc_description;
    }
    public function location2(){
        $locations = DB::table('spa_location')->orderBy('created_at', 'asc')->orderBy('name', 'asc')->pluck('name', 'id');
        return $locations;
    }
    public function validateData($table, $colName, $meetingNo) {
        return DB::table($table)
            ->where($colName, $meetingNo)
            ->first();
    }
    
  public function getlistDalaman() {
    $users = DB::table('user_role')->whereIn('role_id',[1,16])->select('user_id')->get();
    
    $role = array();

    foreach ($users as $key => $value) {
      array_push($role, $value->user_id);
    }

    $data = DB::table('main_booking')
    ->join('spa_booking_event','main_booking.id','=','spa_booking_event.fk_main_booking')
    ->join('spa_booking_person','main_booking.id','=','spa_booking_person.fk_main_booking')
    ->join('spa_location','spa_booking_event.fk_spa_location','=','spa_location.id')
    ->join('lkp_status','lkp_status.id','=','main_booking.fk_lkp_status')
    ->whereIn('main_booking.fk_users',$role)
    // ->where('main_booking.bmb_type_user',2)
    //->where('main_booking.deleted_at','=',NULL)
    ->select('main_booking.bmb_booking_no','main_booking.created_at','main_booking.bmb_booking_date','spa_booking_person.name', 'main_booking.fk_users', 'spa_booking_person.contact_person', 'spa_booking_event.event_name','spa_booking_event.event_date','spa_location.name as locationame','spa_booking_event.total_day','spa_booking_event.fk_spa_location','spa_booking_event.other_location','spa_booking_event.enter_date','spa_booking_event.exit_date','main_booking.fk_lkp_status','main_booking.id','lkp_status.ls_description','main_booking.bmb_type_user','spa_booking_person.contact_person','spa_booking_person.name','spa_booking_event.event_date_end') 
    ->orderBy('spa_booking_event.event_date', 'desc')
    ->get();
    
    return $data;
  }
  
  public function generatetransid() {
    $data = All::GetRow('lkp_sequence', 'id', 1);
    $no = $data->fpx_serial_trans;
    if (($no < 9999)) {
        $run_no = $no + 1;
        /*-----------update lkpsequence-----------*/
        $b = array(
            'fpx_serial_trans' => $run_no,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        All::Insert('lkp_sequence', $b);
        /*-----------end update lkpsequence-----------*/
    } else {
        $run_no = 1;
        /*-----------update lkpsequence-----------*/
        $b = array(
            'fpx_serial_trans' => $run_no,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        All::Insert('lkp_sequence', $b);
        /*-----------end update lkpsequence-----------*/
    }
    while (strlen($run_no) <= 3) {
        $run_no = '0' . $run_no;
    }
    return $run_no;
    }

    public function getBookedAcaralist($startdate, $enddate, $loc) {
        $startdate = date("Y-m-d", strtotime($startdate));
        $enddate = date("Y-m-d", strtotime($enddate));
    
        $query = DB::table('spa_booking_confirm')
            ->select(
                    'spa_booking_confirm.*',
                    'users.fullname',
                    'spa_location.name',
                    'main_booking.bmb_booking_no',
                    'spa_booking_event.total_day',
                    'spa_booking_event.event_date',
                    'spa_booking_event.event_date_end'
                    )
            ->join('main_booking', 'main_booking.id', '=', 'spa_booking_confirm.fk_main_booking')
            ->join('users', 'users.id', '=', 'main_booking.fk_users')
            ->join('spa_location', 'spa_location.id', '=', 'spa_booking_confirm.fk_spa_location')
            ->join('spa_booking_event', 'spa_booking_event.fk_main_booking', '=', 'main_booking.id')
            ->where('spa_booking_event.event_date', '<=', $enddate)
            ->where('spa_booking_event.event_date_end', '>=', $startdate)
            ->where('spa_booking_confirm.fk_spa_location', $loc)
            ->groupBy('spa_booking_event.fk_spa_location')
            ->get();
        // dd($query);
        return $query;
    }
    
}

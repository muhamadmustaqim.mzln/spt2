<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthenticateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Check if the user is authenticated
        if (!Session::get('user')) {
            // If not authenticated, you can redirect to login page or return unauthorized response
            return redirect('/auth'); // Example redirect to login page
        }

        // If authenticated, proceed with the request
        return $next($request);
    }
    // public function handle(Request $request, Closure $next)
    // {
    //     return $next($request);
    // }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        'fpx/*', 'sport/ajax', 'sport/ajaxpublic', 'event/ajax', 'upload/*', 'file/*', 'hall/submitrating', 'sport/ajax/*', 'event/carianSekatan'
    ];
}

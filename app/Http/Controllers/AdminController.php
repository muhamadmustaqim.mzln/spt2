<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Takwim;
use App\Models\All;
use App\Models\Sport;
use App\Models\Users;
use App\Models\AuditLog;

class AdminController extends Controller
{
    public function index() {
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
        return view('takwim.lists', compact('data'));
    }
    
    public function systemconfiguration(){
        $data['config'] = All::ShowAll('lkp_configuration');
        
        return view('admin.configuration.lists', compact('data'));
    }
    
    public function systemconfiguration_edit($id){
        $data['id'] = Crypt::decrypt($id);
        $data['config'] = All::GetRow('lkp_configuration', 'id', $data['id']);

        if(request()->isMethod('post')){
            $status = 0;
            if(request()->status == 'on'){
                $status = 1;
            }

            $update_config = [
                'status'    => $status
            ];
            $query = All::GetUpdate('lkp_configuration', $data['id'], $update_config);

            if($query){
                return redirect('/admin/systemconfiguration');
            } else {
                Session::flash('flash', 'Failed.'); 
                return Redirect::to(url('/admin/systemconfiguration'));
            }
        }

        return view('admin.configuration.edit', compact('data'));
    }

	// Pengurusan Pengguna
	public function userlist(Request $request) {
        // $data['pengguna'] = All::JoinTwoTable('user_profiles', 'users');
        // $data['pengguna'] = All::ShowLimit('users', 'id', 'ASC', 10000);
        // $data['pengguna'] = Users::select('id', 'email', 'fullname', 'isAdmin', 'status')->where('deleted_at', null)->paginate(10);
        $data['pengguna'] = Users::select('id', 'email', 'fullname', 'isAdmin', 'status')->where('deleted_at', null);
        if(request()->isMethod('post')){
            // $data['pengguna'] = Users::select('id', 'email', 'fullname', 'isAdmin', 'status')->where('deleted_at', null)->where('email', 'LIKE', '%'.request()->searchInput.'%')->get()->toArray();
            $searchInput = request()->id;
            $data['pengguna'] = $data['pengguna']->where('email', 'LIKE', '%' . $searchInput . '%');
        }
        if(request('id') != null) {
            $data['pengguna'] = $data['pengguna']->where('email', 'LIKE', '%' . request('id') . '%')->orwhere('fullname', 'LIKE', '%' . request('id') . '%');
            $data['search'] = ['id' => request('id')];
        } else {
            $data['search'] = null;
        }
        $data['pengguna'] = $data['pengguna']->paginate(10);
        // dd($data);
        return view('admin.user.lists', compact('data'));
    }

    public function usersearch(Request $request){
        $query = $request->input('query');
        if ($query != '') {
            $data['pengguna'] = Users::select('id', 'email', 'fullname', 'isAdmin', 'status')->where('deleted_at', null)->where('email', 'LIKE', '%'.$query.'%')->get()->toArray();
            foreach ($data['pengguna'] as $key => $user) {
                $data['pengguna'][$key]['id'] = Crypt::encrypt($user['id']);
            }
            // dd($s, $data['pengguna'], gettype($data['pengguna']), gettype($s));
        }
        return $data['pengguna'];
    }

    public function formUserlist(){
        return view('admin.user.form');
    }

    public function addUserlist(Request $request){
        $status = request()->status;
        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $data = array(
            'email'             => request()->email,
            'username'          => request()->nama,
            'fullname'          => request()->nama,
            'isAdmin'           => request()->isAdmin,
            'status'            => $status,
            'updated_at'        => date('Y-m-d H:i:s'),
            'created_at'        => date('Y-m-d H:i:s')
        );
        try {
            $query = All::Insert('users', $data);
        } catch (\Throwable $th) {
            Session::flash('flash', 'Failed. Email already exist.'); 
            return Redirect::to(url('/admin/user/list'));
        }

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(171), Crypt::encrypt($query), 'Tambah Pengguna : ' . request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/user/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/user/list'));
        }
    }

    public function editUserlist($id) {
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('users', 'id', $id);
        $data['role_id'] = All::GetAllRow('user_role', 'user_id', $id)->where('status', 1);
        if($data['role_id'] == null) {
            $newdata = array(
                'user_id'           => $id,
                'role_id'           => 10,
                'updated_at'        => date('Y-m-d H:i:s'),
                'created_at'        => date('Y-m-d H:i:s')
            );
            $query = All::Insert('user_role', $newdata);
            $data['role_id'] = All::GetRow('user_role', 'user_id', $id);
        }
        $data['roles'] = All::Show('roles');
        
        return view('admin.user.edit', compact('data'));
    }

    public function updateUserlist(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $role_id = request()->peranan;
        $isAdmin = 0;
        $isAdminData = json_decode($request->peranan, true);
        $roles = [];
        foreach ($isAdminData as $key => $value) {
            $roles[] = $value['id'];
        }
        foreach ($roles as $role) {
            if ($role !== 10) {
                $isAdmin = 1;
                break;
            }
        }

        $userCurrentRoles = All::GetAllRow('user_role', 'user_id', $id)->whereIn('status', [null, 1])->pluck('id','role_id');
        $rolesAssoc = array_fill_keys($roles, true);
        $filteredRoles = $userCurrentRoles->diffKeys($rolesAssoc);
        $filteredRolesArray = $filteredRoles->all();
        // dd($id, $userCurrentRoles, $roles, $rolesAssoc, $filteredRoles, $filteredRolesArray);
        
        // Output or further process the filtered array
        $data['role_id'] = All::GetAllRow('user_role', 'user_id', $id);

        // Deactivate all first
        $role_to_deactivate = [
            'status'        => 0,
            'updated_at'    => date('Y-m-d H:i:s')
        ];
        $query = All::GetUpdateSpec('user_role', 'user_id', $id, $role_to_deactivate);

        // Activate/add new role
        foreach ($roles as $key => $value) {
            $ifRoleExistForUser = All::GetAllRow('user_role', 'user_id', $id)->where('role_id', $value)->first();
            if(in_array($value, [5,6,7,8])){
                $ifSPAExistForUser = All::GetAllRow('user_role', 'user_id', $id)->where('role_id', 23)->first();
                if($ifSPAExistForUser){
                    $activate_role = [
                        'status'        => 1,
                        'updated_at'    => date('Y-m-d H:i:s')
                    ];
                    $query = All::GetUpdate('user_role', $ifSPAExistForUser->id, $activate_role);
                } else {
                    $dataArr = array(
                        'user_id'           => $id,
                        'role_id'           => 23,
                        'status'            => 1,
                        'created_at'        => now(),
                        'updated_at'        => now()
                    );
                    $query = All::Insert('user_role', $dataArr);
                }
            }
            if($ifRoleExistForUser){
                $activate_role = [
                    'status'        => 1,
                    'updated_at'    => date('Y-m-d H:i:s')
                ];
                $query = All::GetUpdate('user_role', $ifRoleExistForUser->id, $activate_role);
            } else {
                $dataArr = array(
                    'user_id'           => $id,
                    'role_id'           => $value,
                    'status'            => 1,
                    'created_at'        => now(),
                    'updated_at'        => now()
                );
                $query = All::Insert('user_role', $dataArr);
            }
        }

        $dataUser = array(
            'email'             => request()->email,
            'fullname'          => request()->nama,
            'isAdmin'           => $isAdmin,
            'status'            => $status,
            'updated_at'        => now()
        );
        $query = All::GetUpdate('users', $id, $dataUser);

        if ($query) {
            // $audit = AuditLog::log(Crypt::encrypt(170), Crypt::encrypt($id), 'Kemas kini Pengguna : ' . request()->nama . ' => ' . $role_name ,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/user/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/user/list'));
        }
    }

    public function deleteUserlist($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('users', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(171), Crypt::encrypt($id), 'Padam Pengguna : ' . $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/user/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/user/list'));
        }
    }

    // Pengurusan Peranan
	public function rolelist() {
        $data['roles'] = All::Show('roles', 'id', 'ASC');
        
        return view('admin.role.lists', compact('data'));
    }
	
	public function roleform(){
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_status', 1);
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray)->sortBy('lc_description');
        // dd($data);
        return view('admin.role.form', compact('data'));
    }

    public function roleadd(Request $request){
        
        $data = array(
            'name'    => request()->name,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
		
        $query = All::Insert('roles', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/role/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/role/list'));
        }
    }
	
    public function editRole($id) {
        $id = Crypt::decrypt($id);
        $data['role'] = All::GetRow('roles', 'id', $id);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_status', 1);
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        return view('admin.role.edit', compact('data'));
    }
	
	public function updateRole(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'name'              => request()->name,
            'updated_at'        => date('Y-m-d H:i:s')
        );
		
        $query = All::GetUpdate('roles', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/role/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/role/list'));
        }
    }

    public function deleteRole($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('roles', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/role/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/role/list'));
        }
    }

    // Pengurusan Tempahan
    public function tempahanList() {
        $data['roles'] = All::Show('roles', 'name', 'ASC');
        return view('admin.tempahan_list', compact('data'));
    }

    public function tempahanForm(){
        return view('admin.roleform');
    }

    public function tempahanAdd(Request $request){
        
        $data = array(
            'name'    => request()->name,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
		
        $query = All::Insert('roles', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }
    }
	
    public function tempahanEdit($id) {
        $id = Crypt::decrypt($id);
        $data['role'] = All::GetRow('roles', 'id', $id);
        return view('admin.roleedit', compact('data'));
    }
	
	public function updateTempahan(Request $request, $id){
        
        $data = array(
            'name'              => request()->name,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
		
        $query = All::Insert('roles', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }
    }

    public function deleteTempahan($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('roles', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/tempahan_list'));
        }
    }


    // Pengurusan Maklumat Sistem
    public function listMaklumat() {
        // $data['roles'] = All::Show('roles', 'name', 'ASC');
        $data['bh_question'] = All::Show('bh_question', 'id', 'ASC');
        return view('admin.question.list_maklumat', compact('data'));
    }

    public function formMaklumat(){
        $data['lkp_question_cat'] = All::ShowAll('lkp_question_cat', 'id', 'ASC');
        return view('admin.question.form', compact('data'));
    }

    public function addMaklumat(Request $request){
        
        $data = array(
            'bq_question'           => request()->bq_question,
            'fk_lkp_question_cat'   => request()->fk_lkp_question_cat,
            'created_at'            => date('Y-m-d H:i:s')
        );
		
        $query = All::Insert('bh_question', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }
    }
	
    public function editMaklumat($id) {
        $id = Crypt::decrypt($id);
        // $data['role'] = All::GetRow('roles', 'id', $id);
        $data['bh_question'] = All::Show('bh_question', 'id', 'ASC');
        $data['question'] = All::GetRow('bh_question', 'id', $id);
        $data['lkp_question_cat'] = All::ShowAll('lkp_question_cat', 'id', 'ASC');
        return view('admin.question.edit', compact('data'));
    }
	
	public function updateMaklumat(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'bq_question'           => request()->bq_question,
            'fk_lkp_question_cat'   => request()->fk_lkp_question_cat,
            'updated_at'            => date('Y-m-d H:i:s')
        );
		
        $query = All::GetUpdate('bh_question', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }
    }

    public function deleteMaklumat($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_question', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/list_maklumat'));
        }
    }

    // Pengurusan Banner
    public function bannerList() {
        $data['banner'] = All::Show('bb_banner', 'updated_at', 'DESC');
        // dd($data);
        return view('admin.banner.list', compact('data'));
    }

    public function bannerform(){
        return view('admin.banner.form');
    }

    public function addbanner(Request $request){
        $uuid = $request->post('uuidKey');
        $status = $request->input('status') == 'on' ? 1 : 0;
        $data = array(
            'bb_name'           => $request->post('nama'),
            'bb_text'           => $request->post('text'),
            'bb_status'         => $status,
            'bb_start_date'     => $request->post('tarikhmula'),
            'bb_end_date'       => $request->post('tarikhtamat'),
            'bb_expired_date'   => $request->post('tarikhluput'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        
        $query = All::Insert('bb_banner', $data);
        if ($query) {
            Session::flash('flash', 'Success'); 
        }else{
            Session::flash('flash', 'Failed'); 
        }
        return Redirect::to(url('/admin/banner/list'));
    }
	
    public function editbanner($id) {
        $id = Crypt::decrypt($id);
        $data['bannerlist'] = All::GetRow('bb_banner', 'id', $id);
        return view('admin.banner.edit', compact('data'));
    }
	
	public function updatebanner(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = $request->input('status') == 'on' ? 1 : 0;
        $data = array(
            'bb_name'           => $request->post('nama'),
            'bb_text'           => $request->post('text'),
            'bb_status'         => $status,
            'bb_start_date'     => $request->post('tarikhmula'),
            'bb_end_date'       => $request->post('tarikhtamat'),
            'bb_expired_date'   => $request->post('tarikhluput'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
		// dd($data);
        $query = All::GetUpdate('bb_banner', $id, $data);
        
        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/banner/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/banner/list'));
        }
    }

    public function deletebanner($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bb_banner', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/banner/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/banner/list'));
        }
    }


    public function imageform($id){
        $id = Crypt::decrypt($id);
        $data['id'] = $id;
        $data['uuid'] = Str::uuid()->toString();
        $data['image'] = All::Show('bb_banner', 'id', 'ASC');
        return view('admin.banner.imageform', compact('data'));
    }

    public function imageadd(Request $request, $id){
        $id = Crypt::decrypt($id);

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/banner/'.$id);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
            return redirect()->back()->with('error', 'Request has no file');
        }

        $path = str_replace(public_path(), '', $path);

        $data = array(
            // 'eft_uuid'              => $id,
            'bb_filename'           => $filename,
            'bb_location'           => $path,
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user')['id'],
        );
        $query = All::GetUpdate('bb_banner', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/banner/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/banner/list'));
        }
    }

    public function imageEdit($id){
        $id = Crypt::decrypt($id);
        $data['id'] = $id;
        $data['bannerlist'] = All::GetRow('bb_banner', 'id', $id);
        return view('admin.banner.image', compact('data'));
    }

    public function imageupdate(Request $request, $id){
        $id = Crypt::decrypt($id);
        $uuid = $request->post('uuidKey');

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/banner/'.$id);
            if (File::isDirectory($path)) {
                $files = File::files($path);
                foreach ($files as $file) {
                    File::delete($file);
                }
            }
        
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);

            $data = array(
                'bb_filename'       => $filename,
                'bb_location'       => 'assets/upload/banner/'.$id,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            $query = All::GetUpdate('bb_banner', $id, $data);
            if ($query) {
                Session::flash('flash', 'Success'); 
                return Redirect::to(url('/admin/banner/list'));
            }else{
                Session::flash('flash', 'Failed'); 
                return Redirect::to(url('/admin/banner/list'));
            }
        }
    }
    
    public function imagedelete($id){
        $id = Crypt::decrypt($id);
        // $file = Crypt::decrypt($file);

        $image = All::GetRow('bb_banner', 'id', $id);

        $path = public_path('assets/upload/banner/'.$id.'/'.$image->bb_filename);
        if (File::exists($path)) {
            File::delete($path);
        }

        $data = array(
            'bb_filename'       => null,
            'bb_location'       => null,
            'updated_by'        => Session::get('user')['id'],
        );
        $query = All::GetUpdate('bb_banner', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/banner/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/banner/list',Crypt::encrypt($id)));
        }
    }

    // Pengurusan Announcement
    public function announcementlist() {
        $umum = All::Show('bh_announcement', 'ba_name', 'ASC');
        $data['umum'] = $umum->sortByDesc('created_at');
        return view('admin.announcement.lists', compact('data'));
    }

    public function announcementform(){
        return view('admin.announcement.form');
    }

    public function addAnn(Request $request){        
        $status = $request->input('status') == 'on' ? 1 : 0;

        $data = array(
            'ba_name'            => request()->umum,
            'ba_detail'          => request()->butiran,
            'ba_status'          => $status,
            'ba_type'            => request()->kategori,
            'ba_start_date'      => request()->tarikhmula,
            'ba_end_date'        => request()->tarikhtamat,
            'ba_expired_date'    => request()->tarikhluput,
            'updated_at'         => date('Y-m-d H:i:s'),
            'created_at'         => date('Y-m-d H:i:s'),
            'updated_by'         => Session::get('user')['id'],
        );
        $query = All::InsertGetID('bh_announcement', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(24), Crypt::encrypt($query), 'Id Pengumuman = ' . $query,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/announcement/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/announcement/list'));
        }
    }
    public function editAnn($id) {
        $id = Crypt::decrypt($id);
        $data['announcement'] = All::GetRow('bh_announcement', 'id', $id);
        return view('admin.announcement.edit', compact('data'));
    }

    public function editAnnButiran($id) {
        $id = Crypt::decrypt($id);
        $data['announcement'] = All::GetRow('bh_announcement', 'id', $id);
        return view('admin.announcement.editbutiran', compact('data'));
    }

    public function updateAnn(Request $request, $id){
        $id = Crypt::decrypt($id);
        // $status = request()->status;

        // if($status == "on"){
        //     $status = 1;
        // }else{
        //     $status = 0;
        // }
        $status = $request->status == "on" ? 1 : 0;
        $data = array(
            'ba_name'            => request()->umum,
            'ba_status'          => $status,
            'ba_start_date'      => request()->tarikhmula,
            'ba_end_date'        => request()->tarikhtamat,
            'ba_expired_date'    => request()->tarikhluput,
            'updated_at'         => date('Y-m-d H:i:s'),
            'updated_by'         => Session::get('user')['id'],
        );

        $query = All::GetUpdate('bh_announcement', $id, $data);
        // dd($id, $data, $query);
        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Pengumuman id = ' . $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/announcement/list/'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/announcement/list/'));
        }
    }

    public function updateAnnButiran(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $data = array(
            'ba_name'            => request()->umum,
            'ba_detail'          => request()->butiran,
            'ba_status'          => $status,
            'updated_at'         => date('Y-m-d H:i:s'),
            'updated_by'         => Session::get('user')['id'],
        );

        $query = All::GetUpdate('bh_announcement', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/announcement/list/'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/announcement/list/'));
        }
    }

    public function deleteAnn($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s'),
        );

        $query = All::GetUpdate('bh_announcement', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Padam Pengumuman = ' . $query,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/announcement/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/announcement/list'));
        }
    }


    //Pengurusan Pakej

    public function packageList() {
        // $data['pengguna'] = All::JoinTwoTable('user_profiles', 'users');
        $data['pakej'] = All::Show('bh_package', 'id', 'ASC');
        $data['detail'] = All::JoinTwoTable('bh_package_detail', 'bh_package');
        // dd($data);
        return view('admin.package.list', compact('data'));
    }

    public function packageForm(){
        return view('admin.package.form');
    }

    public function addpackage(Request $request){
        $status = $request->status == "on" ? 1 : 0;
        $data1 = array(
            'bp_package_name'      => request()->pakejPerkahwinan,
            'bp_package_price'     => request()->pricepackage,
            'bp_gst_rm'            => request()->pricegst,
            'bp_status'            => $status,
            'updated_at'           => date('Y-m-d H:i:s'),
            'updated_by'           => Session::get('user')['id'],
            'created_at'           => date('Y-m-d H:i:s')
        );
        $query1 = All::Insert('bh_package', $data1);
        
        $data2 = array(
            'fk_bh_package'        => request()->keterangan,
            'bpd_description'      => request()->keterangan,
            'updated_at'           => date('Y-m-d H:i:s'),
            'updated_by'           => Session::get('user')['id'],
            'created_at'           => date('Y-m-d H:i:s')
        );
        $query2 = All::Insert('bh_package_detail', $data2);

        // dd($data1, $data2);
        if ($query1 && $query2) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/package/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/package/list'));
        }


    }

    public function editpackage($id) {
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('bh_package', 'id', $id);
        $data['desc'] = All::GetRow('bh_package_detail', 'fk_bh_package', $data['list']->id);
        // dd($id,$data);
        return view('admin.package.edit', compact('data'));
    }

    public function updatepackage(Request $request, $id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('bh_package', 'id', $id);
        $idDesc = All::GetRow('bh_package_detail', 'fk_bh_package', $data['list']->id);
        $idbpd = $idDesc->fk_bh_package;

        $status = $request->status == "on" ? 1 : 0;

        $data = array(
            'bp_package_name'      => request()->pakejPerkahwinan,
            'bp_package_price'     => request()->pricepackage,
            'bp_gst_rm'            => request()->pricegst,
            'bp_status'            => $status,
            'updated_at'           => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_package', $id, $data);
        // dd($id,$data, $idDesc, $idbpd, $query);

        $data1 = array(
            'bpd_description'     => request()->keterangan,
            'updated_at'          => date('Y-m-d H:i:s')
        );
        $query1 = All::GetUpdate('bh_package_detail', $idbpd, $data1);
        // dd($id, $data, $data1, $query, $query1);
        if ($query && $query1) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/package/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/package/list'));
        }
    }

    public function deletepackage($id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('bh_package', 'id', $id);
        $packageId = $data['list']->id;
        $idDesc = All::GetRow('bh_package_detail', 'id', $packageId);
        $idbpd = $idDesc->fk_bh_package;
        // dd($idDesc, $packageId ,$idbpd);
        $data = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_package', $id, $data);

        $data1 = array(
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query1 = All::GetUpdate('bh_package_detail', $idbpd, $data1);

        if ($query && $query1) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/package/list'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/package/list'));
        }
    }
}

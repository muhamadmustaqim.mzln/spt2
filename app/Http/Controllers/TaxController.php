<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\All;
use App\Models\AuditLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Validation\ValidatesRequests;

class TaxController extends Controller
{
    public function index(){
        $data['tax'] = All::Show('lkp_gst_rate', 'lgr_rate', 'DESC');
        // $data['tax'] = $tax->sortByDesc('lgr_rate');
        // $data['tax'] = All::Show('lkp_tax', 'lt_name', 'ASC');
        // dd($data);
        return view('admin.tax.lists', compact('data'));
    }

    public function taxForm(){
        return view('admin.tax.form');
    }

    public function addTax(Request $request){
        $status = $request->input('status') == 'on' ? 1 : 0;

        // $data = array(
        //     'lt_name'            => request()->cukai,
        //     'lt_rate'            => request()->ratecukai,
        //     'lt_code'            => request()->kodcukai,
        //     'lt_status'          => $status,
        //     'updated_at'         => date('Y-m-d H:i:s'),
        //     'created_at'         => date('Y-m-d H:i:s'),
        //     'updated_by'         => Session::get('user')['id'],
        // );
        // $query = All::Insert('lkp_tax', $data);

        $data = array(
            'lgr_description'            => request()->cukai,
            'lgr_rate'            => request()->ratecukai,
            'lgr_gst_code'            => request()->kodcukai,
            'lgr_status'          => $status,
            'updated_at'         => date('Y-m-d H:i:s'),
            'created_at'         => date('Y-m-d H:i:s'),
            'updated_by'         => Session::get('user')['id'],
        );
        $query = All::Insert('lkp_gst_rate', $data);

        if ($query) {
            // $audit = AuditLog::log(Crypt::encrypt(24), Crypt::encrypt($query), 'Id Pengumuman = ' . $query,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }
    }

    public function editTax($id) {
        $id = Crypt::decrypt($id);
        // $data['tax'] = All::GetRow('lkp_tax', 'id', $id);
        $data['tax'] = All::GetRow('lkp_gst_rate', 'id', $id);
        return view('admin.tax.edit', compact('data'));
    }

    public function updateTax(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = $request->status == "on" ? 1 : 0;
        // $data = array(
        //     'lt_name'            => request()->cukai,
        //     'lt_rate'            => request()->ratecukai,
        //     'lt_code'            => request()->kodcukai,
        //     'lt_status'          => $status,
        //     'updated_at'         => date('Y-m-d H:i:s'),
        //     'updated_by'         => Session::get('user')['id'],
        // );
        // $query = All::GetUpdate('lkp_tax', $id, $data);

        $data = array(
            'lgr_description'     => request()->cukai,
            'lgr_rate'            => request()->ratecukai,
            'lgr_gst_code'        => request()->kodcukai,
            'lgr_status'          => $status,
            'updated_at'          => date('Y-m-d H:i:s'),
            'updated_by'          => Session::get('user')['id'],
        );
        $query = All::GetUpdate('lkp_gst_rate', $id,$data);

        // dd($data,$query);
        if ($query) {
            // $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Pengumuman id = ' . $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }
    }

    public function deleteTax($id){
        $id = Crypt::decrypt($id);
        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s'),
        );
        // $query = All::GetUpdate('lkp_tax', $id, $data);
        $query = All::GetUpdate('lkp_gst_rate', $id, $data);

        if ($query) {
            // $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Padam Pengumuman = ' . $query,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/admin/taxmanagement'));
        }
    }
    // public function updateGstSetting(Request $request)
    // {
    //     // Validate input
    //     $request->validate([
    //         'gst_enabled' => 'required|boolean',
    //     ]);

    //     // Update the setting
    //     Settings::setGstEnabled($request->input('gst_enabled'));

    // }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use App\Models\All;
use App\Models\AuditLog;

class BapiController extends Controller
{
    // begin::General BAPI
    public function check_bp($ic){
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
        $soapUser = "tempahansys";  //  username
        $soapPassword = "ppjtempahan1234"; // password
        // xml post structure
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_GETBP>
                        <!--Optional:-->
                        <ICNO>'.$ic.'</ICNO>
                    </urn:Z_TEMPAHAN_GETBP>
                </soapenv:Body>
            </soapenv:Envelope>
        ';

        $headers =  array(
            "Accept: text/xml",
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: = ", 
            "Content-length: ".strlen($xml_post_string),
            "cache-control: no-cache",
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $xml = simplexml_load_string($response);
        $businessPartner = (count($xml->xpath('//BUSINESSPARTNER')) != 0) ? (string) $xml->xpath('//BUSINESSPARTNER')[0] : null;
        $contractAccount = (count($xml->xpath('//CONTRACT')) != 0) ? (string) $xml->xpath('//CONTRACT')[0] : null;
        if($businessPartner == '') {
            $businessPartner = null;
        }
        return [$businessPartner, $contractAccount, $response, 'checkbp'];
    }
    public function create_bp($user){
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
        $soapUser = "tempahansys";  //  username
        $soapPassword = "ppjtempahan1234"; // password
        // <ICUST_ID>950411145935</ICUST_ID>
        // <ICOUNTRY>'.$user->fk_lkp_country.'</ICOUNTRY>
        // <IADDR_LN1>'.$user->bud_address.'</IADDR_LN1>
        // <ISTATE>'.$user->fk_lkp_state.'</ISTATE>
        // xml post structure
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:Z_TEMPAHAN_CREATEBP>
                    <IADDR_LN1>Selangor</IADDR_LN1>
                    <!--Optional:-->
                    <IADDR_LN2></IADDR_LN2>
                    <!--Optional:-->
                    <IADDR_LN3></IADDR_LN3>
                    <!--Optional:-->
                    <IADDR_LN4></IADDR_LN4>
                    <!--Optional:-->
                    <IAPPLN_TYPE></IAPPLN_TYPE>
                    <ICOUNTRY>MY</ICOUNTRY>
                    <!--Optional:-->
                    <ICUST_ID>'.(string)$user->bud_reference_id.'</ICUST_ID>
                    <ICUST_NAME>'.$user->bud_name.'</ICUST_NAME>
                    <ICUST_TP>B</ICUST_TP>
                    <IEMAIL>'.$user->bud_email.'</IEMAIL>
                    <!--Optional:-->
                    <IPHONE>'.$user->bud_phone_no.'</IPHONE>
                    <IPOSTCODE>'.$user->bud_poscode.'</IPOSTCODE>
                    <ISTATE>16</ISTATE>
                </urn:Z_TEMPAHAN_CREATEBP>
                </soapenv:Body>
            </soapenv:Envelope>
        ';

        $headers =  array(
            "Accept: text/xml",
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: = ", 
            "Content-length: ".strlen($xml_post_string),
            // "Content-length: "."0",
            "cache-control: no-cache",
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $xml = simplexml_load_string($response);
        $businessPartner = (count($xml->xpath('//E_BUSINESPARTNER')) != 0) ? (string) $xml->xpath('//E_BUSINESPARTNER')[0] : null;
        $contractAccount = (count($xml->xpath('//E_CONTRACT')) != 0) ? (string) $xml->xpath('//E_CONTRACT')[0] : null;
        if($businessPartner == '') {
            $businessPartner = null;
        }
        if($contractAccount == '') {
            $contractAccount = null;
        }
        return [$businessPartner, $contractAccount, $response, 'createbp'];
    }
    public function adhoc($sap_doc_no = null, $user = null, $data = null) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$data->bmb_booking_no.'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->bmb_booking_no.'</GL_TEXT>
                        <SUB_SYSCODE>AD</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                // Log::info($th);
                return false;
            }
        } catch (\Throwable $th) {
            // Log::info($th);
            return false;
        }
    }
    // end::General BAPI

    // begin::Sport - Sport
    public function adhocSport($sap_doc_no = null, $user = null, $data = null) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$data->bmb_booking_no.'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->exOrderNo.'</GL_TEXT>
                        <SUB_SYSCODE>AD</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // dd($response, $status);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                Log::info("sap_doc_no" . $sap_doc_no . " | Response: " . $response);
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                Log::info($xml);
                return false;
            }
        } catch (\Throwable $th) {
            Log::info($th);
            return false;
        }
    }
    // end::Sport - Sport

    // begin::Event
    public function adhocEvent($sap_doc_no, $data, $type, $amount, $feecode) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no[0].'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>'.$sap_doc_no[0].'</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->exOrderNo.'</GL_TEXT>
                        <SUB_SYSCODE>'.$type.'</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                // Log::info($th);
                return false;
            }
        } catch (\Throwable $th) {
            // Log::info($th);
            return false;
        }
    }
    public function generate_bill_event($dataBP, $maklumatacaralokasi, $type, $amount){
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
        $soapUser = "tempahansys";  //  username
        $soapPassword = "ppjtempahan1234"; // password

        // xml post structure
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:Z_TEMPAHAN_GENBILL>
                    <!--Optional:-->
                    <APPL_AREA>P</APPL_AREA>
                    <!--Optional:-->
                    <BILL_DUE_DATE></BILL_DUE_DATE>
                    <BUSINESSPARTNER1>'.$dataBP[0].'</BUSINESSPARTNER1>
                    <CONTRACTACCOUNT1>000000'.$dataBP[1].'</CONTRACTACCOUNT1>
                    <!--Optional:-->
                    <CREATED_BY></CREATED_BY>
                    <!--Optional:-->
                    <CURRENCY>RM</CURRENCY>
                    <DOC_DATE>2024-01-01</DOC_DATE>
                    <!--Optional:-->
                    <DOC_TYPE>ZB</DOC_TYPE>
                    <!--Optional:-->
                    <ENTRY_DATE></ENTRY_DATE>
                    <!--Optional:-->
                    <ENTRY_TIME></ENTRY_TIME>
                    <EVENT_DESC>'.$maklumatacaralokasi['namaDewan'].'</EVENT_DESC>
                    <FEE_TYPE>'.$maklumatacaralokasi['feecode_depo'].'</FEE_TYPE>
                    <ITEM>
                        <!--Zero or more repetitions:-->
                        <item>
                            <FEE_TYPE>'.$maklumatacaralokasi['feecode_depo'].'</FEE_TYPE>
                            <FEE_AMOUNT>'.$amount.'</FEE_AMOUNT>
                        </item>
                    </ITEM>
                    <!--Optional:-->
                    <MAIN_TRANS>'.$maklumatacaralokasi['fee_code'].'</MAIN_TRANS>
                    <POST_DATE></POST_DATE>
                    <!--Optional:-->
                    <REF_DOC_NO></REF_DOC_NO>
                    <!--Optional:-->
                    <RETURN>
                        <!--Zero or more repetitions:-->
                        <item>
                            <TYPE></TYPE>
                            <ID></ID>
                            <NUMBER></NUMBER>
                            <MESSAGE></MESSAGE>
                            <LOG_NO></LOG_NO>
                            <LOG_MSG_NO></LOG_MSG_NO>
                            <MESSAGE_V1></MESSAGE_V1>
                            <MESSAGE_V2></MESSAGE_V2>
                            <MESSAGE_V3></MESSAGE_V3>
                            <MESSAGE_V4></MESSAGE_V4>
                            <PARAMETER></PARAMETER>
                            <ROW></ROW>
                            <FIELD></FIELD>
                            <SYSTEM>?</SYSTEM>
                        </item>
                    </RETURN>
                    <SUBSYSTEM_ID>'.$type.'</SUBSYSTEM_ID>
                    <!--Optional:-->
                    <SUB_TRANS>'.$maklumatacaralokasi['fee_code_sub'].'</SUB_TRANS>
                    <!--Optional:-->
                    <TRANS_DATE>2024-01-01</TRANS_DATE>
                </urn:Z_TEMPAHAN_GENBILL>
                </soapenv:Body>
            </soapenv:Envelope>
        ';
        // <MAIN_TRANS>1004</MAIN_TRANS>
        // <SUB_TRANS>1001</SUB_TRANS>
        $headers =  array(
            "Accept: text/xml",
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: = ", 
            "Content-length: ".strlen($xml_post_string),
            // "Content-length: "."0",
            "cache-control: no-cache",
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $xml = simplexml_load_string($response);
        if (count($xml->xpath('//SAP_DOCNO')) != 0){
            if ((string) $xml->xpath('//SAP_DOCNO')[0] != '') {
                $sap_doc_no = (string) $xml->xpath('//SAP_DOCNO')[0];
            } else {
                $sap_doc_no = null;
            }
        }
        return [$sap_doc_no, $response, 'generatebill'];
    }
    public function adhocEventPS($sap_doc_no = null, $user = null, $data = null) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no.'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->bmb_booking_no.'</GL_TEXT>
                        <SUB_SYSCODE>PS</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                Log::info($status);
                return false;
            }
        } catch (\Throwable $th) {
            // Log::info($th);
            return false;
        }
    }
    // end::Event

    // begin::hall
    public function adhocHall($sap_doc_no = null, $user = null, $data = null) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no.'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->bmb_booking_no.'</GL_TEXT>
                        <SUB_SYSCODE>AD</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                // Log::info($th);
                return false;
            }
        } catch (\Throwable $th) {
            // Log::info($th);
            return false;
        }
    }
    public function generate_bill_hall($dataBP){
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
        $soapUser = "tempahansys";  //  username
        $soapPassword = "ppjtempahan1234"; // password

        // xml post structure
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:Z_TEMPAHAN_GENBILL>
                    <!--Optional:-->
                    <APPL_AREA>P</APPL_AREA>
                    <!--Optional:-->
                    <BILL_DUE_DATE></BILL_DUE_DATE>
                    <BUSINESSPARTNER1>'.$dataBP[0].'</BUSINESSPARTNER1>
                    <CONTRACTACCOUNT1>000000'.$dataBP[1].'</CONTRACTACCOUNT1>
                    <!--Optional:-->
                    <CREATED_BY></CREATED_BY>
                    <!--Optional:-->
                    <CURRENCY>RM</CURRENCY>
                    <DOC_DATE>2024-01-01</DOC_DATE>
                    <!--Optional:-->
                    <DOC_TYPE>ZB</DOC_TYPE>
                    <!--Optional:-->
                    <ENTRY_DATE></ENTRY_DATE>
                    <!--Optional:-->
                    <ENTRY_TIME></ENTRY_TIME>
                    <EVENT_DESC>DEWAN SRI MELATI</EVENT_DESC>
                    <FEE_TYPE>CAGAR16</FEE_TYPE>
                    <ITEM>
                        <!--Zero or more repetitions:-->
                        <item>
                            <FEE_TYPE>CAGAR16</FEE_TYPE>
                            <FEE_AMOUNT>1600.00</FEE_AMOUNT>
                        </item>
                    </ITEM>
                    <!--Optional:-->
                    <MAIN_TRANS>1004</MAIN_TRANS>
                    <POST_DATE></POST_DATE>
                    <!--Optional:-->
                    <REF_DOC_NO></REF_DOC_NO>
                    <!--Optional:-->
                    <RETURN>
                        <!--Zero or more repetitions:-->
                        <item>
                            <TYPE></TYPE>
                            <ID></ID>
                            <NUMBER></NUMBER>
                            <MESSAGE></MESSAGE>
                            <LOG_NO></LOG_NO>
                            <LOG_MSG_NO></LOG_MSG_NO>
                            <MESSAGE_V1></MESSAGE_V1>
                            <MESSAGE_V2></MESSAGE_V2>
                            <MESSAGE_V3></MESSAGE_V3>
                            <MESSAGE_V4></MESSAGE_V4>
                            <PARAMETER></PARAMETER>
                            <ROW></ROW>
                            <FIELD></FIELD>
                            <SYSTEM>?</SYSTEM>
                        </item>
                    </RETURN>
                    <SUBSYSTEM_ID>DE</SUBSYSTEM_ID>
                    <!--Optional:-->
                    <SUB_TRANS>1001</SUB_TRANS>
                    <!--Optional:-->
                    <TRANS_DATE>2024-01-01</TRANS_DATE>
                </urn:Z_TEMPAHAN_GENBILL>
                </soapenv:Body>
            </soapenv:Envelope>
        ';

        $headers =  array(
            "Accept: text/xml",
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: = ", 
            "Content-length: ".strlen($xml_post_string),
            // "Content-length: "."0",
            "cache-control: no-cache",
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $xml = simplexml_load_string($response);
        if (count($xml->xpath('//SAP_DOCNO')) != 0){
            if ((string) $xml->xpath('//SAP_DOCNO')[0] != '') {
                $sap_doc_no = (string) $xml->xpath('//SAP_DOCNO')[0];
            } else {
                $sap_doc_no = null;
            }
        }
        return [$sap_doc_no, $response, 'generatebill'];
    }
    public function adhocHallPS($sap_doc_no = null, $user = null, $data = null) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahansys"; 
        $soapPassword = "ppjtempahan1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>'.$data->approvalCode.'</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE></DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME></DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$data->fpxNum.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no.'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID></DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE></PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE></PTRANSDATE>
                        <!--Optional:-->
                        <GL_TEXT>'.$data->bmb_booking_no.'</GL_TEXT>
                        <SUB_SYSCODE>PS</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
                
        try {
            $xml = simplexml_load_string($response);
            $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
            if($status == 200){
                return [$sap_doc_no, $response];
                // return $response;
            }else{
                Log::info($status);
                return false;
            }
        } catch (\Throwable $th) {
            // Log::info($th);
            return false;
        }
    }
    // end::hall


    public function adhocdeposit($sap_doc_no, $user, $data) {
        // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
        // $soapUser = "rfc1";  //  username
        // $soapPassword = "init1234"; // password
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahandev";  //  username
        $soapPassword = "tempahandev1234"; // password
        
        // xml post structure
        // $amount = "4.00";
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>5225</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE>'.$user->bud_name.'</DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1>'.$user->bud_address.'</DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE>'.$user->bud_poscode.'</DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE>Selangor</DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME>'.$user->bud_name.'</DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$user->bud_reference_id.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode_depo.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no[0].'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID>'.$user->bud_reference_id.'</DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE>2024-01-20</PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE>2024-01-20</PTRANSDATE>
                        <!--Optional:-->
                        <SUB_SYSCODE>DE</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return [$response, 'adhocdepo'];
        }else{
            return false;
        }
    }
    public function sappayment($sap_doc_no, $user, $data, $type) {
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
        $soapUser = "tempahandev";  
        $soapPassword = "tempahandev1234"; 
        
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                    <urn:Z_TEMPAHAN_PAYFPX>
                        <!--Optional:-->
                        <AMOUNT>'.$data->amount.'</AMOUNT>
                        <!--Optional:-->
                        <APPROVALCODE>5225</APPROVALCODE>
                        <!--Optional:-->
                        <DCOLLECTION_REFERENCE>'.$user->bud_name.'</DCOLLECTION_REFERENCE>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS1>'.$user->bud_address.'</DCUSTOMER_ADDRESS1>
                        <!--Optional:-->
                        <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
                        <!--Optional:-->
                        <DCUSTOMER_POSTCODE>'.$user->bud_poscode.'</DCUSTOMER_POSTCODE>
                        <!--Optional:-->
                        <DCUSTOMER_STATE>Selangor</DCUSTOMER_STATE>
                        <!--Optional:-->
                        <DCUST_NAME>'.$user->bud_name.'</DCUST_NAME>
                        <!--Optional:-->
                        <DDOC_REFERENCE>'.$user->bud_reference_id.'</DDOC_REFERENCE>
                        <!--Optional:-->
                        <DFEECODE>'.$data->feecode_depo.'</DFEECODE>
                        <!--Optional:-->
                        <DOCDATE>2024-01-20</DOCDATE>
                        <!--Optional:-->
                        <DOCUMENTNO>'.$sap_doc_no[0].'</DOCUMENTNO>
                        <!--Optional:-->
                        <DSTATUOTARY_ID>'.$user->bud_reference_id.'</DSTATUOTARY_ID>
                        <!--Optional:-->
                        <PCARDNO>1234</PCARDNO>
                        <!--Optional:-->
                        <PPOSTDATE>2024-01-20</PPOSTDATE>
                        <!--Optional:-->
                        <PTRANSDATE>2024-01-20</PTRANSDATE>
                        <!--Optional:-->
                        <SUB_SYSCODE>'.$type.'</SUB_SYSCODE>
                    </urn:Z_TEMPAHAN_PAYFPX>
                </soapenv:Body>
            </soapenv:Envelope>
        ';   // data from the form, e.g. some ID number
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return [$response, 'adhocdepo'];
        }else{
            return false;
        }
    }
    public function tunggakan() {
        $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_tunggak/700/zws_tunggak/zws_tunggak"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password
        
        // xml post structure
        $xml_post_string = '
        <?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:soap:functions:mc-style">
            <soapenv:Header/>
            <soapenv:Body>
            <urn:ZfkkPscdtunggak>
                <!--Optional:-->
                <Icno>550414-03-5091</Icno>
            </urn:ZfkkPscdtunggak>
            </soapenv:Body>
        </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Accept: text/xml",
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                        "cache-control: no-cache",
                    ); //SOAPAction: your op URL
    
        $url = $soapUrl;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            $xml = simplexml_load_string($response);
            $data1 = $xml->children('soap-env', TRUE)->Body->children('n0', TRUE)->ZfkkPscdtunggakResponse->children('', TRUE)->Tunggakan;
            //return 
            $data['tunggak'] = json_decode(json_encode($data1), true);
            return view('bapi.lists', compact('data'));
        }else{
            return false;
        }
    }

    public function generate_bill($dataBP){
        $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
        $soapUser = "tempahansys";  //  username
        $soapPassword = "ppjtempahan1234"; // password

        // xml post structure
        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:Z_TEMPAHAN_GENBILL>
                    <!--Optional:-->
                    <APPL_AREA>P</APPL_AREA>
                    <!--Optional:-->
                    <BILL_DUE_DATE></BILL_DUE_DATE>
                    <BUSINESSPARTNER1>'.$dataBP[0].'</BUSINESSPARTNER1>
                    <CONTRACTACCOUNT1>000000'.$dataBP[1].'</CONTRACTACCOUNT1>
                    <!--Optional:-->
                    <CREATED_BY></CREATED_BY>
                    <!--Optional:-->
                    <CURRENCY>RM</CURRENCY>
                    <DOC_DATE>2024-01-01</DOC_DATE>
                    <!--Optional:-->
                    <DOC_TYPE>ZB</DOC_TYPE>
                    <!--Optional:-->
                    <ENTRY_DATE></ENTRY_DATE>
                    <!--Optional:-->
                    <ENTRY_TIME></ENTRY_TIME>
                    <EVENT_DESC>DEWAN SRI MELATI</EVENT_DESC>
                    <FEE_TYPE>CAGAR16</FEE_TYPE>
                    <ITEM>
                        <!--Zero or more repetitions:-->
                        <item>
                            <FEE_TYPE>CAGAR16</FEE_TYPE>
                            <FEE_AMOUNT>1600.00</FEE_AMOUNT>
                        </item>
                    </ITEM>
                    <!--Optional:-->
                    <MAIN_TRANS>1004</MAIN_TRANS>
                    <POST_DATE></POST_DATE>
                    <!--Optional:-->
                    <REF_DOC_NO></REF_DOC_NO>
                    <!--Optional:-->
                    <RETURN>
                        <!--Zero or more repetitions:-->
                        <item>
                            <TYPE></TYPE>
                            <ID></ID>
                            <NUMBER></NUMBER>
                            <MESSAGE></MESSAGE>
                            <LOG_NO></LOG_NO>
                            <LOG_MSG_NO></LOG_MSG_NO>
                            <MESSAGE_V1></MESSAGE_V1>
                            <MESSAGE_V2></MESSAGE_V2>
                            <MESSAGE_V3></MESSAGE_V3>
                            <MESSAGE_V4></MESSAGE_V4>
                            <PARAMETER></PARAMETER>
                            <ROW></ROW>
                            <FIELD></FIELD>
                            <SYSTEM>?</SYSTEM>
                        </item>
                    </RETURN>
                    <SUBSYSTEM_ID>DE</SUBSYSTEM_ID>
                    <!--Optional:-->
                    <SUB_TRANS>1001</SUB_TRANS>
                    <!--Optional:-->
                    <TRANS_DATE>2024-01-01</TRANS_DATE>
                </urn:Z_TEMPAHAN_GENBILL>
                </soapenv:Body>
            </soapenv:Envelope>
        ';

        $headers =  array(
            "Accept: text/xml",
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: = ", 
            "Content-length: ".strlen($xml_post_string),
            // "Content-length: "."0",
            "cache-control: no-cache",
        ); //SOAPAction: your op URL

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        $xml = simplexml_load_string($response);
        if (count($xml->xpath('//SAP_DOCNO')) != 0){
            if ((string) $xml->xpath('//SAP_DOCNO')[0] != '') {
                $sap_doc_no = (string) $xml->xpath('//SAP_DOCNO')[0];
            } else {
                $sap_doc_no = null;
            }
        }
        return [$sap_doc_no, $response, 'generatebill'];
    }

    // public function adhoc() {
    //     // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
    //     // $soapUser = "rfc1";  //  username
    //     // $soapPassword = "init1234"; // password
    //     $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
    //     $soapUser = "tempahansys";  //  username
    //     $soapPassword = "ppjtempahan1234"; // password
        
    //     // xml post structure
    //     $amount = "4.00";
    //     $xml_post_string = '
    //         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
    //             <soapenv:Header/>
    //             <soapenv:Body>
    //                 <urn:Z_TEMPAHAN_PAYFPX>
    //                     <!--Optional:-->
    //                     <AMOUNT>1000.00</AMOUNT>
    //                     <!--Optional:-->
    //                     <APPROVALCODE>5225</APPROVALCODE>
    //                     <!--Optional:-->
    //                     <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_ADDRESS1></DCUSTOMER_ADDRESS1>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_ADDRESS2></DCUSTOMER_ADDRESS2>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_POSTCODE></DCUSTOMER_POSTCODE>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_STATE></DCUSTOMER_STATE>
    //                     <!--Optional:-->
    //                     <DCUST_NAME></DCUST_NAME>
    //                     <!--Optional:-->
    //                     <DDOC_REFERENCE></DDOC_REFERENCE>
    //                     <!--Optional:-->
    //                     <DFEECODE></DFEECODE>
    //                     <!--Optional:-->
    //                     <DOCDATE>2024-01-20</DOCDATE>
    //                     <!--Optional:-->
    //                     <DOCUMENTNO>600000019033</DOCUMENTNO>
    //                     <!--Optional:-->
    //                     <DSTATUOTARY_ID></DSTATUOTARY_ID>
    //                     <!--Optional:-->
    //                     <PCARDNO>1234</PCARDNO>
    //                     <!--Optional:-->
    //                     <PPOSTDATE>2024-01-20</PPOSTDATE>
    //                     <!--Optional:-->
    //                     <PTRANSDATE>2024-01-20</PTRANSDATE>
    //                     <!--Optional:-->
    //                     <SUB_SYSCODE>PS</SUB_SYSCODE>
    //                 </urn:Z_TEMPAHAN_PAYFPX>
    //             </soapenv:Body>
    //         </soapenv:Envelope>
    //     ';   // data from the form, e.g. some ID number
    
    //     $headers =  array(
    //                     "Content-type: text/xml;charset=\"utf-8\"",
    //                     "SOAPAction: = ", 
    //                     "Content-length: ".strlen($xml_post_string),
    //                 ); //SOAPAction: your op URL
    
    //     $url = $soapUrl;
    
    //     // PHP cURL  for https connection with auth
    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
    //     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    //     // converting
    //     $response = curl_exec($ch); 
    //     $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);
        
    //     if($status == 200){
    //         return $response;
    //     }else{
    //         return false;
    //     }
    // }

    // public function adhocdeposit($data) {
    //     // $soapUrl = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_adhoc/700/zws_adhoc/zws_adhoc"; // asmx URL of WSDL
    //     // $soapUser = "rfc1";  //  username
    //     // $soapPassword = "init1234"; // password
    //     $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_PAYFPX"; // asmx URL of WSDL
    //     $soapUser = "tempahandev";  //  username
    //     $soapPassword = "tempahandev1234"; // password
        
    //     // xml post structure
    //     $amount = "4.00";
    //     $xml_post_string = '
    //         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
    //             <soapenv:Header/>
    //             <soapenv:Body>
    //                 <urn:Z_TEMPAHAN_PAYFPX>
    //                     <!--Optional:-->
    //                     <AMOUNT>1000.00</AMOUNT>
    //                     <!--Optional:-->
    //                     <APPROVALCODE>5225</APPROVALCODE>
    //                     <!--Optional:-->
    //                     <DCOLLECTION_REFERENCE>Mustaqim</DCOLLECTION_REFERENCE>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_ADDRESS1>Address</DCUSTOMER_ADDRESS1>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_ADDRESS2>Address</DCUSTOMER_ADDRESS2>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_POSTCODE>12345</DCUSTOMER_POSTCODE>
    //                     <!--Optional:-->
    //                     <DCUSTOMER_STATE>Selangor</DCUSTOMER_STATE>
    //                     <!--Optional:-->
    //                     <DCUST_NAME>Mustaqim</DCUST_NAME>
    //                     <!--Optional:-->
    //                     <DDOC_REFERENCE>0108979008</DDOC_REFERENCE>
    //                     <!--Optional:-->
    //                     <DFEECODE>DEP07</DFEECODE>
    //                     <!--Optional:-->
    //                     <DOCDATE>2024-01-20</DOCDATE>
    //                     <!--Optional:-->
    //                     <DOCUMENTNO>600000019032</DOCUMENTNO>
    //                     <!--Optional:-->
    //                     <DSTATUOTARY_ID>950411145917</DSTATUOTARY_ID>
    //                     <!--Optional:-->
    //                     <PCARDNO>1234</PCARDNO>
    //                     <!--Optional:-->
    //                     <PPOSTDATE>2024-01-20</PPOSTDATE>
    //                     <!--Optional:-->
    //                     <PTRANSDATE>2024-01-20</PTRANSDATE>
    //                     <!--Optional:-->
    //                     <SUB_SYSCODE>DE</SUB_SYSCODE>
    //                 </urn:Z_TEMPAHAN_PAYFPX>
    //             </soapenv:Body>
    //         </soapenv:Envelope>
    //     ';   // data from the form, e.g. some ID number
    
    //     $headers =  array(
    //                     "Content-type: text/xml;charset=\"utf-8\"",
    //                     "SOAPAction: = ", 
    //                     "Content-length: ".strlen($xml_post_string),
    //                 ); //SOAPAction: your op URL
    
    //     $url = $soapUrl;
    
    //     // PHP cURL  for https connection with auth
    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
    //     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    //     // converting
    //     $response = curl_exec($ch); 
    //     $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);
        
    //     if($status == 200){
    //         return $response;
    //     }else{
    //         return false;
    //     }
    // }
    // public function check_bp(Request $request){
    //     $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
    //     $soapUser = "tempahansys";  //  username
    //     $soapPassword = "ppjtempahan1234"; // password
        
    //     // xml post structure
    //     $xml_post_string = '
    //         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
    //             <soapenv:Header/>
    //             <soapenv:Body>
    //                 <urn:Z_TEMPAHAN_GETBP>
    //                     <!--Optional:-->
    //                     <ICNO>950411145922</ICNO>
    //                 </urn:Z_TEMPAHAN_GETBP>
    //             </soapenv:Body>
    //         </soapenv:Envelope>
    //     ';

    //     $headers =  array(
    //         "Accept: text/xml",
    //         "Content-type: text/xml;charset=\"utf-8\"",
    //         "SOAPAction: = ", 
    //         "Content-length: ".strlen($xml_post_string),
    //         "cache-control: no-cache",
    //     ); //SOAPAction: your op URL

    //     $url = $soapUrl;

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
    //     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     // converting
    //     $response = curl_exec($ch); 
    //     $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);
    //     $xml = simplexml_load_string($response);
    //     $businessPartner = (string) $xml->xpath('//BUSINESSPARTNER')[0];
    //     return $businessPartner;
    // }
    // public function create_bp(Request $request){
    //     $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
    //     $soapUser = "tempahansys";  //  username
    //     $soapPassword = "ppjtempahan1234"; // password
        
    //     // xml post structure
    //     $xml_post_string = '
    //         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
    //             <soapenv:Header/>
    //             <soapenv:Body>
    //             <urn:Z_TEMPAHAN_CREATEBP>
    //                 <IADDR_LN1>Selangor</IADDR_LN1>
    //                 <!--Optional:-->
    //                 <IADDR_LN2></IADDR_LN2>
    //                 <!--Optional:-->
    //                 <IADDR_LN3></IADDR_LN3>
    //                 <!--Optional:-->
    //                 <IADDR_LN4></IADDR_LN4>
    //                 <!--Optional:-->
    //                 <IAPPLN_TYPE></IAPPLN_TYPE>
    //                 <ICOUNTRY>MY</ICOUNTRY>
    //                 <!--Optional:-->
    //                 <ICUST_ID>950411145935</ICUST_ID>
    //                 <ICUST_NAME>MUHAMAD MUSTAQIM BIN MAZLAN</ICUST_NAME>
    //                 <ICUST_TP>B</ICUST_TP>
    //                 <IEMAIL>muhamadmustaqim.mzln@gmail.com</IEMAIL>
    //                 <!--Optional:-->
    //                 <IPHONE>0108979008</IPHONE>
    //                 <IPOSTCODE>43900</IPOSTCODE>
    //                 <ISTATE>16</ISTATE>
    //             </urn:Z_TEMPAHAN_CREATEBP>
    //             </soapenv:Body>
    //         </soapenv:Envelope>
    //     ';

    //     $headers =  array(
    //         "Accept: text/xml",
    //         "Content-type: text/xml;charset=\"utf-8\"",
    //         "SOAPAction: = ", 
    //         "Content-length: ".strlen($xml_post_string),
    //         // "Content-length: "."0",
    //         "cache-control: no-cache",
    //     ); //SOAPAction: your op URL

    //     $url = $soapUrl;

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
    //     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     // converting
    //     $response = curl_exec($ch); 
    //     $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);
        
    // }
    // public function generate_bill(Request $request){
    //     $soapUrl = "http://eccdev.ppj.gov.my:8000/sap/bc/soap/rfc?sap-client=800/Z_TEMPAHAN_GETBP"; // asmx URL of WSDL
    //     $soapUser = "tempahansys";  //  username
    //     $soapPassword = "ppjtempahan1234"; // password
        
    //     // xml post structure
    //     $xml_post_string = '
    //         <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
    //             <soapenv:Header/>
    //             <soapenv:Body>
    //             <urn:Z_TEMPAHAN_GENBILL>
    //                 <!--Optional:-->
    //                 <APPL_AREA>P</APPL_AREA>
    //                 <!--Optional:-->
    //                 <BILL_DUE_DATE></BILL_DUE_DATE>
    //                 <BUSINESSPARTNER1>0010031480</BUSINESSPARTNER1>
    //                 <CONTRACTACCOUNT1>000000712474</CONTRACTACCOUNT1>
    //                 <!--Optional:-->
    //                 <CREATED_BY></CREATED_BY>
    //                 <!--Optional:-->
    //                 <CURRENCY>RM</CURRENCY>
    //                 <DOC_DATE>2024-01-01</DOC_DATE>
    //                 <!--Optional:-->
    //                 <DOC_TYPE>ZB</DOC_TYPE>
    //                 <!--Optional:-->
    //                 <ENTRY_DATE></ENTRY_DATE>
    //                 <!--Optional:-->
    //                 <ENTRY_TIME></ENTRY_TIME>
    //                 <EVENT_DESC>DEWAN SRI MELATI</EVENT_DESC>
    //                 <FEE_TYPE>CAGAR16</FEE_TYPE>
    //                 <ITEM>
    //                     <!--Zero or more repetitions:-->
    //                     <item>
    //                         <FEE_TYPE>CAGAR16</FEE_TYPE>
    //                         <FEE_AMOUNT>1600.00</FEE_AMOUNT>
    //                     </item>
    //                 </ITEM>
    //                 <!--Optional:-->
    //                 <MAIN_TRANS>1004</MAIN_TRANS>
    //                 <POST_DATE></POST_DATE>
    //                 <!--Optional:-->
    //                 <REF_DOC_NO></REF_DOC_NO>
    //                 <!--Optional:-->
    //                 <RETURN>
    //                     <!--Zero or more repetitions:-->
    //                     <item>
    //                         <TYPE></TYPE>
    //                         <ID></ID>
    //                         <NUMBER></NUMBER>
    //                         <MESSAGE></MESSAGE>
    //                         <LOG_NO></LOG_NO>
    //                         <LOG_MSG_NO></LOG_MSG_NO>
    //                         <MESSAGE_V1></MESSAGE_V1>
    //                         <MESSAGE_V2></MESSAGE_V2>
    //                         <MESSAGE_V3></MESSAGE_V3>
    //                         <MESSAGE_V4></MESSAGE_V4>
    //                         <PARAMETER></PARAMETER>
    //                         <ROW></ROW>
    //                         <FIELD></FIELD>
    //                         <SYSTEM>?</SYSTEM>
    //                     </item>
    //                 </RETURN>
    //                 <SUBSYSTEM_ID>DE</SUBSYSTEM_ID>
    //                 <!--Optional:-->
    //                 <SUB_TRANS>1001</SUB_TRANS>
    //                 <!--Optional:-->
    //                 <TRANS_DATE>2024-01-01</TRANS_DATE>
    //             </urn:Z_TEMPAHAN_GENBILL>
    //             </soapenv:Body>
    //         </soapenv:Envelope>
    //     ';

    //     $headers =  array(
    //         "Accept: text/xml",
    //         "Content-type: text/xml;charset=\"utf-8\"",
    //         "SOAPAction: = ", 
    //         "Content-length: ".strlen($xml_post_string),
    //         // "Content-length: "."0",
    //         "cache-control: no-cache",
    //     ); //SOAPAction: your op URL

    //     $url = $soapUrl;

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword);
    //     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     // converting
    //     $response = curl_exec($ch); 
    //     $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //     curl_close($ch);
    //     $xml = simplexml_load_string($response);
    //     $sap_doc_no = (count($xml->xpath('//SAP_DOCNO')) != 0) ? (string) $xml->xpath('//SAP_DOCNO')[0] : null;
    //     return $sap_doc_no;
    // }


    public function test() {
        $txnReference = "230531050000051";
        $txnId = "FPX2300001405";
        $online = array(
            'amount' => "10.00",
            'app_code' => $txnReference,
            'address1' => 'ABCDE',
            'address2' => '',
            'postcode' => 62675,
            'state' => 'WP PUTRAJAYA',
            'name' => 'AHMAD AHMED',
            'reference' => $txnId.'AC'.$txnReference,
            'feecode' => 'SEWA11',
            'date' => date('Y-m-d'),
            'documentno' => $txnId,
            'id_number' => 800808080807,
            'card' => '000000000000000',
            'syscode' => 'AD'
        );
        $response = funcOnlinePay($online);
    }
}

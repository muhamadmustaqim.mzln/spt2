<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\All;
use App\Models\Auth;

class AuthController extends Controller
{
    public function index(Request $requests, $redirect = null){
        if(empty($requests->id)){
            return Redirect::to("https://www.ppj.gov.my/logs?url=".url('/auth'));
        }else{
            // $requests->id = 'intan@ppj.gov.my';
            // $requests->id = 'syalinda@ppj.gov.my';
            // $requests->id = 'm.mustaqim@titiansystems.com';
            // $requests->id = 'dzulhilmi@turbine-group.com';
            $allAdmin = 0;
            $depoStatus = All::GetRow('lkp_configuration', 'id', 2);

            $validate = Auth::Validate($requests->id);  // users table
            $validateProfiles = Auth::ValidateProfiles($requests->id);  // user_profiles table
            $response = Auth::Sso($requests->id);       // SSO
            // dd($validate, $validateProfiles, $response);
            if($validate){
                if($validateProfiles == null){
                    $dataUP = [
                        'fk_users'          => $id,
                        'fk_lkp_country'    => $response->user->fk_lkp_country,
                        'fk_lkp_state'      => $response->user->fk_lkp_state,
                        'bud_name'          => $response->user->name,
                        // 'bud_category'      => ,
                        // 'bud_type_of_ic'    => ,
                        'bud_reference_id'  => $response->user->reference_id,
                        'bud_address'       => $response->user->address1.','.$response->user->address2.','.$response->user->address3,
                        // 'bud_postcode'      => $response->user->postcode,
                        'bud_poscode'       => $response->user->postcode,
                        'bud_town'          => $response->user->city,
                        'bud_phone_no'      => $response->user->mobile_no,
                        'bud_office_no'     => $response->user->office_no,
                        'bud_email'         => $response->user->email,
                        // 'bud_user_indicator'=> ,
                        'bud_fax_no'        => $response->user->fax_no,
                        // 'fk_lkp_location'   => ,
                        // 'accept_mysms'      => ,
                        'bud_staff_id'      => $response->user->staff_id,
                        // 'bud_department'    => ,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ];
                    // dd('1', $dataUP);
                    $id = All::InsertGetID('user_profiles', $data);
                } else if($validateProfiles->fk_users == null){
                    $dataUP = [
                        'fk_users'      => $validate->id,
                        'updated_at'    => date('Y-m-d')
                    ];
                    // dd('2', $dataUP, $validate->id);
                    $query = All::GetUpdate('user_profiles', $validateProfiles->id, $dataUP);
                }
                $validateProfile = All::GetRow('user_profiles', 'bud_email', $requests->id);

                $roles = All::GetAllRow('user_role', 'user_id', $validate->id);
                $roles = DB::table('user_role')->join('roles', 'roles.id', 'user_role.role_id')->where('user_id', $validate->id)->where('status', 1)->get();
                $rolesArr = [];

                foreach ($roles as $key => $value) {
                    // $rolesArr[] = $this->role_location($value->role_id);
                    $rolesArr[] = ($value->role_id);
                }

                $validateProfile = All::GetRow('user_profiles', 'bud_email', $requests->id);
                $role_id = 0;
                if($allAdmin == 1){
                    $role_id = 1;
                } else {
                    if($validateProfile == null) {
                        $userProfile = [
                            'fk_users'          => $validate->id,
                            'fk_lkp_country'    => $response->user->fk_lkp_country,
                            'fk_lkp_state'      => $response->user->fk_lkp_state,
                            'bud_name'          => $response->user->name,
                            'bud_reference_id'  => $response->user->reference_id,
                            'bud_address'       => $response->user->address1 .','. $response->user->address2 .','. $response->user->address3,
                            'bud_poscode'       => $response->user->postcode,
                            'bud_town'          => $response->user->city,
                            'bud_phone_no'      => $response->user->mobile_no ?? '',
                            'bud_office_no'     => $response->user->office_no ?? '',
                            'bud_email'         => $response->user->email,
                            'bud_fax_no'        => $response->user->fax_no,
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s'),
                        ];
                        All::Insert('user_profiles', $userProfile);
                    }
                    if($validate->isAdmin == 1){
                        $role_id = $validate->isAdmin;
                    } else {
                        $role_id = 10;
                    }
                    if($response->status != 0){
                        $staffppj = ($response->user->staff_id != null) ? true : false;
                    } else {
                        $staffppj = false;
                    }
                }

                Session::put('user', [
                    'id'            => $validate->id,
                    'name'          => $validate->fullname,
                    // 'role'          => $validate->isAdmin,
                    'staff'         => $validate->isAdmin,
                    'staffppj'      => $staffppj,
                    'role'          => $role_id,
                    'roles'         => $rolesArr,
                    'logged_in'     => TRUE,

                    'deposit_status'=> $depoStatus->status,

                    'email'         => $requests->id,
                    'ic'            => $response->user->reference_id ?? '',
                    'mobile_no'     => $response->user->mobile_no ?? '',
                    'fax_no'        => $response->user->fax_no ?? '',
                    'address'       => $response->user->address1 ?? '' . ', ' . ucwords(strtolower($response->user->address2 ?? '')) ,
                    'postcode'      => $response->user->postcode ?? '',
                    'city'          => $response->user->city ?? '',
                    'state'         => $response->user->fk_lkp_state ?? '',
                    'country'         => $response->user->fk_lkp_country ?? '',
                    'nationality'   => $response->user->nationality ?? '',
                ]);
                
                // dd(Session::get('user'));
                return Redirect::to(url('/'));
            }else{
                $response = Auth::Sso($requests->id);   // SSO
                if($response->status == 1){
                    $data = array(
                        'email'         => $requests->id,
                        'username'      => $response->user->username ?? $response->user->name,
                        'fullname'      => $response->user->name,
                        'isAdmin'       => 0, //Pengguna Biasa
                        'status'        => 1,
                        'created_at'    => date('Y-m-d H:i:s')
                    );
                    $id = All::InsertGetID('users', $data);

                    $data = [
                        'fk_users'          => $id,
                        'fk_lkp_country'    => $response->user->fk_lkp_country,
                        'fk_lkp_state'      => $response->user->fk_lkp_state,
                        'bud_name'          => $response->user->name,
                        // 'bud_category'      => ,
                        // 'bud_type_of_ic'    => ,
                        'bud_reference_id'  => $response->user->reference_id,
                        'bud_address'       => $response->user->address1.','.$response->user->address2.','.$response->user->address3,
                        // 'bud_postcode'      => $response->user->postcode,
                        'bud_poscode'      => $response->user->postcode,
                        'bud_town'          => $response->user->city,
                        'bud_phone_no'      => $response->user->mobile_no,
                        'bud_office_no'     => $response->user->office_no,
                        'bud_email'         => $response->user->email,
                        // 'bud_user_indicator'=> ,
                        'bud_fax_no'        => $response->user->fax_no,
                        // 'fk_lkp_location'   => ,
                        // 'accept_mysms'      => ,
                        'bud_staff_id'      => $response->user->staff_id,
                        // 'bud_department'    => ,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                    ];
                    $id = All::InsertGetID('user_profiles', $data);

                    $data = [
                        'user_id'       => $id,
                        'role_id'       => 10,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                    ];
                    $id = All::InsertGetID('user_role', $data);
                    
                    Session::put('user', [
                        'id'        => $id,
                        'name'      => $response->user->name,

                        'ic'            => $response->user->reference_id,
                        'mobile_no'     => $response->user->mobile_no,
                        'address1'      => $response->user->address1,
                        'address2'      => $response->user->address2,
                        'postcode'      => $response->user->postcode,
                        'nationality'   => $response->user->nationality,

                        'role'      => 0,
                        'logged_in' => TRUE
                    ]);
                    
                    return Redirect::to(url('/'));
    
                }else{
                    return Redirect::to("https://ppj.gov.my/error?app=Sistem Pengurusan Tempahan&email=".$requests->id);
                }
            }
        }
    }

    public function test()
    {
        $email = "halimaton@titiansystems.com";
        $response = Auth::Sso($email );
    }

    private function role_location($id){
        if ($id == 5){
            return 4;
        } else if ($id == 6){
            return 5;
        } else if ($id == 7){
            return 6;
        } else if ($id == 8){
            return 7;
        } else if ($id == 18){
            return 9;
        } else if ($id == 19){
            return 10;
        } else if ($id == 20){
            return 11;
        } else return $id;
    }

    public function logout()
    {
        Session::flush();
        return Redirect::to("https://ppj.gov.my");
    }
}

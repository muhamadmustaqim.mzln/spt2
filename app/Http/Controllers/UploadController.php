<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function file(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $uploadedFile = $request->file('file');

            // Ensure the upload directory exists
            $uploadPath = 'upload/' . $id;
            Storage::makeDirectory($uploadPath);

            $allowedTypes = ['gif', 'jpg', 'png', 'jpeg', 'pdf'];

            // Validate file type
            $extension = $uploadedFile->getClientOriginalExtension();
            if (!in_array($extension, $allowedTypes)) {
                return "File cannot be uploaded";
            }

            // Generate a unique filename
            $filename = $uploadedFile->getClientOriginalName();

            // Store the uploaded file
            Storage::putFileAs($uploadPath, $uploadedFile, $filename);

            $size = $uploadedFile->getSize();

            return response()->json([
                'filename' => $filename,
                'size' => $size,
                'extension' => $extension,
            ], 200);

            // Save file data to the database
            // $data = [
            //     'id_fail' => $id,
            //     'nama' => $filename,
            //     'created_date' => now(),
            //     'created_by' => auth()->user()->id, // Assuming you have authentication set up
            // ];

            // // Replace 'fail' and 'latihan' with your actual database table and model names
            // YourModelName::create($data);
        } elseif ($request->has('name')) {
            $fileToRemove = $request->input('name');
            $filePath = 'upload/' . $id . '/' . $fileToRemove;

            // Delete the file from storage
            if (Storage::exists($filePath)) {
                Storage::delete($filePath);
            }

            // Delete the corresponding database record
            // Replace 'fail' and 'latihan' with your actual database table and model names
            // YourModelName::where('id_fail', $id)
            //     ->where('nama', $fileToRemove)
            //     ->delete();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\All;
use App\Models\Sport;

class MailController extends Controller
{
    public function get_status_tempahan($id){
        $data = all::GetSpecRow('lkp_status', 'id', $id);
        foreach($data as $d){
            $data = $d->ls_description;
        }
        return $data;
    }

    public function index() {
        $data = array('name'=>"AdminSPT");
     
        Mail::send(['text'=>'mail'], $data, function($message) {
           $message->to('dzulhilmijamal.dj@gmail.com')->subject
              ('Laravel Basic Testing Mail');
           $message->from('tempahan@ppj.gov.my','Sistem Pengurusan Tempahan');
        });
        if (Mail::failures()) {
            return "Fail";
        }else{
            return "Success";
        }
    }

    public function acara($userEmail, $type, $data = null, $status = null) {
        $data = [
            'type'      => $type,
            'data'      => $data,
            'status'    => $status,
        ];
        $test = self::get_status_tempahan($data['type']);
        $subject = "Permohonan ACARA (" . $data['data']['bmb_booking_no'] . ") Status : " . self::get_status_tempahan($data['type']);
        Mail::send(['html' => 'event.pdf.mail'], $data, function ($message) use ($userEmail, $subject) {
            $message->to($userEmail)->subject($subject);
            $message->from('tempahan@ppj.gov.my', 'Sistem Pengurusan Tempahan');
            $message->setBody('html', 'text/html');
        });
        if (Mail::failures()) {
            return "Fail";
        }else{
            return "Success";
        }
    }

    public function payment_received($main_booking){
        $data['main'] = All::GetRow('main_booking', 'id', $main_booking);
        $data['notempahan'] = $data['main']->bmb_booking_no;
        $data['type'] = ($data['main']->fk_lkp_status == 4) ? 1 : (($data['main']->fk_lkp_status == 6) ? 3 : 2);
        $userEmail = All::GetRow('users', 'id', $data['main']->fk_users)->email;
        if ($userEmail === 'dzulhilmi@turbine-group.com') {
            $userEmail = 'muhamadmustaqim.mzln@gmail.com'; // remove in prod
        }
        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $main_booking);
        // $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['checkfacility'] = 1; // 1 = dewan, 2 = sukan
        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        if (count($data['et_hall_book']) == 0){
            $data['checkfacility'] = 2; // 1 = dewan, 2 = sukan
        }
        $data['url'] = request()->getSchemeAndHttpHost();
        $data['email'] = Sport::validation_email($main_booking, $data['checkfacility']);

        // $subject = "Permohonan ACARA (" . $data['data']['bmb_booking_no'] . ") Status : " . self::get_status_tempahan($data['type']);
        Mail::send(['html' => 'mail.notify'], $data, function ($message) use ($userEmail) {
            $message->to($userEmail)->subject('Pre UAT Modul Tempahan Sukan');
            $message->from('tempahan@ppj.gov.my', 'Sistem Pengurusan Tempahan');
            $message->setBody('html', 'text/html');
        });

    }

    public function reschedule_email($id){
        $data['main'] = All::GetRow('main_booking', 'id', $id);
        $data['notempahan'] = $data['main']->bmb_booking_no;
        $data['type'] = 4;
        $userEmail = All::GetRow('users', 'id', $data['main']->fk_users)->email;
        // $userEmail = 'muhamadmustaqim.mzln@gmail.com';

        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $id);
        $data['checkfacility'] = 1; // 1 = dewan, 2 = sukan
        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        if (count($data['et_hall_book']) == 0){
            $data['checkfacility'] = 2; // 1 = dewan, 2 = sukan
        }

        $data['url'] = request()->getSchemeAndHttpHost();
        $data['email'] = Sport::validation_email($id, $data['checkfacility']);

        // $subject = "Permohonan ACARA (" . $data['data']['bmb_booking_no'] . ") Status : " . self::get_status_tempahan($data['type']);
        Mail::send(['html' => 'mail.notify'], $data, function ($message) use ($userEmail) {
            $message->to($userEmail)->subject('Pre UAT Modul Tempahan Sukan');
            $message->from('tempahan@ppj.gov.my', 'Sistem Pengurusan Tempahan');
            $message->setBody('html', 'text/html');
        });
    }

    public function test() {
        $client = new SoapClient();
    }
}

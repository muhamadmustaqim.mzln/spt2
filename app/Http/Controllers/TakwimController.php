<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\Takwim;
use App\Models\All;
use App\Models\Sport;

class TakwimController extends Controller
{
    public function index() {
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
        return view('takwim.lists', compact('data'));
    }

    public function call()
    {
        $takwim = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
        foreach($takwim as $row)
        {
            $data[] = array(
                'title' => $row->lh_name,
                'start' => date("Y-m-d", strtotime($row->lh_start_date)),
                'description' => '',
                'end' => date("Y-m-d", strtotime($row->lh_start_date)),
                'className' => 'fc-event-light fc-event-solid-success'
            );
        }
        echo json_encode($data);
    }

    public function sport($id)
    {
        $takwim = Sport::takwim($id);
        foreach($takwim as $row)
        {
            $data[] = array(
                'title' => $row->efd_name,
                'start' => date("Y-m-d", strtotime($row->esb_booking_date)),
                'description' => $row->est_slot_time,
                'end' => date("Y-m-d", strtotime($row->esb_booking_date)),
                'className' => 'fc-event-light fc-event-solid-success'
            );
        }
        if(!$data){
            return redirect()->back();
        }
        echo json_encode($data);
    }

    public function form(){
        return view('takwim.form');
    }

    public function addtakwim(Request $request){
        $status = $request->input('status') == 'on' ? 1 : 0;
        $validatedData = $request->validate([
            'tarikhmula' => 'required|date',
            'tarikhtamat' => 'required|date',
        ], [
            'tarikhmula.required' => 'Diperlukan',
            'tarikhtamat.required' => 'Diperlukan',
        ]);
        $data = array(
            'lh_name'           => request()->takwim,
            'lh_start_date'     => $validatedData['tarikhmula'],
            'lh_end_date'       => $validatedData['tarikhtamat'],
            'lh_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('lkp_holiday', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/takwim'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/takwim'));
        }


    }

    public function edittakwim($id) {
        $id = Crypt::decrypt($id);
        $data['takwim'] = All::GetRow('lkp_holiday', 'id', $id);
        return view('takwim.edit', compact('data'));
    }

    public function updatetakwim(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = $request->input('status') == 'on' ? 1 : 0;
        $data = array(
            'lh_name'           => request()->takwim,
            'lh_start_date'     => request()->tarikhmula,
            'lh_end_date'       => request()->tarikhtamat,
            'lh_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );

        $query = All::GetUpdate('lkp_holiday', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/takwim'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/takwim'));
        }
    }

    public function deletetakwim($id){
        $id = Crypt::decrypt($id);
        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_holiday', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/takwim'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/takwim'));
        }
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'lh_name'           => request()->lokasi,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('lkp_location', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }


    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('lkp_location', 'id', $id);
        return view('location.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
// use App\Helpers\Helper as HP;
use Illuminate\Support\Carbon;
use PDF;
use Illuminate\Support\Facades\DB;

class DepositController extends Controller
{
    public function listdeposit() {
        $data['list'] = All::ShowAll('deposit_list');

        return view('deposit.list', compact('data'));
    }

    public function formdeposit(Request $request, $id) {
        $data['id'] = Crypt::decrypt($id);
        $data['deposit_'] = All::GetRow('deposit_list', 'id', $data['id']);
        // dd($data);

        if(request()->isMethod('post')){
            $data_dept_review = [
                'fk_deposit_list'       => $data['id'],
                'ddr_date'              => Carbon::createFromFormat('d-m-Y', request()->reportdate)->format('Y-m-d'),
                'ddr_synopsis'          => request()->reportsynopsis,
                'ddr_depo_amount'       => request()->depo_amount,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s')
            ];
            $query = All::InsertGetID('deposit_dept_review', $data_dept_review);

            $data_depo_list = [
                'status'        => 2,
                'updated_at'    => date("Y-m-d H:i:s")
            ];
            $query = All::GetUpdate('deposit_list', $data['id'], $data_depo_list);

            return redirect('/deposit_payment_info/'. Crypt::encrypt($data['id']));
        }

        return view('deposit.form', compact('data'));
    }

    public function updateuser(Request $request, $id) {
        $data['id'] = Crypt::decrypt($id);
        $data['deposit_'] = All::GetRow('deposit_list', 'id', $data['id']);

        $data_depo_list = [
            'name'                  => request()->name,
            'ref_id'                => request()->ref_id,
            'email'                 => request()->email,
            'mobile_no'             => request()->mobile_no,
            'bank_account'          => request()->bank_account,
            'bank_name'             => request()->bank_name,
            'updated_at'            => date('Y-m-d H:i:s')
        ];
        $query = All::GetUpdate('deposit_list', $data['id'], $data_depo_list);

        return redirect('/list_deposit/form/'. Crypt::encrypt($data['id']));
    }

    public function maklumatbayaran($id) {
        $data['id'] = Crypt::decrypt($id);
        $data['deposit_'] = All::GetRow('deposit_list', 'id', $data['id']);

        return view('deposit.maklumatbayaran.maklumatbayaran', compact('data'));
    }

    public function support() {
        $data['list'] = All::GetAllRow('deposit_list', 'status', 2);

        return view('deposit.supportAction.list', compact('data'));
    }

    public function supportform(Request $request, $id) {
        $data['id'] = Crypt::decrypt($id);
        $data['deposit_'] = All::GetRow('deposit_list', 'id', $data['id']);
        $data['deposit_review'] = All::GetRow('deposit_dept_review', 'fk_deposit_list', $data['id']);

        if(request()->isMethod('post')){
            $data_dept_review_approval = [
                'approved_by'       => Session::get('user.id'),
                'approval_review'   => request()->ulasan,
                'approved_at'       => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ];
            $query = All::GetUpdateSpec('deposit_dept_review', 'fk_deposit_list', $data['id'], $data_dept_review_approval);
            
            $data_dept_list = [
                'status'            => 3,
                'updated_at'        => date('Y-m-d H:i:s')
            ];
            $query = All::GetUpdateSpec('deposit_list', 'id', $data['id'], $data_dept_list);
        }

        return view('deposit.supportAction.form', compact('data'));
    }

    public function review() {
        
        return view('deposit.reviewAction.list');
    }

    public function reviewform() {
        // return view('home', compact('data'));
        return view('deposit.reviewAction.form');
    }
    
    public function reviewformOpenItem() {
        // return view('home', compact('data'));
        return view('deposit.reviewAction.formOpenItem');
    }

    public function verify() {
        // return view('home', compact('data'));
        return view('deposit.verifyAction.list');
    }

    public function verifyform() {
        // return view('home', compact('data'));
        return view('deposit.verifyAction.form');
    }
    public function resitDeposit()
    {
        
        $pdf = PDF::loadView('deposit.pdf.resitonline');
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width();
        $h = $canvas->get_height();

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png';
        $imgWidth = 450;
        $imgHeight = 500;

        // Set image opacity
        $canvas->set_opacity(.2);

        // Specify horizontal and vertical position
        $x = (($w - $imgWidth) / 2);
        $y = (($h - $imgHeight) / 2);

        // Add an image to the pdf
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        return $pdf->download('Resit Rasmi Deposit.pdf');
    }
}
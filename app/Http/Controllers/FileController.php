<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Models\All;
use Illuminate\Support\Facades\Crypt;

class FileController extends Controller
{
    public function image(Request $request, $id){
        $path = public_path('assets/upload/main/'.$id);
        File::ensureDirectoryExists($path);

        $image = $request->file('file');
        $filename = $image->getClientOriginalName();
        $image->move($path, $fileName);
    }

    public function imageCover(Request $request, $id){
        $uuid = $request->post('uuidKey');
        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/main/'.$uuid);
            File::ensureDirectoryExists($path);
            $image = $request->post('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);

            $data = array(
                'cover_img'         => $filename,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            $query = All::Insert('et_facility_type', $data);
        }
    }


    public function imageVirtual(Request $request, $id){
        $path = public_path('assets/upload/virtual/'.$id);
        File::ensureDirectoryExists($path);

        $image = $request->file('file');
        $filename = $image->getClientOriginalName();
        $image->move($path, $fileName);
    }

    public function fileAcara(Request $request, $main_bookingId)
    {
        $path = public_path('dokumen/acara/' . $main_bookingId);
        $fileNames = ALL::GetAllRow('lkp_spa', 'fk_lkp_spa_type', 1, 'id', 'ASC');
        $list = ['suratIringan', 'kertasCadangan', 'suratPelantikan', 'petaLaluan'];

        try {
            foreach ($request->allFiles() as $fieldName => $file) {
                if (in_array($fieldName, $list)) { 
                    $index = (array_search($fieldName, $list));
                    if ($request->hasFile($fieldName)) {
                        $file = $request->file($fieldName);
                        $name = $fileNames[$index]->description . '.' . $file->getClientOriginalExtension();
                        $fileSize = $file->getSize();

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $file->move($path, $name);
                        
                        $spa_booking_attachmentData = [
                            'fk_main_booking'       => $main_bookingId,
                            'fk_lkp_spa_filename'   => $index+1,
                            'name'                  => $name,
                            'size'                  => $fileSize,
                            'location'              => '/dokumen/acara/'. $main_bookingId . '/' . $name,
                            'created_at'            => now(),
                            'updated_at'            => now()
                        ];
                        All::InsertGetID('spa_booking_attachment', $spa_booking_attachmentData);
                    } 
                }
            }
        } catch (\Throwable $th) {
            dd($th);
            //throw $th;
        }
        return response()->json([
            'success' => 'Successfully updated.',
        ]);
    }

    public function filePembayaranAcara(Request $request, $main_bookingId)
    {
        $name = request()->all('name')['name'];
        $path = public_path('dokumen/acara/' . $main_bookingId);
        $fileNames = ALL::GetAllRow('lkp_spa', 'fk_lkp_spa_type', 1, 'id', 'ASC');
        $list = ['suratAkuan', 'resitPembayaran', 'suratJaminan', 'lainLain'];
        $fk_lkp_spa_file = [14, 19, 18, 21];

        foreach ($request->allFiles() as $fieldName => $file) {
            if (in_array($fieldName, $list)) {
                $index = array_search($fieldName, $list);
                if ($request->hasFile($fieldName)) {
                    $file = $request->file($fieldName);
    
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
    
                    $file->move($path, $name);
    
                    $fileSize = filesize($path . '/' . $name);
    
                    $spa_booking_attachmentData = [
                        'fk_main_booking'       => $main_bookingId,
                        'fk_lkp_spa_filename'   => $fk_lkp_spa_file[$index],
                        'name'                  => $name,
                        'size'                  => $fileSize,
                        'location'              => '/dokumen/acara/' . $main_bookingId . '/' . $name,
                        'created_at'            => now(),
                        'updated_at'            => now()
                    ];
                    All::InsertGetID('spa_booking_attachment', $spa_booking_attachmentData);
                    
                    // Return a success response if the file is successfully uploaded
                    return redirect()->back();
                }
            }
        }
        return redirect()->back();
        // Return a failure response if no file is uploaded
        // return response()->json([
        //     'error' => 'No file uploaded.',
        // ]);
    }
    
    public function fileAcaraPembayaran(Request $request, $bookingId) {
        $path = public_path('dokumen/acara/' . $main_bookingId);
        $fileNames = ALL::GetAllRow('lkp_spa', 'fk_lkp_spa_type', 1, 'id', 'ASC');
        $list = ['suratAkuan', 'resitPembayaran', 'suratJaminan', 'lain-lain'];
        
        foreach ($request->allFiles() as $fieldName => $file) {
            if (in_array($fieldName, $list)) { 
                $index = (array_search($fieldName, $list));
                if ($request->hasFile($fieldName)) {
                    $file = $request->file($fieldName);
                    $name = $fileNames[$index]->description . '.' . $file->getClientOriginalExtension();
        
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
        
                    $file->move($path, $name);
        
                    $fileSize = filesize($path . '/' . $name);
                    
                    $spa_booking_attachmentData = [
                        'fk_main_booking'       => $main_bookingId,
                        'fk_lkp_spa_filename'   => $index+1,
                        'name'                  => $name,
                        'size'                  => $fileSize,
                        'location'              => '/dokumen/acara/'. $main_bookingId . '/' . $name,
                        'created_at'            => now(),
                        'updated_at'            => now()
                    ];
                    All::InsertGetID('spa_booking_attachment', $spa_booking_attachmentData);
                } 
            }
        }
        return response()->json([
            'success' => 'Successfully updated.',
        ]);
    }


    public function file(Request $request, $id){
        $path = public_path('assets/upload/main/'.$id);

        if($request->post('name')){
            $fileName = $path.'/'.$request->post('name');  
            unlink($fileName); 
        }else{
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
        
            $file = $request->file('file');
            //$name = uniqid() . '_' . trim($file->getClientOriginalName());
            $name = $file->getClientOriginalName();
        
            $file->move($path, $name);
        
            return response()->json([
                'name'          => $name,
                'original_name' => $file->getClientOriginalName(),
            ]);
        }
    }

    public function scan_file($id)
    {
        $ds = DIRECTORY_SEPARATOR; 
        $storeFolder = public_path('assets/upload/main/'.$id);
        $result = array();

        $files = scandir($storeFolder);     
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {       
                    $obj['name'] = $file;
                    $obj['size'] = filesize($storeFolder.$ds.$file);
                    $result[] = $obj;
                }
            }
        }
        
        header('Content-type: text/json');              
        header('Content-type: application/json');
        echo json_encode($result);
    }
}

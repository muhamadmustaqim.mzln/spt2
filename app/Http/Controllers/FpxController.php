<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Sport;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper as HP;
use App\Http\Controllers\BapiController;
use App\Http\Controllers\MailController as Mail;
use Illuminate\Support\Facades\DB;
use App\Models\AuditLog;
use Illuminate\Support\Facades\Http;

class FpxController extends Controller
{
    public function direct(Request $request) {
        $payload = $request->get('payload');
        if(is_array($payload))
        {
            $orderNo = $payload['orderNo'];
            $status = $payload['status'];
            $txnReference = $payload['txnReference'];
            $txnId = $payload['txnId'];
            $txnTime = $payload['txnTime'];
            $amount = $payload['amount'];
            $bankname = $payload['bankName'];
            $exOrderNo = $payload['exOrderNo'];
        }
        $data = array(
            'bank'              => $bankname,
            'fpx_order_no'      => $orderNo,
            'fpx_txn_reference' => $txnReference,
            'fpx_txn_id'        => $txnId,
            'fpx_date'          => $txnTime,
            'fpx_txn_time'      => $txnTime,    
            'fpx_status'        => $status,                      
            'total_amount'      => $amount,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        All::Insert('et_fpx_log', $data);

        if($status == "SUCCESS"){
            $status = 5;
        }else if ($status == "FAILED"){
            $status = 6;
        }else{
            $status = 32;
        }

        // For Event/Acara
        if (strpos($data['orderNo'], 'SPA') === 0) {
            $data = array(
                'bank_name'         => $bankname,
                'application_no'    => $orderNo,
                'no_bil'            => $txnReference,
                'fpx_serial_no'     => $txnId,
                'payment_date'      => $txnTime,
                'transaction_date'  => $txnTime,    
                'paid_amount'       => $amount,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('spa_payment', 'application_no', $orderNo, $data);
        } 
        // For Sukan/Dewan
        else {
            $data_fpx = array(
                'bank'              => $bankname,
                'fpx_serial_no'     => $txnId,
                'fpx_trans_id'      => $orderNo,
                'fpx_date'          => $txnTime,
                'fpx_trans_date'    => $txnTime,
                'fpx_status'        => $status,                      
                'amount_paid'       => $amount,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data_fpx);
            $id = All::GetRow('et_payment_fpx', 'fpx_trans_id', $orderNo);
    
            $data_main = array(
                'fk_lkp_status'              => $status,
                'updated_at'                 => date('Y-m-d H:i:s'),
                'updated_by'                 => Session::get('user')['id'] ?? 1,
            );
            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
            $b_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $id->fk_main_booking);
            $t_facility = All::GetRow('et_facility_type', 'id', $b_facility->fk_et_facility_type);
            $facility = All::GetRow('et_facility', 'id', $t_facility->fk_et_facility);
            $main = All::GetRow('main_booking', 'id', $id->fk_main_booking);
            $user = All::GetRow('user_profiles', 'fk_users', $main->fk_users);
            if($facility->ef_type == 1){
                Log::info("Dewan");
            }else{
                // $online = array(
                //     'amount' => $amount,
                //     'app_code' => $txnReference,
                //     'address1' => $user->bud_address,
                //     'address2' => '',
                //     'postcode' => $user->bud_poscode,
                //     'state' => $user->bud_town,
                //     'name' => $user->bud_name,
                //     'reference' => $txnId.'AC'.$txnReference,
                //     'feecode' => 'SEWA11',
                //     'date' => date('Y-m-d'),
                //     'documentno' => $txnId,
                //     'id_number' => $user->bud_reference_id,
                //     'card' => '',
                //     'syscode' => 'AD'
                // );
                // $response = funcOnlinePay($online);
                Log::info("Sukan: ".$response);
            }
        }
        // $id = All::GetRow('et_payment_fpx', 'fpx_trans_id', $data['orderNo']);
        // $data_main = array(
        //     'bank'              => $bankname,
        //     'fpx_serial_no'     => $txnId,
        //     'fpx_trans_id'      => $orderNo,
        //     'fpx_date'          => $txnTime,
        //     'fpx_txn_time'      => $txnTime,    
        //     'fpx_status'        => HP::fpx_status($status),                      
        //     'amount_paid'       => $amount,
        //     'created_at'        => date('Y-m-d H:i:s'),
        //     'updated_at'        => date('Y-m-d H:i:s')
        // );
        // All::GetUpdateSpec('main_booking', 'id', $id->fk_main_booking, $data_main);
    }

    public function indirect(Request $request) {
        $status = '';
        $data['orderNo'] = request()->orderNo;
        $data['status'] = request()->status;
        $data['txnReference'] = request()->txnReference;
        $data['txnId'] = request()->txnId;
        $data['txnTime'] = date("Y-m-d H:i:s", strtotime(request()->txnTime));
        $data['amount'] = request()->amount;
        $data['bankname'] = request()->bankName;
        $data['exOrderNo'] = request()->exOrderNo;
        $sapDocNo = "";
        $data['id'] = All::GetRow('bh_payment_fpx', 'fpx_trans_id', $data['orderNo']);
        $approvalCode = $request->post('txnId') . 'AC' . $request->post('txnReference');
        if (strlen($data['orderNo']) == 18) {
            $data['newOrderNo'] = substr($data['orderNo'], 0, 14);
        } else {
            $data['newOrderNo'] = $data['orderNo'];
        }
        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['newOrderNo']);
        $data['bh_booking'] = All::GetRow('bh_booking', 'fk_main_booking', $data['main']->id);
        $data['bh_booking_all_id'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['main']->id)->pluck('id');
        foreach ($data['bh_booking_all_id'] as $bbid) {
            $data['bh_equipment_all'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $bbid);
        }
        $data['bh_booking_detail'] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $data['bh_booking']->id);
        $data['quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);

        $data['user'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['user']->fk_lkp_state = HP::negeri($data['user']->fk_lkp_state);
        $data['user']->fk_lkp_country = HP::negara($data['user']->fk_lkp_country);

        $data['bh_hall_depo_feecode'] = All::GetRow('bh_hall', 'id', $data['bh_booking']->fk_bh_hall)->feecode_depo_online;
        $data['bh_hall_feecode'] = All::GetRow('bh_hall', 'id', $data['bh_booking']->fk_bh_hall)->bh_code_fee;
        if ($data['main']->fk_lkp_status == 2) {        // Deposit & Pendahuluan
            $data['paystatus'] = 4;
        } else if ($data['main']->fk_lkp_status == 4) { // Penuh
            $data['paystatus'] = 5;
        }
        $data['trans'] = (object)[
            'txnTime'   => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
            'amount'    => request()->amount,
            'feecode'   => $data['bh_hall_feecode'],
            'depo_feecode'  => $data['bh_hall_depo_feecode'],
            'fpxNum'    => $approvalCode,//$request->post('txnId'),
            'approvalCode'  => $request->post('txnReference'),
            'exOrderNo'         => $data['exOrderNo']
        ];
        try {
            DB::transaction(function () use($request, $data) {
                if($data['status'] == "SUCCESS"){
                    $input = array(
                        "amount" => request()->amount,
                        "app_code" => request()->txnReference,
                        "address1" => $data['user']->bud_address,
                        "address2" => '',
                        "postcode" => $data['user']->bud_poscode,
                        "state" => $data['user']->bud_town,
                        "name" => $data['user']->bud_name,
                        "reference" => request()->txnId.'AC'.substr(request()->txnReference, 0, -1),
                        "date" => date("Y-m-d"),
                        "documentno" => request()->txnId,
                        "id_number" =>  $data['user']->bud_reference_id,
                        "card" => '',
                        "syscode" => 'AD',
                    );
                    
                    if($data['main']->fk_lkp_status == 2) { // Deposit + Pendahuluan
                        // DEPOSIT::Begin
                            // $dataBP = BapiController::check_bp($user->bud_reference_id);
                            // $checkbpstatus = $dataBP;
                            // if($dataBP[0] == null || $dataBP[1] == null) {
                            //     $dataBP = BapiController::create_bp($user);
                            // } 
                            // $sap_doc_no = BapiController::generate_bill($dataBP, $user);
                            // $adhocResponse = BapiController::sappayment($sap_doc_no, $user, $data['trans'], 'DE');

                            $data_fpx = array(
                                'fk_main_booking'   => $data['main']->id,
                                'fk_bh_quotation'   => $data['quotation']->id,
                                'fk_lkp_payment_type'   => 1,
                                // 'sap_doc_no'        => $adhocResponse[0],
                                'bank'              => $data['bankname'],
                                'fpx_serial_no'     => $data['txnId'],
                                'fpx_trans_id'      => $data['orderNo'],
                                'fpx_date'          => $data['txnTime'],
                                'total_amount'      => $data['main']->bmb_subtotal,
                                'deposit_amount'    => $data['main']->bmb_deposit_rm,
                                'amount_paid'       => $data['main']->bmb_deposit_rm,
                                'fpx_status'        => 1,
                                'fpx_trans_date'    => $data['txnTime'],
                                'created_at'        => date('Y-m-d H:i:s'),
                            );
                            All::GetUpdate2SpecColumn('bh_payment_fpx', 'fpx_trans_id', $data['orderNo'], 'fk_lkp_payment_type', 1, $data_fpx);
                            $bpfID = All::GetAllRow('bh_payment_fpx', 'fpx_trans_id', $data['orderNo'])->where('fk_lkp_payment_type', 1)->first()->id;
                            $data_fpx_detail = [        // Save Hall FPX Detail
                                'fk_bh_payment_fpx' => $bpfID,
                                'fk_lkp_gst_rate'   => $data['bh_booking']->fk_lkp_gst_rate,
                                'fk_bh_hall'        => $data['bh_booking']->fk_bh_hall,
                                'fk_bh_equipment'   => null,
                                'product_indicator' => 1,
                                'booking_date'      => $data['main']->bmb_booking_date,
                                'start_time'          => $data['bh_booking_detail']->bbd_start_time,
                                'end_time'          => $data['bh_booking_detail']->bbd_end_time,
                                'unit_price'        => $data['bh_booking']->hall_price,
                                'quantity'          => 1,
                                'total_amount'      => $data['bh_booking']->hall_price,
                                'created_at'        => date('Y-m-d H:i:s'),
                            ];
                            All::Insert('bh_fpx_detail', $data_fpx_detail);
                            foreach ($data['bh_equipment_all'] as $beall) {        // Save Equipment FPX Detail
                                $data_fpx_detail = [
                                    'fk_bh_payment_fpx' => $bpfID,
                                    'fk_lkp_gst_rate'   => $data['bh_booking']->fk_lkp_gst_rate,
                                    'fk_bh_hall'        => $data['bh_booking']->fk_bh_hall,
                                    'fk_bh_equipment'   => $beall->fk_bh_equipment,
                                    'product_indicator' => 2,
                                    'booking_date'      => $data['main']->bmb_booking_date,
                                    'start_time'          => $data['bh_booking_detail']->bbd_start_time,
                                    'end_time'          => $data['bh_booking_detail']->bbd_end_time,
                                    'unit_price'        => $beall->bbe_total,
                                    'quantity'          => $beall->bbe_quantity,
                                    'total_amount'      => $beall->bbe_total,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];
                                All::Insert('bh_fpx_detail', $data_fpx_detail);
                            }
                        // DEPOSIT::End
                        
                        // DOWNPAYMENT::Begin
                            // $dataBP = BapiController::check_bp($user->bud_reference_id);
                            // $checkbpstatus = $dataBP;
                            // if($dataBP[0] == null || $dataBP[1] == null) {
                            //     $dataBP = BapiController::create_bp($user);
                            // } 
                            // $sap_doc_no = BapiController::generate_bill($dataBP, $user);
                            // $adhocResponse = BapiController::adhoc($sap_doc_no, $user, $data['trans'], 'PS');
                            $data_fpx2 = array(
                                'fk_main_booking'   => $data['main']->id,
                                'fk_bh_quotation'   => $data['quotation']->id,
                                'fk_lkp_payment_type'   => 3,
                                // 'sap_doc_no'        => $adhocResponse[0],
                                'bank'              => $data['bankname'],
                                'fpx_serial_no'     => $data['txnId'],
                                'fpx_trans_id'      => $data['orderNo'],
                                'fpx_date'          => $data['txnTime'],
                                'total_amount'      => $data['main']->bmb_subtotal,
                                'deposit_amount'    => $data['main']->bmb_deposit_rm,
                                'amount_paid'       => ($data['main']->bmb_subtotal / 2),
                                'fpx_status'        => 1,
                                'fpx_trans_date'    => $data['txnTime'],
                                'created_at'        => date('Y-m-d H:i:s'),
                            );
                            All::GetUpdate2SpecColumn('bh_payment_fpx', 'fpx_trans_id', $data['orderNo'], 'fk_lkp_payment_type', 3, $data_fpx2);
                        // DOWNPAYMENT::End

                        $dates = HP::getDatesBetween($data['bh_booking']->bb_start_date, $data['bh_booking']->bb_end_date);
                        foreach($dates as $key => $value){
                            $data_bcb = array(
                                'fk_bh_hall'            => $data['bh_booking']->fk_bh_hall,
                                'fk_main_booking'       => $data['main']->id,
                                'date_booking'          => $value,
                                'created_at'            => date('Y-m-d H:i:s'),
                                'updated_at'            => date('Y-m-d H:i:s'),
                            );
                            All::Insert('bh_confirm_booking', $data_bcb);
                        }
                        
                        $data_main = array(
                            // 'sap_no'                    => $adhocResponse[0],
                            'fk_lkp_status'             => $data['paystatus'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'updated_by'                => Session::get('user')['id'],
                        );
                        All::GetUpdate('main_booking', $data['id']->fk_main_booking, $data_main);

                        $auditData = [
                            'fk_users'      => Session::get('user')['id'],
                            'fk_lkp_task'   => 35, // 35 - Proses Pembayaran
                            'table_ref_id'  => $data['main']->id,
                            'task'          => 'Bayaran FPX No Resit -' . $data['orderNo'],
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s'),
                        ];
                        All::Insert('audit_trail', $auditData);
                        
                        Log::info("request AD: " . json_encode($input));
                        // Log::info("response AD: " . json_encode($adhocResponse));
                        // if($adhocResponse != false){
                        //     $sapDocNo = $adhocResponse[0];            
                        //     Log::info("SAP No: " . $sapDocNo);
                        // }
                        // Log::info("request AD: " . json_encode($input));
                        // $data_sap = funcOnlinePay($input);
                        // Log::info("response AD: " . $data_sap);
                        // if($data_sap != false){
                        //     $xml = simplexml_load_string($data_sap);
                        //     $sapDocNo = (string) $xml->xpath('//SAP_DOCNO')[0];            
                        //     Log::info("SAP No: " . $sapDocNo);
                        // }
                    } else if($data['main']->fk_lkp_status == 4) { // Penuh
                        // DOWNPAYMENT::Begin
                        // $dataBP = BapiController::check_bp($user->bud_reference_id);
                        // $checkbpstatus = $dataBP;
                        // if($dataBP[0] == null || $dataBP[1] == null) {
                        //     $dataBP = BapiController::create_bp($user);
                        // } 
                        // $sap_doc_no = BapiController::generate_bill($dataBP, $user);
                        // $adhocResponse = BapiController::adhoc($sap_doc_no, $user, $data['trans'], 'PS');
                        $data_fpx = array(
                            'fk_main_booking'   => $data['main']->id,
                            'fk_bh_quotation'   => $data['quotation']->id,
                            'fk_lkp_payment_type'   => 2,
                            // 'sap_doc_no'        => $adhocResponse[0],
                            'bank'              => $data['bankname'],
                            'fpx_serial_no'     => $data['txnId'],
                            'fpx_trans_id'      => $data['orderNo'],
                            'fpx_date'          => $data['txnTime'],
                            'total_amount'      => $data['main']->bmb_subtotal,
                            'deposit_amount'    => $data['main']->bmb_deposit_rm,
                            'amount_paid'       => ($data['main']->bmb_subtotal / 2),
                            'fpx_status'        => 1,
                            'fpx_trans_date'    => $data['txnTime'],
                            'created_at'        => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdate2SpecColumn('bh_payment_fpx', 'fpx_trans_id', $data['orderNo'], 'fk_lkp_payment_type', 2, $data_fpx);
                        // All::GetUpdateSpec('bh_payment_fpx', 'fpx_trans_id', $data['orderNo'], $data_fpx2);
                        // DOWNPAYMENT::End

                        $data_main = array(
                            // 'sap_no'                    => $adhocResponse[0],
                            'fk_lkp_status'             => $data['paystatus'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'updated_by'                => Session::get('user')['id'],
                        );
                        All::GetUpdate('main_booking', $data['id']->fk_main_booking, $data_main);

                        $data_fpx = array(
                            'fk_bh_hall'            => $data['bh_booking']->fk_bh_hall,
                            'fk_main_booking'       => $data['main']->id,
                            'date_booking'          => $data['main']->id,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdateSpec('bh_confirm_booking', 'fk_main_booking', $data['main']->id, $data_fpx);
                        
                        $auditData = [
                            'fk_users'      => Session::get('user')['id'],
                            'fk_lkp_task'   => 35, // 35 - Proses Pembayaran
                            'table_ref_id'  => $data['main']->id,
                            'task'          => 'Bayaran FPX No Resit -' . $data['orderNo'],
                            'created_at'        => date('Y-m-d H:i:s'),
                            'updated_at'        => date('Y-m-d H:i:s'),
                        ];
                        All::Insert('audit_trail', $auditData);
                        
                    }
                } else if ($data['status'] == "FAILED"){
                    $status = 6;
                }else{
                    $status = 32;
                }
                // $data_main = array(
                //     // 'sap_no'                    => $sapDocNo,
                //     'fk_lkp_status'              => $status,
                //     'updated_at'                 => date('Y-m-d H:i:s'),
                //     'updated_by'                 => Session::get('user')['id'],
                // );
                // All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
            });
        } catch (\Throwable $th) {
            dd($th);
            DB::rollBack();
        }

        $data['main'] = All::GetRow('main_booking', 'id', $data['main']->id);
        return view('hall.public.status', compact('data'));
    }

    public function directsps(Request $request) {
        $payload = $request->get('payload');
        Log::info("FPX SPS Direct". json_encode($payload));
        
        if(is_array($payload))
        {
            $orderNo = $payload['orderNo'];
            $status = $payload['status'];
            $txnReference = $payload['txnReference'];
            $txnId = $payload['txnId'];
            $txnTime = $payload['txnTime'];
            $amount = $payload['amount'];
            $bankname = $payload['bankName'];
            $exOrderNo = $payload['exOrderNo'];
        }

        if (strlen($orderNo) == 18) {
            $newOrderNo = substr($orderNo, 0, 14);
        } 
        $main = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
        if($status == "SUCCESS"){
            $status1 = 5;
            AuditLog::log(Crypt::encrypt(122), Crypt::encrypt($main->id), 'Bayaran Telah Diterima (Direct) - '.$main->bmb_booking_no);
        }else if ($status == "FAILED"){
            $status1 = 6;
            AuditLog::log(Crypt::encrypt(176), Crypt::encrypt($main->id), 'Bayaran Gagal - '.$main->bmb_booking_no);
        }else{
            $status1 = 32;
        }
        $id = All::GetRow('et_payment_fpx', 'fk_main_booking', $main->id);
        $et_booking_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $main->id);
        $sport = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $et_booking_facility->id);
        $hall = All::GetAllRow('et_hall_book', 'fk_et_booking_facility', $et_booking_facility->id);
        $fk_lkp_location = $main->fk_lkp_location;
        $efd_fee_code = All::GetAllRow('et_facility_detail', 'fk_et_facility_type', $et_booking_facility->fk_et_facility_type)[0]->efd_fee_code;
        $user = All::GetRow('user_profiles', 'fk_users', $main->fk_users);  
        $user->fk_lkp_state = HP::negeri($user->fk_lkp_state);
        $user->fk_lkp_country = HP::negara($user->fk_lkp_country);
        $approvalCode = $txnId . 'AC' . $txnReference;


        if($status == 'PENDING_APPROVAL'){
            // $data = array(
            //     'bank'              => $bankname,
            //     'fpx_order_no'      => $orderNo,
            //     'fpx_txn_reference' => $txnReference,   // 2403151140020992
            //     'fpx_txn_id'        => $txnId,          // FPX2400000243
            //     'fpx_date'          => $data['trans']->txnTime,     // 2024-03-15
            //     'fpx_txn_time'      => $txnTime,        // 20240315
            //     'fpx_status'        => $status,         // SUCCEESS
            //     'total_amount'      => $amount,
            //     'created_at'        => date('Y-m-d H:i:s'),
            //     'updated_at'        => date('Y-m-d H:i:s')
            // );
            // All::Insert('et_fpx_log', $data);

            // Update Sukan Payment
            $data = array(
                'bank'              => $bankname,
                'fpx_trans_id'      => $orderNo,
                'fpx_txn_reference' => $txnReference,   // 2403151140020992
                'fpx_serial_no'     => $txnId,          // FPX2400000243
                'fpx_trans_date'    => $txnTime,        // 2024-03-15
                'status'            => $status1,        // 1
                'amount_paid'       => $amount,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data);

            // Update Sukan Booking Details
            $data_main = array(
                'fk_lkp_status'             => $status1,
                'sap_flag'                  => 1,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'] ?? 1,
            );
            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);

        } else if($status == 'SUCCESS'){
            try {
                $data['trans'] = (object)[
                    'txnTime'           => date("Y-m-d H:i:s", strtotime($txnTime)),
                    'amount'            => $amount,
                    'feecode'           => $efd_fee_code,
                    'fpxNum'            => $approvalCode,//$request->post('txnId'),
                    'approvalCode'      => $txnReference,
                    'bmb_booking_no'    => $newOrderNo,
                    'exOrderNo'         => $exOrderNo,
                ];

                $sap_flag = All::GetRow('main_booking', 'id', $main->id)->sap_flag;
                // BAPI SAP Process::Begin
                if($sap_flag == 0 || $sap_flag == null){
                    try {
                        if(count($sport) > 0){ // Sukan
                            $adhocResponse = BapiController::adhocSport('', $user, $data['trans']);
                            AuditLog::log(Crypt::encrypt(178), Crypt::encrypt($main->id), 'Sukan Direct: '. $newOrderNo .' | '. $adhocResponse[0], 1);

                            // Update Sukan Booking Details
                            $data_main = array(
                                'fk_lkp_status'             => $status1,
                                'sap_flag'                  => 1,
                                'updated_at'                => date('Y-m-d H:i:s'),
                                'updated_by'                => Session::get('user')['id'] ?? 1,
                            );
                            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
                            
                            $data_fpx = array(
                                'bank'              => $bankname,
                                'fpx_trans_id'      => $orderNo,
                                'ex_order_no'       => $exOrderNo,
                                'sap_doc_no'        => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,
                                'fpx_txn_reference' => $txnReference,
                                'fpx_serial_no'     => $txnId,
                                'fpx_date'          => date('Y-m-d 00:00:00'),    
                                'fpx_trans_date'    => $txnTime,    
                                'fpx_status'        => 1,
                                'amount_paid'       => $amount,
                                'created_at'        => date('Y-m-d H:i:s'),
                            );
                            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data_fpx);
                        } else { // Dewan
                            $dataBP = BapiController::check_bp($user->bud_reference_id);
                            $checkbpstatus = $dataBP;
                            if($dataBP[0] == null || $dataBP[1] == null) {
                                $dataBP = BapiController::create_bp($user);
                            } 
                            $sap_doc_no = BapiController::generate_bill($dataBP, $user, $newOrderNo);
                            // $adhocResponse = BapiController::adhocSport(null, $user, $data['trans']);
                            $adhocResponse = BapiController::adhocSport($sap_doc_no, $user, $data['trans']);
                            AuditLog::log(Crypt::encrypt(178), Crypt::encrypt($main->id), 'Sukan Direct', 1);
                        }
                    } catch (\Throwable $th) {
                        Log::info('Direct Error: ');
                        Log::info($th);
                        AuditLog::log(Crypt::encrypt(180), Crypt::encrypt($main->id), 'sap_flag: '. $th, 1);
                    }
                }
                // BAPI SAP Process::End

                if(count($sport) > 0){
                    $type = 2;
                } else {
                    $type = 1;
                }
                $data_ebf = [
                    'ebf_subtotal'      => $main->bmb_subtotal,
                    'ebf_facility_indi' => $type,
                    'updated_at'        => date('Y-m-d')
                ];
                All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $main->id, $data_ebf);

                $ecb = All::GetRow('et_confirm_booking', 'fk_main_booking', $main->id);
                if($ecb == null) {
                    $data_fpx = array(
                        'fk_main_booking'       => $main->id,
                        'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                        'ecb_date_booking'      => $et_booking_facility->ebf_start_date,
                        'ecb_flag_indicator'    => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    );
                    $query = All::InsertGetID('et_confirm_booking', $data_fpx);
                }
            } catch (\Throwable $th) {
                AuditLog::log(Crypt::encrypt(179), Crypt::encrypt($main->id), 'Sukan Direct Error: '. $newOrderNo, 1);
            }
    
        } else if($status == 'FAILED'){
            // Update Sukan Payment
            $data = array(
                'bank'              => $bankname,
                'fpx_trans_id'      => $orderNo,
                'fpx_txn_reference' => $txnReference,   // 2403151140020992
                'fpx_serial_no'     => $txnId,          // FPX2400000243
                'fpx_trans_date'    => $txnTime,        // 2024-03-15
                'status'            => $status1,        // 1
                'amount_paid'       => $amount,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data);

            // Update Sukan Booking Details
            $data_main = array(
                'fk_lkp_status'             => $status1,
                'sap_flag'                  => 2,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'] ?? 1,
            );
            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
        }
        // $validateFPXStatus = All::GetAllRow('et_payment_fpx', 'fpx_trans_id', $data['orderNo'])->first();
        // if ($validateFPXStatus->fpx_status == 0){}

        // $statusFpx = All::GetRow('et_fpx_log', 'fpx_order_no', $orderNo);
        // if($statusFpx != null){
        //     if($statusFpx->fpx_status == 'PENDING_APPROVAL'){
        //         try {
        //             $data['trans'] = (object)[
        //                 'txnTime'   => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
        //                 'amount'    => request()->amount,
        //                 'feecode'   => $efd_fee_code,
        //                 'fpxNum'    => $approvalCode,//$request->post('txnId'),
        //                 'approvalCode' => $request->post('txnReference')
        //             ];
        
        //             $data = array(
        //                 'bank'              => $bankname,
        //                 'fpx_order_no'      => $orderNo,
        //                 'fpx_txn_reference' => $txnReference,   // 2403151140020992
        //                 'fpx_txn_id'        => $txnId,          // FPX2400000243
        //                 'fpx_date'          => $data['trans']->txnTime,     // 2024-03-15
        //                 'fpx_txn_time'      => $txnTime,        // 20240315
        //                 'fpx_status'        => $status,         // SUCCEESS
        //                 'total_amount'      => $amount,
        //                 'created_at'        => date('Y-m-d H:i:s'),
        //                 'updated_at'        => date('Y-m-d H:i:s')
        //             );
        //             // All::Insert('et_fpx_log', $data);
        //             All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $data);

        //             // Update Sukan Payment
        //             $data = array(
        //                 'bank'              => $bankname,
        //                 'fpx_trans_id'      => $orderNo,
        //                 'fpx_txn_reference' => $txnReference,   // 2403151140020992
        //                 'fpx_serial_no'     => $txnId,          // FPX2400000243
        //                 'fpx_trans_date'    => $txnTime,        // 2024-03-15
        //                 'status'            => $status1,        // 1
        //                 'amount_paid'       => $amount,
        //                 'created_at'        => date('Y-m-d H:i:s'),
        //                 'updated_at'        => date('Y-m-d H:i:s')
        //             );
        //             All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data);

        //             // BAPI SAP Process::Begin
        //             $dataBP = BapiController::check_bp($user->bud_reference_id);
        //             $checkbpstatus = $dataBP;
        //             if($dataBP[0] == null || $dataBP[1] == null) {
        //                 $dataBP = BapiController::create_bp($user);
        //             } 
        //             $sap_doc_no = BapiController::generate_bill($dataBP, $user);
        //             $adhocResponse = BapiController::adhoc($sap_doc_no, $user, $data['trans']);
        //             // BAPI SAP Process::End
        //         } catch (\Throwable $th) {
        //             Log::info("Error Direct: " . $th);
        //         }
        //     }
        // } else {
        //     try {
        //         $data['trans'] = (object)[
        //             'txnTime'   => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
        //             'amount'    => request()->amount,
        //             'feecode'   => $efd_fee_code,
        //             'fpxNum'    => $approvalCode,//$request->post('txnId'),
        //             'approvalCode' => $request->post('txnReference'),
        //             'bmb_booking_no'    => $newOrderNo
        //         ];
    
        //         $data = array(
        //             'bank'              => $bankname,
        //             'fpx_order_no'      => $orderNo,
        //             'fpx_txn_reference' => $txnReference,   // 2403151140020992
        //             'fpx_txn_id'        => $txnId,          // FPX2400000243
        //             'fpx_date'          => $data['trans']->txnTime,     // 2024-03-15
        //             'fpx_txn_time'      => $txnTime,        // 20240315
        //             'fpx_status'        => $status,         // SUCCEESS
        //             'total_amount'      => $amount,
        //             'created_at'        => date('Y-m-d H:i:s'),
        //             'updated_at'        => date('Y-m-d H:i:s')
        //         );
        //         All::Insert('et_fpx_log', $data);

        //         // Update Sukan Payment
        //         $data = array(
        //             'bank'              => $bankname,
        //             'fpx_trans_id'      => $orderNo,
        //             'fpx_txn_reference' => $txnReference,   // 2403151140020992
        //             'fpx_serial_no'     => $txnId,          // FPX2400000243
        //             'fpx_trans_date'    => $txnTime,        // 2024-03-15
        //             'status'            => $status1,        // 1
        //             'amount_paid'       => $amount,
        //             'created_at'        => date('Y-m-d H:i:s'),
        //             'updated_at'        => date('Y-m-d H:i:s')
        //         );
        //         All::Insert('et_payment_fpx', $data);

        //         $sap_flag = All::GetRow('main_booking', 'id', $main->id)->sap_flag;
        //         // BAPI SAP Process::Begin
        //         if($sap_flag == 0 || $sap_flag == null){
        //             try {
        //                 $adhocResponse = BapiController::adhocSport($sap_doc_no, $user, $data['trans']);
        //                 AuditLog::log(Crypt::encrypt(178), Crypt::encrypt($main->id), 'Sukan Direct', 1);
        //             } catch (\Throwable $th) {
        //                 Log::info($th);
        //             } 
        //         }
        //         // BAPI SAP Process::End
        //         // Update Sukan Booking Details
        //         $data_main = array(
        //             'fk_lkp_status'             => $status1,
        //             'sap_flag'                  => 1,
        //             'updated_at'                => date('Y-m-d H:i:s'),
        //             'updated_by'                => Session::get('user')['id'] ?? 1,
        //         );
        //         All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
                
        //     } catch (\Throwable $th) {
        //         Log::info("Error Direct: " . $th);
        //     }
        // }
    }

    public function indirectsps(Request $request) {
        Log::info("FPX SPS Indirect");
        $status = '';
        $data['orderNo'] = request()->orderNo;
        $data['exOrderNo'] = request()->exOrderNo;
        $data['status'] = request()->status;
        $data['txnReference'] = request()->txnReference;    // 2403151128020963
        $data['txnId'] = request()->txnId;                  // FPX2400000240
        $data['txnTime'] = date("Y-m-d H:i:s", strtotime(request()->txnTime));
        $data['amount'] = request()->amount;
        $data['bankname'] = request()->bankName;
        $sapDocNo = "";
        $approvalCode = $request->post('txnId') . 'AC' . $request->post('txnReference');

        // Fetch Payment Details (spa_payment), Booking Details (main_booking) & User Details (user_profiles)
        if (strlen($data['orderNo']) == 18) {
            $newOrderNo = substr($data['orderNo'], 0, 14);
        } 
        $main = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
        $id = All::GetRow('et_payment_fpx', 'fk_main_booking', $main->id);
        $et_booking_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $main->id);
        // Find if sps type sport
        $sportbook = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $et_booking_facility->id);
        // dd($sportbook);
        $efprice = [];
        if(count($sportbook) > 0){
            $sporttime = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $sportbook[0]->id)->toArray();
            foreach ($sporttime as $key => $value) {
                $efprice[] = $value->fk_et_slot_price;
            }   
        }

        $fk_lkp_location = $main->fk_lkp_location;
        $efd_fee_code = All::GetAllRow('et_facility_detail', 'fk_et_facility_type', $et_booking_facility->fk_et_facility_type)[0]->efd_fee_code;
        $user = All::GetRow('user_profiles', 'fk_users', $main->fk_users);
        $user->fk_lkp_state = HP::negeri($user->fk_lkp_state);
        $user->fk_lkp_country = HP::negara($user->fk_lkp_country);
        $data['trans'] = (object)[
            'txnTime'           => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
            'amount'            => request()->amount,
            'feecode'           => $efd_fee_code,
            'fpxNum'            => $approvalCode,//$request->post('txnId'),
            'approvalCode'      => $request->post('txnReference'),
            'bmb_booking_no'    => $newOrderNo,
            'exOrderNo'         => $data['exOrderNo']
        ];

        // Update Sukan Details
        // $validateFPXStatus = All::GetAllRow('et_payment_fpx', 'fpx_trans_id', $data['orderNo'])->first();
        // if ($validateFPXStatus->fpx_status == 0){
        if($data['status'] == "SUCCESS"){
            try {
                $status = 5;
                $input = array(
                    "amount" => request()->amount,
                    "app_code" => request()->txnReference,
                    "address1" => $user->bud_address,
                    "address2" => '',
                    "postcode" => $user->bud_poscode,
                    "state" => $user->bud_town,
                    "name" => $user->bud_name,
                    "reference" => request()->txnId.'AC'.substr(request()->txnReference, 0, -1),
                    "feecode" => $efd_fee_code,
                    "date" => date("Y-m-d"),
                    "documentno" => request()->txnId,
                    "id_number" =>  $user->bud_reference_id,
                    "card" => '',
                    "syscode" => 'AD',
                );

                $dataFpxLog = array(
                    'bank'              => $data['bankname'],
                    'fpx_order_no'      => $data['orderNo'],        // SPS**
                    'sap_doc_no'        => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,    
                    'fpx_txn_reference' => $data['txnReference'],   // 221003
                    'fpx_txn_id'        => $data['txnId'],          // FPX**
                    'fpx_date'          => $data['txnTime'],        // 2024-10-03
                    'fpx_txn_time'      => request()->txnTime,      // 20221003
                    'fpx_status'        => $data['status'],         
                    'total_amount'      => $data['amount'],
                    'created_at'        => date('Y-m-d H:i:s'),
                );
                $query = All::Insert('et_fpx_log', $dataFpxLog);
                
                $sap_flag = All::GetRow('main_booking', 'id', $main->id)->sap_flag;
                $data_fpx = array(
                    'bank'              => $data['bankname'],
                    'fpx_trans_id'      => $data['orderNo'],
                    'ex_order_no'       => $data['exOrderNo'],
                    // 'sap_doc_no'        => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,
                    'fpx_txn_reference' => $data['txnReference'],
                    'fpx_serial_no'     => $data['txnId'],
                    'fpx_date'          => date('Y-m-d 00:00:00'),    
                    'fpx_trans_date'    => $data['txnTime'],    
                    'fpx_status'        => 1,
                    'amount_paid'       => $data['amount'],
                    'created_at'        => date('Y-m-d H:i:s'),
                );
                All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $data['orderNo'], $data_fpx);
                $data_main = array(
                    'fk_lkp_status'             => $status,
                    // 'sap_no'                    => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,
                    'sap_flag'                  => 2,
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
                // BAPI SAP Process::Begin
                if($sap_flag == 0 || $sap_flag == null){
                    try {
                        // $dataBP = BapiController::check_bp($user->bud_reference_id);
                        // $checkbpstatus = $dataBP;
                        // if($dataBP[0] == null || $dataBP[1] == null) {
                        //     $dataBP = BapiController::create_bp($user);
                        // } 
                        // $sap_doc_no = BapiController::generate_bill($dataBP, $user, $newOrderNo);
                        // $adhocResponse = BapiController::adhocSport(null, $user, $data['trans']);
                        $adhocResponse = BapiController::adhocSport('', $user, $data['trans']);
                        // AuditLog::log(Crypt::encrypt(177), Crypt::encrypt($main->id), 'Sukan Indirect: '. $newOrderNo . ' | '. $adhocResponse[0], 1);
                        
                        $data_main = array(
                            // 'fk_lkp_status'             => $status,
                            'sap_no'                    => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,
                            // 'sap_flag'                  => 2,
                            // 'updated_at'                => date('Y-m-d H:i:s'),
                            // 'updated_by'                => Session::get('user')['id'],
                        );
                        All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
                        
                        $data_fpx = array(
                            // 'bank'              => $data['bankname'],
                            // 'fpx_trans_id'      => $data['orderNo'],
                            // 'ex_order_no'       => $data['exOrderNo'],
                            'sap_doc_no'        => (isset($adhocResponse[0])) ? $adhocResponse[0] : null,
                            // 'fpx_txn_reference' => $data['txnReference'],
                            // 'fpx_serial_no'     => $data['txnId'],
                            // 'fpx_date'          => date('Y-m-d 00:00:00'),    
                            // 'fpx_trans_date'    => $data['txnTime'],    
                            // 'fpx_status'        => 1,
                            // 'amount_paid'       => $data['amount'],
                            // 'created_at'        => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $data['orderNo'], $data_fpx);
                    } catch (\Throwable $th) {
                        // dd('th', $th);
                    } 
                }
                // BAPI SAP Process::End

                $data_ebf = [
                    'ebf_subtotal'      => $main->bmb_subtotal,
                    'updated_at'        => date('Y-m-d')
                ];
                All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $main->id, $data_ebf);

                // $ecb = All::GetRow('et_confirm_booking', 'fk_main_booking', $main->id);
                // if($ecb == null) {
                    $data_ecb = array(
                        'fk_main_booking'       => $main->id,
                        'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                        'ecb_date_booking'      => $et_booking_facility->ebf_start_date,
                        'ecb_flag_indicator'    => 1,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    );
                    $ecbid = All::InsertGetID('et_confirm_booking', $data_ecb);
                    // $ecbid = 301059;
                // }
                // if(Session::get('user.id') == '61894'){
                    foreach ($efprice as $key => $value) {
                        $plcholder = All::GetRow('et_slot_price', 'id', $value);
                        $data_ecbd = array(
                            'fk_et_confirm_booking' => $ecbid,
                            'fk_et_facility_detail' => $et_booking_facility->fk_et_facility_detail,
                            'fk_et_slot_time'       => $plcholder->fk_et_slot_time,
                            'ecbd_date_booking'     => $et_booking_facility->ebf_start_date,
                            'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        $query = All::InsertGetID('et_confirm_booking_detail', $data_ecbd);
                    }
                // }
            } catch (\Throwable $th) {
            }
                            
            AuditLog::log(Crypt::encrypt(34), Crypt::encrypt($main->id), 'Masuk ke confirm booking', 1);
            // AuditLog::log(Crypt::encrypt(122), Crypt::encrypt($main->id), 'Bayaran Penuh Diterima (Indirect) - '.$main->bmb_booking_no);

            Log::info("request AD: " . json_encode($input));
            // Log::info("response AD: " . json_encode($adhocResponse));
            // if($adhocResponse != false){
            //     $sapDocNo = $adhocResponse[0];            
            //     Log::info("SAP No: " . $sapDocNo);
            // }
        } else if ($data['status'] == "FAILED"){
            $status = 6;
            $status1 = 'FAILED';

            $dataFpxLog = array(
                'bank'              => $data['bankname'],
                'fpx_order_no'      => $data['orderNo'],
                'fpx_txn_reference' => $data['txnReference'],
                'fpx_txn_id'        => $data['txnId'],
                'fpx_date'          => $data['txnTime'],
                'fpx_txn_time'      => request()->txnTime,    
                'fpx_status'        => $status1,                      
                'total_amount'      => $data['amount'],
                'created_at'        => date('Y-m-d H:i:s'),
                // 'updated_at'        => date('Y-m-d H:i:s')
            );
            $query = All::Insert('et_fpx_log', $dataFpxLog);

            // Transaction Audit Trail
            $auditData = [
                'fk_users'      => Session::get('user')['id'],
                'fk_lkp_task'   => 35, // 35 - Proses Pembayaran
                'table_ref_id'  => $main->id,
                'task'          => 'Bayaran FPX No Resit -' . $newOrderNo,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
            All::Insert('audit_trail', $auditData);
            
            // Update Sukan Payment
            $data_fpx = array(
                'fpx_status'        => $status,
                'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', request()->orderNo, $data_fpx);

            $data_main = array(
                'sap_no'                    => $sapDocNo,
                'fk_lkp_status'             => $status,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);

            AuditLog::log(Crypt::encrypt(176), Crypt::encrypt($main->id), 'Bayaran Gagal - '.$main->bmb_booking_no);
            // Log::Begin
            Log::info("request AD: Transaction Failed for Order No: ". request()->orderNo);
            // Log::End
        }else{
            $status = 32;
            $status1 = 'PENDING_APPROVAL';

            $dataEFL = array(
                'bank'              => $data['bankname'],
                'fpx_order_no'      => $data['orderNo'],
                'fpx_txn_reference' => $data['txnReference'],
                'fpx_txn_id'        => $data['txnId'],
                'fpx_date'          => $data['txnTime'],
                'fpx_txn_time'      => request()->txnTime,    
                'fpx_status'        => $status1,                      
                'total_amount'      => $data['amount'],
                'created_at'        => date('Y-m-d H:i:s'),
                // 'updated_at'        => date('Y-m-d H:i:s')
            );
            $query = All::Insert('et_fpx_log', $dataEFL);
            Log::info("FPX SPS Indirect else query". json_encode($query));

            // Transaction Audit Trail
            $auditData = [
                'fk_users'      => Session::get('user')['id'],
                'fk_lkp_task'   => 35, // 35 - Proses Pembayaran
                'table_ref_id'  => $main->id,
                'task'          => 'Bayaran FPX No Resit -' . $data['orderNo'],
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
            All::Insert('audit_trail', $auditData);

            // Update Sukan Payment
            $data_fpx = array(
                'bank'              => $data['bankname'],
                'fpx_trans_id'      => $data['orderNo'],
                'fpx_txn_reference' => $data['txnReference'],
                'fpx_serial_no'     => $data['txnId'],
                'fpx_status'        => $status,
                'fpx_date'          => date('Y-m-d H:i:s'),    
                'fpx_trans_date'    => $data['txnTime'],    
                'amount_paid'       => $data['amount'],
                'created_at'        => date('Y-m-d H:i:s'),
                // 'updated_at'        => date('Y-m-d H:i:s')
            );
            All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $data['orderNo'], $data_fpx);
            
            $data_main = array(
                'sap_no'                    => $sapDocNo,
                'fk_lkp_status'             => $status,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);

            $data['main'] = All::GetRow('main_booking', 'id', $id->fk_main_booking);
        }
        // }
        $data['main'] = All::GetRow('main_booking', 'id', $id->fk_main_booking);
        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);
        
		// if (!isset($b_facility->fk_et_facility_detail)) {
		// 	return view('sport.public.status', compact('data'));
		// }
        return view('sport.public.status', compact('data'));
    }

    public function directevent(Request $request) {
        $payload = $request->get('payload');
        if(is_array($payload))
        {
            $orderNo = $payload['orderNo'];
            $status = $payload['status'];
            $txnReference = $payload['txnReference'];
            $txnId = $payload['txnId'];
            $txnTime = $payload['txnTime'];
            $amount = $payload['amount'];
            $bankname = $payload['bankName'];
            $exOrderNo = $payload['exOrderNo'];

            $data['orderNo'] = $payload['orderNo'];
            $data['status'] = $payload['status'];
            $data['txnReference'] = $payload['txnReference'];
            $data['txnId']  = $payload['txnId'];
            $data['txnTime'] = $payload['txnTime'];
            $data['amount'] = $payload['amount'];
            $data['bankname'] = $payload['bankName'];
            $approvalCode = $request->post('txnId') . 'AC' . $request->post('txnReference');
            $data['txnDate'] = date('Y-m-d', strtotime($data['txnTime']));
            $data['txnDateTime'] = date('Y-m-d H:i:s', strtotime($data['txnTime']));
            $data['exOrderNo'] = $payload['exOrderNo'];
        }
        $status = '';
        $sapDocNo = "";
        if (strlen($data['orderNo']) == 18) {
            $newOrderNo = substr($data['orderNo'], 0, 14);
        } 
        $data['trans'] = (object)[
            'txnTime'           => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
            'amount'            => request()->amount,
            'feecode_depo'      => 'CAGAR11',
            'fpxNum'            => $approvalCode,//$request->post('txnId'),
            'approvalCode'      => $request->post('txnReference'),
            'bmb_booking_no'    => $newOrderNo,
            'exOrderNo'         => $data['exOrderNo'],
        ];

        // Fetch Payment Details (spa_payment), Booking Details (main_booking) & User Details (user_profiles)
        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
        $data['booking_event'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['main']->id);
        $data['spa_location'] = All::GetRow('spa_location', 'id', $data['booking_event']->fk_spa_location);
        $data['user'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['user']->fk_lkp_state = HP::negeri($data['user']->fk_lkp_state);
        $data['user']->fk_lkp_country = HP::negara($data['user']->fk_lkp_country);

        $data['spa'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('deleted_at', NULL);
        $data['spa_depo'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 1)->where('deleted_at', NULL)->first();
        $data['spa_full'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 2)->where('deleted_at', NULL)->first();
        $data['spa_downpayment'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 3)->where('deleted_at', NULL)->first();
        
        $data['maklumatacaralokasi'] = [
            'namaDewan'         => $data['spa_location']->name,
            'feecode_depo'      => $data['spa_location']->feecode_depo,     // CAGAR11 etc
            'fee_code'          => $data['spa_location']->fee_code,         // 1022 etc
            'fee_code_sub'      => $data['spa_location']->fee_code_sub,     // 1001 etc
            'amount_depo'       => $data['main']->bmb_deposit_rm,
            'amount_subtotal'   => $data['main']->bmb_subtotal,
            'exOrderNo'         => $data['exOrderNo'],
        ];

        Log::info("Fpx Direct Event ". $data['exOrderNo']);
        
        $paymentType = 0;
        $data['paymentstatusDepo'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 1)->where('payment_status', 0);
        $data['paymentstatusDownP'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 3)->where('payment_status', 0);
        $data['paymentstatusFull'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 2)->where('payment_status', 0);
        if(count($data['paymentstatusDepo']) > 0 || count($data['paymentstatusDownP']) > 0 || count($data['paymentstatusFull'])> 0){
            $paymentType = 1;
            $updatePaymentStatus = [
                'payment_status'    => 1,
                'updated_at'        => date('Y-m-d'),
            ];
            $query = All::GetUpdateSpec('spa_payment', 'fk_main_booking', $data['main']->id, $updatePaymentStatus);
        } 
        if($paymentType == 1){
            if($data['status'] == "SUCCESS"){
                $dataBP = BapiController::check_bp($data['user']->bud_reference_id);
                $checkbpstatus= $dataBP;
                if($dataBP[0] == null || $dataBP[1] == null) {
                    $dataBP = BapiController::create_bp($user);
                } 
                if($data['main']->fk_lkp_status == 24 || $data['main']->fk_lkp_status == 6){
                    $status = 26;
                    $event_start_date = $data['booking_event']->event_date;
                    $event_total_day = $data['booking_event']->total_day;
                    $dates = [];
                    for ($i = 0; $i < $event_total_day; $i++) {
                        $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
                    }
                    $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                    if(count($spa_booking_confirm_validate) == 0){
                        foreach ($dates as $key => $value) {
                            $data_confirm_booking = array(
                                'fk_main_booking'           => $data['main']->id,
                                'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                'fk_user'                   => $data['main']->fk_users,
                                'date_booking'              => $value,
                                'created_at'                => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s'),
                            );
                            All::Insert('spa_booking_confirm', $data_confirm_booking);
                        }
                    }

                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'DE', $data['main']->bmb_deposit_rm);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'DE', $data['main']->bmb_deposit_rm, $data['spa_location']->feecode_depo,);

                    $data['spa_payment_depo'] = array(
                        'bank_name'         => $data['bankname'],
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],
                        'payment_status'    => 1,
                        'paid_amount'       => $data['main']->bmb_deposit_rm,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    $query = All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 1, $data['spa_payment_depo']);

                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'PS', $data['main']->bmb_subtotal / 2);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);

                    $data['spa_payment_downpymt'] = array(
                        'bank_name'         => $data['bankname'],
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],    
                        'payment_status'    => 1,
                        'paid_amount'       => $data['main']->bmb_subtotal / 2,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 3, $data['spa_payment_downpymt']);

                    $data_main = array(
                        'fk_lkp_status'             => $status,
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'updated_by'                => Session::get('user.id'),
                    );
                    All::GetUpdate('main_booking', $data['main']->id, $data_main);

                    
                    $data['pendahuluandeposit'] = [
                        'bmb_booking_no'    => $data['orderNo'],
                    ];
                    Mail::acara('muhamadmustaqim.mzln@gmail.com', $status, $data['pendahuluandeposit']);
                } 
                else if($data['main']->fk_lkp_status == 26 || $data['main']->fk_lkp_status == 34){
                    $status = 20;
                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'PS', $data['main']->bmb_subtotal / 2);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);
                
                    $event_start_date = $data['booking_event']->event_date;
                    $event_end_date = $data['booking_event']->event_date_end;
                    $event_total_day = $data['booking_event']->total_day;
            
                    // Determine the number of days based on event_total_day or event_date_end if total_day is null
                    if ($event_total_day === null) {
                        // Calculate number of days between start and end date (inclusive)
                        $start = new DateTime($event_start_date);
                        $end = new DateTime($event_end_date);
                        $interval = $start->diff($end);
                        $event_total_day = $interval->days + 1; // Adding 1 to include end date
                    }

                    $dates = [];
                    for ($i = 0; $i < $event_total_day; $i++) {
                        $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
                    }

                    // Check if there are existing records in spa_booking_confirm
                    $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                    if (count($spa_booking_confirm_validate) == 0) {
                        foreach ($dates as $key => $value) {
                            $data_confirm_booking = array(
                                'fk_main_booking'           => $data['main']->id,
                                'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                'fk_user'                   => $data['main']->fk_users,
                                'date_booking'              => $value,
                                'created_at'                => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s'),
                            );
                            All::Insert('spa_booking_confirm', $data_confirm_booking);
                        }
                    } else {
                        // Handle the condition for existing spa_booking_confirm records based on date range
                        // For example, you might want to update existing records or perform other actions.
                        foreach ($dates as $key => $value) {
                            // Check if a record exists for this date range in spa_booking_confirm
                            $existing_record = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                            if (count($existing_record) == 0) {
                                // Insert new record if no record exists for this date
                                $data_confirm_booking = array(
                                    'fk_main_booking'           => $data['main']->id,
                                    'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                    'fk_user'                   => $data['main']->fk_users,
                                    'date_booking'              => $value,
                                    'created_at'                => date('Y-m-d H:i:s'),
                                    'updated_at'                => date('Y-m-d H:i:s'),
                                );
                                All::Insert('spa_booking_confirm', $data_confirm_booking);
                            } else {
                                // Update existing record if needed
                                // Example: All::Update('spa_booking_confirm', $update_data, 'condition');
                            }
                        }
                    }

                    $data['spa_payment_full'] = array(
                        'bank_name'         => $data['bankname'],
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],    
                        'paid_amount'       => $data['main']->bmb_subtotal / 2,
                        'payment_status'    => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 2, $data['spa_payment_full']);

                    $data_main = array(
                        'fk_lkp_status'             => $status,
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'updated_by'                => Session::get('user.id'),
                    );
                    All::GetUpdate('main_booking', $data['main']->id, $data_main);
                }
            } else if ($data['status'] == "FAILED"){
                // Unsuccessful
                if($data['main']->fk_lkp_status == 24){
                    $status = 6;
                } else {
                    $status = 34;
                }

                $data_main = array(
                    'fk_lkp_status'             => $status,
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                All::GetUpdate('main_booking', $data['main']->id, $data_main);
            } else { // Pending 
                $status = 32;

                $data_main = array(
                    'fk_lkp_status'             => $status,
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                All::GetUpdate('main_booking', $data['main']->id, $data_main);
            }
        }
        
        // if($status == "SUCCESS"){
        //     if($data['main']->fk_lkp_status == 24){
        //         $status_mainbooking = 26;
        //     } else if ($data['main']->fk_lkp_status == 26) {
        //         $status_mainbooking = 5;
        //     }
        //     $status = 1;
        // }else if ($status == "FAILED"){
        //     $status = 0;
        // }else{
        //     $status = 32;
        // }
        // if($status == "SUCCESS"){
        //     $dataBP = BapiController::check_bp($data['user']->bud_reference_id);
        //     $checkbpstatus= $dataBP;
        //     if($dataBP[0] == null || $dataBP[1] == null) {
        //         $dataBP = BapiController::create_bp($user);
        //     } 
        //     if($data['main']->fk_lkp_status == 24){
        //         $status = 26;
                
        //         $event_start_date = $data['booking_event']->event_date;
        //         $event_total_day = $data['booking_event']->total_day;
        //         $dates = [];
        //         for ($i = 0; $i < $event_total_day; $i++) {
        //             $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
        //         }
        //         $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
        //         if(count($spa_booking_confirm_validate) == 0){
        //             foreach ($dates as $key => $value) {
        //                 $data_confirm_booking = array(
        //                     'fk_main_booking'           => $data['main']->id,
        //                     'fk_spa_location'           => $data['booking_event']->fk_spa_location,
        //                     'fk_user'                   => $data['main']->fk_users,
        //                     'date_booking'              => $value,
        //                     'updated_at'                => date('Y-m-d H:i:s'),
        //                 );
        //                 All::Insert('spa_booking_confirm', 'fk_main_booking', $data['main']->id, $data_confirm_booking);
        //             }
        //         }

        //         $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'DE', $data['main']->bmb_deposit_rm);
        //         $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'DE', $data['main']->bmb_deposit_rm, $data['spa_location']->feecode_depo,);

        //         $data['spa_payment_depo'] = array(
        //             'bank_name'         => $request->post('bankName'),
        //             'no_bil'            => $sap_doc_no[0],
        //             'fpx_serial_no'     => $request->post('txnId'),
        //             'payment_date'      => $data['txnDateTime'],
        //             'transaction_date'  => $data['txnDate'],    
        //             'paid_amount'       => $data['main']->bmb_deposit_rm,
        //             'created_at'        => date('Y-m-d H:i:s'),
        //             'updated_at'        => date('Y-m-d H:i:s')
        //         );
        //         All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 1, $data['spa_payment_depo']);

        //         $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'PS', $data['main']->bmb_subtotal / 2);
        //         $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);

        //         $data['spa_payment_downpymt'] = array(
        //             'bank_name'         => $request->post('bankName'),
        //             'no_bil'            => $sap_doc_no[0],
        //             'fpx_serial_no'     => $request->post('txnId'),
        //             'payment_date'      => $data['txnDateTime'],
        //             'transaction_date'  => $data['txnDate'],    
        //             'paid_amount'       => $data['main']->bmb_subtotal / 2,
        //             'created_at'        => date('Y-m-d H:i:s'),
        //             'updated_at'        => date('Y-m-d H:i:s')
        //         );
        //         All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 3, $data['spa_payment_downpymt']);

        //         $data_main = array(
        //             'fk_lkp_status'             => $status,
        //             'updated_at'                => date('Y-m-d H:i:s'),
        //             'updated_by'                => Session::get('user')['id'],
        //         );
        //         All::GetUpdate('main_booking', $data['main']->id, $data_main);

                
        //         $data['pendahuluandeposit'] = [
        //             'bmb_booking_no'    => $data['orderNo'],
        //         ];
        //         Mail::acara('muhamadmustaqim.mzln@gmail.com', $status, $data['pendahuluandeposit']);
        //     } 
        //     else if($data['main']->fk_lkp_status == 26){
        //         $status = 5;
        //         $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'DE', $data['main']->bmb_subtotal / 2);
        //         $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);
            
        //         $event_start_date = $data['booking_event']->event_date;
        //         $event_total_day = $data['booking_event']->total_day;
        //         // $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
        //         // if (count($spa_booking_confirm_validate) == 0){
        //         //     $dates = [];
        //         //     for ($i = 0; $i < $event_total_day; $i++) {
        //         //         $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
        //         //     }
        //         //     foreach ($dates as $key => $value) {
        //         //         $data_confirm_booking = array(
        //         //             'fk_main_booking'           => $data['main']->id,
        //         //             'fk_spa_location'           => $data['booking_event']->fk_spa_location,
        //         //             'fk_user'                   => $data['main']->fk_users,
        //         //             'date_booking'              => $value,
        //         //             'updated_at'                => date('Y-m-d H:i:s'),
        //         //         );
        //         //         All::Insert('spa_booking_confirm', 'fk_main_booking', $data['main']->id, $data_confirm_booking);
        //         //     }
        //         // }

        //         $data['spa_payment_full'] = array(
        //             'bank_name'         => $request->post('bankName'),
        //             'no_bil'            => $sap_doc_no[0],
        //             'fpx_serial_no'     => $request->post('txnId'),
        //             'payment_date'      => $data['txnDateTime'],
        //             'transaction_date'  => $data['txnDate'],    
        //             'paid_amount'       => $data['main']->bmb_subtotal / 2,
        //             'created_at'        => date('Y-m-d H:i:s'),
        //             'updated_at'        => date('Y-m-d H:i:s')
        //         );
        //         All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 2, $data['spa_payment_full']);

        //         $data_main = array(
        //             'fk_lkp_status'             => $status_mainbooking,
        //             'updated_at'                => date('Y-m-d H:i:s'),
        //             'updated_by'                => Session::get('user')['id'],
        //         );
        //         All::GetUpdate('main_booking', $data['main']->id, $data_main);
        //     }
        // } else if($status == 'FAILED'){
        //     $data_main = array(
        //         'fk_lkp_status'             => $status,
        //         'updated_at'                => date('Y-m-d H:i:s'),
        //         'updated_by'                => Session::get('user')['id'],
        //     );
        //     All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
        // } else { // Pending 
        //     $status = 32;

        //     $data_main = array(
        //         'fk_lkp_status'             => $status,
        //         'updated_at'                => date('Y-m-d H:i:s'),
        //         'updated_by'                => Session::get('user')['id'],
        //     );
        //     All::GetUpdate('main_booking', $id->fk_main_booking, $data_main);
        // }
    }

    public function indirectevent(Request $request) {
        $status = '';
        $data['orderNo'] = request()->orderNo;
        $data['status'] = request()->status;
        $data['txnReference'] = request()->txnReference;
        $data['exOrderNo'] = request()->exOrderNo;
        $data['txnId'] = request()->txnId;
        $data['txnTime'] = date("Y-m-d H:i:s", strtotime(request()->txnTime));
        $data['amount'] = request()->amount;
        $data['bankname'] = request()->bankName;
        $approvalCode = $request->post('txnId') . 'AC' . $request->post('txnReference');
        $data['txnDate'] = date('Y-m-d', strtotime($data['txnTime']));
        $data['txnDateTime'] = date('Y-m-d H:i:s', strtotime($data['txnTime']));

        $sapDocNo = "";
        if (strlen($data['orderNo']) == 18) {
            $newOrderNo = substr($data['orderNo'], 0, 14);
        } 
        $data['trans'] = (object)[
            'txnTime'           => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
            'amount'            => request()->amount,
            'feecode_depo'      => 'CAGAR11',
            'fpxNum'            => $approvalCode,//$request->post('txnId'),
            'approvalCode'      => $request->post('txnReference'),
            'bmb_booking_no'    => $newOrderNo,
            'exOrderNo'         => $data['exOrderNo']
        ];

        // Fetch Payment Details (spa_payment), Booking Details (main_booking) & User Details (user_profiles)
        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
        $receiptExistence = All::GetAllRow('spa_payment', 'transaction_id', $data['orderNo'])->where('status', 1);
        if(!$receiptExistence){
            return redirect(url('/event/postpayment', [Crypt::encrypt($data['main']->id), Crypt::encrypt($data['orderNo']), Crypt::encrypt($data['status'])]));
        }
        $data['booking_event'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['main']->id);
        $data['spa_location'] = All::GetRow('spa_location', 'id', $data['booking_event']->fk_spa_location);
        $data['user'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['user']->fk_lkp_state = HP::negeri($data['user']->fk_lkp_state);
        $data['user']->fk_lkp_country = HP::negara($data['user']->fk_lkp_country);

        $data['spa'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('deleted_at', NULL);
        $data['spa_depo'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 1)->where('deleted_at', NULL)->first();
        $data['spa_full'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 2)->where('deleted_at', NULL)->first();
        $data['spa_downpayment'] = All::GetSpecRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 3)->where('deleted_at', NULL)->first();
        
        $data['maklumatacaralokasi'] = [
            'namaDewan'         => $data['spa_location']->name,
            'feecode_depo'      => $data['spa_location']->feecode_depo,     // CAGAR11 etc
            'fee_code'          => $data['spa_location']->fee_code,         // 1022 etc
            'fee_code_sub'      => $data['spa_location']->fee_code_sub,     // 1001 etc
            'amount_depo'       => $data['main']->bmb_deposit_rm,
            'amount_subtotal'   => $data['main']->bmb_subtotal
        ];
        
        $paymentType = 0;
        $data['paymentstatusDepo'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 1)->where('payment_status', 0);
        $data['paymentstatusDownP'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 3)->where('payment_status', 0);
        $data['paymentstatusFull'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id)->where('fk_lkp_payment_type', 2)->where('payment_status', 0);
        if(count($data['paymentstatusDepo']) > 0 || count($data['paymentstatusDownP']) > 0 || count($data['paymentstatusFull']) > 0){
            $paymentType = 1;
            $updatePaymentStatus = [
                'payment_status'    => 1,
                'updated_at'        => date('Y-m-d'),
            ];
            $query = All::GetUpdateSpec('spa_payment', 'fk_main_booking', $data['main']->id, $updatePaymentStatus);
        } 
        // Update Event Details
        if($paymentType == 1){
            if($data['status'] == "SUCCESS"){
                $status = 5;
                $dataBP = BapiController::check_bp($data['user']->bud_reference_id);
                $checkbpstatus= $dataBP;
                if($dataBP[0] == null || $dataBP[1] == null) {
                    $dataBP = BapiController::create_bp($user);
                } 
                if($data['main']->fk_lkp_status == 24 || $data['main']->fk_lkp_status == 6){
                    $status = 26;
                    $event_start_date = $data['booking_event']->event_date;
                    $event_total_day = $data['booking_event']->total_day;
                    $dates = [];
                    for ($i = 0; $i < $event_total_day; $i++) {
                        $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
                    }
                    $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                    if(count($spa_booking_confirm_validate) == 0){
                        foreach ($dates as $key => $value) {
                            $data_confirm_booking = array(
                                'fk_main_booking'           => $data['main']->id,
                                'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                'fk_user'                   => $data['main']->fk_users,
                                'date_booking'              => $value,
                                'created_at'                => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s'),
                            );
                            All::Insert('spa_booking_confirm', $data_confirm_booking);
                        }
                    }

                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'DE', $data['main']->bmb_deposit_rm);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'DE', $data['main']->bmb_deposit_rm, $data['spa_location']->feecode_depo,);

                    $data['spa_payment_depo'] = array(
                        'bank_name'         => $request->post('bankName'),
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],
                        'payment_status'    => 1,
                        'paid_amount'       => $data['main']->bmb_deposit_rm,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    $query = All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 1, $data['spa_payment_depo']);

                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'PS', $data['main']->bmb_subtotal / 2);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);

                    $data['spa_payment_downpymt'] = array(
                        'bank_name'         => $request->post('bankName'),
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],    
                        'payment_status'    => 1,
                        'paid_amount'       => $data['main']->bmb_subtotal / 2,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 3, $data['spa_payment_downpymt']);

                    $data_main = array(
                        'fk_lkp_status'             => $status,
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'updated_by'                => Session::get('user')['id'],
                    );
                    All::GetUpdate('main_booking', $data['main']->id, $data_main);

                    
                    $data['pendahuluandeposit'] = [
                        'bmb_booking_no'    => $data['orderNo'],
                    ];
                    Mail::acara('muhamadmustaqim.mzln@gmail.com', $status, $data['pendahuluandeposit']);
                } 
                else if($data['main']->fk_lkp_status == 26 || $data['main']->fk_lkp_status == 34){
                    $status = 20;
                    $sap_doc_no = BapiController::generate_bill_event($dataBP, $data['maklumatacaralokasi'], 'PS', $data['main']->bmb_subtotal / 2);
                    $adhocResponse = BapiController::adhocEvent($sap_doc_no, $data['trans'], 'PS', $data['main']->bmb_subtotal / 2, $data['spa_location']->fee_code);
                
                    $event_start_date = $data['booking_event']->event_date;
                    $event_end_date = $data['booking_event']->event_date_end;
                    $event_total_day = $data['booking_event']->total_day;
            
                    // Determine the number of days based on event_total_day or event_date_end if total_day is null
                    if ($event_total_day === null) {
                        // Calculate number of days between start and end date (inclusive)
                        $start = new DateTime($event_start_date);
                        $end = new DateTime($event_end_date);
                        $interval = $start->diff($end);
                        $event_total_day = $interval->days + 1; // Adding 1 to include end date
                    }

                    $dates = [];
                    for ($i = 0; $i < $event_total_day; $i++) {
                        $dates[] = date('Y-m-d', strtotime($event_start_date . ' + ' . $i . ' day'));
                    }

                    // Check if there are existing records in spa_booking_confirm
                    $spa_booking_confirm_validate = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                    if (count($spa_booking_confirm_validate) == 0) {
                        foreach ($dates as $key => $value) {
                            $data_confirm_booking = array(
                                'fk_main_booking'           => $data['main']->id,
                                'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                'fk_user'                   => $data['main']->fk_users,
                                'date_booking'              => $value,
                                'created_at'                => date('Y-m-d H:i:s'),
                                'updated_at'                => date('Y-m-d H:i:s'),
                            );
                            All::Insert('spa_booking_confirm', $data_confirm_booking);
                        }
                    } else {
                        // Handle the condition for existing spa_booking_confirm records based on date range
                        // For example, you might want to update existing records or perform other actions.
                        foreach ($dates as $key => $value) {
                            // Check if a record exists for this date range in spa_booking_confirm
                            $existing_record = All::GetAllRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                            if (count($existing_record) == 0) {
                                // Insert new record if no record exists for this date
                                $data_confirm_booking = array(
                                    'fk_main_booking'           => $data['main']->id,
                                    'fk_spa_location'           => $data['booking_event']->fk_spa_location,
                                    'fk_user'                   => $data['main']->fk_users,
                                    'date_booking'              => $value,
                                    'created_at'                => date('Y-m-d H:i:s'),
                                    'updated_at'                => date('Y-m-d H:i:s'),
                                );
                                All::Insert('spa_booking_confirm', $data_confirm_booking);
                            } else {
                                // Update existing record if needed
                                // Example: All::Update('spa_booking_confirm', $update_data, 'condition');
                            }
                        }
                    }

                    $data['spa_payment_full'] = array(
                        'bank_name'         => $request->post('bankName'),
                        'no_bil'            => $sap_doc_no[0],
                        'sap_doc_no'        => $sap_doc_no[0],
                        'fpx_serial_no'     => $request->post('txnId'),
                        'payment_date'      => $data['txnDateTime'],
                        'transaction_date'  => $data['txnDate'],    
                        'paid_amount'       => $data['main']->bmb_subtotal / 2,
                        'payment_status'    => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s')
                    );
                    All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['main']->id, 'fk_lkp_payment_type', 2, $data['spa_payment_full']);

                    $data_main = array(
                        'fk_lkp_status'             => $status,
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'updated_by'                => Session::get('user')['id'],
                    );
                    All::GetUpdate('main_booking', $data['main']->id, $data_main);
                }
            } else if ($data['status'] == "FAILED"){
                // Unsuccessful
                if($data['main']->fk_lkp_status == 24){
                    $status = 6;
                } else {
                    $status = 34;
                }

                $data_main = array(
                    'fk_lkp_status'             => $status,
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                All::GetUpdate('main_booking', $data['main']->id, $data_main);
            } else { // Pending 
                $status = 32;

                $data_main = array(
                    'fk_lkp_status'             => $status,
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                All::GetUpdate('main_booking', $data['main']->id, $data_main);
            }
        }
            
        return redirect(url('/event/postpayment', [Crypt::encrypt($data['main']->id), Crypt::encrypt($data['orderNo']), Crypt::encrypt($data['status'])]));
    }

    public function handle()
    {
        try {
            $pending = All::GetAllRow('et_fpx_log', 'fpx_status', 'PENDING_APPROVAL');
            $filtered = $pending->filter(function ($item, $key) {
                return strpos($item->fpx_order_no, 'SPS') === 0;
            });
            Log::info("Fpx Cron Filter". json_encode($filtered));
            foreach ($filtered as $f) {
                $orderNo = $f->fpx_order_no;
                // $orderNo = 'SPS24022779336';
                $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
                $data = array("payload" => array(
                    "subsysId"      => "SPS",
                    "password"      => "e_TemPahan@@2024!",
                    "orderNo"       => $orderNo,
                ));
        
                $postdata = json_encode($data);
        
                $ch = curl_init($url); 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $result = curl_exec($ch);
                $response = json_decode($result, true);
                $responseArray[] = $response;
                Log::info("Fpx Cron Response: ". json_encode($response));
                if(!isset($response['isError'])) {
                    $payload = $response['payload'];
                    if(is_array($payload)) {
                        $orderNo = $payload['orderNo'];
                        $status = $payload['status'];
                        $txnReference = $payload['sellerTxnTime'];
                        $txnId = $payload['txnId'];
                        $txnTime = $payload['txnTime'];
                        $amount = $payload['amount'];
                        $bankname = $payload['bankName'];
                        $fpxvalue = $payload['ppjTransNo'];
                        $approvalCode = $fpxvalue . 'AC' . $txnId;
                    }
                    if($payload['status'] == 'SUCCESS') {
                        if (strlen($orderNo) == 18) {
                            $newOrderNo = substr($orderNo, 0, 14);
                        } 
                        Log::info("Fpx Cron Order No". json_encode($orderNo));
                        $sapDocNo = "";
                        $main = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
                        $id = All::GetRow('et_payment_fpx', 'fk_main_booking', $main->id);
                        $et_booking_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $main->id);
                        $fk_lkp_location = $main->fk_lkp_location;
                        $efd_fee_code = All::GetAllRow('et_facility_detail', 'fk_et_facility_type', $et_booking_facility->fk_et_facility_type)[0]->efd_fee_code;
                        $user = All::GetRow('user_profiles', 'fk_users', $main->fk_users);   
                        $user->fk_lkp_state = HP::negeri($user->fk_lkp_state);
                        $user->fk_lkp_country = HP::negara($user->fk_lkp_country);
                        $data['trans'] = (object)[
                            'txnTime'   => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
                            'amount'    => $amount,
                            'feecode'   => $efd_fee_code,
                            'fpxNum'    => $approvalCode,
                            'approvalCode' => $txnId
                        ];

                        $data_main = array(
                            'fk_lkp_status'             => 5,
                            'updated_at'                => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdate('main_booking', $main->id, $data_main);

                        $dataArr = [
                            'fpx_status'    => 'SUCCESS',
                            'fpx_txn_id'    => $fpxvalue,
                            'updated_at'    => date('Y-m-d'),
                        ];
                        All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);

                        // $dataBP = BapiController::check_bp($user->bud_reference_id);
                        // $checkbpstatus = $dataBP;
                        // if($dataBP[0] == null || $dataBP[1] == null) {
                        //     $dataBP = BapiController::create_bp($user);
                        // } 
                        // $sap_doc_no = BapiController::generate_bill($dataBP, $user);
                        // $adhocResponse = BapiController::adhoc($sap_doc_no, $user, $data['trans']);
                        


                        $data_fpx = array(
                            'bank'              => $bankname,
                            'sap_doc_no'        => $adhocResponse[0],
                            'fpx_serial_no'     => $txnId,
                            'fpx_status'        => 1,
                            'fpx_date'          => date('Y-m-d 00:00:00'),    
                            'fpx_trans_date'    => $txnTime,    
                            'amount_paid'       => $amount,
                            'created_at'        => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data_fpx);
                        
                        $data_fpx = array(
                            'fk_main_booking'       => $main->id,
                            'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                            'ecb_date_booking'      => $et_booking_facility->ebf_start_date,
                            'ecb_flag_indicator'    => 1,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        $query = All::InsertGetID('et_confirm_booking', $data_fpx);
                        
                        $data_fpx = array(
                            'et_confirm_booking'    => $query,
                            // 'fk_et_facility_detail' => $,
                            // 'fk_et_slot_time'       => 1,
                            'ecbd_date_booking'     => date('Y-m-d'),
                            'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        All::Insert('et_confirm_booking', $data_fpx);

                    } else if($payload['status'] == 'FAILED') {
                        $dataArr = [
                            'fpx_status'    => 'FAILED',
                            'updated_at'    => date('Y-m-d'),
                        ];
                        All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                    }
                }
            }
        } catch (\Throwable $th) {
            Log::info($th);
        }
    }
    
    public function query(Request $request) {
        $pending = All::GetAllRow('et_fpx_log', 'fpx_status', 'PENDING_APPROVAL');
        $filtered = $pending->filter(function ($item, $key) {
            return strpos($item->fpx_order_no, 'SPS') === 0;
        });
        foreach ($filtered as $f) {
            $orderNo = $f->fpx_order_no;
            $orderNo = 'SPS240306793910001';
            $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
            $data = array("payload" => array(
                "subsysId"      => "SPS",
                "password"      => "e_TemPahan@@2024!",
                "orderNo"       => $orderNo,
            ));
    
            $postdata = json_encode($data);
    
            $ch = curl_init($url); 
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $result = curl_exec($ch);
            $response = json_decode($result, true);
            $responseArray[] = $response;
            if(!isset($response['isError'])) {
                $payload = $response['payload'];
                if($payload['status'] == 'SUCCESS') {
                    $dataArr = [
                        'fpx_status'    => 'SUCCESS',
                        'updated_at'    => date('Y-m-d'),
                    ];
                    All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                } else if($payload['status'] == 'FAILED') {
                    $dataArr = [
                        'fpx_status'    => 'FAILED',
                        'updated_at'    => date('Y-m-d'),
                    ];
                    All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                }
            } else {
                Log::info("Fpx Query Error: ". json_encode($response));
            }
        }
        // $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
        // $orderNo = 'SPS24022279319';
        // $data = array("payload" => array(
        //     "subsysId"      => "SPS",
        //     "password"      => "5pS!!@2023pY",
        //     "orderNo"       => $orderNo,
        // ));

        // $postdata = json_encode($data);

        // $ch = curl_init($url); 
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        // $result = curl_exec($ch);
        // $response = json_decode($result, true);
        // $responseArray[] = $response;
        // $payload = $response['payload'];
        // if($payload['status'] == 'SUCCESS') {
        //     $dataArr = [
        //         'fpx_status'    => 'SUCCESS',
        //         'updated_at'    => date('Y-m-d'),
        //     ];
        //     All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
        // } else if($payload['status'] == 'FAILED') {
        //     $dataArr = [
        //         'fpx_status'    => 'FAILED',
        //         'updated_at'    => date('Y-m-d'),
        //     ];
        //     All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
        // }
    }

    public function curl(Request $request) {
        
        // $successNoSap = DB::table('et_fpx_log')
        //     ->where('fpx_status', 'SUCCESS')
        //     ->where('sap_doc_no', null)
        //     ->get();
        // foreach ($successNoSap as $key => $value) {
        // $data1 = array("payload" => array(
        //     "subsysId"  => "SPT",
        //     "password"  => "SPt!!@2022",
        //     "orderNo"   => $data['oderNo'],
        //     "description" => $data['oderNo'],
        //     "txnTime" => date('Y-m-d H:i:s'),
        //     "amount"  => $data['total']
        // ));
        //     $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
        //     $data = array("payload" => array(
        //         "subsysId"      => "SPM",
        //         // "password"      => "SpM@2023!!!",    // DEV
        //         "password"      => "SPm@@2024!!PMPJ",   // PROD
        //         "orderNo"       => $value->fpx_order_no,
        //     ));
        // }
    }

}

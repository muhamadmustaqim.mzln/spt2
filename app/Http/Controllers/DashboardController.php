<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\MainBooking;
use App\Models\All;
use App\Models\Sport;
use App\Models\Event;
use App\Models\Hall;
use App\Helpers\Helper as HP;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function home() {
        $data['banner'] = array_values(All::show('bb_banner')->where('bb_status', 1)->where('bb_expired_date', '>=', date('Y-m-d'))->sortBy('bb_order')->toArray());
        // $data['banner'] = All::show('bb_banner')
        //     ->where('bb_status', 1)
        //     ->where(function ($query) {
        //         $query->where('bb_expired_date', '>=', date('Y-m-d'))
        //             ->orWhereNull('bb_expired_date');
        //     })
        //     ->orderBy('bb_order')
        //     ->get()
        //     ->toArray();

        // dd(Session::get('user'));
        return view('home', compact('data'));
    }

    public function manualPengguna() {
        
    }
    
    public function sport() {
        if(Session::get('user') == null) {
            return redirect('auth');
        }
        $data['laporan_keseluruhan'] = Sport::laporan_keseluruhan();
        // $data['totalRecords'] = Sport::laporan_keseluruhanEloquent()['totalRecords'];    // Eloquent
        $data['laporan_dalaman'] = Sport::laporan_dalaman();
        $data['laporan_bayaran'] = Sport::laporan_bayaran();
        $data['laporan_kelulusan'] = Sport::laporan_kelulusan();
        $data['laporan_harian'] = Sport::laporan_harian();
        $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
        $data['laporan_tindih'] = Sport::duplicate_all();
        // dd(count($data['laporan_keseluruhan']));
        return view('dashboard.sport', compact('data'));
    }

    public function report_my(){
        $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);

        return view('dashboard.report.my', compact('data'));
    }

    public function my(){
        if (!Session::get('user')) {
            return redirect('/auth');
        }
        $data['laporan_user_hall'] = Hall::laporan_user(Session::get('user')['id']);
        $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
        $data['laporan_user_acara'] = Event::laporan_user(Session::get('user')['id']);
        
        return view('dashboard.my', compact('data'));
    }
    
    public function search(Request $request)
    {
        $data['laporan_user'] = Sport::laporan_keseluruhan();
        $query = $request->input('query');
    
    
        return view('dashboard.report.all', compact('data'));
    }
    
    public function report_keseluruhan(Request $request){
        $flashData = Session::get('flash');
        
        $data['all'] = Sport::laporan_keseluruhan();

        return view('dashboard.report.all', compact('data'));
        // dd($flashData);
        // $search = 'SPS';
        // if($request->isMethod('post')){
        //     $searchTerm = $request->input('searchTerm');
        //     if($searchTerm == '') {
        //         $searchTerm = 'SPS';
        //     }
        //     $data['test'] = MainBooking::where('bmb_booking_no', 'like', '%' . $searchTerm . '%')->where('fk_lkp_status', '!=', 1)->orderBy('id', 'DESC')->get();
        //     foreach ($data['test'] as $key => $value) {
        //         $value['encryptedId'] = Crypt::encrypt($value['id']);
        //         $value['nama_pemohon'] = strtoupper(HP::get_nama($value['fk_users']));
        //         $value['no_tel'] = HP::get_no($value['fk_users']);
        //         $value['nama_kemudahan'] = HP::get_kemudahan($value['id']);
        //         $value['lokasi'] = HP::location($value['fk_lkp_location']);
        //         $value['status'] = HP::get_status_tempahan($value['fk_lkp_status']);
        //     }
        //     return response()->json($data);
        // } else {
        //     // $data['test'] = MainBooking::where('bmb_booking_no', 'LIKE', "%{$search}%")->orderBy('id', 'DESC')->paginate(10);
        //     $data['test'] = MainBooking::where('bmb_booking_no', 'LIKE', "%{$search}%")->where('deleted_at', null)->where('fk_lkp_status', '!=', 1)->orderBy('id', 'DESC')->paginate(10);
        //     // $data['test'] = All::GetAllRowLike('main_booking', 'bmb_booking_no', 'SPS')->where('deleted_at', null)->where('fk_lkp_status', '!=', 1);
        // }
    }

    public function report_dalaman(){
        $data['laporan_user'] = Sport::laporan_dalaman();
        
        return view('dashboard.report.dalaman', compact('data'));
    }

    public function report_bayaran(){
        $data['laporan_user'] = Sport::laporan_bayaran();
        return view('dashboard.report.bayaran', compact('data'));
    }

    public function report_kelulusan(){
        $data['laporan_user'] = Sport::laporan_kelulusan();

        return view('dashboard.report.kelulusan', compact('data'));
    }

    public function report_harian(){
        $data['laporan_user'] = Sport::laporan_harian();
        return view('dashboard.report.harian', compact('data'));
    }

    public function report_tindih(){
        $data['laporan_user'] = Sport::duplicate_all();
        return view('dashboard.report.tindih', compact('data'));
    }

    public function top_pengguna()
    {
        $data = Sport::laporan_top_pengguna();
        $arr = array();
        foreach($data as $a){
            $arr[] =  array(
                'x' => $a->fullname,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    public function top_tempahan()
    {
        $data = Sport::laporan_top_tempahan();
        $arr = array();
        foreach($data as $a){
            $nama = $a->eft_type_desc .' , '. $a->lc_description;
            $arr[] =  array(
                'x' => $nama,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    // public function pengesahan(Request $request, $id) {
    //     $data['etbooking'] = Crypt::decrypt($id);
    //     $data['main'] = All::GetRow('main_booking', 'id', $data['etbooking']);
    //     $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
    //     $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);
    //     $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['etbooking']);
    //     foreach ($data['bh_booking'] as $s) {
    //         $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
    //     }
    //     // $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
    //     $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
    //     $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
    //     // $data['slot'] = Hall::priceSlot($data['booking']);
    //     $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
    //     $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
    //     $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
    //     $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);

    //     if(request()->isMethod('post')){
    //         $caraBayaran = $request->post('carabayaran');
    //         if($caraBayaran == 4) {     // FPX
    //             $updateMainBooking = [
    //                 'fk_lkp_status'     => 2
    //             ];
    //             $query = All::GetUpdate('main_booking', $data['booking'], $updateMainBooking);
    //             if ($query) {
    //                 Session::flash('flash', 'Success'); 
    //                 return redirect()->back();
    //             }else{
    //                 Session::flash('flash', 'Failed'); 
    //                 return redirect()->back();
    //             }
    //         } else if($caraBayaran) {   // LO/PO
    //             // dd('lopo');
    //             Session::flash('flash', 'An error occured.'); 
    //             return redirect()->back();
    //         }
    //     }

    //     return view('hall.dashboard.pengesahan', compact('data'));
    // }
    
    public function pengesahan(Request $request, $id) {
        $data['etbooking'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['etbooking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['ebfd'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['ebf']->pluck('id'));

        $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['eht'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['ehb']->pluck('id'));

        $data['slot'] = Sport::priceSlot2($data['etbooking']);

        $data['kelengkapan'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['etbooking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['etbooking'], 'id', 'DESC');
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);

        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);

        if(request()->isMethod('post')){
            $data['eqd'] = All::GetRow('et_quotation_detail', 'fk_bh_quotation', $data['bh_quotation']->id);
            $data['ld'] = All::GetAllRow('lkp_deposit', 'fk_main_booking', $data['main']->id)->where('status', 0)->first();
            if($data['ld']){
                $update_deposit_amount = [
                    'deposit_amount'        => request()->jumlahBayaran
                ];
                $query = All::GetUpdate('lkp_deposit', $data['ld']->id, $update_deposit_amount);

                // API to deposit refund system
                $api_data = [
                    'name'                  => $data['ld']->name,
                    'ref_id'                => $data['ld']->ref_id,
                    'email'                 => $data['ld']->email,
                    'mobile_no'             => $data['ld']->mobile_no,
                    'bank_name'             => $data['ld']->bank_name,
                    'bank_account'          => $data['ld']->bank_account,
                    'subsystem'             => $data['ld']->subsystem,
                    'subsystem_booking_no'  => $data['main']->bmb_booking_no,
                    'status'                => 1,
                    'deposit_amount'        => request()->jumlahBayaran,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                ];
                $api = All::InsertGetID('deposit_list', $api_data);
            }
            
            $jenis_bayaran      = request()->jenis_bayaran;
            $caraBayaran        = $request->post('carabayaran');

            // $genid = Sport::getId();
            $no = Sport::generatereceipt();

            if($jenis_bayaran == 1){
                $updateBP = [
                    'fk_lkp_payment_type'   => $jenis_bayaran,
                    'fk_lkp_payment_mode'   => $caraBayaran,
                    'fk_main_booking'       => $data['main']->id,
                    'fk_users'              => $data['main']->fk_users,
                    'fk_bh_quotation'       => $data['bh_quotation']->id,
    
                    'bp_total_amount'       => $data['main']->bmb_total_book_hall,
                    'bp_deposit'            => $data['main']->bmb_deposit_rounding,
                    'bp_paid_amount'        => request()->jumlahBayaran,
                    'amount_received'       => request()->jumlahBayaran,
                    'bp_subtotal'           => request()->jumlahBayaran,
    
                    'bp_payment_ref_no'     => request()->rujukanBayaran,
                    'bp_receipt_number'     => sprintf('%07d', $no),
                    'no_lopo'               => request()->lopo,
                    'no_cek'                => request()->no_cek,
                    'nama_bank'             => request()->nama_bank,
                    'bp_payment_status'     => 1,
    
                    'bp_receipt_date'       => date('Y-m-d H:i:s'),
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                $BPid = All::InsertGetID('bh_payment', $updateBP);
    
                $updateEPD = [
                    'fk_bh_payment'         => $BPid,
                    // 'fk_lkp_gst_rate'       => ,
                    'fk_et_facility_detail' => $data['ebf'][0]->fk_et_facility_detail,
                    // 'fk_et_equipment'       => ,
                    'fk_et_booking_facility'=> $data['ebf'][0]->id,
                    'product_indicator'     => 1,
                    'booking_date'          => date('Y-m-d'),
                    'unit_price'            => $data['main']->bmb_total_book_hall,
                    'quantity'              => 1,
                    'total_amount'          => $data['main']->bmb_total_book_hall,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id'),
                    // 'code_gst'              => ,
                    // 'gst_amount'            => ,
                ];
                $query = All::Insert('et_payment_detail', $updateEPD);

                $fk_lkp_status = 4;
                $message = "Pembayaran Deposit Tempahan: ".$data['main']->bmb_booking_no." berjaya.";
            } else {
                // Hall Payment::start
                $updateBP = [
                    'fk_lkp_payment_type'   => $jenis_bayaran,
                    'fk_lkp_payment_mode'   => $caraBayaran,
                    'fk_main_booking'       => $data['main']->id,
                    'fk_users'              => $data['main']->fk_users,
                    'fk_bh_quotation'       => $data['bh_quotation']->id,
    
                    'bp_total_amount'       => $data['main']->bmb_total_book_hall,
                    'bp_deposit'            => $data['main']->bmb_deposit_rounding,
                    'bp_paid_amount'        => $data['main']->bmb_total_book_hall,
                    'amount_received'       => $data['main']->bmb_total_book_hall,
                    'bp_subtotal'           => $data['main']->bmb_total_book_hall,
    
                    'bp_payment_ref_no'     => request()->rujukanBayaran,
                    'bp_receipt_number'     => sprintf('%07d', $no),
                    'no_lopo'               => request()->lopo,
                    'no_cek'                => request()->no_cek,
                    'nama_bank'             => request()->nama_bank,
                    'bp_payment_status'     => 1,
    
                    'bp_receipt_date'       => date('Y-m-d H:i:s'),
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                $BPid = All::InsertGetID('bh_payment', $updateBP);
    
                $updateEPD = [
                    'fk_bh_payment'         => $BPid,
                    // 'fk_lkp_gst_rate'       => ,
                    'fk_et_facility_detail' => $data['ebf'][0]->fk_et_facility_detail,
                    // 'fk_et_equipment'       => ,
                    'fk_et_booking_facility'=> $data['ebf'][0]->id,
                    'product_indicator'     => 1,
                    'booking_date'          => date('Y-m-d'),
                    'unit_price'            => $data['main']->bmb_total_book_hall,
                    'quantity'              => 1,
                    'total_amount'          => $data['main']->bmb_total_book_hall,
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d'),
                    'updated_by'            => Session::get('user.id'),
                    // 'code_gst'              => ,
                    // 'gst_amount'            => ,
                ];
                $query = All::Insert('et_payment_detail', $updateEPD);
                // Hall Payment::end

                // Equipment Payment::start
                $updateBPeq = [
                    'fk_lkp_payment_type'   => $jenis_bayaran,
                    'fk_lkp_payment_mode'   => $caraBayaran,
                    'fk_main_booking'       => $data['main']->id,
                    'fk_users'              => $data['main']->fk_users,
                    'fk_bh_quotation'       => $data['bh_quotation']->id,
    
                    'bp_total_amount'       => $data['main']->bmb_total_book_hall,
                    'bp_deposit'            => $data['main']->bmb_deposit_rounding,
                    'bp_paid_amount'        => $data['main']->bmb_total_equipment,
                    'amount_received'       => $data['main']->bmb_total_equipment,
                    'bp_subtotal'           => $data['main']->bmb_total_equipment,
    
                    'bp_payment_ref_no'     => request()->rujukanBayaran,
                    'bp_receipt_number'     => sprintf('%07d', $no),
                    'no_lopo'               => request()->lopo,
                    'no_cek'                => request()->no_cek,
                    'nama_bank'             => request()->nama_bank,
                    'bp_payment_status'     => 1,
    
                    'bp_receipt_date'       => date('Y-m-d H:i:s'),
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d'),
                    'updated_by'            => Session::get('user.id')
                ];
                $BPideq = All::InsertGetID('bh_payment', $updateBPeq);
    
                foreach ($data['kelengkapan'] as $key => $val) {
                    $updateEPDeq = [
                        'fk_bh_payment'         => $BPideq,
                        'fk_lkp_gst_rate'       => $val->fk_lkp_gst_rate,
                        'fk_et_facility_detail' => $data['ebf'][0]->fk_et_facility_detail,
                        'fk_et_equipment'       => $val->fk_et_equipment,
                        'fk_et_booking_facility'=> $data['ebf'][0]->id,
                        'product_indicator'     => 1,
                        'booking_date'          => date('Y-m-d'),
                        'unit_price'            => $val->eeb_unit_price,
                        'quantity'              => $val->eeb_quantity,
                        'code_gst'              => $val->fk_lkp_gst_rate,
                        'gst_amount'            => $val->eeb_gst_rm,
                        'total_amount'          => $val->eeb_subtotal,
                        'created_at'            => date('Y-m-d'),
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => Session::get('user.id'),
                    ];
                    $query = All::Insert('et_payment_detail', $updateEPDeq);
                }
                // Equipment Payment::end

                // et_confirm_booking
                $updateConfirmBooking = [
                    'fk_main_booking'       => $data['main']->id,
                    'fk_et_facility_type'   => $data['ebf'][0]->fk_et_facility_type,
                    'ecb_date_booking'      => $data['ebf'][0]->ebf_start_date,
                    'ecb_flag_indicator'    => 2,
                    'updated_by'            => Session::get('user')['id'],
                    'updated_at'            => date('Y-m-d H:i:s')
                ];
                $ecbid = All::InsertGetID('et_confirm_booking', $updateConfirmBooking);

                // et_confirm_booking_detail
                foreach ($data['eht'] as $key => $value) {
                    $updateConfirmBookingDetail = [
                        'fk_et_confirm_booking' => $ecbid,
                        'fk_et_facility_detail' => $data['ebf'][0]->fk_et_facility_detail,
                        'fk_et_slot_time'       => $value->fk_et_slot_time,
                        'ecbd_date_booking'     => $data['ebf'][0]->ebf_start_date,
                        'fk_et_facility_type'   => $data['ebf'][0]->fk_et_facility_type,
                        'updated_by'            => Session::get('user')['id'],
                        'updated_at'            => date('Y-m-d H:i:s')
                    ];
                    $query = All::InsertGetID('et_confirm_booking_detail', $updateConfirmBookingDetail);
                }

                if($data['main']->internal_indi == 1){
                    $fk_lkp_status = 11;
                } else{
                    $fk_lkp_status = 5;
                }
                $message = "Pembayaran Penuh Tempahan: ".$data['main']->bmb_booking_no." berjaya.";
            }

            $updateMainBooking = [
                'fk_lkp_status'     => $fk_lkp_status,
                'updated_at'        => date('Y-m-d'),
                'updated_by'        => Session::get('user.id')
            ];
            $query = All::GetUpdate('main_booking', $data['main']->id, $updateMainBooking);
            
            if ($query) {
                Session::flash('flash', $message);
                return redirect('/sport/dashboard/report_keseluruhan');
            }else{
                Session::flash('flash', 'Failed'); 
                return redirect()->back();
            }
        }

        $data['depoStatus'] = 0;
        $data['paid'] = 0;
        foreach ($data['kaunter'] as $key => $value) {
            $data['paid'] += $value->bp_paid_amount;
        }
        if($data['main']->bmb_deposit_rm > 0 && $data['main']->fk_lkp_status == 2){
            $data['depoStatus'] = 1;
            $data['payNeeded'] = $data['main']->bmb_deposit_rm;
        } else {
            $data['payNeeded'] = $data['main']->bmb_subtotal;
        }
        // dd($data);

        return view('sport.dashboard.pengesahan', compact('data'));
    }
    
    public function approval(Request $request, $id) {
        $data['mbid'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['mbid']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);

        $data['date'] = $data['main']->bmb_booking_date;
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);

        $data['payment_mode'] = All::Show('lkp_payment_mode')->where('id', '!=', 4);

        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mbid']);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));

        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['et_hall_time'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['et_hall_book']->pluck('id'))->whereNull('deleted_at');
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility'][0]->fk_et_facility_type);

        $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'))->whereNull('deleted_at');

        if(request()->isMethod('post')){
            $type = request()->type;
            if($type == 1) {
                if($request->hasFile('fail')){
                    $path = public_path('/upload_document/main_booking/' . $data['mbid']);
                    File::ensureDirectoryExists($path);
                    $image1 = $request->file('fail');
                    $filename = $image1->getClientOriginalName();
                    $request->file('fail')->move($path, $filename);

                    $filePath = $path . '/' . $filename;
                    $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $fileType = mime_content_type($filePath); 
                    $fileSize = filesize($filePath);

                    $file_data = array(
                        'fk_main_booking'   => $data['mbid'],
                        'ba_date'           => date('Y-m-d'),
                        'ba_dir'            => 'upload_document',
                        'ba_full_path'      => $path,
                        'ba_file_name'      => $filename,
                        'ba_file_ext'       => $file_ext,
                        'ba_file_size'      => $fileSize,
                        // 'ba_generated_name' => 
                        // 'ba_status'         => 
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                        'updated_by'        => Session::get('user')['id'],
                    );
                    $query = All::Insert('bh_attachment', $file_data);

                    return redirect()->back();
                } else {
                    return redirect()->back();
                }
            } else {
                if($data['main']->internal_indi){
                    
                }
                $tindakankelulusan = request()->tindakan;
                if($tindakankelulusan == 1){
                    $update_main_booking = [
                        'fk_lkp_status'     => 2,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', $data['mbid'], $update_main_booking);

                    return redirect('/sport/maklumatbayaran/'. Crypt::encrypt($data['mbid']));
                } else {
                    $update_main_booking = [
                        'fk_lkp_status'     => 14,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', $data['mbid'], $update_main_booking);
                }
            }

            // $caraBayaran = $request->post('carabayaran');
            // if($caraBayaran == 4) {     // FPX
            //     $updateMainBooking = [
            //         'fk_lkp_status'     => 2
            //     ];
            //     $query = All::GetUpdate('main_booking', $data['booking'], $updateMainBooking);
            //     if ($query) {
            //         Session::flash('flash', 'Success'); 
            //         return redirect()->back();
            //     }else{
            //         Session::flash('flash', 'Failed'); 
            //         return redirect()->back();
            //     }
            // } else if($caraBayaran) {   // LO/PO
            //     // dd('lopo');
            //     Session::flash('flash', 'An error occured.'); 
            //     return redirect()->back();
            // }
        }
        $data['bh_attachment'] = All::GetAllRow('bh_attachment', 'fk_main_booking', $data['mbid'])->where('deleted_at', NULL);
        // dd($data);

        return view('sport.dashboard.approval', compact('data'));
    }

    public function removefile($id){
        $id = Crypt::decrypt($id);

        $deleteFile = [
            'deleted_at'    => date('Y-m-d H:i:s')
        ];
        $query = All::GetUpdateSpec('bh_attachment', 'fk_main_booking', $id, $deleteFile);

        return redirect()->back();
    }
}

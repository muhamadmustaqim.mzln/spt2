<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Models\All;
use App\Models\Event;
use App\Models\Sport;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;
use Carbon\Carbon;
use DateTime;

class EventController extends Controller
{
    public function carianSekatan(Request $request){
        $data = Event::SekatanHari(request()->location_id);

        return json_encode($data);
    }

    public function lists(Request $request) {
        $data['fasility'] = Event::fasility();
        $collection = collect($data['fasility']);
        $data['fasility'] = $collection->map(function ($item) {
            $item->encrypt = Crypt::encrypt($item->id);
            return $item;
        });
        $data['carian'] = null;
        // $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['location'] = Event::locationPictures();
        $data['event'] = Event::event();
        $collection = collect($data['event']);
        $data['event'] = $collection->map(function ($item) {
            $item->encrypt = Crypt::encrypt($item->id);
            return $item;
        });
        $data['start'] = "";
        $data['end'] = "";
        $data['pengumuman'] = all::Show('bh_announcement', 'created_at', 'DESC')->where('ba_type', 1)->where('ba_status', 1);
        $data['kadarTapak'] = Event::kadarTapak();
        // $data['kadarTapak'] = Event::rateTapak();
        // $data['kadarTapak'] = $data['kadarTapak']->groupBy('category');
        // dd($data['kadarTapak']);

        if(request()->isMethod('post')){
            $lokasi = $request->post('lokasi');
            $tarikhMula = $request->post('tarikhMula');
            $tarikhAkhir = $request->post('tarikhAkhir');

            $data['carian'] = (object) array(
                'lokasi'        => $lokasi,
                'tarikhMula'    => $tarikhMula,
                'tarikhAkhir'   => $tarikhAkhir
            );
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = Event::event($data['id']);
            $collection = collect($data['event']);
            $data['fasility'] = $collection->map(function ($item) {
                return $item;
            });

            $encryptedId = Crypt::encrypt($lokasi);
            $encryptedTarikhMula = Crypt::encrypt($tarikhMula);
            $encryptedTarikhAkhir = Crypt::encrypt($tarikhAkhir);
            
            return redirect()->route('event.details', [
                'id' => $encryptedId,
                'tarikhMula' => $encryptedTarikhMula,
                'tarikhAkhir' => $encryptedTarikhAkhir,
            ]);
        }

        // dd($data);
        return view('event.public.lists', compact('data'));
    }

    public function details(Request $request, $id, $tarikhMula = null, $tarikhAkhir = null) {
        $id = Crypt::decrypt($id);
        $data = [];
        if ($tarikhMula != null || $tarikhMula != null) { 
            $tarikhMula = Crypt::decrypt($tarikhMula); 
            $tarikhAkhir = Crypt::decrypt($tarikhAkhir); 
            $data['carian'] = (object) array(
                'lokasi'        => $id,
                'tarikhMula'    => $tarikhMula,
                'tarikhAkhir'   => $tarikhAkhir
            );
            $data['date'] = $data['carian']->tarikhMula.'/'.$data['carian']->tarikhAkhir;
        } else {
            // $data['carian'] = null;
            $data['carian'] = (object) array(
                'lokasi'        => $id,
                'tarikhMula'    => date("Y-m-d"),
                'tarikhAkhir'   => date("Y-m-d")
            );
            $data['date'] = date("d-m-Y").'/'.date("d-m-Y");
        }
        $data['id'] = $id;
        $data['event'] = Event::eventById($id);
        $data['event']->deposit = $data['event']->pluck('deposit');
        $data['location1'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['fasility1'] = All::GetRow('spa_location', 'id', $id);
        $data['fasility2'] = Event::locationPictures($id)->first();
        $data['others1'] = Event::locationPictures()->where('id', '!=' , $id)->where('status', 1);

        $data['location'] = All::Show('bh_hall', 'id', 'ASC')->where('bh_status', 1);
        $data['fasility'] = All::GetRow('bh_hall', 'id', 1);
        // $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('id', '!=' , $id)->where('id', '!=' , $id)->where('bh_status', 1);
        $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('id', '!=' , 3)->where('id', '!=' , 3)->where('bh_status', 1);
        $data['price'] = All::Show('bh_hall_price', 'id', 'ASC')->where('fk_bh_hall', $id)->where('bhp_status', 1);

        $data['start'] = "";
        $data['end'] = "";
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        $data['form'] = 1;
        $data['formPemohon'] = null;
        $data['formAcara'] = null;
        $data['formPenganjurUtama'] = null;

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $dates = explode('/', $data['date']);
            $data['carian']->tarikhMula = request()->tarikhMula;
            $data['carian']->tarikhAkhir = request()->tarikhAkhir;
            // $data['carian'] = is_object($data['carian']) ? $data['carian'] : new \stdClass();
            // if (count($dates) === 2) {
            //     $data['carian']->tarikhMula = $dates[0];
            //     $data['carian']->tarikhAkhir = $dates[1];
            // } else {
            //     $data['carian']->tarikhMula = $dates[0];
            //     $data['carian']->tarikhAkhir = $dates[0];
            // }
            $data['fasility1'] = All::GetRow('spa_location', 'id', $request->post('lokasi'));
            // $data['fasility'] = All::GetRow('bh_hall', 'id', $data['id']);
            // $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('bh_status', 1);
            // $data['price'] = All::Show('bh_hall_price', 'id', 'ASC')->where('fk_bh_hall', $data['id'])->where('bhp_status', 1);
        }

        // $data['booked_date'] = json_encode(Event::sekatanHari($data['id'], $data['carian'])->pluck('date_booking')->unique());
        $data['booked_date'] = json_encode(Event::SekatanHari($id));
        $data['booked_date1'] = json_encode(Event::sekatanHariSemuaLokasi());
        // dd($data['booked_date1']);
        // dd($data, $id, Event::SekatanHari($id));
        
        return view('event.public.details_temp', compact('data'));
    }

    public function reservation(Request $request, $id, $tarikhMula = null, $tarikhAkhir = null) {
        $id = Crypt::decrypt($id);
        $tarikhMula = ($tarikhMula === "null") ? null : $tarikhMula;
        $tarikhAkhir = ($tarikhAkhir === "null") ? null : $tarikhAkhir;
        if ($tarikhMula != null || $tarikhMula != null) { 
            $tarikhMula = Crypt::decrypt($tarikhMula); 
            $tarikhAkhir = Crypt::decrypt($tarikhAkhir); 
            $startDate = new DateTime($tarikhMula);
            $endDate = new DateTime($tarikhAkhir);
            $interval = $startDate->diff($endDate);
            // $numberOfDays = $interval->days + 1;
            $data['carian'] = (object) array(
                'lokasi'        => $id,
                'tarikhMula'    => $tarikhMula,
                'tarikhAkhir'   => $tarikhAkhir
            );
            $data['date'] = $data['carian']->tarikhMula.'/'.$data['carian']->tarikhAkhir;
        } else {
            $data['carian'] = (object) array(
                'lokasi'        => $id,
                'tarikhMula'    => null,
                'tarikhAkhir'   => null
            );
            $data['date'] = date("Y-m-d");
        }
        $data['id'] = $id;
        $data['event'] = Event::locationPictures($id)->first();
        // $data['event'] = Event::eventById($id);
        // $data['event']->deposit = $data['event']->pluck('deposit');
        // $data['event']->rent_perday = $data['event']->pluck('rent_perday');
        // dd($data);   
        $data['location1'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['fasility1'] = All::GetRow('spa_location', 'id', $id);
        $data['others1'] = All::ShowAll('spa_location', 'id', 'ASC')->where('id', '!=' , $id)->where('status', 1);
        $data['location'] = All::Show('bh_hall', 'id', 'ASC')->where('bh_status', 1);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['start'] = "";
        $data['end'] = "";
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        $data['form'] = 1;
        $data['formPemohon'] = null;
        $data['formAcara'] = null;
        $data['formPenganjurUtama'] = null;
        $data['total_type'] = 0;

        $data['config_depoForm'] = All::GetAllRow('lkp_configuration', 'id', 2)->first()->status;
        
        if(request()->isMethod('post')){
            $running =  Event::getRn();
            $rn = sprintf( '%05d', $running);
            $year = date('y');
            $md = date('md');
            $bookrn='SPA'.$year.$md.$rn;

            try {
                $main_bookingData = array(
                    'fk_users'                  => Session::get('user')['id'],
                    'fk_lkp_status'             => 19,
                    'bmb_booking_no'            => $bookrn,
                    'bmb_type_user'             => 1,
                    'bmb_booking_date'          => date('Y-m-d H:i:s'),
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );
                $main_bookingId = All::InsertGetID('main_booking', $main_bookingData);
                // $main_bookingId = 1;

                if($data['config_depoForm'] == 1){
                    $data_bank = [
                        'name'                  => request()->namaPenuh,
                        'ref_id'                => request()->kadPengenalan,
                        'email'                 => request()->emel,
                        'mobile_no'             => request()->phoneNo,
                        'bank_name'             => request()->namaBank,
                        'bank_account'          => request()->akaunBank,
                        'fk_main_booking'       => $main_bookingId,
                        'status'                => 0,
                        // 'deposit_amount'        => ,
                        'subsystem'             => 'SPT',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ];
                    $bankId = All::InsertGetID('lkp_deposit', $data_bank);
                }

                $contactPerson = '';
                $nogst = '';
                if ($request->post('statusPemohon') == 2) {
                    $contactPerson = $request->post('contactPerson');
                    $nogst = $request->post('nogst');
                    $spa_booking_organiserData = array(
                        'fk_main_booking'           => $main_bookingId,
                        'fk_lkp_state'              => $request->post('negeriAgensi'),
                        'fk_lkp_country'            => $request->post('negaraAgensi'),
                        'agency_name'               => $request->post('namaAgensi'),
                        'address'                   => $request->post('alamatAgensi'),
                        'city'                      => $request->post('bandarAgensi'),
                        'postcode'                  => $request->post('poskodAgensi'),
                        'telephone_no'              => $request->post('telBimbitAgensi'),
                        'created_at'                => date('Y-m-d H:i:s'),
                        'updated_at'                => date('Y-m-d H:i:s'),
                    );        
                    All::InsertGetID('spa_booking_organiser', $spa_booking_organiserData);
                }
                $spa_booking_personData = array(
                    'fk_main_booking'           => $main_bookingId,
                    'name'                      => $request->post('namaPemohon'),
                    'post'                      => $request->post('jawatan'),
                    'address'                   => $request->post('alamat'),
                    'postcode'                  => $request->post('poskod'),
                    'city'                      => $request->post('bandar'),
                    'fk_lkp_state'              => $request->post('negeri'),
                    'fk_lkp_country'            => $request->post('negara'),
                    'ic_no'                     => $request->post('kadPengenalanPendaftaran'),
                    'office_no'                 => $request->post('noPejabat'),
                    'hp_no'                     => $request->post('noBimbit'),
                    'fax_no'                    => $request->post('noFaks') ?? '',
                    'email'                     => $request->post('email'),
                    'applicant_status'          => $request->post('statusPemohon'),
                    'applicant_type'            => $request->post('jenisPemohon'),
                    'contact_person'            => $contactPerson,
                    'GST_no'                    => $nogst,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                );        
                All::InsertGetID('spa_booking_person', $spa_booking_personData);

                $spa_booking_eventData = array(
                    'fk_main_booking'           => $main_bookingId,
                    'fk_spa_location'           => $id,
                    'event_name'                => $request->post('namaAcara'),
                    'visitor_amount'            => $request->post('jumlahPesertaPengunjung'),
                    'rank_host'                 => $request->post('peringkatPenganjuran'),
                    'other_rank'                => $request->post('nyatakanPeringkatPenganjuran'),
                    'event_date'                => (new DateTime($request->post('tarikhAcaraDari')))->format('Y-m-d'),
                    'event_time'                => (new DateTime($request->post('masaPenganjuranDari')))->format('H:i:s'),
                    'event_time_to'             => (new DateTime($request->post('masaPenganjuranHingga')))->format('H:i:s'),
                    'event_date_end'            => (new DateTime($request->post('tarikhAcaraHingga')))->format('Y-m-d'),
                    'enter_date'                => (new DateTime($request->post('tarikhMasukTapak')))->format('Y-m-d'),
                    'enter_time'                => (new DateTime($request->post('masaMasukTapak')))->format('H:i:s'),
                    'exit_date'                 => (new DateTime($request->post('tarikhKeluarTapak')))->format('Y-m-d'),
                    'exit_time'                 => (new DateTime($request->post('masaKeluarTapak')))->format('H:i:s'),
                    'total_day'                 => $request->post('jumlahHariPenggunaanPerakuan'),
                    'drone'                     => $request->post('libatPenggunaanDrone'),
                    'vip_name'                  => $request->post('namaTetamuKehormat'),
                    'remark'                    => $request->post('maklumatTambahan'),
                    'booking_verification'      => 1,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                );
                // dd($main_bookingData, $spa_booking_organiserData, $spa_booking_personData, $spa_booking_eventData);
                All::InsertGetID('spa_booking_event', $spa_booking_eventData);
            } catch (\Throwable $th) {
            }
            $redirectUrl = route('event.summary', ['id' => Crypt::encrypt($id), 'bookingId' => $main_bookingId]);
            return response()->json(['redirectUrl' => $redirectUrl, 'id' => Crypt::encrypt($id), 'bookingId' => $main_bookingId]);
        }
        
        return view('event.public.reservation', compact('data'));
    }

    public function summary(Request $request, $id, $bookingId = null) {
        $id = Crypt::decrypt($id);
        $data['date'] = date("Y-m-d");
        $data['id'] = $id;
        // $data['location'] = All::Show('bh_hall', 'id', 'ASC')->where('bh_status', 1);
        // $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        // $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('id', '!=' , $id)->where('id', '!=' , $id)->where('bh_status', 1);
        // $data['price'] = All::Show('bh_hall_price', 'id', 'ASC')->where('fk_bh_hall', $id)->where('bhp_status', 1);
        $data['start'] = "";
        $data['end'] = "";
        $data['event'] = Event::locationPictures($id)->first();
        // dd($data, $id, $bookingId, $request->post());
        $data['main_booking'] = ALL::GetRow('main_booking', 'id', $bookingId);
        $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $data['main_booking']->id);
        if ($data['booking_person']->applicant_status == 2) {
            $data['booking_organiser'] = ALL::GetRow('spa_booking_organiser', 'fk_main_booking', $data['main_booking']->id);
        }
        $data['fasility1'] = All::GetRow('spa_location', 'id', $id);
        $data['bookingId'] = (int)$bookingId;
        // $data['event'] = Event::eventById($id);
        $data['total_type'] = 0;
        if (($data['event']->rent_perday) == "0.00" || ($data['event']->rent_perday) == "1.00") {
            $data['total'] = $data['fasility1']->remark;
        } else {
            $data['total'] = $data['event']->rent_perday * $data['booking_event']->total_day;
            $data['total_type'] = 1;
        }
        // dd($data);

        return view('event.public.summary', compact('data'));
    }

    public function postPayment(Request $request, $id, $orderNo, $status){
        $id = Crypt::decrypt($id);
        $data['orderNo'] = Crypt::decrypt($orderNo);
        $data['status'] = Crypt::decrypt($status);
        $data['main'] = All::GetRow('main_booking', 'id', $id);
        $data['be'] = All::GetRow('spa_booking_event', 'fk_main_booking', $id);
        $data['receipt'] = All::GetRow('spa_payment', 'transaction_id', $data['orderNo']);

        return view('event.public.status', compact('data'));
    }
    
    public function mybookings($jenis = null) {
        if (!Session::get('user')) {
            return redirect('/auth');
        }
        $data['jenis'] = $jenis;
        $data['event'] = '';
        if($jenis != null){
            $jenisVal = 0;
            if ($jenis == 'draft') {
                $jenisVal = 18;
            } else if ($jenis == 'new') {
                $jenisVal = 19;
            } else if ($jenis == 'not-complete') {
                $jenisVal = 28;
            } else if ($jenis == 'require-deposit') {
                $jenisVal = 24;
            } else if ($jenis == 'require-fullpayment') {
                $jenisVal = [25, 26];
            } else if ($jenis == 'all') {
                $jenisVal = [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
            }
            $data['event'] = All::JoinThreeTablesWithWhereAnd('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', $jenisVal, 'fk_users', Session::get('user.id'));
        }
        else {
            $data['all'] = All::JoinThreeTablesWithWhereAnd('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], 'fk_users', Session::get('user.id'));
            $data['draft'] = $data['all']->where('fk_lkp_status', 18);
            $data['new'] = $data['all']->where('fk_lkp_status', 19);
            $data['not-complete'] = $data['all']->where('fk_lkp_status', 28);
            $data['require-deposit'] = $data['all']->where('fk_lkp_status', 24);
            $data['require-fullpayment'] = $data['all']->whereIn('fk_lkp_status', [25, 26]);
        }
        return view('event.dashboard.tempahansaya', compact('data'));
    }

    public function tempahan(Request $request, $bookingId) {
        if (!Session::get('user')) {
            return redirect('/auth');
        }
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        
        $bookingId = Crypt::decrypt($bookingId);
        $data['bookingId'] = $bookingId;
        $data['userId'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->fk_users;
        $userEmail = All::GetRow('users', 'id', $data['userId'])->email;
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->id;

        if(request()->isMethod('post')){
            $main_bookingData = array(
                'fk_lkp_status'             => 19,
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $main_bookingId = All::GetUpdate('main_booking', $id, $main_bookingData);

            $booking_reviewData = [
                'deleted_at'        => date('Y-m-d H:i:s')
            ];
            $update = All::GetUpdateSpec('spa_booking_review', 'fk_main_booking', $id, $booking_reviewData);

            $contactPerson = '';
            $nogst = '';
            if ($request->post('statusPemohon') == 2) {
                $ifExist = All::GetRow('spa_booking_organiser', 'fk_main_booking', $id);
                $contactPerson = $request->post('contactPerson');
                $nogst = $request->post('nogst');
                if($ifExist){
                    $spa_booking_organiserData = array(
                        'fk_lkp_state'              => $request->post('negeriAgensi'),
                        'fk_lkp_country'            => $request->post('negaraAgensi'),
                        'agency_name'               => $request->post('namaAgensi'),
                        'address'                   => $request->post('alamatAgensi'),
                        'city'                      => $request->post('bandarAgensi'),
                        'postcode'                  => $request->post('poskodAgensi'),
                        'telephone_no'              => $request->post('telBimbitAgensi'),
                        'created_at'                => date('Y-m-d H:i:s'),
                        'updated_at'                => date('Y-m-d H:i:s'),
                    );        
                    All::GetUpdateSpec('spa_booking_organiser', 'fk_main_booking', $id, $spa_booking_organiserData);
                } else {
                    $spa_booking_organiserData = array(
                        'fk_main_booking'           => $id,
                        'fk_lkp_state'              => $request->post('negeriAgensi'),
                        'fk_lkp_country'            => $request->post('negaraAgensi'),
                        'agency_name'               => $request->post('namaAgensi'),
                        'address'                   => $request->post('alamatAgensi'),
                        'city'                      => $request->post('bandarAgensi'),
                        'postcode'                  => $request->post('poskodAgensi'),
                        'telephone_no'              => $request->post('telBimbitAgensi'),
                        'created_at'                => date('Y-m-d H:i:s'),
                        'updated_at'                => date('Y-m-d H:i:s'),
                    );        
                    All::InsertGetID('spa_booking_organiser', $spa_booking_organiserData);
                }
            }
            // dd($request->post(), $spa_booking_organiserData);

            $spa_booking_personData = array(
                'name'                      => $request->post('namaPemohon'),
                'post'                      => $request->post('jawatan'),
                'address'                   => $request->post('alamat'),
                'postcode'                  => $request->post('poskod'),
                'city'                      => $request->post('bandar'),
                'fk_lkp_state'              => $request->post('negeri'),
                'fk_lkp_country'            => $request->post('negara'),
                'ic_no'                     => $request->post('kadPengenalanPendaftaran'),
                'office_no'                 => $request->post('noPejabat'),
                'hp_no'                     => $request->post('noBimbit'),
                'fax_no'                    => $request->post('noFaks') ?? '',
                'email'                     => $request->post('email'),
                'applicant_status'          => $request->post('statusPemohon'),
                'applicant_type'            => $request->post('jenisPemohon'),
                'contact_person'            => $contactPerson,
                'GST_no'                    => $nogst,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            );
            $query = All::GetUpdateSpec('spa_booking_person', 'fk_main_booking', $id, $spa_booking_personData);

            $spa_booking_eventData = array(
                'event_name'                => $request->post('namaAcara'),
                'visitor_amount'            => $request->post('jumlahPesertaPengunjung'),                               
                'rank_host'                 => $request->post('peringkatPenganjuran'),                                  
                'other_rank'                => $request->post('nyatakanPeringkatPenganjuran'),                          
                'event_date'                => (new DateTime($request->post('tarikhAcaraDari')))->format('Y-m-d'),      
                'event_time'                => (new DateTime($request->post('masaPenganjuranDari')))->format('H:i:s'),  
                'event_time_to'             => (new DateTime($request->post('masaPenganjuranHingga')))->format('H:i:s'),
                'event_date_end'            => (new DateTime($request->post('tarikhAcaraHingga')))->format('Y-m-d'),    
                'enter_date'                => (new DateTime($request->post('tarikhMasukTapak')))->format('Y-m-d'),     
                'enter_time'                => (new DateTime($request->post('masaMasukTapak')))->format('H:i:s'),       
                'exit_date'                 => (new DateTime($request->post('tarikhKeluarTapak')))->format('Y-m-d'),    
                'exit_time'                 => (new DateTime($request->post('masaKeluarTapak')))->format('H:i:s'),      
                'total_day'                 => $request->post('jumlahHariPenggunaanPerakuan'),                          
                'drone'                     => $request->post('libatPenggunaanDrone'),                                  
                'vip_name'                  => $request->post('namaTetamuKehormat'),                                    
                'remark'                    => $request->post('maklumatTambahan'),                                      
                'booking_verification'      => 1,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            );
            All::GetUpdateSpec('spa_booking_event', 'fk_main_booking', $id, $spa_booking_eventData);

            $spa_reviewData = [
                'deleted_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ];
            All::GetUpdateSpec('spa_booking_review', 'fk_main_booking', $id, $spa_reviewData);

            $validate = Event::validateData('spa_meeting_result', 'meeting_no', $request->post('meeting_no'));
            $bookingId = Crypt::encrypt($bookingId);

            return redirect()->back();
            // return redirect()->route('reservation.get', ['id' => $bookingId]);
        }

        try {
            $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $bookingId);
            $bookingId = $data['main_booking']->id;
            if ($data['main_booking']->fk_lkp_status == null) {
                return redirect()->back();
            }
            $lkp_status = ALL::GetSpecRow('lkp_status', 'id', $data['main_booking']->fk_lkp_status);
            $data['status_tempahan'] = $lkp_status[0]->ls_description;
            $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $bookingId);
            $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $bookingId);
            $data['booking_organiser'] = ALL::GetRow('spa_booking_organiser', 'fk_main_booking', $bookingId);
            $data['spa_booking_review'] = ALL::GetRowDeleteNull('spa_booking_review', 'fk_main_booking', $bookingId);

            $data['spa_booking_attachment'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['spa_booking_attachment_pendahuluan'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $bookingId, 'id', 'ASC')->whereIn('fk_lkp_spa_filename', [14, 19, 18])->values();
            $data['spa_meeting_result'] = ALL::GetAllRow('spa_meeting_result', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['spa_booking_event'] = ALL::GetAllRow('spa_booking_event', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['spa_quotation_detail'] = ALL::GetAllRow('spa_quotation_detail', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['quotation_detail_item10'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 10)->first();
            $data['quotation_detail_item22'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 22)->first();
            $data['quotation_detail_item11'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 11)->first();
            $data['quotation_detail_item12'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 12)->first();
            $data['quotation_detail_item13'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 13)->first();
            $data['quotation_detail_item6'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 9)->first();
        } catch (\Throwable $th) {
            Log::Info($th);
        }
        $data['total'] = ($data['quotation_detail_item10']->total ?? 0) + ($data['quotation_detail_item22']->total ?? 0) + ($data['quotation_detail_item11']->total ?? 0) + ($data['quotation_detail_item12']->total ?? 0) + ($data['quotation_detail_item13']->total ?? 0) ;
        $data['totalHalf'] = $data['total'] / 2;
        $data['total_v'] = ($data['total'] / 2) + ($data['quotation_detail_item6']->rate ?? 0);
        $data['id'] = $data['main_booking']->id;
        if (!isset($data['deposit'])) {
            $data['deposit'] = new \stdClass();
            $data['deposit']->amount = null;
        }
        if ($data['quotation_detail_item6'] != null) {
            $data['deposit']->amount = $data['quotation_detail_item6']->total;
        }
        $data['spa_payment'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main_booking']->id)->where('payment_status', 1)->where('deleted_at', NULL);
        
        $data['spa_booking_attachment'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $latestAttachments = [];
        foreach ($data['spa_booking_attachment'] as $attachment) {
            if (isset($latestAttachments[$attachment->fk_lkp_spa_filename])) {
                // Compare the created_at timestamps to determine which one is the latest
                if ($attachment->created_at > $latestAttachments[$attachment->fk_lkp_spa_filename]->created_at) {
                    // If the current attachment is newer, replace the previous one
                    $latestAttachments[$attachment->fk_lkp_spa_filename] = $attachment;
                }
            } else {
                // If it doesn't exist, add it to $latestAttachments
                $latestAttachments[$attachment->fk_lkp_spa_filename] = $attachment;
            }
        }
        $data['spa_booking_attachment'] = $latestAttachments;

        return view('event.public.reservationTest', compact('data'));
    }

    public function maklumattempahan($booking_no) {
        $data['booking'] = Crypt::decrypt($booking_no);
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $data['booking'])[0]->id;
        $data['main'] = All::GetRow('main_booking', 'id', $id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        // $data['slot'] = Sport::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $id, 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $id, 'id', 'DESC');
        
        return view('event.public.maklumattempahan', compact('data'));
        //return view('sport.public.maklumattempahansap', compact('data'));
    }

    public function tempahan_delete($booking_no){
        $booking_no = Crypt::decrypt($booking_no);

        $data = [
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        ];
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $booking_no)[0]->id;
        $query = All::GetUpdate('main_booking', $id, $data);
        $query = All::GetUpdateSpec('spa_booking_event', 'fk_main_booking', $id, $data);
        $query = All::GetUpdateSpec('spa_booking_person', 'fk_main_booking', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/tempahan'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/tempahan'));
        }
    }

    public function bayaran(Request $request, $booking, $value) {
        $data['booking'] = Crypt::decrypt($booking);
        $main = All::GetRow('main_booking', 'id', $data['booking']);
        $data['sbe'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['booking']);
        $data['sbp'] = All::GetRow('spa_booking_person', 'fk_main_booking', $data['booking']);
        $data['sbp'] = All::GetRow('spa_booking_person', 'fk_main_booking', $data['booking']);
        $data['oderNo'] = $main->bmb_booking_no;//.'-'.$no;
        $data['total'] = $value;

        // $quono = Sport::generatequono();
        $transHistory = All::GetAllRow('spa_payment', 'fk_main_booking', $data['booking'])->where('deleted_at', NULL);
        $no = Sport::generatetransid();
        $data['newOrderNo'] = $data['oderNo'] . $no;
        if(count($transHistory) == 0) {
            if($main->fk_lkp_status == 24){ // Deposit + Pendahuluan
                $data_fpx_depo = array(
                    'fk_main_booking'           => $data['booking'],
                    'fk_lkp_payment_type'       => 1,
                    'fk_users'                  => Session::get('user.id'),
                    'amount'                    => $main->bmb_subtotal,
                    'transaction_id'            => $data['newOrderNo'],
                    // 'transaction_date'          => ,
                    'fk_lkp_payment_mode'       => 4,
                    'payment_status'            => 0,
                    'application_no'            => $data['oderNo'],
                    'reference_id'              => $data['sbp']->ic_no, // no ic / pendaftaran
                    'booking_location'          => HP::locationspa($data['sbe']->fk_spa_location), // lokasi acara
                    'booking_title'             => 'Tempahan Acara',
                    'booking_start'             => $data['sbe']->event_date, // tarikh acara mula
                    'booking_end'               => $data['sbe']->event_date_end, // tarikh acara tamat
                    'booking_time'              => $data['sbe']->exit_time, // masa acara
                    'payment_date'              => date('Y-m-d'),
                    'total_amount'              => $main->bmb_subtotal + $main->bmb_deposit_rm,
                    'deposit_amount'            => $main->bmb_deposit_rm,
                    'status'                    => 1,
                    // 'paid_amount'               => ,
                    // 'bank_name'                 => ,
                    // 'fpx_serial_no'             => ,
                    // 'no_bil'                    => ,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::InsertGetID('spa_payment', $data_fpx_depo);
    
                $data_fpx_pendahuluan = array(
                    'fk_main_booking'           => $data['booking'],
                    'fk_lkp_payment_type'       => 3,
                    'fk_users'                  => Session::get('user.id'),
                    'amount'                    => $main->bmb_subtotal,
                    'transaction_id'            => $data['newOrderNo'],
                    // 'transaction_date'          => ,
                    'fk_lkp_payment_mode'       => 4,
                    'payment_status'            => 0,
                    'application_no'            => $data['oderNo'],
                    'reference_id'              => $data['sbp']->ic_no, // no ic / pendaftaran
                    'booking_location'          => HP::locationspa($data['sbe']->fk_spa_location), // lokasi acara
                    'booking_title'             => 'Tempahan Acara',
                    'booking_start'             => $data['sbe']->event_date, // tarikh acara mula
                    'booking_end'               => $data['sbe']->event_date_end, // tarikh acara tamat
                    'booking_time'              => $data['sbe']->exit_time, // masa acara
                    'payment_date'              => date('Y-m-d'),
                    'total_amount'              => $main->bmb_subtotal + $main->bmb_deposit_rm,
                    'deposit_amount'            => $main->bmb_deposit_rm,
                    'status'                    => 1,
                    // 'paid_amount'               => ,
                    // 'bank_name'                 => ,
                    // 'fpx_serial_no'             => ,
                    // 'no_bil'                    => ,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::InsertGetID('spa_payment', $data_fpx_pendahuluan);
            }
        } else if (count($transHistory) <= 2) {
            if($main->fk_lkp_status == 24){ // Deposit + Pendahuluan
                $data_fpx_depo = array(
                    'transaction_id'            => $data['newOrderNo'],
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['booking'], 'fk_lkp_payment_type', 1, $data_fpx_depo);

                $data_fpx_pendahuluan = array(
                    'transaction_id'            => $data['newOrderNo'],
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $data['booking'], 'fk_lkp_payment_type', 3, $data_fpx_pendahuluan);
            }
            if($main->fk_lkp_status == 26){
                $data_fpx_penuh = array(
                    'fk_main_booking'           => $data['booking'],
                    'fk_lkp_payment_type'       => 2,
                    'fk_users'                  => Session::get('user.id'),
                    'amount'                    => $main->bmb_subtotal,
                    'transaction_id'            => $data['newOrderNo'],
                    // 'transaction_date'          => ,
                    'fk_lkp_payment_mode'       => 4,
                    'payment_status'            => 0,
                    'application_no'            => $data['oderNo'],
                    'reference_id'              => $data['sbp']->ic_no, // no ic / pendaftaran
                    'booking_location'          => HP::locationspa($data['sbe']->fk_spa_location), // lokasi acara
                    'booking_title'             => 'Tempahan Acara',
                    'booking_start'             => $data['sbe']->event_date, // tarikh acara mula
                    'booking_end'               => $data['sbe']->event_date_end, // tarikh acara tamat
                    'booking_time'              => $data['sbe']->exit_time, // masa acara
                    'payment_date'              => date('Y-m-d'),
                    'total_amount'              => $main->bmb_subtotal + $main->bmb_deposit_rm,
                    'deposit_amount'            => $main->bmb_deposit_rm,
                    'status'                    => 1,
                    // 'paid_amount'               => ,
                    // 'bank_name'                 => ,
                    // 'fpx_serial_no'             => ,
                    // 'no_bil'                    => ,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::InsertGetID('spa_payment', $data_fpx_penuh);
            }
        } else if (count($transHistory) == 3){
            $updateIdTransaksi = [
                'transaction_id'    => $data['newOrderNo'],
                'updated_at'        => date('Y-m-d H:i:s')
            ];
            All::GetUpdate2SpecColumn('spa_payment', 'fk_main_booking', $main->id, 'fk_lkp_payment_type', 2, $updateIdTransaksi);
        }

        $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';
        $data1 = array("payload" => array(
            "subsysId"  => "SPA",
            "password"  => "4pA@1py2023!",
            "orderNo"   => $data['newOrderNo'],
            "description" => $data['newOrderNo'],
            "txnTime" => date('Y-m-d H:i:s'),
            "amount"  => $data['total']  
        ));
        $postdata = json_encode($data1);
		
        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        curl_close($ch);
        if(isset($response['message']) == 'Payment already successful!'){
            // AuditLog::log(Crypt::encrypt(32), Crypt::encrypt($data['oderNo']), 'No Tempahan telah digunakan untuk pembayaran', 1);
            return redirect()->back();
        }
        return view('event.public.bayaran', compact('data', 'response'));
    }
    
	public function maklumatbayaran($booking) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['spa_booking_event'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        // $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        // $data['spa_payment'] = All::GetRow('spa_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['spa_payment'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['booking'])->where('deleted_at', NULL);
        // $data['spa_payment_detail'] = All::GetAllRow2('spa_payment_detail', 'fk_spa_payment', $data['spa_payment']->id, 'id', 'DESC');

        $data['config_gst'] = All::GetAllRow('lkp_configuration', 'id', 1)->first()->status;

        return view('event.public.maklumattempahan', compact('data'));
        //return view('sport.public.maklumattempahansap', compact('data'));
    }

    public function internalexternal() {
        if(Session::get('flash')){
            $data['flashMessage'] = Session::get('flash');
        }
        $data['post'] = false;
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['event'] = Event::getlistDalaman();
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['startDate'] = Carbon::createFromFormat('d M Y', $request->post('tarikhDari'))->toDateString();
            $startDate = $data['startDate'];
            $data['endDate'] = Carbon::createFromFormat('d M Y', $request->post('tarikhHingga'))->toDateString();
            $endDate = $data['endDate'];
            $data['event'] = $data['event']->filter(function ($event) use ($data, $startDate, $endDate) {
                return $event->fk_spa_location == $data['id'] &&
                    (($event->event_date >= $startDate && $event->event_date <= $endDate) ||
                     ($event->event_date_end >= $startDate && $event->event_date_end <= $endDate) ||
                     ($event->event_date <= $startDate && $event->event_date_end >= $endDate));
            });
        }

        return view('event.dashboard.internalexternal', compact('data'));
    }

	public function details500() 
	{
        //return abort(500, 'custom error');
		return view('event.public.update');
        //return view('event.public.details', compact('data'));
    }

    public function slot($id, $date){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);

        //No Booking Sukan
        $running =  Hall::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $data['book']='SPT'.$year.$md.$rn;

        $data['id'] = $id;
        $data['date'] = $date;
        $dates = explode(" / ", $data['date']);
        if (count($dates) === 2) {
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
        }
        $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
        $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id',  $id);
        $data['capacity'] = All::Show('bh_hall_usage', 'id', 'ASC')->where('fk_bh_hall_detail', $data['details']->id);
        $data['event'] = All::Show('lkp_event', 'id', 'ASC')->where('le_status', 1);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        return view('event.public.slot', compact('data'));
    }
	
	public function cancel(Request $request) {
        $user = Session::get('user');
        if (!$user || !isset($user['id']) || $user['id'] === null) {
            return redirect('/auth');
        }
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            if (substr($data['id'], 0, 3) === 'SPA') {
                $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            } else {
                $data['main'] = null;
            }
        }
		// if (Session::get('user')['id'] == "50554")
		// {
        return view('event.cancel.index', compact('data'));
		// }
    }

    public function submitcancel(Request $request, $id) {
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_lkp_status'           => 21,
            'bmb_reason_cancel'       => request()->reason,
            'updated_by'              => Session::get('user')['id'],
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('main_booking', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
        }else{
            Session::flash('flash', 'Failed'); 
        }
        return Redirect::to(url('/event/cancel'));
    }

    public function submitcancel2(Request $request, $id) {
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_lkp_status'           => 21,
            'bmb_reason_cancel'       => request()->reason,
            'updated_by'              => Session::get('user')['id'],
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('main_booking', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
        }else{
            Session::flash('flash', 'Failed'); 
        }
        return Redirect::to(url('/event'));
    }
	
	
	public function duplicate(Request $request) {
        $data['post'] = false;
		$data['location'] = Event::fasility();
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['date2'] = $request->post('tarikhHingga');
            $data['slot'] = Event::duplicate($data['id'], $data['date'], $data['date2']);
            return view('event.duplicate.index', compact('data'));	
        }
        //$data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $data['slot'] = Event::duplicate_all2();

        return view('event.duplicate.index', compact('data'));		
    }

    public function reschedule(Request $request, $location, $dateStart, $dateEnd) {
        $data['slot'] = Event::duplicateData($location, $dateStart, $dateEnd);
        $data['lokasi'] = $data['slot'][0]->fk_spa_location;

        return view('event.duplicate.reschedule', compact('data'));	
    }

    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }

}

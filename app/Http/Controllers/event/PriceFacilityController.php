<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class PriceFacilityController extends Controller
{
    public function show() {
        $data['list'] = All::Show('bh_hall_price', 'id', 'ASC');
        return view('hall.facilityprice.lists', compact('data'));
    }

    public function form(){
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 1);
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['package'] = All::Show('et_package', 'id', 'ASC');
        $data['slot'] = All::Show('lkp_slot_cat', 'id', 'ASC');
        return view('sport.facilityprice.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_facility'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_et_slot_time'   => request()->masa,
            'fk_lkp_gst_rate'   => request()->gst,
            'fk_et_package'     => request()->pakej,
            'fk_lkp_slot_cat'   => request()->slot,
            'efp_day_cat'       => request()->hari,
            'efp_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('et_facility_price', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 1);
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['package'] = All::Show('et_package', 'id', 'ASC');
        $data['slot'] = All::Show('lkp_slot_cat', 'id', 'ASC');
        $data['list'] = All::GetRow('et_facility_price', 'id', $id);
        return view('sport.facilityprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_facility'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_et_slot_time'   => request()->masa,
            'fk_lkp_gst_rate'   => request()->gst,
            'fk_et_package'     => request()->pakej,
            'fk_lkp_slot_cat'   => request()->slot,
            'efp_day_cat'       => request()->hari,
            'efp_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }
}

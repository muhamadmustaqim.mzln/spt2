<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Event;
use Carbon\Carbon;

class RescheduleController extends Controller
{
    public function index(Request $request) {
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
        $data['post'] = false;
        if (request()->isMethod('post')) {
            $data['post'] = true;
            $data['main_booking'] = All::GetRow('main_booking', 'bmb_booking_no', $request->id);
            if($data['main_booking'] != null) {
                $data['spa_booking_event'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['main_booking']->id);
                $data['user'] = All::GetRow('users', 'id', $data['main_booking']->fk_users);
            }
            // dd($data);
        }
		return view('event.reschedule.index', compact('data'));
    }
    public function event(Request $request, $bookingId) {
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        
        $bookingId = Crypt::decrypt($bookingId);
        $data['bookingId'] = $bookingId;
        $data['userId'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->fk_users;
        $userEmail = All::GetRow('users', 'id', $data['userId'])->email;
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->id;
        if(request()->isMethod('post')){
            $spa_booking_eventData = [
                'event_date'    => Carbon::createFromFormat('d M Y', $request->post('event_date'))->format('Y-m-d'),
                'event_date_end'=> Carbon::createFromFormat('d M Y', $request->post('event_date_end'))->format('Y-m-d'),
                'event_time'    => $request->post('event_time'),
                'event_time_to' => $request->post('event_time_to'),
                'enter_date'    => Carbon::createFromFormat('d M Y', $request->post('enter_date'))->format('Y-m-d'),
                'enter_time'    => $request->post('enter_time'),
                'exit_date'     => Carbon::createFromFormat('d M Y', $request->post('exit_date'))->format('Y-m-d'),
                'exit_time'     => $request->post('exit_time'),
            ];
            All::GetUpdateSpec('spa_booking_event', 'fk_main_booking', $id, $spa_booking_eventData);
            return redirect('event/duplicate');
        }
        $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $bookingId);
        $bookingId = $data['main_booking']->id;
        if ($data['main_booking']->fk_lkp_status == null) {
            return redirect()->back();
        }
        $lkp_status = ALL::GetSpecRow('lkp_status', 'id', $data['main_booking']->fk_lkp_status);
        $data['status_tempahan'] = $lkp_status[0]->ls_description;
        $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $bookingId);
        $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $bookingId);
        $data['spa_booking_review'] = ALL::GetRow('spa_booking_review', 'fk_main_booking', $bookingId);
        $data['spa_meeting_result'] = ALL::GetAllRow('spa_meeting_result', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $data['spa_booking_event'] = ALL::GetAllRow('spa_booking_event', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $data['spa_quotation_detail'] = ALL::GetAllRow('spa_quotation_detail', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $data['quotation_detail_item10'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 10)->first();
        $data['quotation_detail_item22'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 22)->first();
        $data['quotation_detail_item11'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 11)->first();
        $data['quotation_detail_item12'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 12)->first();
        $data['quotation_detail_item13'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 13)->first();
        $data['quotation_detail_item6'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 9)->first();

        $eventData = ALL::GetAllRow('spa_booking_event', 'fk_main_booking', $bookingId)->first();
        if ($eventData) {
            $loc = $eventData->fk_spa_location;
            $startdate = $eventData->event_date;
            $enddate = $eventData->event_date_end;
        
            $data['booked'] = Event::getBookedAcaralist($startdate, $enddate, $loc);
            // dd($data['booked']);
        }
        return view('event.reschedule.reservation', compact('data'));
    }
}

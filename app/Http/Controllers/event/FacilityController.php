<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\All;

class FacilityController extends Controller
{
    public function show() {
        $data['facility'] = All::ShowAll('spa_location', 'id', 'ASC');
        // dd($data);
        return view('event.facility.lists', compact('data'));
    }

    public function form(){
        $data['uuid'] = Str::uuid()->toString();
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        $data['facility_type'] = All::Show('et_facility_type', 'id', 'ASC');
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        return view('event.facility.form1', compact('data'));
    }

    public function add(Request $request){
        $uuid = $request->post('uuidKey');

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/main/'.$uuid);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
            // dd('Request Has No File');
        }

        if ($request->hasFile('file1')) {
            $path1 = public_path('assets/upload/virtual/'.$uuid);
            File::ensureDirectoryExists($path1);
            $image1 = $request->file('file1');
            $filename1 = $image1->getClientOriginalName();
            $request->file('file1')->move($path1, $filename1);
        }else{
            // dd('Request Has No File');
        }

        $data = array(
            'fk_lkp_location'   => $request->post('lokasi'),
            'fk_et_facility'    => $request->post('jenis'),
            'eft_type_desc'     => $request->post('fasiliti'),
            'eft_status'        => $request->post('status'),
            'cover_img'         => $filename,
            'virtual_img'       => $filename1,
            'highlight'         => $request->post('highlight'),
            'eft_uuid'          => $request->post('uuidKey'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        $query = All::Insert('et_facility_type', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facility'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        // $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        // $data['list'] = All::GetRow('et_facility_type', 'id', $id);
        $data['facility'] = All::ShowAll('spa_location', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        $data['list'] = All::GetRow('spa_location', 'id', $id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        // dd($data['list']->category);
        return view('event.facility.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $uuid = $request->post('uuidKey');

        // if ($request->hasFile('file')) {
        //     $path = public_path('assets/upload/main/'.$uuid);
        //     File::ensureDirectoryExists($path);
        //     $image = $request->file('file');
        //     $filename = $image->getClientOriginalName();
        //     $request->file('file')->move($path, $filename);

        //     $data = array(
        //         'cover_img'         => $filename,
        //         'created_at'        => date('Y-m-d H:i:s'),
        //         'updated_at'        => date('Y-m-d H:i:s'),
        //         'updated_by'        => Session::get('user')['id'],
        //     );
        //     // $query = All::GetUpdate('et_facility_type', $id, $data);
        // }

        // if ($request->hasFile('file1')) {
        //     $path1 = public_path('assets/upload/virtual/'.$uuid);
        //     File::ensureDirectoryExists($path1);
        //     $image1 = $request->file('file1');
        //     $filename1 = $image1->getClientOriginalName();
        //     $request->file('file1')->move($path1, $filename1);

        //     $data = array(
        //         'virtual_img'       => $filename1,
        //         'created_at'        => date('Y-m-d H:i:s'),
        //         'updated_at'        => date('Y-m-d H:i:s'),
        //         'updated_by'        => Session::get('user')['id'],
        //     );
        //     // $query = All::GetUpdate('et_facility_type', $id, $data);
        // }

        $data = array(
            'name'              => $request->post('namaLokasi'),
            'category'          => $request->post('kategori'), // kategori platinum/emas/perak
            // 'rent_perday'       => $request->post('fasiliti'),
            'deposit'           => $request->post('deposit'),
            'capacity'          => $request->post('kapasiti'),
            'status'            => $request->post('status'),
            'remark'            => $request->post('remark'),
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            // 'highlight'         => $request->post('highlight'),
            'longitude'         => $request->post('longitude'),
            'latitude'          => $request->post('latitude'),
            // 'type'              => $request->post('type'), x pasti type apa
            'fee_code'          => $request->post('fee_code'),
            // 'eft_uuid'          => $request->post('uuidKey'),
        );
        $query = All::GetUpdate('spa_location', $id, $data);
        // $query = All::GetUpdate('et_facility_type', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facility'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_type', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facility'));
        }
    }

    public function imageshow($id){
        $id = Crypt::decrypt($id);
        $data['id'] = $id;
        $data['facility'] = All::GetAllRow('et_facility_type_image', 'fk_et_facility_type', $id, 'id', 'ASC');
        return view('event.facility.imagelist', compact('data'));
    }

    public function imageform($id){
        $id = Crypt::decrypt($id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        $data['id'] = $id;
        return view('event.facility.imageform', compact('data'));
    }

    public function imageadd(Request $request, $id){
        $id = Crypt::decrypt($id);

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/pic/'.$id);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
            // dd('Request Has No File');
        }

        $data = array(
            'fk_et_facility_type'    => $id,
            'img'                    => $filename,
            'title'                  => $request->post('tajuk'),
            'created_at'             => date('Y-m-d H:i:s'),
            'updated_at'             => date('Y-m-d H:i:s'),
            'updated_by'             => Session::get('user')['id'],
        );
        $query = All::Insert('et_facility_type_image', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facility/image',Crypt::encrypt($id)));
        }
    }

    public function imagedelete($id, $file){
        $id = Crypt::decrypt($id);
        $file = Crypt::decrypt($file);

        $image = All::GetRow('et_facility_type_image', 'id', $file);

        $path = public_path('assets/upload/pic/'.$id.'/'.$image->img);
        if (File::exists($path)) {
            File::delete($path);
        }

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_type_image', $file, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facility/image',Crypt::encrypt($id)));
        }
    }
}

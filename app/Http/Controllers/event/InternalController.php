<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController as Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use App\Models\All;
use App\Models\Auth;
use App\Models\Event;
use Carbon\Carbon;

class InternalController extends Controller
{
    public function index(Request $request, $permohonan = null) {
        $data['post'] = false;
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['id'] = "";
        $data['startDate'] = "";
        $data['endDate'] = "";
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['event'] = Event::getlistDalaman()->where('fk_lkp_status', '!=', 18);

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['startDate'] = Carbon::createFromFormat('d-m-Y', $request->post('tarikhDari'))->toDateString();
            $startDate = $data['startDate'];
            $data['endDate'] = Carbon::createFromFormat('d-m-Y', $request->post('tarikhHingga'))->toDateString();
            $endDate = $data['endDate'];
            $data['event'] = $data['event']->filter(function ($event) use ($data, $startDate, $endDate) {
                if($data['id'] != null){
                    return $event->fk_spa_location == $data['id'] &&
                        (($event->event_date >= $startDate && $event->event_date <= $endDate) ||
                         ($event->event_date_end >= $startDate && $event->event_date_end <= $endDate) ||
                         ($event->event_date <= $startDate && $event->event_date_end >= $endDate));
                } else {
                    return 
                    (($event->event_date >= $startDate && $event->event_date <= $endDate) ||
                    ($event->event_date_end >= $startDate && $event->event_date_end <= $endDate) ||
                    ($event->event_date <= $startDate && $event->event_date_end >= $endDate));
                }
            });
        }
        
        return view('event.internal.index', compact('data'));
    }
    
    public function internaladd(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $flashMessage = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('nama');
            $data['user'] = Auth::user($data['id']);
            
            $data['user'] = array_filter($data['user'], function ($user) {
                // Check if the email contains "@ppj.gov.my"
                return strpos($user->bud_email, '@ppj.gov.my') !== false;
            });

            $data['user'] = array_values($data['user']);
            $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $data['id']);
        }
        // if (Session::get('user')['id'] == "50554")
        // if (Session::get('user')['role'] == 1)
		// {
			return view('event.internal.tapisandalaman', compact('data'));
		// } else {
        //     return redirect()->back();
        // }
    }

	public function form(Request $request, $location, $startDate, $endDate) {
        $data['user'] = null;
        // if ($email != null) {
        //     $email = Crypt::decrypt($email);
        //     $data['user_id'] = All::GetRow('user_profiles', 'bud_email', $email)->fk_users;
        //     $data['email'] = $email;
        //     $data['user'] = Auth::sso($email);
        //     $data['user'] = $data['user']->user;
        // }
        $location = Crypt::decrypt($location);
        $startDate = Crypt::decrypt($startDate);
        $endDate = Crypt::decrypt($endDate);

        $data['jenis'] = 1;
        $data['lokasi'] = $location;
        $data['dateFrom'] = $startDate;
        $data['dateUntil'] = $endDate;
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC');
        
        if(request()->isMethod('post')){
            $data['user_id'] = All::GetSpecRow('users', 'email', $request->post('email'))->first()->id;
            $running =  Event::getRn();
            $rn = sprintf( '%05d', $running);
            $year = date('y');
            $md = date('md');
            $bookrn='SPA'.$year.$md.$rn;

            $main_bookingData = array(
                'fk_users'                  => Session::get('user.id'),
                'fk_lkp_status'             => 20,
                'bmb_booking_no'            => $bookrn,
                'bmb_type_user'             => $request->post('jenisAcara'),
                'internal_indi'             => 1,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user.id'),
            );
            $mbid = All::InsertGetID('main_booking', $main_bookingData);

            $spa_booking_personData = array(
                'fk_main_booking'           => $mbid,
                'name'                      => $request->post('namaPegawai'),
                'post'                      => $request->post('jawatan'),
                'address'                   => $request->post('alamat'),
                'postcode'                  => $request->post('poskod'),
                'city'                      => $request->post('bandar'),
                'fk_lkp_state'              => $request->post('negeri'),
                'fk_lkp_country'            => $request->post('negara'),
                'ic_no'                     => $request->post('reference_id'),
                'office_no'                 => $request->post('noTelPejabat1'),
                'hp_no'                     => $request->post('noBimbit'),
                'fax_no'                    => $request->post('noFaks') ?? '',
                'email'                     => $request->post('email'),
                'applicant_status'          => $request->post('statusPemohon'),
                'applicant_type'            => $request->post('jenisPemohon'),
                // 'contact_person'            => $contactPerson,
                // 'GST_no'                    => $nogst,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            );        
            $query = All::InsertGetID('spa_booking_person', $spa_booking_personData);

            $tarikhAcaraDari = Carbon::createFromFormat('d-m-Y', $request->post('tarikhAcaraDari'))->format('Y-m-d');
            $tarikhAcaraHingga = Carbon::createFromFormat('d-m-Y', $request->post('tarikhAcaraHingga'))->format('Y-m-d');
            $tarikhMasukTapak = Carbon::createFromFormat('d-m-Y', $request->post('tarikhMasukTapak'))->format('Y-m-d');
            $tarikhKeluarTapak = Carbon::createFromFormat('d-m-Y', $request->post('tarikhKeluarTapak'))->format('Y-m-d');
            $masaMasukTapak = Carbon::createFromFormat('g:i A', $request->post('masaMasukTapak'))->format('H:i:s');
            $masaKeluarTapak = Carbon::createFromFormat('g:i A', $request->post('masaKeluarTapak'))->format('H:i:s');
            $masaPenganjuran = Carbon::createFromFormat('g:i A', $request->post('masaPenganjuran'))->format('H:i:s');

            $totalDays = (Carbon::parse($tarikhMasukTapak))->diffInDays(Carbon::parse($tarikhKeluarTapak)) + 1;

            $spa_booking_eventData = array(
                'fk_main_booking'           => $mbid,
                'fk_spa_location'           => $request->post('lokasiAcara'),
                'event_name'                => $request->post('namaAcara'),
                'visitor_amount'            => $request->post('jumlahPesertaPengunjung'),
                'event_date'                => $tarikhAcaraDari,
                'event_time'                => $masaPenganjuran,
                // 'event_time_to'             => (new DateTime($request->post('masaPenganjuranHingga')))->format('H:i:s'),
                'event_date_end'            => $tarikhAcaraHingga,
                'enter_date'                => $tarikhMasukTapak,
                'enter_time'                => $masaMasukTapak,
                'exit_date'                 => $tarikhKeluarTapak,
                'exit_time'                 => $masaKeluarTapak,
                'total_day'                 => $totalDays,
                'drone'                     => $request->post('drone'),
                'vip_name'                  => $request->post('namaTetamuKehormat'),
                'remark'                    => $request->post('maklumatTambahan'),
                'booking_verification'      => 1,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            );
            All::InsertGetID('spa_booking_event', $spa_booking_eventData);

            for ($i=0; $i < $totalDays; $i++) { 
                $date = date('Y-m-d', strtotime($tarikhMasukTapak . ' +' . $i . ' days'));
                $spa_confirm_booking = [
                    'fk_main_booking'       => $mbid,
                    'fk_spa_location'       => request()->lokasiAcara,
                    'fk_user'               => Session::get('user.id'),
                    'date_booking'          => $date,
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d')
                ];
                $query = All::InsertGetID('spa_booking_confirm', $spa_confirm_booking);
            }

            $id = $request->post('lokasiAcara');

            Session::flash('flash', 'Anda berjaya menambah acara.');
            return redirect('event/list/reservation');
            // return redirect()->route('event.summary', ['id' => Crypt::encrypt($id), 'bookingId' => $main_bookingId]);
        }

        return view('event.internal.form', compact('data'));
    }

	public function reservation(Request $request, $bookingId)
    {
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        
        $bookingId = Crypt::decrypt($bookingId);
        $data['bookingId'] = $bookingId;
        $data['userId'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->fk_users;
        $userEmail = All::GetRow('users', 'id', $data['userId'])->email;
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->id;
        if(request()->isMethod('post')){
            // dd('internal');
            // main_booking, spa_booking_review [22 notifyterima, 28 , 29 gagal]
            if($request->post('tab') == 'tindakanSemakan') {
                $lkp_status = ($request->post('statusSemakanPermohonan') == 1) ? 22 : (($request->post('statusSemakanPermohonan') == 2) ? 28 : 29);
                $data['tindakanSemakan'] = [
                    'bmb_booking_no'    => $bookingId,
                    'date'              => $request->post('tarikhSemakan'),
                    'time'              => $request->masaSemakan,
                    'location'          => $request->location,
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'remark'            => $request->post('maklumatTambahan'),
                    'ulasan'            => $request->post('ulasan'),
                    'maklumatKemasKini' => $request->post('maklumatKemasKini')
                ];
                Mail::acara('muhamadmustaqim.mzln@gmail.com', $lkp_status, $data['tindakanSemakan'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));

                $mainBookingData = array(
                    'fk_lkp_status' => $lkp_status,
                    'updated_at'    => now()
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);
                
                $tarikhSemakan = $request->post('tarikhSemakan');
                $masaSemakan = $request->post('masaSemakan');
                $tarikhSemakan = Carbon::parse(Carbon::createFromFormat('d-m-Y', "$tarikhSemakan")->toDateTimeSTring())->format('Y-m-d');
                $masaSemakan = Carbon::parse(Carbon::createFromFormat('g:i A', "$masaSemakan")->toDateTimeSTring())->format('H:i:s');
                
                $spa_booking_reviewData = array(
                    'fk_main_booking'   => $id,
                    'fk_user'           => $request->post('fk_user'),
                    'fk_lkp_spa_review' => ($request->post('statusSemakanPermohonan') == 1) ? 5 : (($request->post('statusSemakanPermohonan') == 2) ? 29 : 28),
                    'date'              => $tarikhSemakan,
                    'time'              => $masaSemakan,
                    'remark'            => $request->maklumatTambahan,
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'location'          => $request->post('location'),
                    'created_at'        => now(),
                    'updated_at'        => now()
                );
                All::Insert('spa_booking_review', $spa_booking_reviewData);
            }
            // main_booking, spa_meeting_result [30 lulus, 29 gagal, 23 pembentangan semula]
            else if($request->post('tab') == 'tindakanKeputusanMesyuarat') {
                $lkp_status = ($request->post('fk_lkp_status_meeting') == 3) ? 30 : (($request->post('fk_lkp_status_meeting') == 2) ? 29 : 23);
                if ($lkp_status != 23) {
                    $data['tindakanSemakan'] = [
                        'bmb_booking_no'    => $bookingId,
                        'date_meeting'      => $request->post('date_meeting'),
                        'meeting_no'        => $request->meeting_no,
                        'remark'            => $request->post('remark')
                    ];
                } else if ($lkp_status != 29) {
                    $data['tindakanSemakan'] = [
                        'bmb_booking_no'    => $bookingId,
                        'date'              => $request->post('tarikhSemakan'),
                        'time'              => $request->masaSemakan,
                        'location'          => $request->location,
                        'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                        'remark'            => $request->post('maklumatTambahan'),
                        'ulasan'            => $request->post('ulasan'),
                        'maklumatKemasKini' => $request->post('maklumatKemasKini')
                    ];
                }
                if ($lkp_status != 30) {
                    Mail::acara('muhamadmustaqim.mzln@gmail.com', $lkp_status, $data['tindakanSemakan'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));
                }

                // $validate = ALL::GetSpecRow('spa_meeting_result', 'meeting_no', $request->post('meeting_no'));
                // if (!$validate || $validate){
                $mainBookingData = array(
                    'fk_lkp_status' => $lkp_status,
                    'updated_at'    => now()
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);
                $spa_meeting_result = array(
                    'fk_main_booking'           => $id,
                    'fk_user'                   => Session::get('user')['id'],
                    'fk_spa_booking_attachment' => null,
                    'date_meeting'              => Carbon::createFromFormat('d/m/Y', $request->post('date_meeting'))->toDateString(),
                    'meeting_no'                => $request->post('meeting_no'),
                    'remark'                    => $request->post('remark'),
                    'fk_lkp_status_meeting'     => ($request->post('fk_lkp_status_meeting') == 3) ? 30 : (($request->post('fk_lkp_status_meeting') == 2) ? 29 : 23),
                    'created_at'                => now(),
                    'updated_at'                => now()
                );
                All::Insert('spa_meeting_result', $spa_meeting_result);
                // }
            }
            // main_booking, spa_quotation_detail, spa_booking_event [24]
            else if ($request->post('tab') == 'tindakanMaklumatBayaran') {
                $lkp_status = 24;
                $data['tindakanMaklumatBayaran'] = [
                    'bmb_booking_no'    => $bookingId,
                    'date'              => $request->post('tarikhSemakan'),
                    'time'              => $request->masaSemakan,
                    'location'          => $request->location,
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'remark'            => $request->post('maklumatTambahan')
                ];
                Mail::acara('muhamadmustaqim.mzln@gmail.com', $lkp_status, $data['tindakanMaklumatBayaran'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));

                $mainBookingData = array(
                    'fk_lkp_status' => 24,
                    'bmb_total_word'=> $request->post('jumlahDeposit'),
                    'updated_at'    => now()
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);

                $fk_lkp_spa_payitem = [10, 22, 11, 12, 13];

                $jumlah = ['sewaTapak_jumlah', 'sewaTasik_jumlah', 'laluanBawah50_jumlah', 'laluanAtas50_jumlah', 'bukanMilikPPj_jumlah'];
                $kadar = ['sewaTapak_kadar', 'sewaTasik_kadar', 'laluanBawah50_kadar', 'laluanAtas50_kadar', 'bukanMilikPPj_kadar'];
                $kuantiti = ['sewaTapak_kuantiti', 'sewaTasik_kuantiti', 'laluanBawah50_kuantiti', 'laluanAtas50_kuantiti', 'bukanMilikPPj_kuantiti'];
                $diskaunRM = ['sewaTapak_diskaun', 'sewaTasik_diskaun', 'laluanBawah50_diskaun', 'laluanAtas50_diskaun', 'bukanMilikPPj_diskaun'];
                $diskaunPer = ['sewaTapak_diskaunPer', 'sewaTasik_diskaunPer', 'laluanBawah50_diskaunPer', 'laluanAtas50_diskaunPer', 'bukanMilikPPj_diskaunPer'];

                $hasValue = array_map(function ($item) use ($request) {
                    return $request->post($item) > 0;
                }, $jumlah);
                foreach ($hasValue as $index => $hv) {
                    if ($hv) {
                        $spa_quotation_detail = [
                            'fk_main_booking'   => $id,
                            'fk_user'           => Session::get('user')['id'],
                            'fk_lkp_spa_payitem' => $fk_lkp_spa_payitem[$index],
                            'rate'              => $request->post($kadar[$index]),
                            'quantity'          => $request->post($kuantiti[$index]),
                            'exceptional'       => 0,
                            'discount_rm'       => $request->post($diskaunRM[$index]),
                            'discount_percentage'=> $request->post($diskaunPer[$index]),
                            'gst_rm'            => 0.00,
                            'total'             => $request->post($jumlah[$index]),
                            'created_at'        => now(),
                            'updated_at'        => now(),
                        ];
                        // validate, if not exist yet main_booking_id & fk_lkp_spa_payitem then insert into. Else update value
                        All::Insert('spa_quotation_detail', $spa_quotation_detail);
                    }
                } 
                $spa_quotation_detail2 = [
                    'fk_main_booking'   => $id,
                    'fk_user'           => Session::get('user')['id'],
                    'fk_lkp_spa_payitem'=> 9,
                    'rate'              => $request->post('jumlahKeseluruhan'),
                    'quantity'          => 1,
                    'exceptional'       => 0,
                    'discount_rm'       => 0.00,
                    'discount_percentage'=> 0.00,
                    'gst_rm'            => 0.00,
                    'total'             => $request->post('jumlahKeseluruhan'),
                    'created_at'        => now(),
                    'updated_at'        => now(),
                ];
                All::Insert('spa_quotation_detail', $spa_quotation_detail2);

                $spa_booking_eventData = [
                    'letter_no_five'    => $request->post('ayatSuratKelulusan')
                ];
                All::GetUpdateSpec('spa_booking_event', 'fk_main_booking', $id, $spa_booking_eventData);
            }
            else if ($request->post('tab') == 'bayaranPendahuluan') {
                
            }
            else if ($request->post('tab') == 'bayaranPenuh') {
                
            }
            $validate = Event::validateData('spa_meeting_result', 'meeting_no', $request->post('meeting_no'));
            $bookingId = Crypt::encrypt($bookingId);
            return redirect()->route('reservation.get', ['id' => $bookingId]);
        }
        try {
            $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $bookingId);
            $bookingId = $data['main_booking']->id;
            if ($data['main_booking']->fk_lkp_status == null) {
                return redirect()->back();
            }
            $lkp_status = ALL::GetSpecRow('lkp_status', 'id', $data['main_booking']->fk_lkp_status);
            $data['status_tempahan'] = $lkp_status[0]->ls_description;
            $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $bookingId);
            $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $bookingId);
            $data['spa_booking_review'] = ALL::GetRow('spa_booking_review', 'fk_main_booking', $bookingId);
            $data['spa_meeting_result'] = ALL::GetAllRow('spa_meeting_result', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['spa_booking_event'] = ALL::GetAllRow('spa_booking_event', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['spa_quotation_detail'] = ALL::GetAllRow('spa_quotation_detail', 'fk_main_booking', $bookingId, 'id', 'ASC');
            $data['quotation_detail_item10'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 10)->first();
            $data['quotation_detail_item22'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 22)->first();
            $data['quotation_detail_item11'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 11)->first();
            $data['quotation_detail_item12'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 12)->first();
            $data['quotation_detail_item13'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 13)->first();
            $data['quotation_detail_item6'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 9)->first();
            // $allPossibleValues = [10, 22, 11, 12, 13];
            // $existingValues = [];
            // foreach ($data['spa_quotation_detail'] as $detail) {
            //     $existingValues[$detail->fk_lkp_spa_payitem] = $detail;
            // }
            // $newArray = [];
            // foreach ($allPossibleValues as $value) {
            //     $newArray[] = $existingValues[$value] ?? null;
            // }
            // $data['spa_quotation_detail'] = $newArray;
        } catch (\Throwable $th) {
        }
        $data['total'] = 
            ($data['quotation_detail_item10']->total ?? 0) +
            ($data['quotation_detail_item22']->total ?? 0) +
            ($data['quotation_detail_item11']->total ?? 0) +
            ($data['quotation_detail_item12']->total ?? 0) +
            ($data['quotation_detail_item13']->total ?? 0) ;
        $data['total_v'] = ($data['total']) + ($data['quotation_detail_item6']->total ?? 0);
        $data['id'] = $data['main_booking']->id;
        // dd($data);
        return view('event.internal.reservation', compact('data'));
    }

    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }

    public function slotbook(Request $request, $id, $date){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        $location = All::GetRow('et_facility_type', 'id', $id);

        $data = array(
            'fk_users'                   => Session::get('user')['id'],
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('main_booking', $data);

        $slot = $request->post('slot');
        $varCompLocation = 0;
        $i = 0;
        foreach($slot as $s){
            $row = explode(',', $s);
            if($row[0] != $varCompLocation){
                $varCompLocation = $row[0];
                $data_book[$i] = array(
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $id,
                    'fk_et_facility_detail'      => $varCompLocation,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => $row[2],
                    'ebf_end_date'               => $row[2],
                    'ebf_no_of_day'              => 1,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);

                $data_sport[$i] = array(
                    'fk_et_booking_facility'     => $etBookFacility,
                    'esb_booking_date'           => $row[2],
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
            }
            $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
            $total_gst = $row[4] * $gst->lgr_rate;
            $total = $row[4] + $total_gst;

            $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);

            $data_time[$i] = array(
                'fk_et_sport_book'           => $etSportBook,
                'fk_et_slot_price'           => $slotPrice->id,
                'est_price'                  => $row[4],
                'est_discount_type_rm'       => 0.00,
                'est_discount_rm'            => 0.00,
                'est_total'                  => $row[4],
                'est_gst_code'               => $row[5],
                'est_gst_rm'                 => $total_gst,
                'est_subtotal'               => $total,
                'created_at'                 => date('Y-m-d H:i:s'),
                'updated_at'                 => date('Y-m-d H:i:s')
            );
            $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
            $i++;
        }

        if ($etSportTime) {
            return Redirect::to(url('sport/internal/tempahan', [Crypt::encrypt($query)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function tempahan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['slot'] = Sport::priceSlot($data['booking']);

        return view('sport.internal.tempahan', compact('data'));
    }

    public function tempahan_delete($booking, $id){
        $id = Crypt::decrypt($id);
        $query = All::GetDelete('et_sport_time', $id);

        if ($query) {
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport/internal/tempahan', $booking));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/internal/tempahan', $booking));
        }
    }

    public function rumusan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['slot'] = Sport::priceSlot($data['booking']);

        return view('sport.internal.rumusan', compact('data'));
    }

    // public function bayaran(Request $request, $booking)
    // {
    //     $data['booking'] = Crypt::decrypt($booking);
    //     $data['total'] = $request->post('total');
    //     $data1 = array(
    //         'fk_lkp_status'     => 11,
    //         'bmb_subtotal'               => $request->post('total'),
    //         'bmb_rounding'               => 0,
    //         'updated_by'        => Session::get('user')['id'],
    //         'updated_at'        => date('Y-m-d H:i:s')
    //     );
    //     $query = All::GetUpdate('main_booking', $data['booking'], $data1);

    //     if ($query) {
    //         Session::flash('flash', 'Success'); 
    //         return Redirect::to(url('/sport/maklumatbayaran', Crypt::encrypt($data['booking'])));
    //     }else{
    //         Session::flash('flash', 'Failed'); 
    //         return Redirect::to(url('/sport/admin/facilitytype'));
    //     }
    // }

}

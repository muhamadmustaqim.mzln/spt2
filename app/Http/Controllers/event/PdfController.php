<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use PDF;
use App\Models\All;
use App\Models\Sport;
use App\Models\Event;
use App\Helpers\Helper as HP;
use App\Models\AuditLog;
use Carbon\Carbon;

class PdfController extends Controller
{
    public function index($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $first_three = substr($data['main']->bmb_booking_no, 0, 3);
        if($first_three == 'SPS'){
            $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'])->last();
            if($data['online'] == null){
                $data['counter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'])->last();
            }
        } else {
            $data['online'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['booking'])->last();
        }
        $data['location'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);
          
        $pdf = PDF::loadView('sport.pdf.resitonline', $data);
        
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width(); 
        $h = $canvas->get_height(); 

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png'; 
        $imgWidth = 450; 
        $imgHeight = 500; 
         
        // Set image opacity 
        $canvas->set_opacity(.2); 
         
        // Specify horizontal and vertical position 
        $x = (($w-$imgWidth)/2); 
        $y = (($h-$imgHeight)/2); 
         
        // Add an image to the pdf 
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        return $pdf->download('resitonline.pdf');
    }

    
    public function index2($booking, $type){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['slot'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        // $data['slot'] = Sport::priceSlot($data['booking']);
        // $data['online'] = All::GetRow('et_payment_fpx', 'fk_main_booking', $data['booking']);
        $data['spa_payment'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['booking'])->where('fk_lkp_payment_type', $type)->where('deleted_at', NULL)->first();
        // $data['online'] = All::JoinTwoTableWithWhereIdBooking('spa_payment_detail', 'spa_payment', 'fk_main_booking', $data['booking'])->where('fk_lkp_payment_type', $type);
        $data['online'] = All::GetAllRow2('spa_payment_detail', 'fk_spa_payment', $data['spa_payment']->id)->where('fk_lkp_payment_type', $type)->first();
        $data['location'] = All::GetRow('spa_location', 'id', $data['slot']->fk_spa_location);
        $data['type'] = HP::get_jenis_bayaran($type);
        // dd($data, $type);
          
        $pdf = PDF::loadView('event.pdf.resitonlineacara', $data);
        
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width(); 
        $h = $canvas->get_height(); 

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png'; 
        $imgWidth = 450; 
        $imgHeight = 500; 
         
        // Set image opacity 
        $canvas->set_opacity(.2); 
         
        // Specify horizontal and vertical position 
        $x = (($w-$imgWidth)/2); 
        $y = (($h-$imgHeight)/2); 
         
        // Add an image to the pdf 
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        $type = ucfirst(strtolower(HP::get_jenis_bayaran($type)));
        $audit = AuditLog::log(Crypt::encrypt(168), Crypt::encrypt($data['booking']), 'Sukan - Memuat Turun Resit',1);
        return $pdf->download('Resit '. $type .' Acara.pdf');
        // return $pdf->download('resitonline.pdf');
    }

    public function rumusanBorangTempahan($booking) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main_booking'] = ALL::GetRow('main_booking', 'id', $data['booking']);
        $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_organiser'] = ALL::GetRow('spa_booking_organiser', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_attachment'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $data['main_booking']->id, 'id', 'ASC');
        $data['bookingId'] = $data['booking'];

        // dd($data);
        $pdf = PDF::loadView('event.pdf.borangtempahan', compact('data'));
        
        $pdf->setPaper('P');
        $pdf->output();

        // stream = view file, download = download file
        return $pdf->stream('borangtempahan.pdf', compact('data'));
    }

    public function janaSurat($noBorang) {
        $data['getbmbno'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $noBorang)->first();
        $data['getbmbno']->ls_description = (self::get_status_tempahan($data['getbmbno']->fk_lkp_status));
        $data['booking_event'] = All::GetSpecRow('spa_booking_event', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['booking_event']->locationname = (self::locationspa($data['booking_event']->fk_spa_location));
        $data['booking_person'] = All::GetSpecRow('spa_booking_person', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['booking_person']->lc_description = (self::country($data['booking_person']->fk_lkp_country));
        $data['meeting_result'] = All::GetSpecRow('spa_meeting_result', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['quotation_detail'] = Event::spa_quotation_detail_all($data['getbmbno']->id);
        $data['spa_quotation_detail_bpsk'] = Event::spa_quotation_detail_bpsk($data['getbmbno']->id);
        $data['spa_deposit_kecuali'] = Event::spa_deposit_kecuali($data['getbmbno']->id);
        $data['spa_quotation_detail_all_count'] = Event::spa_quotation_detail_all_count($data['getbmbno']->id);
        $data['sumtotal'] = Event::sumtotal($data['getbmbno']->id);
        $data['sumtotal_all'] = Event::sumtotal_all($data['getbmbno']->id);
        // dd($data);
        $pdf = PDF::loadView('event.pdf.surat', compact('data'));
        return $pdf->stream('Surat Kelulusan, Akuan Terima dan Aku Janji.pdf', compact('data'));
    }

    public function janaSebutHarga($id){
        $id = Crypt::decrypt($id);
        // if(Session::get('user.id') == 61894 || Session::get('user.id') == 50554){
        $data['getbmbno'] = All::GetSpecRow('main_booking', 'id', $id)->first();
        $data['getbmbno']->ls_description = (self::get_status_tempahan($data['getbmbno']->fk_lkp_status));
        $data['user'] = All::GetRow('users', 'id', $data['getbmbno']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['user']->id);
        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['getbmbno']->id);
        // $data['et_sport_book'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        // $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['et_sport_book'] = All::GetAllRowJoin2('et_sport_book', 'et_sport_time', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['et_hall_book'] = All::GetAllRowJoin2('et_hall_book', 'et_hall_time', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        if(count($data['et_sport_book']) == 0){
            $data['checkfacility'] = 1;
            $data['datatambahan'] = $data['et_hall_book'];
        } else {
            $data['checkfacility'] = 2;
            $data['datatambahan'] = $data['et_sport_book'];
        }
        
        $data['et_booking_facility_detail'] = All::GetAllRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['datatambahaneqp'] = $data['et_equipment_book'];

        $data['bh_quotation'] = All::GetAllRow('bh_quotation', 'fk_main_booking', $id)->first();
        // dd($data);
        $pdf = PDF::loadView('sport.pdf.sebutharga', compact('data'));
        // return $pdf->stream('sport.pdf.sebutharga.pdf', compact('data'));
        $audit = AuditLog::log(Crypt::encrypt(169), Crypt::encrypt($id), 'Sukan - Memuat Turun SebutHarga',1);
        return $pdf->download('Sebut harga.pdf');
    }

    public function kutipanharianpdf($location, $tarikh, $typebayaran) {
        $location = Crypt::decrypt($location);
        $tarikh = Crypt::decrypt($tarikh);
        $typebayaran = Crypt::decrypt($typebayaran);
        $data['id'] = $location;
        $data['mode'] = $typebayaran;
        $data['tarikh'] = $tarikh;
        $data['harian'] = Sport::kutipanharian($data['id'], $data['tarikh'], $data['mode']);
        $data['dayOfWeek'] = Carbon::parse($data['tarikh'])->locale('ms')->isoFormat('dddd');
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		// dd($data);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('sport.pdf.kutipanharian', compact('data'));
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($tarikh), 'Laporan Sukan - Eksport PDF',null);
        // dd($pdf);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Kutipan Harian Kompleks Sukan.pdf');
    }

    public function get_status_tempahan($id)
    {
        $data = all::GetSpecRow('lkp_status', 'id', $id);
        foreach($data as $d){
            $data = $d->ls_description;
        }
        return $data;
    }
    public function locationspa($id)
    {
        $data = all::GetRow('spa_location', 'id', $id);
        return $data->name;
    }
    public function country($id)
    {
        $data = all::GetRow('lkp_country', 'id', $id);
        return $data->lc_description;
    }
}

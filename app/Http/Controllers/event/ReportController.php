<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Event;
use Carbon\Carbon;
use App\Models\AuditLog;

class ReportController extends Controller
{
    public function index(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Event::laporan_acara($data['id'], $data['mula'], $data['tamat']);
            foreach ($data['slot'] as $key => $value) {
                $user = All::GetSpecRow('user_profiles', 'bud_email', $value->email)->first();
            
                if ($user) {
                    $data['slot'][$key]->bud_phone_no = $user->bud_phone_no;
                    $data['slot'][$key]->bud_reference_id = $user->bud_reference_id;
                } else {
                    $data['slot'][$key]->bud_phone_no = 'Unknown Number'; 
                    $data['slot'][$key]->bud_reference_id = 'Unknown Reference Id';
                }
            }
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
            // $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
            // dd($data);
        }
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        // $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('event.report.index', compact('data'));
    }

    public function pelanggan(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('event.report.pelanggan', compact('data'));
    }

    public function hasil(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $dates = explode(" / ", $data['tarikh']);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mode'] = $request->post('bayaran');
            $data['tarikh'] = $request->post('tarikh');
            $dates = explode(" / ", $data['tarikh']);
            if (count($dates) === 2) {
                $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
            }
            $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
            $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
            $data['slot'] = Sport::hasil($data['id'], $data['mode'], $startDate, $endDate);
        }
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        return view('event.report.hasil', compact('data'));
    }

    public function acara(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        // $data['dataacara'] = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $tarikh = explode(" / ", $request->post('tarikh'));
            $tarikh[0] = request()->mula;
            $tarikh[1] = request()->tamat;
            $data['dataacara'] = Event::listacara($tarikh[0], $tarikh[1], $data['id'], 0, '');
        }else {
            $data['dataacara'] = Event::listacara($data['mula'], $data['tamat'], $data['id'], 0, '');
        }
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['status'] = All::ShowAll('lkp_status', 'ls_description', 'ASC')->whereIn('id', [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]);

        // dd($data);
        return view('event.report.acara', compact('data'));
    }

    public function jumlahpengunjung(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        $dataacara = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $tarikh = explode(" / ", $request->post('tarikh'));
            // $data['mula'] = $tarikh[0];
            // $data['tamat'] = $tarikh[1];
            $tarikh[0] = request()->mula;
            $tarikh[1] = request()->tamat;
            $dataacara = Event::listacara($tarikh[0], $tarikh[1], $data['id'], 0, '');
        }
        $data['location'] = Event::location2();
        $data['dataspalocation'] = All::ShowAll('spa_location', 'id', 'ASC');
        // dd($data);

        return view('event.report.jumlahpengunjung', compact('data', 'dataacara'));
    }
    
    public function jumlahacara(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        $dataacara = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $tarikh = explode(" / ", $request->post('tarikh'));
            // $data['mula'] = $tarikh[0];
            // $data['tamat'] = $tarikh[1];
            $tarikh[0] = request()->mula;
            $tarikh[1] = request()->tamat;
            $dataacara = Event::listacara($tarikh[0], $tarikh[1], $data['id'], 0, '');
        }
        $data['tarikh'] = $data['mula'] . ' / ' . $data['tamat'];
        $data['location'] = Event::location2();
        $data['dataspalocation'] = All::ShowAll('spa_location', 'id', 'ASC');
        // dd($data);

        return view('event.report.jumlahacara', compact('data', 'dataacara'));
    }

    public function harian(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		
        return view('event.report.harian', compact('data'));
    }

	// public function penggunaan(Request $request) {
        
    //     $data['location'] = All::ShowAuditTrail();
    //     return view('event.report.penggunaan', compact('data'));
    // }

    public function penggunaan(Request $request, $id = null) {
        $data['post'] = false;
        $data['type'] = 1;
		$data['search'] = "";
        $data['log'] = [];
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['type'] = request()->type;
		    $data['search'] = $request->post('search');
            $search = $request->post('search');
            if($data['type'] == 2){
                $data['users'] = All::GetAllRowLike('users', 'fullname', $search);
            } elseif($data['type'] == 3) {
                $data['log'] = Sport::laporan_penggunaan($search);
            }
        } 
        if($id != null){
            $data['post'] = true;
            $id = Crypt::decrypt($id);
            $data['log'] = All::GetAllRow('audit_trail', 'fk_users', $id, 'id', 'DESC');
            $data['type'] = 3;
        }

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Audit Acara - Carian Log Audit',null);
        return view('event.report.usage', compact('data'));
    }

    public function audit(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetAllRow('main_booking', 'bmb_booking_no', $data['id']);
            dd($data);

            if(count($data['main']) > 0){
                $data['main'] = $data['main']->first();
                $data['users'] = All::GetRow('users', 'id', $data['main']->fk_users);
                $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
                $data['spa_booking'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['main']->id);
                $data['spa_booking_confirm'] = All::GetRow('spa_booking_confirm', 'fk_main_booking', $data['main']->id);
                // $data['et_hall_book'] = All::GetAllRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking']->id);
                // $data['et_sport_book'] = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $data['et_booking']->id);
                $data['spa_payment'] = All::GetAllRow('spa_payment', 'fk_main_booking', $data['main']->id);   // Dewan bawah Sukan
                $data['spa_payment_fpx'] = All::GetAllRow('spa_payment_fpx', 'fk_main_booking', $data['main']->id);   // Online Transaction
                $data['audit_log'] = All::GetAllRow('audit_trail', 'table_ref_id', $data['main']->id)->where('deleted_at', null);
            } else {
                $data['post'] = false;
                Session::flash('flash', 'Non-existant'); 
            }
            dd($data);
        }
        
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Audit Acara - Carian Maklumat Audit Trail',null);
        return view('event.report.audit', compact('data'));
    }
}

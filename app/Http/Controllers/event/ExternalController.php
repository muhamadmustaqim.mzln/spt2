<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use App\Models\All;
use App\Models\Auth;
use App\Models\Event;
use Carbon\Carbon;

class ExternalController extends Controller
{
    public function index(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('nama');
            $data['user'] = Auth::user($data['id']);
            $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            // dd($data['user']);
        }
        // if (Session::get('user')['id'] == "50554")
        // if (Session::get('user')['role'] == 1)
		// {
			return view('event.external.index', compact('data'));
		// } else {
        //     return redirect()->back();
        // }
    }

    public function event(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['id']);
        // $data['main_booking'] = All::JoinTwoTableWithWhere('spa_booking_event', 'main_booking', 'fk_users', $data['id']);
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC');
        $data['post'] = false;
        $data['lokasi'] = "";
        $data['tarikhDari'] = "";
        $data['tarikhHingga'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['lokasi'] = $request->post('lokasi');
            $data['tarikhDari'] = $request->post('tarikhDari');
            $data['tarikhHingga'] = $request->post('tarikhHingga');
            $tarikhDari = Carbon::createFromFormat('d-m-Y', $request->post('tarikhDari'))->format('Y-m-d');
            $tarikhHingga = Carbon::createFromFormat('d-m-Y', $request->post('tarikhHingga'))->format('Y-m-d');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['lokasi']);
            $data['slot'] = All::GetRowDateRange('spa_booking_event', 'fk_spa_location', $data['lokasi'], $tarikhDari, $tarikhHingga);
            if (count($data['slot']) != 0){
                $data['main_booking'] = All::GetSpecRow('main_booking', 'id', $data['slot']->first()->fk_main_booking)->first();
            }
            // dd($data);
            // $data['slot'] = Event::duplicate($data['lokasi'], $data['tarikhDari'], $data['tarikhHingga']);
            // $data['slot'] = Event::duplicate_all2();
        }
        
        return view('event.external.event', compact('data'));
    }

    public function addevent(Request $request, $locationId, $dateFrom, $dateUntil, $email = null) {
        $data['jenis'] = 2;
        if(request()->isMethod('post')){
        // dd('test');
        }
        $data['lokasi'] = Crypt::decrypt($locationId);
        $data['dateFrom'] = Crypt::decrypt($dateFrom);
        $data['dateUntil'] = Crypt::decrypt($dateUntil);
        $email = Crypt::decrypt($email);
        if ($email != null) {
            $data['user'] = All::GetRow('user_profiles', 'bud_email', $email);
        }
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC');

        return view('event.external.addevent', compact('data'));
    }

    public function addeventsubmit(Request $request) {
        $data['user_id'] = All::GetSpecRow('users', 'email', $request->post('email'))->first()->id;

        $running =  Event::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPA'.$year.$md.$rn;

        $main_bookingData = array(
            'fk_users'                  => $data['user_id'],
            'fk_lkp_status'             => 19,
            'bmb_booking_no'            => $bookrn,
            'bmb_type_user'             => $request->post('jenisAcara'),
            'bmb_booking_date'          => date('Y-m-d'),
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s'),
            'updated_by'                => Session::get('user')['id'],
        );
        $main_bookingId = All::InsertGetID('main_booking', $main_bookingData);

        $spa_booking_personData = array(
            'fk_main_booking'           => $main_bookingId,
            'name'                      => $request->post('namaPegawai'),
            'post'                      => $request->post('jawatan'),
            'address'                   => $request->post('alamat'),
            'postcode'                  => $request->post('poskod'),
            'city'                      => $request->post('bandar'),
            'fk_lkp_state'              => $request->post('negeri'),
            'fk_lkp_country'            => $request->post('negara'),
            'ic_no'                     => $request->post('reference_id'),
            'office_no'                 => $request->post('noTelPejabat1'),
            'hp_no'                     => $request->post('noBimbit'),
            'fax_no'                    => $request->post('noFaks') ?? '',
            'email'                     => $request->post('email'),
            'applicant_status'          => $request->post('statusPemohon'),
            'applicant_type'            => $request->post('jenisPemohon'),
            // 'contact_person'            => $contactPerson,
            // 'GST_no'                    => $nogst,
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s'),
        );        
        All::InsertGetID('spa_booking_person', $spa_booking_personData);

        $tarikhAcaraDari = Carbon::createFromFormat('d M Y', $request->post('tarikhAcaraDari'))->format('Y-m-d');
        $tarikhAcaraHingga = Carbon::createFromFormat('d M Y', $request->post('tarikhAcaraHingga'))->format('Y-m-d');
        $tarikhMasukTapak = Carbon::createFromFormat('d M Y', $request->post('tarikhMasukTapak'))->format('Y-m-d');
        $tarikhKeluarTapak = Carbon::createFromFormat('d M Y', $request->post('tarikhKeluarTapak'))->format('Y-m-d');
        $masaMasukTapak = Carbon::createFromFormat('g:i A', $request->post('masaMasukTapak'))->format('H:i:s');
        $masaKeluarTapak = Carbon::createFromFormat('g:i A', $request->post('masaKeluarTapak'))->format('H:i:s');
        $masaPenganjuran = Carbon::createFromFormat('g:i A', $request->post('masaPenganjuran'))->format('H:i:s');

        $totalDays = (Carbon::parse($tarikhAcaraDari))->diffInDays(Carbon::parse($tarikhAcaraHingga)) + 1;

        $spa_booking_eventData = array(
            'fk_main_booking'           => $main_bookingId,
            'fk_spa_location'           => $request->post('lokasiAcara'),
            'event_name'                => $request->post('namaAcara'),
            'visitor_amount'            => $request->post('jumlahPesertaPengunjung'),
            'event_date'                => $tarikhAcaraDari,
            'event_time'                => $masaPenganjuran,
            // 'event_time_to'             => (new DateTime($request->post('masaPenganjuranHingga')))->format('H:i:s'),
            'event_date_end'            => $tarikhAcaraHingga,
            'enter_date'                => $tarikhMasukTapak,
            'enter_time'                => $masaMasukTapak,
            'exit_date'                 => $tarikhKeluarTapak,
            'exit_time'                 => $masaKeluarTapak,
            'total_day'                 => $totalDays,
            'drone'                     => $request->post('drone'),
            'vip_name'                  => $request->post('namaTetamuKehormat'),
            'remark'                    => $request->post('maklumatTambahan'),
            'booking_verification'      => 1,
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s'),
        );
        $query = All::InsertGetID('spa_booking_event', $spa_booking_eventData);

        $id = $request->post('lokasiAcara');

        if($query){
            Session::flash('flash', 'Anda berjaya menambah acara.'); 
            return redirect('/event/list/reservation');
        } else {
            Session::flash('flash', 'Anda tidak berjaya menambah acara.'); 
            return redirect('/event/list/reservation');
        }
        // return redirect()->route('event.summary', ['id' => Crypt::encrypt($id), 'bookingId' => $main_bookingId]);
    }

    public function hall($id){
        $data['id'] = Crypt::decrypt($id);
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['location'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['location']);
            $data['slot'] = Sport::slot($data['id'], $data['date']);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		
		if (Session::get('user')['id'] == "50554")
		{
			return view('event.reschedule.index', compact('data'));
		}
    }

    public function sport(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['id']);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $data['post'] = false;
        $data['lokasi'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['lokasi'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['lokasi']);
            $data['slot'] = Sport::slot($data['lokasi'], $data['date']);
        }
        return view('event.external.sport', compact('data'));
    }

    public function sport_slotbook(Request $request, $id, $date, $user){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $user = Crypt::decrypt($user);

        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        $location = All::GetRow('et_facility_type', 'id', $id);

        $data = array(
            'fk_users'                   => $user,
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id']
        );
        $query = All::InsertGetID('main_booking', $data);

        $slot = $request->post('slot');
        $varCompLocation = 0;
        $i = 0;
        foreach($slot as $s){
            $row = explode(',', $s);
            if($row[0] != $varCompLocation){
                $varCompLocation = $row[0];
                $data_book[$i] = array(
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $id,
                    'fk_et_facility_detail'      => $varCompLocation,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => $row[2],
                    'ebf_end_date'               => $row[2],
                    'ebf_no_of_day'              => 1,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);

                $data_sport[$i] = array(
                    'fk_et_booking_facility'     => $etBookFacility,
                    'esb_booking_date'           => $row[2],
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
            }
            $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
            $total_gst = $row[4] * $gst->lgr_rate;
            $total = $row[4] + $total_gst;

            $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);

            $data_time[$i] = array(
                'fk_et_sport_book'           => $etSportBook,
                'fk_et_slot_price'           => $slotPrice->id,
                'est_price'                  => $row[4],
                'est_discount_type_rm'       => 0.00,
                'est_discount_rm'            => 0.00,
                'est_total'                  => $row[4],
                'est_gst_code'               => $row[5],
                'est_gst_rm'                 => $total_gst,
                'est_subtotal'               => $total,
                'created_at'                 => date('Y-m-d H:i:s'),
                'updated_at'                 => date('Y-m-d H:i:s')
            );
            $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
            $i++;
        }

        if ($etSportTime) {
            return Redirect::to(url('event/external/tempahan', [Crypt::encrypt($query)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function tempahan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);

        return view('event.external.tempahan', compact('data'));
    }

    public function tempahan_delete($booking, $id){
        $id = Crypt::decrypt($id);
        $query = All::GetDelete('et_sport_time', $id);

        if ($query) {
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/event/external/tempahan', $booking));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/external/tempahan', $booking));
        }
    }

    public function rumusan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);

        return view('event.external.rumusan', compact('data'));
    }

    public function bayaran(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);
        $data['total'] = $request->post('total');
        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        return view('event.external.bayaran', compact('data'));
    }

    public function submit_bayaran(Request $request, $booking)
    {
        $no = Event::generatereceipt();
        $data['booking'] = Crypt::decrypt($booking);
        $data['total'] = $request->post('total');
        $data_fpx = array(
            'fk_main_booking'               => $data['booking'],
            'fk_lkp_payment_type'           => 2,
            'fk_lkp_payment_mode'           => $request->post('bayaran'),
            'fk_users'                      => Session::get('user')['id'],
            'bp_total_amount'               => $data['total'],
            'bp_deposit'                    => 0,
            'bp_paid_amount'                => $data['total'],
            'bp_receipt_number'             => sprintf('%07d', $no),
            'bp_payment_ref_no'             => $request->post('ref'),
            'bp_receipt_date'               => date('Y-m-d H:i:s'),
            'amount_received'               => $data['total'],
            'bp_subtotal'                   => $data['total'],
            'bp_payment_status'             => 1,
            'created_at'                    => date('Y-m-d H:i:s'),
            'updated_at'                    => date('Y-m-d H:i:s'),
            'updated_by'                    => Session::get('user')['id'],
        );
        $query = All::InsertGetID('bh_payment', $data_fpx);

        // $data_ebf = array(
        //     'ebf_subtotal'                  => $data['total'],
        //     // 'ebf_deposit'                   => ,
        //     'updated_at'                    => date('Y-m-d H:i:s'),
        //     'updated_by'                    => Session::get('user')['id'],
        // );
        // $query = All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $data['booking'], $data_ebf);

        $data_main = array(
            'fk_lkp_status'              => 5,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/maklumatbayaran', Crypt::encrypt($data['booking'])));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/facilitytype'));
        }
    }

    public function equipment($id){
        $data['id'] = Crypt::decrypt($id);
        return view('event.reschedule.index', compact('data'));
    }

}

<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\MailController as Mail;
use App\Models\All;
use App\Models\Sport;
use App\Models\Event;
use App\Models\AuditLog;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function event() {
        if (!Session::get('user')) {
            return redirect('/auth');
        }
        $data['laporan_keseluruhan'] = Event::laporan_keseluruhan();
        $data['laporan_baharu'] = Event::laporan_baharu();
        $data['keputusan_mesyuarat'] = Event::keputusan_mesyuarat();
        $data['laporan_pembentangan_semula'] = Event::laporan_pembentangan_semula();
        $data['laporan_perlu_maklumat_bayaran'] = Event::laporan_perlu_maklumat_bayaran();
        $data['laporan_perlu_bayaran_pendahuluan'] = Event::laporan_perlu_bayaran_pendahuluan();
        $data['laporan_pendahuluan_diterima'] = Event::laporan_pendahuluan_diterima();
        $data['laporan_perlu_bayaran_penuh'] = Event::laporan_perlu_bayaran_penuh();
        $data['laporan_bayaran_penuh_diterima'] = Event::laporan_bayaran_penuh_diterima();
        $data['laporan_kelulusan'] = Event::laporan_kelulusan();
        $data['laporan_harian'] = Event::laporan_harian();
        $data['laporan_user'] = Event::laporan_user(Session::get('user')['id']);
        $data['laporan_tindih'] = Event::duplicate_all();

        return view('event.dashboard.index', compact('data'));
    } 

    public function event_info(Request $request, $permohonan) {
        $data['post'] = false;
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        // $data['event'] = Event::getlistDalaman();
        $data['event'] = Event::getlistDalaman();
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['startDate'] = Carbon::createFromFormat('d M Y', $request->post('tarikhDari'))->toDateString();
            $startDate = $data['startDate'];
            $data['endDate'] = Carbon::createFromFormat('d M Y', $request->post('tarikhHingga'))->toDateString();
            $endDate = $data['endDate'];
            // $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
            // $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
            // $datesBetween = $this->getDatesBetween($startDate, $endDate);
            // $data['slot'] = [];
        }
        $permohonanVal = 0;
        if ($permohonan == 'baharu') {
            $data['title'] = 'Baharu';
            $permohonanVal = 19;
        } else if ($permohonan == 'keputusanMesyuarat') {
            $data['title'] = 'Keputusan Mesyuarat';
            $permohonanVal = 22;
        } else if ($permohonan == 'pembentanganSemula') {
            $data['title'] = 'Pembentangan Semula';
            $permohonanVal = 23;
        } else if ($permohonan == 'perluMaklumatBayaran') {
            $data['title'] = 'Perlu Maklumat Bayaran';
            $permohonanVal = 30;
        } else if ($permohonan == 'perluBayaranPendahuluan') {
            $data['title'] = 'Perlu Bayaran Pendahuluan';
            $permohonanVal = 24;
        } else if ($permohonan == 'pendahuluanDiterima') {
            $data['title'] = 'Pendahuluan Diterima';
            $permohonanVal = 25;
        } else if ($permohonan == 'perluBayaranPenuh') {
            $data['title'] = 'Perlu Bayaran Penuh';
            $permohonanVal = 26;
        } else if ($permohonan == 'bayaranPenuhDiterima') {
            $data['title'] = 'Bayaran Penuh Diterima';
            $permohonanVal = 27;
        } else if ($permohonan == 'hari_ini'){
            $data['title'] = 'Hari Ini';
            $today = date('Y-m-d');
            $data['laporan_harian'] = Event::laporan_harian();
            $data['event'] = All::JoinThreeTablesWithWhereDate('spa_booking_event', 'main_booking', 'spa_booking_person', 'created_at', $today);
        } else {
            $data['title'] = 'Keseluruhan';
            $permohonanVal = 0;
            $data['event'] = All::JoinThreeTables('spa_booking_event', 'main_booking', 'spa_booking_person')->where('fk_lkp_status', '!=', 18);
        }
        if ($permohonan != 'all' && $permohonan != 'hari_ini') {
            $data['event'] = All::JoinThreeTablesWithWhere('spa_booking_event', 'main_booking', 'spa_booking_person', 'fk_lkp_status', $permohonanVal);
        }
        $data['location'] = All::GetSpecRow('spa_location', 'fee_code', '1022');
        // foreach($data['event'] as $e) {
        //     foreach ($data['location'] as $l) {
        //         if ($e->fk_spa_location == $l->id) {
        //             $e->fk_spa_location = $l->name;
        //         }
        //     }
        // }
        $data['event'] = $data['event']->sortByDesc('id');

        return view('event.dashboard.event_info', compact('data'));
    }
    
	public function reservation(Request $request, $bookingId) {
        $data['negeri'] = All::Show('lkp_state', 'id', 'ASC');
        $data['negara'] = All::Show('lkp_country', 'id', 'ASC');
        
        $bookingId = Crypt::decrypt($bookingId);
        $data['bookingId'] = $bookingId;
        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)->first()->id;
        $data['userId'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->fk_users;
        $data['user'] = All::GetRow('users', 'id', $data['userId']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['userId']);
        $userEmail = All::GetRow('users', 'id', $data['userId'])->email;
        if($userEmail == 'dzulhilmi@turbine-group.com'){
            $userEmail = 'muhamadmustaqim.mzln@gmail.com';
        }

        $id = All::GetSpecRow('main_booking', 'bmb_booking_no', $bookingId)[0]->id;

        if(request()->isMethod('post')){
            // main_booking, spa_booking_review [22 notifyterima, 28 , 29 gagal]
            if($request->post('tab') == 'tindakanSemakan') {
                $lkp_status = ($request->post('statusSemakanPermohonan') == 1) ? 22 : (($request->post('statusSemakanPermohonan') == 2) ? 28 : 29);
                if($request->post('statusSemakanPermohonan') == 1) {
                    $lkp_status = 22;
                    $audit = AuditLog::log(Crypt::encrypt(114), Crypt::encrypt($id), 'Permohonan diterima - '.$bookingId, 1);
                } else if ($request->post('statusSemakanPermohonan') == 2) {
                    $lkp_status = 28;
                    $audit = AuditLog::log(Crypt::encrypt(115), Crypt::encrypt($id), 'Maklumat Permohonan tidak lengkap - '.$bookingId, 1);
                } else if ($request->post('statusSemakanPermohonan') ==  3) {
                    $lkp_status = 29;
                    $audit = AuditLog::log(Crypt::encrypt(116), Crypt::encrypt($id), 'Permohonan gagal/ ditolak - '.$bookingId, 1);
                }
                $data['tindakanSemakan'] = [
                    'bmb_booking_no'    => $bookingId,
                    'date'              => $request->post('tarikhSemakan'),
                    'time'              => $request->masaSemakan,
                    'location'          => $request->location,
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'remark'            => $request->post('maklumatTambahan'),
                    'maklumatKemasKini' => $request->post('maklumatKemasKini')
                ];
                Mail::acara($userEmail, $lkp_status, $data['tindakanSemakan'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));

                $mainBookingData = array(
                    'fk_lkp_status' => $lkp_status,
                    'updated_at'    => now()
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);
                
                $tarikhSemakan = $request->post('tarikhSemakan');
                $masaSemakan = $request->post('masaSemakan');
                if ($tarikhSemakan != null) {
                    $tarikhSemakan = Carbon::parse(Carbon::createFromFormat('d-m-Y', "$tarikhSemakan")->toDateTimeSTring())->format('Y-m-d');
                }
                $masaSemakan = Carbon::parse(Carbon::createFromFormat('g:i A', "$masaSemakan")->toDateTimeSTring())->format('H:i:s');
                
                $spa_booking_reviewData = array(
                    'fk_main_booking'   => $id,
                    'fk_user'           => $request->post('fk_user'),
                    'fk_lkp_spa_review' => ($request->post('statusSemakanPermohonan') == 1) ? 5 : (($request->post('statusSemakanPermohonan') == 2) ? 7 : 17),
                    'date'              => $tarikhSemakan,
                    'time'              => $masaSemakan,
                    'remark'            => $request->post('maklumatTambahan'),
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'location'          => $request->post('location'),
                    'created_at'        => date('Y-m-d'),
                    'updated_at'        => date('Y-m-d')
                );
                All::Insert('spa_booking_review', $spa_booking_reviewData);
            }
            // main_booking, spa_meeting_result [30 lulus, 29 gagal, 23 pembentangan semula]
            else if($request->post('tab') == 'tindakanKeputusanMesyuarat') {
                $lkp_status = ($request->post('fk_lkp_status_meeting') == 3) ? 30 : (($request->post('fk_lkp_status_meeting') == 2) ? 29 : 23);
                $fileName = 'JKK PPj Minit Mesyuarat.pdf';
                if ($request->hasFile('minitMesyuarat')) {
                    $file = $request->file('minitMesyuarat');
                    $fileSize = $file->getSize();
        
                    $destinationPath = public_path('dokumen/acara/'. $id);
                    // $destinationPath = ('/dokumen/acara/'. $id);
                    $file->move($destinationPath, $fileName );
        
                    // Optionally, you can also save the file path to the database
                    // $filePath = 'dokumen/acara/' . $fileName;
                    // Save $filePath to the database if needed
                    $booking_attachmentData = [
                        'fk_main_booking'       => $id,
                        'fk_lkp_spa_filename'   => 8,
                        'name'                  => $fileName,
                        'size'                  => $fileSize,
                        'location'              => '/dokumen/acara/'. $id . '/' . $fileName,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'updated_by'            => session('user.id'),
                        'deleted_at'            => null
                    ];
                    All::Insert('spa_booking_attachment', $booking_attachmentData);
                }
                if ($lkp_status != 23) {
                    $data['tindakanSemakan'] = [
                        'bmb_booking_no'    => $bookingId,
                        'date_meeting'      => $request->post('date_meeting'),
                        'meeting_no'        => $request->meeting_no,
                        'remark'            => $request->post('remark')
                    ];
                } else if ($lkp_status != 29) {
                    $data['tindakanSemakan'] = [
                        'bmb_booking_no'    => $bookingId,
                        'date'              => $request->post('tarikhSemakan'),
                        'time'              => $request->masaSemakan,
                        'location'          => $request->location,
                        'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                        'remark'            => $request->post('maklumatTambahan'),
                        'ulasan'            => $request->post('ulasan'),
                        'maklumatKemasKini' => $request->post('maklumatKemasKini')
                    ];
                }
                if ($lkp_status != 30) {
                    Mail::acara($userEmail, $lkp_status, $data['tindakanSemakan'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));
                }

                // $validate = ALL::GetSpecRow('spa_meeting_result', 'meeting_no', $request->post('meeting_no'));
                // if (!$validate || $validate){
                $mainBookingData = array(
                    'fk_lkp_status' => $lkp_status,
                    'updated_at'    => now()
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);
                $spa_meeting_result = array(
                    'fk_main_booking'           => $id,
                    'fk_user'                   => Session::get('user')['id'],
                    'fk_spa_booking_attachment' => null,
                    'date_meeting'              => Carbon::createFromFormat('d-m-Y', $request->post('date_meeting'))->format('Y-m-d'),
                    'meeting_no'                => $request->post('meeting_no'),
                    'remark'                    => $request->post('remark'),
                    'fk_lkp_status_meeting'     => ($request->post('fk_lkp_status_meeting') == 3) ? 30 : (($request->post('fk_lkp_status_meeting') == 2) ? 29 : 23),
                    'created_at'                => now(),
                    'updated_at'                => now()
                );
                All::Insert('spa_meeting_result', $spa_meeting_result);
                // }
            }
            // main_booking, spa_quotation_detail, spa_booking_event [24]
            else if ($request->post('tab') == 'tindakanMaklumatBayaran') {
                $lkp_status = 24;
                $data['tindakanMaklumatBayaran'] = [
                    'bmb_booking_no'    => $bookingId,
                    'date'              => $request->post('tarikhSemakan'),
                    'time'              => $request->masaSemakan,
                    'location'          => $request->location,
                    'meeting_chairman'  => $request->namaPengerusiMesyuarat,
                    'remark'            => $request->post('maklumatTambahan')
                ];
                try{
                    Mail::acara($userEmail, $lkp_status, $data['tindakanMaklumatBayaran'], $request->post('statusSemakanPermohonan') == 1 ? 'diterima' : ($request->post('statusSemakanPermohonan') == 2 ? 'gagal' : 'tidakLengkap'));
                } catch (\Throwable $th) {
                }
                $mainBookingData = array(
                    'fk_lkp_status'     => 24,
                    'bmb_booking_date'  => date('Y-m-d'),
                    'bmb_subtotal'      => ($request->post('jumlahKeseluruhan') - $request->post('cajDeposit')),
                    'bmb_total_word'    => $request->post('jumlahDeposit'),
                    'bmb_deposit_rm'    => $request->post('cajDeposit'),
                    'updated_at'        => date('Y-m-d')
                );
                All::GetUpdate('main_booking', $id, $mainBookingData);

                $fk_lkp_spa_payitem = [10, 22, 11, 12, 13];

                $jumlah = ['sewaTapak_jumlah', 'sewaTasik_jumlah', 'laluanBawah50_jumlah', 'laluanAtas50_jumlah', 'bukanMilikPPj_jumlah'];
                $kadar = ['sewaTapak_kadar', 'sewaTasik_kadar', 'laluanBawah50_kadar', 'laluanAtas50_kadar', 'bukanMilikPPj_kadar'];
                $kuantiti = ['sewaTapak_kuantiti', 'sewaTasik_kuantiti', 'laluanBawah50_kuantiti', 'laluanAtas50_kuantiti', 'bukanMilikPPj_kuantiti'];
                $diskaunRM = ['sewaTapak_diskaun', 'sewaTasik_diskaun', 'laluanBawah50_diskaun', 'laluanAtas50_diskaun', 'bukanMilikPPj_diskaun'];
                $diskaunPer = ['sewaTapak_diskaunPer', 'sewaTasik_diskaunPer', 'laluanBawah50_diskaunPer', 'laluanAtas50_diskaunPer', 'bukanMilikPPj_diskaunPer'];

                $hasValue = array_map(function ($item) use ($request) {
                    // Convert the value to a float before comparison
                    return floatval($request->post($item)) > 0;
                }, $kadar, $kuantiti, $diskaunRM,$diskaunPer);
                
                foreach ($hasValue as $index => $hv) {
                    if ($hv) {
                        $spa_quotation_detail = [
                            'fk_main_booking'   => $id,
                            'fk_user'           => Session::get('user')['id'],
                            'fk_lkp_spa_payitem' => $fk_lkp_spa_payitem[$index],
                            'rate'              => $request->post($kadar[$index]),
                            'quantity'          => $request->post($kuantiti[$index]),
                            'exceptional'       => 0,
                            'discount_rm'       => $request->post($diskaunRM[$index]),
                            'discount_percentage'=> $request->post($diskaunPer[$index]),
                            'gst_rm'            => 0.00,
                            'total'             => $request->post($jumlah[$index]),
                            'created_at'        => now(),
                            'updated_at'        => now(),
                        ];
                        // validate, if not exist yet main_booking_id & fk_lkp_spa_payitem then insert into. Else update value
                        All::Insert('spa_quotation_detail', $spa_quotation_detail);
                    }
                } 
                $spa_quotation_detail2 = [
                    'fk_main_booking'   => $id,
                    'fk_user'           => Session::get('user')['id'],
                    'fk_lkp_spa_payitem'=> 9,
                    'rate'              => $request->post('cajDeposit'),
                    'quantity'          => 1,
                    'exceptional'       => 0,
                    'discount_rm'       => 0.00,
                    'discount_percentage'=> 0.00,
                    'gst_rm'            => 0.00,
                    'total'             => $request->post('cajDeposit'),
                    'created_at'        => now(),
                    'updated_at'        => now(),
                ];
                All::Insert('spa_quotation_detail', $spa_quotation_detail2);

                $spa_booking_eventData = [
                    'letter_no_five'    => $request->post('ayatSuratKelulusan')
                ];
                All::GetUpdateSpec('spa_booking_event', 'fk_main_booking', $id, $spa_booking_eventData);
            }
            else if ($request->post('tab') == 'sah') {
                $main_bookingData = [
                    'fk_lkp_status'     => 26,
                    'updated_at'        => now()
                ];
                All::GetUpdate('main_booking', $id, $main_bookingData);
            }
            $validate = Event::validateData('spa_meeting_result', 'meeting_no', $request->post('meeting_no'));
            $bookingId = Crypt::encrypt($bookingId);
            return redirect()->route('reservationdash.get', ['id' => $bookingId]);
        }

        $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $bookingId);
        $bookingId = $data['main_booking']->id;
        if ($data['main_booking']->fk_lkp_status == null) {
            return redirect()->back();
        }
        $lkp_status = ALL::GetSpecRow('lkp_status', 'id', $data['main_booking']->fk_lkp_status);
        $data['status_tempahan'] = $lkp_status[0]->ls_description;
        $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $bookingId);
        $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $bookingId);
        $data['booking_organiser'] = ALL::GetRow('spa_booking_organiser', 'fk_main_booking', $bookingId);
        
        $data['spa_payment'] = All::GetAllRow('spa_payment', 'fk_main_booking', $bookingId)->where('payment_status', 1)->where('deleted_at', NULL);
        $data['spa_booking_review'] = ALL::GetRowDeleteNull('spa_booking_review', 'fk_main_booking', $bookingId);
        $data['spa_meeting_result'] = ALL::GetAllRow('spa_meeting_result', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $data['spa_booking_event'] = ALL::GetAllRow('spa_booking_event', 'fk_main_booking', $bookingId, 'id', 'ASC');

        $data['spa_booking_attachment'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $latestAttachments = [];
        foreach ($data['spa_booking_attachment'] as $attachment) {
            if (isset($latestAttachments[$attachment->fk_lkp_spa_filename])) {
                // Compare the created_at timestamps to determine which one is the latest
                if ($attachment->created_at > $latestAttachments[$attachment->fk_lkp_spa_filename]->created_at) {
                    // If the current attachment is newer, replace the previous one
                    $latestAttachments[$attachment->fk_lkp_spa_filename] = $attachment;
                }
            } else {
                // If it doesn't exist, add it to $latestAttachments
                $latestAttachments[$attachment->fk_lkp_spa_filename] = $attachment;
            }
        }
        $data['spa_booking_attachment'] = $latestAttachments;

        $data['payment_mode'] = All::Show('lkp_payment_mode')->where('id', '=', 2);
        $data['payment_mode'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);

        $data['spa_quotation_detail'] = ALL::GetAllRow('spa_quotation_detail', 'fk_main_booking', $bookingId, 'id', 'ASC');
        $data['quotation_detail_item10'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 10)->first();
        $data['quotation_detail_item22'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 22)->first();
        $data['quotation_detail_item11'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 11)->first();
        $data['quotation_detail_item12'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 12)->first();
        $data['quotation_detail_item13'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 13)->first();
        $data['quotation_detail_item6'] = ALL::GetSpecRow('spa_quotation_detail', 'fk_main_booking', $bookingId)->where('fk_lkp_spa_payitem', 9)->first();
        $data['total'] = 
            ($data['quotation_detail_item10']->total ?? 0) +
            ($data['quotation_detail_item22']->total ?? 0) +
            ($data['quotation_detail_item11']->total ?? 0) +
            ($data['quotation_detail_item12']->total ?? 0) +
            ($data['quotation_detail_item13']->total ?? 0) ;
        $data['deposit'] = $data['quotation_detail_item6']->total ?? 0;
        $data['downpymt'] = $data['total'] / 2;
        if($data['main_booking']->fk_lkp_status == 24){
            $data['total_v'] = $data['total'] / 2 + $data['deposit'];
        } else {
            $data['total_v'] = $data['total'] / 2;
        }
        $data['id'] = $data['main_booking']->id;
        
        return view('event.dashboard.reservation', compact('data'));
    }

    public function updatePayment(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['id']);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['sba'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['main']->id);
        $data['spa_location'] = All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location);

        $data['ld'] = All::GetAllRow('lkp_deposit', 'fk_main_booking', $data['main']->id)->where('status', 0)->first();
        if($data['main']->fk_lkp_status == 24){
            $data['validate'] = All::GetRowDateRangeCustom('spa_booking_confirm', 'fk_spa_location', $data['sba']->fk_spa_location, 'date_booking', 'date_booking', $data['sba']->enter_date, $data['sba']->exit_date);
            if(!empty($data['validate']->toArray())){       // date and location is booked already
                return redirect()->back();
            }

            $data['datesBetween'] = $this->getDatesBetween($data['sba']->enter_date, $data['sba']->exit_date);
            // start::Book confirm
            foreach ($data['datesBetween'] as $key => $value) {
                $data_spa_book_confirm = [
                    'fk_main_booking'       => $data['id'],
                    'fk_spa_location'       => $data['sba']->fk_spa_location,
                    'fk_user'               => Session::get('user.id'),
                    'date_booking'          => $value,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s')
                ];
                $query = All::Insert('spa_booking_confirm', $data_spa_book_confirm);
            }
            // end::Book confirm

            // start::payment update | deposit
            $data_spa_depo = [
                'fk_main_booking'       => $data['id'],
                'fk_lkp_payment_type'   => 1,
                'fk_users'              => $data['main']->fk_users,
                'reference_id'          => $data['user_detail']->bud_reference_id,

                'amount'                => request()->jumlahTempahan,
                // 'transaction_id'        => ,
                // 'transaction_date'      => ,
                'fk_lkp_payment_mode'   => request()->payment_mode,
                // 'sap_doc_no'            => ,
                'payment_status'        => 1,
                'application_no'        => $data['main']->bmb_booking_no,

                'booking_location'      => All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location)->name,
                'booking_title'         => 'Tempahan Acara',

                'booking_start'         => $data['sba']->event_date,
                'booking_end'           => $data['sba']->event_date_end,
                'booking_time'          => $data['sba']->event_time,
                'payment_date'          => date('Y-m-d'),

                'total_amount'          => (float)request()->jumlahTempahan + (float)request()->deposit,
                'deposit_amount'        => request()->deposit,
                'paid_amount'           => request()->deposit,

                'bank_name'             => request()->namabank,
                'no_lopo'               => request()->lopo,
                'no_cek'                => request()->nocek,
                'no_bil'                => request()->rujukanBayaran,

                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $depoID = All::InsertGetID('spa_payment', $data_spa_depo);
            $data_spadetail_depo = [
                'fk_spa_payment'        => $depoID,
                'fk_lkp_payment_type'   => 1,
                'description'           => 'DEPOSIT-PENGANJURAN ACARA SEWAAN TAPAK',
                'amount'                => $data['main']->bmb_deposit_rm,
                'fee_code'              => All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location)->feecode_depo,
                'total_amount'          => $data['main']->bmb_deposit_rm,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('spa_payment_detail', $data_spadetail_depo);
            // end::payment update | deposit

            // start::payment update | downpayment
            $data_spa_downpymt = [
                'fk_main_booking'       => $data['id'],
                'fk_lkp_payment_type'   => 3,
                'fk_users'              => $data['main']->fk_users,
                'reference_id'          => $data['user_detail']->bud_reference_id,

                'amount'                => request()->jumlahTempahan,
                // 'transaction_id'        => ,
                // 'transaction_date'      => ,
                'fk_lkp_payment_mode'   => request()->payment_mode,
                'payment_status'        => 1,
                'application_no'        => $data['main']->bmb_booking_no,

                // 'booking_location'      => ,
                'booking_title'         => 'Tempahan Acara',

                'booking_start'         => $data['sba']->event_date,
                'booking_end'           => $data['sba']->event_date_end,
                'booking_time'          => $data['sba']->event_time,
                // 'payment_date'          => ,

                'total_amount'          => (float)request()->jumlahTempahan + (float)request()->deposit,
                'deposit_amount'        => request()->deposit,
                'paid_amount'           => request()->jumlahPendahuluan,

                'bank_name'             => request()->namabank,
                'no_lopo'               => request()->lopo,
                'no_cek'                => request()->nocek,
                'no_bil'                => request()->rujukanBayaran,
                
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $downpymtID = All::InsertGetID('spa_payment', $data_spa_downpymt);
            $data_spadetail_downpymt = [
                'fk_spa_payment'        => $downpymtID,
                'fk_lkp_payment_type'   => 1,
                'description'           => $data['sba']->event_name,
                'amount'                => $data['main']->bmb_subtotal / 2,
                'fee_code'              => All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location)->fee_code,
                'total_amount'          => $data['main']->bmb_subtotal / 2,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('spa_payment_detail', $data_spadetail_downpymt);
            // end::payment update | downpayment
            
            $lkp_deposit_updateAmt = [
                'deposit_amount'    => request()->deposit,
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
            $query = All::GetUpdate('lkp_deposit', $data['ld']->id, $lkp_deposit_updateAmt);
            // API to deposit refund system
            $api_data = [
                'name'                  => $data['ld']->name,
                'ref_id'                => $data['ld']->ref_id,
                'email'                 => $data['ld']->email,
                'mobile_no'             => $data['ld']->mobile_no,
                'bank_name'             => $data['ld']->bank_name,
                'bank_account'          => $data['ld']->bank_account,
                'subsystem'             => $data['ld']->subsystem,
                'subsystem_booking_no'  => $data['main']->bmb_booking_no,
                'status'                => 1,
                'deposit_amount'        => request()->deposit,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
            ];
            $api = All::InsertGetID('deposit_list', $api_data);

            $data_mb = [
                'fk_lkp_status'     => 26,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user.id')
            ];
            All::GetUpdate('main_booking', $data['id'], $data_mb);

            return redirect()->back();
        } else {
            // start::payment update | penuh
            $data_spa_full = [
                'fk_main_booking'       => $data['id'],
                'fk_lkp_payment_type'   => 2,
                'fk_users'              => $data['main']->fk_users,
                'reference_id'          => $data['user_detail']->bud_reference_id,

                'amount'                => $data['main']->bmb_subtotal,
                // 'transaction_id'        => ,
                // 'transaction_date'      => ,
                'fk_lkp_payment_mode'   => request()->payment_mode,
                // 'sap_doc_no'            => ,
                'payment_status'        => 1,
                'application_no'        => $data['main']->bmb_booking_no,

                'booking_location'      => All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location)->name,
                'booking_title'         => 'Tempahan Acara',

                'booking_start'         => $data['sba']->event_date,
                'booking_end'           => $data['sba']->event_date_end,
                'booking_time'          => $data['sba']->event_time,
                'payment_date'          => date('Y-m-d'),

                'total_amount'          => (float)request()->jumlahTempahan + (float)request()->deposit,
                'deposit_amount'        => request()->deposit,
                'paid_amount'           => $data['main']->bmb_subtotal / 2,

                'bank_name'             => request()->namabank,
                'no_lopo'               => request()->lopo,
                'no_cek'                => request()->nocek,
                'no_bil'                => request()->rujukanBayaran,

                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $fullPymtID = All::InsertGetID('spa_payment', $data_spa_full);

            $data_spadetail_full = [
                'fk_spa_payment'        => $fullPymtID,
                'fk_lkp_payment_type'   => 2,
                'description'           => $data['sba']->event_name,
                'amount'                => $data['main']->bmb_subtotal / 2,
                'fee_code'              => All::GetRow('spa_location', 'id', $data['sba']->fk_spa_location)->fee_code,
                'total_amount'          => $data['main']->bmb_subtotal / 2,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('spa_payment_detail', $data_spadetail_full);
            // end::payment update | penuh

            $data_mb = [
                'fk_lkp_status'     => 20,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user.id')
            ];
            All::GetUpdate('main_booking', $data['id'], $data_mb);

            return redirect()->back();
        }
    }

    public function my(){
        $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
        return view('dashboard.my', compact('data'));
    }

    public function report_keseluruhan(){
        $data['laporan_user'] = Event::laporan_keseluruhan();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_dalaman(){
        $data['laporan_user'] = Sport::laporan_dalaman();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_bayaran(){
        $data['laporan_user'] = Sport::laporan_bayaran();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_kelulusan(){
        $data['laporan_user'] = Sport::laporan_kelulusan();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_harian(){
        $data['laporan_user'] = Sport::laporan_harian();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_tindih(){
        $data['laporan_user'] = Sport::duplicate_all();
        return view('dashboard.report.my', compact('data'));
    }

    public function report_my(){
        $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
        return view('dashboard.report.my', compact('data'));
    }

    public function top_pengguna()
    {
        $data = Sport::laporan_top_pengguna();
        $arr = array();
        foreach($data as $a){
            $arr[] =  array(
                'x' => $a->fullname,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    public function top_tempahan()
    {
        $data = Sport::laporan_top_tempahan();
        $arr = array();
        foreach($data as $a){
            $nama = $a->eft_type_desc .' , '. $a->lc_description;
            $arr[] =  array(
                'x' => $nama,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    public function bayaran(Request $request, $booking) {
        // $no = Event::generatetransid();
        $data['booking'] = Crypt::decrypt($booking);
        $main = All::GetRow('main_booking', 'id', $data['booking']);
        $data['oderNo'] = $main->bmb_booking_no;//.$no;
        $data['total'] = $request->post('total');
        //$data['total1'] = "1600.00";

        $data_fpx = array(
            'fk_main_booking'            => $data['booking'],
            'fk_lkp_payment_type'        => 2,
            'fpx_trans_id'               => $data['oderNo'],
            'total_amount'               => $data['total'],
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::InsertGetID('et_payment_fpx', $data_fpx);

        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';
        $data1 = array("payload" => array(
            "subsysId"  => "SPT",
            "password"  => "SPt!!@2022",
            "orderNo"   => $data['oderNo'],
            "description" => $data['oderNo'],
            "txnTime" => date('Y-m-d H:i:s'),
            "amount"  => $data['total']
        ));
        $postdata = json_encode($data1);
		
		//echo $postdata;

        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $response = json_decode($result, true);
		//echo "<br>" . $result;
        curl_close($ch);

        return view('event.public.bayaran', compact('data', 'response'));
    }

    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }
}

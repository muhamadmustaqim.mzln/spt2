<?php

namespace App\Http\Controllers\event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\All;
use App\Models\Event;

class LocationController extends Controller
{
    public function show() {
        // $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 1);
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC');
        // dd($data);
        return view('event.location.lists', compact('data'));
    }

    public function form(){
		
		return view('event.location.form');
    }

    public function add(Request $request){
        $status = request()->status;
        $data = array(
            'name'              => request()->location,
            'category'          => request()->category,
            'rent_perday'       => request()->rent_perday,
            'deposit'           => request()->deposit,
            'capacity'          => request()->capacity,
            'status'            => (request()->status == 'on') ? 1 : 0,
            'remark'            => request()->remark,
            'longitude'         => request()->longitude,
            'latitude'          => request()->latitude,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('spa_location', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/location'));
        }


    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        // $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        // $data['list'] = All::GetRow('et_facility_type', 'id', $id);
        $data['facility'] = All::ShowAll('spa_location', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        $data['list'] = Event::locationPictures($id)->first();
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        // dd($data['list']->category);
        // return view('event.facility.edit', compact('data'));
        // $id = Crypt::decrypt($id);
        // // $data['location'] = All::GetRow('lkp_location', 'id', $id);
        // $data['facility'] = All::ShowAll('spa_location', 'id', 'ASC');
        // $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        // $data['location_picture'] = All::GetRow('spa_location_picture', 'fk_spa_location', $id);
        // $data['list'] = All::GetRow('spa_location', 'id', $id);
        // // $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        // dd($id, $data);

        return view('event.location.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;
        $status = $request->post('status');
        // if($status == "1"){
        //     $status = 1;
        // }else{
        //     $status = 0;
        // }
        // dd($request->post(), $request->hasFile('file'), $request->hasFile('file1'));
        
        if ($request->hasFile('file')) {
            try {
                $image = $request->file('file');
                $filename = $image->getClientOriginalName();
                $path = public_path('assets/upload/main/'. $uuid);
                File::ensureDirectoryExists($path);
                // dd($image, $filename, $path);
                $request->file('file')->move($path, $filename);

                $data = array(
                    'cover_img'         => $filename,
                    'eft_uuid'          => $uuid,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => Session::get('user')['id'],
                );
                $query = All::GetUpdate('et_facility_type', $id, $data);
            } catch (\Throwable $th) {
                dd($th);
            }
        }
        
        if ($request->hasFile('file1')) {
            // $path1 = public_path('assets/upload/virtual/'.$uuid);
            $path1 = public_path('assets/upload/virtual/360_Image_PPj/Acara/'.$id);
            File::ensureDirectoryExists($path1);
            $image1 = $request->file('file1');
            $filename1 = $image1->getClientOriginalName();
            $request->file('file1')->move($path1, $filename1);

            $filePath = $path1 . '/' . $filename1;
            $fileType = mime_content_type($filePath); 
            $fileSize = filesize($filePath);

            $data = array(
                'virtual_img'       => '/upload/virtual/360_Image_PPj/Acara/'.$id.'/'.$filename1,
                'file_size'         => $fileSize,
                'file_type'         => $fileType,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            $query = All::GetUpdateSpec('spa_location_picture', 'fk_spa_location', $id, $data);
        }
        
        $data = array(
            'name'              => request()->location,
            'category'          => request()->category,
            'rent_perday'       => request()->rent_perday,
            'deposit'           => request()->deposit,
            'capacity'          => request()->capacity,
            'status'            => request()->status,
            'remark'            => request()->remark,
            'longitude'         => request()->longitude,
            'latitude'          => request()->latitude,
            'fee_code'          => request()->fee_code,
            'feecode_depo'      => request()->feecode_depo,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('spa_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/location'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/event/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/event/admin/location'));
        }
    }
}

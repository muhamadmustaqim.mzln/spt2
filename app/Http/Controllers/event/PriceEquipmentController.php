<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class PriceEquipmentController extends Controller
{
    public function show() {
        $data['list'] = All::Show('bh_equipment_price', 'id', 'ASC');
        return view('hall.equipmentprice.lists', compact('data'));
    }

    public function form(){
        $data['equipment'] = All::Show('et_equipment', 'id', 'ASC');
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 2);
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        return view('sport.equipmentprice.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_equipment'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_lkp_gst_rate'   => request()->gst,
            'eep_day_cat'       => request()->hari,
            'eep_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('et_equipment_price', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['equipment'] = All::Show('et_equipment', 'id', 'ASC');
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 2);
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['list'] = All::GetRow('et_equipment_price', 'id', $id);
        return view('sport.equipmentprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_equipment'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_lkp_gst_rate'   => request()->gst,
            'eep_day_cat'       => request()->hari,
            'eep_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }
}

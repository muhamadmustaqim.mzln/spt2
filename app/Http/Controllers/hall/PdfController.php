<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use PDF;
use App\Models\All;
use App\Models\Sport;
use App\Models\Hall;
use App\Models\Event;
use DateTime;
use Carbon\Carbon;
use App\Helpers\Helper as HP;
use Illuminate\Support\Facades\Session;

class PdfController extends Controller
{
    public function index($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate)/100;
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['online'] = All::GetRow('et_payment_fpx', 'fk_main_booking', $data['booking']);
        $data['location'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);
        
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['datatambahaneqp'] = $data['kelengkapan'];

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
          
        $pdf = PDF::loadView('hall.pdf.surat', $data);
        
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width(); 
        $h = $canvas->get_height(); 

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png'; 
        $imgWidth = 450; 
        $imgHeight = 500; 
         
        // Set image opacity 
        $canvas->set_opacity(.2); 
         
        // Specify horizontal and vertical position 
        $x = (($w-$imgWidth)/2); 
        $y = (($h-$imgHeight)/2); 
         
        // Add an image to the pdf 
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);


        return $pdf->download('surat.pdf');
    }
    
    public function index2($booking, $type){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['slot'] = All::GetRow('spa_booking_event', 'fk_main_booking', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        // $data['slot'] = Sport::priceSlot($data['booking']);
        // $data['online'] = All::GetRow('et_payment_fpx', 'fk_main_booking', $data['booking']);
        $data['spa_payment'] = All::GetRow('spa_payment', 'fk_main_booking', $data['booking']);
        // $data['online'] = All::JoinTwoTableWithWhereIdBooking('spa_payment_detail', 'spa_payment', 'fk_main_booking', $data['booking'])->where('fk_lkp_payment_type', $type);
        $data['online'] = All::GetAllRow2('spa_payment_detail', 'fk_spa_payment', $data['spa_payment']->id)->where('fk_lkp_payment_type', $type)->first();
        $data['location'] = All::GetRow('spa_location', 'id', $data['slot']->fk_spa_location);
        $data['type'] = HP::get_jenis_bayaran($type);
          
        $pdf = PDF::loadView('event.pdf.resitonlineacara', $data);
        
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width(); 
        $h = $canvas->get_height(); 

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png'; 
        $imgWidth = 450; 
        $imgHeight = 500; 
         
        // Set image opacity 
        $canvas->set_opacity(.2); 
         
        // Specify horizontal and vertical position 
        $x = (($w-$imgWidth)/2); 
        $y = (($h-$imgHeight)/2); 
         
        // Add an image to the pdf 
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        $type = ucfirst(strtolower(HP::get_jenis_bayaran($type)));
        return $pdf->download('Resit '. $type .' Acara.pdf');
        // return $pdf->download('resitonline.pdf');
    }

    public function rumusanBorangTempahan($booking) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main_booking'] = ALL::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
        $data['booking_event'] = ALL::GetRow('spa_booking_event', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_person'] = ALL::GetRow('spa_booking_person', 'fk_main_booking', $data['main_booking']->id);
        $data['booking_attachment'] = ALL::GetAllRow('spa_booking_attachment', 'fk_main_booking', $data['main_booking']->id, 'id', 'ASC');
        // $data['fasility1'] = All::GetRow('spa_location', 'id', $id);
        $data['bookingId'] = $data['booking'];

        $pdf = PDF::loadView('event.pdf.borangtempahan', compact('data'));
        
        $pdf->setPaper('P');
        $pdf->output();

        // stream = view file, download = download file
        return $pdf->stream('borangtempahan.pdf', compact('data'));
    }

    public function janaSurat($noBorang) {
        $data['getbmbno'] = All::GetSpecRow('main_booking', 'bmb_booking_no', $noBorang)->first();
        $data['getbmbno']->ls_description = (self::get_status_tempahan($data['getbmbno']->fk_lkp_status));
        $data['booking_event'] = All::GetSpecRow('spa_booking_event', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['booking_event']->locationname = (self::locationspa($data['booking_event']->fk_spa_location));
        $data['booking_person'] = All::GetSpecRow('spa_booking_person', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['booking_person']->lc_description = (self::country($data['booking_person']->fk_lkp_country));
        $data['meeting_result'] = All::GetSpecRow('spa_meeting_result', 'fk_main_booking', $data['getbmbno']->id)->first();
        $data['quotation_detail'] = Event::spa_quotation_detail_all($data['getbmbno']->id);
        $data['spa_quotation_detail_bpsk'] = Event::spa_quotation_detail_bpsk($data['getbmbno']->id);
        $data['spa_deposit_kecuali'] = Event::spa_deposit_kecuali($data['getbmbno']->id);
        $data['spa_quotation_detail_all_count'] = Event::spa_quotation_detail_all_count($data['getbmbno']->id);
        $data['sumtotal'] = Event::sumtotal($data['getbmbno']->id);
        $data['sumtotal_all'] = Event::sumtotal_all($data['getbmbno']->id);
        $pdf = PDF::loadView('event.pdf.surat', compact('data'));
        return $pdf->stream('event.pdf.surat.pdf', compact('data'));
    }
    
    public function janaSebutHarga($noBorang) {
        $noBorang = Crypt::decrypt($noBorang);
        $data['getbmbno'] = All::GetSpecRow('main_booking', 'id', $noBorang)->first();
        if (!$data['getbmbno']) {
            abort(404, 'Booking not found');
        }
        $mainBookingId = $data['getbmbno']->id;
        $data['bh_payment_fpx'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $mainBookingId);
        $data['bh_payment'] = All::GetAllRow('bh_payment', 'fk_main_booking', $mainBookingId);
        $data['getbmbno']->ls_description = self::get_status_tempahan($data['getbmbno']->fk_lkp_status);
        $data['user'] = All::GetRow('users', 'id', $data['getbmbno']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['user']->id);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $mainBookingId);
        $data['bh_booking_detail'] = All::GetAllRow('bh_booking_detail', 'fk_bh_booking', $data['bh_booking']->pluck('id'));
        $data['bh_booking_equipment'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking']->pluck('id'));
        $data['amount_paid_fpx'] = $data['bh_payment_fpx']->sum('amount_paid_fpx');
        $data['amount_paid'] = $data['bh_payment']->sum('amount_paid');
        $data['bh_quotation'] = All::GetAllRow('bh_quotation', 'fk_main_booking', $noBorang)->first();
        $pdf = PDF::loadView('hall.pdf.sebutharga', compact('data'));
        return $pdf->stream('hall.pdf.sebutharga.pdf', compact('data'));
    }
    
    public function get_status_tempahan($id)
    {
        $data = all::GetSpecRow('lkp_status', 'id', $id);
        foreach($data as $d){
            $data = $d->ls_description;
        }
        return $data;
    }
    public function locationspa($id)
    {
        $data = all::GetRow('spa_location', 'id', $id);
        return $data->name;
    }
    public function country($id)
    {
        $data = all::GetRow('lkp_country', 'id', $id);
        return $data->lc_description;
    }

    public function maklumattempahanpdf($location, $start, $end) {
        $location = Crypt::decrypt($location);
        $start = Crypt::decrypt($start);
        $end = Crypt::decrypt($end);
        $data['id'] = $location;
        $data['mula'] = $start;
        $data['tamat'] = $end;
        $data['slot'] = Hall::cariantempahan($data['id'], $data['mula'], $data['tamat'], 1);
        // $data['dayOfWeek'] = Carbon::parse($data['tarikh'])->locale('ms')->isoFormat('dddd');
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $slotId = $data['slot']->pluck('fk_main_booking');
        $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
        $slotId2 = $data['bh_booking']->pluck('id');
        $data['bh_booking'] = array_values($data['bh_booking']->toArray());
         $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
        $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());
		// dd($data['slot'],$slotId,$data['bh_booking'],$data['bh_booking_detail']);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.maklumattempahan', compact('data'));
        // dd($pdf);
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($type), 'Laporan Sukan - Eksport PDF Maklumat Tempahan',null);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Maklumat Tempahan Dewan.pdf');
    }
    public function maklumatpelangganpdf($location, $start, $end) {
        $location = Crypt::decrypt($location);
        $start = Crypt::decrypt($start);
        $end = Crypt::decrypt($end);
        $data['id'] = $location;
        $data['mula'] = $start;
        $data['tamat'] = $end;
        $data['slot'] = Hall::carianpelanggan($data['id'], $data['mula'], $data['tamat'], 1);
        // dd($data);
        // $data['dayOfWeek'] = Carbon::parse($data['tarikh'])->locale('ms')->isoFormat('dddd');
		// $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		// dd($data);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.maklumatpelanggan', compact('data'));
        // dd($pdf);
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($location), 'Laporan Sukan - Eksport PDF Maklumat Pelanggan',null);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Maklumat Pelanggan Dewan.pdf');
    }

    public function hasilpdf($location, $payment, $start, $end) {
        $location = Crypt::decrypt($location);
        $payment = Crypt::decrypt($payment);
        $start = Crypt::decrypt($start);
        $end = Crypt::decrypt($end);
        $data['id'] = $location;
        $data['mode'] = $payment;
        $data['start'] = $start;
        $data['end'] = $end;
        $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['start']);
        $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['end']);
        $data['mula'] = $tarikhMula->format('Y-m-d');
        $data['tamat'] = $tarikhAkhir->format('Y-m-d');
        $data['slot'] = Hall::hasil($data['id'], $data['mode'], $data['mula'], $data['tamat']);
		// dd($data);

        $slotId = $data['slot']->pluck('fk_main_booking');
        $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
        $slotId2 = $data['bh_booking']->pluck('id');
        $data['bh_booking'] = array_values($data['bh_booking']->toArray());
        $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
        $data['bh_booking_equipment'] = All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray();
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->first();
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->first();
		// dd($data);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.hasil', compact('data'));
        // dd($pdf);
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($location), 'Laporan Sukan - Eksport PDF Kutipan Hasil',null);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Kutipan Hasil Dewan.pdf');
    }

    public function kutipanharianpdf($location, $tarikh, $typebayaran) {
        $location = Crypt::decrypt($location);
        $tarikh = Crypt::decrypt($tarikh);
        $typebayaran = Crypt::decrypt($typebayaran);
        $data['id'] = $location;
        $data['bayaran'] = $typebayaran;
        $data['mula'] = $tarikh;
        $data['slot'] = Hall::harian($data['id'], $data['bayaran'], $data['mula']);
        
        $data['dayOfWeek'] = Carbon::parse($data['mula'])->locale('ms')->isoFormat('dddd');
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		// dd($data);
        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.kutipanharian', compact('data'));
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($tarikh), 'Laporan Sukan - Eksport PDF',null);
        // dd($pdf);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Kutipan Harian Kompleks Dewan.pdf');
    }

    public function depositpdf($location, $payment, $type, $start, $end) {
        $location = Crypt::decrypt($location);
        $payment = Crypt::decrypt($payment);
        $type = Crypt::decrypt($type);
        $start = Crypt::decrypt($start);
        $end = Crypt::decrypt($end);
        $data['id'] = $location;
        $data['mode'] = $payment;
        $data['jeniskutipan'] = $type;
        $data['mula'] = $start;
        $data['tamat'] = $end;
        // $data['payment'] = Hall::kutipandeposit($data['id'], $data['mula'], $data['tamat'], $data['mode'], $data['jeniskutipan']);
        $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['mula']);
        $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['tamat']);
        $data['slot'] = Hall::kutipandeposit($data['id'],$tarikhMula,$tarikhAkhir,$data['mode'],1,$data['jeniskutipan']);
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		// dd($data);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.bayarandepo', compact('data'));
        // dd($pdf);
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($location), 'Laporan Sukan - Eksport PDF Bayaran Deposit',null);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Bayaran Deposit Dewan.pdf');
    }

    public function agingpdf($location, $payment, $start, $end) {
        $location = Crypt::decrypt($location);
        $payment = Crypt::decrypt($payment);
        $start = Crypt::decrypt($start);
        $end = Crypt::decrypt($end);
        $data['id'] = $location;
        $data['event'] = $payment;
        $data['mula'] = $start;
        $data['tamat'] = $end;
        $data['slot'] = Hall::kutipantertunggak($data['id'], $data['event'], $data['mula'], $data['tamat']);
        // $data['dayOfWeek'] = Carbon::parse($data['tarikh'])->locale('ms')->isoFormat('dddd');
        $slotId = $data['slot']->pluck('id');
        $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
        $slotId2 = $data['bh_booking']->pluck('id');
        $data['bh_booking'] = array_values($data['bh_booking']->toArray());
        $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
        $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		// dd($data);

        //Eksport PDF
        try {
        $pdf = PDF::loadView('hall.pdf.aging', compact('data'));
        // dd($pdf);
        // $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($location), 'Laporan Sukan - Eksport PDF Kutipan Bayaran Tertunggak',null);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return $pdf->download('Laporan Kutipan Bayaran Tertunggak Dewan.pdf');
    }

    public function resitonline($booking, $hasEquipment){
        $data['booking'] = Crypt::decrypt($booking);
        $data['equiponly'] = Crypt::decrypt($hasEquipment);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['eeb'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);

        $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['eht'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['ehb']->pluck('id'));

        $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['est'] = All::GetAllRowIn('et_sport_time', 'fk_et_sport_book', $data['esb']->pluck('id'));

        if(count($data['esb']) > 0) {
            $data['type'] = 2;
        } else {
            $data['type'] = 1;
        }

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['dewan'] = Sport::priceSlot2($data['booking']);

        $first_three = substr($data['main']->bmb_booking_no, 0, 3);
        if($first_three == 'SPS'){
            $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'])->last();
            if($data['online'] == null){
                $data['counter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'])->last();
            }
        } else {
            $data['online'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['booking'])->last();
        }

        $data['location'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);
        $data['tax'] = All::Show('lkp_tax', 'id', 'ASC');
        $data['sso'] = Auth::Sso($data['user']->email);

        if($data['sso']->status == 1){
            if($data['sso']->user->staff_id == 1){
                $data['perc'] = '50'; 
            } else {
                $data['perc'] = '0';
            }
        } else {
            return redirect()->back();
            // dd('no sso data');
        }
        // dd($data);
        $pdf = PDF::loadView('hall.pdf.resit', $data);
        
        $pdf->setPaper('P');
        $pdf->output();
        $canvas = $pdf->getDomPDF()->getCanvas();
        $w = $canvas->get_width(); 
        $h = $canvas->get_height(); 

        $imageURL = 'https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png'; 
        $imgWidth = 450; 
        $imgHeight = 500; 
         
        // Set image opacity 
        $canvas->set_opacity(.2); 
         
        // Specify horizontal and vertical position 
        $x = (($w-$imgWidth)/2); 
        $y = (($h-$imgHeight)/2); 
         
        // Add an image to the pdf 
        $canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);
        // return $pdf->download('resitonline.pdf');
        return $pdf->stream('resit.pdf');
    }
}

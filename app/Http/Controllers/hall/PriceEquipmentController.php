<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class PriceEquipmentController extends Controller
{
    public function show() {
        $data['list'] = All::Show('bh_equipment_price', 'id', 'ASC');
        return view('hall.equipmentprice.lists', compact('data'));
    }

    public function form(){
        $data['equipment'] = All::Show('bh_equipment', 'id', 'ASC');
        $data['equipment_price'] = All::Show('bh_equipment_price', 'id', 'ASC');
        $data['discount_type'] = All::Show('lkp_discount_type', 'id', 'ASC');
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        return view('hall.equipmentprice.form', compact('data'));
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == 'on'){
            $status1 = 1;
        } else {
            $status1 = 0;
        }

        $data = array(
            'fk_bh_equipment'       => request()->peralatan,
            'fk_lkp_gst_rate'       => request()->kadargst,
            'fk_lkp_discount_type'  => request()->discountcategory,
            'be_day_cat'            => request()->daycategory,
            'be_price'              => request()->hargaRM,
            'be_discount'           => request()->diskaunPer,
            'be_discount_rm'        => request()->diskaunRM,
            'be_gst_rm'             => request()->gst4350,
            'be_total_price'        => request()->jumlahharga,
            'be_status'             => $status1,
            // 'updated_by'            => Session::get('user')['id'],
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
        $query = All::Insert('bh_equipment_price', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['equipment'] = All::Show('bh_equipment', 'id', 'ASC');
        $data['discount_type'] = All::Show('lkp_discount_type', 'id', 'ASC');
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['list'] = All::GetRow('bh_equipment_price', 'id', $id);
        // dd($data);
        return view('hall.equipmentprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;
        if($status == 'on'){
            $status1 = 1;
        } else {
            $status1 = 0;
        }

        $data = array(
            'fk_bh_equipment'       => request()->peralatan,
            'fk_lkp_gst_rate'       => request()->kadargst,
            'fk_lkp_discount_type'  => request()->discountcategory,
            'be_day_cat'            => request()->daycategory,
            'be_price'              => request()->hargaRM,
            'be_discount'           => request()->diskaunPer,
            'be_discount_rm'        => request()->diskaunRM,
            'be_gst_rm'             => request()->gst4350,
            'be_total_price'        => request()->jumlahharga,
            'be_status'             => $status1,
            'updated_by'            => Session::get('user')['id'],
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_equipment_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipmentprice'));
        }
    }
}

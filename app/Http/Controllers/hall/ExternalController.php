<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use App\Models\All;
use App\Models\Auth;
use App\Models\Hall;
use Carbon\Carbon;
use DateTime;
use App\Helpers\Helper as HP;


class ExternalController extends Controller
{
    public function index() {
        $data['list'] = Hall::getInfoTempahanExternal();

        return view('hall.external.index', compact('data'));
    }

    public function user(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('nama');
            $data['user'] = Auth::user($data['id']);
        }
        
        return view('hall.external.user', compact('data'));
    }

    public function book(Request $request, $userId) {
        $data['post'] = false;
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['main_booking'] = [];
        $userId = Crypt::decrypt($userId);
        $data['userId'] = $userId;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['jenis'] = $request->post('jenisTempahan');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['id']);
            $dates[0] = request()->tarikhMula;
            $dates[1] = request()->tarikhAkhir;
            $data['start'] = request()->tarikhMula;
            $data['end'] = request()->tarikhAkhir;
            $data['date'] = $dates['0'] .'/'. $dates[1];
            // $data['date'] = $request->post('tarikh');
            // $dates = explode(" / ", $data['date']);
            // if (count($dates) === 2) {
            //     $dates[0] = date('Y-m-d', strtotime($dates[0]));
            //     $dates[1] = date('Y-m-d', strtotime($dates[1]));
            //     $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            //     $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
            // }
            // $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
            // $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
            $datesBetween = $this->getDatesBetween($data['start'], $data['end']);
            $data['slot'] = [];
            foreach($datesBetween as $d){
                // $data['slot'][$d] = Hall::slot($data['id'], $d);
                $data['slot'] = array_values(All::GetAllRow('bh_confirm_booking', 'fk_bh_hall', $data['id'])->where('date_booking', $dates[0])->toArray());
            }
            foreach ($data['slot'] as $s) {
                $data['main_booking'] = All::GetAllRow('main_booking', 'id', $s->fk_main_booking);
            }
        }
		//$data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 1)->where('lc_status', 1);
		$data['location'] = Hall::fasilityActive();
		// $data['bh_booking'] = All::JoinTwoTable('bh_booking', 'main_booking');
		$data['bh_booking'] = array_values(All::JoinThreeTablesHall('bh_booking', 'main_booking', 'bh_booking_detail', 'desc')->where('internal_indi', null)->toArray());

        return view('hall.external.book', compact('data'));
    }

    public function slot($id, $date, $userId){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $userId = Crypt::decrypt($userId);
        $data['userId'] = $userId;
        $data['user'] = All::GetRow('user_profiles', 'fk_users', $userId);

        $data['id'] = $id;
        $data['date'] = $date;
        $dates = explode("/", $data['date']);
		
        if (count($dates) === 2) {
            $dates[0] = HP::date_format_for_db($dates[0]);
            $dates[1] = HP::date_format_for_db($dates[1]);
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
        }
        //add condition for if only 1 date selected//
        elseif (count($dates) === 1) {
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
        }

        $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
        $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id',  $id);
        $data['capacity'] = All::Show('bh_hall_usage', 'id', 'ASC')->where('fk_bh_hall_detail', $data['details']->id);
        $data['event'] = All::Show('lkp_event', 'id', 'ASC')->where('le_status', 1);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');
        $data['list'] = All::GetRow('et_facility_price', 'id', $id);
		
        return view('hall.external.slot', compact('data'));
    }
	
	public function slotbook(Request $request, $id, $date, $userId){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $userId = Crypt::decrypt($userId);

        $bh_hall = All::GetRow('bh_hall', 'id', $id);
        $bh_hall_price = All::GetAllRow('bh_hall_price', 'fk_bh_hall', $id);
        $number_of_days = ($start_date = DateTime::createFromFormat('d-m-Y', $request->post('bbd_start_date')))->diff($end_date = DateTime::createFromFormat('d-m-Y', $request->post('bbd_end_date')))->days + 1;
        $date_range = [];
        $current_date = clone $start_date;
        while ($current_date <= $end_date) {
            $day_of_week = $current_date->format('N'); 
            $is_weekend = ($day_of_week == 6 || $day_of_week == 7);
        
            $date_range[] = $is_weekend;
        
            $current_date->modify('+1 day');
        }
        if($request->post('sesiAcara') == 1) {
            $bbd_start_time = '08:00:00';
            $bbd_end_time = '16:00:00';
        } else {
            $bbd_start_time = '16:00:00';
            $bbd_end_time = '00:00:00';
        }
        $fk_lkp_deposit_rate = All::GetRow('lkp_deposit_rate', 'fk_lkp_location', $bh_hall->fk_lkp_location);
        $total_book_hall = 0;
        foreach ($date_range as $dr) {
            $price = $bh_hall_price->first(function ($item) use ($dr) {
                return $item->bhp_day_cat == ($dr ? '2' : '1');
            });
        
            if ($price) {
                $total_book_hall += $price->bhp_total_price;
            }
        }

        //No Booking Dewan
        $year = date('y');
        $md = date('md');
        $running =  Hall::getRn();
        $rn = sprintf( '%05d', $running);
        $bookrn='SPT'.$year.$md.$rn;
        $dataMainBooking = array(
            'fk_users'                  => $userId,
            'fk_lkp_status'             => 1,
            'fk_lkp_deposit_rate'       => $fk_lkp_deposit_rate->id,
            'fk_lkp_location'           => $bh_hall->fk_lkp_location,
            'bmb_total_book_hall'       => $total_book_hall,
            'fk_lkp_discount_type'      => $request->post('jenisTempahan'),
            'bmb_booking_no'            => $bookrn,
            'bmb_type_user'             => 2, 
            'bmb_booking_date'          => date('Y-m-d H:i:s'),
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $mainbookingid = All::InsertGetID('main_booking', $dataMainBooking);
        
        if($request->hasFile('dokumensokongan')){
            $image = $request->file('dokumensokongan');
            $filename = $mainbookingid;
            $imageSize = $image->getSize();
            $imageExt = $image->getClientOriginalExtension();
            $original_filename = $image->getClientOriginalName();
            $path = public_path('upload_document/mainbooking/' . $mainbookingid);
    
            // Ensure the directory exists
            File::ensureDirectoryExists($path);
    
            // Move the uploaded file to the destination directory
            $image->move($path, $filename);
            
            $data_ba = array(
                'fk_main_booking'   => $mainbookingid,
                'ba_date'           => date('Y-m-d'),
                'ba_dir'            => 'ba_dir', // Example path to the directory
                'ba_full_path'      => $path, // Full path including filename
                'ba_file_name'      => $original_filename,
                'ba_file_ext'       => $imageExt,
                'ba_file_size'      => $imageSize,
                'ba_generated_name' => $mainbookingid,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            $query = All::Insert('bh_attachment', $data_ba);
        } 

        $slot = $request->post('slot');
        $i = 0;

        if($request->post('sesiAcara') == 1) {
            $bbd_start_time = '08:00:00';
            $bbd_end_time = '16:00:00';
        } else {
            $bbd_start_time = '16:00:00';
            $bbd_end_time = '00:00:00';
        }
        for ($i=0; $i < $number_of_days; $i++) {
            $isWeekend = true;
            $hallprice = $bh_hall_price[1]->bhp_total_price;
            if ($date_range[$i] == false) {
                $isWeekend = false;
                $hallprice = $bh_hall_price[0]->bhp_total_price;
            }
            $dataHallBooking = array(
                'fk_bh_hall'        => $id,
                'fk_main_booking'   => $mainbookingid,
                // 'fk_bh_package'     => ,
                'fk_lkp_gst_rate'   => 3,
                // 'fk_lkp_discount'   => null,
                // 'fk_bh_hall_usage'  => $id, // need to update
                'fk_bh_hall_usage'  => null,
                'bb_hall_status'    => 0,
                'bb_start_date'    => HP::date_format_for_db($request->post('bbd_start_date')),
                'bb_end_date'      => HP::date_format_for_db($request->post('bbd_end_date')),
                'hall_price'        => $hallprice,
                // 'bb_discount_type_rm'   => 
                'bb_no_of_day'      => $number_of_days,
                'bb_gst'            => 0.00,
                'bb_total'          => 0.00,
                'bb_subtotal'       => 0.00,
                // 'bb_equipment_price'=>
                'created_at'        => date('Y-m-d H:i:s'),
            );
            $bhbookingid = All::InsertGetID('bh_booking', $dataHallBooking);
            $dataHallBookingDetail = array(
                'fk_bh_booking'                 => $bhbookingid,
                'fk_lkp_event'                  => $request->post("JenisAcaraTempDewan"),
                'fk_lkp_time_session'           => $request->post("sesiAcara"),
                'bbd_event_name'                => $request->post("namaAcara"),
                'bbd_event_description'         => $request->post("keteranganAcara"),
                'bbd_vvip'                      => $request->post("bilanganPesertaVVIP"),
                'bbd_vip'                       => $request->post("bilanganPesertaVIP"),
                'bbd_participant'               => $request->post("bilanganPesertaLain"),
                'bbd_total_no_of_pax'           => $request->post("bilanganPesertaVVIP") + $request->post("bilanganPesertaVIP") + $request->post("bilanganPesertaLain"),
                'bbd_user_apply'                => $request->post('namaPemohon'),
                'bbd_contact_no'                => $request->post('noTel'),
                'bbd_start_date'                => HP::date_format_for_db($request->post('bbd_start_date')),
                'bbd_end_date'                  => HP::date_format_for_db($request->post('bbd_end_date')),
                'bbd_start_time'                => $bbd_start_time,
                'bbd_end_time'                  => $bbd_end_time,
                'created_at'                    => date('Y-m-d H:i:s'),
            );
            $query = All::InsertGetID('bh_booking_detail', $dataHallBookingDetail);
        }
        // }
            $etHall = true;
            $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
            $data['main'] = All::GetRow('main_booking', 'id', $mainbookingid);
            $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
            $data['date'] = $date;
            $dates = explode("/", $data['date']);
            if (count($dates) === 2) {
                $dates[0] = HP::date_format_for_db($dates[0]);
                $dates[1] = HP::date_format_for_db($dates[1]);
                $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
            }
            elseif (count($dates) === 1) {
                $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            }
        
        $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
        $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id',  $id);
        $data['capacity'] = All::Show('bh_hall_usage', 'id', 'ASC')->where('fk_bh_hall_detail', $data['details']->id);
        $data['event'] = All::Show('lkp_event', 'id', 'ASC')->where('le_status', 1);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);

        return Redirect::to(url('hall/external/tempahan', [Crypt::encrypt($mainbookingid)]));
				// return view('hall.public.tempahan', compact('data'));
        //     Session::flash('flash', 'Failed'); 
    }
    
    public function tempahan(Request $request, $booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['temp'] = [];
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
            $data['temp'][] = $s->fk_bh_hall;
        }
        $data['fasility'] = All::GetRow('bh_hall', 'id', $data['bh_booking'][0]->fk_bh_hall);
        $data['datesBetween'] = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
        $presint = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description;
        if(request()->isMethod('post')){
            $statusKelengkapan = request()->statusKelengkapan;
            // statusKelengkapan = 1, Sepanjang Program / = 2, Harian
            if($statusKelengkapan == 1) {
                $mejaBulat = request()->jumlahMejaBulat;
                $mejaPanjang = request()->jumlahMejaPanjang;
                $kerusi = request()->jumlahKerusi;
                $cdlayar = request()->jumlahLCDlayar;
            } else {
                $numberOfDays = request()->numberOfDays;
                $mejaBulat = [];
                $mejaPanjang = [];
                $kerusi = [];
                $cdlayar = [];
                for($i = 0; $i < $numberOfDays; $i++) {
                    // dd($request->post('jumlahMejaBulat_'.$i), $request->post());
                    $mejaBulat[] = $request->post('jumlahMejaBulat_'.$i);
                    $mejaPanjang[] = $request->post('jumlahMejaPanjang_'.$i);
                    $kerusi[] = $request->post('jumlahKerusi_'.$i);
                    $cdlayar[] = $request->post('jumlahLCDlayar_'.$i);
                }
            }
            // dd($mejaBulat, $mejaPanjang, $kerusi, $cdlayar, $request->post(), $data['bh_booking']);
            $data['main_booking'] = All::GetRow('main_booking', 'id', $data['booking']);
            $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
            $data['temp'] = [];

            foreach ($data['bh_booking'] as $s) {
                $data['bh_booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
                $data['temp'][] = $s->fk_bh_hall;
            }
            $bh_equipment = All::GetAllRow('bh_equipment', 'fk_lkp_location', $data['main_booking']->fk_lkp_location);
            $bh_equipment_id = [];
            foreach ($bh_equipment as $eq) {
                $bh_equipment_id[] = $eq->id;
            }
            $bhBookingDetail_lkpTimeSession = [];
            foreach ($data['bh_booking_detail'] as $bhd) {
                $bhBookingDetail_lkpTimeSession[] = $bhd->fk_lkp_time_session;
            }
            $bhBookingDetail_lkpTimeSession = array_unique($bhBookingDetail_lkpTimeSession);
            $bh_equipment_price = [];
            foreach ($bhBookingDetail_lkpTimeSession as $key) {
                $bh_equipment_price[] = All::GetAllRow('bh_equipment_price', 'be_day_cat', $bhBookingDetail_lkpTimeSession)->whereIn('fk_bh_equipment', $bh_equipment_id);
            }
            // $bh_equipment_price = All::GetAllRow('bh_equipment_price', 'be_day_cat', $data['bh_booking_detail']->fk_lkp_time_session)->whereIn('fk_bh_equipment', $bh_equipment_id);

            $fk_lkp_discount_type = $data['main_booking']->fk_lkp_discount_type;
            switch ($fk_lkp_discount_type) {
                case 5:
                    $lkp_discount_type = 1;
                    break;
                case 6:
                    $lkp_discount_type = 2;
                    break;
                case 7:
                    $lkp_discount_type = 3;
                    break;
                case 8:
                    $lkp_discount_type = 4;
                    break;
                default:
                    $lkp_discount_type = $fk_lkp_discount_type; 
                    break;
            }
            // dd($data['bh_booking']);
            $bh_booking_id = $data['bh_booking'][0]->id;
            $fk_lkp_location = $data['main_booking']->fk_lkp_location;

            $availability = [
                $mejaBulat > 0 || !is_null($mejaBulat) ? $mejaBulat : false,
                $mejaPanjang > 0 || !is_null($mejaPanjang) ? $mejaPanjang : false,
                $kerusi > 0 || !is_null($kerusi) ? $kerusi : false,
                $cdlayar > 0 || !is_null($cdlayar) ? $cdlayar : false,
            ];
            $availability2 = [];
            $bbe_price = [];
            for($i = 0; $i < count($data['bh_booking']); $i++){
                for($j = 0; $j < count($availability); $j++){
                    if($availability[$j] == 0){
                        $availability[$j][$i] = null;
                    } else {
                        $availability2[$i][$j] = $availability[$j][$i];
                    }
                }
            }
            if(count($bh_equipment_price) == 1) {
                $bh_equipment_price = $bh_equipment_price[0];
            }

            $totalEquipmentPrice = 0;
            $dates = [];
            // dd($data['bh_booking'][0]->bb_end_date);
            $dates = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
            // dd($bh_equipment_price, $lkp_discount_type, $availability2, $bbe_price);
            //Save into booking equipment
            for ($j = 0; $j < count($data['bh_booking']); $j++){
                for ($i=0; $i < 4; $i++) { 
                    if ($availability[$i] != false || $availability[$i] != 0) {
                        $dataBHBookingEquipment = array(
                            'fk_bh_booking'     => $bh_booking_id,
                            'fk_bh_equipment'   => $bh_equipment[$i]->id,
                            // 'fk_lkp_gst_rate'   => $data['bh_booking']->fk_lkp_gst_rate,
                            // 'fk_bh_equipment_price' => ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->id),
                            'bbe_booking_date'  => $dates[$j],
                            'bbe_price'         => ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price),
                            'bbe_quantity'      => $availability[$i][$j],
                            'bbe_diskaun_rm'    => 0.00,
                            // 'special_discount_rm'   => ,
                            // // 'special_disc'      => ,
                            // 'baucer_rm'         => ,
                            // // 'baucer'            => ,
                            // 'bbe_gst'           => ,
                            'bbe_total'         => (int)$availability[$i] * ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price),
                            'created_at'        => date('Y-m-d H:i:s'),
                        );
                        $totalEquipmentPrice = (int)$availability[$i] * ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price);
                        All::Insert('bh_booking_equipment', $dataBHBookingEquipment);
                    }
                }
            }
            $dataMainBooking = [
                'bmb_total_equipment' => $totalEquipmentPrice
            ];
            $bh_quotation = All::GetUpdate('main_booking', $data['main_booking']->id, $dataMainBooking);

            $quono = Hall::generatequono();
            $databh_quotation = [
                'fk_main_booking'       => $data['booking'],
                'fk_users'              => Session::get('user')['id'],
                'fk_lkp_discount_type'  => $data['main_booking']->fk_lkp_discount_type,
                'bq_quotation_no'       => 'PPj/' .$presint.'/'.$data['main_booking']->bmb_booking_no.'/'.$quono,
                'bq_quotation_date'     => date('Y-m-d'),
                // 'bq_quotation_status'   => 
                // 'bq_total_amount'       => 
                // 'bq_deposit'            => 
                // 'bq_payment_status'     => 0
            ]; 
            $bh_quotation = All::InsertGetId('bh_quotation', $databh_quotation);

            return Redirect::to(url('hall/rumusan', [Crypt::encrypt($data['booking'])]));
        }
        $data['numberOfDays'] = ((new DateTime($data['bh_booking'][0]->bb_start_date))->diff(new DateTime($data['bh_booking'][0]->bb_end_date)))->days + 1;
        
        return view('hall.external.tempahan', compact('data'));
    }

    // public function tempahan(Request $request, $booking){
    //     $data['booking'] = Crypt::decrypt($booking);

    //     $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
    //     $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
    //     $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);

    //     $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
    //     $data['booking_detail'][] = All::GetAllRowIn('bh_booking_detail', 'fk_bh_booking', $data['bh_booking']->pluck('id'));

    //     // $data['temp'] = [];
    //     // foreach ($data['bh_booking'] as $s) {
    //     //     $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
    //     //     $data['temp'][] = $s->fk_bh_hall;
    //     // }
    //     $data['fasility'] = All::GetRow('bh_hall', 'id', $data['bh_booking'][0]->fk_bh_hall);
    //     $data['datesBetween'] = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
    //     $presint = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description;
    //     if(request()->isMethod('post')){
    //         $statusKelengkapan = request()->statusKelengkapan;
    //         // statusKelengkapan = 1, Sepanjang Program / = 2, Harian
    //         if($statusKelengkapan == 1) {
    //             $mejaBulat = request()->jumlahMejaBulat;
    //             $mejaPanjang = request()->jumlahMejaPanjang;
    //             $kerusi = request()->jumlahKerusi;
    //             $cdlayar = request()->jumlahLCDlayar;
    //         } else {
    //             $numberOfDays = request()->numberOfDays;
    //             $mejaBulat = [];
    //             $mejaPanjang = [];
    //             $kerusi = [];
    //             $cdlayar = [];
    //             for($i = 0; $i < $numberOfDays; $i++) {
    //                 $mejaBulat[] = $request->post('jumlahMejaBulat_'.$i);
    //                 $mejaPanjang[] = $request->post('jumlahMejaPanjang_'.$i);
    //                 $kerusi[] = $request->post('jumlahKerusi_'.$i);
    //                 $cdlayar[] = $request->post('jumlahLCDlayar_'.$i);
    //             }
    //         }
    //         $data['main_booking'] = All::GetRow('main_booking', 'id', $data['booking']);
    //         $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
    //         $data['temp'] = [];

    //         foreach ($data['bh_booking'] as $s) {
    //             $data['bh_booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
    //             $data['temp'][] = $s->fk_bh_hall;
    //         }
    //         $bh_equipment = All::GetAllRow('bh_equipment', 'fk_lkp_location', $data['main_booking']->fk_lkp_location);
    //         $bh_equipment_id = [];
    //         foreach ($bh_equipment as $eq) {
    //             $bh_equipment_id[] = $eq->id;
    //         }
    //         $bhBookingDetail_lkpTimeSession = [];
    //         foreach ($data['bh_booking_detail'] as $bhd) {
    //             $bhBookingDetail_lkpTimeSession[] = $bhd->fk_lkp_time_session;
    //         }
    //         $bhBookingDetail_lkpTimeSession = array_unique($bhBookingDetail_lkpTimeSession);
    //         $bh_equipment_price = [];
    //         foreach ($bhBookingDetail_lkpTimeSession as $key) {
    //             $bh_equipment_price[] = All::GetAllRow('bh_equipment_price', 'be_day_cat', $bhBookingDetail_lkpTimeSession)->whereIn('fk_bh_equipment', $bh_equipment_id);
    //         }
    //         // $bh_equipment_price = All::GetAllRow('bh_equipment_price', 'be_day_cat', $data['bh_booking_detail']->fk_lkp_time_session)->whereIn('fk_bh_equipment', $bh_equipment_id);

    //         $fk_lkp_discount_type = $data['main_booking']->fk_lkp_discount_type;
    //         switch ($fk_lkp_discount_type) {
    //             case 5:
    //                 $lkp_discount_type = 1;
    //                 break;
    //             case 6:
    //                 $lkp_discount_type = 2;
    //                 break;
    //             case 7:
    //                 $lkp_discount_type = 3;
    //                 break;
    //             case 8:
    //                 $lkp_discount_type = 4;
    //                 break;
    //             default:
    //                 $lkp_discount_type = $fk_lkp_discount_type; 
    //                 break;
    //         }
    //         $bh_booking_id = $data['bh_booking'][0]->id;
    //         $fk_lkp_location = $data['main_booking']->fk_lkp_location;

    //         $availability = [
    //             $mejaBulat > 0 || !is_null($mejaBulat) ? $mejaBulat : false,
    //             $mejaPanjang > 0 || !is_null($mejaPanjang) ? $mejaPanjang : false,
    //             $kerusi > 0 || !is_null($kerusi) ? $kerusi : false,
    //             $cdlayar > 0 || !is_null($cdlayar) ? $cdlayar : false,
    //         ];
    //         $availability2 = [];
    //         $bbe_price = [];
    //         for($i = 0; $i < count($data['bh_booking']); $i++){
    //             for($j = 0; $j < count($availability); $j++){
    //                 $availability2[$i][$j] = $availability[$j][$i];
    //             }
    //         }
    //         if(count($bh_equipment_price) == 1) {
    //             $bh_equipment_price = $bh_equipment_price[0];
    //         }

    //         $totalEquipmentPrice = 0;
    //         $dates = [];
    //         $dates = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
    //         //Save into booking equipment
    //         for ($j = 0; $j < count($data['bh_booking']); $j++){
    //             for ($i=0; $i < 4; $i++) { 
    //                 if ($availability[$i] != false || $availability[$i] != 0) {
    //                     $dataBHBookingEquipment = array(
    //                         'fk_bh_booking'     => $bh_booking_id,
    //                         'fk_bh_equipment'   => $bh_equipment[$i]->id,
    //                         // 'fk_lkp_gst_rate'   => $data['bh_booking']->fk_lkp_gst_rate,
    //                         // 'fk_bh_equipment_price' => ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->id),
    //                         'bbe_booking_date'  => $dates[$j],
    //                         'bbe_price'         => ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price),
    //                         'bbe_quantity'      => $availability[$i][$j],
    //                         'bbe_diskaun_rm'    => 0.00,
    //                         // 'special_discount_rm'   => ,
    //                         // // 'special_disc'      => ,
    //                         // 'baucer_rm'         => ,
    //                         // // 'baucer'            => ,
    //                         // 'bbe_gst'           => ,
    //                         'bbe_total'         => (int)$availability[$i] * ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price),
    //                         'created_at'        => date('Y-m-d H:i:s'),
    //                     );
    //                     $totalEquipmentPrice = (int)$availability[$i] * ($bh_equipment_price->where('fk_bh_equipment', $i+1)->where('fk_lkp_discount_type', $lkp_discount_type)->first()->be_price);
    //                     All::Insert('bh_booking_equipment', $dataBHBookingEquipment);
    //                 }
    //             }
    //         }
    //         $dataMainBooking = [
    //             'bmb_total_equipment' => $totalEquipmentPrice
    //         ];
    //         $bh_quotation = All::GetUpdate('main_booking', $data['main_booking']->id, $dataMainBooking);

    //         $quono = Hall::generatequono();
    //         $databh_quotation = [
    //             'fk_main_booking'       => $data['booking'],
    //             'fk_users'              => Session::get('user')['id'],
    //             'fk_lkp_discount_type'  => $data['main_booking']->fk_lkp_discount_type,
    //             'bq_quotation_no'       => 'PPj/' .$presint.'/'.$data['main_booking']->bmb_booking_no.'/'.$quono,
    //             'bq_quotation_date'     => date('Y-m-d'),
    //             // 'bq_quotation_status'   => 
    //             // 'bq_total_amount'       => 
    //             // 'bq_deposit'            => 
    //             // 'bq_payment_status'     => 0
    //         ]; 
    //         $bh_quotation = All::InsertGetId('bh_quotation', $databh_quotation);

    //         return Redirect::to(url('hall/rumusan', [Crypt::encrypt($data['booking'])]));
    //     }
    //     $data['numberOfDays'] = ((new DateTime($data['bh_booking'][0]->bb_start_date))->diff(new DateTime($data['bh_booking'][0]->bb_end_date)))->days + 1;

    //     // dd($data);
    //     return view('hall.external.tempahan', compact('data'));
    // }

    public function hall(Request $request){
        $data['post'] = false;
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        
        if($request->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['id']);
            $data['date'] = $request->post('tarikh');
    
            $dates = explode("/", $data['date']);
		
            // $dates[0] = "2023-11-01";
            // $dates[1] = "2023-11-03";
            if (count($dates) === 2) {
                $dates[0] = HP::date_format_for_db($dates[0]);
                $dates[1] = HP::date_format_for_db($dates[1]);
                $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
            }
            //add condition for if only 1 date selected//
            elseif (count($dates) === 1) {
                $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            }
            
            // Format start and end dates
            $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
            $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
    
            // Fetch slots
            $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
            $data['slot'] = [];
            foreach( $data['datesBetween'] as $d){
                $data['slot'][$d] = Hall::slot($data['id'], $d);
            }
        }
    
        $data['location'] = Hall::fasility();
        $data['bh_booking'] = All::JoinTwoTable('bh_booking', 'main_booking');
    
        if (Session::get('user')['id'] == "50554") {
            return view('hall.reschedule.index', compact('data'));
        }
    }
    

    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }
    
    public function sport(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['id']);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $data['post'] = false;
        $data['lokasi'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['lokasi'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['lokasi']);
            $data['slot'] = Sport::slot($data['lokasi'], $data['date']);
        }
        return view('sport.external.sport', compact('data'));
    }

    public function sport_slotbook(Request $request, $id, $date, $user){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $user = Crypt::decrypt($user);

        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        $location = All::GetRow('et_facility_type', 'id', $id);

        $data = array(
            'fk_users'                   => $user,
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id']
        );
        $query = All::InsertGetID('main_booking', $data);

        $slot = $request->post('slot');
        $varCompLocation = 0;
        $i = 0;
        foreach($slot as $s){
            $row = explode(',', $s);
            if($row[0] != $varCompLocation){
                $varCompLocation = $row[0];
                $data_book[$i] = array(
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $id,
                    'fk_et_facility_detail'      => $varCompLocation,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => $row[2],
                    'ebf_end_date'               => $row[2],
                    'ebf_no_of_day'              => 1,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);

                $data_sport[$i] = array(
                    'fk_et_booking_facility'     => $etBookFacility,
                    'esb_booking_date'           => $row[2],
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
            }
            $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
            $total_gst = $row[4] * $gst->lgr_rate;
            $total = $row[4] + $total_gst;

            $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);

            $data_time[$i] = array(
                'fk_et_sport_book'           => $etSportBook,
                'fk_et_slot_price'           => $slotPrice->id,
                'est_price'                  => $row[4],
                'est_discount_type_rm'       => 0.00,
                'est_discount_rm'            => 0.00,
                'est_total'                  => $row[4],
                'est_gst_code'               => $row[5],
                'est_gst_rm'                 => $total_gst,
                'est_subtotal'               => $total,
                'created_at'                 => date('Y-m-d H:i:s'),
                'updated_at'                 => date('Y-m-d H:i:s')
            );
            $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
            $i++;
        }

        if ($etSportTime) {
            return Redirect::to(url('sport/external/tempahan', [Crypt::encrypt($query)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    // public function tempahan($booking){
    //     $data['booking'] = Crypt::decrypt($booking);
    //     $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
    //     $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
    //     $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
    //     $data['slot'] = Sport::priceSlot($data['booking']);

    //     return view('sport.external.tempahan', compact('data'));
    // }

    public function tempahan_delete($booking, $id){
        $id = Crypt::decrypt($id);
        $query = All::GetDelete('et_sport_time', $id);

        if ($query) {
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport/external/tempahan', $booking));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/external/tempahan', $booking));
        }
    }

    public function rumusan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);

        return view('sport.external.rumusan', compact('data'));
    }

    public function bayaran(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);
        $data['total'] = $request->post('total');
        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        return view('sport.external.bayaran', compact('data'));
    }

    public function submit_bayaran(Request $request, $booking)
    {
        $no = Sport::generatereceipt();
        $data['booking'] = Crypt::decrypt($booking);
        $data['total'] = $request->post('total');
        $data_fpx = array(
            'fk_main_booking'            => $data['booking'],
            'fk_lkp_payment_type'        => 2,
            'fk_lkp_payment_mode'        => $request->post('bayaran'),
            'fk_users'                   => Session::get('user')['id'],
            'bp_total_amount'            => $data['total'],
            'bp_deposit'            => 0,
            'bp_paid_amount'               => $data['total'],
            'bp_receipt_number'               => sprintf('%07d', $no),
            'bp_payment_ref_no'               => $request->post('ref'),
            'bp_receipt_date'                 => date('Y-m-d H:i:s'),
            'amount_received'               => $data['total'],
            'bp_subtotal'               => $data['total'],
            'bp_payment_status'               => 1,
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::InsertGetID('bh_payment', $data_fpx);

        $data_main = array(
            'fk_lkp_status'              => 5,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/maklumatbayaran', Crypt::encrypt($data['booking'])));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }
    }

    public function equipment($id){
        $data['id'] = Crypt::decrypt($id);
        return view('sport.reschedule.index', compact('data'));
    }

}

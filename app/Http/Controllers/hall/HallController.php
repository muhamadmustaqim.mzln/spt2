<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper as HP;
use App\Models\All;
use App\Models\Hall;
use App\Models\Sport;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;

class HallController extends Controller
{
    public function lists(Request $request) {
        $data['ppj'] = true;
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');
        $data['carian'] = null;
        $data['location'] = All::ShowAll('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id', 1);
        // $data['location'] = All::ShowAll('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        $data['hall'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', 1)->where('bh_status', 1);
        $data['komuniti'] = Sport::fasilityPublicKomuniti();
        $collection = collect($data['komuniti']);
        $data['komuniti'] = $collection->map(function ($item) {
            $item->encrypt = Crypt::encrypt($item->id);
            return $item;
        });
        // dd($data);
        $data['fasiliti'] = 1;
        $collection = collect($data['hall']);
        $data['hall'] = $collection->map(function ($item) {
            $item->encrypt = Crypt::encrypt($item->id);
            return $item;
        });
        $data['start'] = date('Y-m-d');
        $data['end'] = date('Y-m-d');
        $data['pengumuman'] = all::Show('bh_announcement', 'created_at', 'DESC')->where('ba_type', 1)->where('ba_status', 1);
        // dd($data['pengumuman']);
        if(request()->isMethod('post')){
            $data['lokasi'] = $request->post('lokasi');
            $data['kapasiti'] = $request->post('kapasiti');
            $data['fasiliti'] = $request->post('fasiliti');
            $tarikhMula = $request->post('tarikhMula');
            $tarikhAkhir = $request->post('tarikhAkhir');
            $data['start'] = request()->tarikhMula;
            $data['end'] = request()->tarikhAkhir;

            $data['ppj'] = true;
            $data['hall'] = Hall::hall3($data['fasiliti'], '', $data['start'], $data['end'], $data['kapasiti']);
            if(Session('user.id') == 61894){
                // dd($data['hall']);
            }
            $data['carian'] = (object) array(
                'lokasi'        => $data['lokasi'],
                'kapasiti'      => $data['kapasiti'],
                'fasiliti'      => $data['fasiliti'],
                'tarikhMula'    => $tarikhMula,
                'tarikhAkhir'   => $tarikhAkhir
            );
        }
        $data['confirmed_booking'] = json_encode(Hall::sekatanHari()->pluck('date_booking')->unique());

        return view('hall.public.lists', compact('data'));
    }

    public function details(Request $request, $id, $tarikhMula = null, $tarikhAkhir = null, $kapasiti = null, $fasiliti = null) {
        $id = Crypt::decrypt($id);
        if ($tarikhMula || $tarikhAkhir || $kapasiti || $fasiliti) {
            $tarikhMula = ($tarikhMula != null) ? Crypt::decrypt($tarikhMula) : null;
            $tarikhAkhir = Crypt::decrypt($tarikhAkhir);
            $kapasiti = Crypt::decrypt($kapasiti);
            $fasiliti = ($fasiliti != null) ? Crypt::decrypt($fasiliti) : null;
            $data['carian'] = (object) array(
                'lokasi'        => $id,
                'tarikhMula'    => $tarikhMula,
                'tarikhAkhir'   => $tarikhAkhir
            );
            $data['date'] = $data['carian']->tarikhMula.'/'.$data['carian']->tarikhAkhir;
        } else {
            $data['carian'] = null;
            $tarikhMula = new DateTime('now');
            $tarikhAkhir = new DateTime('now');
            $tarikhMula->modify('+3 months');
            $tarikhAkhir->modify('+3 months');
            $data['date'] = $tarikhMula->format("d-m-Y").'/'. $tarikhAkhir->format("d-m-Y");
        }
        $data['id'] = $id;
        $data['location'] = All::Show('bh_hall', 'id', 'ASC')->where('bh_status', 1);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('id', '!=' , $id)->where('id', '!=' , $id)->where('bh_status', 1);
        $data['price'] = All::Show('bh_hall_price', 'id', 'ASC')->where('fk_bh_hall', $id)->where('bhp_status', 1);
        $data['start'] = "";
        $data['end'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['fasility'] = All::GetRow('bh_hall', 'id', $data['id']);
            $data['others'] = All::Show('bh_hall', 'id', 'ASC')->where('fk_lkp_location', $data['fasility']->fk_lkp_location)->where('id', '!=' , $data['id'])->where('id', '!=' , $data['id'])->where('bh_status', 1);
            $data['price'] = All::Show('bh_hall_price', 'id', 'ASC')->where('fk_bh_hall', $data['id'])->where('bhp_status', 1);
        }
        // $mysqlDate = DateTime::createFromFormat('d-m-Y', $tarikhMula);
        // $mysqlDate = $tarikhMula->format('Y-m-d');
        // dd($data['date'], $tarikhMula);

        // $data['confirmed_booking'] = All::GetAllRow('bh_confirm_booking', 'fk_bh_hall', $data['id'])->where('date_booking', '>=', $mysqlDate)->pluck('date_booking')->toArray();
        $data['confirmed_booking'] = json_encode(Hall::sekatanHari($data['id'], $tarikhMula)->pluck('date_booking')->unique());

        return view('hall.public.details', compact('data'));
    }

    // -----------------------------------------------------------------------------

    public function slot($id, $date){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);

        //No Booking
        $running =  Hall::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $data['book']='SPT'.$year.$md.$rn;
        $data['id'] = $id;
        $data['date'] = $date;
        $dates = explode("/", $data['date']);
		
        if (count($dates) === 2) {
            $dates[0] = HP::date_format_for_db($dates[0]);
            $dates[1] = HP::date_format_for_db($dates[1]);
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
        }
        //add condition for if only 1 date selected//
        elseif (count($dates) === 1) {
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
        }
        // dd($dates);
        $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
        $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id',  $id);
        $data['capacity'] = All::Show('bh_hall_usage', 'id', 'ASC')->where('fk_bh_hall_detail', $data['details']->id);
        $data['event'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('le_status', 1);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        $data['bank_info_config'] = All::GetRow('lkp_configuration', 'id', '3');
        // dd($data['bank_info_config']);

        $data['weddingPackage'] = All::JoinTwoTable('bh_package_detail', 'bh_package');
        //new class data
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');
        $data['list'] = All::GetRow('et_facility_price', 'id', $id);
        $tarikhMula = new DateTime($data['start']);
        $mysqlDate = $tarikhMula->format('Y-m-d');
        $data['confirmed_booking'] = (Hall::sekatanHari($data['id'], $mysqlDate)->pluck('date_booking')->unique());
        $data['startJson'] = json_encode($data['start']);

        $dateEnd = DateTime::createFromFormat('d-m-Y', $data['end']);
        $dateEnd->modify('+1 day');
        $data['endJson'] = $dateEnd->format('Y-m-d');
        // dd($data, Session::get('user'));

        return view('hall.public.slot', compact('data'));
    }

	public function slotbook(Request $request, $id, $date){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $bh_hall = All::GetRow('bh_hall', 'id', $id);
        $bh_hall_price = All::GetAllRow('bh_hall_price', 'fk_bh_hall', $id);
        $number_of_days = ($start_date = DateTime::createFromFormat('d-m-Y', $request->post('bbd_start_date')))->diff($end_date = DateTime::createFromFormat('d-m-Y', $request->post('bbd_end_date')))->days + 1;
        $date_range = [];
        $current_date = clone $start_date;
        $bookrn = request()->book;
        $fk_lkp_deposit_rate = All::GetRow('lkp_deposit_rate', 'fk_lkp_location', $bh_hall->fk_lkp_location);

        if($request->post('jenisTempahan') == 5){ 
            $lkp_discount_type = 5;
        } else if($request->post('jenisTempahan') == 6){
            $lkp_discount_type = 2;
        } else if($request->post('jenisTempahan') == 7){
            $lkp_discount_type = 3;
        } else if($request->post('jenisTempahan') == 8){
            $lkp_discount_type = 4;
        }
        
        $data['discount_rate'] = ((All::GetRow('lkp_discount_type', 'id', $lkp_discount_type)->ldt_discount_rate) / 100);
        // Check each day if its weekends or weekdays
        $totalHall = 0;
        $data['totalHall'] = 0;
        while ($current_date <= $end_date) {
            try {
                $day_of_week = $current_date->format('N'); 
                $is_weekend = ($day_of_week == 6 || $day_of_week == 7);
                $date_range[] = $is_weekend;
                if($is_weekend){
                    $totalHall = All::GetAllRow('bh_hall_price', 'fk_bh_hall', $id)->where('bhp_day_cat', 2)->first();
                    $data['totalHall'] += $totalHall->bhp_total_price;
                } else {
                    $totalHall = All::GetAllRow('bh_hall_price', 'fk_bh_hall', $id)->where('bhp_day_cat', 1)->first();
                    $data['totalHall'] += $totalHall->bhp_total_price;
                }
                $current_date->modify('+1 day');
            } catch (\Throwable $th) {
            }
        }
        // $data['dates2'] = explode(" / ", $date);
        // $data['dates2'] = HP::getDatesBetween($data['dates2'][0], $data['dates2'][1]);
        
        $total_book_hall = 0;
        $total_gst = 0;
        foreach ($date_range as $dr) {
            $price = $bh_hall_price->first(function ($item) use ($dr) {
                return $item->bhp_day_cat == ($dr ? '2' : '1');
            });
            if ($price) {
                $deposit_rate = All::GetRow('lkp_gst_rate', 'id', $price->fk_lkp_gst_rate);
                $total_gst += $price->bhp_total_price * $deposit_rate->lgr_rate;
                $total_book_hall += $price->bhp_total_price;
            }
        }
        $discount_price = $total_book_hall * $data['discount_rate'];
        $hall_post_discount = $total_book_hall - $discount_price;

        $booking_time_24hr = (date_create_from_format('h:i A', $request->post('masaMula')))->format('H:i:s');
        $booking_time_24hr_end = (date_create_from_format('h:i A', $request->post('masaTamat')))->format('H:i:s');

        $dataMainBooking = array(
            'fk_users'                  => Session::get('user')['id'],
            'fk_lkp_status'             => 1,
            'fk_lkp_deposit_rate'       => $fk_lkp_deposit_rate->id,
            'fk_lkp_location'           => $bh_hall->fk_lkp_location,
            'fk_lkp_discount_type'      => $lkp_discount_type,
            // 'bmb_subtotal'              => $totalHall,
            // 'bmb_total_book_hall'       => $totalHall,
            'bmb_deposit_rm'            => 1000.00,
            'bmb_booking_no'            => $bookrn,
            'bmb_booking_date'          => date('Y-m-d H:i:s'),
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $mainbookingid = All::InsertGetID('main_booking', $dataMainBooking);

        $data_fileattachment = array(
            'fk_main_booking'           => $mainbookingid,
            'ba_date'                   => date('Y-m-d'),
            'ba_dir'                    => 'upload',
            'ba_full_path'              => '/var/www/html/spt/storage/app/upload/' . $bookrn,
            'ba_file_name'              => request()->filename,
            'ba_file_ext'               => request()->filetype,
            'ba_file_size'              => request()->filesize,
            'ba_generated_name'         => '',
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s'),
        );
        $attachmentid = All::InsertGetID('bh_attachment', $data_fileattachment);

        $dataBanking = [
            'fk_main_booking'   => $mainbookingid,
            'name'              => request()->namaPemohon,
            // 'date_refund'       => ,
            // 'reason'            => ,
            // 'comment'           => ,
            // 'amount_refund'     => ,
            'ic_no'             => request()->kadPengenalan,
            'phone_no'          => request()->noTel,
            'email'             => request()->emel,
            'bank_name'         => request()->namabank,
            'bank_reference'    => Crypt::encrypt(request()->akaunBank),
            // 'reference_no'      => ,
            // 'receipt_no'        => ,
            'brr_status'        => 0,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
        ];
        $attachmentid = All::InsertGetID('bh_refund_request', $dataBanking);
        
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['user']->id);
        $depositValidateMB = All::GetRow('lkp_deposit', 'fk_main_booking', $mainbookingid);
        if(!$depositValidateMB){
            $data_bank = [
                'name'                  => $data['user']->fullname,
                'ref_id'                => $data['user_detail']->bud_reference_id,
                'email'                 => $data['user_detail']->bud_email,
                'mobile_no'             => $data['user_detail']->bud_phone_no,
                'bank_name'             => request()->namaBank,
                'bank_account'          => request()->akaunBank,
                'fk_main_booking'       => $mainbookingid,
                'status'                => 0,
                // 'deposit_amount'        => ,
                'subsystem'             => 'SPS',
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
            ];
            $bankId = All::InsertGetID('lkp_deposit', $data_bank);
        }

        // Based on hall
        $dataHallBooking = array(
            'fk_bh_hall'            => $id,
            'fk_main_booking'       => $mainbookingid,
            // 'fk_bh_package'     => ,
            'fk_lkp_gst_rate'       => 3,
            // 'fk_bh_hall_usage'  => $id, // need to update
            // 'fk_bh_hall_usage'      => null,
            'bb_hall_status'        => 0,
            'bb_start_date'         => HP::date_format_for_db($request->post('bbd_start_date')),
            'bb_end_date'           => HP::date_format_for_db($request->post('bbd_end_date')),
            'hall_price'            => $data['totalHall'],
            // 'bb_discount_type_rm'   => $discount_price,
            'bb_no_of_day'          => $number_of_days,
            // 'bb_gst'                => $total_gst,
            'bb_total'              => $data['totalHall'],
            'bb_subtotal'           => $data['totalHall'],
            // 'bb_equipment_price'=>
            'created_at'            => date('Y-m-d H:i:s'),
        );
        $bhbookingid = All::InsertGetID('bh_booking', $dataHallBooking);

        $dataHallBookingDetail = array(
            'fk_bh_booking'                 => $bhbookingid,
            'fk_lkp_event'                  => $request->post("JenisAcaraTempDewan"),
            'fk_lkp_time_session'           => $request->post("sesiAcara"),
            'bbd_event_name'                => $request->post("namaAcara"),
            'bbd_event_description'         => $request->post("keteranganAcara"),
            'bbd_vvip'                      => $request->post("bilanganPesertaVVIP"),
            'bbd_vip'                       => $request->post("bilanganPesertaVIP"),
            'bbd_participant'               => $request->post("bilanganPesertaLain"),
            'bbd_total_no_of_pax'           => $request->post("bilanganPesertaVVIP") + $request->post("bilanganPesertaVIP") + $request->post("bilanganPesertaLain"),
            'bbd_user_apply'                => $request->post('namaPemohon'),
            'bbd_contact_no'                => $request->post('noTel'),
            'bbd_start_date'                => HP::date_format_for_db($request->post('bbd_start_date')),
            'bbd_end_date'                  => HP::date_format_for_db($request->post('bbd_end_date')),
            // 'bbd_start_time'                => $bbd_start_time,
            'bbd_start_time'                => $booking_time_24hr,
            'bbd_end_time'                  => $booking_time_24hr_end,
            // 'bbd_end_time'                  => $bbd_end_time,
            'created_at'                    => date('Y-m-d H:i:s'),
        );
        $query = All::InsertGetID('bh_booking_detail', $dataHallBookingDetail);

        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['main'] = All::GetRow('main_booking', 'id', $mainbookingid);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['date'] = $date;
        $dates = explode("/", $data['date']);
        if (count($dates) === 2) {
            $dates[0] = HP::date_format_for_db($dates[0]);
            $dates[1] = HP::date_format_for_db($dates[1]);
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
        } elseif (count($dates) === 1) {
            $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
            $endDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
        }
        
        $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
        $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        $data['datesBetween'] = $this->getDatesBetween($startDate, $endDate);
        $data['fasility'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id',  $id);
        $data['capacity'] = All::Show('bh_hall_usage', 'id', 'ASC')->where('fk_bh_hall_detail', $data['details']->id);
        $data['event'] = All::Show('lkp_event', 'id', 'ASC')->where('le_status', 1);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        // dd($data);

        return Redirect::to(url('hall/tempahan', [Crypt::encrypt($mainbookingid)]));
    }

    public function tempahan(Request $request, $booking){
        if(!Session::get('user')) {
            return redirect('auth');
        }
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate)/100;
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['temp'] = [];
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
            $data['temp'][] = $s->fk_bh_hall;
        }
        $data['fasility'] = All::GetRow('bh_hall', 'id', $data['bh_booking'][0]->fk_bh_hall);
        $data['datesBetween'] = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
        // dd($data);
        $presint = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description;

        if(request()->isMethod('post')){
            $statusKelengkapan = request()->statusKelengkapan;
            if($statusKelengkapan == 1) {
                $jumlahMejaBulat = request()->jumlahMejaBulat;
                $jumlahMejaPanjang = request()->jumlahMejaPanjang;
                $jumlahKerusi = request()->jumlahKerusi;
                $jumlahLCDlayar = request()->jumlahLCDlayar;
            } else {
                $jumlahMejaBulat = request()->jumlahMejaBulatHarian;
                $jumlahMejaPanjang = request()->jumlahMejaPanjangHarian;
                $jumlahKerusi = request()->jumlahKerusiHarian;
                $jumlahLCDlayar = request()->jumlahLCDlayarHarian;
            }
            $equipmentId = [1, 2, 3, 4];
            $equipmentByDays = array_map(null, $jumlahMejaBulat, $jumlahMejaPanjang, $jumlahKerusi, $jumlahLCDlayar);
            // dd($data);
            $bh_booking_id = $data['bh_booking'][0]->id;
            $dates = $this->getDatesBetween($data['bh_booking'][0]->bb_start_date, $data['bh_booking'][0]->bb_end_date);
            $bh_equipment_price = [];
            $totalEquipmentPrice = 0;
            foreach ($equipmentId as $a) {
                $bh_equipment_price[] = All::GetSpecRow('bh_equipment_price', 'fk_bh_equipment', $a)->where('be_day_cat', 1)->where('fk_lkp_discount_type', $data['main']->fk_lkp_discount_type)->first();
            }
            $result = [];
            foreach ($equipmentByDays[0] as $index => $value) {
                $maxIndex = 0;
                $maxValue = $equipmentByDays[$maxIndex][$index];
                foreach ($equipmentByDays as $arrayIndex => $day) {
                    if ($day[$index] > $maxValue) {
                        $maxValue = $day[$index];
                        $maxIndex = $arrayIndex;
                    }
                }
                $result[$index] = $maxIndex;
            }
            // dd($bh_equipment_price, $equipmentId);
            foreach ($equipmentByDays as $j => $e) {
                for ($i = 0; $i < count($equipmentId); $i++) {
                    $dataBHBookingEquipment = array(
                        'fk_bh_booking'     => $bh_booking_id,
                        'fk_bh_equipment'   => $equipmentId[$i],
                        'fk_lkp_gst_rate'   => $bh_equipment_price[$i]->fk_lkp_gst_rate,
                        'fk_bh_equipment_price' => $bh_equipment_price[$i]->id,
                        'bbe_booking_date'  => $dates[$j],
                        'bbe_price'         => $bh_equipment_price[$i]->be_price,
                        'bbe_quantity'      => $e[$i],
                        'bbe_diskaun_rm'    => 0.00,
                        // 'special_discount_rm'   => ,
                        // 'special_disc'      => ,
                        // 'baucer_rm'         => ,
                        // 'baucer'            => ,
                        'bbe_gst'           => $bh_equipment_price[$i]->be_gst_rm,
                        'bbe_total'         => ($result[$i] == $j) ? $bh_equipment_price[$i]->be_total_price * $e[$i] : 0.00,
                        // 'bbe_total'         => $bh_equipment_price[$i]->be_total_price,
                        'created_at'        => date('Y-m-d H:i:s'),
                    );
                    // dd($dataBHBookingEquipment, $equipmentByDays, $result, $j, $e, $i);
                    if($e[$i] != null && (int)$e[$i] != 0) {
                        if($result[$i] == $j){
                            $totalEquipmentPrice += $bh_equipment_price[$i]->be_total_price * $e[$i];
                        }
                        All::Insert('bh_booking_equipment', $dataBHBookingEquipment);
                    }
                }
            }

            $dataMainBooking = [
                'bmb_total_equipment'   => $totalEquipmentPrice,
                'bmb_subtotal'          => ($data['main']->bmb_total_book_hall + $totalEquipmentPrice),
                'bmb_deposit_rm'        => 1000,
                'bmb_deposit_rounding'  => 1000,
            ];
            $bh_quotation = All::GetUpdate('main_booking', $data['main']->id, $dataMainBooking);

            $quono = Hall::generatequono();
            $databh_quotation = [
                'fk_main_booking'       => $data['booking'],
                'fk_users'              => Session::get('user')['id'],
                'fk_lkp_discount_type'  => $data['main']->fk_lkp_discount_type,
                'bq_quotation_no'       => 'PPj/' .$presint.'/'.$data['main']->bmb_booking_no.'/'.$quono,
                'bq_quotation_date'     => date('Y-m-d'),
                // 'bq_quotation_status'   => 
                // 'bq_total_amount'       => 
                // 'bq_deposit'            => 
                // 'bq_payment_status'     => 0
            ]; 
            $bh_quotation = All::InsertGetId('bh_quotation', $databh_quotation);

            return Redirect::to(url('hall/rumusan', [Crypt::encrypt($data['booking'])]));
        }

        $data['numberOfDays'] = ((new DateTime($data['bh_booking'][0]->bb_start_date))->diff(new DateTime($data['bh_booking'][0]->bb_end_date)))->days + 1;
        return view('hall.public.tempahan', compact('data'));
    }

	public function rumusan(Request $request, $booking, $total = null, $depo = null){
        if(!Session::get('user')) {
            return redirect('auth');
        }
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate)/100;
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        // dd($data['bh_booking']);
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        // dd($data['kelengkapan']);
        $start_date = DateTime::createFromFormat('Y-m-d H:i:s', $data['bh_booking'][0]->bb_start_date);
        $end_date = DateTime::createFromFormat('Y-m-d H:i:s', $data['bh_booking'][0]->bb_end_date);
        $date_range = [];
        $current_date =  $start_date;
        
        while ($current_date <= $end_date) {
            $date_range[] = $current_date->format('d-m-Y');
            $current_date->modify('+1 day');
        }
        $data['date_range'] = $date_range;
        //$data['slot'] = Sport::priceSlot($data['booking']);
        if(request()->isMethod('post')){
            $total = Crypt::decrypt($total);
            $depo = Crypt::decrypt($depo);
            $data['depo'] = 1000.00;
            $data_main_booking = [
                'bmb_subtotal'          => $total, 
                'bmb_deposit_rm'        => $data['depo'],
                'bmb_total_book_hall'   => request()->jumlahdewan,
                'bmb_total_equipment'   => request()->jumlahkelengkapan,
                // 'bmb_rounding'          => 
                'bmb_deposit_rounding'  => 1000
            ];
            $query = All::GetUpdate('main_booking', $data['booking'], $data_main_booking);
            return Redirect::to(url('hall/maklumatbayaran', [Crypt::encrypt($data['booking'])]));
        }

        // dd($data, $data['date_range']);
        return view('hall.public.rumusan', compact('data'));
    }

	public function maklumatbayaran($booking, $draf = null) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate) / 100;
        $data['bh_rating'] = All::GetAllRow('bh_rating', 'fk_main_booking', $data['booking']);
        $data['bh_refund_request'] = All::GetAllRow('bh_refund_request', 'fk_main_booking', $data['booking']);
        
        if ($draf == null) {
            if ($data['main']->fk_lkp_status == 1) {
                $updateTempahan = [
                    // 'fk_lkp_status' => 2
                    'fk_lkp_status' => 19
                ];
                All::GetUpdate('main_booking', $data['main']->id, $updateTempahan);
                $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
            }
        }
        
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['temp'] = [];
        $data['booking_detail'] = [];
        
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
            $data['temp'][] = $s->fk_bh_hall;
        }
        
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC')->where('fpx_status', 1);
        $data['depositrate'] = All::Show('lkp_deposit_rate', 'id', 'ASC')->whereIn('id', $data['temp'])->values();
        // dd($data);
        // Calculate the total
        $total = 0;
        foreach ($data['bh_booking'] as $booking) {
            if (isset($booking->bb_no_of_day)) {
                $total += ($booking->hall_price * $booking->bb_no_of_day);
            }
        }
        $total += $data['main']->bmb_total_equipment;

        return view('hall.public.maklumattempahan', compact('data', 'total'));
    }
    

    //Bayaran//
	public function bayaran(Request $request, $booking, $price = null) {
        // $no = uniqid();//Hall::generatetransid();
        $data['booking'] = Crypt::decrypt($booking);
        $data['total'] = Crypt::decrypt($price);
        $main = All::GetRow('main_booking', 'id', $data['booking']);
        $paymentFpxExist = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $main->id);
        $no = Sport::generatetransid();
        // $no = '0002';
        $data['oderNo'] = $main->bmb_booking_no . $no;
        // if($main->fk_lkp_status == 2){
        //     $data['total'] = $main->bmb_deposit_rm;
        // } else {
        //     $data['total'] = $main->subtotal;
        // }
        // $data['total'] = "6822.00"; //$request->post('total');
        //$data['total1'] = "1600.00";
        //dd($request->post('total'));
        if($main->fk_lkp_status == 2) { // Depo + Pendahuluan
            $data_fpx = array(
                'fk_main_booking'           => $data['booking'],
                'fk_lkp_payment_type'       => 1,
                'fpx_trans_id'              => $data['oderNo'],
                'total_amount'              => $main->bmb_subtotal,
                'deposit_amount'            => $main->bmb_deposit_rm,
                'fpx_status'                => 0,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $query = All::InsertGetID('bh_payment_fpx', $data_fpx);
            $data_fpx = array(
                'fk_main_booking'           => $data['booking'],
                'fk_lkp_payment_type'       => 3,
                'fpx_trans_id'              => $data['oderNo'],
                'total_amount'              => $main->bmb_subtotal,
                'deposit_amount'            => $main->bmb_deposit_rm,
                'fpx_status'                => 0,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $query = All::InsertGetID('bh_payment_fpx', $data_fpx);
        } else {    // Penuh
            $data_fpx = array(
                'fk_main_booking'           => $data['booking'],
                'fk_lkp_payment_type'       => 2,
                'fpx_trans_id'              => $data['oderNo'],
                'total_amount'              => $main->bmb_subtotal,
                'deposit_amount'            => $main->bmb_deposit_rm,
                'fpx_status'                => 0,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $query = All::InsertGetID('bh_payment_fpx', $data_fpx);
        }
        // $data_main = array(
        //     'fk_lkp_status'              => 2,
        //     'bmb_subtotal'               => $data['total'],
        //     'bmb_rounding'               => 0,
        //     'updated_at'                 => date('Y-m-d H:i:s'),
        //     'updated_by'                 => Session::get('user')['id'],
        // );
        // $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
        // $data_fpx = array(
        //     'fk_main_booking'            => $data['booking'],
        //     'fk_lkp_payment_type'        => 2,
        //     'fpx_trans_id'               => $data['oderNo'] . $no,
        //     'total_amount'               => $data['total'],
        //     'created_at'                 => date('Y-m-d H:i:s'),
        //     'updated_at'                 => date('Y-m-d H:i:s'),
        //     'updated_by'                 => Session::get('user')['id'],
        // );
        // $query = All::InsertGetID('bh_payment_fpx', $data_fpx);

        $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';
        $data1 = array("payload" => array(
            "subsysId"  => "SPT",
            // "password"  => "SPt!!@2022",
            "password"  => "S4T@!2024dEev",
            "orderNo"   => $data['oderNo'],
            "description" => $data['oderNo'],
            "txnTime" => date('Y-m-d H:i:s'),
            // "amount"  => "1.00"//$data['total']  
            "amount"  => $data['total']
        ));
        $postdata = json_encode($data1);

        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        curl_close($ch);

        return view('hall.public.bayaran', compact('data', 'response'));
    }
	
	public function bayaranBalance(Request $request, $booking) {
        $no = uniqid();//Hall::generatetransid();
        $data['booking'] = Crypt::decrypt($booking);
        $main = All::GetRow('main_booking', 'id', $data['booking']);
        $data['oderNo'] = $main->bmb_booking_no; //.$no;
        $data['total'] = "6622.00"; //$request->post('total');
        //$data['total1'] = "1600.00";
        //dd($request->post('total'));

        $data_fpx = array(
            'fk_main_booking'            => $data['booking'],
            'fk_lkp_payment_type'        => 2,
            'fpx_trans_id'               => $data['oderNo'],
            'total_amount'               => $data['total'],
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::InsertGetID('et_payment_fpx', $data_fpx);

        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';
        $data1 = array("payload" => array(
            "subsysId"  => "SPT",
            "password"  => "SPt!!@2022",
            "orderNo"   => $data['oderNo'],
            "description" => $data['oderNo'],
            "txnTime" => date('Y-m-d H:i:s'),
            "amount"  => "6622.00"//$data['total']  
        ));
        $postdata = json_encode($data1);

        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        curl_close($ch);

		if (Session::get('user')['id'] == "50554")
		{
			return view('hall.public.bayaran', compact('data', 'response'));
		}
    }
	
	public function status(){
        $data['orderNo'] = "SPS221003781900001";
        $data['status'] = "FAILED";
        $data['txnReference'] = "2210030928330089";
        $data['txnId'] = "FPX2200000600";
        $data['txnTime'] = date("Y-m-d H:i:s", strtotime("20221003092833"));
        $data['amount'] = "8.00";
        $data['bankname'] = "SBI Bank A";
		
		if (Session::get('user')['id'] == "50554")
		{
			return view('hall.public.status', compact('data'));
		}
    }
	
    public function mybookings($type = null) {
        $data['jenis'] = $type;
        $data['hall'] = Hall::laporan_user(Session::get('user')['id']);
        $data['event_complete'] = Hall::laporan_user(Session::get('user')['id'])->where('fk_lkp_status', 5);

        // dd($data);
        return view('hall.public.tempahansaya', compact('data'));
    }

    public function getDatesBetween($startDate, $endDate) {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }
    public function getTimesBetween($startTime, $endTime) {
        $times = [];

        $start = Carbon::parse($startTime);
        $end = Carbon::parse($endTime);

        while ($start->lte($end)) {
            $times[] = $start->toTimeString();
            $start->addDay();
        }

        return $times;
    }
	
    public function batal($id){
        $data['id'] = Crypt::decrypt($id);
        $cancelBooking = [
            'deleted_at'    => date('Y-m-d'),
            'deleted_by'    => Session('user.id'),
        ];
        $query = All::GetUpdateSpec('main_booking', 'bmb_booking_no', $data['id'], $cancelBooking);
        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall'));
        }
    }

	public function cancel(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            // dd($data['main']);
        }
		
		if (Session::get('user')['id'] == "50554")
		{
			return view('hall.cancel.index', compact('data'));
		}
    }

    public function submitcancel(Request $request, $id) {
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_lkp_status'           => 10,
            'bmb_reason_cancel'       => request()->reason,
            'updated_by'              => Session::get('user')['id'],
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('main_booking', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/cancel'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/cancel'));
        }
    }

    public function duplicate(Request $request) {
        $data['post'] = false;
        $data['slot'] = Hall::duplicateAll();
        // dd($data['slot'][0]);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['slot'] = Hall::duplicate($data['id'], $data['date'], $data['post']);
            // dd($data, $data['id']);
        }
        //$data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		$data['location'] = Hall::fasilityActive();
		// dd($data['location']);
        return view('hall.duplicate.index', compact('data'));
    }

    public function available(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['slot'] = Hall::slot($data['id'], $data['date']);
            $data['facility'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $data['id'])->where('efd_status', 1);
            // dd($data);
        }
        //$data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		$data['location'] = Hall::fasility();
		
		if (Session::get('user')['id'] == "50554")
		{
			return view('hall.available.index', compact('data'));
		}
    }

    public function ajax(Request $request) {
        $location_id = $request->post('location_id');
        $location = Sport::One($location_id);
        if(count($location)>0)
        {
            $location_box = '';
            $location_box .= '<option value="">Sila Pilih</iption>';
            foreach ($location as $k){
                $location_box .= '<option value="'.$k->id. '">' .$k->eft_type_desc.'</option>';
            }
            echo json_encode($location_box);
        }else{
            $location_box = '';
            $location_box .= '<option value="">Sila Pilih</iption>';
            $location_box .= '<option value="0">Tiada</iption>';
            echo json_encode($location_box);
        }
    }

    public function pemulangandeposit(Request $request, $id) {
        $data['id'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['id']);
        $data['bh_booking'] = All::GetRow('bh_booking', 'fk_main_booking', $data['main']->id);
        $lastEndDate = $data['bh_booking']->bb_end_date;
        $currentDate = date('Y-m-d');
        $endDatePlus7Days = date('Y-m-d', strtotime($lastEndDate . ' + 7 days'));
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['id']);
        $data['online'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['id']);
        $validate = All::GetAllRow('bh_refund_request', 'fk_main_booking', $data['id'])->where('deleted_at', null)->where('brr_status', '!=', 2);
        if(($data['online']->where('fk_lkp_payment_type', 3))->isNotEmpty() && $data['main']->fk_lkp_status == 5) {
            $data_rowTransaction = $data['online']->where('fk_lkp_payment_type', 1)->first();
        } else if(($data['online']->where('fk_lkp_payment_type', 3))->isNotEmpty() && $data['main']->fk_lkp_status == 10) {
            $data_rowTransaction = $data['online']->where('fk_lkp_payment_type', 2)->first();
        } else if(($data['online']->where('fk_lkp_payment_type', 2))->isNotEmpty() && $data['main']->fk_lkp_status == 10) {
            $data_rowTransaction = $data['online']->where('fk_lkp_payment_type', 3)->first();
        }
        if(count($validate) == 0) {
            $data_refund = [
                'fk_main_booking'       => $data['id'],
                'name'                  => request()->name,
                // 'date_refund'           => 
                // 'reason'                => 
                // 'comment'               => 
                // 'amount_refund'         => 
                'ic_no'                 => request()->ic,
                'phone_no'              => request()->phoneno,
                'email'                 => request()->email,
                'bank_name'             => request()->bankname,
                'bank_reference'        => request()->bankaccount,
                'receipt_no'            => (count($data['kaunter']) > 0) ? $data['kaunter']->bh_receipt_number : $data_rowTransaction->fpx_serial_no,
                'brr_status'            => 1,
                'created_at'            => now()
            ];
            $refund_request_id = All::InsertGetID('bh_refund_request', $data_refund);
        }
        // if (($endDatePlus7Days >= $currentDate)) {
        // if (($refund_request_id)) {
        if (($validate)) {
            Session::flash('flash', 'Success');
            // return redirect('/hall/my-bookings/my');
            // return redirect()->back();
        }else{
            Session::flash('flash', 'Failed'); 
            // return redirect('/hall/my-bookings/my');
            // return redirect()->back();
        }
    }

    public function submitrating(Request $request){
        $id = Crypt::decrypt(request()->id);
        $rating = request()->rating;
        $highlight = request()->highlight;
        $validate = All::GetAllRow('bh_rating', 'fk_main_booking', $id);
        if(count($validate) == 0){
            $data_rating = [
                'fk_users'              => session('user.id'),
                'fk_main_booking'       => $id,
                // 'hall_id'               => ,
                // 'et_facility_type_id'   => ,
                'br_comment'            => $highlight,
                'br_total_rating'       => $rating * 20,
                'created_at'            => now(),
                'updated_at'            => now(),
            ];
            $rating_id = All::InsertGetID('bh_rating', $data_rating);
        } else {
            $response = ['status' => 'success'];
            return response()->json($response);
        }
        if($rating_id) {
            $response = ['status' => 'success'];
            return response()->json($response);
        } else {
            $response = ['status' => 'failed'];
            return response()->json($response, 400);
        }
    }


    // For Hall (not paid in 3 days) [implement in cron]
    public function test(Request $request) {
        $data['bh_booking'] = All::JoinTwoTable('bh_booking', 'main_booking')->whereIn('fk_lkp_status', [2, 3]);
        foreach ($data['bh_booking'] as $bb) {
            if (!empty($bb) && $bb->created_at) {
                $createdAt = date_create($bb->created_at); 
                $now = new DateTime();
                if ($createdAt) {
                    $interval = $now->diff($createdAt);
                    if ($interval->days > 2) {
                        $batalTempahan = [
                            'fk_lkp_status'     => 16,
                            'bmb_reason_cancel' => 'Tamat Tempoh Pembayaran',
                            'updated_at'        => now()
                        ];
                        $query = All::GetUpdate('main_booking', $bb->fk_main_booking, $batalTempahan);
                        Log::info("Booking order : ". $bb->bmb_booking_no ." canceled due to non-payment after 3 days");
                    } 
                } else {
                    Log::info("Error: Unable to create DateTime object for created_at at order no: ". $bb->bmb_booking_no);
                }
            } else {
                Log::info("Error: Unable to retrieve bh_booking data or created_at is null at order no: ". $bb->bmb_booking_no);
            }
        }
    }

    public function refund(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            // dd($data['main']);
        }
		
		if (Session::get('user')['id'] == "50554")
		{
			return view('hall.refund.index', compact('data'));
		}
    }

    public function refundinfo($booking, $draf = null) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate)/100;
        $data['bh_rating'] = All::GetAllRow('bh_rating', 'fk_main_booking', $data['booking']);
        $data['bh_refund_request'] = All::GetAllRow('bh_refund_request', 'fk_main_booking', $data['booking']);
        if($draf == null) {
            if($data['main']->fk_lkp_status == 1){
                $updateTempahan = [
                    // 'fk_lkp_status'     => 2
                    'fk_lkp_status'     => 19
                ];
                All::GetUpdate('main_booking', $data['main']->id, $updateTempahan);
                $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
            }
        }
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['temp'] = [];
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
            $data['temp'][] = $s->fk_bh_hall;
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC')->where('fpx_status', 1);
        $data['depositrate'] = (All::Show('lkp_deposit_rate', 'id', 'ASC')->whereIn('id', $data['temp']))->values();
        return view('hall.refund.maklumattempahan', compact('data'));
    }
}

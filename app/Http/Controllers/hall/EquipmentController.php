<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class EquipmentController extends Controller
{
    public function show() {
        $data['list'] = All::Show('bh_equipment', 'id', 'ASC');
        return view('hall.equipment.lists', compact('data'));
    }

    public function form(){
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('hall.equipment.form', compact('data'));
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'be_name'           => request()->nama,
            'be_quantity'       => request()->kuantiti,
            'be_code'       => request()->kod,
            'be_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('bh_equipment', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $data['list'] = All::GetRow('bh_equipment', 'id', $id);
        return view('hall.equipment.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'be_name'           => request()->nama,
            'be_quantity'       => request()->kuantiti,
            'be_code'           => request()->kod,
            'be_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_equipment', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_equipment', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/equipment'));
        }
    }
}

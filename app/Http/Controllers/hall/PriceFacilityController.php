<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class PriceFacilityController extends Controller
{
    public function show() {
        $data['list'] = All::Show('bh_hall_price', 'id', 'ASC');
        return view('hall.facilityprice.lists', compact('data'));
    }

    public function form(){
        $data['bh_hall'] = All::Show('bh_hall', 'id', 'ASC');
        $data['bh_hall_price'] = All::Show('bh_hall_price', 'id', 'ASC');
        $data['lkp_gst_rate'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        return view('hall.facilityprice.form', compact('data'));
    }

    public function add(Request $request){
        $data = array(
            'fk_bh_hall'        => request()->dewan,
            'fk_lkp_gst_rate'   => request()->kadargst,
            'bhp_day_cat'       => request()->kategorihari,
            // 'bhp_time_cat'      => request()->jam,
            'bhp_discount'      => request()->diskaunPer,
            'bhp_discount_rm'   => request()->diskaunRM,
            'bhp_gst_rm'        => request()->gstrm,
            'bhp_total_price'   => request()->jumlahhargarm,
            'bhp_status'        => 1,
        );
        $query = All::Insert('bh_hall_price', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('bh_hall_price', 'id', $id);
        $data['bh_hall'] = All::Show('bh_hall', 'id', 'ASC');
        $data['bh_hall_price'] = All::Show('bh_hall_price', 'id', 'ASC');
        $data['lkp_gst_rate'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 1);
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');
        $data['gst'] = All::Show('lkp_gst_rate', 'id', 'ASC');
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['package'] = All::Show('et_package', 'id', 'ASC');
        $data['slot'] = All::Show('lkp_slot_cat', 'id', 'ASC');
        $data['list'] = All::GetRow('et_facility_price', 'id', $id);
        return view('hall.facilityprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;
        if($status == 'on'){
            $status1 = 1;
        } else {
            $status1 = 0;
        }

        $data = array(
            'fk_bh_hall'        => request()->dewan,
            'fk_lkp_gst_rate'   => request()->kadargst,
            'bhp_day_cat'       => request()->kategorihari,
            // 'bhp_time_cat'      => request()->jam,
            'bhp_discount'      => request()->diskaunPer,
            'bhp_discount_rm'   => request()->diskaunRM,
            'bhp_gst_rm'        => request()->gstrm,
            'bhp_total_price'   => request()->jumlahhargarm,
            'bhp_status'        => $status1,
            'bhp_price'         => request()->jumlahhargarm,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_hall_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_hall_price', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facilityprice'));
        }
    }
}

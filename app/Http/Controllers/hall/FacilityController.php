<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\All;

class FacilityController extends Controller
{
    public function show() {
        $data['facility'] = All::Show('bh_hall', 'id', 'ASC');
        // $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        return view('hall.facility.lists', compact('data'));
    }

    public function form(){
        $data['uuid'] = Str::uuid()->toString();
        $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        $data['bh_hall'] = All::Show('bh_hall', 'id', 'ASC');
        $data['facility_type'] = All::Show('et_facility_type', 'id', 'ASC');
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        return view('hall.facility.form1', compact('data'));
    }

    public function add(Request $request){
        $uuid = $request->post('uuidKey');

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/main/'.$uuid);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
            // dd('Request Has No File');
        }

        if ($request->hasFile('file1')) {
            $path1 = public_path('assets/upload/virtual/'.$uuid);
            File::ensureDirectoryExists($path1);
            $image1 = $request->file('file1');
            $filename1 = $image1->getClientOriginalName();
            $request->file('file1')->move($path1, $filename1);
        }else{
            // dd('Request Has No File');
        }

        $data1 = array(
            'bh_name'           => $request->post('dewan'),
            'fk_lkp_location'   => $request->post('lokasi'),
            'bh_hall_owner'     => $request->post('bawahpengurusan'),
            'bh_status'         => $request->post('status'),
            // 'bh_parent_id'      => $request->post('dewanutama'),
            'bh_code'           => $request->post('kod'),
            'bh_percint'        => $request->post('presint'),
            'bh_description'    => $request->post('keterangan'),
            'bh_size'           => $request->post('bh_size'),
            // 'bh_filename'       => $request->post(''),
            // 'bh_file_location'  => $request->post(''),
            'bh_code_fee'       => $request->post('kodfee'),
            // 'bh_reason'         => $request->post(''),
            'bh_code_deposit'   => $request->post('kodfeedepo'),
            'feecode_depo_online'   => $request->post('koddepoonline'),
            'feecode_online'    => $request->post('kodpenuhonline'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        $query1 = All::Insert('bh_hall', $data1);

        $data2 = array(
            'fk_bh_hall'        => $request->post('dewanutama'),
            'bhd_cap_banquet'   => $request->post('kapasitibanquet'),
            'bhd_cap_seminar'   => $request->post('kapasitiseminar'),
            // 'bhd_pameran'       => $request->post('kod'),
            // 'bhd_other'         => $request->post('presint'),
            'bhd_wifi'          => $request->post('wifi'),
            'bhd_capasity'      => $request->post('kapasitibiasa'),
            'bhd_option'        => $request->post('pecahan'),
            // 'bh_code_deposit'   => $request->post('kodfeedepo'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        $query2 = All::Insert('bh_hall_detail', $data2);

        // dd($data1,$query1,$data2,$query2);
        if ($query1 && $query2) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facility'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['facility'] = All::Show('bh_hall', 'id', 'ASC');
        // $data['facility'] = All::Show('et_facility', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        // $data['list'] = All::GetRow('et_facility_type', 'id', $id);
        $data['bh_hall'] = All::Show('bh_hall', 'id', 'ASC');
        $data['list'] = All::GetRow('bh_hall', 'id', $id);
        $data['details'] = All::GetRow('bh_hall_detail', 'id', $id);
        $data['usage'] = All::GetAllRow('bh_hall_usage', 'fk_bh_hall_detail', $id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');

        // dd($data, $id);
        return view('hall.facility.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $uuid = $request->post('uuidKey');
        // dd($request->post());
        $dataBhHall = array(
            'bh_name'           => $request->post('dewan'),
            'fk_lkp_location'   => $request->post('lokasi'),
            'bh_hall_owner'     => $request->post('bawahpengurusan'),
            'bh_status'         => $request->post('status'),
            // 'bh_parent_id'      => $request->post('dewanutama'),
            'bh_code'           => $request->post('kod'),
            'bh_percint'        => $request->post('presint'),
            'bh_description'    => $request->post('keterangan'),
            'bh_size'           => $request->post('bh_size'),
            // 'bh_filename'       => $request->post(''),
            // 'bh_file_location'  => $request->post(''),
            'bh_code_fee'       => $request->post('kodfee'),
            // 'bh_reason'         => $request->post(''),
            'bh_code_deposit'   => $request->post('kodfeedepo'),
            'feecode_depo_online'   => $request->post('koddepoonline'),
            'feecode_online'    => $request->post('kodpenuhonline'),
            // 'latitude'         => $request->post('latitude'),
            // 'longitude'         => $request->post('longitude'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        $query1 = All::GetUpdate('bh_hall', $id, $dataBhHall);

        // $dataBhCapacity = array(
        //     'fk_bh_hall_detail'     => $request->post(''),
        //     'type'                  => $request->post(''),
        //     'capasity'              => $request->post('kapasiti'),
        //     'created_at'            => date('Y-m-d H:i:s'),
        //     'updated_at'            => date('Y-m-d H:i:s'),
        //     'updated_by'            => Session::get('user')['id'],
        // );
        // $query = All::GetUpdate('bh_hall_usage', $id, $dataBhCapacity);

        $dataWifi = array(
            'fk_bh_hall'            => $request->post('dewanutama'),
            'bhd_cap_banquet'       => $request->post('kapasitibanquet'),
            'bhd_cap_seminar'       => $request->post('kapasitiseminar'),
            // 'bhd_pameran'       => $request->post('kod'),
            // 'bhd_other'         => $request->post('presint'),
            'bhd_wifi'              => $request->post('wifi'),
            'bhd_capasity'          => $request->post('kapasitibiasa'),
            'bhd_option'            => $request->post('pecahan'),
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user')['id'],
        );
        $query2 = All::GetUpdate('bh_hall_detail', $id, $dataWifi);
        // if ($request->hasFile('file')) {
        //     $path = public_path('assets/upload/main/'.$uuid);
        //     File::ensureDirectoryExists($path);
        //     $image = $request->file('file');
        //     $filename = $image->getClientOriginalName();
        //     $request->file('file')->move($path, $filename);

        //     $data = array(
        //         'cover_img'         => $filename,
        //         'created_at'        => date('Y-m-d H:i:s'),
        //         'updated_at'        => date('Y-m-d H:i:s'),
        //         'updated_by'        => Session::get('user')['id'],
        //     );
        //     $query = All::GetUpdate('et_facility_type', $id, $data);
        // }

        // if ($request->hasFile('file1')) {
        //     $path1 = public_path('assets/upload/virtual/'.$uuid);
        //     File::ensureDirectoryExists($path1);
        //     $image1 = $request->file('file1');
        //     $filename1 = $image1->getClientOriginalName();
        //     $request->file('file1')->move($path1, $filename1);

        //     $data = array(
        //         'virtual_img'       => $filename1,
        //         'created_at'        => date('Y-m-d H:i:s'),
        //         'updated_at'        => date('Y-m-d H:i:s'),
        //         'updated_by'        => Session::get('user')['id'],
        //     );
        //     $query = All::GetUpdate('et_facility_type', $id, $data);
        // }

        if ($query1 && $query2) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facility'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_hall', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facility'));
        }
    }

    public function imageshow($id){
        $id = Crypt::decrypt($id);
        $data['id'] = $id;
        $data['facility'] = All::GetAllRowWithoutDeleted_at('bh_hall_image', 'fk_bh_hall', $id, 'id', 'ASC');
      
        // dd($data, $id);
        return view('hall.facility.imagelist', compact('data'));
    }

    public function imageform($id){
        $id = Crypt::decrypt($id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        $data['id'] = $id;
        return view('hall.facility.imageform', compact('data'));
    }

    public function imageadd(Request $request, $id){
        $id = Crypt::decrypt($id);

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/pic/'.$id);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
            dd('Request Has No File');
        }

        $data = array(
            'fk_bh_hall'             => $id,
            'img'                    => $filename,
            'title'                  => $request->post('tajuk'),
            'created_at'             => date('Y-m-d H:i:s'),
            'updated_at'             => date('Y-m-d H:i:s'),
            'updated_by'             => Session::get('user')['id'],
        );
        $query = All::Insert('bh_hall_image', $data);

        // dd($query);
        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facility/image',Crypt::encrypt($id)));
        }
    }

    public function imagedelete($id, $file){
        $id = Crypt::decrypt($id);
        $file = Crypt::decrypt($file);

        $image = All::GetRow('bh_hall_image', 'id', $file);

        $path = public_path('assets/upload/pic/'.$id.'/'.$image->img);
        if (File::exists($path)) {
            File::delete($path);
        }

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('bh_hall_image', $file, $data);
        // dd($id,$file,$image,$data,$query);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/facility/image',Crypt::encrypt($id)));
        }
    }
}

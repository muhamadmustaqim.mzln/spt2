<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Sport;
use App\Models\Hall;

class DashboardController extends Controller
{
    public function hall() {
        $data['laporan_harian'] = Hall::laporan_harian();
        $data['laporan_keseluruhan'] = Hall::laporan_keseluruhan();
        $data['laporan_bulanan'] = Hall::laporan_bulanan();
        $data['laporan_dalaman'] = Hall::laporan_dalaman();
        $data['laporan_tertunggak'] = Hall::laporan_tertunggak();
        $data['laporan_user'] = Hall::laporan_user(Session::get('user')['id']);
        $data['laporan_pengesahan'] = Hall::laporan_pengesahan();
        $data['laporan_selesai'] = Hall::laporan_selesai();

        return view('hall.dashboard.index', compact('data'));
    }

    public function report($type) {
        $data['type'] = $type;
        if ($type == 'harian') {
            $data['laporan'] = Hall::laporan_harian();
            $data['title'] = 'Tempahan Harian';
        } else if ($type == 'keseluruhan') {
            $data['laporan'] = Hall::laporan_keseluruhan();
            $data['title'] = 'Tempahan Keseluruhan';
        } else if ($type == 'bulanan') {
            $data['laporan'] = Hall::laporan_bulanan();
            $data['title'] = 'Tempahan Bulanan';
        } else if ($type == 'dalaman') {
            $data['laporan'] = Hall::laporan_dalaman();
            $data['title'] = 'Tempahan Dalaman';
        } else if ($type == 'tertunggak') {
            $data['laporan'] = Hall::laporan_tertunggak();
            $data['title'] = 'Tempahan Tertunggak';
        } else if ($type == 'my') {
            $data['laporan'] = Hall::laporan_user(Session::get('user')['id']);
            $data['title'] = 'Tempahan Saya';
        } else if ($type == 'pengesahan') {
            $data['laporan'] = Hall::laporan_pengesahan();
            $data['title'] = 'Menunggu Pengesahan';
            $data['type'] = 'pengesahan';
        } else if ($type == 'pemeriksaan') {
            $data['laporan'] = Hall::laporan_selesai();
            $data['title'] = 'Menunggu Pemeriksaan';
        }

        return view('dashboard.report.report', compact('data'));
    }

	public function maklumatbayaran($booking) {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['booking_detail'] = [];
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        if($data['user']->isAdmin == 1){
            $data['lkp_discount_rate'] = 1 - (All::GetRow('lkp_discount_type', 'id', $data['main']->fk_lkp_discount_type)->ldt_discount_rate) / 100;
        } else {
            $data['lkp_discount_rate'] = 1;
        }
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $total = 0;
        foreach ($data['bh_booking'] as $booking) {
            if (isset($booking->bb_no_of_day)) {
                $total += ($data['main']->bmb_total_book_hall * $booking->bb_no_of_day);
            }
        }
        $total += $data['main']->bmb_total_equipment;
        return view('hall.dashboard.maklumattempahan_all', compact('data', 'total'));
    }

    public function maklumatkaunter(Request $request, $booking) {
        $data['booking'] = Crypt::decrypt($booking);
        if(request()->isMethod('post')){
            $caraBayaran = $request->post('carabayaran');
            if($caraBayaran == 4) {     // FPX
                $updateMainBooking = [
                    'fk_lkp_status'     => 2
                ];
                $query = All::GetUpdate('main_booking', $data['booking'], $updateMainBooking);
                if ($query) {
                    Session::flash('flash', 'Success'); 
                    return redirect()->back();
                }else{
                    Session::flash('flash', 'Failed'); 
                    return redirect()->back();
                }
            } else if($caraBayaran) {   // LO/PO
                dd('lopo');
            }
        }
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');

        return view('hall.dashboard.maklumatkaunter', compact('data'));
    }

    public function deposit(Request $request) {
        $data['type'] = 1;
        $data['user_profiles'] = All::GetRow('user_profiles', 'id', session('user.id'));
        $data['status'] = All::ShowAll('lkp_status')->whereIn('id', [10, 4,5])->pluck('id');
        if(request()->isMethod('post')){
            $data['type'] = request()->jenis;
        }
        if($data['user_profiles']->fk_lkp_location != 0 || $data['user_profiles']->fk_lkp_location != null){
            $data['main'] = All::GetAllRow('main_booking', 'fk_lkp_status', $data['status'])->where('fk_lkp_location', '=', $data['user_profiles']->fk_lkp_location)->pluck('id');
        } else {
            $data['main'] = All::GetAllRow('main_booking', 'fk_lkp_status', $data['status'])->where('fk_lkp_location', '=', 1)->pluck('id');
        }
        $data['bh_refund_req'] = All::GetAllRow('bh_refund_request', 'brr_status', 1);
        $data['lists'] = Hall::refundList($data['type'], $data['main'])->toArray();

        return view('dashboard.report.deposit', compact('data'));
    }

    public function depositcancel(Request $request, $id) {
        $id = Crypt::decrypt($id);
        $data_refund_dlt = [
            'deleted_at'    => now()
        ];
        All::GetUpdate('bh_refund_request', $id, $data_refund_dlt);
        return redirect()->back();
    }

    public function approval(Request $request, $id) {
        // $data['mbid'] = Crypt::decrypt($id);
        // $data['main'] = All::GetRow('main_booking', 'id', $data['mbid']);
        // $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        // $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);

        // $data['date'] = $data['main']->bmb_booking_date;
        // $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        // $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);

        // $data['payment_mode'] = All::Show('lkp_payment_mode')->where('id', '!=', 4);

        // $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mbid']);
        // $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));

        // $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        // $data['et_hall_time'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['et_hall_book']->pluck('id'))->whereNull('deleted_at');
        // $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility'][0]->fk_et_facility_type);

        // $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'))->whereNull('deleted_at');

        $data['booking'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        if(request()->isMethod('post')){
            $type = request()->type;
            if($type == 1) {
                if($request->hasFile('fail')){
                    $path = public_path('/upload_document/main_booking/' . $data['mbid']);
                    File::ensureDirectoryExists($path);
                    $image1 = $request->file('fail');
                    $filename = $image1->getClientOriginalName();
                    $request->file('fail')->move($path, $filename);

                    $filePath = $path . '/' . $filename;
                    $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $fileType = mime_content_type($filePath); 
                    $fileSize = filesize($filePath);

                    $file_data = array(
                        'fk_main_booking'   => $data['mbid'],
                        'ba_date'           => date('Y-m-d'),
                        'ba_dir'            => 'upload_document',
                        'ba_full_path'      => $path,
                        'ba_file_name'      => $filename,
                        'ba_file_ext'       => $file_ext,
                        'ba_file_size'      => $fileSize,
                        // 'ba_generated_name' => 
                        // 'ba_status'         => 
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                        'updated_by'        => Session::get('user')['id'],
                    );
                    $query = All::Insert('bh_attachment', $file_data);

                    return redirect()->back();
                } else {
                    return redirect()->back();
                }
            } else {
                if($data['main']->internal_indi){
                    
                }
                $tindakankelulusan = request()->tindakan;
                if($tindakankelulusan == 1){
                    $update_main_booking = [
                        'fk_lkp_status'     => 2,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', $data['mbid'], $update_main_booking);

                    return redirect('/sport/maklumatbayaran/'. Crypt::encrypt($data['mbid']));
                } else {
                    $update_main_booking = [
                        'fk_lkp_status'     => 14,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', $data['bmid'], $update_main_booking);
                }
            }
        }
        $data['bh_attachment'] = All::GetAllRow('bh_attachment', 'fk_main_booking', $data['mbid'])->where('deleted_at', NULL);
        // dd($data);

        return view('sport.dashboard.approval', compact('data'));
    }

    public function pengesahan(Request $request, $id) {
        $data['booking'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        // if(request()->isMethod('post')){
        //     $caraBayaran = $request->post('carabayaran');
        //     if($caraBayaran == 4) {     // FPX
        //         $updateMainBooking = [
        //             'fk_lkp_status'     => 2
        //         ];
        //         $query = All::GetUpdate('main_booking', $data['booking'], $updateMainBooking);
        //         if ($query) {
        //             Session::flash('flash', 'Success'); 
        //             return redirect()->back();
        //         }else{
        //             Session::flash('flash', 'Failed'); 
        //             return redirect()->back();
        //         }
        //     } else if($caraBayaran) {   // LO/PO
        //         dd('lopo');
        //     }
        // }
        if(request()->isMethod('post')){
            $type = request()->type;
            if($type == 1) {
                if($request->hasFile('fail')){
                    $path = public_path('/upload_document/main_booking/' . $data['main']->id);
                    File::ensureDirectoryExists($path);
                    $image1 = $request->file('fail');
                    $filename = $image1->getClientOriginalName();
                    $request->file('fail')->move($path, $filename);

                    $filePath = $path . '/' . $filename;
                    $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $fileType = mime_content_type($filePath); 
                    $fileSize = filesize($filePath);

                    $file_data = array(
                        'fk_main_booking'   => $data['main']->id,
                        'ba_date'           => date('Y-m-d'),
                        'ba_dir'            => 'upload_document',
                        'ba_full_path'      => $path,
                        'ba_file_name'      => $filename,
                        'ba_file_ext'       => $file_ext,
                        'ba_file_size'      => $fileSize,
                        // 'ba_generated_name' => 
                        // 'ba_status'         => 
                        'created_at'        => date('Y-m-d H:i:s'),
                        'updated_at'        => date('Y-m-d H:i:s'),
                        'updated_by'        => Session::get('user')['id'],
                    );
                    $query = All::Insert('bh_attachment', $file_data);

                    return redirect()->back();
                } else {
                    return redirect()->back();
                }
            } else {
                if($data['main']->internal_indi){
                    
                }
                $tindakankelulusan = request()->tindakan;
                if($tindakankelulusan == 1){
                    $update_main_booking = [
                        'fk_lkp_status'     => 2,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', $data['main']->id, $update_main_booking);

                    return redirect('/hall/maklumatbayaran/'. Crypt::encrypt($data['main']->id));
                } else {
                    $update_main_booking = [
                        'fk_lkp_status'     => 14,
                        'updated_at'        => date('Y-m-d'),
                        'updated_by'        => Session::get('user.id')
                    ];
                    $query = All::GetUpdate('main_booking', ($data['main']->id), $update_main_booking);
                }
            }
        }
        $data['bh_attachment'] = All::GetAllRow('bh_attachment', 'fk_main_booking', $data['booking'])->where('deleted_at', NULL);
        // dd($data);
        return view('hall.dashboard.pengesahan', compact('data'));
    }

    public function pemeriksaan(Request $request, $id) {
        $id = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'id', $data['main']->fk_users);
        $data['bh_booking'] = All::GetRow('bh_booking', 'fk_main_booking', $data['main']->id);
        $data['bh_booking_detail'] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $data['bh_booking']->id);
        if(request()->isMethod('post')){
            $data['tarikhPemeriksaan'] = $request->post('tarikhPemeriksaan');
            $data['masaPemeriksaan'] = $request->post('masaPemeriksaan');
            $data['pegawai'] = $request->post('pegawai');
            $data['dewan'] = $request->post('dewan');
            $data['catatanDewan'] = $request->post('catatanDewan');
            $data['auditorium'] = $request->post('auditorium');
            $data['catatanAuditorium'] = $request->post('catatanAuditorium');
            $data['penyediaan'] = $request->post('penyediaan');
            $data['catatanPenyediaan'] = $request->post('catatanPenyediaan');
            $data['kebersihan'] = $request->post('kebersihan');
            $data['catatanKebersihan'] = $request->post('catatanKebersihan');
            $data['kelengkapan'] = $request->post('kelengkapan');
            $data['catatanKelengkapan'] = $request->post('catatanKelengkapan');
            $data['peralatan'] = $request->post('peralatan');
            $data['catatanPeralatan'] = $request->post('catatanPeralatan');
            $data['lainlain'] = $request->post('lainlain');
            $data['catatanLainlain'] = $request->post('catatanLainlain');
            $data['overall'] = $request->post('overall');
            $data['namaPemeriksa'] = $request->post('namaPemeriksa');
            $data['Pengesah'] = $request->post('Pengesah');
            $data['deporeturn'] = $request->post('deporeturn');
            dd($data);
        } else {
            return view('hall.dashboard.pemeriksaan', compact('data'));
        }
    }

    public function top_pengguna()
    {
        $data = Sport::laporan_top_pengguna();
        $arr = array();
        foreach($data as $a){
            $arr[] =  array(
                'x' => $a->fullname,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    public function top_tempahan()
    {
        $data = Sport::laporan_top_tempahan();
        $arr = array();
        foreach($data as $a){
            $nama = $a->eft_type_desc .' , '. $a->lc_description;
            $arr[] =  array(
                'x' => $nama,
                'y' => (int) $a->total
            );
        }
        
        echo json_encode($arr);
    }

    // public function my(){
    //     $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
    //     return view('dashboard.my', compact('data'));
    // }

    // public function report_keseluruhan(){
    //     $data['laporan_user'] = Sport::laporan_keseluruhan();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_dalaman(){
    //     $data['laporan_user'] = Sport::laporan_dalaman();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_bayaran(){
    //     $data['laporan_user'] = Sport::laporan_bayaran();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_kelulusan(){
    //     $data['laporan_user'] = Sport::laporan_kelulusan();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_harian(){
    //     $data['laporan_user'] = Sport::laporan_harian();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_tindih(){
    //     $data['laporan_user'] = Sport::duplicate_all();
    //     return view('dashboard.report.my', compact('data'));
    // }

    // public function report_my(){
    //     $data['laporan_user'] = Sport::laporan_user(Session::get('user')['id']);
    //     return view('dashboard.report.my', compact('data'));
    // }
    public function removefile($id){
        $id = Crypt::decrypt($id);

        $deleteFile = [
            'deleted_at'    => date('Y-m-d H:i:s')
        ];
        $query = All::GetUpdateSpec('bh_attachment', 'fk_main_booking', $id, $deleteFile);

        return redirect()->back();
    }
}

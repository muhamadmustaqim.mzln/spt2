<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class DiscountController extends Controller
{
    public function show() {
        $data['location'] = All::Show('lkp_discount_type', 'id', 'ASC');
        return view('hall.discount.lists', compact('data'));
    }

    public function form(){
        return view('hall.discount.form');
    }

    public function add(Request $request){
        $data = array(
            'ldt_user_cat'      => request()->kategoripengguna,
            'ldt_discount_rate' => request()->kadardiskaun,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('lkp_discount_type', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/location'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('lkp_discount_type', 'id', $id);
        return view('hall.discount.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $data = array(
            'ldt_user_cat'      => request()->kategoripengguna,
            'ldt_discount_rate' => request()->kadardiskaun,
            'ldt_indicator'     => $status,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_discount_type', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/discount'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/discount'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_discount_type', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/location'));
        }
    }
}

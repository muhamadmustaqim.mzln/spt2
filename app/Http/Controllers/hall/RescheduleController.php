<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Hall;
use Carbon\Carbon;
use DateTime;
use App\Helpers\Helper as HP;
class RescheduleController extends Controller
{
    public function index(Request $request) {
		$data['post'] = false;
        $data['id'] = "";
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['main']->id);
            $data['numberOfDays'] = ($start_date = DateTime::createFromFormat('Y-m-d H:i:s', $data['bh_booking'][0]->bb_start_date))->diff($end_date = DateTime::createFromFormat('Y-m-d H:i:s', $data['bh_booking'][0]->bb_end_date))->days + 1;
        }
		// dd($data);
		// if (Session::get('user')['id'] == "50554")
		// {
		return view('hall.reschedule.index', compact('data'));
		// }
        
    }

    public function penjadualanSemula($booking) {
        $data['booking'] = Crypt::decrypt($booking);
        // dd($data['booking']);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        if($data['main']->fk_lkp_status == 1){
            $updateTempahan = [
                'fk_lkp_status'     => 2
            ];
            All::GetUpdate('main_booking', $data['main']->id, $updateTempahan);
            $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        }
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['booking']);
        $data['temp'] = [];
        foreach ($data['bh_booking'] as $s) {
            $data['booking_detail'][] = All::GetRow('bh_booking_detail', 'fk_bh_booking', $s->id);
            $data['temp'][] = $s->fk_bh_hall;
        }
        $data['kelengkapan'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $data['bh_booking'][0]->id);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Hall::priceSlot($data['booking']);
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['depositrate'] = (All::Show('lkp_deposit_rate', 'id', 'ASC')->whereIn('id', $data['temp']))->values();
        $data['location'] = All::Show('bh_hall', 'id', 'ASC')->where('bh_status', 1);
        $data['fasility'] = All::GetRow('bh_hall', 'id', HP::getHallTwo($data['booking']));

        // dd($data);
		// if (Session::get('user')['id'] == "50554")
		// {
        return view('hall.reschedule.penjadualan', compact('data'));
		// }
        //return view('sport.public.maklumattempahansap', compact('data'));
    }

    public function updateDate(Request $request, $id){
        try {
            $id = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            // Handle decryption failure (e.g., redirect back with an error message)
            return redirect()->back()->with('error', 'Invalid ID');
        }
    
        $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $id);
    
        $data['bh_booking_input'] = [
            'bb_start_date' => $request->input('tarikhmula'),
            'bb_end_date' => $request->input('tarikhtamat'),
            'updated_by' => Session::get('user')['id'],
            'updated_at' => now(),
        ];
    
        // Update bh_booking
        $query1 = All::GetUpdateSpec('bh_booking', 'fk_main_booking', $id, $data['bh_booking_input']);
    
        // Update bh_booking_detail
        $query2 = true; // Assume success by default
        foreach ($data['bh_booking'] as $booking) {
            $bh_booking_id = $booking->id;
            $data['bh_booking_detail'] = [
                'bbd_start_date' => $request->input('tarikhmula'),
                'bbd_end_date' => $request->input('tarikhtamat'),
                'updated_by' => Session::get('user')['id'],
                'updated_at' => now(),
            ];
            // Update each booking detail individually
            $query2 = $query2 && All::GetUpdateSpec('bh_booking_detail', 'fk_bh_booking', $bh_booking_id, $data['bh_booking_detail']);
        }

        
        
            // Update bh_booking_equipment
            $bh_booking_id = $data['bh_booking'][0]->id;
            $bilanganHari = $request->input('bilanganHari');
            $data['bbe'] = All::GetAllRow('bh_booking_equipment', 'fk_bh_booking', $bh_booking_id)->groupBy('bbe_booking_date');

            // Step 1: Determine the unique booking dates
            $uniqueDates = $data['bbe']->keys();

            // Initialize query status
            $query3 = true;
            $equipmentDate = $request->input('tarikhmula');
            // Iterate over each unique booking date and update equipment dates
            foreach ($uniqueDates as $bookingDate) {
                // Construct the query to update each equipment type for the current booking date
                foreach ($data['bbe'][$bookingDate] as $equipment) {
                    $query3 = $query3 && All::GetUpdateSpec('bh_booking_equipment', 'id', $equipment->id, [
                        // $query3 =  [
                        'bbe_booking_date' => $equipmentDate,
                        'updated_at' => now(),
                    ]);
                    // ];
                }
                $equipmentDate = date('Y-m-d', strtotime($equipmentDate . ' +1 day'));

            }

            // dd($uniqueDates,$query3);

        if ($query1 && $query2) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/reschedule'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/reschedule'));
        }
    }

    // public function update(Request $request, $id){
    //     $id = Crypt::decrypt($id);
    //     $data = array(
    //         'lc_description'    => request()->lokasi,
    //         'lc_type'           => 1,
    //         'updated_by'        => Session::get('user')['id'],
    //         'updated_at'        => date('Y-m-d H:i:s')
    //     );
    //     $query = All::GetUpdate('lkp_location', $id, $data);

    //     if ($query) {
    //         Session::flash('flash', 'Success'); 
    //         return Redirect::to(url('/hall/admin/location'));
    //     }else{
    //         Session::flash('flash', 'Failed'); 
    //         return Redirect::to(url('/hall/admin/location'));
    //     }
    // }

    public function getDatesBetween($startDate, $endDate) {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }
}

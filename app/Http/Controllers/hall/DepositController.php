<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class DepositController extends Controller
{
    public function show() {
        $data['location'] = All::Show('lkp_deposit_rate', 'id', 'ASC');
        return view('hall.deposit.lists', compact('data'));
    }

    public function form(){
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        return view('hall.deposit.form', compact('data'));
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == "on"){
            $status1 = 1;
        }else{
            $status1 = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'ldr_rate'          => request()->deposit,
            'ldr_status'        => $status1,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('lkp_deposit_rate', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['deposit_rate'] = All::GetRow('lkp_deposit_rate', 'id', $id);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC');
        return view('hall.deposit.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == "on"){
            $status1 = 1;
        }else{
            $status1 = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'ldr_rate'          => request()->deposit,
            'ldr_status'        => $status1,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_deposit_rate', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_deposit_rate', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/deposit'));
        }
    }
}

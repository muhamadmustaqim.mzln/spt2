<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;

class LocationController extends Controller
{
    public function show() {
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('hall.location.lists', compact('data'));
    }

    public function form(){
        return view('hall.location.form');
    }

    public function add(Request $request){
        $status = request()->status;

        // if($status == "on"){
        //     $status1 = 1;
        // }else{
        //     $status1 = 0;
        // }

        $data = array(
            'lc_description'    => request()->namalokasi,
            'lc_no_of_days'     => request()->bilangansekatanhari,
            'lc_status'         => request()->status,
            'lc_contact_no'     => request()->notel,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::Insert('lkp_location', $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/location'));
        }


    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('lkp_location', 'id', $id);
        return view('hall.location.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        // if($status == "on"){
        //     $status = 1;
        // }else{
        //     $status = 0;
        // }

        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_no_of_days'     => request()->bilangansekatanhari,
            'lc_status'         => request()->status,
            'lc_contact_no'     => request()->notel,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/location'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/hall/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/hall/admin/location'));
        }
    }
}

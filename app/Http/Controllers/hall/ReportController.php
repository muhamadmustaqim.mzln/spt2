<?php

namespace App\Http\Controllers\hall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Hall;
use Carbon\Carbon;
use DateTime;
use App\Models\AuditLog;

class ReportController extends Controller
{
    public function index(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Event::laporan_acara($data['id'], $data['mula'], $data['tamat']);
            foreach ($data['slot'] as $key => $value) {
                $user = All::GetSpecRow('user_profiles', 'bud_email', $value->email)->first();
            
                if ($user) {
                    $data['slot'][$key]->bud_phone_no = $user->bud_phone_no;
                    $data['slot'][$key]->bud_reference_id = $user->bud_reference_id;
                } else {
                    $data['slot'][$key]->bud_phone_no = 'Unknown Number'; 
                    $data['slot'][$key]->bud_reference_id = 'Unknown Reference Id';
                }
            }
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
            // $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        // $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('event.report.index', compact('data'));
    }

    public function tempahan(Request $request) {
        $data['post'] = false;

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = request()->lokasi;
            $data['tarikhMula'] = request()->tarikhMula;
            $data['tarikhAkhir'] = request()->tarikhAkhir;
            $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['tarikhMula']);
            $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['tarikhAkhir']);
            $data['tarikhMula'] = $tarikhMula->format('Y-m-d');
            $data['tarikhAkhir'] = $tarikhAkhir->format('Y-m-d');
            $data['slot'] = Hall::cariantempahan($data['id'], $data['tarikhMula'], $data['tarikhAkhir'], 1);
            $slotId = $data['slot']->pluck('fk_main_booking');
            $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
            $slotId2 = $data['bh_booking']->pluck('id');
            $data['bh_booking'] = array_values($data['bh_booking']->toArray());
            $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
            $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());
        }

        return view('hall.report.tempahan', compact('data'));
    }

    public function pelanggan(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Hall::carianpelanggan($data['id'], $data['mula'], $data['tamat'], 1);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        return view('hall.report.pelanggan', compact('data'));
    }

    // public function hasil(Request $request) {
    //     $data['post'] = false;
    //     $data['id'] = "";
    //     $data['mode'] = "";
    //     $data['tarikh'] = "";
    //     $data['start'] = "";
    //     $data['end'] = "";
    //     $dates = explode(" / ", $data['tarikh']);

    //     if(request()->isMethod('post')){
    //         $data['post'] = true;
    //         $data['id'] = $request->post('lokasi');
    //         $data['bayaran'] = $request->post('bayaran');
    //         $data['mula'] = $request->post('mula');
    //         $data['tamat'] = $request->post('tamat');
    //         $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['mula']);
    //         $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['tamat']);
    //         $data['mula'] = $tarikhMula->format('Y-m-d');
    //         $data['tamat'] = $tarikhAkhir->format('Y-m-d');
    //         $data['slot'] = Hall::kutipanhasil($data['id'], $data['bayaran'], $data['mula'], $data['tamat']);
    //         $slotId = $data['slot']->pluck('fk_main_booking');
    //         $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
    //         $slotId2 = $data['bh_booking']->pluck('id');
    //         $data['bh_booking'] = array_values($data['bh_booking']->toArray());
    //         $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
    //         $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());
    //         $data['payment'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
    //         $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);

    //         // $data['mode'] = $request->post('bayaran');
    //         // $data['tarikh'] = $request->post('tarikh');
    //         // $dates = explode(" / ", $data['tarikh']);
    //         // if (count($dates) === 2) {
    //         //     $startDate = Carbon::createFromFormat('Y-m-d', $dates[0])->toDateString();
    //         //     $endDate = Carbon::createFromFormat('Y-m-d', $dates[1])->toDateString();
    //         // }
    //         // $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
    //         // $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
    //         // $data['slot'] = Sport::hasil($data['id'], $data['mode'], $startDate, $endDate);
    //     // dd($data);

    //     }
    //     $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
    //     $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
    //     // dd($data['location']);

    //     return view('hall.report.hasil', compact('data'));
    // }

    public function hasil(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
    
        if ($request->isMethod('post')) {
            $data['post'] = true;
            $data['id'] = $request->input('lokasi');
            $data['mode'] = $request->input('bayaran');
            $data['start'] = $request->input('mula');
            $data['end'] = $request->input('tamat');
    
            $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['start']);
            $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['end']);
            $data['mula'] = $tarikhMula->format('Y-m-d');
            $data['tamat'] = $tarikhAkhir->format('Y-m-d');
    
            $data['slot'] = Hall::hasil($data['id'], $data['mode'], $data['mula'], $data['tamat']);
            $slotId = $data['slot']->pluck('fk_main_booking');
            $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
            $slotId2 = $data['bh_booking']->pluck('id');
            $data['bh_booking'] = array_values($data['bh_booking']->toArray());
            $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
            $data['bh_booking_equipment'] = All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray();
            $data['payment'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->first();
            $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->first();
            // dd($data);
        }
    
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 1)->where('lc_status', 1);
    
        return view('hall.report.hasil', compact('data'));
    }

    public function harian(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['mula'] = "";
        $data['bayaran'] = "";
        
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mode'] = $request->post('bayaran');
            $data['mula'] = $request->post('tarikh');
            $data['slot'] = Hall::harian($data['id'], $data['mode'], $data['mula']);
		// dd($data);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        return view('hall.report.harian', compact('data'));
    }

    public function deposit(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['jeniskutipan'] = "";
        
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = request()->lokasi;
            $data['jeniskutipan'] = request()->jeniskutipan;
            $data['jenisbayaran'] = request()->jenisbayaran;
            $data['tarikhMula'] = request()->mula;
            $data['tarikhAkhir'] = request()->tamat;
            $tarikhMula = DateTime::createFromFormat('d-m-Y', $data['tarikhMula']);
            $tarikhAkhir = DateTime::createFromFormat('d-m-Y', $data['tarikhAkhir']);
            $data['slot'] = Hall::kutipandeposit($data['id'],$tarikhMula,$tarikhAkhir,$data['jenisbayaran'],1,$data['jeniskutipan']);
        // dd($data['slot']);
        }
        // dd($data);
        return view('hall.report.deposit', compact('data'));
    }

    public function bayaran(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['event'] = "";
        $data['mula'] = "";
        // $data['tarikh'] = "";
        // $data['start'] = "";
        // $data['end'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['event'] = $request->post('acara');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Hall::kutipantertunggak($data['id'], $data['event'], $data['mula'], $data['tamat']);
            $slotId = $data['slot']->pluck('id');
            $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
            $slotId2 = $data['bh_booking']->pluck('id');
            $data['bh_booking'] = array_values($data['bh_booking']->toArray());
            $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
            $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());
            // $data['slot'] = ::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
        //     $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        // dd($data);
        }
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
        $data['event_list'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('le_status', 1);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		
        return view('hall.report.bayaran', compact('data'));
    }

    public function pemulangan(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        // $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['tarikhMula'] = request()->mula;
            $data['tarikhAkhir'] = request()->tamat;

        //     $data['mula'] = $request->post('mula');
        //     $data['tamat'] = $request->post('tamat');
        //     $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
        //     $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
		
        return view('hall.report.pemulangan', compact('data'));
    }

    public function acara(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        $dataacara = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $tarikh = explode(" / ", $request->post('tarikh'));
            $data['mula'] = $tarikh[0];
            $data['tamat'] = $tarikh[1];
            $dataacara = Event::listacara($data['mula'], $data['tamat'], $data['id'], 0, '');
        }
        $data['location'] = All::ShowAll('spa_location', 'id', 'ASC')->where('status', 1);
        $data['status'] = All::ShowAll('lkp_status', 'ls_description', 'ASC')->whereIn('id', [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]);

        return view('event.report.acara', compact('data', 'dataacara'));
    }

    public function jumlahpengunjung(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        $dataacara = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $tarikh = explode(" / ", $request->post('tarikh'));
            $data['mula'] = $tarikh[0];
            $data['tamat'] = $tarikh[1];
            $dataacara = Event::listacara($data['mula'], $data['tamat'], $data['id'], 0, '');
        }
        $data['location'] = Event::location2();
        $data['dataspalocation'] = All::ShowAll('spa_location', 'id', 'ASC');

        return view('event.report.jumlahpengunjung', compact('data', 'dataacara'));
    }
    
    public function jumlahacara(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $data['mula'] = Carbon::today()->startOfMonth()->toDateString();
        $data['tamat'] = Carbon::today()->endOfMonth()->toDateString();
        $dataacara = Event::listacara('', '', '', 0, '');
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $tarikh = explode(" / ", $request->post('tarikh'));
            $data['mula'] = $tarikh[0];
            $data['tamat'] = $tarikh[1];
            $dataacara = Event::listacara($data['mula'], $data['tamat'], $data['id'], 0, '');
        }
        $data['tarikh'] = $data['mula'] . ' / ' . $data['tamat'];
        $data['location'] = Event::location2();
        $data['dataspalocation'] = All::ShowAll('spa_location', 'id', 'ASC');
        // dd($data);

        return view('event.report.jumlahacara', compact('data', 'dataacara'));
    }

	// public function penggunaan(Request $request) {
        
    //     $data['location'] = All::ShowAuditTrail();
    //     return view('event.report.penggunaan', compact('data'));
    // }

    // public function audit(Request $request) {
    //     $data['post'] = false;
    //     $data['id'] = "";
    //     $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');

    //     if(request()->isMethod('post')){
    //         $data['post'] = true;
    //         $data['id'] = $request->post('id');
    //         $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
    //         $data['bh_booking'] = All::GetAllRow('bh_booking', 'fk_main_booking', $data['main']->id);
    //         $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
    //         $slotId2 = $data['bh_booking']->pluck('id');
    //         $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
            
    //         // $data['slot'] = Hall::cariantempahan($data['id'], $data['tarikhMula'], $data['tarikhAkhir'], 1);
    //         // $slotId = $data['slot']->pluck('fk_main_booking');
    //         // $data['bh_booking'] = All::Show('bh_booking', 'id', 'ASC')->whereIn('fk_main_booking', $slotId);
    //         // $slotId2 = $data['bh_booking']->pluck('id');
    //         // $data['bh_booking'] = array_values($data['bh_booking']->toArray());
    //         // $data['bh_booking_detail'] = array_values(All::Show('bh_booking_detail', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->toArray());
    //         // $data['bh_booking_equipment'] = (All::Show('bh_booking_equipment', 'id', 'ASC')->whereIn('fk_bh_booking', $slotId2)->groupBy('fk_bh_booking')->toArray());

    //         // dd($data);
    //     }
    //         // dd($data);

    //     return view('hall.report.audit', compact('data'));
    // }

    public function penggunaan(Request $request, $id = null) {
        $data['post'] = false;
        $data['type'] = 1;
		$data['search'] = "";
        $data['log'] = [];
        // dd($data);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['type'] = request()->type;
		    $data['search'] = $request->post('search');
            $search = $request->post('search');
            if($data['type'] == 2){
                $data['users'] = All::GetAllRowLike('users', 'fullname', $search);
            } elseif($data['type'] == 3) {
                $data['log'] = Sport::laporan_penggunaan($search);
            }
        } 
        if($id != null){
            $data['post'] = true;
            $id = Crypt::decrypt($id);
            $data['log'] = All::GetAllRow('audit_trail', 'fk_users', $id, 'id', 'DESC');
            $data['type'] = 3;
        }

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Audit Dewan - Carian Log Audit',null);
        return view('hall.report.penggunaan', compact('data'));
    }

    public function audit(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetAllRow('main_booking', 'bmb_booking_no', $data['id']);
            if(count($data['main']) > 0){
                $data['main'] = $data['main']->first();
                $data['users'] = All::GetRow('users', 'id', $data['main']->fk_users);
                $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
                $data['bh_booking'] = All::GetRow('bh_booking', 'fk_main_booking', $data['main']->id);
                // $data['et_hall_book'] = All::GetAllRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking']->id);
                // $data['et_sport_book'] = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $data['et_booking']->id);
                $data['bh_payment'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['main']->id);   // Dewan bawah Sukan
                $data['bh_payment_fpx'] = All::GetAllRow('bh_payment_fpx', 'fk_main_booking', $data['main']->id);   // Online Transaction
                $data['audit_log'] = All::GetAllRow('audit_trail', 'table_ref_id', $data['main']->id)->where('deleted_at', null);
            } else {
                $data['post'] = false;
                Session::flash('flash', 'Non-existant'); 
            }
            // dd($data);
        }
        
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Audit Trail',null);
        return view('hall.report.audit', compact('data'));
    }

}

<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\Sport;
use Carbon\Carbon;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class ReportController extends Controller
{
    public function index(Request $request) {
        $data['post'] = false;
        $data['start'] = "";
        $data['end'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['type'] = $request->post('kemudahan');
            $data['id'] = $request->post('lokasi');
            $data['mula'] = Carbon::createFromFormat('d-m-Y', $request->post('mula'))->format('Y-m-d');
            $data['tamat'] = Carbon::createFromFormat('d-m-Y', $request->post('tamat'))->format('Y-m-d');
            // $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            $data['slot'] = Sport::cariantempahan($data['id'], $data['mula'], $data['tamat'], null, $data['type']);
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
            // dd($data);
        }
        $data['main'] = 
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        $data['presint'] = Sport::locationByPresint();
        
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Tempahan',null);
        return view('sport.report.index', compact('data'));
    }

    public function pelanggan(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = Carbon::createFromFormat('d-m-Y', $request->post('mula'))->format('Y-m-d');
            $data['tamat'] = Carbon::createFromFormat('d-m-Y', $request->post('tamat'))->format('Y-m-d');
            $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);

            $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Pelanggan = ' .$request->post('lokasi'). ' Tarikh Mula = ' .$request->post('mula'). ' Tarikh Tamat = ' .$request->post('tamat'),null);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Pelanggan',null);
        return view('sport.report.pelanggan', compact('data'));
    }

    public function hasil(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
        $dates = explode(" / ", $data['tarikh']);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mode'] = $request->post('bayaran');
            $dates[0] = request()->mula;
            $dates[1] = request()->tamat;
            // $data['tarikh'] = $request->post('tarikh');
            // $dates = explode(" / ", $data['tarikh']);
            // if (count($dates) === 2) {
                $startDate = Carbon::createFromFormat('d-m-Y', $dates[0])->toDateString();
                $endDate = Carbon::createFromFormat('d-m-Y', $dates[1])->toDateString();
            // }
            $data['start'] = $startDate;
            $data['end'] = $endDate;
            $data['slot'] = Sport::hasil($data['id'], $data['mode'], $startDate, $endDate);
        }
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Kutipan Hasil',null);
        return view('sport.report.hasil', compact('data'));
    }

    public function harian(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		$data['dayOfWeek'] = "";

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mode'] = $request->post('bayaran');
            $data['tarikh'] = $request->post('tarikh');
            // $data['harian'] = Sport::hasil($data['id'], $data['mode'], $data['tarikh'],'');
            $data['harian'] = Sport::kutipanharian($data['id'], $data['tarikh'], $data['mode']);
            $data['dayOfWeek'] = Carbon::parse($data['tarikh'])->locale('ms')->isoFormat('dddd');
        }
		// $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Kutipan Harian',null);
        return view('sport.report.harian', compact('data'));
    }

    public function bayarandepo(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['mode'] = $request->post('bayaran');
            $data['jeniskutipan'] = $request->post('jeniskutipan');
            // $data['jenis'] = $request->post('jenis');
            // $data['payment'] = Sport::laporan_bayaran();
            // $data['sukan'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            // $dates = explode(" / ", request()->tarikh);
            $data['payment'] = Sport::laporandeposit($data['id'], $data['mula'], $data['tamat'], $data['mode'], $data['jeniskutipan']);
            // $data['payment'] = Sport::kutipandeposit($data['id'], $data['mula'], $data['tamat'], $data['mode'], '', $data['jeniskutipan']);
        }
		// $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Bayaran Deposit',null);
        return view('sport.report.bayarandepo', compact('data'));
    }

    public function aging(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mode'] = $request->post('bayaran');
            // $data['acara'] = $request->post('acara');
            $data['mula'] = Carbon::createFromFormat('d-m-Y', $request->post('mula'))->format('Y-m-d');
            $data['tamat'] = Carbon::createFromFormat('d-m-Y', $request->post('tamat'))->format('Y-m-d');
            // $data['slot'] = Sport::hasil($data['id'], $data['mula'], $data['tamat']);
            $data['slot'] = Sport::carianaging($data['id'], $data['mula'], $data['tamat'], '', $data['mode']);
            // $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
		// $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Bayaran Tertunggak',null);
        return view('sport.report.aging', compact('data'));
    }

    public function refund(Request $request) {
        $data['post'] = false;
		$data['id'] = "";
        $data['mode'] = "";
        $data['tarikh'] = "";
        $data['start'] = "";
        $data['end'] = "";
		
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['mula'] = $request->post('mula');
            $data['tamat'] = $request->post('tamat');
            $data['slot'] = Sport::laporan_sukan($data['id'], $data['mula'], $data['tamat']);
            $data['umum'] = Sport::laporan_umum($data['id'], $data['mula'], $data['tamat']);
        }
		$data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1);;
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        return view('sport.report.refund', compact('data'));
    }

    public function usage(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['usage_type'] = "";
        $data['search_type'] = "";
        $data['tarikh'] = "";
        $data['year'] = "";
        $data['start'] = "";
        $data['end'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['usage_type'] = $request->post('usage_type');     // 1 = Fasiliti, 2 = Peralatan
            $data['search_type'] = $request->post('search_type');   // 1 = Tarikh, 2 = Tahun
            if($data['search_type'] == 1){
                $startDate = Carbon::createFromFormat('d-m-Y', $request->input('mula'))->toDateString();
                $endDate = Carbon::createFromFormat('d-m-Y', $request->input('tamat'))->toDateString();
                $data['start'] = $startDate;
                $data['end'] = $endDate;
            } else {
                $data['year'] = $request->post('year');
            }
            if($data['usage_type'] == 1){           // Dewan / Fasiliti
                $data['list_facility'] = Sport::list_booking_facility($data['id'],$data['usage_type']);
            } else if($data['usage_type'] == 2){    // Peralatan
                $data['list_equipment'] = Sport::list_equipment($data['id'],$data['usage_type']);
            }
            if($data['search_type'] == 2 && $data['usage_type'] == 2){  // Due to large amount of data
                $data['test'] = [];
                $months = array(1 => 'Januari', 2 => 'Februari', 3 => 'March', 4 => 'April',
                                5 => 'Mei', 6 => 'Jun', 7 => 'Julai', 8 => 'Ogos', 9 => 'September', 10 => 'Oktober',
                                11 => 'November', 12 => 'Disember');
                $ebf = HP::laporan_penggunaan_peralatan_tahun('', '', '', '', 1);
                foreach ($data['list_equipment'] as $key => $s) {
                    foreach ($months as $num => $name) {
                        $data['data_facility_year'][] = HP::laporan_penggunaan_peralatan_tahun($num, $data['year'], $key, $ebf, 2);
                    }
                }
            }

            // $data['usage'] = Sport::usage1($data['id'], $data['year'], $data['usage_type'], $data['search_type'], $startDate, $endDate);
        }
        // $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        // dd($data);

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Kutipan Hasil',null);
        return view('sport.report.usage', compact('data'));
    }

	public function penggunaan(Request $request, $id = null) {
        $data['post'] = false;
        $data['type'] = 1;
		$data['search'] = "";
        $data['log'] = [];
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['type'] = request()->type;
		    $data['search'] = $request->post('search');
            $search = $request->post('search');
            if($data['type'] == 2){
                $data['users'] = All::GetAllRowLike('users', 'fullname', $search);
            } elseif($data['type'] == 3) {
                $data['log'] = Sport::laporan_penggunaan($search);
            }
        } 
        if($id != null){
            $data['post'] = true;
            $id = Crypt::decrypt($id);
            $data['log'] = All::GetAllRow('audit_trail', 'fk_users', $id, 'id', 'DESC');
            $data['type'] = 3;
        }

        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Log Audit',null);
        return view('sport.report.penggunaan', compact('data'));
    }

    public function audit(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            $data['main'] = All::GetAllRow('main_booking', 'bmb_booking_no', $data['id']);
            if(count($data['main']) > 0){
                $data['main'] = $data['main']->first();
                $data['users'] = All::GetRow('users', 'id', $data['main']->fk_users);
                $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
                $data['et_booking'] = All::GetRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
                $data['et_hall_book'] = All::GetAllRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking']->id);
                $data['et_sport_book'] = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $data['et_booking']->id);
                $data['bh_payment'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['main']->id);   // Dewan bawah Sukan
                $data['et_payment_fpx'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['main']->id);   // Online Transaction
                $data['audit_log'] = All::GetAllRow('audit_trail', 'table_ref_id', $data['main']->id)->where('deleted_at', null);
            } else {
                $data['post'] = false;
                Session::flash('flash', 'Non-existant'); 
            }
            // dd($data);
        }
        
        $audit = AuditLog::log(Crypt::encrypt(173), Crypt::encrypt($data['post']), 'Laporan Sukan - Carian Maklumat Audit Trail',null);
        return view('sport.report.audit', compact('data'));
    }
}

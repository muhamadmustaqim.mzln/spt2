<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class TypeFacilityController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_facility', 'updated_at', 'DESC');
        
        return view('sport.facilitytype.lists', compact('data'));
    }

    public function form(){
        return view('sport.facilitytype.form');
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'ef_desc'           => request()->nama,
            'ef_code'           => request()->kod,
            'ef_type'           => request()->jenis,
            'ef_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_facility', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(153), Crypt::encrypt($query), 'Sukan - Tambah Jenis Kemudahan = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('et_facility', 'id', $id);
        return view('sport.facilitytype.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'ef_desc'           => request()->nama,
            'ef_code'           => request()->kod,
            'ef_type'           => request()->jenis,
            'ef_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(154), Crypt::encrypt($id), 'Sukan - Kemas kini Jenis Kemudahan = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(155), Crypt::encrypt($id), 'Sukan - Padam Jenis Kemudahan = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }
    }
}

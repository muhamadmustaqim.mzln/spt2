<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use App\Models\All;
use App\Models\Sport;
use App\Models\AuditLog;
use Carbon\Carbon;
use App\Helpers\Helper;
use Illuminate\Database\QueryException;

    // if(Session::get('user.id') == '50554'){
    //     // dd($data);
    // }
class SportController extends Controller
{
    public function lists(Request $request) {
        $data['location'] = All::Show('lkp_location', 'lc_description', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        $locationArray = $data['location']->all();

        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });

        // Convert the array back to a collection
        $data['location'] = collect($locationArray);
        $data['fasility'] = Sport::fasilityPublic();
        $collection = collect($data['fasility']);
        $data['fasility'] = $collection->map(function ($item) {
            $item->encrypt = Crypt::encrypt($item->id);
            return $item;
        });
        $data['sintetik_openBook'] = All::GetSpecRow('et_facility_sintetik_open_book', 'efsob_open_date', date('Y-m-d'));
        // dd($data);

        $data['pengumuman'] = all::Show('bh_announcement', 'created_at', 'DESC')->where('ba_type', 2)->where('ba_status', 1);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = date('Y-m-d', strtotime($request->post('tarikh')));
            $data['fasility'] = Sport::facility($data['id']);
            $collection = collect($data['fasility']);
            $data['fasility'] = $collection->map(function ($item) {
                $item->encrypt = Crypt::encrypt($item->id);
                return $item;
            });
            // $data['slot'] = Sport::slot($data['id'], $data['date']);
            return redirect(url('sport/details', ['id' => Crypt::encrypt($data['id']), 'date' => $data['date']]));
        }
        // dd(Session::get('user'), count(array_intersect(Session::get('user.roles'), [1, 23])) > 0);
        // dd(Session::get('user'), in_array(Session::get('user.roles'), [1, 23]));

        $data['location'] = $data['location']->sortBy(function($location) {
            // Extract the numeric part of the lc_description field
            preg_match('/\d+/', $location->lc_description, $matches);
            // If a numeric part is found, return it for sorting, else return 0
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        return view('sport.public.lists', compact('data'));
    }

    public function details(Request $request, $id, $date = null) {
        $id = Crypt::decrypt($id);
        $data['id'] = $id;

        if ($date == null) {
            $data['date'] = date("Y-m-d");
        } else {
            $data['date'] = date('Y-m-d', strtotime($date));
        }
        if (request()->isMethod('post')){
            $data['date'] = date('Y-m-d', strtotime(request()->tarikh));
            $id = request()->fasility;
            $data['id'] = $id;
        }

        try {
            $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
            // Convert the collection to an array
            $locationArray = $data['location']->all();
            // Sort the array by the length of the lc_description
            usort($locationArray, function($a, $b) {
                return strlen($a->lc_description) - strlen($b->lc_description);
            });
            // Convert the array back to a collection
            $data['location'] = collect($locationArray);
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);
            $data['others'] = All::Show('et_facility_type', 'id', 'ASC')->where('fk_et_facility', $data['fasility']->fk_et_facility)->where('id', '!=' , $id);
            $data['price'] = Sport::price($id, $data['date']);
            $data['slot'] = Sport::slot($id, $data['date']);
            $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id)->WhereNull('deleted_at')->where('efd_status', 1);
        } catch (\Throwable $th) {
            Session::flash('flash', 'An error occured.'); 
            return redirect()->back();
        }
        $data['facility2'] = Sport::Onepublic($data['fasility']->fk_lkp_location);
        $data['et_fac_type_img'] = array_values(All::GetAllRow('et_facility_type_image', 'fk_et_facility_type', $id)->where('deleted_at', NULL)->toArray());

        $data['location'] = $data['location']->sortBy(function($location) {
            // Extract the numeric part of the lc_description field
            preg_match('/\d+/', $location->lc_description, $matches);
            // If a numeric part is found, return it for sorting, else return 0
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        
        $audit = AuditLog::log(Crypt::encrypt(175), Crypt::encrypt($id), 'Tempahan Sukan = '. $id,null);
        return view('sport.public.details', compact('data'));
    }

    public function slot($id, $date){
        try {
            $id = Crypt::decrypt($id);
            $date = Crypt::decrypt($date);
            $data['id'] = $id;
            $data['date'] = $date;
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);
            $data['policy'] = All::GetRowSport('et_facility_type_slot_restriction', 'fk_et_facility_type', $id, ['id', 'fk_et_facility_type', 'max_slot', 'max_court', 'max_per_court', 'max_per_day', 'expiry_by_month', 'type']);
            $data['maxAllowed'] = All::GetRow('et_facility_type_slot_restriction', 'fk_et_facility_type', $id)->max_slot;
            $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id);
            $data['slot'] = Sport::slot($id, $date);
            $data['bookingMonthly'] = Sport::bookedPolicy_ThreeOrMore(Session::get('user.id'), $data['date']);
            $data['bookingMonthly'] = 1; // Allow more than 3 per month
            // dd($data['policy'], $id);
            foreach ($data['policy'] as $property => $value) {
                if ($property != 'type' && $value === null) {
                    AuditLog::log(Crypt::encrypt(179), Crypt::encrypt($id), 'Polisi perlu dikemas kini: '. Helper::getSportFacility($id), null);

                    Session::flash('flash', 'BelumSedia');
                    return redirect()->back();
                } 
            }
        } catch (\Throwable $th) {
            // dd($th->getMessage());
            $errorText = $th->getMessage();
            // AuditLog::log(Crypt::encrypt(179), Crypt::encrypt($id), 'Polisi perlu dikemas kini: '. $errorText, null);

            Session::flash('flash', 'An error occured.'); 
            return redirect()->back();
        }
        if($data['policy'] == null){
            AuditLog::log(Crypt::encrypt(179), Crypt::encrypt($id), 'Polisi perlu dikemas kini: '. Helper::getSportFacility($id), null);
            Session::flash('flash', 'BelumSedia');
            return redirect()->back();
        }

        $audit = AuditLog::log(Crypt::encrypt(174), Crypt::encrypt($id), 'Paparan Slot Sukan = '.$id,1);
        if(Session::get('flash')){
            Session::flash('flash', 'Tempahan gagal. Lokasi, tarikh dan slot masa dipilih telah ditempah. Sila pilih slot masa yang lain.');
        }
        return view('sport.public.slot', compact('data'));
    }

    public function slotbook(Request $request, $id, $date){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $slot = $request->post('slot');
        $location = All::GetRow('et_facility_type', 'id', $id);

        foreach($slot as $s){
            $row = explode(',', $s);
            $validation = All::GetAllRow('et_payment_timeout', 'fk_et_facility_type', $id)->where('fk_et_facility_detail', (int)$row[0])->where('fk_et_slot_time', $row[1])->where('fk_esb_booking_date', $row[2])->where('book_time_end', '>=', date('Y-m-d H:i:s'));
            if(count($validation) > 0){
                Session::flash('flash', 'Tempahan gagal. Lokasi, tarikh dan slot masa dipilih telah ditempah. Sila pilih slot masa yang lain.');
                return redirect()->back();
            }
        }

        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        $data = array(
            'fk_users'                   => Session::get('user')['id'],
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('main_booking', $data);

        $i = 0;
        $varCompLocation = 0;
        $esb_booking_date_temp = '';
        foreach($slot as $s){
            $row = explode(',', $s);
            $esb_booking_date_temp = $row[2];
            if($row[0] != $varCompLocation){
                $varCompLocation = $row[0];
                $data_book[$i] = array(
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $id,
                    'fk_et_facility_detail'      => $varCompLocation,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => $row[2],
                    'ebf_end_date'               => $row[2],
                    'ebf_no_of_day'              => 1,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);

                $data_sport[$i] = array(
                    'fk_et_booking_facility'     => $etBookFacility,
                    'esb_booking_date'           => $row[2],
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
            }
            $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
            $total_gst = $row[4] * $gst->lgr_rate;
            $total = $row[4] + $total_gst;

            if(Session::get('user.staffppj') == true){
                $staff_discount = 1 - ((All::GetRow('lkp_discount_type', 'id', 6)->ldt_discount_rate) / 100);
                $total = $total * $staff_discount;
            }
            
            $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);
            
            $data_time[$i] = array(
                'fk_et_sport_book'           => $etSportBook,
                'fk_et_slot_price'           => $slotPrice->id,
                'est_price'                  => $row[4],
                'est_discount_type_rm'       => 0.00,
                'est_discount_rm'            => 0.00,
                'est_total'                  => $row[4],
                'est_gst_code'               => $row[5],
                'est_gst_rm'                 => $total_gst,
                'est_subtotal'               => $total,
                'created_at'                 => date('Y-m-d H:i:s'),
                'updated_at'                 => date('Y-m-d H:i:s')
            );
            $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 

            $auditGelanggang = AuditLog::log(Crypt::encrypt(99), Crypt::encrypt($query), 'Tempah sukan bagi (fk_et_facility_type) '.$id.'| Lokasi : '.Helper::location($location->fk_lkp_location).'| Gelanggang (fk_et_facility_detail) : '.$row[0], 1);
            $auditSlotMasa = AuditLog::log(Crypt::encrypt(99), Crypt::encrypt($query), 'Tambah Slot Masa Kegunaan : '.$row[0], 1);
            $i++;
        }
        $sportTimeout = [
            'fk_main_booking'       => $query,
            'fk_et_facility_type'   => $id,
            'fk_et_facility_detail' => $varCompLocation,
            'fk_et_slot_time'       => $row[1],
            'fk_esb_booking_date'   => $row[2],
            'book_time_end'         => date('Y-m-d H:i:s', strtotime('+5 minutes')),
            // 'book_time_end' => date('Y-m-d H:i:s', strtotime('+30 seconds')),
            'created_at'            => date('Y-m-d H:i:s'),
        ];
        $sportTimeoutId = All::InsertGetID('et_payment_timeout', $sportTimeout); 

        AuditLog::log(Crypt::encrypt(96), Crypt::encrypt($query), 'Data Tempahan Disimpan dan Dikira - Main Booking dikemas kini (Pengiraan)', 1);

        if ($etSportTime) {
            return Redirect::to(url('sport/tempahan', [Crypt::encrypt($query)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function tempahan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['slot'] = Sport::priceSlot($data['booking']);

        if (empty($data['slot'])) {
            return Redirect::to(url('/sport'));
        }
        // dd($data);

        return view('sport.public.tempahan', compact('data'));
    }

    public function tempahan_delete($booking, $id, $esbid){
        $id = Crypt::decrypt($id);
        $esbid = Crypt::decrypt($esbid);

        $query = All::GetDelete('et_sport_time', $id);
        // $data['dataTempahan'] = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $esbid, 'id', 'ASC')->whereNull('deleted_at')->first();
        // $data['cidd'] = All::GetRow('et_slot_price', 'id', $data['dataTempahan']->fk_et_slot_price)->id;
        // if (count($dataTempahan) > 1) {
        //     $booking = Crypt::decrypt($booking);
        //     $main_booking = All::GetRow('main_booking', 'id', $booking);
        //     $booking_date = $main_booking->bmb_booking_date;
        //     $et_booking_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $main_booking->id)->fk_et_facility_type;
        //     $booking_date = date('Y-m-d', strtotime($booking_date));
        //     return Redirect::to(url('/sport/slot', ['id' => Crypt::encrypt($et_booking_facility), 'date' => Crypt::encrypt($booking_date)]));
        //     // return Redirect::to(url('/sport/slot', Crypt::encrypt($booking), Crypt::encrypt($booking_date)));
        // }

        if ($query) {
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport/tempahan', $booking));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/tempahan', $booking));
        }
    }

    public function rumusan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['tax'] = All::Show('lkp_tax', 'lt_name', 'ASC');

        AuditLog::log(Crypt::encrypt(96), Crypt::encrypt($data['booking']), 'Paparan Rumusan Tempahan', 1);

        return view('sport.public.rumusan', compact('data'));
    }

    public function bayaran(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $main = All::GetRow('main_booking', 'id', $data['booking']);
        $data['oderNo'] = $main->bmb_booking_no;//.$no;
        $data['total'] = $request->post('total');
        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'fk_main_booking', $data['booking']);
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);
        $data['et_facility_detail'] = All::GetRow('et_facility_detail', 'id', $data['et_booking_facility']->fk_et_facility_detail);
        $feeCode = $data['et_facility_detail']->efd_fee_code;
        $bmbbookingno = $main->bmb_booking_no;
        $presint = All::GetRow('lkp_location', 'id', $main->fk_lkp_location)->lc_description;
        $no = Sport::generatetransid();
        $quono = Sport::generatequono();
        
        $transHistory = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $main->id);
        if(count($transHistory) == 0){
            $data['newOrderNo'] = $data['oderNo'] . $no;
            
            $data_bhquotation = array(
                'fk_main_booking'           => $data['booking'],
                'fk_users'                  => $main->fk_users,
                'fk_lkp_discount_type'      => 5,
                'bq_quotation_no'           => 'PPj/'.$presint.'/'.$bmbbookingno.'/'.$quono,
                'bq_quotation_date'         => date('Y-m-d'),
                'bq_quotation_status'       => 1,
                'bq_total_amount'           => $data['total'],
                'bq_deposit'                => 0.00,
                'bq_payment_status'         => 0,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $id_bhquotation = All::InsertGetID('bh_quotation', $data_bhquotation);
    
            $data_fpx = array(
                'fk_main_booking'           => $data['booking'],
                'fk_bh_quotation'           => $id_bhquotation,
                'fk_lkp_payment_type'       => 2,
                'fpx_trans_id'              => $data['newOrderNo'],
                'total_amount'              => $data['total'],
                'deposit_amount'            => 0.00,
                'amount_paid'               => $data['total'],
                'fpx_status'                => 0,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $query = All::InsertGetID('et_payment_fpx', $data_fpx);
        } else {
            $transHistory = $transHistory->last();
            if($transHistory->fpx_status != 0) {
                $currentCount = (int)substr($transHistory->fpx_trans_id, 14, 18);
                $count = str_pad($currentCount + 1, 4, '0', STR_PAD_LEFT);
                $data['newOrderNo'] = $data['oderNo'] . $count;

                $data_bhquotation = array(
                    'fk_main_booking'           => $data['booking'],
                    'fk_users'                  => $main->fk_users,
                    'fk_lkp_discount_type'      => 5,
                    'bq_quotation_no'           => 'PPj/'.$presint.'/'.$bmbbookingno.'/'.$quono,
                    'bq_quotation_date'         => date('Y-m-d'),
                    'bq_quotation_status'       => 1,
                    'bq_total_amount'           => $data['total'],
                    'bq_deposit'                => 0.00,
                    'bq_payment_status'         => 0,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $id_bhquotation = All::InsertGetID('bh_quotation', $data_bhquotation);
        
                $data_fpx = array(
                    'fk_main_booking'           => $data['booking'],
                    'fk_bh_quotation'           => $id_bhquotation,
                    'fk_lkp_payment_type'       => 2,
                    'fpx_trans_id'              => $data['newOrderNo'],
                    'total_amount'              => $data['total'],
                    'deposit_amount'            => 0.00,
                    'amount_paid'               => $data['total'],
                    'fpx_status'                => 0,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user')['id'],
                );
                $query = All::InsertGetID('et_payment_fpx', $data_fpx);
            } else {
                $data['newOrderNo'] = $transHistory->fpx_trans_id;
            }
        }

        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';
        $data1 = array("payload" => array(
            "subsysId"      => "SPS",
            "password"      => "e_TemPahan@@2024!",
            "orderNo"       => $data['newOrderNo'],
            "description"   => $data['newOrderNo'],
            "txnTime"       => date('Y-m-d H:i:s'),
            'adhoc'         => 0,
            "amount"        => $data['total'],
            // "feeCode"       => $feeCode,
        ));
        $postdata = json_encode($data1);

        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        curl_close($ch);

        if (strpos($result, 'Illuminate\Database\QueryException') !== false) {
            // Log the error
            \Log::error('Illuminate\Database\QueryException found in response.');
            \Log::info($result);
            
            Session::flash('flash', 'Sistem sedang menghadapi masalah. Sila tunggu dan cuba sebentar lagi.'); 
            // Redirect back with error message
            return redirect()->back()->with('error', 'An error occurred while processing your request. Please try again later.');
        }

        AuditLog::log(Crypt::encrypt(97), Crypt::encrypt($data['booking']), 'Status Main Booking Diupdate kepada : 2 ,Teruskan Ke Pembayaran', 1);

        return view('sport.public.bayaran', compact('data', 'response'));
    }

    public function cancel(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('id');
            if (substr($data['id'], 0, 3) === 'SPS') {
                $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            } else {
                $data['main'] = null;
            }
        }

        return view('sport.cancel.index', compact('data'));
    }

    public function submitcancelget(Request $request, $id) {
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_lkp_status'           => 10,
            'bmb_reason_cancel'       => request()->reason,
            'updated_by'              => Session::get('user')['id'],
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('main_booking', $id, $data);

        // $data2 = array(
        //     'updated_by'              => Session::get('user')['id'],
        //     'deleted_at'              => date('Y-m-d H:i:s'),
        //     'updated_at'              => date('Y-m-d H:i:s')
        // );
        // $query = All::GetUpdateSpec('et_confirm_booking', 'fk_main_booking', $id, $data2);

        $query = All::GetDeleteWhere('et_payment_timeout', 'fk_main_booking', $id);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function submitcancel(Request $request, $id) {
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_lkp_status'           => 10,
            'bmb_reason_cancel'       => request()->reason,
            'updated_by'              => Session::get('user')['id'],
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('main_booking', $id, $data);

        $data2 = array(
            'updated_by'              => Session::get('user')['id'],
            'deleted_at'              => date('Y-m-d H:i:s'),
            'updated_at'              => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdateSpec('et_confirm_booking', 'fk_main_booking', $id, $data2);

        $query = All::GetDeleteWhere('et_payment_timeout', 'fk_main_booking', $id);

        if ($query) {
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/cancel'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/cancel'));
        }
    }

    public function duplicate(Request $request) {
        $data['post'] = false;
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['slot'] = Sport::duplicate($data['id'], $data['date']);
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        
        return view('sport.duplicate.index', compact('data'));
    }

    public function available(Request $request) {
        $data['post'] = false;
        $data['postdewan'] = false;
        $data['tab'] = 1;
        if(request()->isMethod('post')){
            $data['id'] = $request->post('lokasi');
            $data['date'] = $request->post('tarikh');
            $data['tab'] = $request->post('tab');
            // $data['slot'] = [];
            // $data['facility'] = [];
            // $data['slotdewan'] = [];
            // $data['facilitydewan'] = [];
            if($data['tab'] == 1) {
                $data['post'] = true;
                $data['slot'] = Sport::slot($data['id'], $data['date']);
                $data['facility'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $data['id'])->where('efd_status', 1);
            } else {
                $data['presint'] = $request->post('presint');
                $data['lokasidewan'] = $request->post('lokasi');
                $data['postdewan'] = true;
                $data['slotdewan'] = Sport::slotdewan_bertindih($data['id'], $data['date']);
                $data['facilitydewan'] = All::Show('et_facility_type', 'id', 'ASC')->where('id', $data['id']);
            }
        }

        $data['location'] = Sport::getLocation(session('user.id'));
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        
        return view('sport.available.index', compact('data'));
    }

    public function ajaxPaymentStatus(Request $request)
    {
        $mbid = $request->post('mbid');
        $main = All::GetRow('main_booking', 'id', $mbid);
        if($main->fk_lkp_status == 10) {
            return response()->json(['status' => 'fail', 'message' => 'Booking has been cancelled automatically after 60 minute']);
        } else {
            return response()->json(['status' => 'success', 'message' => 'Proceed payment.']);
        }
    }
    
    public function ajax(Request $request)
    {
        $location_id = $request->post('location_id');
        $kaunter = $request->post('kaunter');
        $location = Sport::Onepublic($location_id);

        if($kaunter == 1){
            if(count($location)>0)
            {
                $location_box = '';
                $location_box .= '<option value="">Sila Pilih</option>';
                foreach ($location as $k){
                    $location_box .= '<option value="'.$k->id. '">' .$k->eft_type_desc.'</option>';
                }
                echo json_encode($location_box);
            }else{
                $location_box = '';
                $location_box .= '<option value="">Sila Pilih</option>';
                $location_box .= '<option value="0">Tiada</option>';
                echo json_encode($location_box);
            }
        } else if(isset($location[0])) {
            if($location[0]->id == 65){ // Temporary method (need flexible method in case of facility addition)
                $data['sintetik_openBook'] = All::GetSpecRow('et_facility_sintetik_open_book', 'efsob_open_date', date('Y-m-d'));
                $data['sintetik_openBook_nearest'] = All::GetNearestRow('et_facility_sintetik_open_book', 'efsob_open_date', date('Y-m-d'));
                if ($data['sintetik_openBook']->isEmpty()) {
                    if ($data['sintetik_openBook_nearest']->isEmpty()){
                        Session::flash('flash', 'Padang Sintetik belum dibuka untuk tempahan.'); 
                        return response()->json(['flash_message' => session('flash')]);
                    } else {
                        $nextDate = date('d-m-Y', strtotime($data['sintetik_openBook_nearest']->first()->efsob_open_date));
                        Session::flash('flash', 'Padang Sintetik belum dibuka untuk tempahan. Tempahan seterusnya dibuka pada <b>'.$nextDate.'</b>. Terima kasih.'); 
                        return response()->json(['flash_message' => session('flash')]);
                    }
                }
            } else {
                if(count($location)>0)
                {
                    $location_box = '';
                    $location_box .= '<option value="">Sila Pilih</option>';
                    foreach ($location as $k){
                        $location_box .= '<option value="'.$k->id. '">' .$k->eft_type_desc.'</option>';
                    }
                    echo json_encode($location_box);
                }else{
                    $location_box = '';
                    $location_box .= '<option value="">Sila Pilih</option>';
                    $location_box .= '<option value="0">Tiada</option>';
                    echo json_encode($location_box);
                }
            }
        } else {
            $location_box = '';
            $location_box .= '<option value="" selected disabled>Sila Pilih Lokasi</option>';
            // $location_box .= '<option value="0">Tiada</option>';
            echo json_encode($location_box);
        }
    }

    public function ajaxpublic(Request $request)
    {
        $location_id = $request->post('location_id');
        $location = Sport::Onepublic($location_id);
        if($location[0]->id == 65){ // Temporary method (need flexible method in case of facility addition)
            $data['sintetik_openBook'] = All::GetSpecRow('et_facility_sintetik_open_book', 'efsob_open_date', date('Y-m-d'));
            $data['sintetik_openBook_nearest'] = All::GetNearestRow('et_facility_sintetik_open_book', 'efsob_open_date', date('Y-m-d'));
            if ($data['sintetik_openBook']->isEmpty()) {
                if ($data['sintetik_openBook_nearest']->isEmpty()){
                    Session::flash('flash', 'Padang Sintetik belum dibuka untuk tempahan.'); 
                    return response()->json(['flash_message' => session('flash')]);
                } else {
                    $nextDate = date('d-m-Y', strtotime($data['sintetik_openBook_nearest']->first()->efsob_open_date));
                    Session::flash('flash', 'Padang Sintetik belum dibuka untuk tempahan. Tempahan seterusnya dibuka pada <b>'.$nextDate.'</b>. Terima kasih.'); 
                    return response()->json(['flash_message' => session('flash')]);
                }
            }
        } else {
            if(count($location)>0)
            {
                $location_box = '';
                $location_box .= '<option value="">Sila Pilih</option>';
                foreach ($location as $k){
                    $location_box .= '<option value="'.$k->id. '">' .$k->eft_type_desc.'</option>';
                }
                echo json_encode($location_box);
            }else{
                $location_box = '';
                $location_box .= '<option value="">Sila Pilih</option>';
                $location_box .= '<option value="0">Tiada</iption>';
                echo json_encode($location_box);
            }
        }
    }

    public function ajaxhall(Request $request)
    {
        $location_id = $request->post('location_id');
        $location = Sport::One1($location_id);
        if(count($location)>0)
        {
            $location_box = '';
            $location_box .= '<option value="">Sila Pilih</iption>';
            foreach ($location as $k){
                $location_box .= '<option value="'.$k->id. '">' .$k->eft_type_desc. '</option>';
            }
            echo json_encode($location_box);
        }else{
            $location_box = '';
            $location_box .= '<option value="" disabled>Sila Pilih</iption>';
            $location_box .= '<option value="0" selected>Tiada</iption>';
            echo json_encode($location_box);
        }
    }

    public function maklumatbayaran($booking, $type = null)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['type'] = null;
        if($type != null){
            $data['type'] = Crypt::decrypt($type);
        }
        $data['rating'] = All::GetRow('et_rating', 'fk_main_booking', $data['booking']);
        if($data['rating'] == null){
            $data['rating'] = 0;
        } else {
            $data['rating'] = 1;
        }
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['et_sport_book'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        
        $data['slot'] = Sport::priceSlot($data['booking']);

        $data['type'] = 1; // 1 = sport, 2 = hall
        // Dewan if slot empty
        if(count($data['et_sport_book']) == 0) {
            $data['type'] = 2;
            $data['slot'] = Sport::priceSlot2($data['booking']);
            $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id, 'id', 'DESC');
            $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
            $data['et_equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        }
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['kaunterdetail'] = [];
        foreach ($data['kaunter'] as $kaunter) {
            $details = All::GetAllRow('et_payment_detail', 'fk_bh_payment', $kaunter->id, 'id', 'DESC');
            $data['kaunterdetail'][$kaunter->id] = $details;
        }
        $data['penuh'] = new \stdClass();
        $data['penuh']->bp_paid_amount = 0;
        $ifHaveType2 = 0;
        foreach ($data['kaunter'] as $key => $value) {
            if($value->fk_lkp_payment_type == 2){
                $data['penuh']->fk_lkp_payment_type = 2;
                $data['penuh']->bp_receipt_number = $value->bp_receipt_number;
                $data['penuh']->bp_receipt_date = $value->bp_receipt_date;
                $data['penuh']->bp_paid_amount += $value->bp_paid_amount;
                $ifHaveType2 = 1;
            }
        }
        $data['newKaunter'] = [];
        foreach ($data['kaunter'] as $key => $value) {
            if ($value->fk_lkp_payment_type != 2) {
                $data['newKaunter'][] = $value;
            }
        }
        // if(!empty($data['kaunter'])){
        //     $data['kaunter'] = $data['newKaunter'];
        // }
        // if(!empty($data['kaunter']) && $ifHaveType2 == 1){
        //     $data['kaunter'][] = $data['penuh'];
        // } else if(!empty($data['kaunter']) && in_array($data['main']->fk_lkp_status, [5, 11])){
        //     $data['kaunter'][] = $data['penuh'];
        // }

        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['booking'], 'id', 'DESC');
        $data['et_attachment'] = All::GetAllRow('et_attachment', 'fk_main_booking', $data['booking'])->where('deleted_at', NULL)->first();
        $data['tax'] = All::Show('lkp_tax', 'lt_name', 'ASC');

        // dd($data);
        return view('sport.public.maklumattempahan', compact('data'));
    }

    public function ratingtempahan(Request $request, $id){
        $id = Crypt::decrypt($id);
        $data['main_booking'] = All::GetRow('main_booking', 'id', $id);
        $data['validate_rating'] = All::GetRow('et_rating', 'fk_main_booking', $id);
        if(!$data['validate_rating']){
            $rating_data = [
                'fk_users'          => Session::get('user.id'),
                'fk_main_booking'   => $id,
                'fk_lkp_location'   => $data['main_booking']->fk_lkp_location,
                'et_star'           => request()->rate,
                'et_comment'        => request()->comment,
                'created_at'        => date('Y-m-d H:i:s')
            ];
            $et_ratingID = All::InsertGetID('et_rating', $rating_data);
        }

        return redirect(url('/sport/maklumatbayaran', Crypt::encrypt($id)));
    }

    public function validation(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', 2);
        // $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->whereNotIn('lpm_description', 'FPX');
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        
        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id, 'id', 'DESC');
        $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['et_equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC')->sortBy('fk_et_function')->whereNull('deleted_at');
        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['et_hall_time'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['et_hall_book']->pluck('id'), 'id', 'DESC');

        $data['bookingData'] = Sport::dewan_sukan_rumusan($data['booking']);

        if(request()->isMethod('post')){
            // dd($data['main'], $request->post('jenis_bayaran'));
            if($data['main']->fk_lkp_status == 13){
                $statusPengesahan = [
                    'fk_lkp_status'     => request()->pengesahan,
                    'updated_at'        => date('Y-m-d')
                ];
                $query = All::GetUpdate('main_booking', $data['booking'], $statusPengesahan);
                $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
            } else if($data['main']->fk_lkp_status == 2){
                if($request->post('jenis_bayaran') == 1){
                    // Deposit
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 1,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_deposit_rm,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => 1000,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => 1000,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    // Pendahuluan
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 3,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);
                    
                    $data_main = [
                        'fk_main_booking'           => $data['booking'],
                        'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                        'ecb_date_booking'          => $data['et_hall_book'][0]->ehb_booking_date,
                        'ecb_flag_indicator'        => 2,
                        'updated_by'                => Session::get('user')['id'],
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'created_at'                => date('Y-m-d H:i:s')
                    ];
                    $ecbid = All::InsertGetID('et_confirm_booking', $data_main);

                    foreach ($data['et_hall_time'] as $key => $value) {
                        $data_main = [
                            'fk_et_confirm_booking'     => $ecbid,
                            'fk_et_facility_detail'     => $data['et_booking_facility'][0]->fk_et_facility_detail,
                            'fk_et_slot_time'           => $value->fk_et_slot_time,
                            'ecbd_date_booking'         => $data['et_hall_book'][0]->ehb_booking_date,
                            'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                            'updated_by'                => Session::get('user')['id'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'created_at'                => date('Y-m-d H:i:s')
                        ];
                        $query = All::InsertGetID('et_confirm_booking_detail', $data_main);
                    }

                    // main_booking_status
                    $data_main = [
                        'bmb_subtotal'      => $data['main']->bmb_subtotal,
                        'fk_lkp_status'     => 4,
                        'updated_by'        => Session::get('user')['id'],
                        'updated_at'        => date('Y-m-d H:i:s')
                    ];
                    $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                    if($query){
                        $audit = AuditLog::log(Crypt::encrypt(120), Crypt::encrypt($data['booking'], 'Bayaran Deposit diterima', 1));
                    }
                } else {
                    // Deposit
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 1,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_deposit_rm,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => 1000,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => 1000,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    // Pendahuluan
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 3,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);
                    
                    $data_main = [
                        'fk_main_booking'           => $data['booking'],
                        'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                        'ecb_date_booking'          => $data['et_hall_book'][0]->ehb_booking_date,
                        'ecb_flag_indicator'        => 2,
                        'updated_by'                => Session::get('user')['id'],
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'created_at'                => date('Y-m-d H:i:s')
                    ];
                    $ecbid = All::InsertGetID('et_confirm_booking', $data_main);

                    foreach ($data['et_hall_time'] as $key => $value) {
                        $data_main = [
                            'fk_et_confirm_booking'     => $ecbid,
                            'fk_et_facility_detail'     => $data['et_booking_facility'][0]->fk_et_facility_detail,
                            'fk_et_slot_time'           => $value->fk_et_slot_time,
                            'ecbd_date_booking'         => $data['et_hall_book'][0]->ehb_booking_date,
                            'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                            'updated_by'                => Session::get('user')['id'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'created_at'                => date('Y-m-d H:i:s')
                        ];
                        $query = All::InsertGetID('et_confirm_booking_detail', $data_main);
                    }

                    // Penuh
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 2,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        'bp_receipt_date'           => date('Y-m-d H:i:s'),
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    $data_ebf = array(
                        'ebf_subtotal'                  => $data['main']->bmb_subtotal,
                        'ebf_deposit'                   => $data['main']->bmb_deposit_rm,
                        'updated_at'                    => date('Y-m-d H:i:s'),
                        'updated_by'                    => Session::get('user')['id'],
                    );
                    $query = All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $data['booking'], $data_ebf);

                    // main_booking_status
                    $data_main = [
                        'fk_lkp_status'     => 5,
                        'updated_by'        => Session::get('user')['id'],
                        'updated_at'        => date('Y-m-d H:i:s')
                    ];
                    $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                    if($query){
                        $audit = AuditLog::log(Crypt::encrypt(120), Crypt::encrypt($data['booking'], 'Bayaran Deposit diterima', 1));
                    }
                }
            } elseif ($data['main']->fk_lkp_status == 4){
                $data['ecb'] = All::GetRow('et_confirm_booking', 'fk_main_booking', $data['main']->id);
                $data['ecbd'] = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $data['ecb']->id, 'id', 'DESC');
                // Penuh
                $dataBhPayment = [
                    'fk_lkp_payment_type'       => 2,
                    'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                    'fk_main_booking'           => $data['main']->id,
                    'fk_users'                  => $data['main']->fk_users,
                    'fk_bh_quotation'           => $data['bh_quotation']->id,
                    'bp_total_amount'           => $data['main']->bmb_subtotal,
                    'bp_deposit'                => $data['main']->bmb_deposit_rm,
                    'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    'bp_receipt_number'         => request()->rujukanBayaran,
                    // 'bp_payment_ref_no'         => ,
                    // 'bp_receipt_date'           => ,
                    'no_lopo'                   => request()->lopo,
                    // 'bp_special_discount'       => ,
                    'amount_received'           => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    // 'deposit_type'              => ,
                    'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    // 'no_cek'                    => ,
                    // 'nama_bank'                 => ,
                    'bp_payment_status'         => 1,
                    'created_at'                => date('Y-m-d'),
                    'updated_at'                => date('Y-m-d'),
                ];
                $bpid = All::InsertGetID('bh_payment', $dataBhPayment);
                
                // et_payment_detail
                // foreach ($data['et_equipment_book'] as $key => $value) {
                //     $data_epd = [
                //         'fk_bh_payment'             => $bpid,
                //         // 'fk_lkp_gst_rate'           => ,
                //         // 'fk_et_facility_detail'     => ,
                //         // 'fk_et_equipment'           => ,
                //         // 'fk_et_booking_facility'    => ,
                //         // 'product_indicator'         => ,
                //         // 'booking_date'              => ,
                //         // 'unit_price'                => ,
                //         // 'quantity'                  => ,
                //         // 'code_gst'                  => ,
                //         // 'gst_amount'                => ,
                //         // 'total_amount'              => ,
    
                //         'bp_payment_status'         => 1,
                //         'created_at'                => date('Y-m-d'),
                //         'updated_at'                => date('Y-m-d'),
                //     ];
                //     $query = All::InsertGetID('et_payment_detail', $data_epd);
                // }

                $data_ebf = array(
                    'ebf_subtotal'                  => $data['main']->bmb_subtotal,
                    'ebf_deposit'                   => $data['main']->bmb_deposit_rm,
                    'updated_at'                    => date('Y-m-d H:i:s'),
                    'updated_by'                    => Session::get('user')['id'],
                );
                $query = All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $data['booking'], $data_ebf);

                // main_booking_status
                $data_main = [
                    'fk_lkp_status'     => 5,
                    'updated_by'        => Session::get('user')['id'],
                    'updated_at'        => date('Y-m-d H:i:s')
                ];
                $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                $audit = AuditLog::log(Crypt::encrypt(122), Crypt::encrypt($data['booking'], 'Bayaran Penuh diterima', 1));
            }
            return redirect('/sport/booking/validation/'. Crypt::encrypt((int)$data['main']->id));
        }
        // dd($data);

        return view('sport.public.pengesahan', compact('data'));
    }

    public function hallpayment(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', 2);
        // $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->whereNotIn('lpm_description', 'FPX');
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        
        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id, 'id', 'DESC');
        $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['et_equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC')->sortBy('fk_et_function')->whereNull('deleted_at');
        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['et_hall_time'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['et_hall_book']->pluck('id'), 'id', 'DESC');
        $data['bookingData'] = Sport::dewan_sukan_rumusan($data['booking']);
        if(request()->isMethod('post')){
            // dd($data['main'], $request->post('jenis_bayaran'));
            if($data['main']->fk_lkp_status == 13){
                $statusPengesahan = [
                    'fk_lkp_status'     => request()->pengesahan,
                    'updated_at'        => date('Y-m-d')
                ];
                $query = All::GetUpdate('main_booking', $data['booking'], $statusPengesahan);
                $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
            } else if($data['main']->fk_lkp_status == 2){
                if($request->post('jenis_bayaran') == 1){
                    // Deposit
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 1,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_deposit_rm,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => 1000,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => 1000,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    // Pendahuluan
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 3,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);
                    
                    $data_main = [
                        'fk_main_booking'           => $data['booking'],
                        'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                        'ecb_date_booking'          => $data['et_hall_book'][0]->ehb_booking_date,
                        'ecb_flag_indicator'        => 2,
                        'updated_by'                => Session::get('user')['id'],
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'created_at'                => date('Y-m-d H:i:s')
                    ];
                    $ecbid = All::InsertGetID('et_confirm_booking', $data_main);

                    foreach ($data['et_hall_time'] as $key => $value) {
                        $data_main = [
                            'fk_et_confirm_booking'     => $ecbid,
                            'fk_et_facility_detail'     => $data['et_booking_facility'][0]->fk_et_facility_detail,
                            'fk_et_slot_time'           => $value->fk_et_slot_time,
                            'ecbd_date_booking'         => $data['et_hall_book'][0]->ehb_booking_date,
                            'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                            'updated_by'                => Session::get('user')['id'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'created_at'                => date('Y-m-d H:i:s')
                        ];
                        $query = All::InsertGetID('et_confirm_booking_detail', $data_main);
                    }

                    // main_booking_status
                    $data_main = [
                        'bmb_subtotal'      => $data['main']->bmb_subtotal,
                        'fk_lkp_status'     => 4,
                        'updated_by'        => Session::get('user')['id'],
                        'updated_at'        => date('Y-m-d H:i:s')
                    ];
                    $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                    if($query){
                        $audit = AuditLog::log(Crypt::encrypt(120), Crypt::encrypt($data['booking'], 'Bayaran Deposit diterima', 1));
                    }
                } else {
                    // Deposit
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 1,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_deposit_rm,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => 1000,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => 1000,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    // Pendahuluan
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 3,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        // 'bp_receipt_date'           => ,
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);
                    
                    $data_main = [
                        'fk_main_booking'           => $data['booking'],
                        'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                        'ecb_date_booking'          => $data['et_hall_book'][0]->ehb_booking_date,
                        'ecb_flag_indicator'        => 2,
                        'updated_by'                => Session::get('user')['id'],
                        'updated_at'                => date('Y-m-d H:i:s'),
                        'created_at'                => date('Y-m-d H:i:s')
                    ];
                    $ecbid = All::InsertGetID('et_confirm_booking', $data_main);

                    foreach ($data['et_hall_time'] as $key => $value) {
                        $data_main = [
                            'fk_et_confirm_booking'     => $ecbid,
                            'fk_et_facility_detail'     => $data['et_booking_facility'][0]->fk_et_facility_detail,
                            'fk_et_slot_time'           => $value->fk_et_slot_time,
                            'ecbd_date_booking'         => $data['et_hall_book'][0]->ehb_booking_date,
                            'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
                            'updated_by'                => Session::get('user')['id'],
                            'updated_at'                => date('Y-m-d H:i:s'),
                            'created_at'                => date('Y-m-d H:i:s')
                        ];
                        $query = All::InsertGetID('et_confirm_booking_detail', $data_main);
                    }

                    // Penuh
                    $dataBhPayment = [
                        'fk_lkp_payment_type'       => 2,
                        'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                        'fk_main_booking'           => $data['main']->id,
                        'fk_users'                  => $data['main']->fk_users,
                        'fk_bh_quotation'           => $data['bh_quotation']->id,
                        'bp_total_amount'           => $data['main']->bmb_subtotal,
                        'bp_deposit'                => $data['main']->bmb_deposit_rm,
                        'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        'bp_receipt_number'         => request()->rujukanBayaran,
                        // 'bp_payment_ref_no'         => ,
                        'bp_receipt_date'           => date('Y-m-d H:i:s'),
                        'no_lopo'                   => request()->lopo,
                        // 'bp_special_discount'       => ,
                        'amount_received'           => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        // 'deposit_type'              => ,
                        'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                        // 'no_cek'                    => ,
                        // 'nama_bank'                 => ,
                        'bp_payment_status'         => 1,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d'),
                    ];
                    $query = All::InsertGetID('bh_payment', $dataBhPayment);

                    $data_ebf = array(
                        'ebf_subtotal'                  => $data['main']->bmb_subtotal,
                        'ebf_deposit'                   => $data['main']->bmb_deposit_rm,
                        'updated_at'                    => date('Y-m-d H:i:s'),
                        'updated_by'                    => Session::get('user')['id'],
                    );
                    $query = All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $data['booking'], $data_ebf);

                    // main_booking_status
                    $data_main = [
                        'fk_lkp_status'     => 5,
                        'updated_by'        => Session::get('user')['id'],
                        'updated_at'        => date('Y-m-d H:i:s')
                    ];
                    $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                    if($query){
                        $audit = AuditLog::log(Crypt::encrypt(120), Crypt::encrypt($data['booking'], 'Bayaran Deposit diterima', 1));
                    }
                }
            } elseif ($data['main']->fk_lkp_status == 4){
                $data['ecb'] = All::GetRow('et_confirm_booking', 'fk_main_booking', $data['main']->id);
                $data['ecbd'] = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $data['ecb']->id, 'id', 'DESC');

                // Penuh
                $dataBhPayment = [
                    'fk_lkp_payment_type'       => 2,
                    'fk_lkp_payment_mode'       => (int)request()->payment_mode,
                    'fk_main_booking'           => $data['main']->id,
                    'fk_users'                  => $data['main']->fk_users,
                    'fk_bh_quotation'           => $data['bh_quotation']->id,
                    'bp_total_amount'           => $data['main']->bmb_subtotal,
                    'bp_deposit'                => $data['main']->bmb_deposit_rm,
                    'bp_paid_amount'            => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    'bp_receipt_number'         => request()->rujukanBayaran,
                    // 'bp_payment_ref_no'         => ,
                    // 'bp_receipt_date'           => ,
                    'no_lopo'                   => request()->lopo,
                    // 'bp_special_discount'       => ,
                    'amount_received'           => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    // 'deposit_type'              => ,
                    'bp_subtotal'               => $data['main']->bmb_total_book_hall / 2 + $data['main']->bmb_total_equipment,
                    // 'no_cek'                    => ,
                    // 'nama_bank'                 => ,
                    'bp_payment_status'         => 1,
                    'created_at'                => date('Y-m-d'),
                    'updated_at'                => date('Y-m-d'),
                ];
                $query = All::InsertGetID('bh_payment', $dataBhPayment);

                $data_ebf = array(
                    'ebf_subtotal'                  => $data['main']->bmb_subtotal,
                    'ebf_deposit'                   => $data['main']->bmb_deposit_rm,
                    'updated_at'                    => date('Y-m-d H:i:s'),
                    'updated_by'                    => Session::get('user')['id'],
                );
                $query = All::GetUpdateSpec('et_booking_facility', 'fk_main_booking', $data['booking'], $data_ebf);

                // main_booking_status
                $data_main = [
                    'fk_lkp_status'     => 5,
                    'updated_by'        => Session::get('user')['id'],
                    'updated_at'        => date('Y-m-d H:i:s')
                ];
                $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
                $audit = AuditLog::log(Crypt::encrypt(122), Crypt::encrypt($data['booking'], 'Bayaran Penuh diterima', 1));
            }
            return redirect('/sport/booking/hallpayment/'. Crypt::encrypt((int)$data['main']->id));
        }
        // dd($data);

        return view('sport.public.hallpayment', compact('data'));
    }

    public function pelarasan(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id, 'id', 'DESC');
        $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        $data['et_equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC')->sortBy('fk_et_function');
        $data['et_hall_book'] = All::GetAllRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility'][0]->id);
        $data['et_hall_time'] = All::GetAllRow('et_hall_time', 'fk_et_hall_book', $data['et_hall_book'][0]->id);
        $data['eht_ehb_ebf'] = All::JoinThreeTablesWithWhere2('et_hall_time', 'et_hall_book', 'et_booking_facility', 'fk_et_hall_book', $data['et_hall_book'][0]->id);

        if (request()->isMethod('post')){
            $data['eht_data'] = [];
            $data['eeb_data'] = [];

            $max = 1;
            $max = count($data['et_hall_time']) < $max ? $max : count($data['et_hall_time']);
            for ($i=0; $i < $max; $i++) {
                $data['eht_data'][$i] = $request->post('perc')[$i] .','. $request->post('val')[$i] .','. $request->post('hdsubtotal')[$i];
            }
            
            $max = count($data['et_equipment_book']) < $max ? $max : count($data['et_equipment_book']);
            for ($i=0; $i < $max; $i++) {
                $data['eeb_data'][$i] = $request->post('eqperc')[$i] .','. $request->post('eqval')[$i] .','. $request->post('hdeqsubtotal')[$i];
            }
            
            $i = 0;
            // Update et_hall_time
            foreach ($data['eht_data'] as $key => $value) {
                $id_ehb = $data['et_hall_time']->pluck('id');
                $row = explode(',', $value);
                $eht_data = [
                    'eht_discount_type_rm'      => 0.00,
                    'eht_special_disc'          => $row[0],
                    'eht_special_disc_rm'       => $row[1],
                    'eht_discount_rm'           => 0.00,
                    'eht_subtotal'              => $row[2]
                ];
                All::GetUpdate('et_hall_time', $id_ehb[$i], $eht_data);
                AuditLog::log(Crypt::encrypt(5), Crypt::encrypt($data['booking']), 'Kemas kini Harga Dewan - id = '. $id_ehb[$i], 1);
                $i++;
            }

            $i = 0;
            // Update et_equipment_book
            foreach ($data['eeb_data'] as $key => $value) {
                $id_eeb = $data['et_equipment_book']->pluck('id');
                $row = explode(',', $value);
                $eeb_data = [
                    'eeb_discount_type_rm'      => 0.00, 
                    'eeb_discount_rm'           => 0.00,
                    'eeb_special_disc'          => $row[0],
                    'eeb_special_disc_rm'       => $row[1],
                    'eeb_subtotal'              => $row[2]
                ];
                All::GetUpdate('et_equipment_book', $id_eeb[$i], $eeb_data);
                AuditLog::log(Crypt::encrypt(5), Crypt::encrypt($data['booking']), 'Kemas kini Harga Peralatan - id = '. $id_eeb[$i], 1);
                $i++;
            }
            return redirect('sport/booking/validation/'. Crypt::encrypt($data['booking']));
        }

        $data['bh_quotation'] = All::GetRow('bh_quotation', 'fk_main_booking', $data['main']->id);
        $data['payment_mode'] = All::ShowAll('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->whereNotIn('lpm_description', 'FPX');
        $data['kaunter'] = All::GetAllRow('bh_payment', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        $data['online'] = All::GetAllRow('et_payment_fpx', 'fk_main_booking', $data['main']->id, 'id', 'DESC')->whereNull('deleted_at');
        
        // $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id, 'id', 'DESC');
        // $data['et_booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC');
        // $data['et_equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'), 'id', 'DESC')->sortBy('fk_et_function');
        // $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility'][0]->id);
        // $data['eht_ehb_ebf'] = All::JoinThreeTablesWithWhere2('et_hall_time', 'et_hall_book', 'et_booking_facility', 'fk_et_hall_book', $data['et_hall_book']->id);
        
        return view('sport.public.pelarasan', compact('data'));
    }

    public function validateSportSlot(Request $request)
    {
        $id = request()->post('id');
        $data['mb'] = All::GetRow('main_booking', 'id', $id);
        $data['ebf'] = All::GetRow("et_booking_facility", 'fk_main_booking', $data['mb']->id);
        $data['esb'] = All::GetRow("et_sport_book", 'fk_et_booking_facility', $data['ebf']->id);
        $data['est'] = All::GetAllRow("et_sport_time", 'fk_et_sport_book', $data['esb']->id);
        $data['exist'] = Sport::slotexist($data['ebf']->fk_et_facility_type, $data['ebf']->fk_et_facility_detail, $data['esb']->esb_booking_date, $data['est']->pluck('fk_et_slot_price'));
        if (!$data['mb'] || !$data['ebf']) {
            return response()->json(['status' => 'error', 'message' => 'Data not found'], 404);
        }
        if(count($data['exist']) > 0){
            return response()->json(['status' => 'error', 'message' => 'Fasiliti pada tarikh berkenaan telah ditempah'], 500);
        }
        $data_fpx = array(
            'fk_main_booking'       => $data['mb']->id,
            'fk_et_facility_type'   => $data['ebf']->fk_et_facility_type,
            'ecb_date_booking'      => $data['ebf']->ebf_start_date,
            'ecb_flag_indicator'    => 1,
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s'),
        );
        $query = All::InsertGetID('et_confirm_booking', $data_fpx);
        if (!$query) {
            return response()->json(['status' => 'error', 'message' => 'Failed to insert data'], 500);
        }
        return response()->json(['status' => 'success', 'message' => 'Date, time and location is available']);
    }

    public function equipment(Request $request){
        $data['post'] = 0;
        $data['booking_no'] = '';
        
        if(request()->isMethod('post')){
            $data['post'] = 1;
            $data['type'] = request()->type;

            $data['booking_no'] = request()->bmb_booking_no;
            $data['mb'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking_no']);
            $data['mbebf'] = All::JoinTwoTableWithWhere('et_booking_facility', 'main_booking', 'bmb_booking_no', $data['booking_no']);

            // "Tempahan Ini Tidak Dibenarkan Untuk Membuat Tempahan Peralatan Kerana Tiada Bayaran Diterima"
            if(count($data['mbebf']) == 0 && $data['mb']->fk_lkp_status != 5 && $data['mb']->fk_lkp_status != 11){    
                return redirect()->back();
            }

            if($data['type'] == 2){
                $data['post'] = 2;
                $data['jenisTempahan'] = 1; // 1 = Sukan, 2 = Dewan

                $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mb']->id);
                
                $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
                if(count($data['esb']) == 0){
                    $data['jenisTempahan'] = 2;
                    $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
                }
                
                $data['equipment_int'] = Sport::getEquipdalam($data['mb']->fk_lkp_location);
                $data['equipment_ext'] = Sport::getEquipluar($data['mb']->fk_lkp_location);

                $data['eebdalaman'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 8);
                $data['eebluaran'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 9);
                // dd($data);
            } else if($data['type'] == 3){
                $kegunaan = request()->kegunaan;
                $kuantiti = request()->kuantiti;

                if($kegunaan == 1){         // Dalaman
                    $ebfID = request()->itemId;
                    $eqid = request()->dalaman;

                    $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                    $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                    if ($date->isWeekend()) {
                        $day_cat = 2;
                    } else {
                        $day_cat = 1;
                    }

                    $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 8)->where('eep_day_cat', $day_cat)->first();
                    $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                    $data_eq = [
                        'fk_et_booking_facility'        => $ebfID,
                        'fk_et_function'                => 8,
                        'fk_et_equipment'               => $eqid,
                        'fk_et_equipment_price'         => $eq_price->id,
                        'fk_lkp_gst_rate'               => $gst_rate->id,
                        'eeb_booking_date'              => $ebf_date,
                        'eeb_unit_price'                => $eq_price->eep_unit_price,
                        'eeb_quantity'                  => $kuantiti,
                        'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                        // 'eeb_discount_type_rm'          => ,
                        // 'eeb_discount_rm'               => ,
                        'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                        'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                        'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                        'created_at'                    => date('Y-m-d H:i:s'),
                        'updated_at'                    => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_equipment_book', $data_eq);
                } else if($kegunaan == 2){  // Luaran
                    $ebfID = request()->itemId;
                    $eqid = request()->dalaman;

                    $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                    $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                    if ($date->isWeekend()) {
                        $day_cat = 2;
                    } else {
                        $day_cat = 1;
                    }

                    $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 9)->where('eep_day_cat', $day_cat)->first();
                    $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                    $data_eq = [
                        'fk_et_booking_facility'        => $ebfID,
                        'fk_et_function'                => 9,
                        'fk_et_equipment'               => $eqid,
                        'fk_et_equipment_price'         => $eq_price->id,
                        'fk_lkp_gst_rate'               => $gst_rate->id,
                        'eeb_booking_date'              => $ebf_date,
                        'eeb_unit_price'                => $eq_price->eep_unit_price,
                        'eeb_quantity'                  => $kuantiti,
                        'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                        // 'eeb_discount_type_rm'          => ,
                        // 'eeb_discount_rm'               => ,
                        'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                        'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                        'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                        'created_at'                    => date('Y-m-d H:i:s'),
                        'updated_at'                    => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_equipment_book', $data_eq);

                }
            }
        }

        return view('sport.equipmentadd.index', compact('data'));
    }

    public function equipment_edit(Request $request, $mbid, $eebid = null){
        $mbid = Crypt::decrypt($mbid);
        $data['mb'] = All::GetRow('main_booking', 'id', $mbid);
        $data['booking_no'] = $data['mb']->bmb_booking_no;
        $data['mbebf'] = All::JoinTwoTableWithWhere('et_booking_facility', 'main_booking', 'bmb_booking_no', $data['booking_no']);

        $data['type'] = request()->type;
        $data['post'] = 1;
        $data['jenisTempahan'] = 1; // 1 = Sukan, 2 = Dewan

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mb']->id);
        
        $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        if(count($data['esb']) == 0){
            $data['jenisTempahan'] = 2;
            $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        }
        
        $data['equipment_int'] = Sport::getEquipdalam($data['mb']->fk_lkp_location);
        $data['equipment_ext'] = Sport::getEquipluar($data['mb']->fk_lkp_location);

        $data['eebdalaman'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 8);
        $data['eebluaran'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 9);
        
        if($data['type'] == 3){
            $kegunaan = request()->kegunaan;
            $kuantiti = request()->kuantiti;

            if($kegunaan == 1){         // Dalaman
                $ebfID = request()->itemId;
                $eqid = request()->dalaman;

                $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                if ($date->isWeekend()) {
                    $day_cat = 2;
                } else {
                    $day_cat = 1;
                }

                $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 8)->where('eep_day_cat', $day_cat)->first();
                $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                $data_eq = [
                    'fk_et_booking_facility'        => $ebfID,
                    'fk_et_function'                => 8,
                    'fk_et_equipment'               => $eqid,
                    'fk_et_equipment_price'         => $eq_price->id,
                    'fk_lkp_gst_rate'               => $gst_rate->id,
                    'eeb_booking_date'              => $ebf_date,
                    'eeb_unit_price'                => $eq_price->eep_unit_price,
                    'eeb_quantity'                  => $kuantiti,
                    'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                    // 'eeb_discount_type_rm'          => ,
                    // 'eeb_discount_rm'               => ,
                    'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                    'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                    'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                    'created_at'                    => date('Y-m-d H:i:s'),
                    'updated_at'                    => date('Y-m-d H:i:s'),
                ];
                $query = All::InsertGetID('et_equipment_book', $data_eq);
            } else if($kegunaan == 2){  // Luaran
                $ebfID = request()->itemId;
                $eqid = request()->dalaman;

                $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                if ($date->isWeekend()) {
                    $day_cat = 2;
                } else {
                    $day_cat = 1;
                }

                $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 9)->where('eep_day_cat', $day_cat)->first();
                $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                $data_eq = [
                    'fk_et_booking_facility'        => $ebfID,
                    'fk_et_function'                => 9,
                    'fk_et_equipment'               => $eqid,
                    'fk_et_equipment_price'         => $eq_price->id,
                    'fk_lkp_gst_rate'               => $gst_rate->id,
                    'eeb_booking_date'              => $ebf_date,
                    'eeb_unit_price'                => $eq_price->eep_unit_price,
                    'eeb_quantity'                  => $kuantiti,
                    'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                    // 'eeb_discount_type_rm'          => ,
                    // 'eeb_discount_rm'               => ,
                    'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                    'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                    'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                    'created_at'                    => date('Y-m-d H:i:s'),
                    'updated_at'                    => date('Y-m-d H:i:s'),
                ];
                $query = All::InsertGetID('et_equipment_book', $data_eq);
            }
            return redirect()->back();
        }

        return view('sport.equipmentadd.edit', compact('data'));
    }

    public function equipment_summary(Request $request, $mbid){
        $mbid = Crypt::decrypt($mbid);
        $data['mb'] = All::GetRow('main_booking', 'id', $mbid);
        $data['booking_no'] = $data['mb']->bmb_booking_no;
        $data['mbebf'] = All::JoinTwoTableWithWhere('et_booking_facility', 'main_booking', 'bmb_booking_no', $data['booking_no']);

        $data['type'] = request()->type;
        $data['post'] = 1;
        $data['jenisTempahan'] = 1; // 1 = Sukan, 2 = Dewan

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mb']->id);
        
        $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        if(count($data['esb']) == 0){
            $data['jenisTempahan'] = 2;
            $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        }
        
        $data['equipment_int'] = Sport::getEquipdalam($data['mb']->fk_lkp_location);
        $data['equipment_ext'] = Sport::getEquipluar($data['mb']->fk_lkp_location);

        $data['eebdalaman'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 8);
        $data['eebluaran'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'))->where('fk_et_function', 9);
        
        return view('sport.equipmentadd.summary', compact('data'));
    }

    public function equipment_payment(Request $request, $mbid){
        $mbid = Crypt::decrypt($mbid);
        $data['main'] = All::GetRow('main_booking', 'id', $mbid);
        
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        
        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);
        $data['total'] = $request->post('total');
        
        $data['int_eq'] = [];
        $data['ext_eq'] = [];
        
        if(($request->post('type'))){
            $type = 1;
        } else {
            $type = 0;
        }
        if($type != 1){
            foreach ($request->post() as $key => $value) {
                if (strpos($key, 'int_') === 0) {
                    // For internal equipment, decode the JSON string and push it to the internal equipment array
                    $val = json_decode($value, true);
                    $data['int_eq'][] = $value;
                } elseif (strpos($key, 'ext_') === 0) {
                    // For external equipment, decode the JSON string and push it to the external equipment array
                    $val = json_decode($value, true);
                    $data['ext_eq'][] = $value;
                }
            }
        } else {
            $ext_eq = json_decode(request()->ext_eq, true);
            $int_eq = json_decode(request()->int_eq, true);

            foreach ($request->post() as $key => $value) {
                if ($key == 'int_eq' && !empty($int_eq)) {
                    // For internal equipment, decode the JSON string and push it to the internal equipment array
                    $val = json_decode($request->post('int_eq'), true);
                    foreach($val as $val){
                        $int_eq_data = json_decode($val, true);
                        $data['int_eq'][] = $int_eq_data;
                    }
                } elseif ($key == 'ext_eq' && !empty($ext_eq)) {
                    // For external equipment, decode the JSON string and push it to the external equipment array
                    $val = json_decode($request->post('ext_eq'), true);
                    foreach($val as $val){
                        $ext_eq_data = json_decode($val, true);
                        $data['ext_eq'][] = $ext_eq_data;
                    }
                }
            }
        }
        
        if($request->isMethod('post')){
            $no = Sport::generatereceipt();
            
            $data_fpx = array(
                'fk_main_booking'           => $mbid,
                'fk_lkp_payment_type'       => 2,
                'fk_lkp_payment_mode'       => $request->post('bayaran'),
                'fk_users'                  => $data['main']->fk_users,
                'bp_total_amount'           => $data['total'],
                'bp_deposit'                => 0,
                'bp_paid_amount'            => $data['total'],
                'bp_receipt_number'         => sprintf('%07d', $no),
                'bp_payment_ref_no'         => $request->post('ref'),
                'bp_receipt_date'           => date('Y-m-d H:i:s'),
                'no_lopo'                   => $request->post('lopo') ? $request->post('lopo') : '',
                'amount_received'           => $data['total'],
                'bp_subtotal'               => $data['total'],
                'no_cek'                    => $request->post('nocek'),
                'nama_bank'                 => $request->post('namabank'),
                'bp_payment_status'         => 1,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $bpid = All::InsertGetID('bh_payment', $data_fpx);
            // $bpid = 1;

            if(count($data['ext_eq']) > 0){
                foreach ($data['ext_eq'] as $key => $value) {
                    $eq_data = All::GetRow('et_equipment_book', 'id', $value['id']);
                    $lkp_gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_data->fk_lkp_gst_rate);
                    $efdid = All::GetRow('et_booking_facility', 'id', $value['ebf'])->fk_et_facility_detail;

                    $data_epd = [
                        'fk_bh_payment'         => $bpid,
                        'fk_lkp_gst_rate'       => $lkp_gst_rate->id,
                        'fk_et_facility_detail' => $efdid,
                        'fk_et_equipment'       => $eq_data->fk_et_equipment,
                        'fk_et_booking_facility'=> $value['ebf'],
                        'product_indicator'     => 2,
                        'booking_date'          => $eq_data->eeb_booking_date,
                        'unit_price'            => $eq_data->eeb_unit_price,
                        'quantity'              => $eq_data->eeb_quantity,
                        'code_gst'              => $lkp_gst_rate->lgr_gst_code,
                        'gst_amount'            => $eq_data->eeb_gst_rm,
                        'total_amount'          => $eq_data->eeb_subtotal,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_payment_detail', $data_epd);
                }
            }
            if(count($data['int_eq']) > 0){
                foreach ($data['int_eq'] as $key => $value) {
                    $eq_data = All::GetRow('et_equipment_book', 'id', $value['id']);
                    $lkp_gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_data->fk_lkp_gst_rate);
                    $efdid = All::GetRow('et_booking_facility', 'id', $value['ebf'])->fk_et_facility_detail;

                    $data_epd = [
                        'fk_bh_payment'         => $bpid,
                        'fk_lkp_gst_rate'       => $lkp_gst_rate->id,
                        'fk_et_facility_detail' => $efdid,
                        'fk_et_equipment'       => $eq_data->fk_et_equipment,
                        'fk_et_booking_facility'=> $value['ebf'],
                        'product_indicator'     => 2,
                        'booking_date'          => $eq_data->eeb_booking_date,
                        'unit_price'            => $eq_data->eeb_unit_price,
                        'quantity'              => $eq_data->eeb_quantity,
                        'code_gst'              => $lkp_gst_rate->lgr_gst_code,
                        'gst_amount'            => $eq_data->eeb_gst_rm,
                        'total_amount'          => $eq_data->eeb_subtotal,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_payment_detail', $data_epd);
                }
            }

            $data_main = array(
                // 'bmb_subtotal'              => $data['main']->bmb_subtotal + $data['total'],
                // 'bmb_total_equipment'       => $data['main']->bmb_total_equipment + $data['total'],
                // 'bmb_rounding'              => $data['main']->bmb_rounding + $data['total'],
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Session::get('user')['id'],
            );
            $query = All::GetUpdate('main_booking', $mbid, $data_main);

            if($query){
                return redirect('sport/maklumatbayaran/'. Crypt::encrypt($mbid));
            } else {
                return redirect()->back();
            }
        }
        
        return view('sport.equipmentadd.bayaran', compact('data'));
    }

    public function equipment_delete($eebid){
        $eebid = Crypt::decrypt($eebid);
        $eeb = All::GetRow('et_equipment_book', 'id', $eebid);
        $query = All::GetDelete('et_equipment_book', $eebid);
        // $delete_eeb = [
        //     'updated_at'        => date('Y-m-d H:i:s'),
        //     'deleted_at'        => date('Y-m-d H:i:s'),
        // ];
        // $query = All::GetUpdate('et_equipment_book', $eebid, $delete_eeb);

        return response()->json(['success' => true]);
        // return redirect()->back();
    }

    public function status(){
        $data['orderNo'] = "SPS221003781900001";
        $data['status'] = "FAILED";
        $data['txnReference'] = "2210030928330089";
        $data['txnId'] = "FPX2200000600";
        $data['txnTime'] = date("Y-m-d H:i:s", strtotime("20221003092833"));
        $data['amount'] = "8.00";
        $data['bankname'] = "SBI Bank A";
        return view('sport.public.status', compact('data'));
    }

    public function mybookings($type = null){
        $data['jenis'] = $type;
        $data['laporan_user'] = Sport::laporan_user(Session::get('user.id'));

        return view('sport.public.tempahansaya', compact('data'));
    }

    public function test_slot(){
        $startDate = '2023-08-29';
        $endDate = '2023-09-02';
        
        $datesBetween = $this->getDatesBetween($startDate, $endDate);
        $data = [];
        foreach($datesBetween as $d){
            $data[] = Sport::slot('23',$d);
        }
    }

    public function getDatesBetween($startDate, $endDate)
    {
        $dates = [];

        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);

        while ($start->lte($end)) {
            $dates[] = $start->toDateString();
            $start->addDay();
        }

        return $dates;
    }

    public function bannerStatus(){
        $data['status'] = All::GetAllRow('bb_banner', 'bb_status', 1)->where('deleted_at', NULL);
        return response()->json(['success' => true, 'data' => count($data['status'])]);
        // if(count($data['status']) >= 3){
        //     return response()->json(['error' => true, 'message' => 'Banner status count is 3 or more. Please deactivate another banner to activate or use deactive first.'], 422);
        // } else {
        //     return response()->json(['success' => true]);
        // }
    }

    // public function details(Request $request, $id) {
    //     $id = Crypt::decrypt($id);
    //     $data['id'] = $id;
    //     $data['date'] = date("Y-m-d");
    //     $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2);
    //     $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);

    //     if(request()->isMethod('post')){
    //         $data['post'] = true;
    //         $data['id'] = $request->post('lokasi');
    //         $data['date'] = $request->post('tarikh');
    //         $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['id']);
    //     }
    //     $data['others'] = All::Show('et_facility_type', 'id', 'ASC')->where('fk_et_facility', $data['fasility']->fk_et_facility)->where('id', '!=' , $id);
    //     $data['price'] = Sport::price($id);
    //     $data['slot'] = Sport::slot($id, $data['date']);
    //     $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id)->WhereNull('deleted_at')->where('efd_status', 1);


    //     return view('sport.public.details', compact('data'));
    // }

    // public function slotMasaSukan(Request $request){
    //     $id = Crypt::decrypt(request()->id);
    //     $date = request()->date;
    //     $convertedDate = date('Y-m-d', strtotime($date));
    //     $data = Sport::slot($id, $convertedDate);
    //     dd($data);
    // }

    public function pdgsintetikForm($id, $date){
        $date = Crypt::decrypt($date);
        $id = Crypt::decrypt($id);
        $data['date'] = $date;
        $data['id'] = $id;
        $et_facility_type = All::GetRow('et_facility_type', 'id', $data['id']);
        $data['et_facility_type'] = $et_facility_type;
        $etbooking = All::GetRow('et_booking_facility', 'fk_et_facility_type', $et_facility_type->id);
        $data['etbooking'] = $etbooking->fk_main_booking;
        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        return view('sport.public.form', compact('data'));
    }

    public function testfpxuat(){
        // $data['newOrderNo'] = 'SPS24051878576';
        // $data['total'] = '4.00';
        // $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/access';

        // $data1 = array(
        //     "payload" => array(
        //         "subsysId"      => "SPS",
        //         "password"      => "e_TemPahan@@2024!",
        //         "orderNo"       => $data['newOrderNo'],
        //         "description"   => $data['newOrderNo'],
        //         "txnTime"       => date('Y-m-d H:i:s'),
        //         'adhoc'         => 0,
        //         "amount"        => $data['total'],
        //         // "feeCode"       => $feeCode,
        //     )
        // );
    
        // $postdata = json_encode($data1);
    
        // $ch = curl_init($url); 
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        // $result = curl_exec($ch);
        // $response = json_decode($result, true);
        // curl_close($ch);
        
        // if (strpos($result, 'Illuminate\Database\QueryException') !== false) {
        //     dd('2');
        //     // Log the error
        //     \Log::error('Illuminate\Database\QueryException found in response.');

        //     // Redirect back with error message
        //     return redirect()->back()->with('error', 'An error occurred while processing your request. Please try again later.');
        // }
        // dd($response, $result);
    }
}

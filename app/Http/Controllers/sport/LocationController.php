<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class LocationController extends Controller
{
    public function show() {
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['location'] = All::Show('lkp_location', 'updated_at', 'DESC')->where('lc_type', 2);
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $SportLocation = (array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]));
            foreach($SportLocation as $key => $value){
                $SportLocation[$key] = HP::role_location($value);
            }
            $data['location'] = All::Show('lkp_location', 'updated_at', 'DESC')->where('lc_type', 2)->whereIn('id', $SportLocation);
        } else {
            return redirect('/');
        }

        return view('location.lists', compact('data'));
    }

    public function form(){
        return view('location.form');
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_contact_no'     => request()->nocontact,
            'lc_fax_no'         => request()->nofax,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('lkp_location', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(137), Crypt::encrypt($query), 'Sukan - Tambah Rekod Lokasi '.request()->lokasi,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }


    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('lkp_location', 'id', $id);
        return view('location.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;
        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }


        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_contact_no'     => request()->nocontact,
            'lc_fax_no'         => request()->nofax,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(138), Crypt::encrypt($id), 'Sukan - Kemas kini Rekod Lokasi = '. request()->lokasi,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(139), Crypt::encrypt($id), 'Sukan - Padam Rekod Lokasi = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/location'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/location'));
        }
    }
    
}

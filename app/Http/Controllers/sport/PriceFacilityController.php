<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class PriceFacilityController extends Controller
{
    public function show() {
        // $data['list'] = All::Show('et_facility_price', 'updated_at', 'DESC');
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['list'] = All::Show('et_facility_price', 'updated_at', 'DESC');
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $SportLocation = (array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]));
            foreach($SportLocation as $key => $value){
                $SportLocation[$key] = HP::role_location($value);
            }
            $data['list'] = All::Show('et_facility_price', 'updated_at', 'DESC');
            foreach($data['list'] as $key => $value){
                $lkp_loc = HP::lkp_loc_from_ef2($value->fk_et_facility);
                if($lkp_loc != ""){
                    if(in_array($lkp_loc->id, $SportLocation)){
                        $data['list'][$key]->fk_lkp_location = $lkp_loc->id;
                    } else {
                        unset($data['list'][$key]);
                    }
                } else {
                    unset($data['list'][$key]);
                }
            }
        } else {
            return redirect('/');
        }

        return view('sport.facilityprice.lists', compact('data'));
    }

    public function form(){
        $data['facility'] = All::Show('et_facility', 'ef_desc', 'ASC');
        $data['function'] = All::Show('et_function', 'ef_desc', 'ASC');
        $data['package'] = All::Show('et_package', 'updated_at', 'DESC');
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');

        $data['gst'] = All::Show('lkp_gst_rate', 'updated_at', 'DESC');
        $data['slot'] = All::Show('lkp_slot_cat', 'updated_at', 'DESC');

        // $data['facility'] = AlL::JoinTwoTable2('et_facility', 'et_facility_type');
        // $data['function'] = All::Show('et_function', 'id', 'DESC')->where('ef_type', 1);

        return view('sport.facilityprice.form', compact('data'));
    }

    public function add(Request $request){
        $status = $request->input('status') == 'on' ? 1 : 0;

        $data = array(
            'fk_et_facility'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_et_slot_time'   => request()->masa,
            'fk_lkp_gst_rate'   => request()->gst,
            'fk_et_package'     => request()->pakej,
            'fk_lkp_slot_cat'   => request()->slot,
            'efp_day_cat'       => request()->hari,
            'efp_unit_price'    => request()->harga,
            'efp_status'        => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_facility_price', $data);

        $data = array(
            'fk_et_facility'            => request()->fasiliti,
            'fk_et_slot_time'           => request()->masa,
            'fk_et_facility_price'      => $query,
            'esp_day_cat'               => request()->hari,
            'updated_by'                => Session::get('user')['id'],
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $query = All::Insert('et_slot_price', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(147), Crypt::encrypt($query), 'Sukan - Tambah Harga Fasiliti = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['facility'] = All::Show('et_facility', 'ef_desc', 'ASC');
        $data['function'] = All::Show('et_function', 'ef_desc', 'ASC')->whereIn('ef_type', [1, 0]);
        $data['package'] = All::Show('et_package', 'updated_at', 'DESC');
        $data['time'] = All::Show('et_slot_time', 'id', 'ASC');

        $data['list'] = All::GetRow('et_facility_price', 'id', $id);

        $data['gst'] = All::Show('lkp_gst_rate', 'updated_at', 'DESC');
        $data['slot'] = All::Show('lkp_slot_cat', 'updated_at', 'DESC');
        // dd($data);
        return view('sport.facilityprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == "on"){
            $status = 1;
        }else{
            $status = 0;
        }

        $data = array(
            'fk_et_facility'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_et_slot_time'   => request()->masa,
            'fk_lkp_gst_rate'   => request()->gst,
            'fk_et_package'     => request()->pakej,
            'fk_lkp_slot_cat'   => request()->slot,
            'efp_day_cat'       => request()->hari,
            'efp_unit_price'    => request()->harga,
            'efp_status'        => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_price', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(148), Crypt::encrypt($id), 'Sukan - Kemas kini Harga Fasiliti = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_price', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(149), Crypt::encrypt($id), 'Sukan - Padam Harga Fasiliti = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilityprice'));
        }
    }
}

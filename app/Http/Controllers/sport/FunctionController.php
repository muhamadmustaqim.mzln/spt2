<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class FunctionController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_function_detail', 'updated_at', 'DESC');
        return view('sport.function.lists', compact('data'));
    }

    public function form(){
        $data['list'] = All::Show('et_function', 'ef_desc', 'ASC');
        $data['facility'] = All::Show('et_facility', 'ef_desc', 'ASC');

        return view('sport.function.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_function'           => request()->nama,
            'fk_et_facility'           => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_function_detail', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(134), Crypt::encrypt($query), 'Tambah Rekod Fungsi Fasiliti = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/function'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/function'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::Show('et_function', 'ef_desc', 'ASC');
        $data['facility'] = All::Show('et_facility', 'ef_desc', 'ASC');
        $data['function'] = All::GetRow('et_function_detail', 'id', $id);

        return view('sport.function.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_function'           => request()->nama,
            'fk_et_facility'           => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_function_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(135), Crypt::encrypt($id), 'Kemas kini Rekod Fungsi Fasiliti = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/function'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/function'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_function_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(136), Crypt::encrypt($id), 'Padam Rekod Fungsi Fasiliti = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/function'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/function'));
        }
    }
}

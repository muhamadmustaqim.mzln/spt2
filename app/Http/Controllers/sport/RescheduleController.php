<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\MailController as Mail;
use App\Models\All;
use App\Models\Auth;
use App\Models\Sport;
use App\Models\AuditLog;

class RescheduleController extends Controller
{
    public function index(Request $request) {
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
		
		$data['post'] = false;
        $data['id'] = "";
		
        if(request()->isMethod('post')){
			
            $data['post'] = true;
            $data['id'] = $request->post('id');
			
			//$data['booking'] = Crypt::decrypt($booking);
			//$data['main'] = All::GetAllRow('main_booking', 'bmb_booking_no', $data['id'], 'bmb_booking_no', 'DESC');
            if(substr($data['id'], 0, 3) == 'SPS') {
                $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            }

            if (isset($data['main']->fk_users))
			{
				$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
			}
			//$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
			
        }

        return view('sport.reschedule.index', compact('data'));
    }
	
	public function sport($booking) {
		$data['booking'] = Crypt::decrypt($booking);
		$data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
		$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
		$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
		$data['slot'] = Sport::priceSlot($data['main']->id);    // Sukan , 1
		$data['slot2'] = Sport::priceSlot2($data['main']->id);  // Dewan , 2
        // dd($data);

        return view('sport.reschedule.reschedule', compact('data'));
    }
	
	public function slot(Request $request, $type, $id, $date, $booking, $estid){
        $type = Crypt::decrypt($type);
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
		$data['booking'] = Crypt::decrypt($booking);
		$data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
        $ebf = All::GetRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $esb = All::GetRow('et_sport_book', 'fk_et_booking_facility', $ebf->id);

        $data['estid'] = Crypt::decrypt($estid);
        $data['id'] = $id;
        if(request()->isMethod('post')){
            $date = date('Y-m-d', strtotime($request->post('tarikh')));
        }
        
        $data['date'] = $date;
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);
        $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id);
		$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
		$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::slot($id, $date);
        // dd($id, $date, $data);
        // $data['slot2'] = Sport::slotDewan($id, $date);

        AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Buka paparan Penjadualan bagi Sukan', 1);
        return view('sport.reschedule.slot', compact('data'));
    }
	
	public function slotbook(Request $request, $id, $date, $booking, $estid){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
		$bookrn = Crypt::decrypt($booking);
		$estid = Crypt::decrypt($estid);
        $est = All::GetRow('et_sport_time', 'id', $estid);
        $esb = All::GetRow('et_sport_book', 'id', $est->fk_et_sport_book);
        $ebf = All::GetAllRow('et_booking_facility', 'id', $esb->fk_et_booking_facility);
        $mb = All::GetRow('main_booking', 'id', $ebf[0]->fk_main_booking);
        $ebf2 = All::GetAllRow('et_booking_facility', 'id', $esb->fk_et_booking_facility)->where('ebf_start_date', $date);
        $allest = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $esb->id);
        $ecb = All::GetAllRow('et_confirm_booking', 'fk_main_booking', $mb->id);

        $slot = $request->post('slot');
        $row = explode(',', $slot[0]);

        if(count($ebf2) == 0){      // No data of date selected/changed slot date
            // dd('t1', $id, $bookrn, $estid, $est, $esb, $ebf, $ebf2, $allest);
            
            // Insert et_booking_faciltiy
            $dataebf = [
                'fk_main_booking'       => $ebf[0]->fk_main_booking,
                'fk_et_facility_type'   => $ebf[0]->fk_et_facility_type,
                'fk_et_facility_detail' => (int)$row[0],
                'fk_lkp_discount'       => $ebf[0]->fk_lkp_discount,
                'ebf_start_date'        => $date,
                'ebf_end_date'          => $date,
                'ebf_no_of_day'         => 1,
                'ebf_subtotal'          => $ebf[0]->ebf_subtotal,
                'ebf_deposit'           => $ebf[0]->ebf_deposit,
                'ebf_facility_indi'     => $ebf[0]->ebf_facility_indi,
                'ebf_total_before_gst'  => $ebf[0]->ebf_total_before_gst,
                'ebf_total_gst'         => $ebf[0]->ebf_total_gst,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('et_booking_facility', $dataebf);
            // $query = 1;

            // Insert et_sport_book
            $dataesb = [
                'fk_et_booking_facility'=> $query,
                'esb_booking_date'      => $date,
                // 'esb_total_hour'        => ,
                // 'esb_total'             => ,
                // 'esb_total_before_gst'  => ,
                // 'esb_total_gst'         => ,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $newEstID = All::InsertGetID('et_sport_book', $dataesb);
            // $newEstID = 1;

            // Delete est based on $estid [soft delete]
            $dataestdlt = [
                'updated_at'            => date('Y-m-d H:i:s'),
                'deleted_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_sport_time', $estid, $dataestdlt);

            // Insert et_sport_time
            $data['est'] = All::GetRow('et_slot_time', 'id', $row[1]);
            $et_slot_price = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6])->id;
            $dataest = [
                'fk_et_sport_book'      => $newEstID,
                'fk_et_slot_price'      => $et_slot_price,
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('et_sport_time', $dataest);        // Insert new if delete

            // Insert et_confirm_booking
            // $dataecb = [
            //     'ecb_date_booking'      => $date,
            //     'updated_at'            => date('Y-m-d H:i:s'),
            //     'updated_by'            => Session::get('user.id')
            // ];
            // $query = All::GetUpdateSpec('et_confirm_booking', 'fk_main_booking', $ebf->fk_main_booking, $dataecb);
              
            // Update et_confirm_booking_detail
            $fk_et_slot_time = All::GetRow('et_slot_price', 'id', $est->fk_et_slot_price)->fk_et_slot_time;
            $ecbdSpec = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $ecb[0]->id)->where('fk_et_slot_time', $fk_et_slot_time)->where('ecbd_date_booking', $ecb[0]->ecb_date_booking)->first();
            // Delete ecbd [soft delete]
            if($ecbdSpec != null){
                $dataecbddlt = [
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'deleted_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                $query = All::GetUpdate('et_confirm_booking_detail', $ecbdSpec->id, $dataecbddlt);
            }
            
            $dataecbd = [
                'fk_et_confirm_booking' => $ecb[0]->id,
                'fk_et_facility_detail' => $ebf[0]->fk_et_facility_detail,
                'fk_et_slot_time'       => $row[1],
                'ecbd_date_booking'     => $date,
                'fk_et_facility_type'   => $ecb[0]->fk_et_facility_type,
                // 'ecb_flag_indicator'    => ,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            // dd($row, $dataebf, $dataesb, $estid, $dataestdlt, $dataest, $dataecbd);
            $query = All::Insert('et_confirm_booking_detail', $dataecbd);   

        } else {
            // dd('t2', $id, $bookrn, $estid, $est, $esb, $ebf, $ebf2, $allest);
            $ecbData = All::GetRow('et_confirm_booking', 'fk_main_booking', $mb->id);
            $esp = All::GetRow('et_slot_price', 'id', $est->fk_et_slot_price);
            // dd($ecbData, $mb->id);
            $ecbdData = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $ecbData->id)->where('fk_et_slot_time', $esp->fk_et_slot_time)->first();

            $data['esp'] = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6])->id;
            $dataest = [
                'fk_et_sport_book'      => $esb->id,
                'fk_et_slot_price'      => $data['esp'],
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_sport_time', $estid, $dataest); 

            if($ecbdData){
                // Update et_confirm_booking_detail
                $dataecbd = [
                    'fk_et_facility_detail' => (int)$row[0],
                    'fk_et_slot_time'       => (int)$row[1],
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                // dd($dataest, $dataecbd, $ecbdData);
                $query = All::GetUpdate('et_confirm_booking_detail', $ecbdData->id, $dataecbd);  // Update the row instead of delete
            }
        }

        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $bookrn);
        $location = All::GetRow('et_facility_type', 'id', $id);

        Mail::reschedule_email($data['main']->id);


        if ($query) {
            AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Tukar Slot Masa Kegunaan : '.$row[1], 1);
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('sport/reschedule/sport', [Crypt::encrypt($bookrn)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

	public function slothall(Request $request, $type, $id, $date, $booking, $estid){
        $type = Crypt::decrypt($type);
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
		$data['booking'] = Crypt::decrypt($booking);
		$data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
        $ebf = All::GetRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $ehb = All::GetRow('et_hall_book', 'fk_et_booking_facility', $ebf->id);

        $data['estid'] = Crypt::decrypt($estid);
        $data['id'] = $id;
        if(request()->isMethod('post')){
            $date = date('Y-m-d', strtotime($request->post('tarikh')));
        }
        
        $data['date'] = $date;
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);
        $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id);
		$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
		$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $et_func = $ehb->fk_et_function;
        $data['slot'] = Sport::slot($id, $data['date'], $et_func);
        $data['slot2'] = Sport::slotDewan($id, $date);
        // dd($data);
        
        AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Buka paparan Penjadualan bagi Sukan', 1);
        return view('sport.reschedule.slothall', compact('data'));
    }
    
    public function slotbookhall(Request $request, $id, $date, $booking, $estid) {
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
		$bookrn = Crypt::decrypt($booking);
		$estid = Crypt::decrypt($estid);
        $eht = All::GetRow('et_hall_time', 'id', $estid);
        $ehb = All::GetRow('et_hall_book', 'id', $eht->fk_et_hall_book);
        $ebf = All::GetAllRow('et_booking_facility', 'id', $ehb->fk_et_booking_facility);
        $mb = All::GetRow('main_booking', 'id', $ebf[0]->fk_main_booking);
        $ebf2 = All::GetAllRow('et_booking_facility', 'id', $ehb->fk_et_booking_facility)->where('ebf_start_date', $date);
        $allest = All::GetAllRow('et_hall_time', 'fk_et_hall_book', $ehb->id);
        $ecb = All::GetAllRow('et_confirm_booking', 'fk_main_booking', $mb->id);

        $slot = $request->post('slot');
        $row = explode(',', $slot[0]);

        if(count($ebf2) == 0){      // No data of date selected/changed slot date
            // dd('t1', $id, $date, $estid, $eht, $ehb, $ebf, $ebf2, $allest, $request->post());

            // Insert et_booking_faciltiy with new date
            $dataebf = [
                'fk_main_booking'       => $ebf[0]->fk_main_booking,
                'fk_et_facility_type'   => $ebf[0]->fk_et_facility_type,
                'fk_et_facility_detail' => (int)$row[0],
                'fk_lkp_discount'       => $ebf[0]->fk_lkp_discount,
                'ebf_start_date'        => $date,
                'ebf_end_date'          => $date,
                'ebf_no_of_day'         => 1,
                'ebf_subtotal'          => $ebf[0]->ebf_subtotal,
                'ebf_deposit'           => $ebf[0]->ebf_deposit,
                'ebf_facility_indi'     => $ebf[0]->ebf_facility_indi,
                'ebf_total_before_gst'  => $ebf[0]->ebf_total_before_gst,
                'ebf_total_gst'         => $ebf[0]->ebf_total_gst,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('et_booking_facility', $dataebf);
            // $query = 1;

            // Insert et_hall_book with new date, copy fk_et_function
            $dataehb = [
                'fk_et_booking_facility'=> $query,
                'fk_et_function'        => $ehb->fk_et_function,
                'ehb_booking_date'      => $date,
                'ehb_total_hour'        => $ehb->ehb_total_hour,
                'ehb_total'             => $ehb->ehb_total,
                'ehb_total_before_gst'  => $ehb->ehb_total_before_gst,
                'ehb_total_gst'         => $ehb->ehb_total_gst,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $newEstID = All::InsertGetID('et_hall_book', $dataehb);
            // $newEstID = 1;

            // Delete eht based on $estid [soft delete]
            $dataehtdlt = [
                'updated_at'            => date('Y-m-d H:i:s'),
                'deleted_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_hall_time', $estid, $dataehtdlt);

            // Insert et_hall_time
            $fk_et_slot_time = All::GetRow('et_facility_price', 'id', $row[6])->fk_et_slot_time;
            $dataeht = [
                'fk_et_hall_book'       => $newEstID,
                'fk_et_slot_time'       => $fk_et_slot_time,
                'fk_et_facility_price'  => (int)$row[6],
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::InsertGetID('et_hall_time', $dataeht);        // Insert new if delete

            // Insert et_confirm_booking
            // $dataecb = [
            //     'fk_main_booking'       => $ebf[0]->fk_main_booking,
            //     'fk_et_facility_type'   => $ecb[0]->fk_et_facility_type,
            //     'ecb_date_booking'      => $date,
            //     // 'ecb_flag_indicator'    => ,
            //     'updated_at'            => date('Y-m-d H:i:s'),
            //     'updated_by'            => Session::get('user.id')
            // ];
            // $ecbid = All::InsertGetID('et_confirm_booking', $dataecb);        // Insert new if delete
            // $ecbid = 1;

            // Update et_confirm_booking_detail
            $ehtestSpec = All::GetAllRow('et_hall_time', 'id', $estid)->first();
            $ecbdSpec = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $ecb[0]->id)->where('fk_et_slot_time', $ehtestSpec->fk_et_slot_time)->where('ecbd_date_booking', $ecb[0]->ecb_date_booking)->first();
            // Delete ecbd [soft delete]
            $dataecbddlt = [
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
                'deleted_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_confirm_booking_detail', $ecbdSpec->id, $dataecbddlt);

            $dataecbd = [
                'fk_et_confirm_booking' => $ecb[0]->id,
                'fk_et_facility_detail' => $ebf[0]->fk_et_facility_detail,
                'fk_et_slot_time'       => $fk_et_slot_time,
                'ecbd_date_booking'     => $date,
                'fk_et_facility_type'   => $ecb[0]->fk_et_facility_type,
                // 'ecb_flag_indicator'    => ,
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            // dd($row, $dataebf, $dataehb, $estid, $dataehtdlt, $dataeht, $dataecbd, $ecbdSpec);
            $query = All::Insert('et_confirm_booking_detail', $dataecbd);        // Insert new if delete
            
        } else {
            $ecbData = All::GetRow('et_confirm_booking', 'fk_main_booking', $mb->id);
            $ecbdData = All::GetAllRow('et_confirm_booking_detail', 'fk_et_confirm_booking', $ecbData->id)->where('fk_et_slot_time', $eht->fk_et_slot_time)->first();

            // Update et_hall_time
            $dataest = [
                'fk_et_slot_time'       => (int)$row[1],
                'fk_et_facility_price'  => $row[6],
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_hall_time', $estid, $dataest);  // Update the row instead of delete

            // Update et_confirm_booking_detail
            $dataecbd = [
                'fk_et_slot_time'       => (int)$row[1],
                'updated_at'            => date('Y-m-d H:i:s'),
                'updated_by'            => Session::get('user.id')
            ];
            $query = All::GetUpdate('et_confirm_booking_detail', $ecbdData->id, $dataecbd);  // Update the row instead of delete
        }

        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $bookrn);
        $location = All::GetRow('et_facility_type', 'id', $id);

        Mail::reschedule_email($data['main']->id);
        if ($query) {
            AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Tukar Slot Masa Kegunaan : '.$row[1], 1);
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('sport/reschedule/sport', [Crypt::encrypt($bookrn)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function delete(Request $request, $id, $esbid, $mbid){
        $id = Crypt::decrypt($id);
        $esbid = Crypt::decrypt($esbid);
        $mbid = Crypt::decrypt($mbid);

        $data['et_sport_time'] = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $esbid);
        
        $deleteSlot = [
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_sport_time', 'id', $id);
        // If only 1 row of time slot, delete booking
        if(count($data['et_sport_time']) == 1){
            $deleteSlot = [
                'deleted_at'    => date('Y-m-d H:i:s'),
                'deleted_by'    => Session::get('user.id')
            ];
            $query = All::GetUpdate('main_booking', 'id', $mbid);
            
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport'));
        }
        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(30), Crypt::encrypt($id), 'Batal Tempahan = ' . $id,1);
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('sport/reschedule/sport', [Crypt::encrypt($bookrn)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

}

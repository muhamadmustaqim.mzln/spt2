<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class DetailsFacilityController extends Controller
{
    public function show() {
        // $data['list'] = All::Show('et_facility_detail', 'updated_at', 'DESC');
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['list'] = All::Show('et_facility_detail', 'updated_at', 'DESC');
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $SportLocation = (array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]));
            foreach($SportLocation as $key => $value){
                $SportLocation[$key] = HP::role_location($value);
            }
            $data['list'] = All::Show('et_facility_detail', 'updated_at', 'DESC');
            foreach($data['list'] as $key => $value){
                $lkp_loc = HP::lkp_loc_from_eft($value->fk_et_facility_type);
                if($lkp_loc != ""){
                    if(in_array($lkp_loc->id, $SportLocation)){
                        $data['list'][$key]->fk_lkp_location = $lkp_loc->id;
                    } else {
                        unset($data['list'][$key]);
                    }
                } else {
                    unset($data['list'][$key]);
                }
            }
            // $data['facility'] = All::Show('et_facility_type', 'updated_at', 'DESC')->whereIn('fk_lkp_location', $SportLocation);
        } else {
            return redirect('/');
        }

        return view('sport.facilitydetail.lists', compact('data'));
    }

    public function form(){
        $data['list'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC')->sortBy('eft_type_desc');
        $data['rent'] = All::Show('lkp_slot_rent', 'updated_at', 'DESC');

        return view('sport.facilitydetail.form', compact('data'));
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'efd_name'              => request()->nama,
            'fk_et_facility_type'   => request()->jenis,
            'efd_rent_cat'          => request()->sewa,
            'efd_fee_code'          => request()->kod,
            'efd_status'             => $status,
            'updated_by'            => Session::get('user')['id'],
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_facility_detail', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(165), Crypt::encrypt($query), 'Sukan - Tambah Fasiliti Terperinci = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['jenis'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC');
        $data['rent'] = All::Show('lkp_slot_rent', 'updated_at', 'DESC');
        $data['list'] = All::GetRow('et_facility_detail', 'id', $id);

        return view('sport.facilitydetail.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'efd_name'              => request()->nama,
            'fk_et_facility_type'   => request()->jenis,
            'efd_rent_cat'          => request()->sewa,
            'efd_fee_code'          => request()->kod,
            'efd_status'             => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(166), Crypt::encrypt($id), 'Sukan - Kemas kini Fasiliti Terperinci = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(167), Crypt::encrypt($id), 'Sukan - Padam Fasiliti Terperinci = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitydetail'));
        }
    }
}

<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class EquipmentController extends Controller
{
    public function show() {
        // $data['list'] = All::Show('et_equipment', 'updated_at', 'DESC');
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['list'] = All::Show('et_equipment', 'updated_at', 'DESC');
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $data['list'] = All::Show('et_equipment', 'updated_at', 'DESC')->whereIn('fk_lkp_location', Session::get('user.roles'));
        } else {
            return redirect('/');
        }

        return view('sport.equipment.lists', compact('data'));
    }

    public function form(){
        $data['location'] = All::Show('lkp_location', 'updated_at', 'DESC')->where('lc_type', 2)->sortBy('lc_description');

        return view('sport.equipment.form', compact('data'));
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'ee_name'           => request()->nama,
            'ee_quantity'       => request()->kuantiti,
            'ee_fee_code'       => request()->kod,
            'ee_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_equipment', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(126), Crypt::encrypt($query), 'Tambah Peralatan = '. $query,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::Show('lkp_location', 'updated_at', 'DESC')->where('lc_type', 2)->sortBy('lc_description');
        $data['list'] = All::GetRow('et_equipment', 'id', $id);

        return view('sport.equipment.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'fk_lkp_location'   => request()->lokasi,
            'ee_name'           => request()->nama,
            'ee_quantity'       => request()->kuantiti,
            'ee_fee_code'       => request()->kod,
            'ee_status'         => $status,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(128), Crypt::encrypt($id), 'Kemas kini Peralatan = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(127), Crypt::encrypt($id), 'Padam Peralatan = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipment'));
        }
    }
}

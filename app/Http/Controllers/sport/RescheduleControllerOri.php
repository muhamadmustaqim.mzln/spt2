<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\MailController as Mail;
use App\Models\All;
use App\Models\Auth;
use App\Models\Sport;
use App\Models\AuditLog;

class RescheduleController extends Controller
{
    public function index(Request $request) {
        $data['takwim'] = All::Show('lkp_holiday', 'lh_start_date', 'DESC');
		
		$data['post'] = false;
        $data['id'] = "";
		
        if(request()->isMethod('post')){
			
            $data['post'] = true;
            $data['id'] = $request->post('id');
			
			//$data['booking'] = Crypt::decrypt($booking);
			//$data['main'] = All::GetAllRow('main_booking', 'bmb_booking_no', $data['id'], 'bmb_booking_no', 'DESC');
            if(substr($data['id'], 0, 3) == 'SPS') {
                $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['id']);
            }

            if (isset($data['main']->fk_users))
			{
				$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
			}
			//$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
			
        }
		// dd($data);
        return view('sport.reschedule.index', compact('data'));
    }
	
	public function sport($booking) {
		$data['booking'] = Crypt::decrypt($booking);
		$data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
		$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
		$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
		$data['slot'] = Sport::priceSlot($data['main']->id);
		$data['slot2'] = Sport::priceSlot2($data['main']->id);
        // dd($data);

        return view('sport.reschedule.reschedule', compact('data'));
    }
	
	public function slot(Request $request, $id, $date, $booking, $estid){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $data['estid'] = Crypt::decrypt($estid);
        $data['id'] = $id;
        if(request()->isMethod('post')){
            $date = date('Y-m-d', strtotime($request->post('tarikh')));
            // dd($data, $date, $request->post(), $date2);
        }
        $data['date'] = $date;
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $id);
        $data['details'] = All::Show('et_facility_detail', 'id', 'ASC')->where('fk_et_facility_type', $id);
		$data['booking'] = Crypt::decrypt($booking);
		$data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $data['booking']);
		$data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
		$data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::slot($id, $date);
        $data['slot2'] = Sport::slotDewan($id, $date);
        
        AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Buka paparan Penjadualan bagi Sukan', 1);
		
        return view('sport.reschedule.slot', compact('data'));
    }
	
	public function slotbook(Request $request, $id, $date, $booking, $estid){
        //dd($request->all());
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
		$bookrn = Crypt::decrypt($booking);
		$estid = Crypt::decrypt($estid);
        $est = All::GetRow('et_sport_time', 'id', $estid);
        $esb = All::GetRow('et_sport_book', 'id', $est->fk_et_sport_book);
        $ebf = All::GetRow('et_booking_facility', 'id', $esb->fk_et_booking_facility);

        //No Booking Sukan
        //$running =  Sport::getRn();
        //$rn = sprintf( '%05d', $running);
        //$year = date('y');
        //$md = date('md');
        //$bookrn='SPS'.$year.$md.$rn;

        $data['main'] = All::GetRow('main_booking', 'bmb_booking_no', $bookrn);
        $location = All::GetRow('et_facility_type', 'id', $id);

		/*
        $data = array(
            'fk_users'                   => Session::get('user')['id'],
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('main_booking', $data);
		*/
		
        $dataebf = [
            'ebf_start_date'        => $date,
            'ebf_end_date'          => $date,
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_booking_facility', $ebf->id, $dataebf);
        $dataesb = [
            'esb_booking_date'      => $date,
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_sport_book', $esb->id, $dataesb);
        $dataecb = [
            'ecb_date_booking'      => $date,
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdateSpec('et_confirm_booking', 'fk_main_booking', $ebf->fk_main_booking, $dataecb);

        $slot = $request->post('slot');
        $varCompLocation = 0;
        $i = 0;
        
        $row = explode(',', $slot[0]);
        $data['esp'] = All::GetRow('et_slot_price', 'id', $row[1])->fk_et_facility_price;
        $dataest = [
            'fk_et_slot_price'      => $data['esp'],
            'updated_at'            => date('Y-m-d H:i:s'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_sport_time', $estid, $dataest);

        Mail::reschedule_email($data['main']->id);

        // foreach($slot as $s){
        //     $row = explode(',', $s);
			
        //     if($row[0] != $varCompLocation){
				
		// 		echo $row[1];
		// 		$query = All::GetUpdateReschedule($bookrn, $row[1]);
				
		// 		/* Book start date and end date
        //         $varCompLocation = $row[0];				
        //         $data_book[$i] = array(
        //             'fk_main_booking'            => $query,
        //             'fk_et_facility_type'        => $id,
        //             'fk_et_facility_detail'      => $varCompLocation,
        //             'fk_lkp_discount'            => 1,
        //             'ebf_start_date'             => $row[2],
        //             'ebf_end_date'               => $row[2],
        //             'ebf_no_of_day'              => 1,
        //             'created_at'                 => date('Y-m-d H:i:s'),
        //             'updated_at'                 => date('Y-m-d H:i:s')
        //         );
        //         $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);
		// 		*/ 
				
		// 		/*Sports Date
        //         $data_sport[$i] = array(
        //             'fk_et_booking_facility'     => $etBookFacility,
        //             'esb_booking_date'           => $row[2],
        //             'created_at'                 => date('Y-m-d H:i:s'),
        //             'updated_at'                 => date('Y-m-d H:i:s')
        //         );
        //         $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
		// 		*/
        //     }
		// 	/*
        //     $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
        //     $total_gst = $row[4] * $gst->lgr_rate;
        //     $total = $row[4] + $total_gst;

        //     $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);

        //     $data_time[$i] = array(
        //         'fk_et_sport_book'           => $etSportBook,
        //         'fk_et_slot_price'           => $slotPrice->id,
        //         'est_price'                  => $row[4],
        //         'est_discount_type_rm'       => 0.00,
        //         'est_discount_rm'            => 0.00,
        //         'est_total'                  => $row[4],
        //         'est_gst_code'               => $row[5],
        //         'est_gst_rm'                 => $total_gst,
        //         'est_subtotal'               => $total,
        //         'created_at'                 => date('Y-m-d H:i:s'),
        //         'updated_at'                 => date('Y-m-d H:i:s')
        //     );
        //     $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
		// 	*/
        //     $i++;
        // }

        if ($query) {
            AuditLog::log(Crypt::encrypt(91), Crypt::encrypt($data['main']->id), 'Tukar Slot Masa Kegunaan : '.$data['esp'], 1);
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('sport/reschedule/sport', [Crypt::encrypt($bookrn)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function delete(Request $request, $id, $esbid, $mbid){
        $id = Crypt::decrypt($id);
        $esbid = Crypt::decrypt($esbid);
        $mbid = Crypt::decrypt($mbid);

        $data['et_sport_time'] = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $esbid);
        
        $deleteSlot = [
            'deleted_at'    => date('Y-m-d H:i:s'),
            'deleted_by'    => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_sport_time', 'id', $id);
        // If only 1 row of time slot, delete booking
        if(count($data['et_sport_time']) == 1){
            $deleteSlot = [
                'deleted_at'    => date('Y-m-d H:i:s'),
                'deleted_by'    => Session::get('user.id')
            ];
            $query = All::GetUpdate('main_booking', 'id', $mbid);
            
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport'));
        }
        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(30), Crypt::encrypt($id), 'Batal Tempahan = ' . $id,1);
			Session::flash('flash', 'Updated'); 
            return Redirect::to(url('sport/reschedule/sport', [Crypt::encrypt($bookrn)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }
}

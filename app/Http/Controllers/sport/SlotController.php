<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class SlotController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_slot_time', 'updated_at', 'DESC');
        return view('sport.slot.lists', compact('data'));
    }

    public function form(){
        $data['list'] = All::Show('lkp_slot_cat', 'updated_at', 'DESC');
        $data['rent'] = All::Show('lkp_slot_rent', 'updated_at', 'DESC');
        return view('sport.slot.form', compact('data'));
    }

    public function add(Request $request){
        $data = array(
            'est_slot_time'         => request()->nama,
            'fk_lkp_slot_cat'       => request()->jenis,
            'est_rent_cat'          => request()->sewa,
            'updated_by'            => Session::get('user')['id'],
            'created_at'            => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_slot_time', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(150), Crypt::encrypt($query), 'Sukan - Tambah Slot Masa = ' . request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/slot'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/slot'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['jenis'] = All::Show('lkp_slot_cat', 'updated_at', 'DESC');
        $data['rent'] = All::Show('lkp_slot_rent', 'updated_at', 'DESC');
        $data['list'] = All::GetRow('et_slot_time', 'id', $id);
        return view('sport.slot.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'est_slot_time'     => request()->nama,
            'fk_lkp_slot_cat'   => request()->jenis,
            'est_rent_cat'      => request()->sewa,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_slot_time', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(151), Crypt::encrypt($id), 'Sukan - Kemas kini Slot Masa = ' . request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/slot'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/slot'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_slot_time', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(152), Crypt::encrypt($id), 'Sukan - Padam kini Slot Masa = ' . $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/slot'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/slot'));
        }
    }
}

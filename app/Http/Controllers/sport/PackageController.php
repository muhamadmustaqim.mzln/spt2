<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class PackageController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_package_detail', 'updated_at', 'ASC');
        return view('sport.package.lists', compact('data'));
    }

    public function form(){
        $data['list'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 1);
        $data['package'] = All::Show('et_package', 'id', 'ASC');
        $data['time'] = All::Show('et_slot_time', 'updated_at', 'ASC');

        return view('sport.package.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_package'           => request()->pakej,
            'fk_et_slot_time'           => request()->masa,
            'fk_et_function'           => request()->fungsi,
            'epd_price'           => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_package_detail', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(6), Crypt::encrypt($query), 'Tambah Pakej = '. request()->pakej,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/package'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/package'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['function'] = All::Show('et_function', 'id', 'ASC')->where('ef_type', 1);
        $data['package'] = All::Show('et_package', 'id', 'ASC');
        $data['time'] = All::Show('et_slot_time', 'updated_at', 'ASC');
        $data['list'] = All::GetRow('et_package_detail', 'id', $id);

        return view('sport.package.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_package'           => request()->pakej,
            'fk_et_slot_time'           => request()->masa,
            'fk_et_function'           => request()->fungsi,
            'epd_price'           => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_package_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(7), Crypt::encrypt($id), 'Kemas kini Pakej = '. request()->pakej,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/package'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/package'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_package_detail', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(140), Crypt::encrypt($id), 'Padam Rekod Pakej = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/package'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/package'));
        }
    }
}

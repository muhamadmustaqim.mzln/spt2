<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class TypePackageController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_package', 'updated_at', 'DESC');
        return view('sport.packagetype.lists', compact('data'));
    }

    public function form(){
        return view('sport.packagetype.form');
    }

    public function add(Request $request){

        $data = array(
            'ep_name'           => request()->nama,
            'ep_day_cat'        => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_package', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(159), Crypt::encrypt($query), 'Sukan - Tambah Jenis Pakej = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('et_package', 'id', $id);
        return view('sport.packagetype.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'ep_name'           => request()->nama,
            'ep_day_cat'        => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_package', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(160), Crypt::encrypt($id), 'Sukan - Kemas kini Jenis Pakej = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_package', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(161), Crypt::encrypt($id), 'Sukan - Padam Jenis Pakej = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/packagetype'));
        }
    }
}

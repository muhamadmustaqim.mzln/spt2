<?php

namespace App\Http\Controllers\sport;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\AuditLog;
use App\Models\Sport;
use App\Models\Auth;
use App\Models\Hall;
use App\Models\All;
use Carbon\Carbon;

class ExternalController extends Controller
{
    public function index(Request $request) {
        $data['post'] = false;
        $data['id'] = "";
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['id'] = $request->post('nama');
            $data['user'] = Auth::user($data['id']);
            $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($data['post']), 'Tempah Sukan - Tempahan Luaran = '. $request->post('nama') ,null);
        }
        $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($data['post']), 'Tempah Sukan - Tempahan Luaran',null);
        
        return view('sport.external.index', compact('data'));
    }

    public function hall(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['post'] = false;
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['id']);
        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['userId'] = $data['id'];
            $data['id'] = $request->post('lokasi');
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['id']);
            $data['date'] = $request->post('tarikh');
            $startDate = Carbon::createFromFormat('d-m-Y', $data['date'])->toDateString();
            $endDate = Carbon::createFromFormat('d-m-Y', $data['date'])->toDateString();
            $data['availableList'] = Sport::getAvailableDewanList($startDate, $endDate, $data['id']);
            $data['bookedList'] = Sport::getBookeddewanlist($startDate, $endDate, $data['id']);
            $data['start'] = Carbon::createFromFormat('Y-m-d', $startDate)->format('d-m-Y');
            $data['end'] = Carbon::createFromFormat('Y-m-d', $endDate)->format('d-m-Y');
        }
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id','!=',1)->sortBy('lc_description');
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        return view('sport.external.hall', compact('data'));
    }

    public function purpose(Request $request, $id, $date, $userId){
        $data['id'] = Crypt::decrypt($id);
        $data['date'] = Crypt::decrypt($date);
        $data['userId'] = Crypt::decrypt($userId);
        $data['post'] = true;

        $data['category'] = All::GetAllRow('lkp_discount_type', 'ldt_indicator', 1);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id','!=',1);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $data['function'] = All::Show('et_function', 'id', 'ASC');
        $data['type'] = Sport::getCheckfacilitytype($data['id']);
        $data['checkroom'] = Sport::getCheckroom($data['id']);
        $data['lkp_location'] = $data['checkroom'][0]->fk_lkp_location;

        foreach ($data['checkroom'] as $key => $value) {
            $data['result'] = $value->fk_et_facility;
        }

        if(request()->isMethod('post')){
            if(request()->multipurpose == 0){
                if(isset(request()->et_function_id)){
                    $et_facility_type_id = (int)request()->et_facility_type_id;
                    $et_function_id = request()->et_function_id;
                    $type = 1;
                    $data['function'] = All::GetRow('et_function', 'id', $et_function_id);
                } else if(isset(request()->et_facility_type_id)){
                    $et_facility_type_id = (int)request()->et_facility_type_id;
                    $et_function_id = 13;
                    $type = 2;
                } else {
                    return redirect()->to('/sport/external/hall/'. Crypt::encrypt($data['userId']));
                }
                $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
                $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
                $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
                $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
                $type = 1; // 1 = et_function, 2 = et_facility_type
                $data['et_facility_detail'] = All::GetRow('et_facility_detail', 'fk_et_facility_type', $et_facility_type_id);
                
                $lkp_location = $data['hall']->fk_lkp_location;

                // INSERT::main_booking
                $running =  Sport::getRn();
                $rn = sprintf( '%05d', $running);
                $year = date('y');
                $md = date('md');
                $bookrn='SPS'.$year.$md.$rn;
                $dataMainBooking = [
                    'fk_users'              => $data['userId'],
                    'fk_lkp_status'         => 1,
                    'fk_lkp_deposit_rate'   => All::GetRow('lkp_deposit_rate', 'fk_lkp_location', $lkp_location)->id,
                    'fk_lkp_location'       => $lkp_location,
                    'fk_lkp_discount_type'  => request()->JenisAcaraTempDewan, 
                    'bmb_booking_no'        => $bookrn,
                    'bmb_staff_id'          => request()->staffid,
                    'bmb_kementerian'       => request()->kementerianjabatan,
                    'bmb_agensi'            => request()->bmb_agensi,
                    'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
                    'bmb_type_user'         => 2,
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d'),
                ];
                $query = All::InsertGetID('main_booking', $dataMainBooking);
                $mbid = $query;
                // INSERT::main_booking

                // INSERT::main_booking
                if($request->hasFile('suratAkuan')){
                    $file = $request->file('suratAkuan');
                    $fileName = $file->getClientOriginalName();
                    $fileExtension = $file->getClientOriginalExtension();
                    $fileSize = $file->getSize();
        
                    $newFileName = 'Dokumen Sokongan_' . $bookrn . '.' . $fileExtension;
        
                    // $path = public_path('dokumen/sukan/' . $bookrn);
                    $path = public_path('upload_document/mainbooking/' . $mbid);
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file->move(public_path('upload_document/mainbooking/' . $mbid), $newFileName);
                    // $file->move(public_path('dokumen/sukan/' . $bookrn), $newFileName);
                    $fullPath = 'upload_document/mainbooking/' . $mbid . '/' . $newFileName;
        
                    $file_data = [
                        'fk_main_booking'       => $mbid,
                        'ba_date'               => date('Y-m-d H:i:s'),
                        'ba_dir'                => 'upload_document',
                        'ba_full_path'          => $fullPath,
                        'ba_file_name'          => $newFileName,
                        'ba_file_ext'           => $fileExtension,
                        'ba_file_size'          => $fileSize,
                        'ba_generated_name'     => 'Dokumen Sokongan',
                        'ba_status'             => null,
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                        'updated_by'            => Session::get('user.id')
                    ];
                    $ea = All::InsertGetID('bh_attachment', $file_data);
                    // $file_data = [
                    //     'fk_main_booking'       => $mbid,
                    //     'eta_date'              => date('Y-m-d H:i:s'),
                    //     'eta_dir'               => 'dokumen/sukan/' . $bookrn,
                    //     // 'eta_full_path'         => ,
                    //     'eta_file_name'         => $newFileName,
                    //     'eta_file_ext'          => $fileExtension,
                    //     'eta_file_size'         => $fileSize,
                    //     'eta_generated_name'    => 'Dokumen Sokongan',
                    //     'eta_status'            => 1,
                    //     'created_at'            => date('Y-m-d H:i:s'),
                    //     'updated_at'            => date('Y-m-d H:i:s'),
                    //     'updated_by'            => Session::get('user.id')
                    // ];
                    // $ea = All::InsertGetID('et_attachment', $file_data);
                }

                $dataEtBookingFacility = [
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $et_facility_type_id,
                    'fk_et_facility_detail'      => $data['et_facility_detail']->id,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
                    'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
                    'ebf_no_of_day'              => 1,
                    'ebf_facility_indi'         => $data['et_facility']->ef_type,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                ];
                $etfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);

                $unixTimestamp = strtotime($request->post('date'));
                $mysqlDate = date('Y-m-d', $unixTimestamp);

                $dataEtHallBook = [
                    'fk_et_booking_facility'    => $etfid,
                    'fk_et_function'            => $et_function_id,
                    'ehb_booking_date'          => $mysqlDate,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                ];
                $id = All::InsertGetID('et_hall_book', $dataEtHallBook);

                return redirect('sport/external/slot/' . Crypt::encrypt($mbid));
            } else {
                $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
                $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
                $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
                $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
                $type = 1; // 1 = et_function, 2 = et_facility_type

                $function = request()->fn[0];
                $detail = request()->gelanggang;

                $running =  Sport::getRn();
                $rn = sprintf( '%05d', $running);
                $year = date('y');
                $md = date('md');
                $bookrn='SPS'.$year.$md.$rn;
                
                $dataMainBooking = [
                    'fk_users'              => $data['userId'],
                    'fk_lkp_status'         => 1,
                    'fk_lkp_location'       => $data['checkroom'][0]->fk_lkp_location,
                    'fk_lkp_discount_type'  => 5, 
                    'bmb_booking_no'        => $bookrn,
                    'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
                    'bmb_type_user'         => 2,
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d'),
                ];
                $mbid = All::InsertGetID('main_booking', $dataMainBooking);

                foreach ($detail as $key => $value) {
                    $dataEtBookingFacility = [
                        'fk_main_booking'            => $mbid,
                        'fk_et_facility_type'        => All::GetRow('et_facility_detail', 'id', $value)->fk_et_facility_type,
                        'fk_et_facility_detail'      => $value,
                        'fk_lkp_discount'            => 1,
                        'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
                        'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
                        'ebf_no_of_day'              => 1,
                        'ebf_facility_indi'         => $data['et_facility']->ef_type,
                        'created_at'                 => date('Y-m-d H:i:s'),
                        'updated_at'                 => date('Y-m-d H:i:s')
                    ];
                    $ebfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);

                    $dataEtHallBook = [
                        'fk_et_booking_facility'        => $ebfid,
                        'fk_et_function'                => $function,
                        'ehb_booking_date'              => $data['date'],
                        'created_at'                    => date('Y-m-d H:i:s'),
                        'updated_at'                    => date('Y-m-d H:i:s'),
                    ];
                    $ehbid = All::InsertGetID('et_hall_book', $dataEtHallBook);

                    $date = Carbon::createFromFormat('d-m-Y', $data['date']);
                    if ($date->isWeekend()) {
                        $day_cat = 2;
                    } else {
                        $day_cat = 1;
                    }
                    $data_efp = All::GetAllRow('et_facility_price', 'fk_et_facility', $data['result'])->where('fk_et_function', $function)->where('efp_day_cat', $day_cat)->first();
                    $data_lkp_gst = All::GetAllRow('lkp_gst_rate', 'id', $data_efp->fk_lkp_gst_rate)->first();

                    // $dataEtHallTime = [
                    //     'fk_et_hall_book'               => $ehbid,
                    //     'fk_et_slot_time'               => $data_efp->fk_et_slot_time,
                    //     'fk_et_facility_price'          => $data_efp->id,
                    //     'eht_price'                     => $data_efp->efp_unit_price,
                    //     'eht_discount_type_rm'          => 0.00,
                    //     'eht_discount_rm'               => 0.00,
                    //     'eht_total'                     => $data_efp->efp_unit_price,
                    //     'eht_gst_code'                  => $data_lkp_gst->id,
                    //     'eht_gst_rm'                    => ($data_efp->efp_unit_price * $data_lkp_gst->lgr_rate),
                    //     'eht_subtotal'                  => $data_efp->efp_unit_price,
                    //     'created_at'                    => date('Y-m-d H:i:s'),
                    //     'updated_at'                    => date('Y-m-d H:i:s'),
                    // ];
                    // $ehtid = All::InsertGetID('et_hall_time', $dataEtHallTime);
                }

                return redirect('sport/external/slot/' . Crypt::encrypt($mbid));
            }
        }
        if($data['type'] == 1) { // Dewan
            if($data['result'] == 1) {
                $data['slot'] = Sport::getCheckfacilityfunction($data['id']);
                $data['slot1'] = Sport::getCheckfacilityfunction1($data['id'], $data['date']);
                $combinedSlots = array_merge($data['slot'], $data['slot1']);
                $idColumn = array_column($combinedSlots, 'id');
                
                // Remove duplicates based on the 'id' column
                $uniqueSlots = array_unique($idColumn);
                
                // Create a new array with unique elements
                $uniqueCombinedSlots = [];
                foreach ($combinedSlots as $slot) {
                    if (in_array($slot['id'], $uniqueSlots)) {
                        $uniqueCombinedSlots[] = $slot;
                        // Remove the element from $uniqueSlots to ensure uniqueness
                        unset($uniqueSlots[array_search($slot['id'], $uniqueSlots)]);
                    }
                }
                $data['slot1'] = $uniqueCombinedSlots;
            } else {
                $checkfunction = Sport::getCheckfunction($data['id']);
                if($checkfunction == null){
                    $data['slot1'] = array_values(Sport::getChild($data['id']));
                    if(count($data['slot1']) == 0){
                    } else {
                        $data['slot1'] = $data['slot1'][0];
                    }
                } else {
                    if ($data['id'] == 11 ) {
                        $data['slot1'] = Sport::getCheckfacilityfunction($data['id']);
                        // $data['slot1'] = Sport::getCheckfacilityfunction1($data['id']);
                        // $child = $facfunction;
                        // $child2 = $facfunction;
                        $task = 76;
                        $task_desc = 'Paparkan Muka Hadapan Tempahan Bilik - Pilihan Function/Distype. Facility Type : ' . $data['id'];
                        // event('audit', ['', Auth::user()->id, $task, $task_desc]);
                    } 
		            // else if ($data['id'] == 13 || $data['id'] == 29 || $data['id'] == 38){   // 13, 29, 38 = Squash, tak perlu
                    //     // $child = $facdetails;
                    //     // $child2 = $facdetails;
                    //     $task = 76;
                    //     $task_desc = 'Paparkan Muka Hadapan Tempahan Bilik - Pilihan Distype. Facility Type : ' . $data['id'];
                    //     event('audit', ['', Auth::user()->id, $task, $task_desc]);
                    // }
                    else {
                        $data['slot1'] = array_values(Sport::getChildruangcr($data['id'], $data['date']))[0];
                        $data['slot'] = Sport::getCheckruangfunction($data['id']);
                        $data['slot2'] = Sport::getChildruangcr($data['id'], $data['date']);
                        
                        return view('sport.external.multipurpose', compact('data'));
                    }
                }
            }
        } else { // Sukan, boleh disable
            if($data['result'] == 11) {
                // dd("1", $data);
            } else {
                // dd("2", $data);
            }
        }

        return view('sport.external.purpose', compact('data'));
    }
    
    public function slotDewan(Request $request, $bookingId, $hallId = null) {
        $bookingId = Crypt::decrypt($bookingId);
        $data['booking'] = $bookingId;
        $data['main'] = All::GetRow('main_booking', 'id', $bookingId);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'fk_main_booking', $bookingId);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        $data['date'] = $data['main']->bmb_booking_date;
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);

        $et_facility_type = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type)->id;
        $et_facility_detail = All::GetRow('et_facility_detail', 'fk_et_facility_type', $et_facility_type)->id;

        $data['lokasiData'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);
        $data['equipment_int'] = Sport::getEquipdalam($data['lokasiData']->id);
        $data['equipment_ext'] = Sport::getEquipluar($data['lokasiData']->id);

        $data['attachment'] = All::GetRow('bh_attachment', 'fk_main_booking', $data['main']->id);

        if(request()->isMethod('post')) {
            if ($request->hasFile('suratAkuan')) {
                $file = $request->file('suratAkuan');
                $fileName = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                $fileSize = $file->getSize();

                $newFileName = 'Surat Sokongan ' . $data['main']->bmb_booking_no . '.' . $fileExtension;

                // $path = public_path('dokumen/sukan/' . $data['booking']);
                $path = public_path('upload_document/mainbooking/' . $mbid);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $file->move(public_path('upload_document/mainbooking/' . $mbid), $newFileName);
                // $file->move(public_path('dokumen/sukan/' . $data['booking']), $newFileName);
                $fullPath = 'upload_document/mainbooking/' . $mbid . '/' . $newFileName;
    
                $file_data = [
                    'fk_main_booking'       => $mbid,
                    'ba_date'               => date('Y-m-d H:i:s'),
                    'ba_dir'                => 'upload_document',
                    'ba_full_path'          => $fullPath,
                    'ba_file_name'          => $newFileName,
                    'ba_file_ext'           => $fileExtension,
                    'ba_file_size'          => $fileSize,
                    'ba_generated_name'     => 'Dokumen Sokongan',
                    'ba_status'             => null,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                $ea = All::InsertGetID('bh_attachment', $file_data);
                // $file_data = [
                //     'fk_main_booking'       => $data['booking'],
                //     'eta_date'              => date('Y-m-d H:i:s'),
                //     'eta_dir'               => 'dokumen/sukan/' . $data['booking'],
                //     // 'eta_full_path'         => ,
                //     'eta_file_name'         => $newFileName,
                //     'eta_file_ext'          => $fileExtension,
                //     'eta_file_size'         => $fileSize,
                //     'eta_generated_name'    => 'Dokumen Sokongan',
                //     'eta_status'            => 1,
                //     'created_at'            => date('Y-m-d H:i:s'),
                //     'updated_at'            => date('Y-m-d H:i:s'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::InsertGetID('et_attachment', $file_data);
            }

            // begin::Pengasingan data peralatan [Dalaman & Luaran]
            $data['dalamanID'] = [];
            $data['luaranID'] = [];
            // Remove prev equipment booked to save new ones
            $eeb = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id)->whereNull('deleted_at');
            if(count($eeb) > 0){
                foreach($eeb as $key => $eeb){
                    $removePrevEEB = [
                        'deleted_at'    => date('Y-m-d')
                    ];
                    $query = All::GetUpdate('et_equipment_book', $eeb->id, $removePrevEEB);
                }
            }
            // begin::Pengasingan data peralatan [Dalaman & Luaran]
            foreach ($request->post() as $key => $value) {
                if (strpos($key, 'dalaman_') === 0) {
                    if ($value !== null && $value !== 0) {
                        $id = intval(substr($key, strlen('dalaman_')));
                        $data['dalamanID'][$id] = $value;
                    }
                }
            }
            foreach ($request->post() as $key => $value) {
                if (strpos($key, 'luaran_') === 0) {
                    if($value != null && $value != 0){
                        $id = intval(substr($key, strlen('luaran_')));
                        $data['luaranID'][$id] = $value;
                    }
                }
            }
            // end::Pengasingan data peralatan [Dalaman & Luaran]
            
            // begin::Kategori hari [Hari minggu @ Hujung minggu]
            $dayCat = date('w', strtotime($data['date']));
            if($dayCat == 0 || $dayCat == 6){
                $dayCat = 2;
            } else {
                $dayCat = 1;
            }
            // end::Pengasingan data peralatan [Dalaman & Luaran]
    
            // begin::Maklumat Terperinci Acara
            $dataEtBookingFacilityDetail = [
                'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                'fk_lkp_event'              => request()->JenisAcaraTempDewan,
                'ebfd_event_name'           => request()->namaAcara,
                'ebfd_event_desc'           => request()->keteranganAcara,
                'ebfd_others'               => request()->keteranganAcara,
                'ebfd_total_pax'            => request()->vvip + request()->vip + request()->participant,
                'ebfd_user_apply'           => request()->namaPemohon,
                'ebfd_venue'                => All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description,
                // 'ebfd_address'              => request()->,
                'ebfd_contact_no'           => request()->noTel,
                'ebfd_vvip'                 => request()->vvip,
                'ebfd_vip'                  => request()->vip,
                'ebfd_participant'          => request()->participant,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            ];
            $ebfid = All::InsertGetID('et_booking_facility_detail', $dataEtBookingFacilityDetail);
            // end::Maklumat Terperinci Acara

            // begin::Maklumat Kelengkapan Peralatan
            $totalAmount = 0;
            foreach ($data['dalamanID'] as $key => $quantity) {
                $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 8)->where('eep_day_cat', $dayCat)->first();
                $lkp_gst_rate = (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
                $total = $equipmentPrice->eep_unit_price * $quantity;
                $totalAmount += $total;
                $dataebfd = array(
                    'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                    'fk_et_function'            => 8,
                    'fk_et_equipment'           => $key,
                    'fk_et_equipment_price'     => $equipmentPrice->id,
                    'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                    'eeb_booking_date'          => date('Y-m-d', strtotime($data['date'])),
                    'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                    'eeb_quantity'              => $quantity,
                    'eeb_total_price'           => $total,
                    'eeb_discount_type_rm'      => 0.00,
                    'eeb_discount_rm'           => 0.00,
                    // 'eeb_special_disc'          => ,
                    // 'eeb_special_disc_rm'       => ,
                    'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                    'eeb_gst_rm'                => $total * $lkp_gst_rate,
                    'eeb_subtotal'              => $total,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );
                $query = All::InsertGetID('et_equipment_book', $dataebfd);
            }
            foreach ($data['luaranID'] as $key => $quantity) {
                $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 9)->where('eep_day_cat', $dayCat)->first();
                // dd($key, $quantity, $dayCat, $equipmentPrice);
                $lkp_gst_rate = (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
                $total = $equipmentPrice->eep_unit_price * $quantity;
                $totalAmount += $total;
                $dataebfd = array(
                    'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                    'fk_et_function'            => 9,
                    'fk_et_equipment'           => $key,
                    'fk_et_equipment_price'     => $equipmentPrice->id,
                    'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                    'eeb_booking_date'          => date('Y-m-d', strtotime($data['date'])),
                    'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                    'eeb_quantity'              => $quantity,
                    'eeb_total_price'           => $total,
                    'eeb_discount_type_rm'      => 0.00,
                    'eeb_discount_rm'           => 0.00,
                    // 'eeb_special_disc'          => ,
                    // 'eeb_special_disc_rm'       => ,
                    'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                    'eeb_gst_rm'                => $total * $lkp_gst_rate,
                    'eeb_subtotal'              => $total,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );
                $query = All::InsertGetID('et_equipment_book', $dataebfd);
            }
            // end::Maklumat Kelengkapan Peralatan

            // begin::Maklumat Sebut Harga
            $quono = Sport::generatequono();
            $presint = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description;
            $databq = array(
                'fk_main_booking'           => $data['main']->id,
                'fk_users'                  => $data['main']->fk_users,
                'fk_lkp_discount_type'      => 5,
                'bq_quotation_no'           => 'PPj/' .$presint.'/'.$data['main']->bmb_booking_no.'/'.$quono,
                'bq_quotation_date'         => $data['date'],
                'bq_quotation_status'       => 1,
                'bq_total_amount'           => $totalAmount,
                'bq_payment_status'         => 0,
                'bq_quotation_date'           => date('Y-m-d H:i:s'),
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $bqid = All::InsertGetID('bh_quotation', $databq);

            $data['et_eq_updated'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
            foreach ($data['et_eq_updated'] as $key => $value) {
                $dataeqdetail = [
                    'fk_bh_quotation'           => $bqid,
                    'fk_et_booking_facility'    => $value->fk_et_booking_facility,
                    // 'fk_et_facility_detail'     => ,
                    'fk_et_equipment'           => $value->fk_et_equipment,
                    'fk_et_equipment_book'      => $value->id,
                    'fk_lkp_gst_rate'           => $value->fk_lkp_gst_rate,
                    // 'product_indicator'         => ,
                    'booking_date'              => $value->eeb_booking_date,
                    'unit_price'                => $value->eeb_unit_price,
                    'quantity'                  => $value->eeb_quantity,
                    // 'code_gst'                  => $value->,
                    'gst_amount'                => $value->eeb_gst_rm,
                    'total_amount'              => $value->eeb_subtotal,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user.id')
                ];
                $query = All::InsertGetID('et_quotation_detail', $dataeqdetail);
            }
            // end::Maklumat Sebut Harga

            // begin::Kemas kini harga subtotal Main_Booking
            $data_main = [
                // 'bmb_subtotal'          => $totalAmount,
                'bmb_total_equipment'   => $totalAmount,
                'updated_at'            => date('Y-m-d'),
                'updated_by'            => Session::get('user')['id']
            ];
            $query = All::GetUpdate('main_booking', $bookingId, $data_main);
            // end::Kemas kini harga subtotal Main_Booking

            return redirect('sport/external/slotMasa/'. Crypt::encrypt($bookingId));
        }

        $fk_et_function = $data['et_hall_book']->fk_et_function;
        if($fk_et_function != 10){
            $data['event'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('le_status', 1)->where('id', '!=', 8);
        } else {
            $data['event'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('id', 8);
        }
        $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility']->fk_et_facility_detail, $bookingId, $data['et_booking_facility']->ebf_start_date, $data['et_booking_facility']->id);
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);

        return view('sport.external.slot', compact('data'));
    }

    public function slotMasaDewan(Request $request, $bookingId){
        $bookingId = Crypt::decrypt($bookingId);
        $data['main'] = All::GetRow('main_booking', 'id', $bookingId);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['date'] = $data['main']->bmb_booking_date;

        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'fk_main_booking', $bookingId);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        
        $eftype = $data['et_booking_facility']->fk_et_facility_type;
        $data['ef'] = All::GetRow('et_facility_type', 'id', $eftype)->fk_et_facility;

        $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);
        $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id)->whereNull('deleted_at');

        $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility']->fk_et_facility_detail, $bookingId, $data['et_booking_facility']->ebf_start_date, $data['et_booking_facility']->id);
        $data['slot2'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_hall_book']->fk_et_function);

        $eft = $data['et_hall_book']->fk_et_function;

        if ($eft != 10) {
            $data['et_function'] = All::Show('et_function')->where('ef_type', 1)->whereIn('id', range(0, 7));
        } else {
            $data['et_function'] = All::Show('et_function')->where('id', 10);
        }
        if(request()->isMethod('post')){
            if($data['et_facility_type']->fk_et_facility == 1){
                $depositValidateMB = All::GetRow('lkp_deposit', 'fk_main_booking', $data['main']->id);
                if(!$depositValidateMB){
                    $data_bank = [
                        'name'                  => request()->namaPenuh,
                        'ref_id'                => request()->kadPengenalan,
                        'email'                 => request()->emel,
                        'mobile_no'             => request()->phoneNo,
                        'bank_name'             => request()->namaBank,
                        'bank_account'          => request()->akaunBank,
                        'fk_main_booking'       => $data['main']->id,
                        'status'                => 0,
                        // 'deposit_amount'        => ,
                        'subsystem'             => 'SPS',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ];
                    $bankId = All::InsertGetID('lkp_deposit', $data_bank);
                }
            }

            $totalSlot = 0;
            $slot = request()->slot;
            $et_function = $data['et_hall_book']->fk_et_function;

            // Pakej Majlis perkahwinan
            if($et_function == 10){
                $s = explode(",", $slot[0]);
                $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                $date = $data['et_hall_book']->ehb_booking_date;
                $date = Carbon::createFromFormat('Y-m-d', $date);
                if ($date->isWeekend()) {
                    $day_cat = 2;
                } else {
                    $day_cat = 1;
                }
                if($day_cat == 1){
                    $et_package = All::GetRow('et_package', 'id', 1);
                    $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 1)->where('deleted_at', null);

                    for ($i=1; $i < 10; $i++) {
                        if($i == 1 || $i == 9){
                            $ef = 6;
                        } else {
                            $ef = 2;
                        }
                        $efp = All::GetAllRow('et_facility_price', 'fk_et_facility', 1)->where('fk_et_function', $ef)->where('fk_et_slot_time', $i)->where('fk_lkp_gst_rate', $s[3])->where('fk_lkp_slot_cat', 1)->where('efp_day_cat', $day_cat)->first();

                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $i,
                            'fk_et_facility_price'  => $efp->id,
                            'eht_price'             => $efp->efp_unit_price,
                            'eht_total'             => $efp->efp_unit_price,
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($efp->efp_unit_price * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($efp->efp_unit_price + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }

                    // begin::Kemas kini harga subtotal Main_Booking
                    $data_main = [
                        'bmb_subtotal'          => $et_package->ep_price,
                        'bmb_total_book_hall'   => $et_package->ep_price,
                        'bmb_rounding'          => $et_package->ep_price,
                        'bmb_deposit_rm'        => 175.00,
                        'bmb_deposit_rounding'  => 175.00,
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => Session::get('user')['id']
                    ];
                    $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                    // end::Kemas kini harga subtotal Main_Booking
                } else {
                    $et_package = All::GetRow('et_package', 'id', 2);
                    $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 2)->where('deleted_at', null);

                    foreach ($et_package_detail as $key => $value) {
                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $value->fk_et_slot_time,
                            'fk_et_facility_price'  => $value->fk_et_facility_price,
                            'eht_price'             => $value->epd_price,
                            'eht_total'             => $value->epd_price,
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($value->epd_price * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($value->epd_price + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }

                    // begin::Kemas kini harga subtotal Main_Booking
                    $data_main = [
                        'bmb_subtotal'          => $et_package->ep_price,
                        'bmb_total_book_hall'   => $et_package->ep_price,
                        'bmb_rounding'          => $et_package->ep_price,
                        'bmb_deposit_rm'        => 140.00,
                        'bmb_deposit_rounding'  => 140.00,
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => Session::get('user')['id']
                    ];
                    $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                    // end::Kemas kini harga subtotal Main_Booking
                }
            } 
            // Bukan Majlis perkahwinan 
            else {
                $ef = $data['et_facility_type']->fk_et_facility;
                $efd = All::GetRow('et_facility_detail', 'fk_et_facility_type', $data['et_facility_type']->id);

                if($ef != 20){
                    foreach ($slot as $s) {
                        $s = explode(",", $s);
                        if($s[3] == 'null'){
                            $gstRate = 0;
                        } else {
                            $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                        }

                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $s[1],
                            'fk_et_facility_price'  => $s[0],
                            'eht_price'             => $s[2],
                            'eht_total'             => $s[2],
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }
                } else {    // Ruang Legar
                    $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $bookingId);
                    $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
                    foreach ($data['ehb'] as $a) {
                        foreach ($slot as $s) {
                            $s = explode(",", $s);
                            if($s[3] == 'null'){
                                $gstRate = 0;
                            } else {
                                $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                            }

                            $etHallTimeData = [
                                'fk_et_hall_book'       => $a->id,
                                'fk_et_slot_time'       => $s[1],
                                'fk_et_facility_price'  => $s[0],
                                'eht_price'             => $s[2],
                                'eht_total'             => $s[2],
                                'eht_gst_code'          => $s[3],
                                'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
                                'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
                                'created_at'            => date('Y-m-d'),
                                'updated_at'            => date('Y-m-d'),
                            ];
                            $totalSlot += $currentTotal;
                            $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                        }
                    }
                }
                $deposit = $totalSlot * ($efd->efd_deposit_percent / 100);

                // begin::Kemas kini harga subtotal 
                // $data_main = [
                //     'bmb_total_book_hall'   => $totalSlot,
                //     'bmb_deposit_rm'        => $deposit,
                //     'bmb_deposit_rounding'  => $deposit,
                //     // 'bmb_rounding'          => $data['main']->bmb_subtotal + $deposit + $totalSlot,
                //     'bmb_subtotal'          => $data['main']->bmb_subtotal + $totalSlot + $data['main']->bmb_total_equipment,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user')['id']
                // ];
                // $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                
                // $data_ebf = [
                //     'ebf_deposit'           => $deposit,
                //     'ebf_subtotal'          => $data['main']->bmb_subtotal + $totalSlot,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::GetUpdate('et_booking_facility', $data['et_booking_facility']->id, $data_ebf);
                
                // $data_ehb = [
                //     'ehb_total'             => $data['main']->bmb_subtotal + $totalSlot,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::GetUpdateSpec('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id, $data_ehb);
                // end::Kemas kini harga subtotal 
            }
            // $query = Sport::recalculate_booking(Crypt::encrypt($bookingId));

            // return redirect('sport/external/rumusandewan/'. Crypt::encrypt($bookingId));
            return redirect('sport/external/recalculate_booking/'. Crypt::encrypt($bookingId));
        }

        $data['config_depoForm'] = All::GetAllRow('lkp_configuration', 'id', 2)->first()->status;

        $audit = AuditLog::log(Crypt::encrypt(82), Crypt::encrypt($bookingId), 'Tempahan Internal - Paparan Slot Masa Dewan',1);
        return view('sport.external.halltimeslot', compact('data'));
    }

    public function recalculate_dewan($mbid){
        $query = Sport::recalculate_booking($mbid);
        
        return redirect('sport/external/rumusandewan/'. $mbid);
    }

    public function rumusandewan($bookingId){
        $bookingId = Crypt::decrypt($bookingId);
        $data['main'] = All::GetRow('main_booking', 'id', $bookingId);
        $data['mbid'] = $bookingId;

        $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        $data['date'] = $data['main']->bmb_booking_date;

        $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $bookingId);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));

        $data['et_hall_book'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
        $data['et_hall_time'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['et_hall_book']->pluck('id'))->whereNull('deleted_at');
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility'][0]->fk_et_facility_type);

        $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'))->whereNull('deleted_at');

        $data['slot_masa'] = Sport::slot_masa_rumusan(1, $bookingId);

        if(request()->isMethod('post')){
            $updateExternal = [
                'fk_lkp_status'     => 13,
                'bmb_rounding'      => 0,
                'updated_by'        => Session::get('user')['id'],
                'updated_at'        => date('Y-m-d H:i:s')
            ];
            $query = All::GetUpdate('main_booking', $bookingId, $updateExternal);

            if ($query) {
                $audit = AuditLog::log(Crypt::encrypt(72), Crypt::encrypt($bookingId), 'Status Main Booking Diupdate kepada : 2 ,Teruskan Ke Pembayaran', 1);
                Session::flash('flash', 'Success'); 
                return Redirect::to(url('/sport/dashboard/approval', Crypt::encrypt($bookingId)));
            }else{
                Session::flash('flash', 'Failed'); 
                return redirect()->back();
            }
        }

        $audit = AuditLog::log(Crypt::encrypt(71), Crypt::encrypt($bookingId), 'Bayaran Penuh Diterima',1);
        return view('sport.external.rumusandewan', compact('data'));
    }


    public function tambah_kegunaan_dewan(Request $request, $bookingId){
        $data['mbid'] = Crypt::decrypt($bookingId);
        $data['mb'] = All::GetRow('main_booking', 'id', $data['mbid']);
        $data['post'] = true;

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mbid']);
        $data['ebfd'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        
        $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['eht'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['ehb']->pluck('id'));

        $data['userId'] = $data['mb']->fk_users;
        $data['date'] = $data['ebf'][0]->ebf_start_date;
        $data['id'] = $data['ebf'][0]->fk_et_facility_type;

        if(request()->isMethod('post')){
            $postType = request()->postType;
            if($postType == 1){
                $data['date'] = request()->tarikh;
            } else {
                $data['date'] = request()->date;
                $data['ebfExist'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mbid'])->where('ebf_start_date', $data['date']);
                if(request()->multipurpose == 0){
                    if(isset(request()->et_function_id)){
                        $et_facility_type_id = (int)request()->et_facility_type_id;
                        $et_function_id = request()->et_function_id;
                        $type = 1;
                        $data['function'] = All::GetRow('et_function', 'id', $et_function_id);
                    } else if(isset(request()->et_facility_type_id)){
                        $et_facility_type_id = (int)request()->et_facility_type_id;
                        $et_function_id = 13;
                        $type = 2;
                    } else {
                        return redirect()->to('/sport/external/hall/'. Crypt::encrypt($data['userId']));
                    }
                    $data['user'] = All::GetRow('users', 'id', $data['mb']->fk_users);
                    $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['mb']->fk_users);
                    $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
                    $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
                    $type = 1; // 1 = et_function, 2 = et_facility_type
                    $data['et_facility_detail'] = All::GetRow('et_facility_detail', 'fk_et_facility_type', $et_facility_type_id);
                    
                    $lkp_location = $data['hall']->fk_lkp_location;

                    // INSERT::main_booking
                    // $running =  Sport::getRn();
                    // $rn = sprintf( '%05d', $running);
                    // $year = date('y');
                    // $md = date('md');
                    // $bookrn='SPS'.$year.$md.$rn;
                    // $dataMainBooking = [
                    //     'fk_users'              => $data['userId'],
                    //     'fk_lkp_status'         => 1,
                    //     'fk_lkp_deposit_rate'   => All::GetRow('lkp_deposit_rate', 'fk_lkp_location', $lkp_location)->id,
                    //     'fk_lkp_location'       => $lkp_location,
                    //     'fk_lkp_discount_type'  => request()->JenisAcaraTempDewan, 
                    //     'bmb_booking_no'        => $bookrn,
                    //     'bmb_staff_id'          => request()->staffid,
                    //     'bmb_kementerian'       => request()->kementerianjabatan,
                    //     'bmb_agensi'            => request()->bmb_agensi,
                    //     'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
                    //     'bmb_type_user'         => 2,
                    //     'created_at'            => date('Y-m-d'),
                    //     'updated_at'            => date('Y-m-d'),
                    // ];
                    // $query = All::InsertGetID('main_booking', $dataMainBooking);
                    $mbid = $data['mbid'];
                    // INSERT::main_booking
    
                    // INSERT::main_booking
                    if($request->hasFile('suratAkuan')){
                        $file = $request->file('suratAkuan');
                        $fileName = $file->getClientOriginalName();
                        $fileExtension = $file->getClientOriginalExtension();
                        $fileSize = $file->getSize();
            
                        $newFileName = 'Dokumen Sokongan_' . $bookrn . '.' . $fileExtension;
            
                        // $path = public_path('dokumen/sukan/' . $bookrn);
                        $path = public_path('upload_document/mainbooking/' . $mbid);
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $file->move(public_path('upload_document/mainbooking/' . $mbid), $newFileName);
                        // $file->move(public_path('dokumen/sukan/' . $bookrn), $newFileName);
                        $fullPath = 'upload_document/mainbooking/' . $mbid . '/' . $newFileName;
            
                        $file_data = [
                            'fk_main_booking'       => $mbid,
                            'ba_date'               => date('Y-m-d H:i:s'),
                            'ba_dir'                => 'upload_document',
                            'ba_full_path'          => $fullPath,
                            'ba_file_name'          => $newFileName,
                            'ba_file_ext'           => $fileExtension,
                            'ba_file_size'          => $fileSize,
                            'ba_generated_name'     => 'Dokumen Sokongan',
                            'ba_status'             => null,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                            'updated_by'            => Session::get('user.id')
                        ];
                        $ea = All::InsertGetID('bh_attachment', $file_data);
                    }
                    
                    if(count($data['ebfExist']) > 0){
                        $etfid = $data['ebfExist']->first()->id;
                    } else {
                        $dataEtBookingFacility = [
                            'fk_main_booking'            => $mbid,
                            'fk_et_facility_type'        => $et_facility_type_id,
                            'fk_et_facility_detail'      => $data['et_facility_detail']->id,
                            'fk_lkp_discount'            => 1,
                            'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
                            'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
                            'ebf_no_of_day'              => 1,
                            'ebf_facility_indi'         => $data['et_facility']->ef_type,
                            'created_at'                 => date('Y-m-d H:i:s'),
                            'updated_at'                 => date('Y-m-d H:i:s')
                        ];
                        $etfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);
                    }
    
                    $unixTimestamp = strtotime($request->post('date'));
                    $mysqlDate = date('Y-m-d', $unixTimestamp);
    
                    $dataEtHallBook = [
                        'fk_et_booking_facility'    => $etfid,
                        'fk_et_function'            => $et_function_id,
                        'ehb_booking_date'          => $mysqlDate,
                        'created_at'                => date('Y-m-d H:i:s'),
                        'updated_at'                => date('Y-m-d H:i:s')
                    ];
                    $id = All::InsertGetID('et_hall_book', $dataEtHallBook);
    
                    return redirect('sport/external/slotuse/' . Crypt::encrypt($mbid) .'/'. Crypt::encrypt($etfid));
                } else {
                    $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
                    $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
                    $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
                    $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
                    $type = 1; // 1 = et_function, 2 = et_facility_type
    
                    $function = request()->fn[0];
                    $detail = request()->gelanggang;
    
                    $running =  Sport::getRn();
                    $rn = sprintf( '%05d', $running);
                    $year = date('y');
                    $md = date('md');
                    $bookrn='SPS'.$year.$md.$rn;
                    
                    $dataMainBooking = [
                        'fk_users'              => $data['userId'],
                        'fk_lkp_status'         => 1,
                        'fk_lkp_location'       => $data['checkroom'][0]->fk_lkp_location,
                        'fk_lkp_discount_type'  => 5, 
                        'bmb_booking_no'        => $bookrn,
                        'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
                        'bmb_type_user'         => 2,
                        'created_at'            => date('Y-m-d'),
                        'updated_at'            => date('Y-m-d'),
                    ];
                    $mbid = All::InsertGetID('main_booking', $dataMainBooking);
    
                    foreach ($detail as $key => $value) {
                        $dataEtBookingFacility = [
                            'fk_main_booking'            => $mbid,
                            'fk_et_facility_type'        => All::GetRow('et_facility_detail', 'id', $value)->fk_et_facility_type,
                            'fk_et_facility_detail'      => $value,
                            'fk_lkp_discount'            => 1,
                            'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
                            'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
                            'ebf_no_of_day'              => 1,
                            'ebf_facility_indi'         => $data['et_facility']->ef_type,
                            'created_at'                 => date('Y-m-d H:i:s'),
                            'updated_at'                 => date('Y-m-d H:i:s')
                        ];
                        $ebfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);
    
                        $dataEtHallBook = [
                            'fk_et_booking_facility'        => $ebfid,
                            'fk_et_function'                => $function,
                            'ehb_booking_date'              => $data['date'],
                            'created_at'                    => date('Y-m-d H:i:s'),
                            'updated_at'                    => date('Y-m-d H:i:s'),
                        ];
                        $ehbid = All::InsertGetID('et_hall_book', $dataEtHallBook);
    
                        $date = Carbon::createFromFormat('d-m-Y', $data['date']);
                        if ($date->isWeekend()) {
                            $day_cat = 2;
                        } else {
                            $day_cat = 1;
                        }
                        $data_efp = All::GetAllRow('et_facility_price', 'fk_et_facility', $data['result'])->where('fk_et_function', $function)->where('efp_day_cat', $day_cat)->first();
                        $data_lkp_gst = All::GetAllRow('lkp_gst_rate', 'id', $data_efp->fk_lkp_gst_rate)->first();
    
                        // $dataEtHallTime = [
                        //     'fk_et_hall_book'               => $ehbid,
                        //     'fk_et_slot_time'               => $data_efp->fk_et_slot_time,
                        //     'fk_et_facility_price'          => $data_efp->id,
                        //     'eht_price'                     => $data_efp->efp_unit_price,
                        //     'eht_discount_type_rm'          => 0.00,
                        //     'eht_discount_rm'               => 0.00,
                        //     'eht_total'                     => $data_efp->efp_unit_price,
                        //     'eht_gst_code'                  => $data_lkp_gst->id,
                        //     'eht_gst_rm'                    => ($data_efp->efp_unit_price * $data_lkp_gst->lgr_rate),
                        //     'eht_subtotal'                  => $data_efp->efp_unit_price,
                        //     'created_at'                    => date('Y-m-d H:i:s'),
                        //     'updated_at'                    => date('Y-m-d H:i:s'),
                        // ];
                        // $ehtid = All::InsertGetID('et_hall_time', $dataEtHallTime);
                    }
    
                    return redirect('sport/external/slot/' . Crypt::encrypt($mbid));
                }
            }
        }

        $data['category'] = All::GetAllRow('lkp_discount_type', 'ldt_indicator', 1);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id','!=',1);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        $data['function'] = All::Show('et_function', 'id', 'ASC');
        $data['type'] = Sport::getCheckfacilitytype($data['id']);
        $data['checkroom'] = Sport::getCheckroom($data['id']);
        $data['lkp_location'] = $data['checkroom'][0]->fk_lkp_location;

        foreach ($data['checkroom'] as $key => $value) {
            $data['result'] = $value->fk_et_facility;
        }

        // if(request()->isMethod('post')){
        //     if(request()->multipurpose == 0){
        //         dd($request->post());
        //         if(isset(request()->et_function_id)){
        //             $et_facility_type_id = (int)request()->et_facility_type_id;
        //             $et_function_id = request()->et_function_id;
        //             $type = 1;
        //             $data['function'] = All::GetRow('et_function', 'id', $et_function_id);
        //         } else if(isset(request()->et_facility_type_id)){
        //             $et_facility_type_id = (int)request()->et_facility_type_id;
        //             $et_function_id = 13;
        //             $type = 2;
        //         } else {
        //             return redirect()->to('/sport/external/hall/'. Crypt::encrypt($data['userId']));
        //         }
        //         $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        //         $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        //         $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
        //         $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
        //         $type = 1; // 1 = et_function, 2 = et_facility_type
        //         $data['et_facility_detail'] = All::GetRow('et_facility_detail', 'fk_et_facility_type', $et_facility_type_id);
                
        //         $lkp_location = $data['hall']->fk_lkp_location;

        //         // INSERT::main_booking
        //         $running =  Sport::getRn();
        //         $rn = sprintf( '%05d', $running);
        //         $year = date('y');
        //         $md = date('md');
        //         $bookrn='SPS'.$year.$md.$rn;
        //         $dataMainBooking = [
        //             'fk_users'              => $data['userId'],
        //             'fk_lkp_status'         => 1,
        //             'fk_lkp_deposit_rate'   => All::GetRow('lkp_deposit_rate', 'fk_lkp_location', $lkp_location)->id,
        //             'fk_lkp_location'       => $lkp_location,
        //             'fk_lkp_discount_type'  => request()->JenisAcaraTempDewan, 
        //             'bmb_booking_no'        => $bookrn,
        //             'bmb_staff_id'          => request()->staffid,
        //             'bmb_kementerian'       => request()->kementerianjabatan,
        //             'bmb_agensi'            => request()->bmb_agensi,
        //             'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
        //             'bmb_type_user'         => 2,
        //             'created_at'            => date('Y-m-d'),
        //             'updated_at'            => date('Y-m-d'),
        //         ];
        //         $query = All::InsertGetID('main_booking', $dataMainBooking);
        //         $mbid = $query;
        //         // INSERT::main_booking

        //         // INSERT::main_booking
        //         if($request->hasFile('suratAkuan')){
        //             $file = $request->file('suratAkuan');
        //             $fileName = $file->getClientOriginalName();
        //             $fileExtension = $file->getClientOriginalExtension();
        //             $fileSize = $file->getSize();
        
        //             $newFileName = 'Dokumen Sokongan_' . $bookrn . '.' . $fileExtension;
        
        //             // $path = public_path('dokumen/sukan/' . $bookrn);
        //             $path = public_path('upload_document/mainbooking/' . $mbid);
        //             if (!file_exists($path)) {
        //                 mkdir($path, 0777, true);
        //             }
        //             $file->move(public_path('upload_document/mainbooking/' . $mbid), $newFileName);
        //             // $file->move(public_path('dokumen/sukan/' . $bookrn), $newFileName);
        //             $fullPath = 'upload_document/mainbooking/' . $mbid . '/' . $newFileName;
        
        //             $file_data = [
        //                 'fk_main_booking'       => $mbid,
        //                 'ba_date'               => date('Y-m-d H:i:s'),
        //                 'ba_dir'                => 'upload_document',
        //                 'ba_full_path'          => $fullPath,
        //                 'ba_file_name'          => $newFileName,
        //                 'ba_file_ext'           => $fileExtension,
        //                 'ba_file_size'          => $fileSize,
        //                 'ba_generated_name'     => 'Dokumen Sokongan',
        //                 'ba_status'             => null,
        //                 'created_at'            => date('Y-m-d H:i:s'),
        //                 'updated_at'            => date('Y-m-d H:i:s'),
        //                 'updated_by'            => Session::get('user.id')
        //             ];
        //             $ea = All::InsertGetID('bh_attachment', $file_data);
        //             // $file_data = [
        //             //     'fk_main_booking'       => $mbid,
        //             //     'eta_date'              => date('Y-m-d H:i:s'),
        //             //     'eta_dir'               => 'dokumen/sukan/' . $bookrn,
        //             //     // 'eta_full_path'         => ,
        //             //     'eta_file_name'         => $newFileName,
        //             //     'eta_file_ext'          => $fileExtension,
        //             //     'eta_file_size'         => $fileSize,
        //             //     'eta_generated_name'    => 'Dokumen Sokongan',
        //             //     'eta_status'            => 1,
        //             //     'created_at'            => date('Y-m-d H:i:s'),
        //             //     'updated_at'            => date('Y-m-d H:i:s'),
        //             //     'updated_by'            => Session::get('user.id')
        //             // ];
        //             // $ea = All::InsertGetID('et_attachment', $file_data);
        //         }

        //         $dataEtBookingFacility = [
        //             'fk_main_booking'            => $query,
        //             'fk_et_facility_type'        => $et_facility_type_id,
        //             'fk_et_facility_detail'      => $data['et_facility_detail']->id,
        //             'fk_lkp_discount'            => 1,
        //             'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
        //             'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
        //             'ebf_no_of_day'              => 1,
        //             'ebf_facility_indi'         => $data['et_facility']->ef_type,
        //             'created_at'                 => date('Y-m-d H:i:s'),
        //             'updated_at'                 => date('Y-m-d H:i:s')
        //         ];
        //         $etfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);

        //         $unixTimestamp = strtotime($request->post('date'));
        //         $mysqlDate = date('Y-m-d', $unixTimestamp);

        //         $dataEtHallBook = [
        //             'fk_et_booking_facility'    => $etfid,
        //             'fk_et_function'            => $et_function_id,
        //             'ehb_booking_date'          => $mysqlDate,
        //             'created_at'                => date('Y-m-d H:i:s'),
        //             'updated_at'                => date('Y-m-d H:i:s')
        //         ];
        //         $id = All::InsertGetID('et_hall_book', $dataEtHallBook);

        //         return redirect('sport/external/slot/' . Crypt::encrypt($mbid));
        //     } else {
        //         $data['user'] = All::GetRow('users', 'id', Session::get('user')['id']);
        //         $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', Session::get('user')['id']);
        //         $data['hall'] = All::GetRow('et_facility_type', 'id', $data['id']);
        //         $data['et_facility'] = All::GetRow('et_facility', 'id', $data['hall']->fk_et_facility);
        //         $type = 1; // 1 = et_function, 2 = et_facility_type

        //         $function = request()->fn[0];
        //         $detail = request()->gelanggang;

        //         $running =  Sport::getRn();
        //         $rn = sprintf( '%05d', $running);
        //         $year = date('y');
        //         $md = date('md');
        //         $bookrn='SPS'.$year.$md.$rn;
                
        //         $dataMainBooking = [
        //             'fk_users'              => $data['userId'],
        //             'fk_lkp_status'         => 1,
        //             'fk_lkp_location'       => $data['checkroom'][0]->fk_lkp_location,
        //             'fk_lkp_discount_type'  => 5, 
        //             'bmb_booking_no'        => $bookrn,
        //             'bmb_booking_date'      => date('Y-m-d', strtotime($data['date'])),
        //             'bmb_type_user'         => 2,
        //             'created_at'            => date('Y-m-d'),
        //             'updated_at'            => date('Y-m-d'),
        //         ];
        //         $mbid = All::InsertGetID('main_booking', $dataMainBooking);

        //         foreach ($detail as $key => $value) {
        //             $dataEtBookingFacility = [
        //                 'fk_main_booking'            => $mbid,
        //                 'fk_et_facility_type'        => All::GetRow('et_facility_detail', 'id', $value)->fk_et_facility_type,
        //                 'fk_et_facility_detail'      => $value,
        //                 'fk_lkp_discount'            => 1,
        //                 'ebf_start_date'             => date('Y-m-d', strtotime($data['date'])),
        //                 'ebf_end_date'               => date('Y-m-d', strtotime($data['date'])),
        //                 'ebf_no_of_day'              => 1,
        //                 'ebf_facility_indi'         => $data['et_facility']->ef_type,
        //                 'created_at'                 => date('Y-m-d H:i:s'),
        //                 'updated_at'                 => date('Y-m-d H:i:s')
        //             ];
        //             $ebfid = All::InsertGetID('et_booking_facility', $dataEtBookingFacility);

        //             $dataEtHallBook = [
        //                 'fk_et_booking_facility'        => $ebfid,
        //                 'fk_et_function'                => $function,
        //                 'ehb_booking_date'              => $data['date'],
        //                 'created_at'                    => date('Y-m-d H:i:s'),
        //                 'updated_at'                    => date('Y-m-d H:i:s'),
        //             ];
        //             $ehbid = All::InsertGetID('et_hall_book', $dataEtHallBook);

        //             $date = Carbon::createFromFormat('d-m-Y', $data['date']);
        //             if ($date->isWeekend()) {
        //                 $day_cat = 2;
        //             } else {
        //                 $day_cat = 1;
        //             }
        //             $data_efp = All::GetAllRow('et_facility_price', 'fk_et_facility', $data['result'])->where('fk_et_function', $function)->where('efp_day_cat', $day_cat)->first();
        //             $data_lkp_gst = All::GetAllRow('lkp_gst_rate', 'id', $data_efp->fk_lkp_gst_rate)->first();

        //             // $dataEtHallTime = [
        //             //     'fk_et_hall_book'               => $ehbid,
        //             //     'fk_et_slot_time'               => $data_efp->fk_et_slot_time,
        //             //     'fk_et_facility_price'          => $data_efp->id,
        //             //     'eht_price'                     => $data_efp->efp_unit_price,
        //             //     'eht_discount_type_rm'          => 0.00,
        //             //     'eht_discount_rm'               => 0.00,
        //             //     'eht_total'                     => $data_efp->efp_unit_price,
        //             //     'eht_gst_code'                  => $data_lkp_gst->id,
        //             //     'eht_gst_rm'                    => ($data_efp->efp_unit_price * $data_lkp_gst->lgr_rate),
        //             //     'eht_subtotal'                  => $data_efp->efp_unit_price,
        //             //     'created_at'                    => date('Y-m-d H:i:s'),
        //             //     'updated_at'                    => date('Y-m-d H:i:s'),
        //             // ];
        //             // $ehtid = All::InsertGetID('et_hall_time', $dataEtHallTime);
        //         }

        //         return redirect('sport/external/slot/' . Crypt::encrypt($mbid));
        //     }
        // }
        if($data['type'] == 1) { // Dewan
            if($data['result'] == 1) {
                $data['slot'] = Sport::getCheckfacilityfunction($data['id']);
                $data['slot1'] = Sport::getCheckfacilityfunction1($data['id']);
                $combinedSlots = array_merge($data['slot'], $data['slot1']);
                $idColumn = array_column($combinedSlots, 'id');
                
                // Remove duplicates based on the 'id' column
                $uniqueSlots = array_unique($idColumn);
                
                // Create a new array with unique elements
                $uniqueCombinedSlots = [];
                foreach ($combinedSlots as $slot) {
                    if (in_array($slot['id'], $uniqueSlots)) {
                        $uniqueCombinedSlots[] = $slot;
                        // Remove the element from $uniqueSlots to ensure uniqueness
                        unset($uniqueSlots[array_search($slot['id'], $uniqueSlots)]);
                    }
                }
                $data['slot1'] = $uniqueCombinedSlots;
            } else {
                $checkfunction = Sport::getCheckfunction($data['id']);
                if($checkfunction == null){
                    $data['slot1'] = array_values(Sport::getChild($data['id']));
                    if(count($data['slot1']) == 0){
                    } else {
                        $data['slot1'] = $data['slot1'][0];
                    }
                } else {
                    if ($data['id'] == 11 ) {
                        $data['slot1'] = Sport::getCheckfacilityfunction($data['id']);
                        // $data['slot1'] = Sport::getCheckfacilityfunction1($data['id']);
                        // $child = $facfunction;
                        // $child2 = $facfunction;
                        $task = 76;
                        $task_desc = 'Paparkan Muka Hadapan Tempahan Bilik - Pilihan Function/Distype. Facility Type : ' . $data['id'];
                        // event('audit', ['', Auth::user()->id, $task, $task_desc]);
                    } 
		            // else if ($data['id'] == 13 || $data['id'] == 29 || $data['id'] == 38){   // 13, 29, 38 = Squash, tak perlu
                    //     // $child = $facdetails;
                    //     // $child2 = $facdetails;
                    //     $task = 76;
                    //     $task_desc = 'Paparkan Muka Hadapan Tempahan Bilik - Pilihan Distype. Facility Type : ' . $data['id'];
                    //     event('audit', ['', Auth::user()->id, $task, $task_desc]);
                    // }
                    else {
                        $data['slot1'] = array_values(Sport::getChildruangcr($data['id'], $data['date']))[0];
                        $data['slot'] = Sport::getCheckruangfunction($data['id']);
                        $data['slot2'] = Sport::getChildruangcr($data['id'], $data['date']);
                        
                        return view('sport.external.multipurposeuse', compact('data'));
                    }
                }
            }
        }

        return view('sport.external.purposeuse', compact('data'));
        // else { // Sukan, boleh disable
        //     if($data['result'] == 11) {
        //         // dd("1", $data);
        //     } else {
        //         // dd("2", $data);
        //     }
        // }
    }

    public function slotDewan_kegunaan(Request $request, $bookingId, $ebfId = null) {
        $bookingId = Crypt::decrypt($bookingId);
        $ebfId = Crypt::decrypt($ebfId);

        $data['booking'] = $bookingId;
        $data['main'] = All::GetRow('main_booking', 'id', $bookingId);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'id', $ebfId);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->id);

        $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        $data['date'] = $data['main']->bmb_booking_date;
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);

        $et_facility_type = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type)->id;
        $et_facility_detail = All::GetRow('et_facility_detail', 'fk_et_facility_type', $et_facility_type)->id;

        $data['lokasiData'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);
        $data['equipment_int'] = Sport::getEquipdalam($data['lokasiData']->id);
        $data['equipment_ext'] = Sport::getEquipluar($data['lokasiData']->id);

        $data['attachment'] = All::GetRow('bh_attachment', 'fk_main_booking', $data['main']->id);

        if(request()->isMethod('post')) {
            if ($request->hasFile('suratAkuan')) {
                $file = $request->file('suratAkuan');
                $fileName = $file->getClientOriginalName();
                $fileExtension = $file->getClientOriginalExtension();
                $fileSize = $file->getSize();

                $newFileName = 'Surat Sokongan ' . $data['main']->bmb_booking_no . '.' . $fileExtension;

                // $path = public_path('dokumen/sukan/' . $data['booking']);
                $path = public_path('upload_document/mainbooking/' . $mbid);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $file->move(public_path('upload_document/mainbooking/' . $mbid), $newFileName);
                // $file->move(public_path('dokumen/sukan/' . $data['booking']), $newFileName);
                $fullPath = 'upload_document/mainbooking/' . $mbid . '/' . $newFileName;
    
                $file_data = [
                    'fk_main_booking'       => $mbid,
                    'ba_date'               => date('Y-m-d H:i:s'),
                    'ba_dir'                => 'upload_document',
                    'ba_full_path'          => $fullPath,
                    'ba_file_name'          => $newFileName,
                    'ba_file_ext'           => $fileExtension,
                    'ba_file_size'          => $fileSize,
                    'ba_generated_name'     => 'Dokumen Sokongan',
                    'ba_status'             => null,
                    'created_at'            => date('Y-m-d H:i:s'),
                    'updated_at'            => date('Y-m-d H:i:s'),
                    'updated_by'            => Session::get('user.id')
                ];
                $ea = All::InsertGetID('bh_attachment', $file_data);
                // $file_data = [
                //     'fk_main_booking'       => $data['booking'],
                //     'eta_date'              => date('Y-m-d H:i:s'),
                //     'eta_dir'               => 'dokumen/sukan/' . $data['booking'],
                //     // 'eta_full_path'         => ,
                //     'eta_file_name'         => $newFileName,
                //     'eta_file_ext'          => $fileExtension,
                //     'eta_file_size'         => $fileSize,
                //     'eta_generated_name'    => 'Dokumen Sokongan',
                //     'eta_status'            => 1,
                //     'created_at'            => date('Y-m-d H:i:s'),
                //     'updated_at'            => date('Y-m-d H:i:s'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::InsertGetID('et_attachment', $file_data);
            }

            // begin::Pengasingan data peralatan [Dalaman & Luaran]
            $data['dalamanID'] = [];
            $data['luaranID'] = [];
            // Remove prev equipment booked to save new ones
            $eeb = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id)->whereNull('deleted_at');
            if(count($eeb) > 0){
                foreach($eeb as $key => $eeb){
                    $removePrevEEB = [
                        'deleted_at'    => date('Y-m-d')
                    ];
                    $query = All::GetUpdate('et_equipment_book', $eeb->id, $removePrevEEB);
                }
            }
            // begin::Pengasingan data peralatan [Dalaman & Luaran]
            foreach ($request->post() as $key => $value) {
                if (strpos($key, 'dalaman_') === 0) {
                    if ($value !== null && $value !== 0) {
                        $id = intval(substr($key, strlen('dalaman_')));
                        $data['dalamanID'][$id] = $value;
                    }
                }
            }
            foreach ($request->post() as $key => $value) {
                if (strpos($key, 'luaran_') === 0) {
                    if($value != null && $value != 0){
                        $id = intval(substr($key, strlen('luaran_')));
                        $data['luaranID'][$id] = $value;
                    }
                }
            }
            // end::Pengasingan data peralatan [Dalaman & Luaran]
            
            // begin::Kategori hari [Hari minggu @ Hujung minggu]
            $dayCat = date('w', strtotime($data['date']));
            if($dayCat == 0 || $dayCat == 6){
                $dayCat = 2;
            } else {
                $dayCat = 1;
            }
            // end::Kategori hari [Hari minggu @ Hujung minggu]
    
            // begin::Maklumat Terperinci Acara
            $dataEtBookingFacilityDetail = [
                'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                'fk_lkp_event'              => request()->JenisAcaraTempDewan,
                'ebfd_event_name'           => request()->namaAcara,
                'ebfd_event_desc'           => request()->keteranganAcara,
                'ebfd_others'               => request()->keteranganAcara,
                'ebfd_total_pax'            => request()->vvip + request()->vip + request()->participant,
                'ebfd_user_apply'           => request()->namaPemohon,
                'ebfd_venue'                => All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description,
                // 'ebfd_address'              => request()->,
                'ebfd_contact_no'           => request()->noTel,
                'ebfd_vvip'                 => request()->vvip,
                'ebfd_vip'                  => request()->vip,
                'ebfd_participant'          => request()->participant,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            ];
            $ebfid = All::InsertGetID('et_booking_facility_detail', $dataEtBookingFacilityDetail);
            // end::Maklumat Terperinci Acara

            // begin::Maklumat Kelengkapan Peralatan
            $totalAmount = 0;
            foreach ($data['dalamanID'] as $key => $quantity) {
                $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 8)->where('eep_day_cat', $dayCat)->first();
                $lkp_gst_rate = (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
                $total = $equipmentPrice->eep_unit_price * $quantity;
                $totalAmount += $total;
                $dataebfd = array(
                    'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                    'fk_et_function'            => 8,
                    'fk_et_equipment'           => $key,
                    'fk_et_equipment_price'     => $equipmentPrice->id,
                    'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                    'eeb_booking_date'          => date('Y-m-d', strtotime($data['date'])),
                    'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                    'eeb_quantity'              => $quantity,
                    'eeb_total_price'           => $total,
                    'eeb_discount_type_rm'      => 0.00,
                    'eeb_discount_rm'           => 0.00,
                    // 'eeb_special_disc'          => ,
                    // 'eeb_special_disc_rm'       => ,
                    'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                    'eeb_gst_rm'                => $total * $lkp_gst_rate,
                    'eeb_subtotal'              => $total,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );
                $query = All::InsertGetID('et_equipment_book', $dataebfd);
            }
            foreach ($data['luaranID'] as $key => $quantity) {
                $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 9)->where('eep_day_cat', $dayCat)->first();
                // dd($key, $quantity, $dayCat, $equipmentPrice);
                $lkp_gst_rate = (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
                $total = $equipmentPrice->eep_unit_price * $quantity;
                $totalAmount += $total;
                $dataebfd = array(
                    'fk_et_booking_facility'    => $data['et_booking_facility']->id,
                    'fk_et_function'            => 9,
                    'fk_et_equipment'           => $key,
                    'fk_et_equipment_price'     => $equipmentPrice->id,
                    'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                    'eeb_booking_date'          => date('Y-m-d', strtotime($data['date'])),
                    'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                    'eeb_quantity'              => $quantity,
                    'eeb_total_price'           => $total,
                    'eeb_discount_type_rm'      => 0.00,
                    'eeb_discount_rm'           => 0.00,
                    // 'eeb_special_disc'          => ,
                    // 'eeb_special_disc_rm'       => ,
                    'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                    'eeb_gst_rm'                => $total * $lkp_gst_rate,
                    'eeb_subtotal'              => $total,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s')
                );
                $query = All::InsertGetID('et_equipment_book', $dataebfd);
            }
            // end::Maklumat Kelengkapan Peralatan

            // begin::Maklumat Sebut Harga
            $quono = Sport::generatequono();
            $presint = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location)->lc_description;
            $databq = array(
                'fk_main_booking'           => $data['main']->id,
                'fk_users'                  => $data['main']->fk_users,
                'fk_lkp_discount_type'      => 5,
                'bq_quotation_no'           => 'PPj/' .$presint.'/'.$data['main']->bmb_booking_no.'/'.$quono,
                'bq_quotation_date'         => $data['date'],
                'bq_quotation_status'       => 1,
                'bq_total_amount'           => $totalAmount,
                'bq_payment_status'         => 0,
                'bq_quotation_date'           => date('Y-m-d H:i:s'),
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $bqid = All::InsertGetID('bh_quotation', $databq);

            $data['et_eq_updated'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
            foreach ($data['et_eq_updated'] as $key => $value) {
                $dataeqdetail = [
                    'fk_bh_quotation'           => $bqid,
                    'fk_et_booking_facility'    => $value->fk_et_booking_facility,
                    // 'fk_et_facility_detail'     => ,
                    'fk_et_equipment'           => $value->fk_et_equipment,
                    'fk_et_equipment_book'      => $value->id,
                    'fk_lkp_gst_rate'           => $value->fk_lkp_gst_rate,
                    // 'product_indicator'         => ,
                    'booking_date'              => $value->eeb_booking_date,
                    'unit_price'                => $value->eeb_unit_price,
                    'quantity'                  => $value->eeb_quantity,
                    // 'code_gst'                  => $value->,
                    'gst_amount'                => $value->eeb_gst_rm,
                    'total_amount'              => $value->eeb_subtotal,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                    'updated_by'                => Session::get('user.id')
                ];
                $query = All::InsertGetID('et_quotation_detail', $dataeqdetail);
            }
            // end::Maklumat Sebut Harga

            // begin::Kemas kini harga subtotal Main_Booking
            // $data_main = [
            //     // 'bmb_subtotal'          => $totalAmount,
            //     'bmb_total_equipment'   => $totalAmount,
            //     'updated_at'            => date('Y-m-d'),
            //     'updated_by'            => Session::get('user')['id']
            // ];
            // $query = All::GetUpdate('main_booking', $bookingId, $data_main);
            // end::Kemas kini harga subtotal Main_Booking

            return redirect('sport/external/slotMasause/'. Crypt::encrypt($bookingId) .'/'. Crypt::encrypt($data['et_booking_facility']->id));
        }

        $fk_et_function = $data['et_hall_book']->fk_et_function;
        if($fk_et_function != 10){
            $data['event'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('le_status', 1)->where('id', '!=', 8);
        } else {
            $data['event'] = All::Show('lkp_event', 'le_sequence', 'ASC')->where('id', 8);
        }
        $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility']->fk_et_facility_detail, $bookingId, $data['et_booking_facility']->ebf_start_date, $data['et_booking_facility']->id);
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);

        return view('sport.external.slotuse', compact('data'));
    }

    public function slotMasaDewan_kegunaan(Request $request, $bookingId, $ebfId){
        $bookingId = Crypt::decrypt($bookingId);
        $ebfId = Crypt::decrypt($ebfId);

        $data['main'] = All::GetRow('main_booking', 'id', $bookingId);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['date'] = $data['main']->bmb_booking_date;

        $data['et_booking_facility'] = All::GetRow('et_booking_facility', 'id', $ebfId);
        $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        
        $eftype = $data['et_booking_facility']->fk_et_facility_type;
        $data['ef'] = All::GetRow('et_facility_type', 'id', $eftype)->fk_et_facility;

        $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id);
        $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility']->fk_et_facility_type);
        $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->id)->whereNull('deleted_at');

        $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility']->fk_et_facility_detail, $bookingId, $data['et_booking_facility']->ebf_start_date, $data['et_booking_facility']->id);
        $data['slot2'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_hall_book']->fk_et_function);

        $eft = $data['et_hall_book']->fk_et_function;

        if ($eft != 10) {
            $data['et_function'] = All::Show('et_function')->where('ef_type', 1)->whereIn('id', range(0, 7));
        } else {
            $data['et_function'] = All::Show('et_function')->where('id', 10);
        }
        if(request()->isMethod('post')){
            if($data['et_facility_type']->fk_et_facility == 1){
                $depositValidateMB = All::GetRow('lkp_deposit', 'fk_main_booking', $data['main']->id);
                if(!$depositValidateMB){
                    $data_bank = [
                        'name'                  => request()->namaPenuh,
                        'ref_id'                => request()->kadPengenalan,
                        'email'                 => request()->emel,
                        'mobile_no'             => request()->phoneNo,
                        'bank_name'             => request()->namaBank,
                        'bank_account'          => request()->akaunBank,
                        'fk_main_booking'       => $data['main']->id,
                        'status'                => 0,
                        // 'deposit_amount'        => ,
                        'subsystem'             => 'SPS',
                        'created_at'            => date('Y-m-d H:i:s'),
                        'updated_at'            => date('Y-m-d H:i:s'),
                    ];
                    $bankId = All::InsertGetID('lkp_deposit', $data_bank);
                }
            }

            $totalSlot = 0;
            $slot = request()->slot;
            $et_function = $data['et_hall_book']->fk_et_function;

            // Pakej Majlis perkahwinan
            if($et_function == 10){
                $s = explode(",", $slot[0]);
                $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                $date = $data['et_hall_book']->ehb_booking_date;
                $date = Carbon::createFromFormat('Y-m-d', $date);
                if ($date->isWeekend()) {
                    $day_cat = 2;
                } else {
                    $day_cat = 1;
                }
                if($day_cat == 1){
                    $et_package = All::GetRow('et_package', 'id', 1);
                    $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 1)->where('deleted_at', null);

                    for ($i=1; $i < 10; $i++) {
                        if($i == 1 || $i == 9){
                            $ef = 6;
                        } else {
                            $ef = 2;
                        }
                        $efp = All::GetAllRow('et_facility_price', 'fk_et_facility', 1)->where('fk_et_function', $ef)->where('fk_et_slot_time', $i)->where('fk_lkp_gst_rate', $s[3])->where('fk_lkp_slot_cat', 1)->where('efp_day_cat', $day_cat)->first();

                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $i,
                            'fk_et_facility_price'  => $efp->id,
                            'eht_price'             => $efp->efp_unit_price,
                            'eht_total'             => $efp->efp_unit_price,
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($efp->efp_unit_price * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($efp->efp_unit_price + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }

                    // begin::Kemas kini harga subtotal Main_Booking
                    $data_main = [
                        'bmb_subtotal'          => $et_package->ep_price,
                        'bmb_total_book_hall'   => $et_package->ep_price,
                        'bmb_rounding'          => $et_package->ep_price,
                        'bmb_deposit_rm'        => 175.00,
                        'bmb_deposit_rounding'  => 175.00,
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => Session::get('user')['id']
                    ];
                    $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                    // end::Kemas kini harga subtotal Main_Booking
                } else {
                    $et_package = All::GetRow('et_package', 'id', 2);
                    $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 2)->where('deleted_at', null);

                    foreach ($et_package_detail as $key => $value) {
                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $value->fk_et_slot_time,
                            'fk_et_facility_price'  => $value->fk_et_facility_price,
                            'eht_price'             => $value->epd_price,
                            'eht_total'             => $value->epd_price,
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($value->epd_price * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($value->epd_price + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }

                    // begin::Kemas kini harga subtotal Main_Booking
                    $data_main = [
                        'bmb_subtotal'          => $et_package->ep_price,
                        'bmb_total_book_hall'   => $et_package->ep_price,
                        'bmb_rounding'          => $et_package->ep_price,
                        'bmb_deposit_rm'        => 140.00,
                        'bmb_deposit_rounding'  => 140.00,
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => Session::get('user')['id']
                    ];
                    $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                    // end::Kemas kini harga subtotal Main_Booking
                }
            } 
            // Bukan Majlis perkahwinan 
            else {
                $ef = $data['et_facility_type']->fk_et_facility;
                $efd = All::GetRow('et_facility_detail', 'fk_et_facility_type', $data['et_facility_type']->id);

                if($ef != 20){
                    foreach ($slot as $s) {
                        $s = explode(",", $s);
                        if($s[3] == 'null'){
                            $gstRate = 0;
                        } else {
                            $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                        }

                        $etHallTimeData = [
                            'fk_et_hall_book'       => $data['et_hall_book']->id,
                            'fk_et_slot_time'       => $s[1],
                            'fk_et_facility_price'  => $s[0],
                            'eht_price'             => $s[2],
                            'eht_total'             => $s[2],
                            'eht_gst_code'          => $s[3],
                            'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
                            'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
                            'created_at'            => date('Y-m-d'),
                            'updated_at'            => date('Y-m-d'),
                        ];
                        $totalSlot += $currentTotal;
                        $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                    }
                } else {    // Ruang Legar
                    $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $bookingId);
                    $data['ehb'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
                    foreach ($data['ehb'] as $a) {
                        foreach ($slot as $s) {
                            $s = explode(",", $s);
                            if($s[3] == 'null'){
                                $gstRate = 0;
                            } else {
                                $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
                            }

                            $etHallTimeData = [
                                'fk_et_hall_book'       => $a->id,
                                'fk_et_slot_time'       => $s[1],
                                'fk_et_facility_price'  => $s[0],
                                'eht_price'             => $s[2],
                                'eht_total'             => $s[2],
                                'eht_gst_code'          => $s[3],
                                'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
                                'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
                                'created_at'            => date('Y-m-d'),
                                'updated_at'            => date('Y-m-d'),
                            ];
                            $totalSlot += $currentTotal;
                            $query = All::InsertGetID('et_hall_time', $etHallTimeData);
                        }
                    }
                }
                $deposit = $totalSlot * ($efd->efd_deposit_percent / 100);

                // begin::Kemas kini harga subtotal 
                // $data_main = [
                //     'bmb_total_book_hall'   => $totalSlot,
                //     'bmb_deposit_rm'        => $deposit,
                //     'bmb_deposit_rounding'  => $deposit,
                //     // 'bmb_rounding'          => $data['main']->bmb_subtotal + $deposit + $totalSlot,
                //     'bmb_subtotal'          => $data['main']->bmb_subtotal + $totalSlot + $data['main']->bmb_total_equipment,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user')['id']
                // ];
                // $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                
                // $data_ebf = [
                //     'ebf_deposit'           => $deposit,
                //     'ebf_subtotal'          => $data['main']->bmb_subtotal + $totalSlot,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::GetUpdate('et_booking_facility', $data['et_booking_facility']->id, $data_ebf);
                
                // $data_ehb = [
                //     'ehb_total'             => $data['main']->bmb_subtotal + $totalSlot,
                //     'updated_at'            => date('Y-m-d'),
                //     'updated_by'            => Session::get('user.id')
                // ];
                // $query = All::GetUpdateSpec('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->id, $data_ehb);
                // end::Kemas kini harga subtotal 
            }
            // $query = Sport::recalculate_booking(Crypt::encrypt($bookingId));

            // return redirect('sport/external/rumusandewan/'. Crypt::encrypt($bookingId));
            return redirect('sport/external/recalculate_booking/'. Crypt::encrypt($bookingId));
        }

        $audit = AuditLog::log(Crypt::encrypt(82), Crypt::encrypt($bookingId), 'Tempahan Internal - Paparan Slot Masa Dewan',1);
        return view('sport.external.halltimeslotuse', compact('data'));
    }

    // public function tambah_kegunaan_dewanOri(Request $request, $bookingId){
    //     $bookingId = Crypt::decrypt($bookingId);
    //     $data['main'] = All::GetRow('main_booking', 'id', $bookingId);
        
    //     $data['date'] = $data['main']->bmb_booking_date;
    //     $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
    //     $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

    //     $data['et_booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $bookingId);
    //     $data['et_booking_facility_detail'] = All::GetRow('et_booking_facility_detail', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));

    //     $data['et_hall_book'] = All::GetRow('et_hall_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'));
    //     $data['et_facility_type'] = All::GetRow('et_facility_type', 'id', $data['et_booking_facility'][0]->fk_et_facility_type);

    //     $data['et_equipment_book'] = All::GetAllRow('et_equipment_book', 'fk_et_booking_facility', $data['et_booking_facility']->pluck('id'))->whereNull('deleted_at');

    //     $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility'][0]->fk_et_facility_detail, $bookingId, $data['et_booking_facility'][0]->ebf_start_date, $data['et_booking_facility'][0]->id);
    //     $data['slot2'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_hall_book']->fk_et_function);
    //     $eft = $data['et_hall_book']->fk_et_function;
        
    //     $data['post'] = 0;
        
    //     if(request()->isMethod('post')){
    //         $data['post'] = 1;
    //         $type = request()->type;
    //         $data['type'] = $type;
    //         $data['date'] = request()->tarikh;
    //         $date = $data['date'];

    //         if($type == 1){
    //             $data['date'] = Carbon::CreateFromFormat('d-m-Y', $data['date'])->format('Y-m-d');
    //             $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility'][0]->fk_et_facility_detail, $bookingId, $data['date'], $data['et_booking_facility'][0]->id);
    //             $data['slot2'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_hall_book']->fk_et_function);
    //             $data['lokasi'] = $data['et_booking_facility'][0]->fk_et_facility_type;
    //             $data['checkroom'] = Sport::getCheckroom($data['et_booking_facility'][0]->fk_et_facility_type);

    //             foreach ($data['checkroom'] as $key => $value) {
    //                 $data['result'] = $value->fk_et_facility;
    //             }
    //             $data['kegunaan'] = Sport::getCheckfacilityfunction($data['et_booking_facility'][0]->fk_et_facility_type);
    //             $data['slot1'] = Sport::getCheckfacilityfunction1($data['et_booking_facility'][0]->fk_et_facility_type);
    //             if($data['result'] == 20){
    //                 $data['slot1'] = array_values(Sport::getChildruangcr($data['et_booking_facility'][0]->fk_et_facility_type, $data['date']))[0];
    //                 $data['slot'] = Sport::getCheckruangfunction($data['et_booking_facility'][0]->fk_et_facility_type);
    //                 $data['slot3'] = Sport::getChildruangcr($data['et_booking_facility'][0]->fk_et_facility_type, $data['date']);
    //                 $facilityDetailIdsToRemove = $data['et_booking_facility']->pluck('fk_et_facility_detail')->toArray();
                    
    //                 $filteredSlot3 = array_filter($data['slot3'], function($item) use ($facilityDetailIdsToRemove) {
    //                     return !in_array($item['id'], $facilityDetailIdsToRemove);
    //                 });
    //                 $data['slot3'] = $filteredSlot3;
    //             }

    //             $data['type'] = 3;
    //         } else if($type == 2){
    //             $totalSlot = 0;
    //             $slot = request()->slot;
    //             $et_function = request()->et_function_id;
    //             $date = $data['date'];
    //             $data['checkroom'] = Sport::getCheckroom($data['et_booking_facility'][0]->fk_et_facility_type);
    //             foreach ($data['checkroom'] as $key => $value) {
    //                 $data['result'] = $value->fk_et_facility;
    //             }

    //             $ifEbfEfexist = Sport::determineEbfEhbEF($data['main']->id, $et_function, $date, $data['result']);
    //             // dd($ifEbfEfexist, $data['main']->id, $et_function, $date);

    //             if(empty($ifEbfEfexist)){
    //                 $data_ebf = [
    //                     'fk_main_booking'           => $data['main']->id,
    //                     'fk_et_facility_type'       => $data['et_booking_facility'][0]->fk_et_facility_type,
    //                     'fk_et_facility_detail'     => $data['et_booking_facility'][0]->fk_et_facility_detail,
    //                     'fk_lkp_discount'           => $data['et_booking_facility'][0]->fk_lkp_discount,
    //                     'ebf_start_date'            => $date,
    //                     'ebf_end_date'              => $date,
    //                     'ebf_no_of_day'             => $data['et_booking_facility'][0]->ebf_no_of_day,
    //                     'ebf_facility_indi'         => $data['et_booking_facility'][0]->ebf_facility_indi,
    //                     'created_at'                => date('Y-m-d H:i:s'),
    //                     'updated_at'                => date('Y-m-d H:i:s'),
    //                 ];
    //                 $ebfid = All::InsertGetID('et_booking_facility', $data_ebf);

    //                 $data_ehb = [
    //                     'fk_et_booking_facility'    => $ebfid,
    //                     'fk_et_function'            => $et_function,
    //                     'ehb_booking_date'          => $date,
    //                     'created_at'                => date('Y-m-d H:i:s'),
    //                     'updated_at'                => date('Y-m-d H:i:s'),
    //                 ];
    //                 $ehbid = All::InsertGetID('et_hall_book', $data_ehb);
    //             } else {
    //                 $ebfid = $ifEbfEfexist->ebfid;
    //                 $ehbid = $ifEbfEfexist->ehbid;
    //             }

    //             // Pakej Majlis perkahwinan
    //             if($et_function == 10){
    //                 try {
    //                     $s = explode(",", $slot[0]);
    //                     $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
    //                     $date = Carbon::createFromFormat('Y-m-d', $date);
    //                     if ($date->isWeekend()) {
    //                         $day_cat = 2;
    //                     } else {
    //                         $day_cat = 1;
    //                     }
    //                     if($day_cat == 1){
    //                         $et_package = All::GetRow('et_package', 'id', 1);
    //                         $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 1)->where('deleted_at', null);

    //                         for ($i=1; $i < 10; $i++) {
    //                             if($i == 1 || $i == 9){
    //                                 $ef = 6;
    //                             } else {
    //                                 $ef = 2;
    //                             }
    //                             $efp = All::GetAllRow('et_facility_price', 'fk_et_facility', 1)->where('fk_et_function', $ef)->where('fk_et_slot_time', $i)->where('fk_lkp_gst_rate', $s[3])->where('fk_lkp_slot_cat', 1)->where('efp_day_cat', $day_cat)->first();
    //                             $etHallTimeData = [
    //                                 'fk_et_hall_book'       => $ehbid,
    //                                 'fk_et_slot_time'       => $i,
    //                                 'fk_et_facility_price'  => $efp->id,
    //                                 'eht_price'             => $efp->efp_unit_price,
    //                                 'eht_total'             => $efp->efp_unit_price,
    //                                 'eht_gst_code'          => $s[3],
    //                                 'eht_gst_rm'            => $gstVal = ($efp->efp_unit_price * $gstRate),
    //                                 'eht_subtotal'          => $currentTotal = ($efp->efp_unit_price + $gstVal),
    //                                 'created_at'            => date('Y-m-d'),
    //                                 'updated_at'            => date('Y-m-d'),
    //                             ];
    //                             $totalSlot += $currentTotal;
    //                             $query = All::InsertGetID('et_hall_time', $etHallTimeData);
    //                         }

    //                         // begin::Kemas kini harga subtotal Main_Booking
    //                         $data_main = [
    //                             'bmb_subtotal'          => $et_package->ep_price,
    //                             'bmb_total_book_hall'   => $et_package->ep_price,
    //                             'bmb_rounding'          => $et_package->ep_price,
    //                             'bmb_deposit_rm'        => 175.00,
    //                             'bmb_deposit_rounding'  => 175.00,
    //                             'updated_at'            => date('Y-m-d'),
    //                             'updated_by'            => Session::get('user')['id']
    //                         ];
    //                         $query = All::GetUpdate('main_booking', $bookingId, $data_main);
    //                         // end::Kemas kini harga subtotal Main_Booking
    //                     } else {
    //                         $et_package = All::GetRow('et_package', 'id', 2);
    //                         $et_package_detail = All::GetAllRow('et_package_detail', 'fk_et_package', 2)->where('deleted_at', null);
    //                         // dd($et_package, $et_package_detail);
                            
    //                         foreach ($et_package_detail as $key => $value) {
    //                             $etHallTimeData = [
    //                                 'fk_et_hall_book'       => $ehbid,
    //                                 'fk_et_slot_time'       => $value->fk_et_slot_time,
    //                                 'fk_et_facility_price'  => $value->fk_et_facility_price,
    //                                 'eht_price'             => $value->epd_price,
    //                                 'eht_total'             => $value->epd_price,
    //                                 'eht_gst_code'          => $s[3],
    //                                 'eht_gst_rm'            => $gstVal = ($value->epd_price * $gstRate),
    //                                 'eht_subtotal'          => $currentTotal = ($value->epd_price + $gstVal),
    //                                 'created_at'            => date('Y-m-d'),
    //                                 'updated_at'            => date('Y-m-d'),
    //                             ];
    //                             $totalSlot += $currentTotal;
    //                             $query = All::InsertGetID('et_hall_time', $etHallTimeData);
    //                         }

    //                         // begin::Kemas kini harga subtotal Main_Booking
    //                         $data_main = [
    //                             'bmb_subtotal'          => $et_package->ep_price,
    //                             'bmb_total_book_hall'   => $et_package->ep_price,
    //                             'bmb_rounding'          => $et_package->ep_price,
    //                             'bmb_deposit_rm'        => 140.00,
    //                             'bmb_deposit_rounding'  => 140.00,
    //                             'updated_at'            => date('Y-m-d'),
    //                             'updated_by'            => Session::get('user')['id']
    //                         ];
    //                         $query = All::GetUpdate('main_booking', $bookingId, $data_main);
    //                         // end::Kemas kini harga subtotal Main_Booking
    //                     }
    //                 } catch (\Throwable $th) {
    //                     dd($th);
    //                 }
    //             } 
    //             // Bukan Majlis perkahwinan 
    //             else {
    //                 $ef = $data['et_facility_type']->fk_et_facility;
    //                 $efd = All::GetRow('et_facility_detail', 'fk_et_facility_type', $data['et_facility_type']->id);
    //                 // dd($request->post(), $value, $data, $subtotal, $slot);

    //                 if($data['result'] == 20){
    //                     $slot = request()->slot[0];
    //                     $s = explode(",", $slot);
    //                     foreach ((json_decode(request()->gelanggang)) as $key => $value) {
    //                         $data_ebf = [
    //                             'fk_main_booking'       => $data['main']->id,
    //                             'fk_et_facility_type'   => $data['et_facility_type']->id,
    //                             'fk_et_facility_detail' => $value,
    //                             'fk_lkp_discount'       => 1,
    //                             'ebf_start_date'        => $data['date'],
    //                             'ebf_end_date'          => $data['date'],
    //                             'ebf_no_of_day'         => 1,
    //                             'ebf_subtotal'          => $s[2],
    //                             'ebf_facility_indi'     => 1,
    //                             'created_at'            => date('Y-m-d H:i:s'),
    //                             'updated_at'            => date('Y-m-d'),
    //                             'updated_by'            => Session::get('user.id')
    //                         ];
    //                         $ebfid = All::InsertGetID('et_booking_facility', $data_ebf);
                            
    //                         $data_ehb = [
    //                             'fk_et_booking_facility'=> $ebfid,
    //                             'fk_et_function'        => request()->et_function_id,
    //                             'ehb_booking_date'      => $data['date'],
    //                             'ehb_total'             => $s[2],
    //                             'created_at'            => date('Y-m-d'),
    //                             'updated_at'            => date('Y-m-d'),
    //                             'updated_by'            => Session::get('user.id')
    //                         ];
    //                         $ehbid = All::InsertGetID('et_hall_book', $data_ehb);
                            
    //                         $dayCat = date('w', strtotime($data['date']));
    //                         if($dayCat == 0 || $dayCat == 6){
    //                             $dayCat = 2;
    //                         } else {
    //                             $dayCat = 1;
    //                         }

    //                         $efpid = All::GetAllRow('et_facility_price', 'fk_et_function', request()->et_function_id)->where('fk_et_slot_time', $s[1])->where('efp_day_cat', $dayCat)->first();
    //                         $gstRate = All::GetRow('lkp_gst_rate', 'id', $efpid->fk_lkp_gst_rate)->lgr_rate;
    //                         $etHallTimeData = [
    //                             'fk_et_hall_book'       => $ehbid,
    //                             'fk_et_slot_time'       => $s[1],
    //                             'fk_et_facility_price'  => $efpid->id,
    //                             'eht_price'             => $efpid->efp_unit_price,
    //                             'eht_total'             => $efpid->efp_unit_price,
    //                             'eht_gst_code'          => $efpid->fk_lkp_gst_rate,
    //                             'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
    //                             'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
    //                             'created_at'            => date('Y-m-d'),
    //                             'updated_at'            => date('Y-m-d'),
    //                         ];
    //                         $totalSlot += $currentTotal;
    //                         $query = All::InsertGetID('et_hall_time', $etHallTimeData);
    //                     }
    //                 } else {
    //                     foreach ($slot as $s) {
    //                         $s = explode(",", $s);
    //                         if($s[3] != 'null'){
    //                             $gstRate = All::GetRow('lkp_gst_rate', 'id', $s[3])->lgr_rate;
    //                         } else {
    //                             $gstRate = 0;
    //                         }
    //                         $etHallTimeData = [
    //                             'fk_et_hall_book'       => $ehbid,
    //                             'fk_et_slot_time'       => $s[1],
    //                             'fk_et_facility_price'  => $s[0],
    //                             'eht_price'             => $s[2],
    //                             'eht_total'             => $s[2],
    //                             'eht_gst_code'          => $s[3],
    //                             'eht_gst_rm'            => $gstVal = ($s[2] * $gstRate),
    //                             'eht_subtotal'          => $currentTotal = ($s[2] + $gstVal),
    //                             'created_at'            => date('Y-m-d'),
    //                             'updated_at'            => date('Y-m-d'),
    //                         ];
    //                         $totalSlot += $currentTotal;
    //                         $query = All::InsertGetID('et_hall_time', $etHallTimeData);
    //                     }
    //                     $deposit = $totalSlot * ($efd->efd_deposit_percent / 100);
        
    //                     // begin::Kemas kini harga subtotal 
    //                     $data_main = [
    //                         'bmb_total_book_hall'   => $totalSlot,
    //                         'bmb_deposit_rm'        => $deposit,
    //                         'bmb_deposit_rounding'  => $deposit,
    //                         // 'bmb_rounding'          => $data['main']->bmb_subtotal + $deposit + $totalSlot,
    //                         'bmb_subtotal'          => $data['main']->bmb_subtotal + $totalSlot + $data['main']->bmb_total_equipment,
    //                         'updated_at'            => date('Y-m-d'),
    //                         'updated_by'            => Session::get('user')['id']
    //                     ];
    //                     $query = All::GetUpdate('main_booking', $bookingId, $data_main);
                        
    //                     $data_ebf = [
    //                         'ebf_deposit'           => $deposit,
    //                         'ebf_subtotal'          => $data['main']->bmb_subtotal + $totalSlot,
    //                         'updated_at'            => date('Y-m-d'),
    //                         'updated_by'            => Session::get('user.id')
    //                     ];
    //                     $query = All::GetUpdate('et_booking_facility', $ebfid, $data_ebf);
                        
    //                     $data_ehb = [
    //                         'ehb_total'             => $data['main']->bmb_subtotal + $totalSlot,
    //                         'updated_at'            => date('Y-m-d'),
    //                         'updated_by'            => Session::get('user.id')
    //                     ];
    //                     $query = All::GetUpdateSpec('et_hall_book', 'fk_et_booking_facility', $ebfid, $data_ehb);
    //                     // end::Kemas kini harga subtotal 
    //                 }
    //             }
                
    //             $query = Sport::recalculate_booking(Crypt::encrypt($bookingId));

    //             return Redirect::to(url('/sport/external/rumusandewan', [Crypt::encrypt($data['main']->id)]));
    //         } else if($type == 3){
    //             $data['et_function_id'] = request()->et_function_id;
    //             $data['date'] = request()->tarikh;

    //             $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility'][0]->fk_et_facility_detail, $bookingId, $data['et_booking_facility'][0]->ebf_start_date, $data['et_booking_facility'][0]->id);

    //             if($data['et_function_id'] != null){
    //                 $data['slot2'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_function_id']);
    //             }
    //             $data['lokasi'] = $data['et_booking_facility'][0]->fk_et_facility_type;
    //             $data['checkroom'] = Sport::getCheckroom($data['et_booking_facility'][0]->fk_et_facility_type);
    //             foreach ($data['checkroom'] as $key => $value) {
    //                 $data['result'] = $value->fk_et_facility;
    //             }

    //             $data['kegunaan'] = Sport::getCheckfacilityfunction($data['et_booking_facility'][0]->fk_et_facility_type);
    //             $data['slot1'] = Sport::getCheckfacilityfunction1($data['et_booking_facility'][0]->fk_et_facility_type);
                
    //             if($data['result'] == 20){
    //                 // $data['slot'] = Sport::getAvailslotroom($data['et_facility_type']->fk_et_facility, $data['et_booking_facility'][0]->fk_et_facility_detail, $bookingId, $data['et_booking_facility'][0]->ebf_start_date, $data['et_booking_facility'][0]->id);
    //                 $data['slot'] = Sport::getAvailslotlist($data['main']->id, $data['date'], $data['et_hall_book']->fk_et_function);
    //                 $data['gelanggang'] = json_encode($request->post('gelanggang'));
    //                 $data['et_function_id'] = request()->fn[0];
    //             }

    //             $data['type'] = request()->type;
    //             $data['type'] = 2;
    //         }
    //     }

    //     return view('sport.external.tambahkegunaandewan', compact('data'));
    // }

    public function deleteSlotDewan($mbid, $id){
        $data['mbid'] = Crypt::decrypt($mbid);
        $data['id'] = Crypt::decrypt($id);

        $data['fk_eht'] = All::GetRow('et_hall_time', 'id', $data['id']);
        $data['et_hall_time'] = All::GetAllRow('et_hall_time', 'fk_et_hall_book', $data['fk_eht']->fk_et_hall_book)->whereNull('deleted_at');

        $data['ehb'] = All::GetRow('et_hall_book', 'id', $data['fk_eht']->fk_et_hall_book);

        $data['ebf'] = All::GetRow('et_booking_facility', 'id', $data['ehb']->fk_et_booking_facility);

        $data['mb'] = All::GetRow('main_booking', 'id', $data['ebf']->fk_main_booking);
        
        $data['ebfAll'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['mb']->id);
        $data['ehbAll'] = All::GetAllRowIn('et_hall_book', 'fk_et_booking_facility', $data['ebfAll']->pluck('id'));

        $hapusSlotDewan = [
            'deleted_at'        => date('Y-m-d H:i:s')
        ];
        $query = All::GetDelete('et_hall_time', $data['id']);

        $mb_update = [
            'bmb_subtotal'          => $data['mb']->bmb_subtotal - $data['fk_eht']->eht_price,
            'bmb_deposit_rm'        => ($data['mb']->bmb_subtotal - $data['fk_eht']->eht_price) * .1,
            'bmb_total_book_hall'   => $data['mb']->bmb_subtotal - $data['fk_eht']->eht_price,
            'updated_at'            => date('Y-m-d'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('main_booking', $data['mb']->id, $mb_update);
        
        $ebf_update = [
            'ebf_subtotal'          => $data['ebf']->ebf_subtotal - $data['fk_eht']->eht_price,
            'ebf_deposit'           => ($data['ebf']->ebf_subtotal - $data['fk_eht']->eht_price) * .1,
            'updated_at'            => date('Y-m-d'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_booking_facility', $data['ebf']->id, $ebf_update);
        
        $ehb_update = [
            'ehb_total'             => $data['ebf']->ebf_subtotal - $data['fk_eht']->eht_price,
            'updated_at'            => date('Y-m-d'),
            'updated_by'            => Session::get('user.id')
        ];
        $query = All::GetUpdate('et_hall_book', $data['ehb']->id, $ehb_update);

        $data['ehtAll'] = All::GetAllRowIn('et_hall_time', 'fk_et_hall_book', $data['ehbAll']->pluck('id'))->whereNull('deleted_at');
        $data['et_hall_time'] = All::GetAllRow('et_hall_time', 'fk_et_hall_book', $data['fk_eht']->fk_et_hall_book)->whereNull('deleted_at');

        if(count($data['ehtAll']) == 0){
            return redirect('sport/external/slotMasa/'. $mbid);
        }

        $query = Sport::recalculate_booking($mbid);
        return redirect('sport/external/rumusandewan/'. $mbid);
    }


    
    public function sport(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['id']);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->sortBy('lc_description');
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        $data['post'] = false;
        $data['lokasi'] = "";

        if(request()->isMethod('post')){
            $data['post'] = true;
            $data['lokasi'] = (int)$request->post('lokasi');
            $data['date'] = date('Y-m-d', strtotime($request->post('tarikh')));
            $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['lokasi']);
            $data['slot'] = Sport::slot($data['lokasi'], $data['date']);
            // dd($data['lokasi'], $data['date'], $data['slot']);

            $audit = AuditLog::log(Crypt::encrypt(60), Crypt::encrypt($data['id']), 'Tempahan External - Tempahan Sukan',1);
        }
        // dd($data);

        return view('sport.external.sport', compact('data'));
    }

    public function sport_slotbook(Request $request, $id, $date, $user){
        $id = Crypt::decrypt($id);
        $date = Crypt::decrypt($date);
        $user = Crypt::decrypt($user);
        $date = date('Y-m-d', strtotime($date));
        $slot = $request->post('slot');
        // dd($slot);

        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        $location = All::GetRow('et_facility_type', 'id', $id);

        $data = array(
            'fk_users'                   => $user,
            'fk_lkp_status'              => 1,
            'fk_lkp_deposit_rate'        => 9,
            'fk_lkp_location'            => $location->fk_lkp_location,
            'fk_lkp_discount_type'       => 5,
            'bmb_booking_no'             => $bookrn,
            'bmb_booking_date'           => date('Y-m-d H:i:s'),
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id']
        );
        $query = All::InsertGetID('main_booking', $data);

        $varCompLocation = 0;
        $i = 0;
        foreach($slot as $s){
            $row = explode(',', $s);
            if($row[0] != $varCompLocation){
                $varCompLocation = $row[0];
                $data_book[$i] = array(
                    'fk_main_booking'            => $query,
                    'fk_et_facility_type'        => $id,
                    'fk_et_facility_detail'      => $varCompLocation,
                    'fk_lkp_discount'            => 1,
                    'ebf_start_date'             => date('Y-m-d', strtotime($row[2])),
                    'ebf_end_date'               => date('Y-m-d', strtotime($row[2])),
                    'ebf_no_of_day'              => 1,
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);

                $data_sport[$i] = array(
                    'fk_et_booking_facility'     => $etBookFacility,
                    'esb_booking_date'           => $row[2],
                    'created_at'                 => date('Y-m-d H:i:s'),
                    'updated_at'                 => date('Y-m-d H:i:s')
                );
                $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
            }
            $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
            $total_gst = $row[4] * $gst->lgr_rate;
            $total = $row[4] + $total_gst;

            $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);

            $data_time[$i] = array(
                'fk_et_sport_book'           => $etSportBook,
                'fk_et_slot_price'           => $slotPrice->id,
                'est_price'                  => $row[4],
                'est_discount_type_rm'       => 0.00,
                'est_discount_rm'            => 0.00,
                'est_total'                  => $row[4],
                'est_gst_code'               => $row[5],
                'est_gst_rm'                 => $total_gst,
                'est_subtotal'               => $total,
                'created_at'                 => date('Y-m-d H:i:s'),
                'updated_at'                 => date('Y-m-d H:i:s')
            );
            $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
            $i++;
        }

        if ($etSportTime) {
            $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($query), 'Tempahan External - Tempah Sukan',1);
            // $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($id, 'Tempahan External - Tempah Sukan', 1));
            return Redirect::to(url('sport/external/tempahan', [Crypt::encrypt($query)]));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport'));
        }
    }

    public function tempahan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['eft'] = $data['slot'][0]->fk_et_facility_type;

        $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($data['booking']), 'Teruskan Tempahan = '. $data['main']->bmb_booking_no, 1);

        return view('sport.external.tempahan', compact('data'));
    }

    public function tambah_kegunaan(Request $request, $id){
        $data['booking'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['slotcurrent'] = $data['slot'];
        $data['eft'] = $data['slot'][0]->fk_et_facility_type;
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('id', $data['main']->fk_lkp_location);
        $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['eft']);
        $data['post'] = false;

        if(request()->isMethod('post')){
            $data['post'] = true;
            if(request()->type == 1){
                $data['lokasi'] = $data['eft'];
                $data['date'] = date('Y-m-d', strtotime($request->post('tarikh')));
                // $data['fasility'] = All::GetRow('et_facility_type', 'id', $data['lokasi']);
                $data['slot'] = Sport::slotByMainBooking($data['lokasi'], $data['date'], null, $data['booking']);
            } else if(request()->type == 2) {
                $slot = $request->post('slot');
                $varCompLocation = 0;
                $i = 0;
                foreach($slot as $s){
                    $row = explode(',', $s);
                    $ebfDate = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['booking'])->where('ebf_start_date', $row[2])->where('fk_et_facility_detail', $row[0])->first();
                    // dd($row, $ebfDate, $row[0]);
                    if(!$ebfDate){
                        // dd('1');
                        if($row[0] != $varCompLocation){
                            $varCompLocation = $row[0];
                            $data_book[$i] = array(
                                'fk_main_booking'            => $data['booking'],
                                'fk_et_facility_type'        => $data['eft'],
                                'fk_et_facility_detail'      => $varCompLocation,
                                'fk_lkp_discount'            => 1,
                                'ebf_start_date'             => date('Y-m-d', strtotime($row[2])),
                                'ebf_end_date'               => date('Y-m-d', strtotime($row[2])),
                                'ebf_no_of_day'              => 1,
                                'created_at'                 => date('Y-m-d H:i:s'),
                                'updated_at'                 => date('Y-m-d H:i:s')
                            );
                            $etBookFacility = All::InsertGetID('et_booking_facility', $data_book[$i]);
            
                            $data_sport[$i] = array(
                                'fk_et_booking_facility'     => $etBookFacility,
                                'esb_booking_date'           => $row[2],
                                'created_at'                 => date('Y-m-d H:i:s'),
                                'updated_at'                 => date('Y-m-d H:i:s')
                            );
                            $etSportBook = All::InsertGetID('et_sport_book', $data_sport[$i]); 
                        }
                    } else {
                        $etSportBook = All::GetAllRow('et_sport_book', 'fk_et_booking_facility', $ebfDate->id)->where('esb_booking_date', $row[2])->first()->id;
                        // dd('2', $ebfDate, $etSportBook, $row[4]);
                    }
                    $gst = All::GetRow('lkp_gst_rate', 'id', $row[5]);
                    $total_gst = $row[4] * $gst->lgr_rate;
                    $total = $row[4] + $total_gst;
        
                    $slotPrice = All::GetRow('et_slot_price', 'fk_et_facility_price', $row[6]);
        
                    $data_time[$i] = array(
                        'fk_et_sport_book'           => $etSportBook,
                        'fk_et_slot_price'           => $slotPrice->id,
                        'est_price'                  => $row[4],
                        'est_discount_type_rm'       => 0.00,
                        'est_discount_rm'            => 0.00,
                        'est_total'                  => $row[4],
                        'est_gst_code'               => $row[5],
                        'est_gst_rm'                 => $total_gst,
                        'est_subtotal'               => $total,
                        'created_at'                 => date('Y-m-d H:i:s'),
                        'updated_at'                 => date('Y-m-d H:i:s')
                    );
                    $etSportTime = All::InsertGetID('et_sport_time', $data_time[$i]); 
                    $i++;
                }
                return redirect('/sport/external/tempahan/'. Crypt::encrypt($data['booking']));
            }
            
            // $audit = AuditLog::log(Crypt::encrypt(60), Crypt::encrypt($data['id']), 'Tempahan External - Tempahan Sukan',1);
        }

        // $audit = AuditLog::log(Crypt::encrypt(64), Crypt::encrypt($data['booking']), 'Teruskan Tempahan = '. $data['main']->bmb_booking_no, 1);
        // dd($data);
        return view('sport.external.tambahkegunaan', compact('data'));
    }

    public function tempahan_delete($booking, $id, $esbid){
        $id = Crypt::decrypt($id);
        $esbid = Crypt::decrypt($esbid);
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);

        $query = All::GetDelete('et_sport_time', $id);
        $data['est'] = All::GetAllRow('et_sport_time', 'fk_et_sport_book', $esbid);

        if (count($data['est']) == 0){
            $fk_users = $data['main']->fk_users;
            return redirect('/sport/external/sport/'.  Crypt::encrypt($fk_users));
        };
        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(65), Crypt::encrypt($id), 'Batal Tempahan - '. $data['main']->bmb_booking_no . ', et_sport_time - ' . $id, 1);
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport/external/tempahan', $booking));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/external/tempahan', $booking));
        }
    }

    public function rumusan($booking){
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        $data['slot'] = Sport::priceSlot($data['booking']);
        $data['tax'] = All::Show('lkp_tax', 'lt_name', 'ASC');

        $audit = AuditLog::log(Crypt::encrypt(71), Crypt::encrypt($data['booking']), 'Rumusan Tempahan = '. $data['main']->bmb_booking_no, 1);

        return view('sport.external.rumusan', compact('data'));
    }

    public function bayaran(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['user_detail'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);
        
        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['booking']);
        $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebf']->pluck('id'));
        $data['est'] = All::GetAllRowIn('et_sport_time', 'fk_et_sport_book', $data['esb']->pluck('id'));

        // Validate if date/location/slot time is booked yet
        // $data['confirm_booking'] = All::GetAllRowIn('et_confirm_booking_detail', 'ecbd_date_booking', $data['esb']->pluck('esb_booking_date'))->whereIn('fk_et_facility_detail', $data['ebf']->pluck('fk_et_facility_detail'))->whereIn('fk_et_facility_type', $data['ebf']->pluck('fk_et_facility_type'))->whereIn('fk_et_slot_time', (All::GetRow('et_slot_price', 'id', $data['est']->pluck('fk_et_slot_price')))->fk_et_slot_time);
        // if($data['confirm_booking'] || count($data['confirm_booking'] > 0)){
        //     return redirect()->back();
        // }
        // Validate if date/location/slot time is booked yet

        $data['bayaran'] = All::Show('lkp_payment_mode', 'id', 'ASC')->where('lpm_status', 1)->where('id', '!=', 4);
        $data['total'] = $request->post('total');

        $data_main = array(
            'fk_lkp_status'              => 2,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);
        $data['main']->fk_lkp_status = 2;

        $audit = AuditLog::log(Crypt::encrypt(72), Crypt::encrypt($data['booking']), 'Pembayaran Tempahan = '. $data['main']->bmb_booking_no, 1);

        return view('sport.external.bayaran', compact('data'));
    }

    public function submit_bayaran(Request $request, $booking)
    {
        $data['booking'] = Crypt::decrypt($booking);
        $data['total'] = $request->post('total');
        $data['main'] = All::GetRow('main_booking', 'id', $data['booking']);
        $data['ebfid'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['booking']);
        $data['ebfUnique'] = $data['ebfid']->unique('ebf_start_date')->values()->all();
        $data['facility_type'] = All::GetRow('et_facility_type', 'id', $data['ebfid'][0]->fk_et_facility_type)->fk_et_facility;
        $data['esb'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebfid']->pluck('id'));
        $data['est'] = All::GetAllRowIn('et_sport_time', 'fk_et_sport_book', $data['esb']->pluck('id'));
        $data['test'] = Sport::SportByEfdDate($data['booking']);

        // if($data['facility_type'] != 29){
        //     $data['esb_booking_date'] = All::GetAllRowIn('et_sport_book', 'fk_et_booking_facility', $data['ebfid']->id)->esb_booking_date;
        // } else {
        //     $data['esb_booking_date'] = $data['ebfid']->ebf_start_date;
        // }

        $no = Sport::generatereceipt();
        
        $data_fpx = array(
            'fk_main_booking'           => $data['booking'],
            'fk_lkp_payment_type'       => 2,
            'fk_lkp_payment_mode'       => $request->post('bayaran'),
            'fk_users'                  => $data['main']->fk_users,
            'bp_total_amount'           => $data['total'],
            'bp_deposit'                => 0,
            'bp_paid_amount'            => $data['total'],
            'bp_receipt_number'         => sprintf('%07d', $no),
            'bp_payment_ref_no'         => $request->post('ref'),
            'bp_receipt_date'           => date('Y-m-d H:i:s'),
            'no_lopo'                   => $request->post('lopo') ? $request->post('lopo') : '',
            'amount_received'           => $data['total'],
            'bp_subtotal'               => $data['total'],
            'no_cek'                    => $request->post('nocek'),
            'nama_bank'                 => $request->post('namabank'),
            'bp_payment_status'         => 1,
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s'),
            'updated_by'                => Session::get('user')['id'],
        );
        $query = All::InsertGetID('bh_payment', $data_fpx);

        $data_main = array(
            'fk_lkp_status'              => 5,
            'bmb_subtotal'               => $data['total'],
            'bmb_rounding'               => 0,
            'updated_at'                 => date('Y-m-d H:i:s'),
            'updated_by'                 => Session::get('user')['id'],
        );
        $query = All::GetUpdate('main_booking', $data['booking'], $data_main);

        $ecb = [];
        foreach ($data['ebfUnique'] as $key => $value) {
            $ebfDate = $value->ebf_start_date;
            $data_confirm_booking = array(
                'fk_main_booking'           => $value->fk_main_booking,
                'fk_et_facility_type'       => $value->fk_et_facility_type,
                'ecb_date_booking'          => $value->ebf_start_date,
                'ecb_flag_indicator'        => 1,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            );
            $ecbid = All::InsertGetID('et_confirm_booking', $data_confirm_booking);
            $ecb[$ecbid] = $ebfDate;
        }
        foreach ($ecb as $key => $value) {
            foreach ($data['test'] as $key1 => $value1) {
                if($value1->esb_booking_date == $value){
                    $data_confirm_booking_d = array(
                        'fk_et_confirm_booking'     => $ecbid,
                        'fk_et_facility_detail'     => $value1->fk_et_facility_detail,
                        'fk_et_slot_time'           => All::GetRow('et_slot_price', 'id', $value1->fk_et_slot_price)->fk_et_slot_time,
                        'ecbd_date_booking'         => $value,
                        'fk_et_facility_type'       => $value1->fk_et_facility_type,
                        'created_at'                => date('Y-m-d H:i:s'),
                        'updated_at'                => date('Y-m-d H:i:s'),
                    );
                    $query = All::Insert('et_confirm_booking_detail', $data_confirm_booking_d);
                } 
                // else {
                //     dd($key, $value, $value1->esb_booking_date);
                // }
            }
        }        

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(122), Crypt::encrypt($data['booking']), 'Bayaran Penuh Diterima');
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/maklumatbayaran', Crypt::encrypt($data['booking'])));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facilitytype'));
        }
    }



    public function equipment(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['post'] = 0;
        $data['user'] = All::GetRow('users', 'id', $data['id']);
        $data['userProfile'] = All::GetRow('user_profiles', 'fk_users', $data['user']->id);
        $data['equipment'] = All::GetRow('et_equipment', 'fk_lkp_location', $data['id']);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        $data['presint'] = All::ShowAll('lkp_presint', 'id', 'ASC');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id','!=',1);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });

        if(request()->isMethod('post')){
            $data['post'] = 1;
            $data['kategori'] = request()->kategori;
            $data['lokasi'] = request()->lokasi;
            $data['date'] = request()->tarikh;
            $data['lokasiData'] = All::GetRow('lkp_location', 'id', $data['lokasi']);
            $data['presint'] = All::ShowAll('lkp_presint');
            $data['equipment_int'] = Sport::getEquipdalam($data['lokasiData']->id);
            $data['equipment_ext'] = Sport::getEquipluar($data['lokasiData']->id);

            $data['kj'] = request()->kementerianjabatan;
            $data['staffid'] = request()->staffid;
            $data['aj'] = request()->agensijabatan;

            if(count($data['equipment_int']) == 0 && count($data['equipment_ext']) == 0){
                $data['post'] = 2;
            }
            
            $data['kategori'] = Crypt::encrypt($data['kategori']);
            $data['lokasi'] = Crypt::encrypt($data['lokasi']);
            // $audit = AuditLog::log(Crypt::encrypt(51), null, 'Buka paparan utama Tempahan Peralatan');
        }

        return view('sport.external.equipment', compact('data'));
    }

    public function equipmentSubmit(Request $request, $id, $userid){
        $id = Crypt::decrypt($id);
        $userid = Crypt::decrypt($userid);
        $kategori = Crypt::decrypt(request()->kategori);
        $data['lokasi'] = Crypt::decrypt(request()->lokasi);
        $data['kategori'] = ($kategori == 6 ? 2 : ($kategori == 7 ? 3 : ($kategori == 8 ? 4 : $kategori)));
        $data['date'] = request()->date;
        $data['presint'] = request()->presint;
        $data['address'] = request()->address;
        $data['nama'] = request()->nama;
        $data['noTel'] = request()->noTel;
        $data['dalamanID'] = [];
        $data['luaranID'] = [];
        $totalAmount = 0;
        foreach ($request->post() as $key => $value) {
            if (strpos($key, 'dalaman_') === 0) {
                if ($value !== null && $value !== 0) {
                    $id = intval(substr($key, strlen('dalaman_')));
                    $data['dalamanID'][$id] = $value;
                }
            }
        }
        foreach ($request->post() as $key => $value) {
            if (strpos($key, 'luaran_') === 0) {
                if($value != null && $value != 0){
                    $id = intval(substr($key, strlen('luaran_')));
                    $data['luaranID'][$id] = $value;
                }
            }
        }
        $dayCat = date('w', strtotime($data['date']));
        if($dayCat == 0 || $dayCat == 6){
            $dayCat = 2;
        } else {
            $dayCat = 1;
        }
        // $data['date'] = Carbon::createFromFormat('d-m-Y', request()->date)->format('Y-m-d');
        
        //No Booking Sukan
        $running =  Sport::getRn();
        $rn = sprintf( '%05d', $running);
        $year = date('y');
        $md = date('md');
        $bookrn='SPS'.$year.$md.$rn;

        // main_booking
        $datamb = array(
            'fk_users'                  => $userid,
            'fk_lkp_status'             => 1,
            'fk_lkp_deposit_rate'       => 9,
            'fk_lkp_location'           => $data['lokasi'],
            'fk_lkp_discount_type'      => 5,
            'bmb_booking_no'            => $bookrn,
            'bmb_booking_date'          => date('Y-m-d 00:00:00'),
            'bmb_staff_id'              => request()->staffid,
            'bmb_kementerian'           => request()->kementerianjabatan,
            'bmb_agensi'                => request()->agensijabatan,
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $mbid = All::InsertGetID('main_booking', $datamb);

        // et_booking_facility
        $dataebf = array(
            'fk_main_booking'            => $mbid,
            'fk_et_facility_type'        => 53,
            'fk_et_facility_detail'      => 119,
            'fk_lkp_discount'            => 1,
            'ebf_start_date'             => $data['date'],
            'ebf_end_date'               => $data['date'],
            'ebf_no_of_day'              => 1,
            'created_at'                 => date('Y-m-d H:i:s'),
            'updated_at'                 => date('Y-m-d H:i:s')
        );
        $ebfid = All::InsertGetID('et_booking_facility', $dataebf);

        // et_booking_facility_detail
        $dataebfd = array(
            'fk_et_booking_facility'    => $ebfid,
            'fk_lkp_event'              => 9,
            'ebfd_event_name'           => 'Tempahan Peralatan',
            'ebfd_event_desc'           => 'Tempahan Peralatan',
            'ebfd_others'               => 'Tempahan Peralatan',
            // 'ebfd_total_pax'            => , // null for equipment
            'ebfd_user_apply'           => request()->nama,
            'ebfd_venue'                => 'Presint ' . request()->presint,
            'ebfd_address'              => request()->address,
            'ebfd_contact_no'           => request()->noTel,
            // 'ebfd_vvip'                 => , // 
            // 'ebfd_vip'                  => , // 
            // 'ebfd_participant'          => , // 
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $ebfdid = All::InsertGetID('et_booking_facility_detail', $dataebfd);

        // et_equipment_book
        foreach ($data['dalamanID'] as $key => $quantity) {
            $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 8)->where('eep_day_cat', $dayCat)->first();
            $equipmentName = All::GetRow('et_equipment', 'id', $equipmentPrice->fk_et_equipment)->ee_name;
            $lkp_gst_rate = 1 - (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
            $total = $equipmentPrice->eep_unit_price * $quantity;
            $totalAmount += $total;
            $dataebfd = array(
                'fk_et_booking_facility'    => $ebfid,
                'fk_et_function'            => 8,
                'fk_et_equipment'           => $key,
                'fk_et_equipment_price'     => $equipmentPrice->id,
                'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                'eeb_booking_date'          => $data['date'],
                'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                'eeb_quantity'              => $quantity,
                'eeb_total_price'           => $total,
                'eeb_discount_type_rm'      => 0.00,
                'eeb_discount_rm'           => 0.00,
                // 'eeb_special_disc'          => ,
                // 'eeb_special_disc_rm'       => ,
                'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                'eeb_gst_rm'                => $total * $lkp_gst_rate,
                'eeb_subtotal'              => $total,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $query = All::InsertGetID('et_equipment_book', $dataebfd);
            $audit = AuditLog::log(Crypt::encrypt(67), Crypt::encrypt($id), 'Tambah Peralatan Luaran : '. $equipmentName . '|' . $query);
        }
        foreach ($data['luaranID'] as $key => $quantity) {
            $equipmentPrice = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $key)->where('fk_et_function', 9)->where('eep_day_cat', $dayCat)->first();
            $equipmentName = All::GetRow('et_equipment', 'id', $equipmentPrice->fk_et_equipment)->ee_name;
            $lkp_gst_rate = 1 - (All::GetRow('lkp_gst_rate', 'id', $equipmentPrice->fk_lkp_gst_rate)->lgr_rate);
            $total = $equipmentPrice->eep_unit_price * $quantity;
            $totalAmount += $total;
            $dataebfd = array(
                'fk_et_booking_facility'    => $ebfid,
                'fk_et_function'            => 9,
                'fk_et_equipment'           => $key,
                'fk_et_equipment_price'     => $equipmentPrice->id,
                'fk_lkp_gst_rate'           => $equipmentPrice->fk_lkp_gst_rate,
                'eeb_booking_date'          => $data['date'],
                'eeb_unit_price'            => $equipmentPrice->eep_unit_price,
                'eeb_quantity'              => $quantity,
                'eeb_total_price'           => $total,
                'eeb_discount_type_rm'      => 0.00,
                'eeb_discount_rm'           => 0.00,
                // 'eeb_special_disc'          => ,
                // 'eeb_special_disc_rm'       => ,
                'eeb_total'                 => $total - ($total * $lkp_gst_rate),
                'eeb_gst_rm'                => $total * $lkp_gst_rate,
                'eeb_subtotal'              => $total,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s')
            );
            $query = All::InsertGetID('et_equipment_book', $dataebfd);
            $audit = AuditLog::log(Crypt::encrypt(67), Crypt::encrypt($id), 'Tambah Peralatan Luaran : '. $equipmentName . '|' . $query);
        }

        // bh_quotation
        $quono = Hall::generatequono();
        $presint = All::GetRow('lkp_location', 'id', $data['lokasi'])->lc_description;
        $databq = array(
            'fk_main_booking'           => $mbid,
            'fk_users'                  => $userid,
            'fk_lkp_discount_type'      => 5,
            'bq_quotation_no'           => 'PPj/' .$presint.'/'.$bookrn.'/'.$quono,
            'bq_quotation_date'         => $data['date'],
            'bq_quotation_status'       => 1,
            'bq_total_amount'           => $totalAmount,
            'bq_payment_status'         => 0,
            'bq_quotation_date'           => date('Y-m-d H:i:s'),
            'created_at'                => date('Y-m-d H:i:s'),
            'updated_at'                => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('bh_quotation', $databq);

        $audit = AuditLog::log(Crypt::encrypt(54), Crypt::encrypt($id), 'Jana Quotation Baru : '. $query);

        return Redirect::to(url('sport/external/equipmentSlot', [Crypt::encrypt($mbid)]));
    }

    public function equipmentSlot(Request $request, $id){
        $data['id'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['id']);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['userProfile'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['booking_facility']->pluck('id'));
        $data['equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['booking_facility']->pluck('id'))->where('deleted_at', null);
        
        $data['equipment_int'] = Sport::getEquipdalam($data['main']->fk_lkp_location);
        $data['equipment_ext'] = Sport::getEquipluar($data['main']->fk_lkp_location);

        $data['presint'] = All::ShowAll('lkp_presint', 'id', 'ASC');
        if(request()->isMethod('post')){
            $type = request()->type;
            if($type == 1){
                $date = request()->tarikh;

                $newEbf = [
                    'fk_main_booking'       => $data['id'],
                    'fk_et_facility_type'   => $data['booking_facility'][0]->fk_et_facility_type,
                    'fk_et_facility_detail' => $data['booking_facility'][0]->fk_et_facility_detail,
                    'fk_lkp_discount'       => $data['booking_facility'][0]->fk_lkp_discount,
                    'ebf_start_date'        => $date,
                    'ebf_end_date'          => $date,
                    'ebf_no_of_day'         => 1,
                    'created_at'            => date('Y-m-d'),
                    'updated_at'            => date('Y-m-d')
                ];
                $query = All::InsertGetID('et_booking_facility', $newEbf);
            } else if($type == 2){
                $ifEbfdExist = All::GetRow('et_booking_facility_detail', 'id', request()->itemId);
                if(!$ifEbfdExist){
                    $newEbfd = [
                        'fk_et_booking_facility'    => request()->itemId,
                        'fk_lkp_event'              => 9,
                        'ebfd_event_name'           => 'Tempahan Peralatan',
                        'ebfd_event_desc'           => 'Tempahan Peralatan',
                        'ebfd_others'               => 'Tempahan Peralatan',
                        'ebfd_user_apply'           => request()->namapemohon,
                        'ebfd_venue'                => request()->lokasi,
                        'ebfd_address'              => request()->kegunaan,
                        'ebfd_contact_no'           => request()->notelefon,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d')
                    ];
                    $query = All::InsertGetID('et_booking_facility_detail', $newEbfd);
                } else {
                    $newEbfd = [
                        'fk_et_booking_facility'    => request()->itemId,
                        'fk_lkp_event'              => 9,
                        'ebfd_event_name'           => 'Tempahan Peralatan',
                        'ebfd_event_desc'           => 'Tempahan Peralatan',
                        'ebfd_others'               => 'Tempahan Peralatan',
                        'ebfd_user_apply'           => request()->namapemohon,
                        'ebfd_venue'                => request()->lokasi,
                        'ebfd_address'              => request()->kegunaan,
                        'ebfd_contact_no'           => request()->notelefon,
                        'created_at'                => date('Y-m-d'),
                        'updated_at'                => date('Y-m-d')
                    ];
                    $query = All::GetUpdate('et_booking_facility_detail', request()->itemId, $newEbfd);
                }
            } else if($type == 3){
                $kegunaan = request()->kegunaan;
                $kuantiti = request()->kuantiti;

                if($kegunaan == 1){         // Dalaman
                    $ebfID = request()->itemId;
                    $eqid = request()->dalaman;

                    $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                    $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                    if ($date->isWeekend()) {
                        $day_cat = 2;
                    } else {
                        $day_cat = 1;
                    }

                    $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 8)->where('eep_day_cat', $day_cat)->first();
                    $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                    $data_eq = [
                        'fk_et_booking_facility'        => $ebfID,
                        'fk_et_function'                => 8,
                        'fk_et_equipment'               => $eqid,
                        'fk_et_equipment_price'         => $eq_price->id,
                        'fk_lkp_gst_rate'               => $gst_rate->id,
                        'eeb_booking_date'              => $ebf_date,
                        'eeb_unit_price'                => $eq_price->eep_unit_price,
                        'eeb_quantity'                  => $kuantiti,
                        'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                        // 'eeb_discount_type_rm'          => ,
                        // 'eeb_discount_rm'               => ,
                        'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                        'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                        'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                        'created_at'                    => date('Y-m-d H:i:s'),
                        'updated_at'                    => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_equipment_book', $data_eq);
                } else if($kegunaan == 2){  // Luaran
                    $ebfID = request()->itemId;
                    $eqid = request()->dalaman;

                    $ebf_date = All::GetRow('et_booking_facility', 'id', $ebfID)->ebf_start_date;
                    $date = Carbon::createFromFormat('Y-m-d', $ebf_date);
                    if ($date->isWeekend()) {
                        $day_cat = 2;
                    } else {
                        $day_cat = 1;
                    }

                    $eq_price = All::GetAllRow('et_equipment_price', 'fk_et_equipment', $eqid)->where('fk_et_function', 9)->where('eep_day_cat', $day_cat)->first();
                    $gst_rate = All::GetRow('lkp_gst_rate', 'id', $eq_price->fk_lkp_gst_rate);

                    $data_eq = [
                        'fk_et_booking_facility'        => $ebfID,
                        'fk_et_function'                => 9,
                        'fk_et_equipment'               => $eqid,
                        'fk_et_equipment_price'         => $eq_price->id,
                        'fk_lkp_gst_rate'               => $gst_rate->id,
                        'eeb_booking_date'              => $ebf_date,
                        'eeb_unit_price'                => $eq_price->eep_unit_price,
                        'eeb_quantity'                  => $kuantiti,
                        'eeb_total_price'               => $eq_price->eep_unit_price * $kuantiti,
                        // 'eeb_discount_type_rm'          => ,
                        // 'eeb_discount_rm'               => ,
                        'eeb_total'                     => $eq_price->eep_unit_price * $kuantiti,
                        'eeb_gst_rm'                    => ($eq_price->eep_unit_price * $kuantiti) * $gst_rate->lgr_rate,
                        'eeb_subtotal'                  => $eq_price->eep_unit_price * $kuantiti,
                        'created_at'                    => date('Y-m-d H:i:s'),
                        'updated_at'                    => date('Y-m-d H:i:s'),
                    ];
                    $query = All::InsertGetID('et_equipment_book', $data_eq);

                }
            }
            return redirect()->back();
        }

        return view('sport.external.equipmentSlot', compact('data'));
    }

    public function equipmentSummary($id){
        $data['id'] = Crypt::decrypt($id);
        $data['main'] = All::GetRow('main_booking', 'id', $data['id']);

        $data['user'] = All::GetRow('users', 'id', $data['main']->fk_users);
        $data['userProfile'] = All::GetRow('user_profiles', 'fk_users', $data['main']->fk_users);

        $data['booking_facility'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['booking_facility_detail'] = All::GetAllRowIn('et_booking_facility_detail', 'fk_et_booking_facility', $data['booking_facility']->pluck('id'));
        $data['equipment_book'] = All::GetAllRowIn('et_equipment_book', 'fk_et_booking_facility', $data['booking_facility']->pluck('id'))->where('deleted_at', null);

        $audit = AuditLog::log(Crypt::encrypt(56), Crypt::encrypt($data['id']), 'Teruskan Ke Proses Pembayaran', 1);

        return view('sport.external.equipmentSummary', compact('data'));
    }


    public function equipmentDelete($id, $eqid){
        $data['eqid'] = Crypt::decrypt($eqid);
        $delete = [
            'deleted_at'    => date('Y-m-d')
        ];
        $query = All::GetUpdate('et_equipment_book', $data['eqid'], $delete);
        $audit = AuditLog::log(Crypt::encrypt(127), Crypt::encrypt($data['eqid']), 'Padam Peralatan');

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(67), Crypt::encrypt($data['eqid']), 'Padam Maklumat Peralatan - External');
            Session::flash('flash', 'Updated'); 
            return Redirect::to(url('/sport/external/equipmentSlot', $id));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/external/equipmentSlot', $id));
        }
    }

    public function equipmentAdd ($id){
        $data['id'] = Crypt::decrypt($id);
        $data['discount'] = All::Show('lkp_discount_type', 'id', 'ASC')->where('ldt_indicator', 1);
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_type', 2)->where('lc_status', 1)->where('id','!=',1);
        $data['location'] = $data['location']->sortBy(function($location) {
            preg_match('/\d+/', $location->lc_description, $matches);
            return isset($matches[0]) ? (int)$matches[0] : 0;
        });
        $data['post'] = 1;

        $data['main'] = All::GetRow('main_booking', 'id', $data['id']);
        $data['lokasiData'] = All::GetRow('lkp_location', 'id', $data['main']->fk_lkp_location);

        $data['ebf'] = All::GetAllRow('et_booking_facility', 'fk_main_booking', $data['main']->id);
        $data['date'] = $data['ebf'][0]->ebf_start_date;
        
        $data['equipment_int'] = Sport::getEquipdalam($data['main']->fk_lkp_location);
        $data['equipment_ext'] = Sport::getEquipluar($data['main']->fk_lkp_location);
        
        return view('sport.external.equipmentAdd', compact('data'));
    }
}

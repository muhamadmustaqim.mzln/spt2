<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class TypeFunctionController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_function', 'updated_at', 'DESC');
        return view('sport.functiontype.lists', compact('data'));
    }

    public function form(){
        return view('sport.functiontype.form');
    }

    public function add(Request $request){
        $data = array(
            'ef_desc'           => request()->nama,
            'ef_type'           => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_function', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(156), Crypt::encrypt($query), 'Sukan - Tambah Jenis Fungsi Fasiliti = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['list'] = All::GetRow('et_function', 'id', $id);
        return view('sport.functiontype.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'ef_desc'           => request()->nama,
            'ef_type'           => request()->jenis,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_function', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(157), Crypt::encrypt($id), 'Sukan - Kemas kini Jenis Fungsi Fasiliti = '. request()->nama,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_function', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(158), Crypt::encrypt($id), 'Sukan - Padam Jenis Fungsi Fasiliti = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/functiontype'));
        }
    }
}

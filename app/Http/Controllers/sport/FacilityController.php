<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use App\Models\All;
use App\Models\Sport;
use App\Models\AuditLog;
use App\Helpers\Helper as HP;

class FacilityController extends Controller
{
    public function show() {
        // $data['facility'] = Sport::pengurusanFasiliti();
        // $data['facility'] = All::Show('et_facility_type', 'updated_at', 'DESC');
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['facility'] = All::Show('et_facility_type', 'updated_at', 'DESC');
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $SportLocation = (array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]));
            foreach($SportLocation as $key => $value){
                $SportLocation[$key] = HP::role_location($value);
            }
            $data['facility'] = All::Show('et_facility_type', 'updated_at', 'DESC')->whereIn('fk_lkp_location', $SportLocation);
        } else {
            return redirect('/');
        }

        return view('sport.facility.lists', compact('data'));
    }

    public function form(){
        $data['uuid'] = Str::uuid()->toString();
        $data['facility'] = All::Show('et_facility', 'id', 'ASC')->sortBy('ef_desc');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray)->sortBy('lc_description');
        $data['facility_type'] = All::Show('et_facility_type', 'id', 'ASC');
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');

        return view('sport.facility.form_2', compact('data'));
    }

    public function add(Request $request){
        $uuid = $request->post('uuidKey');

        $data = array(
            'eft_type_desc'     => $request->post('namaFasiliti'),
            'fk_lkp_location'   => $request->post('lokasi'),
            'fk_et_facility'    => $request->post('jenis'),
            'eft_status'        => $request->post('status'),
            'highlight'         => $request->post('highlight'),
            'eft_uuid'          => $request->post('uuidKey'),
            'longitude'         => $request->post('longitude'),
            'latitude'          => $request->post('latitude'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        // dd($data);
        // dd($request->all());
        $query = All::Insert('et_facility_type', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(129), Crypt::encrypt($query), 'Tambah Rekod Fasiliti = '. $request->post('namaFasiliti'),null);
            Session::flash('flash', 'Success'); 
            // $auditData = [
            //     'fk_users'      => Session::get('user')['id'],
            //     'fk_lkp_task'   => 103,
            //     // 'table_ref_id'  => use returned id after add new data
            //     'task'          => 'Tambah Lokasi Baru',
            //     'created_at'        => date('Y-m-d H:i:s'),
            //     'updated_at'        => date('Y-m-d H:i:s'),
            // ];
            // All::Insert('audit_trail', $auditData);
            return Redirect::to(url('/sport/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facility'));
        }
        
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['facility'] = All::Show('et_facility', 'id', 'ASC')->sortBy('ef_desc');
        $data['location'] = All::Show('lkp_location', 'id', 'ASC')->where('lc_status', 1);
        // Convert the collection to an array
        $locationArray = $data['location']->all();
        // Sort the array by the length of the lc_description
        usort($locationArray, function($a, $b) {
            return strlen($a->lc_description) - strlen($b->lc_description);
        });
        // Convert the array back to a collection
        $data['location'] = collect($locationArray)->sortBy('lc_description');
        $data['list'] = All::GetRow('et_facility_type', 'id', $id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');

        return view('sport.facility.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $uuid = $request->post('uuidKey');

        // dd($request->hasFile('file'), $request->hasFile('file1'));
        if ($request->hasFile('file')) {
            try {
                $image = $request->file('file');
                $filename = $image->getClientOriginalName();
                $path = public_path('assets/upload/main/'. $uuid);
                File::ensureDirectoryExists($path);
                // dd($image, $filename, $path);
                $request->file('file')->move($path, $filename);

                $data = array(
                    'cover_img'         => $filename,
                    'eft_uuid'          => $uuid,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => Session::get('user')['id'],
                );
                $query = All::GetUpdate('et_facility_type', $id, $data);
            } catch (\Throwable $th) {
                // dd($th);
            }
        }
        
        if ($request->hasFile('file1')) {
            // $path1 = public_path('assets/upload/virtual/'.$uuid);
            $path1 = public_path('assets/upload/virtual/360_Image_PPj/Panorama/');
            File::ensureDirectoryExists($path1);
            $image1 = $request->file('file1');
            $filename1 = $image1->getClientOriginalName();
            $request->file('file1')->move($path1, $filename1);

            $data = array(
                'virtual_img'       => $filename1,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => Session::get('user')['id'],
            );
            $query = All::GetUpdate('et_facility_type', $id, $data);
        }

        $data = array(
            'fk_lkp_location'   => $request->post('lokasi'),
            'fk_et_facility'    => $request->post('jenis'),
            'eft_type_desc'     => $request->post('fasiliti'),
            'eft_status'        => $request->post('status'),
            'highlight'         => $request->post('highlight'),
            'latitude'         => $request->post('latitude'),
            'longitude'         => $request->post('longitude'),
            'eft_uuid'          => $request->post('uuidKey'),
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Session::get('user')['id'],
        );
        $query = All::GetUpdate('et_facility_type', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(130), Crypt::encrypt($id), 'Kemas kini Rekod Pengumuman = '. $request->post('fasiliti'),1);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facility'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_type', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(131), Crypt::encrypt($id), 'Kemas kini Rekod Pengumuman = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facility'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facility'));
        }
    }

    public function imageshow($id){
        $id = Crypt::decrypt($id);
        $data['id'] = $id;
        $data['facility'] = All::GetAllRow('et_facility_type_image', 'fk_et_facility_type', $id, 'id', 'ASC')->where('deleted_at', NULL);
        return view('sport.facility.imagelist', compact('data'));
    }

    public function imageform($id){
        $id = Crypt::decrypt($id);
        $data['rent'] = All::Show('lkp_slot_rent', 'id', 'ASC');
        $data['id'] = $id;
        return view('sport.facility.imageform', compact('data'));
    }

    public function imageadd(Request $request, $id){
        $id = Crypt::decrypt($id);

        if ($request->hasFile('file')) {
            $path = public_path('assets/upload/pic/'.$id);
            File::ensureDirectoryExists($path);
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $request->file('file')->move($path, $filename);
        }else{
    // dd('Request Has No File');
        }

        $data = array(
            'fk_et_facility_type'    => $id,
            'img'                    => $filename,
            'title'                  => $request->post('tajuk'),
            'created_at'             => date('Y-m-d H:i:s'),
            'updated_at'             => date('Y-m-d H:i:s'),
            'updated_by'             => Session::get('user')['id'],
        );
        $query = All::InsertGetID('et_facility_type_image', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(132), Crypt::encrypt($query), 'Tambah Gambar Fasiliti',null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facility/image',Crypt::encrypt($id)));
        }
    }

    public function imagedelete($id, $file){
        $id = Crypt::decrypt($id);
        $file = Crypt::decrypt($file);

        $image = All::GetRow('et_facility_type_image', 'id', $file);

        $path = public_path('assets/upload/pic/'.$id.'/'.$image->img);
        if (File::exists($path)) {
            File::delete($path);
        }

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_type_image', $file, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(133), Crypt::encrypt($id), 'Padam Gambar Fasiliti',null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/facility/image',Crypt::encrypt($id)));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/facility/image',Crypt::encrypt($id)));
        }
    }
}

<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class AnnouncementController extends Controller
{
    public function showAnnouncement() {
        $data['pengumuman'] = all::Show('bh_announcement', 'updated_at', 'DESC')->where('ba_type', 2);
        return view('sport.announcement.lists', compact('data'));
    }

    public function form(){
        return view('sport.announcement.form');
    }

    public function add(Request $request){
        $status = request()->status;
        if($status == null){
            $status = 0;
        }

        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('lkp_location', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(24), Crypt::encrypt($query), 'Tambah Rekod Pengumuman = '. request()->lokasi,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/announcement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/announcement'));
        }


    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['location'] = All::GetRow('lkp_location', 'id', $id);
        return view('sport.announcement.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);
        $status = request()->status;

        if($status == null){
            $status = 0;
        }

        $data = array(
            'lc_description'    => request()->lokasi,
            'lc_status'         => $status,
            'lc_type'           => 2,
            'updated_by'        => Session::get('user')['id'],
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Kemas kini Rekod Pengumuman = '. request()->lokasi,null);
            Session::flash('flash', 'Success');
            return Redirect::to(url('/sport/admin/announcement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/announcement'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('lkp_location', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(25), Crypt::encrypt($id), 'Padam Pengumuman = ' . $id,null);
            Session::flash('flash', 'Success');
            return Redirect::to(url('/sport/admin/announcement'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/announcement'));
        }
    }
}

<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class PriceEquipmentController extends Controller
{
    public function show() {
        // $data['list'] = All::Show('et_equipment_price', 'updated_at', 'DESC');
        if(count(array_intersect(Session::get('user.roles'), [1]))){
            $data['list'] = All::Show('et_equipment_price', 'updated_at', 'DESC');
        } else if(count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]))){
            $data['list'] = All::GetAllRowJoin2all('et_equipment', 'et_equipment_price', 'updated_at', 'DESC')->whereIn('fk_lkp_location', Session::get('user.roles'));
        } else {
            return redirect('/');
        }

        return view('sport.equipmentprice.lists', compact('data'));
    }

    public function form(){
        $data['equipment'] = All::Show('et_equipment', 'ee_name', 'ASC');
        $data['function'] = All::Show('et_function', 'ef_desc', 'ASC')->where('ef_type', 2);
        
        $data['gst'] = All::Show('lkp_gst_rate', 'updated_at', 'DESC');

        return view('sport.equipmentprice.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_equipment'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_lkp_gst_rate'   => request()->gst,
            'eep_day_cat'       => request()->hari,
            'eep_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_equipment_price', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(144), Crypt::encrypt($query), 'Tambah Harga Peralatan = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['equipment'] = All::Show('et_equipment', 'updated_at', 'DESC');
        $data['function'] = All::Show('et_function', 'updated_at', 'DESC')->where('ef_type', 2);
        $data['gst'] = All::Show('lkp_gst_rate', 'updated_at', 'DESC');
        $data['list'] = All::GetRow('et_equipment_price', 'id', $id);
        return view('sport.equipmentprice.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_equipment'    => request()->fasiliti,
            'fk_et_function'    => request()->fungsi,
            'fk_lkp_gst_rate'   => request()->gst,
            'eep_day_cat'       => request()->hari,
            'eep_unit_price'    => request()->harga,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment_price', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(145), Crypt::encrypt($id), 'Kemas kini Harga Peralatan = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_equipment_price', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(146), Crypt::encrypt($id), 'Padam Harga Peralatan = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/equipmentprice'));
        }
    }
}

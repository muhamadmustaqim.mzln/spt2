<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class PolicyController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_facility_type_slot_restriction', 'updated_at', 'DESC');
        
        return view('sport.policy.lists', compact('data'));
    }

    public function form(){
        $data['facility'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC');

        return view('sport.policy.form', compact('data'));
    }

    public function add(Request $request){
        $type = request()->jenis;

        if ($type == 1) {
            $data = array(
                'fk_et_facility_type'           => request()->fasiliti,
                'efsob_open_date'               => date("Y-m-d", strtotime(request()->date)),
                'efsob_open_time'               => date('00:00:00'),
                'efsob_status'                  => request()->status,
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s')
            );
            $query = All::InsertGetID('et_facility_sintetik_open_book', $data);
        } else {
            $data = array(
                'fk_et_facility_type'           => request()->fasiliti,
                'max_slot'                      => request()->slot,
                'max_court'                     => request()->max_court,
                'max_per_court'                 => request()->max_per_court,
                'max_per_day'                   => request()->max_per_day,
                'expiry_by_month'               => request()->tempoh,
                'type'                          => 0,
                'updated_by'                    => Session::get('user')['id'],
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s')
            );
            $query = All::InsertGetID('et_facility_type_slot_restriction', $data);
        }
        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(141), Crypt::encrypt($query), 'Tambah Rekod Polisi = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/policy'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/policy'));
        }
    }

    public function edit($id, $type){
        $id = Crypt::decrypt($id);
        $type = Crypt::decrypt($type);
        $data['type'] = $type;
        $data['facility'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC');
        $data['list'] = All::GetRow('et_facility_type_slot_restriction', 'id', $id);
        
        if($type == 1){
            $data['sintetik'] = All::GetAllRow('et_facility_sintetik_open_book', 'fk_et_facility_type', $data['list']->fk_et_facility_type);
        }

        return view('sport.policy.edit', compact('data'));
    }

    public function update(Request $request, $id, $type){
        $id = Crypt::decrypt($id);
        $type = Crypt::decrypt($type);
        if($type != request()->jenis){
            $type == request()->jenis;
        }
        // dd( $request->post());
        if($type == 1){
            $arr = [];

            foreach ($_POST as $key => $value) {
                if (strpos($key, 'date_') === 0) {
                    $dateKey = substr($key, strlen('date_'));
                    
                    $arr[$dateKey] = $value;
                }
                if (strpos($key, 'status_') === 0) {
                    $statusKey = substr($key, strlen('status_'));

                    $arr[$dateKey] = $arr[$dateKey] . ',' . $value;
                }
            }

            foreach ($arr as $key => $value) {
                $str = explode(",", $value);
                $update = [
                    'efsob_open_date'   => date("Y-m-d", strtotime($str[0])),
                    'efsob_status'      => $str[1],
                    'updated_at'        => date('Y-m-d')
                ];
                $query = All::InsertGetID('et_facility_sintetik_open_book', $update);

                $data = array(
                    'type'                          => 1,
                    'updated_by'                    => Session::get('user')['id'],
                    'updated_at'                    => date('Y-m-d H:i:s')
                );
                $query = All::GetUpdate('et_facility_type_slot_restriction', $id, $data);
            }
        } else {
            $data = array(
                // 'fk_et_facility_type'           => request()->fasiliti,
                'max_slot'                      => request()->slot,
                'max_court'                     => request()->max_court,
                'max_per_court'                 => request()->max_per_court,
                'max_per_day'                   => request()->max_per_day,
                'expiry_by_month'               => request()->tempoh,
                'type'                          => request()->jenis,
                'updated_by'                    => Session::get('user')['id'],
                'updated_at'                    => date('Y-m-d H:i:s')
            );
            $query = All::GetUpdate('et_facility_type_slot_restriction', $id, $data);
        }

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(142), Crypt::encrypt($id), 'Kemas kini Rekod Polisi = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/policy'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/policy'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_type_slot_restriction', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(143), Crypt::encrypt($id), 'Padam Rekod Polisi = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/policy'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/policy'));
        }
    }
}

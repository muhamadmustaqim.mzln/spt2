<?php

namespace App\Http\Controllers\sport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use App\Models\All;
use App\Models\AuditLog;

class ClosedController extends Controller
{
    public function show() {
        $data['list'] = All::Show('et_facility_close', 'updated_at', 'ASC');
        return view('sport.closed.lists', compact('data'));
    }

    public function form(){
        $data['facility'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC');

        return view('sport.closed.form', compact('data'));
    }

    public function add(Request $request){

        $data = array(
            'fk_et_facility_type'           => request()->fasiliti,
            'day_num'                       => request()->hari,
            'updated_by'                    => Session::get('user')['id'],
            'created_at'                    => date('Y-m-d H:i:s'),
            'updated_at'                    => date('Y-m-d H:i:s')
        );
        $query = All::InsertGetID('et_facility_close', $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(162), Crypt::encrypt($query), 'Sukan - Tambah Penutupan Fasiliti = ' . request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/closed'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/closed'));
        }
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $data['facility'] = All::Show('et_facility_type', 'eft_type_desc', 'ASC');
        $data['list'] = All::GetRow('et_facility_close', 'id', $id);
        
        return view('sport.closed.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $id = Crypt::decrypt($id);

        $data = array(
            'fk_et_facility_type'           => request()->fasiliti,
            'day_num'                       => request()->hari,
            'updated_by'        => Session::get('user')['id'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_close', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(163), Crypt::encrypt($id), 'Sukan - Kemas kini Penutupan Fasiliti = '. request()->fasiliti,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/closed'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/closed'));
        }
    }

    public function delete($id){
        $id = Crypt::decrypt($id);

        $data = array(
            'updated_by'        => Session::get('user')['id'],
            'deleted_at'        => date('Y-m-d H:i:s')
        );
        $query = All::GetUpdate('et_facility_close', $id, $data);

        if ($query) {
            $audit = AuditLog::log(Crypt::encrypt(164), Crypt::encrypt($id), 'Sukan - Padam Penutupan Fasiliti = '. $id,null);
            Session::flash('flash', 'Success'); 
            return Redirect::to(url('/sport/admin/closed'));
        }else{
            Session::flash('flash', 'Failed'); 
            return Redirect::to(url('/sport/admin/closed'));
        }
    }
}

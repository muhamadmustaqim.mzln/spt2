<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\BapiController;
use App\Models\All;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PaymentStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ecb:query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query Check Payment Incomplete Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("ECB Query");      // DB::select( DB::raw("SET foreign_key_checks = 0") ); //set foriegn key to zero so can delete foreign data
        echo("start checking data 30min > \n");
        DB::beginTransaction();
    
        try{
            $data = DB::select( DB::raw("SELECT mb.`id` AS mbid,etb.`id` AS etbid
                FROM `et_confirm_booking` etb, `main_booking` mb,et_payment_fpx epf,et_facility ef,et_facility_type eft
                WHERE etb.`fk_main_booking`=mb.`id`
                AND epf.`fk_main_booking`=mb.`id`
                AND etb.`fk_et_facility_type`=eft.`id`
                AND ef.`ef_type` = 2
                AND ef.`id` = eft.`fk_et_facility`
                AND mb.`fk_lkp_status` IN (2,10)
                AND epf.`fpx_status` IN (0,2)
                AND TIMESTAMPDIFF(MINUTE,mb.created_at,NOW()) > 20
                ") );

            Log::info($data);

            foreach ($data AS $key => $value) {
                
                $update = DB::table('main_booking')
                    ->where('main_booking.id',$value->mbid)
                    ->update(['fk_lkp_status' => DB::raw(10), 'deleted_at' => date('Y-m-d H:i:s')]);

                $delete1 = DB::table('et_confirm_booking_detail')
                    ->where('et_confirm_booking_detail.fk_et_confirm_booking',$value->etbid)
                    ->delete();
            }

            foreach ($data AS $key2 => $value2) {
                
                $delete1 = DB::table('et_confirm_booking_detail')
                    ->where('et_confirm_booking_detail.fk_et_confirm_booking',$value2->etbid)
                    ->delete();

                $delete2 = DB::table('et_confirm_booking')
                    ->where('et_confirm_booking.fk_main_booking',$value2->mbid)
                    ->delete();

                $audit = DB::table('audit_trail')
                    ->insert(
                        ['fk_users' => 1,'table_ref_id' => $value2->mbid,'task' => 'Tempahan di batal secara automatik oleh Sistem (30min-fpx)','created_at' => Carbon::now() , 'updated_at'=> Carbon::now()]
                    );

            }
        }catch(Exception $e){
            DB::rollback();
            Log::info($e);
            return $e->getMessage();
        }

        $this->info('Finishing delete data from et_confirm_booking_detail and et_confirm_booking on 60 minutes no payment received!');
        DB::commit();
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\BapiController;
use App\Models\All;
use App\Helpers\Helper as HP;

class FpxQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fpx:query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query Pending Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Fpx Cron");
        $orderNo = '';
        try {
            $pending = All::GetAllRow('et_fpx_log', 'fpx_status', 'PENDING_APPROVAL');
            $filtered = $pending->filter(function ($item, $key) {
                return strpos($item->fpx_order_no, 'SPS') === 0;
            });
            Log::info("Fpx Cron Filter". json_encode($filtered));
            foreach ($filtered as $f) {
                $orderNo = $f->fpx_order_no;
                // $orderNo = 'SPS24022779336';
                $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
                $data = array("payload" => array(
                    "subsysId"      => "SPS",
                    "password"      => "e_TemPahan@@2024!",
                    "orderNo"       => $orderNo,
                ));
        
                $postdata = json_encode($data);
        
                $ch = curl_init($url); 
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $result = curl_exec($ch);
                $response = json_decode($result, true);
                $responseArray[] = $response;
                Log::info("Fpx Cron Response: ". json_encode($response));
                if(!isset($response['isError'])) {
                    $payload = $response['payload'];
                    if(is_array($payload)) {
                        $orderNo = $payload['orderNo'];
                        $status = $payload['status'];
                        $txnReference = $payload['sellerTxnTime'];
                        $txnId = $payload['txnId'];
                        $txnTime = $payload['txnTime'];
                        $amount = $payload['amount'];
                        $bankname = $payload['bankName'];
                        $fpxvalue = $payload['ppjTransNo'];
                        $approvalCode = $fpxvalue . 'AC' . $txnId;
                    }
                    if($payload['status'] == 'SUCCESS') {
                        if (strlen($orderNo) == 18) {
                            $newOrderNo = substr($orderNo, 0, 14);
                        } 
                        Log::info("Fpx Cron Order No". json_encode($orderNo));
                        $sapDocNo = "";
                        $main = All::GetRow('main_booking', 'bmb_booking_no', $newOrderNo);
                        $id = All::GetRow('et_payment_fpx', 'fk_main_booking', $main->id);
                        $et_booking_facility = All::GetRow('et_booking_facility', 'fk_main_booking', $main->id);
                        $fk_lkp_location = $main->fk_lkp_location;
                        $efd_fee_code = All::GetAllRow('et_facility_detail', 'fk_et_facility_type', $et_booking_facility->fk_et_facility_type)[0]->efd_fee_code;
                        $user = All::GetRow('user_profiles', 'fk_users', $main->fk_users);   
                        $user->fk_lkp_state = HP::negeri($user->fk_lkp_state);
                        $user->fk_lkp_country = HP::negara($user->fk_lkp_country);
                        $data['trans'] = (object)[
                            'txnTime'   => date("Y-m-d H:i:s", strtotime(request()->txnTime)),
                            'amount'    => $amount,
                            'feecode'   => $efd_fee_code,
                            'fpxNum'    => $approvalCode,
                            'approvalCode' => $txnId,
                            'bmb_booking_no'    => $newOrderNo
                        ];

                        $data_main = array(
                            'fk_lkp_status'             => 5,
                            'updated_at'                => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdate('main_booking', $main->id, $data_main);

                        try {
                            if (substr($orderNo, 0, 2) != 'SPS') {
                                $dataBP = BapiController::check_bp($user->bud_reference_id);
                                $checkbpstatus = $dataBP;
                                if($dataBP[0] == null || $dataBP[1] == null) {
                                    $dataBP = BapiController::create_bp($user);
                                } 
                                $sap_doc_no = BapiController::generate_bill($dataBP, $user);
                            } 
                            $adhocResponse = BapiController::adhoc($sap_doc_no, $user, $data['trans']);
                        } catch (\Throwable $th) {
                            Log::info(["Fpx Cron Response: adhocResponse" => json_encode($adhocResponse)]);
                            Log::info(["Fpx Cron Response: sap_doc_no" => json_encode($sap_doc_no)]);
                        }

                        $dataArr = [
                            'fpx_status'    => 'SUCCESS',
                            'fpx_txn_id'    => $fpxvalue,
                            'sap_doc_no'    => $adhocResponse[0],
                            'updated_at'    => date('Y-m-d'),
                        ];
                        All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);

                        $data_fpx = array(
                            'bank'              => $bankname,
                            'sap_doc_no'        => $adhocResponse[0],
                            'fpx_serial_no'     => $fpxvalue,
                            'fpx_status'        => 1,
                            'fpx_date'          => date('Y-m-d 00:00:00'),    
                            'fpx_trans_date'    => $txnTime,    
                            'amount_paid'       => $amount,
                            'created_at'        => date('Y-m-d H:i:s'),
                        );
                        All::GetUpdateSpec('et_payment_fpx', 'fpx_trans_id', $orderNo, $data_fpx);
                        
                        $data_fpx = array(
                            'fk_main_booking'       => $main->id,
                            'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                            'ecb_date_booking'      => $et_booking_facility->ebf_start_date,
                            'ecb_flag_indicator'    => 1,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        $query = All::InsertGetID('et_confirm_booking', $data_fpx);
                        
                        $data_fpx = array(
                            'et_confirm_booking'    => $query,
                            // 'fk_et_facility_detail' => $,
                            // 'fk_et_slot_time'       => 1,
                            'ecbd_date_booking'     => date('Y-m-d'),
                            'fk_et_facility_type'   => $et_booking_facility->fk_et_facility_type,
                            'created_at'            => date('Y-m-d H:i:s'),
                            'updated_at'            => date('Y-m-d H:i:s'),
                        );
                        All::Insert('et_confirm_booking', $data_fpx);
                    } else if($payload['status'] == 'FAILED') {
                        $dataArr = [
                            'fpx_status'    => 'FAILED',
                            'updated_at'    => date('Y-m-d'),
                        ];
                        All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                    }
                }
            }
        } catch (\Throwable $th) {
            Log::info($th, json_encode($orderNo));
        }
    }
}

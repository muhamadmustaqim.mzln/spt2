<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\BapiController;
use App\Models\All;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OpenSlot_FiveMinute_Daily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ept:dailyRemoval';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query Check Payment Timeout Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Payment Timeout Query");      // DB::select( DB::raw("SET foreign_key_checks = 0") ); //set foriegn key to zero so can delete foreign data
        echo("start checking data daily");
        DB::beginTransaction();
    
        try{
            // $query = "
            //     DELETE FROM et_payment_timeout
            //     WHERE TIMESTAMPDIFF(HOUR, book_time_end, :sixHoursAgo) > 0
            // ";
            
            // $deletedRows = DB::delete($query, ['sixHoursAgo' => $sixHoursAgo]);

            $currentDateTime = now(); // Get the current date and time
            $sixHoursAgo = $currentDateTime->subHours(6); 

            $data = DB::select(DB::raw("SELECT *
                FROM et_payment_timeout AS ept
                WHERE TIMESTAMPDIFF(HOUR, ept.book_time_end, :sixHoursAgo) > 0
            "), ['sixHoursAgo' => $sixHoursAgo]);

            foreach ($data as $key => $value) {
                DB::table('et_payment_timeout')->delete($key);
            }

            Log::info($data);

        }catch(Exception $e){
            DB::rollback();
            Log::info($e);
            return $e->getMessage();
        }

        $this->info('Finishing delete data from et_payment_timeout more than 6 hours duration');
        DB::commit();
    }
}

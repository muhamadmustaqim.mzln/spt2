<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\All;

class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Cron is working fine!");
        $pending = All::GetAllRow('et_fpx_log', 'fpx_status', 'PENDING_APPROVAL');
        $filtered = $pending->filter(function ($item, $key) {
            return strpos($item->fpx_order_no, 'SPS') === 0;
        });
        foreach ($filtered as $f) {
            $orderNo = $f->fpx_order_no;
            // $orderNo = 'SPS24022779336';
            $url = 'https://fpxuat.ppj.gov.my/pay-api/pay/inquiry';
            $data = array("payload" => array(
                "subsysId"      => "SPS",
                "password"      => "e_TemPahan@@2024!",
                "orderNo"       => $orderNo,
            ));
    
            $postdata = json_encode($data);
    
            $ch = curl_init($url); 
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $result = curl_exec($ch);
            $response = json_decode($result, true);
            $responseArray[] = $response;
            if(!isset($response['isError'])) {
                $payload = $response['payload'];
                if($payload['status'] == 'SUCCESS') {
                    $dataArr = [
                        'fpx_status'    => 'SUCCESS',
                        'updated_at'    => date('Y-m-d'),
                    ];
                    All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                } else if($payload['status'] == 'FAILED') {
                    $dataArr = [
                        'fpx_status'    => 'FAILED',
                        'updated_at'    => date('Y-m-d'),
                    ];
                    All::GetUpdateSpec('et_fpx_log', 'fpx_order_no', $orderNo, $dataArr);
                }
            }
        }
    }
}

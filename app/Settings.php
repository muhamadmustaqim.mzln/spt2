// app/Settings.php

namespace App;

class Settings
{
    public static $gstEnabled = true;

    public static function setGstEnabled($value)
    {
        self::$gstEnabled = $value;
        // Optionally, you can save the value to a file here
    }
}
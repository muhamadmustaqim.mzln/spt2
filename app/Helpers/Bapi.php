<?php 

use App\Models\All;
use App\Models\Sport;

if (! function_exists('funcCreateBP')) {
    function funcCreateBP($input)
    {
        $url = "http://QAS.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_create_bp_spt4/800/zws_create_bp_spt4/zws_create_bp_spt4"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password

        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:ZBAPI_BP_CREATE_SPT>
                    <IADDR_LN1>'. $input["address1"] .'</IADDR_LN1>
                    <!--Optional:-->
                    <IADDR_LN2>'. $input["address2"] .'</IADDR_LN2>
                    <!--Optional:-->
                    <IADDR_LN3>'. $input["address3"] .'</IADDR_LN3>
                    <!--Optional:-->
                    <IADDR_LN4>'. $input["address4"] .'</IADDR_LN4>
                    <!--Optional:-->
                    <IAPPLN_TYPE></IAPPLN_TYPE>
                    <ICOUNTRY>MY</ICOUNTRY>
                    <!--Optional:-->
                    <ICUST_ID>'. $input["id_number"].'</ICUST_ID>
                    <ICUST_NAME>'. $input["name"] .'</ICUST_NAME>
                    <ICUST_TP>B</ICUST_TP>
                    <IEMAIL>'. $input["email"] .'</IEMAIL>
                    <!--Optional:-->
                    <IPHONE>'. $input["phone_number"] .'</IPHONE>
                    <IPOSTCODE>'. $input["postcode"] .'</IPOSTCODE>
                    <ISTATE>'. $input["state"] .'</ISTATE>
                </urn:ZBAPI_BP_CREATE_SPT>
                </soapenv:Body>
            </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return $response;
        }else{
            return false;
        }
    }
}

if (! function_exists('funcCheckBP')) {
    function funcCheckBP($input)
    {
        $url = "http://QAS.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_check_bp_spt/800/zws_check_bp_spt/zws_check_bp_spt"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password

        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:ZBAPI_CHECK_BP_SPT>
                    <!--Optional:-->
                    <ICNO>'. $input["id_number"].'</ICNO>
                </urn:ZBAPI_CHECK_BP_SPT>
                </soapenv:Body>
            </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return $response;
        }else{
            return false;
        }
    }
}

if (! function_exists('funcOnlinePay')) {
    function funcOnlinePay($input)
    {
        $url = "http://QAS.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_pembayaran_online2_001/800/zws_pembayaran_online_spt/zws_pembayaran_online_spt"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password

        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:ZBAPI_PEMBAYARAN_ONLINE2>
                    <AMOUNT>'. $input["amount"].'</AMOUNT>
                    <APPROVALCODE>'. $input["app_code"].'</APPROVALCODE>
                    <!--Optional:-->
                    <DCOLLECTION_REFERENCE></DCOLLECTION_REFERENCE>
                    <!--Optional:-->
                    <DCUSTOMER_ADDRESS1>'. $input["address1"].'</DCUSTOMER_ADDRESS1>
                    <!--Optional:-->
                    <DCUSTOMER_ADDRESS2>'. $input["address2"].'</DCUSTOMER_ADDRESS2>
                    <!--Optional:-->
                    <DCUSTOMER_POSTCODE>'. $input["postcode"].'</DCUSTOMER_POSTCODE>
                    <!--Optional:-->
                    <DCUSTOMER_STATE>'. $input["state"].'</DCUSTOMER_STATE>
                    <!--Optional:-->
                    <DCUST_NAME>'. $input["name"].'</DCUST_NAME>
                    <!--Optional:-->
                    <DDOC_REFERENCE>'. $input["reference"].'</DDOC_REFERENCE>
                    <!--Optional:-->
                    <DFEECODE>'. $input["feecode"].'</DFEECODE>
                    <DOCDATE>'. $input["date"].'</DOCDATE>
                    <DOCUMENTNO>'. $input["documentno"].'</DOCUMENTNO>
                    <!--Optional:-->
                    <DSTATUOTARY_ID>'. $input["id_number"].'</DSTATUOTARY_ID>
                    <!--Optional:-->
                    <PCARDNO>'. $input["card"].'</PCARDNO>
                    <!--Optional:-->
                    <PPOSTDATE>'. $input["date"].'</PPOSTDATE>
                    <!--Optional:-->
                    <PTRANSDATE>'. $input["date"].'</PTRANSDATE>
                    <!--Optional:-->
                    <RETURN>
                        <!--Zero or more repetitions:-->
                        <item>
                            <TYPE></TYPE>
                            <ID></ID>
                            <NUMBER></NUMBER>
                            <MESSAGE></MESSAGE>
                            <LOG_NO></LOG_NO>
                            <LOG_MSG_NO></LOG_MSG_NO>
                            <MESSAGE_V1></MESSAGE_V1>
                            <MESSAGE_V2></MESSAGE_V2>
                            <MESSAGE_V3></MESSAGE_V3>
                            <MESSAGE_V4></MESSAGE_V4>
                            <PARAMETER></PARAMETER>
                            <ROW></ROW>
                            <FIELD></FIELD>
                            <SYSTEM></SYSTEM>
                        </item>
                    </RETURN>
                    <SUB_SYSCODE>'. $input["syscode"].'</SUB_SYSCODE>
                </urn:ZBAPI_PEMBAYARAN_ONLINE2>
                </soapenv:Body>
            </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return $response;
        }else{
            return false;
        }
    }
}

if (! function_exists('funcGenerateBill')) {
    function funcGenerateBill($input)
    {
        $url = "http://QAS.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_generate_bill_spt/800/zws_generate_bill_spt/zws_generate_bill_spt"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password

        $xml_post_string = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:ZBAPI_GENERATE_BILL_SPT>
                    <!--Optional:-->
                    <APPL_AREA>P</APPL_AREA>
                    <!--Optional:-->
                    <BILL_DUE_DATE></BILL_DUE_DATE>
                    <BUSINESSPARTNER1>'. $input["bpno"].'</BUSINESSPARTNER1>
                    <CONTRACTACCOUNT1>'. $input["contractacc"].'</CONTRACTACCOUNT1>
                    <!--Optional:-->
                    <CREATED_BY></CREATED_BY>
                    <!--Optional:-->
                    <CURRENCY>RM</CURRENCY>
                    <DOC_DATE>'. $input["date"].'</DOC_DATE>
                    <!--Optional:-->
                    <DOC_TYPE>ZB</DOC_TYPE>
                    <!--Optional:-->
                    <ENTRY_DATE></ENTRY_DATE>
                    <!--Optional:-->
                    <ENTRY_TIME>00:00:00</ENTRY_TIME>
                    <EVENT_DESC>'. $input["event"].'</EVENT_DESC>
                    <FEE_TYPE>'. $input["feetype"].'</FEE_TYPE>
                    <ITEM>
                        <!--Zero or more repetitions:-->
                        <item>
                            <FEE_TYPE>'. $input["feetype"].'</FEE_TYPE>
                            <FEE_AMOUNT>'. $input["amount"].'</FEE_AMOUNT>
                        </item>
                    </ITEM>
                    <!--Optional:-->
                    <MAIN_TRANS>'. $input["main"].'</MAIN_TRANS>
                    <POST_DATE>'. $input["date"].'</POST_DATE>
                    <!--Optional:-->
                    <REF_DOC_NO></REF_DOC_NO>
                    <!--Optional:-->
                    <RETURN>
                        <!--Zero or more repetitions:-->
                        <item>
                            <TYPE></TYPE>
                            <ID></ID>
                            <NUMBER></NUMBER>
                            <MESSAGE></MESSAGE>
                            <LOG_NO></LOG_NO>
                            <LOG_MSG_NO></LOG_MSG_NO>
                            <MESSAGE_V1></MESSAGE_V1>
                            <MESSAGE_V2></MESSAGE_V2>
                            <MESSAGE_V3></MESSAGE_V3>
                            <MESSAGE_V4></MESSAGE_V4>
                            <PARAMETER></PARAMETER>
                            <ROW></ROW>
                            <FIELD></FIELD>
                            <SYSTEM></SYSTEM>
                        </item>
                    </RETURN>
                    <SUBSYSTEM_ID>'. $input["syscode"].'</SUBSYSTEM_ID>
                    <!--Optional:-->
                    <SUB_TRANS>'. $input["sub"].'</SUB_TRANS>
                    <!--Optional:-->
                    <TRANS_DATE>'. $input["date"].'</TRANS_DATE>
                </urn:ZBAPI_GENERATE_BILL_SPT>
                </soapenv:Body>
            </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return $response;
        }else{
            return false;
        }
    }
}

if (! function_exists('funcCheckDebt')) {
    function funcCheckDebt($input)
    {
        $url = "http://ECCDEV.ppj.gov.my:8000/sap/bc/srt/rfc/sap/zws_tunggak/700/zws_tunggak/zws_tunggak"; // asmx URL of WSDL
        $soapUser = "rfc1";  //  username
        $soapPassword = "init1234"; // password

        $xml_post_string = '
            <?xml version="1.0" encoding="utf-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:soap:functions:mc-style">
                <soapenv:Header/>
                <soapenv:Body>
                <urn:ZfkkPscdtunggak>
                    <!--Optional:-->
                    <Icno>'. $input["id_number"].'</Icno>
                </urn:ZfkkPscdtunggak>
                </soapenv:Body>
            </soapenv:Envelope>
        ';  
    
        $headers =  array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "SOAPAction: = ", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL
    
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
        // converting
        $response = curl_exec($ch); 
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if($status == 200){
            return $response;
        }else{
            return false;
        }
    }
}



@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                  				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                {{-- <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p> --}}
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-title font-weight-bolder">Semakan Permohonan Acara</h4>
                                </div>
                            </div>
                            <div class="tab-content mt-5" id="myTabContent">
                                <form action="" method="post" enctype="multipart/form-data">
                                    @csrf
                                <!-- Maklumat Permohonan Acara::Start -->
                                    <div class="tab-pane fade show active" id="maklumatPermohonanAcara" role="tabpanel" aria-labelledby="maklumatPermohonanAcara-tab">
                                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                            <li class="nav-item position-relative" id="butiranPemohonNavItem">
                                                <a class="nav-link pr-1" id="butiranPemohon-tab" data-toggle="tab" href="#butiranPemohon">
                                                    <span class="nav-text">Butiran Pemohon</span>
                                                    <i hidden id="exclamation-butiranPemohon" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item position-relative" id="butiranAcaraNavItem">
                                                <a class="nav-link active pr-1" id="butiranAcara-tab" data-toggle="tab" href="#butiranAcara" aria-controls="butiranAcara">
                                                    <span class="nav-text">Butiran Acara</span>
                                                    <i hidden id="exclamation-butiranAcara" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                            @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2)
                                                <li class="nav-item position-relative" id="maklumatPenganjurUtamaNavItem">
                                                    <a class="nav-link pr-1" id="maklumatPenganjurUtama-tab" data-toggle="tab" href="#maklumatPenganjurUtama" aria-controls="maklumatPenganjurUtama">
                                                        <span class="nav-text">Maklumat Penganjur Utama</span>
                                                        <i hidden id="exclamation-maklumatPenganjurUtama" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            <li class="nav-item position-relative" id="muatNaikDokumenNavItem">
                                                <a class="nav-link pr-1" id="muatNaikDokumen-tab" data-toggle="tab" href="#muatNaikDokumen" aria-controls="muatNaikDokumen">
                                                    <span class="nav-text">Muat Naik Dokumen</span>
                                                    <i hidden id="exclamation-muatNaikDokumen" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content mt-5" id="myTabContent">
                                            <!-- Butiran Pemohon::Start -->
                                            <div class="tab-pane fade show" id="butiranPemohon" role="tabpanel" aria-labelledby="butiranPemohon-tab">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="mb-10 font-weight-bolder text-dark bg-secondary py-3 px-8">Maklumat Am</div>
                                                        <!-- begin:No Tempahan & Status -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Tempahan</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['main_booking']->bmb_booking_no }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['status_tempahan'] }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end:No Tempahan & Status -->
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Pemohon</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Jenis Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jenis Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="jenisPemohon" class="jenisPemohon form-control form-control-lg font-weight-bold mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected></option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) selected @endif> Individu</option>
                                                                                <option value="2 "@if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 2) selected @endif> Syarikat/Organisasi</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jenis Pemohon-->
                                                                <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Pemohon (Individu / Syarikat / Organisasi) <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->name ? $data['booking_person']->name : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <!--begin::Nama Pegawai untuk dihubungi-->
                                                                <div @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) hidden @endif class="form-group" id="contactPersonForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Pegawai untuk dihubungi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg contactPerson" name="contactPerson" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->contact_person ? $data['booking_person']->contact_person : '' }}"/>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::Status Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="statusPemohon" class="statusPemohon form-control form-control-lg font-weight-bold mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected></option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 1) selected @endif> Penganjur Utama</option>
                                                                                <option value="2" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2) selected @endif> Penganjur Acara</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_status == 1) Penganjur Utama @else Penganjur Acara @endif" /> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Status Pemohon-->
                                                                <!--begin::Jawatan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jawatan <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="jawatan" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->post ? $data['booking_person']->post : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jawatan-->
                                                                <!--begin::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Kad Pengenalan / No Pendaftaran Syarikat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="kadPengenalanPendaftaran" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->ic_no ? $data['booking_person']->ic_no : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <!--begin::Alamat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->address ? $data['booking_person']->address : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Alamat-->
                                                                <!--begin::Poskod & Bandar-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->postcode ? $data['booking_person']->postcode : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->city ? $data['booking_person']->city : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Poskod & Bandar-->
                                                                <!--begin::Negeri & Negara-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                <div class="col-9">
                                                                                    <select disabled name="negeri" class="form-control form-control-lg font-weight-bold mt-3" id="kt_select2_5" >
                                                                                        <option value="" disabled selected></option>
                                                                                        @foreach ($data['negeri'] as $st)
                                                                                            <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->fk_lkp_state == $st->id) selected @endif>{{ $st->ls_description }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->fk_lkp_state ? $data['booking_person']->fk_lkp_state : '' }}" />  --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->fk_lkp_country ? $data['booking_person']->fk_lkp_country : '' }}" /> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Negeri & Negara-->
                                                                <!--begin::No GST-->
                                                                <div hidden class="form-group" id="nogstForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No. GST</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->GST_no ? $data['booking_person']->GST_no : '' }}" /> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Pejabat <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->office_no ? $data['booking_person']->office_no : '' }}" /> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="noBimbit" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->hp_no ? $data['booking_person']->hp_no : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <!--begin::No. Faks && Email-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Email <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="email" placeholder="" value="{{ $data['booking_person']->email }}" />
                                                                                    <div hidden class="text-danger" id="invalid-email">* Sila isi Email.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Faks</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="noFaks" placeholder="" value="{{ $data['booking_person']->fax_no ?? '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Butiran Pemohon::End -->
                                            <!-- Butiran Acara::Start -->
                                            <div class="tab-pane fade show active" id="butiranAcara" role="tabpanel" aria-labelledby="butiranAcara-tab">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Acara</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Nama Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event'] != null && $data['booking_event']->event_name ? $data['booking_event']->event_name : '' }}" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                    <!--end::Nama Acara-->
                                                                <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                {{-- <div class="input-icon">
                                                                                    <input type="text" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikhMula" placeholder="Tarikh Mula" autocomplete="off" required value="" />
                                                                                    <span>
                                                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                                                    </span>
                                                                                </div> --}}
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" id="kt_datepicker1" name="event_date" placeholder="" autocomplete="off" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_date)->format('d-m-Y') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" id="kt_datepicker2" name="event_date_end" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_date_end)->format('d-m-Y') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <!--begin::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" id="kt_timepicker_1" class="form-control form-control-lg" name="event_time" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" id="kt_timepicker_1" class="form-control form-control-lg" name="event_time_to" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time_to)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <!--begin::Lokasi Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Lokasi Acara </label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::locationspa($data['booking_event']->fk_spa_location)  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Lokasi Acara-->
                                                                <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jumlah Peserta / Pengunjung</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->visitor_amount  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jumlah Peserta/ Pengunjung-->
                                                                <!--begin::Peringkat Penganjuran-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Peringkat Penganjuran</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->event_name  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Peringkat Penganjuran-->
                                                                <!--begin::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    {{-- @if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_date)->format('d-m-Y') }} @endif --}}
                                                                                    {{-- <input hidden required type="text" class="form-control form-control-lg enter_date" name="enter_date" id="kt_daterangepicker_3Hidden" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_date)->format('d-m-Y') }} @endif" /> --}}
                                                                                    <input required type="text" class="form-control form-control-lg enter_date" name="enter_date" id="kt_datepicker3" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_date)->format('d-m-Y') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" id="kt_timepicker_1" class="form-control form-control-lg" name="enter_time" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <!--begin::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    {{-- @if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_date)->format('d-m-Y') }} @endif --}}
                                                                                    {{-- <input hidden type="text" class="form-control form-control-lg exit_date" name="exit_date" id="kt_daterangepicker_4Hidden" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_date)->format('d-m-Y') }} @endif" /> --}}
                                                                                    <input required type="text" class="form-control form-control-lg exit_date" name="exit_date" id="kt_datepicker4" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_date)->format('d-m-Y') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" id="kt_timepicker_1" class="form-control form-control-lg" name="exit_time" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <!--begin::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Melibatkan penggunaan Drone atau Alat Kawalan Jauh (terbang)</label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="libatPenggunaanDrone" class="form-control form-control-lg font-weight-bold mt-3" id="kt_select2_5">
                                                                                <option value=""> </option>
                                                                                <option value="ya" @if ($data['booking_event'] != null && $data['booking_event']->drone == 1) selected @endif> Ya</option>
                                                                                <option value="tidak" @if ($data['booking_event'] != null && $data['booking_event']->drone == 0) selected @endif> Tidak</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->drone  }}" /> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <!--begin::Nama Tetamu kehormat Utama-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Tetamu kehormat Utama (jika ada)</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->vip_name  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Tetamu kehormat Utama-->
                                                                <!--begin::Maklumat Tambahan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Maklumat Tambahan </label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->remark  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Maklumat Tambahan-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Butiran Acara::End -->
                                            <!-- Butiran Penganjur Utama::Start -->
                                            @if ($data['booking_event'] != null && $data['booking_person']->applicant_type != 1)
                                                <div class="tab-pane fade show" id="maklumatPenganjurUtama" role="tabpanel" aria-labelledby="maklumatPenganjurUtama-tab">
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Penganjur Utama</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Agensi <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->event_name  }} @endif" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Alamat</label></div>
                                                                            <div class="col-9">
                                                                                <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamatPenganjur" placeholder="" value="{{ $data['formPenganjurUtama']['alamatPenganjur'] ?? '' }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Poskod</label></div>
                                                                                    <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="poskod" placeholder="" value="{{ $data['formPenganjurUtama']['poskod'] ?? '' }}" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Bandar</label></div>
                                                                                    <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="bandar" placeholder="" value="{{ $data['formPenganjurUtama']['bandar'] ?? '' }}" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                    <div class="col-9">
                                                                                        <select disabled name="negeri" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                            <option value="" disabled selected></option>
                                                                                            <option value="Johor">Johor</option>
                                                                                            <option value="Kedah">Kedah</option>
                                                                                            <option value="Kelantan">Kelantan</option>
                                                                                            <option value="Melaka">Melaka</option>
                                                                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                                                            <option value="Pahang">Pahang</option>
                                                                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                                                                            <option value="Perak">Perak</option>
                                                                                            <option value="Perlis">Perlis</option>
                                                                                            <option value="Sabah">Sabah</option>
                                                                                            <option value="Sarawak">Sarawak</option>
                                                                                            <option value="Selangor">Selangor</option>
                                                                                            <option value="Terengganu">Terengganu</option>
                                                                                            <option value="Wilayah Persekutuan">Wilayah Persekutuan</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negara</label></div>
                                                                                    <div class="col-9">
                                                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="negara" placeholder="" value="" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>No. Telefon</label></div>
                                                                                    <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="telBimbit" placeholder="" value="{{ $data['formPenganjurUtama']['telBimbit'] ?? '' }}" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <!-- Butiran Penganjur Utama::End -->
                                            <!-- Muat Naik Dokumen::Start -->
                                            <div class="tab-pane fade show" id="muatNaikDokumen" role="tabpanel" aria-labelledby="muatNaikDokumen-tab">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Muat Naik Dokumen</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Bil.</th>
                                                                                <th>Senarai Semak</th>
                                                                                <th style="width: 20%">Muat Naik</th>
                                                                                <th>Tindakan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">1</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">2</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">3</th>
                                                                                <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">4</th>
                                                                                <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal"></th>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Muat Naik Dokumen::End -->     
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-light-primary font-weight-bold mr-2 mt-3">Simpan</button>
                                    </div>
                                </form>
                                <!-- Maklumat Permohonan Acara::End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    @include('event.reschedule.js.reservation')
@endsection

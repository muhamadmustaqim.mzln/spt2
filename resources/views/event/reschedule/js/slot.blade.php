<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    // var table = $('table').DataTable({
    //     "bPaginate": false,
    //     "bLengthChange": false,
    //     "bFilter": true,
    //     "bInfo": false,
    //     language: {
    //         "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
    //         "zeroRecords": "Harap maaf, tiada rekod ditemui",
    //         "info": "Halaman _PAGE_ dari _PAGES_",
    //         "infoEmpty": "Tiada rekod dalam sistem",
    //         "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
    //         "sSearch": "Carian:"
    //     },
    //     rowGroup: {
    //         dataSrc: 1,
    //     },
    // } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function(){
        $('#JenisAcaraTempDewan').on('change', function() {
            var selectedText = $(this).find('option:selected').text();
            var selectedValue = $(this).val();
            if (selectedText.trim() === 'Majlis Perkahwinan') {
                $('#pakejPerkahwinan').prop('hidden', false);
                $('#pakejPerkahwinanSelect').attr('required', true);
            } else {
                $('#pakejPerkahwinan').prop('hidden', true);
                $('#pakejPerkahwinanSelect').prop('required', false).removeAttr('required');
            }
        })
        $("#form").submit(function( event ) {
            var $form = $(this);
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }
        });
    });
    
    var maxAllowed = 2;
    var current = 0;
    
$('.chk').on('change', function () {
    var checkedCount = $('.chk:checked').length;

    if (checkedCount > maxAllowed) {
        $(this).prop('checked', false);
        Swal.fire(
            'Harap Maaf!',
            'Anda telah memilih terlalu banyak slot tempahan. Sila pilih maksimum ' + maxAllowed + ' slot.',
            'error'
        );
    }
});
</script>
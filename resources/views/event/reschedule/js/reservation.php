<!-- <script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script> -->
<!--end::Page Vendors-->
<script>
    $(document).ready(function() {
        var reservedDates = {!! json_encode($data['booked']->pluck('event_date')->toArray()) !!};
        
        function isDateReserved(date) {
        var dateString = moment(date).format('D MMM YYYY');
        return reservedDates.includes(dateString);
        }

        $('#kt_daterangepicker_1').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment().add(1, 'month'),
            isInvalidDate: isDateReserved,
            locale: {
                cancelLabel: 'Clear',
                format: 'D-M-YYYY'
            }
        });

        $('#kt_daterangepicker_1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D-M-YYYY'));
            $('#kt_daterangepicker_2').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'D-M-YYYY'
                }
            });
            var minDateForPicker3 = moment(picker.startDate).subtract(2, 'days');
            var maxDateForPicker4 = moment(picker.startDate).add(2, 'days');
            $('#kt_daterangepicker_2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('D-M-YYYY'));
                $('#kt_daterangepicker_4').daterangepicker({
                    singleDatePicker: true,
                    autoApply: true,
                    minDate: picker.startDate,
                    locale: {
                        cancelLabel: 'Clear',
                        format: 'D-M-YYYY'
                    }
                });
                $('#kt_daterangepicker_4').daterangepicker({
                    singleDatePicker: true,
                    autoApply: true,
                    minDate: picker.startDate,
                    maxDate: maxDateForPicker4,
                    isInvalidDate: isDateReserved,
                    locale: {
                        cancelLabel: 'Clear',
                        format: 'D-M-YYYY'
                    }
                });
                $('#kt_daterangepicker_4').prop('disabled', false);
            });
            $('#kt_daterangepicker_3').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: minDateForPicker3,
                maxDate: picker.startDate,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'D MMM YYYY'
                }
            });
            $('#kt_daterangepicker_4').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate,
                maxDate: maxDateForPicker4,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'D MMM YYYY'
                }
            });
            $('#kt_daterangepicker_3').prop('disabled', false);
            $('#kt_daterangepicker_4').prop('disabled', false);
        });

        $('#kt_daterangepicker_1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        
        $('#kt_daterangepicker_2').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            // minDate: datePicker1,
            minDate: moment().add(1, 'month'),
            locale: {
                cancelLabel: 'Clear',
                format: 'D MMM YYYY'
            }
        });

        $('#kt_daterangepicker_2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D MMM YYYY'));
            $('#kt_daterangepicker_4').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'D MMM YYYY'
                }
            });
            $('#kt_daterangepicker_4').prop('disabled', false);
        });

        $('#kt_daterangepicker_2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        var datepicker1 = moment($('#kt_daterangepicker_1').val()).subtract(2, 'days');
        var datepicker2 = moment($('#kt_daterangepicker_2').val());
        var datepicker2add = moment($('#kt_daterangepicker_2').val()).add(1, 'days');
        // var datepicker2 = $('#kt_daterangepicker_2').val(picker.startDate.format('D MMM YYYY'))
        $('#kt_daterangepicker_3').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            // minDate: moment().add(1, 'month'),
            minDate: datepicker1,
            locale: {
                cancelLabel: 'Clear',
                format: 'D MMM YYYY'
            }
        });

        $('#kt_daterangepicker_3').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D MMM YYYY'));
        });

        $('#kt_daterangepicker_3').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        
        $('#kt_daterangepicker_4').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: datepicker2,
            maxDate: datepicker2add,
            locale: {
                cancelLabel: 'Clear',
                format: 'D MMM YYYY'
            }
        });

        $('#kt_daterangepicker_4').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D MMM YYYY'));
        });

        $('#kt_daterangepicker_4').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        checkStatusAndToggle();
        $('#statusPemohon').on('change', function () {
            checkStatusAndToggle();
        });
        function checkStatusAndToggle() {
            var selectedStatus = $('#statusPemohon').val();
            if (selectedStatus == 'penganjurUtama') {
                $('#maklumatPenganjurUtamaNavItem').hide();
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
            }
        }
    });
</script>
<script>
   $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker1').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker2').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker3').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker3').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_datepicker3').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker4').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker4').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_datepicker4').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
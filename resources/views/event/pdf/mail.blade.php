<!DOCTYPE html>
<html>
{{-- tindakanSemakan --}}
    {{-- diterima --}}
    @if ($type == 22)
        <p style="font-family: Arial"><b><u>STATUS: PERMOHONAN BAHARU DITERIMA</u></b></p>
        <p style="font-family: Arial">Salam Sejahtera,</p>
        <p style="font-family: Arial">
            Untuk makluman, Permohonan Acara <b>{{$data['bmb_booking_no']}}</b> telah diterima dan Pemohon diminta untuk hadir <br>
            ke Mesyuarat Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj) bagi membentangkan cadangan acara yang akan diadakan seperti butiran berikut:<br>
        </p>
        <table class="gridtable" style="cellspacing:0px;cellpadding;0px">
            <tbody>
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Tarikh</b></td>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{($data['date'])}}</td>	
                </tr>
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Masa</b></td>	
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['time']}}</td>	
                </tr>
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Tempat</b></td>	
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['location']}}</td>
                </tr>
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Pengerusi</b></td> 
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['meeting_chairman']}}</td>
                </tr>
                <tr> 
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Catatan</b></td>	
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['remark']}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p style="font-family: Arial">Pemohon perlu membuat pembentangan acara sepertimana yang dipohon dengan mengikuti garis panduan yang telah ditetapkan.</p>
        <br>
        <br>
        <br>
        
        <table  width="100%" border="0" cellpadding="0" id="title3">
            <tr>
                <td align="center" style="font-family: Arial"><u><b>GARIS PANDUAN PEMBENTANGAN KEPADA</b></u></td>
            </tr> 
            <tr>
                <td align="center" style="font-family: Arial"><u><b>PANEL MESYUARAT JAWATANKUASA KELULUSAN ACARA PERBADANAN PUTRAJAYA (JKA-PPj)</b></u></td>
            </tr>
        </table>
        <br>
        <table width="100%" border="0" cellpadding="0" id="title"> 
            <tr>
                <td style="font-family: Arial">
                    <table width="100%" border="0" cellpadding="1" id="title"> 
                        <tr>
                            <td>1.</td>
                            <td>Berpakaian kemas.</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td> Penganjur acara yang menggunakan khidmat pengurus acara perlu hadir bersama untuk pembentangan dan tidak melebihi dari 2 orang pembentang.</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Bahan pembentangan disarankan dalam Bahasa Melayu. Walau bagaimanapun penggunaan Bahasa Inggerisdibenarkan.</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Slaid pembentangan dalam format &#34;powerpoint presentation&#34; dan tidak melebihi dari 30 slaid untuk setiap pembentangan termasuk &#34;hyperlink&#34;serta penggunaan tambahan video/audio tidak melebihi 2 minit.</td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>Berada di lokasi 20 minit sebelum masa pembentangan.</td>
                        </tr>
                        <tr>
                            <td>6.</td>
                            <td>Mengikut giliran yang telah ditetapkan.</td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>Semua kos yang terlibat untuk pembentangan tidak akan ditanggung oleh PPj.</td>
                        </tr>
                        <tr>
                            <td>8.</td>
                            <td>Dokumen yang tidak lengkap tidak akan dikemukakan ke jawatankuasa.</td>
                        </tr>
                        <tr>
                            <td>9.</td>
                            <td>Tidak menyentuh sensitiviti agama, kaum, politik atau isu-isu sensitif.</td>
                        </tr>
                        <tr>
                            <td>10.</td>
                            <td>PPj berhak menunda/meminda/membatalkan tarikh pembentangan dengan mengemukakan notis kepada pemohon.</td>
                        </tr>
                        <tr>
                            <td>11.</td>
                            <td>Maklumat wajib untuk dibentangkan:</td>
                        </tr>
                            </td>
                        </tr>
                    </table>
                    <br>
                </td>
            </tr>
        </table>

        <table width="100%" border="1" cellpadding="1"  id="title">
            <tr>
                <td align="center" bgcolor="#d8d5d4" style="font-family: Arial"><b>Maklumat Pemohon</b></td>
                <td align="center" bgcolor="#d8d5d4" style="font-family: Arial"><b> Maklumat Acara</b></td>
            </tr>
            <tr>
                <td style="vertical-align:top" style="font-family: Arial">
                    <ul>
                        <li type="square">Nama Pemohon/Syarikat/Agensi</li>
                        <li type="square">Nama Penganjur</li>
                        <li type="square">Alamat</li>
                        <li type="square">No.Telefon (P)</li>
                        <li type="square">No. Telefon (H/P)</li>
                        <li type="square">E-mel</li>
                    </ul>
                </td>
                <td style="vertical-align:top" style="font-family: Arial">
                    <ul>
                        <li type="square">Nama Acara</li>
                        <li type="square">Tarikh/Hari Acara</li>
                        <li type="square">Masa Acara</li>
                        <li type="square">Lokasi</li>
                        <li type="square">Tarikh/Masa Masuk Tapak</li>
                        <li type="square">Tarikh/Masa Keluar Tapak</li>
                        <li type="square">Jumlah Jam Penggunaan</li>
                        <li type="square">Tarikh/Hari Acara</li>
                        <li type="square">Jumlah Peserta</li>
                        <li type="square">Jumlah Pelawat/Penonton/Pengunjung</li>
                        <li type="square">Peringkat Penganjuran (Antarabangsa / Kebangsaan /Putrajaya)</li>
                        <li type="square">Pelan Tapak</li>
                        <li type="square">Laluan & Jarak (Acara Jalan Kaki/Larian/Berbasikal dll)</li>
                    </ul>
                </td>
            </tr>
        </table>
        
        <br/>
        Perbadanan Putrajaya telah mengambil langkah-langkah pencegahan bagi mengawal penularan Wabak Covid-19 di Putrajaya. Sehubungan dengan itu, pihak tuan/puan yang hadir bagi sesi pembentangan acara adalah diminta untuk mematuhi perkara-perkara berikut: 
        <ol>
            <li>Hanya 2 orang (maksima) pembentang yang dibenarkan untuk hadir bagi membentangan cadangan/permohonan acara.</li>
            <li>Bagi mengelak risiko jangkitan kepada golongan berisiko, kanak-kanak di bawah umur 12 tahun dan warga emas berumur 60 ke atas dinasihatkan untuk tidak hadir bagi sesi pembentangan.</li>
            <li>Saringan suhu badan akan dibuat di kaunter pendaftaran lobi Kompleks A & B Perbadanan Putrajaya. Individu yang bergejala atau mencatatkan suhu melebihi 37.5 darjah selsius adalah tidak dibenarkan memasuki Kompleks PPj.</li>
            <li>Pembentang yang hadir adalah diminta untuk merekodkan maklumat wajib di luar bilik mesyuarat (Nama Penuh & Nombor Telefon).</li>
            <li>Memakai pelitup muka.</li>
            <li>Memakai cecair pembasmi kuman sebelum masuk ke dalam bilik mesyuarat (akan disediakan).</li>
            <li>Bahan pembentangan tidak melebihi 5-7 minit (tidak termasuk sesi soal jawab).</li>
            <li>Cadangan penganjuran acara perlu mematuhi arahan-arahan terkini dan SOP yang telah ditetapkan oleh pihak Kerajaan.</li>
        </ol>
        <br/>
        <br>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        <p style="font-family: Arial">Sekian.</p>
    
    {{-- gagal --}}
    @elseif ($type == 29)
        <p style="font-family: Arial"><b><u>STATUS:GAGAL/DITOLAK</u></b></p>
        
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <p style="font-family: Arial">Adalah dimaklumkan bahawa Permohonan Acara bagi <b>{{$data['bmb_booking_no']}}</b> adalah tidak diluluskan atas sebab-sebab berikut:<br></p>
        
        <p style="font-family: Arial">{{$data['remark']}}</p>
        
        <br>
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p><br>
        
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        
        <br>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        
        <p style="font-family: Arial">Sekian.</p>
    
    {{-- tidakLengkap --}}
    @elseif ($type == 28)
        <p style="font-family: Arial"><b><u>STATUS:TIDAK LENGKAP</u></b></p>

        <p style="font-family: Arial">Salam Sejahtera,</p>

        <p style="font-family: Arial">Adalah dimaklumkan bahawa Permohonan Acara bagi <b>{{$data['bmb_booking_no']}}</b> adalah tidak lengkap atas sebab-sebab berikut:<br></p>

        <p style="font-family: Arial">{{$data['remark']}}</p>

        <br>
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p><br>
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>

        <p style="font-family: Arial">Sekian.</p>

    {{--tindakanKeputusanMesyuarat --}}
    {{-- lulus --}}
    @elseif ($type == 30)

    {{-- pembentanganSemula --}}
    @elseif ($type == 23)
        <p style="font-family: Arial"><b><u>STATUS:PEMBENTANGAN SEMULA</u></b></p>
        <p style="font-family: Arial">Salam Sejahtera,</p>
        <p style="font-family: Arial">Untuk makluman Permohonan Acara <b>{{$data['bmb_booking_no']}}</b> telah diterima dan Pemohon diminta untuk hadir ke Mesyuarat Jawatankuasa Kelulusan Acara yang akan diadakan seperti butiran berikut:</p>
        <table class="gridtable" style="cellspacing:0px;cellpadding;0px">
            <tbody>	
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Tarikh</b></td>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['date']}}</td>	
                </tr>
                <tr>
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Masa</b></td>	
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['time']}}</td>	
                </tr>
                <tr>
                <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Tempat</b></td>	
                <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['location']}}</td>
                </tr>
                <tr>
                <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Pengerusi</b></td> 
                <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['meeting_chairman']}}</td>
                </tr>
                <tr> 
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial"><b>Catatan</b></td>	
                    <td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;font-family: Arial">{{$data['remark']}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <p style="font-family: Arial">Pemohon perlu membuat pembentangan acara sepertimana yang dipohon dengan mengikuti garis panduan yang telah ditetapkan.</p>
        <br>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        <p style="font-family: Arial">Sekian.</p>
        
    {{-- notifylulusdeposit --}}
    @elseif ($type == 24)
        <p style="font-family: Arial"><b><u>STATUS: BAYARAN PENDAHULUAN</u></b></p>
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <p style="font-family: Arial">Sukacita dimaklumkan Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj) telah meluluskan Permohonan Acara <b>{{$data['bmb_booking_no']}}</b>. 
        Pemohon diminta untuk membuat bayaran wang pendahuluan seperti yang ternyata di dalam surat kelulusan acara yang perlu di muat turun melalui Sistem Permohonan Tempahan dalam tempoh 7 hari berkerja dari tarikh surat kelulusan dikeluarkan.
        </p>
        
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:<br></p>
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        
        <p style="font-family: Arial">Sekian.</p>

    {{-- bayaranpenuhditerima --}}
    @elseif ($type == 27)
        <p style="font-family: Arial"><b><u>STATUS:BAYARAN PENUH</u></b></p>

        <p style="font-family: Arial">Salam Sejahtera,</p>

        <p style="font-family: Arial">

        Terima kasih dan pihak kami akan membuat semakan. </p><br>

        <p style="font-family: Arial">Sekian.</p>

    {{-- lulusbersyarat --}}
    @elseif ($type == 28)
        <p style="font-family: Arial"><b><u>STATUS:TIDAK LENGKAP</u></b></p>

        <p style="font-family: Arial">Salam Sejahtera,</p>

        <p style="font-family: Arial">Adalah dimaklumkan bahawa Permohonan Acara bagi <b>{{$databooking->bmb_booking_no}}</b> adalah tidak lengkap atas sebab-sebab berikut:<br></p>

        <p style="font-family: Arial">{{$databookingreview->remark}}</p>


        <br>
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p><br>
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>

        <p style="font-family: Arial">Sekian.</p>

    {{-- gagal --}}
    @elseif ($type == 29)
        <p style="font-family: Arial"><b><u>STATUS:GAGAL/DITOLAK</u></b></p>
        
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <p style="font-family: Arial">Adalah dimaklumkan bahawa Permohonan Acara bagi <b>{{$data['bmb_booking_no']}}</b> adalah tidak diluluskan atas sebab-sebab berikut:<br></p>
        
        <p style="font-family: Arial">{{$data['remark']}}</p>
        
        
        <br>
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p><br>
        
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        
        <br>
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        
        <p style="font-family: Arial">Sekian.</p>
    
    {{-- notifyluluspenuh --}}
    @elseif ($type == 26)
        <p style="font-family: Arial"><b><u>STATUS: LULUS (PERLU BAYARAN PENUH)</u></b></p>
        
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <!-- CSS goes in the document HEAD or added to your external stylesheet -->
        
        {{-- <!--<p style="font-family: Arial">Sukacita dimaklumkan Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj) telah meluluskan Permohonan Acara <b>{{$databooking->bmb_booking_no}}</b>. 
        Pemohon diminta untuk membuat bayaran penuh berjumlah RM {{number_format((($databooking->bmb_rounding-$databooking->bmb_deposit_rm)*0.5),2)}} dalam tempoh 14 hari sebelum tarikh masuk tapak.
        </p>--> --}}
        <p style="font-family: Arial">Sukacita dimaklumkan Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj) telah meluluskan Permohonan Acara <b>{{$data['bmb_booking_no']}}</b>. 
        Pemohon diminta untuk membuat bayaran penuh dalam tempoh 14 hari sebelum tarikh masuk tapak.
        </p>
        
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p>
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        
        <p style="font-family: Arial">Terima kasih kerana menggunakan perkhidmatan kami.<br></p>
        
        <p style="font-family: Arial">Sekian.</p>
    
    {{-- notifybatal --}}
    @elseif ($type == 21)
        <p style="font-family: Arial"><b><u>STATUS:BATAL</u></b></p>
        
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <p style="font-family: Arial">
        
        Terima kasih kerana menggunakan perkhidmatan kami.</p>
        
        <p style="font-family: Arial">Sebarang pertanyaan lanjut, sila hubungi:</p>
        <p style="font-family: Arial">
        -	En. Mohd Farid bin Jamaluddin : 03-8887 7256<br>
        </p>
        
        <p style="font-family: Arial">Sekian.</p>

    {{-- selesai --}}
    @elseif ($type == 20)
        <p style="font-family: Arial"><b><u>STATUS:PROSES PERMOHONAN LENGKAP</u></b></p>
        
        <p style="font-family: Arial">Salam Sejahtera,</p>
        
        <p style="font-family: Arial">
        
        Proses permohanan <b>{{$databooking->bmb_booking_no}}</b> untuk Acara {{$databookingacara->event_name}} 
        pada {{date("d-m-Y", strtotime($databookingacara->event_date))}} di {{$databookingacara->locationname}}  telah lengkap.<br></p>
        
        <p  style="font-family: Arial">Terima Kasih kerana menggunakan perkhidmatan kami.</p><br>
        
        
        <p style="font-family: Arial">Sekian.</p>
    @endif

</html>
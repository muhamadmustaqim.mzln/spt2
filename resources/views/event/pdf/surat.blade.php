 <style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:15px;
      font-family: Arial, Helvetica, sans-serif;
      text-align:justify;
      margin-left: 0.5cm;
      margin-right: 0.5cm;

    }
     #title4{
      font-size:15px;
      font-family: Arial, Helvetica, sans-serif;
      text-align:justify;
      margin-left: 0.8cm;
      margin-right: 0.5cm;

    }
        #title3{
      font-size:15px;
      font-family: Arial, Helvetica, sans-serif;
      text-align:justify;
      

    }
      #title2{
      font-size:13px;
      font-family: Arial, Helvetica, sans-serif;
    }
     #footers{
      font-size:12px;
    }
    #table1 {
    border-collapse: collapse;
    border: 1px solid black;
    margin-left: 0.5cm;
    }
    #back{

   width:100%; 
   height:99%; 
   /*background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
    background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }
   #back1{

   width:100%; 
   height:99%; 
  /* background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
   background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat:repeat;
 
      
  }

    /* Force all children to have a specified font-size */
</style>

@php
  $getbmbno = $data['getbmbno'];
  $datesurat = $data['getbmbno']->bmb_booking_date;
  $spa_person = $data['booking_person'];
  $spa_meeting = $data['meeting_result'];
  $spa_acara = $data['booking_event'];
  $spa_event = $data['booking_event'];
  $spa_quotation_detail_all = $data['quotation_detail'];
  $sumtotal = $data['sumtotal'];
  $sumtotal_all = $data['sumtotal_all'];
  $spa_quotation_detail_bpsk = $data['spa_quotation_detail_bpsk'];
  $spa_deposit_kecuali = $data['spa_deposit_kecuali'];
  $spa_quotation_detail_all_count = $data['spa_quotation_detail_all_count'];
@endphp

<table  width="100%" border="0" cellpadding="0">
  <tr>
    <td align="right" id="footers">
    </td>
    <td>
    </td>
    <td align="right" id="footers">No. Permohonan : {{$data['getbmbno']->bmb_booking_no}}
    </td>
  </tr>
  <tr>
    <td>
      <table border="0" width="100%" cellpadding="2">
        <tr>
          <td id="title2"><strong>PERBADANAN PUTRAJAYA</strong>
          </td>
        </tr>
        <tr>
          <td id="title2"><strong>Kompleks Perbadanan Putrajaya</strong>
          </td>
       
        </tr>
        <tr>
          <td id="title2"><strong>24, Persiaran Perdana,</strong>
          </td>
          
        </tr>
        <tr>
          <td id="title2"><strong>Presint 3,</strong>
          </td>      
        
        </tr>
        <tr>
          <td id="title2"><strong>62625, Putrajaya</strong>
          </td>
       
        </tr>
        <tr>
          <td id="title2"><strong>MALAYSIA</strong>
          </td>
        </tr>

      </table>
    </td>

    <td width="20%"><img src="{{public_path('assets/media/Logo Perbadanan Putrajaya.png')}}" width="120" height="127" />
      <!--
	  <td width="15%"><img src="{{asset('packages/threef/entree/img/logo.png')}}" width="100" height="100" />
	  -->
    </td>
    <td>
      <table border="0" width="100%" cellpadding="2">
        <tr>
          <td id="title2" align="right"><strong>Telefon : 603-8000 8000</strong>
          </td>
        </tr>
        <tr> 
          <td id="title2" align="right"><strong>Laman Web : www.ppj.gov.my</strong></td>
        </tr>
      </table>
    </td>
 
  </tr>
 
</table>
<br>
<table width="100%" border="0" cellpadding="1" id="title3">
  <tr>
    <td width="20px">Ruj.Tuan
    </td>
    <td width="2px">:
    </td>
    <td>
    </td>
  </tr>
  <tr>
    <td width="20px">Ruj.Kami
    </td>
    <td width="2px">:
    </td>
    <td>
       PPj.KA.700-5/1/3&nbsp;Jld.&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
    </td>
  </tr>
  <tr>
    <td width="20px">Tarikh 
    </td>
    <td>:
    </td>
    <?php $month='';?>
    <?php if (date('m',strtotime($datesurat))=='01'){
              $month ='Januari';
    }else if(date('m',strtotime($datesurat))=='02'){
              $month ='Februari';

    }else if(date('m',strtotime($datesurat))=='03'){
              $month ='Mac';

    }else if(date('m',strtotime($datesurat))=='04'){
              $month ='April';

    }else if(date('m',strtotime($datesurat))=='05'){
              $month ='Mei';

    }else if(date('m',strtotime($datesurat))=='06'){
              $month ='Jun';

    }else if(date('m',strtotime($datesurat))=='07'){
              $month ='Julai';

    }else if(date('m',strtotime($datesurat))=='08'){
              $month ='Ogos';

    }else if(date('m',strtotime($datesurat))=='09'){
              $month ='September';

    }else if(date('m',strtotime($datesurat))=='10'){
              $month ='Oktober';

    }else if(date('m',strtotime($datesurat))=='11'){
              $month ='November';

    }else if(date('m',strtotime($datesurat))=='12'){
              $month ='Disember';

    }else{
            $month='8';

    }?>
    <td>
      {{date('d',strtotime($datesurat))}} {{$month}} {{date('Y',strtotime($datesurat))}}
    </td>
  </tr>
</table>
<br>
<table width="100%" border="0" cellpadding="2" id="title">
  <tr>
    <?php $spa_person->name = strtoupper($spa_person->name); ?>
    <td><b>{{$spa_person->name}}</b>
    </td>
  </tr>
   <tr>
    <td>{{strtoupper($spa_person->post)}}
    </td>
  </tr>
  <tr>
    <td>{{$spa_person->address}}
    </td>
  </tr>
  <tr>
    <td >{{$spa_person->postcode}},{{$spa_person->city}}
    </td>
    <td align="right">Tel
    </td>
    <td align="right" width="10px">:
    </td>
    <td align="left">{{$spa_person->office_no}}
    </td>
   
  </tr>
  <tr>
    {{-- <td>{{$spa_person->ls_description}} --}}
    <td>{{$getbmbno->ls_description}}
    </td>
      <td align="right">
      Faks
    </td>
    <td align="right">:
    </td>
    <td align="left">{{$spa_person->fax_no}}</td>
    
  </tr>
 
</table>
  
  </table>
  <br>
  <p id="title"><b>Tuan/Puan,</b></p>

  <p id="title"><b><U>KELULUSAN MENGANJURKAN ACARA DI PUTRAJAYA</U></b></p>
  <p id="title">Dengan hormatnya saya diarah merujuk kepada perkara tersebut di atas</p>
  <p id="title">2. Merujuk kepada <b>Mesyuarat Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj) {{$spa_meeting->meeting_no}} </b>yang telah diadakan pada {{date('d-m-Y',strtotime($spa_meeting->date_meeting))}}.
  Jawatankuasa telah meluluskan permohonan pihak tuan/puan untuk mengadakan acara dengan syarat-syarat seperti maklumat berikut :
  </p>
 
  <table width="90%" border="0" cellpadding="2" id="title">
    <tr>
       <td width="3%">
        &nbsp;
      </td>
    <td width="3%">
        i.
      </td>
      <td width="30%">
      Nama Acara
      </td>
      <td width="1%">
        :
      </td>
        <td>
       {{$spa_acara->event_name}}
      </td>
    </tr>
  <tr>
    <td width="3%">
        &nbsp;
      </td>
    <td width="3%">
        ii.
      </td>
      <td width="30%">
      Lokasi
      </td>
      <td width="1%">
        :
      </td>
        <td>
       {{$spa_acara->locationname}}@if($spa_acara->other_location != null) -{{$spa_acara->other_location}} @endif
      </td>
    </tr>
     <tr>  
      <td width="3%">
        &nbsp;
      </td>
    <td width="3%">
        iii.
      </td>
      <td width="30%">
      Tarikh Acara
      </td>
      <td width="1%">
        :
      </td>
      <?php $day=date("N", strtotime($spa_acara->event_date));

      if($day==1){
         $hari='Isnin';
      }elseif($day==2){
          $hari='Selasa';

      }elseif($day==3){
          $hari='Rabu';

      }elseif($day==4){
          $hari='Khamis';

      }elseif($day==5){
          $hari='Jumaat';

      }elseif($day==6){
          $hari='Sabtu';

      }else{
          $hari='Ahad';

      }


      ?>
       <?php $dayend=date("N", strtotime($spa_acara->event_date_end));

      if($dayend==1){
         $hariend='Isnin';
      }elseif($dayend==2){
          $hariend='Selasa';

      }elseif($dayend==3){
          $hariend='Rabu';

      }elseif($dayend==4){
          $hariend='Khamis';

      }elseif($dayend==5){
          $hariend='Jumaat';

      }elseif($dayend==6){
          $hariend='Sabtu';

      }else{
          $hariend='Ahad';

      }


      ?>
        <td>
      @if(date('d-m-Y',strtotime($spa_acara->event_date))==date('d-m-Y',strtotime($spa_acara->event_date_end)))
      {{date('d-m-Y',strtotime($spa_acara->event_date))}}&nbsp;({{$hari}})
      @else
      {{date('d-m-Y',strtotime($spa_acara->event_date))}}&nbsp;({{$hari}}) hingga {{date('d-m-Y',strtotime($spa_acara->event_date_end))}}&nbsp;({{$hariend}})
      @endif
       <!-- {{date('d-m-Y',strtotime($spa_acara->event_date))}}&nbsp;({{$hari}}) -->
      </td>
    </tr>
     <tr>  
      <td width="3%">
        &nbsp;
      </td>
    <td width="3%">
        iv.
      </td>
      <td width="30%">
     Masa Acara
      </td>
      <td width="1%">
        :
      </td>
        <td>

        <?php  echo date("g:i a", strtotime($spa_acara->event_time))?> - <?php  echo date("g:i a", strtotime($spa_acara->event_time_to))?>
      </td>
    </tr>
      <tr>  
        <td width="3%">
        &nbsp;
      </td>
    <td width="3%">
        v.
      </td>
      <td width="30%">
      Tarikh Masuk Tapak
      </td>
      <td width="1%">
        :
      </td>
        <?php $enter_date=date("N", strtotime($spa_acara->enter_date));

      if($enter_date==1){
         $hari_enter='Isnin';
      }elseif($enter_date==2){
          $hari_enter='Selasa';

      }elseif($enter_date==3){
          $hari_enter='Rabu';

      }elseif($enter_date==4){
          $hari_enter='Khamis';

      }elseif($enter_date==5){
          $hari_enter='Jumaat';

      }elseif($enter_date==6){
          $hari_enter='Sabtu';

      }else{
          $hari_enter='Ahad';

      }


      ?>
        <td>
       {{date('d-m-Y',strtotime($spa_acara->enter_date))}}&nbsp;({{$hari_enter}})
      </td>
    </tr>
      <tr> 
      <td width="3%">
        &nbsp;
      </td> 
    <td width="3%">
        vi.
      </td>
      <td width="30%">
     Tarikh Keluar Tapak
      </td>
      <td width="1%">
        :
      </td>
       <?php $exit_date=date("N", strtotime($spa_acara->exit_date));

      if($exit_date==1){
         $hari_exit='Isnin';
      }elseif($exit_date==2){
          $hari_exit='Selasa';

      }elseif($exit_date==3){
          $hari_exit='Rabu';

      }elseif($exit_date==4){
          $hari_exit='Khamis';

      }elseif($exit_date==5){
          $hari_exit='Jumaat';

      }elseif($exit_date==6){
          $hari_exit='Sabtu';

      }else{
          $hari_exit='Ahad';

      }


      ?>
        <td>
        {{date('d-m-Y',strtotime($spa_acara->exit_date))}}&nbsp;({{$hari_exit}})
      </td>
    </tr>
     <tr> 
     <td width="3%">
        &nbsp;
      </td> 
    <td width="3%">
        vii.
      </td>
      <td width="35%">
     Jumlah Peserta / Pengunjung
      </td>
      <td width="1%">
        :
      </td>
        <td>
        {{$spa_acara->visitor_amount}} orang
      </td>
    </tr>
  </table>
  <br>

<p id="title">3. Kelulusan ini tertakluk kepada syarat-syarat tambahan beserta kadar bayaran yang telah ditetapkan oleh JKA-PPj.
  Kadar ini tidak termasuk bayaran-bayaran lain yang mungkin akan dikenakan selepas cadangan acara secara terperinci dikemukakan oleh pihak tuan/puan.Kadar
  caj sewa tapak dan deposit acara adalah seperti berikut:
  </p>
<br>
<div style="page-break-after:always;">&nbsp;</div>
 <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">No. Permohonan : {{$getbmbno->bmb_booking_no}}
      </td>
    </tr>
  </table>
  <br>
  <table width="95%" border="1" cellpadding="2" id="title">

           
                     <tr style="page-break-inside:avoid;">
                     <th><b>Bil.</b></th>
                     <th><b>Perkara</b></th>
                     <th><b>Kadar (RM)</b></th>
                     <th width="50px"><b>Unit / Kuantiti</b></th>
                     <th><b>Diskaun</b></th>
                     <th><b>GST 0% (RM)</b></th>
                     <th><b>Jumlah (RM)</b></th>
                     </tr>
         

             <?php 
                      $i=1; 
                       ?>

            <tbody>
                @forelse($spa_quotation_detail_all as $key => $value)
                
                  <tr style="page-break-inside:avoid;">
                    <td valign="top">
                      <?php echo $i;?>
                    </td>
                  @if($value->fk_lkp_spa_payitem==10 && $value->total=='0.00')
                    <td >Sewa tapak</td>
                    <td colspan="5">Kelulusan/ Caj tapak sila rujuk kepada pemilik tapak</td>
                  @else
                    <td width="200px">
                      {{$value->description}}
                    </td>
                    <td>
                      {{number_format($value->rate,2)}} 
                      
                    </td>
                    <td >
                      @if($value->fk_lkp_spa_payitem==10)
                       {{$value->quantity}} hari
                      @else
                       {{$value->quantity}}
                      @endif
                     
                    </td>
                    
                    <td>
                       
                      {{number_format($value->discount_rm,2)}}
                    
                    </td>
                    <td>
                       @if($value->fk_lkp_spa_payitem==17)
                      -
                      @else
                      {{number_format($value->gst_rm,2)}}
                       @endif
                    </td>
                    <td>

                      <?php //if($value->exceptional==1){
                      // $total=0.00;
                      // }else{
                      //   $total=$value->total;
                      // }?>
                    
                       @if($value->fk_lkp_spa_payitem==9)<!--deposit-->
                      <?php echo number_format($value->total,2)?>@if($value->exceptional==1)(Surat Jaminan)@endif
                      @else
                      <?php echo number_format($value->total,2)?>@if($value->exceptional==1)(Dikecualikan)@endif
                      @endif
                      
                    </td>
                    @endif
         
                     </tr>  
                
              <?php $i++;?>

                  
                 @empty
                      <tr><td colspan='6'>Tiada Data</td></tr>
                 @endforelse
                   <tr style="page-break-inside:avoid;">
                      <td colspan='6' align="right">
                     <b>Jumlah Keseluruhan (RM)</b>
                      </td>
                       <td>
                      @if($sumtotal->total==0)
                     <b>{{number_format($sumtotal_all->total,2)}} (Dikecualikan)</b>
                     @else
                     <b>{{number_format($sumtotal->total,2)}}</b>
                     @endif
                    </td>
                    </tr>

  
    </tbody>

    </table>

     <!-- <div style="page-break-after:always;">&nbsp;</div>
    
     <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">No. Permohonan : {{$getbmbno->bmb_booking_no}}
      </td>
    </tr>
  </table> -->
 

    <br>
    <p id="title">4. Pihak tuan/puan juga diminta untuk membayar kos-kos lain yang terlibat serta mendapatkan kelulusan/permit
      seperti di bawah sekiranya perkara tersebut dilaksanakan/disediakan sebelum/semasa/selepas acara berlangsung :<br>
  </p>
  <table width="93%" border="0" cellpadding="2" id="title4">
    <tr>
      <td width="1%" valign="top">
        i. 
      </td>
      <td>
        Kos Pembersihan Sampah : Perbadanan Pengurusan Sisa Pepejal dan Pembersihan Awam (SWCorp).
      </td>
    </tr>
    <tr>
      <td width="1%" valign="top">
        ii.
      </td>
      <td>
         Permit Struktur Sementara/Khemah: Bahagian Pembangunan Tanah dan Kelulusan Pelan, Jabatan Perancangan Bandar,Perbadanan Putrajaya.
      </td>
     </tr>
     <tr>
      <td width="1%" valign="top">
        iii. 
      </td>
     <td>
      Aktiviti Komersil/Jualan, Pemasangan Bunting atau Banner. Bahagian Komersil, Pembangunan Perniagaan Dan Pelesenan,Jabatan Perkhidmatan Bandar, Perbadanan Putrajaya.
     </td> 
    </tr>
     <tr>
      <td width="1%" valign="top">
        iv. 
      </td> 
      <td>
        Mengemukakan Lesen dari Pesuruhjaya Sukan Malaysia bagi Acara Sukan Berstatus Antarabangsa (Akta Pembangunan Sukan 1997).
      </td> 
    </tr>
      <tr> 
      <td  width="1%" valign="top">
        v.
      </td>
      <td>
       Lain-lain kelulusan dan permit dari pihak berkaitan seperti BOMBA, Polis (termasuk kelulusan laluan), Suruhanjaya Tenaga dan lain-lain.
      </td>
    </tr>
     <tr>
      <td width="1%" valign="top">
        vi.
      </td>
      <td>
      Kebenaran penggunaan tapak dari Marina Putrajaya Sdn Bhd/ Putrajaya Holdings Sdn Bhd/ lain-lain (jika Berkaitan). 
      </td>   
    </tr>
  </table>
  <br>
  @if($spa_event->letter_no_five=='')
  <p id="title">5. Bayaran wang pendahuluan sebanyak 50% dari keseluruhan sewa tapak dan caj pengurusan acara berjumlah <b>RM {{number_format($getbmbno->bmb_subtotal*0.5,2)}} ({{$getbmbno->bmb_total_word}} sahaja) </b>
    perlu dijelaskan bersama-sama surat akuan terima dan aku janji ini. Pembayaran penuh untuk lainlain caj, 
    baki sewa tapak perlu diselesaikan 7 hari sebelum tarikh masuk tapak. Wang pendahuluan ini tidak akan dipulangkan 
    sekiranya pembatalan atau pertukaran tarikh di buat 30 hari sebelum tarikh acara berlangsung. 
    Sebarang perubahan perlu mendapat kelulusan JKA-PPj. Pembayaran caj Sewa Tapak atau Deposit Acara hendaklah dibuat 
    di Kaunter Bayaran mengikut mana yang berkenaan dengan menggunakan mod bayaran seperti tunai,kad kredit atau deraf bank atas nama Perbadanan Putrajaya.</p>
  
  @else

   <p id="title">5. <?php echo nl2br($spa_event->letter_no_five) ?></p>
  
  @endif



     <p id="title">6. Sekiranya pihak tuan/puan bersetuju dengan perkara-perkara seperti di atas, sila tandatangan Surat 
      akuan terima dan aku janji seperti di bawah dalam tempoh 7 hari dari tarikh surat ini dikeluarkan dan diserahkan 
      ke Bahagian Komunikasi Korporat, Jabatan Perkhidmatan Korporat, Perbadanan Putrajaya. Sekiranya pihak kami tidak 
      menerima surat akuan ini dan pembayaran Wang pendahuluan dalam tempoh seperti yang ditetapkan, 
      kelulusan ini akan dikira terbatal.
   </p>
    <div style="page-break-after:always;">&nbsp;</div>
    
     <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">No. Permohonan : {{$getbmbno->bmb_booking_no}}
      </td>
    </tr>
  </table>
    <p id="title">7. Bersama-sama ini juga dilampirkan syarat dan peraturan yang telah ditetapkan oleh Perbadanan
     Putrajaya untuk dipatuhi. Untuk maklumat lanjut, pihak puan boleh menghubungi
       Pn. Izza Nina binti Hazmi @ Hajemi di talian 03-8887 7615 / izza.nina@ppj.gov.my.
   </p>
   <p id="title">Sekian dimaklumkan, terima kasih.</p>
   <p id="title"><b>&#34;BANDAR RAYA BESTARI KEHIDUPAN BERKUALITI&#34; <br>&#34;MALAYSIA MADANI&#34; <br>&#34;BERKHIDMAT UNTUK NEGARA&#34;</b></p>
    <p id="title"><b>Saya yang menjalankan amanah, <b></p>
     <p id="title"><b>Pengerusi Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj)<b></p>
   <p id="title"><b><i>Cetakan Komputer,tiada tandatangan diperlukan<i><b></p>


  <div style="page-break-after:always;">&nbsp;</div>
   <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">No. Permohonan : {{$getbmbno->bmb_booking_no}}
      </td>
    </tr>
  </table>
    <br>
  <p id="title" align="center">SYARAT DAN GARIS PANDUAN PENGANJURAN ACARA</p>
  <p id="title"><b>1. KEBERSIHAN<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
	Penganjur adalah bertanggungjawab sepenuhnya dalam memastikan kebersihan tapak acara SETIAP MASA, dijaga sama ada semasa persiapan tapak, semasa acara berjalan dan selepasi selesai acara.
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        ii.
      </td>
      <td>
	Surat Persetujuan memberi perkhidmatan dari pihak Perbadanan Pengurusan Sisa Pepejal Dan Pembersihan Awam (SWCorp) perlu dikemukakan ke Perbadanan Putrajaya
      </td>
    </tr>
    
  </table>
  <br>
  <p id="title"><b>2. LALULINTAS<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
	Mengemukakan Pelan pengurusan Trafik kepada Bahagian Penguatkuasa, Perbadanan Putrajaya.
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        ii.
      </td>
      <td>
	Sekiranya acara melibatkan penutupan/ lencongan jalan jalan utama di Putrajaya, pihak penganjur hendaklah menyiarkan notis penutupan/ lencongan jalan dalam akhbar tempatan, website dan melalui media sosial.
      </td>
    </tr>
     <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        iii.
      </td>
      <td>
	Mengambil khidmat RELA, POLIS dan lain-lain.
      </td>
    </tr>
    
  </table>
  <br>
  <p id="title"><b>3. KESELAMATAN,KEMALANGAN DAN KEROSAKAN<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
       Mengemukakan <i>Public Liability Insurance</i> kepada PPj dalam tempoh 7 hari sebelum kerja-kerja persiapan tapak dilaksanakan. 
     Tempoh perlindungan insuran hendaklah sehingga tapak acara dikosongkan
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        ii.
      </td>
      <td>
     Menyediakan khidmat kecemasan (ambulan/tempat rawatan kecemasan di tapak).
    </td>
    </tr>
     <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        iii.
      </td>
      <td>
    Membaiki/mengganti segala kerosakan (utiliti, aset ppj, komponen landskap, rumput
    dan segala komponen sediada).

      </td>
    </tr>
    
  </table>
   <br>
  <p id="title"><b>4. PENGUNAAN LOGO PERBADANAN PUTRAJAYA<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
       Tidak dibenarkan menggunakan logo Perbadanan Putrajaya kecuali dengan kebenaran Perbadanan Putrajaya.
      </td>
    </tr>    
  </table>
    <br>
  <p id="title"><b>5. PEMULANGAN DEPOSIT<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
       Sila kemukakan resit asal bersama-sama surat iringan bagi tuntutan semula wang
        deposit acara setelah tapak dikosongkan sepenuhnya.
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        ii.
      </td>
      <td>
    Jumlah pemulangan tertakluk kepada keputusan Jawatankuasa Kelulusan Acara
     Perbadanan Putrajaya (JKA-PPj).
    </td>
    </tr>
    
  </table>
   <br>
  <p id="title"><b>6. PINDAAN TARIKH<b></p>
  <table width="95%" border="0" cellpadding="1" id="title">
    <tr>
      <td>
        &nbsp;
      </td>
      <td style="vertical-align:top">
        i.
      </td>
      <td>
      Perbadanan Putrajaya berhak membuat pindaan pada tarikh yang diluluskan tertakluk kepada keutamaan <b>Acara Rasmi 
      Negara</b> dan lain-lain keperluan semasa.

      </td>
    </tr>    
  </table>
  <br>
    <div style="page-break-after:always;">&nbsp;</div>
     <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">No. Permohonan : {{$getbmbno->bmb_booking_no}}
      </td>
    </tr>
  </table>
    <br>
    <p id="title"><u><b>SURAT AKUAN TERIMA DAN AKU JANJI PENGANJURAN ACARA DI PUTRAJAYA<b></u></p>
    <p id="title"><b>Dengan ini saya/kami bersetuju dan menerima perkara-perkara seperti yang dinyatakan.<b></p>
    <p id="title"><b>1. Dimaklumkan bahawa saya/kami telah memahami semua syarat kelulusan yang berkaitan dan
    dengan ini kami membuat aku janji bahawa saya/kami akan mematuhi semua syarat yang telah ditetapkan oleh pihak PPj.
    <b></p>
    <p id="title"><b>2. Sekiranya didapati kualiti kerja pembersihan/ penyelenggaraan dan lain-lain kerja yang 
      berkaitan yang dilakukan oleh pihak saya/kami tidak mematuhi standard yang telah ditetapkan, 
      saya/kami bersetuju supaya PPj melaksanakan kerja-kerja ini di mana kerja-kerja ini akan dibiayai oleh 
      pihak saya/kami atau ditolak dari wang deposit yang dikemukakan. Saya/kami juga akan membiayai kos bagi segala
       kerosakan/kehilangan aset-aset PPj semasa aktiviti ini dilaksanakan.
    <b></p>
       <p id="title"><b>3. Saya/kami juga tidak akan mempertanggungjawabkan PPj bagi sebarang insiden termasuklah 
        kehilangan/kerosakan harta benda pihak penganjur, kecederaan, dan kehilangan nyawa, baik di pihak penganjur
         mahupun orang awam. Saya/kami akan menanggung segala liabiliti dan mengganti rugi dan terus mengganti rugi
          PPj bagi apa-apa tuntutan, kerugian, apa-apa tindakan atau prosiding yang mungkin dibawa terhadap PPj akibat 
          daripada atau disebabkan oleh kerja-kerja yang terlibat dalam penganjuran acara tersebut.

    <b></p>
    <p id="title"><b>Sekian dimaklumkan, terima kasih.<b></p>
          <table width="100%" border="0" id="title">
            <tr>
              <td>
                &nbsp;
              </td>
              <td>
                &nbsp;
              </td>
            </tr>
              <tr>
              <td>
                &nbsp;
              </td>
              <td>
                &nbsp;
              </td>
            </tr>
              <tr>
              <td>
                &nbsp;
              </td>
              <td>
                &nbsp;
              </td>
            </tr>
            <tr>
               <td>
                 -------------------------------------------
              </td>
              <td>
                 -------------------------------------------
              </td>
            </tr>
             <tr>
               <td>
                 <b>(Tandatangan Pemohon/Wakil)</b>
              </td>
               <td>
                 <b>(Tandatangan Wakil PPj)</b>
              </td>
            </tr>
             <tr>
               <td>
                 <b>Nama : </b>
              </td>
               <td>
                 <b>Nama : </b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>Tarikh : </b>
              </td>
              <td>
                 <b>Tarikh : </b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>Jawatan : </b>
              </td>
               <td>
                 <b>Jawatan : </b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>Alamat : </b>
              </td>
              <td>
                 <b>Alamat : </b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
              <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
              <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
              
            </tr>
             <tr>
               <td>
                  <b>-----------------------------------------------------------------</b>
              </td>
              <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
              
            </tr>
             <tr>
               <td>
                 <b>------------------------------------------------------------------</b>
              </td>
              <td>
                 <b>-----------------------------------------------------------------</b>
              </td>
            </tr>
            </tr>
          </table>
    
     <p id="title"><b>Cop Agensi : <b></p>
<br>
    <table width="40%" id="table1">
      <tr>
        <td>&nbsp;
        </td>  
      </tr>
      <tr>
        <td>&nbsp;
        </td>  
      </tr>
      <tr>
        <td>&nbsp;
        </td>  
      </tr>
      <tr>
        <td>&nbsp;
        </td>  
      </tr>
      <tr>
        <td>&nbsp;
        </td>  
      </tr>
        <tr>
        <td>&nbsp;
        </td>  
      </tr>
    </table>

      <div style="page-break-after:always;">&nbsp;</div>
       <table  width="100%" border="0" cellpadding="0">
     <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
      <td align="right" id="footers">BPSK-1/2015
      </td>
    </tr>
  </table>
<!--start borang bpsk-->
  <table  width="100%" border="0" cellpadding="0" id="title2">
    <tr>
      <td align="right" id="footers">
      </td>
      <td>
      </td>
     <!--  <td width="10%"align="right" id="footer">BPSK-1/2015
      </td> -->
    <tr>
    <td width="20%"><img src="{{asset('packages/threef/entree/img/logo.png')}}" width="100" height="100" />
    </td>
    <td style="vertical-align:bottom">
     <center><strong>BORANG PERMOHONAN TEMPAHAN PERKHIDMATAN/PROGRAM/AKTIVITI/MAJLIS/SEWAAN DAN LAIN-LAIN</strong><br>
     (Tempahan melalui: Surat / Telefon / Faks / E-Mel / Kaunter)</center></td>
  </tr>
</table>
    </td>
  </tr>
    </tr>
  </table>
  <br>
  <table id="title2" width="100%" border="1" cellpadding="5" cellspacing="0">
                <tr>
                  <td align="center" colspan="3">
                    <strong>A. BUTIRAN PEMOHON</strong>
                  </td>
                  <td align="center" colspan="1">
                    <strong>{{$getbmbno->bmb_booking_no}}</strong>
                  </td>
                </tr>
                <tr>
                  <td width="30%">
                    NAMA
                    (Individu/Syarikat/Organisasi)
                  </td>
                  <td width="70%" colspan="3">
                  {{$spa_person->name}}
                  </td>
                </tr>
                 <tr>
                  <td width="30%">
                   NO KAD PENGENALAN/<BR>
                   PENDAFTARAN SYARIKAT
                  </td>
                  <td width="30%">
                 {{$spa_person->ic_no}}
                  </td><td width="5%">
                   E-MAIL
                  </td>
                  <td width="20%">
                  {{$spa_person->email}}
                  </td>
                </tr>
                  <tr>
                  <td width="30%">
                  NO.GST<br>
                  </td>
                  <td width="70%" colspan="3" style="vertical-align:top">
                   {{$spa_person->GST_no}}<br>
                  </td>
                </tr>
                  <tr>
                  <td width="30%">
                  ALAMAT<br>
                  &nbsp;<br>
                   &nbsp;<br>
                 
                  </td>
                  <td width="70%" colspan="3" style="vertical-align:top">
                   {{$spa_person->address}},{{$spa_person->postcode}},{{$spa_person->city}},
                   {{$getbmbno->ls_description}},
                    {{$spa_person->lc_description}}<br>
                   &nbsp;<br>
                    &nbsp;<br>
                  </td>
                </tr>
                <tr>
                  <td colspan="1">NO TEL (P) :  {{$spa_person->office_no}}</td>
                  <td colspan="1" width="30%">NO TEL (H/P) : {{$spa_person->hp_no}}</td>
                  <td colspan="2">NO FAKS : {{$spa_person->fax_no}}</td>
                </tr>
                 <tr>
                  <td align="center" colspan="4">
                    <strong>B. SENARAI PERKHIDMATAN / TAPAK / PERALATAN / KELENGKAPAN YANG DIPERLUKAN</strong>
                  </td>
                </tr>
                 <tr>
                  <td width="20%">TARIKH</td>
                @if(date('d-m-Y',strtotime($spa_acara->event_date))==date('d-m-Y',strtotime($spa_acara->event_date_end)))
                <td>{{date('d-m-Y',strtotime($spa_acara->event_date))}}</td>
                @else
                 <td>{{date('d-m-Y',strtotime($spa_acara->event_date))}} hingga {{date('d-m-Y',strtotime($spa_acara->event_date_end))}}</td>
                @endif
                  
                  <td width="20%" align="right">MASA </td>
                  <td> {{$spa_acara->event_time}}</td>
                </tr>
                 <tr>
                  <td width="20%">NAMA PROGARM/TUJUAN TEMPAHAN</td>
                  <td>{{$spa_acara->event_name}}</td>
                  <td width="20%" align="right">LOKASI </td>
                  @if($spa_acara->id==1 || $spa_acara->id==2)
                  <td>{{$spa_acara->locationname}} ({{$spa_acara->other_location}})</td>
                  @else
                  <td>{{$spa_acara->locationname}}</td>
                  @endif
                </tr>
                <tr>
                  <td colspan="2">JUMLAH PESERTA : {{$spa_acara->visitor_amount}} orang</td>
                  <td colspan="2" align="center"><b>(Diisi oleh PPj)<b></td> 
                </tr>
                 <tr>
                  <td colspan="1">BUTIRAN</td>
                  <td colspan="1" align="center">KUANTITI</td> 
                  <td colspan="1" align="center"><b>KADAR (RM) <b></td> 
                  <td colspan="1" align="center"><b>AMAUN (RM)<b></td> 
                </tr>
                 <?php 
                      $i=1; 
                      $gsttotal=0;
                      $amaun_tempahan=0;
                       ?>
                 @forelse($spa_quotation_detail_bpsk as $key => $value)
                <tr>
                  <td>
                    <?php echo $i;?>. {{$value->description}}
                  </td>
                  <td align="center">
                    {{$value->quantity}}
                  </td>
                  <td align="right">
                    {{number_format($value->rate,2)}}
                  </td>
                 <td align="right">
                   <?php //if($value->exceptional==1){
                   //    $total=0.00;
                   //    }else{
                   //      $total=$value->total;
                      //}?>
                    

                      
                    <?php echo number_format(($value->total-$value->gst_rm),2)?>@if($value->exceptional==1) (Dikecualikan)@endif
                  </td>
                </tr>
              
                 <?php $i++;?>
                 <?php $gsttotal+=$value->gst_rm;
                       $amaun_tempahan+=($value->total-$value->gst_rm);

                 ?>
                 @empty
                      <tr><td colspan='4'>Tiada Data</td></tr>
                 @endforelse
                 <tr>
                  <td colspan="3" align="right">
                 Jumlah Amaun Tempahan
                  </td>
                  <td align="right">
                {{number_format(($amaun_tempahan),2)}}
                  </td>
                </tr>
                <tr>
                  <td colspan="3" align="right">
                  0% GST
                  </td>
                  <td align="right">
                  {{number_format($sumtotal->totalgst,2)}}
                  </td>
                </tr>
                 <tr>
                  <td colspan="3" align="right">
                 Amaun Cagaran
                  </td>
                  <td align="right">
                    {{number_format($getbmbno->bmb_deposit_rm,2)}}@if($spa_deposit_kecuali->exceptional==1) (Surat Jaminan)@endif
                  </td>
                </tr>
                 <tr>
                  <td colspan="3" align="right">
                  Pelarasan / Pengenapan Sen
                  </td>
                  <?php $genap=0;
                        $genap=(($getbmbno->bmb_rounding)-($getbmbno->bmb_subtotal+$getbmbno->bmb_deposit_rm));
                   ?>
                  <td align="right">
                    <?php echo number_format($genap,2)?>
                  </td>
                </tr>
                 <tr>
                  <td colspan="3" align="right">
                  Jumlah Amaun Kena Bayar
                  </td>
                  <td align="right">
                   {{number_format($sumtotal->total,2)}}
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    Nota: Sertakan dokumen sebut harga /Pesanan Tempatan (LO)
                  </td>
                </tr>   
                   
                 
                
                  @if($spa_quotation_detail_all_count >3)
                 </table>
                 <div style="page-break-before:always;">
                  <table id="title2" width="100%" border="1" cellpadding="5" cellspacing="0">
                 <tr style="page-break-inside: avoid">
                  @else
                   <tr>
                @endif
          
                  <td align="center" colspan="4">
                    <strong>C. PERAKUAN TEMPAHAN</strong>
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                   Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun yang 
                   dikenakan dan akan <b>dijelaskan tidak lewat daripada 14 hari sebelum </b> perkhidmatan / program / 
                   aktiviti / majis / Acara / Sewaan yang ditempah berlangsung<br>
                   Tandatangan : <br>
                   <br>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                   Nama & Jawatan Permohonan :  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tarikh:
                  
                  </td>

                </tr>

                <tr>
                </tr>
                    </table>
                 <div style="page-break-before:always;">
                  <table id="title2" width="100%" border="1" cellpadding="5" cellspacing="0">
                  <tr style="page-break-inside: avoid">
               <tr>
                  <td align="center" colspan="4">
                    <strong>D. KEGUNAAN PEJABAT PERBADANAN PUTRAJAYA</strong>
                  </td>
                </tr>
                 <tr >
                  <td colspan="2">
                   <b>Disi Oleh Jabatan Perlaksana</b><br>
                 <br>
                   <b>Nama Pegawai Melulus :</b><br>
                   <b>Pengerusi Jawatankuasa Kelulusan Acara Perbadanan Putrajaya (JKA-PPj)</b><br><br>
                   <b><i>Cetakan komputer, tandatangan tidak diperlukan.</i></b><br>
                   &nbsp;
                  </td>
                   <td colspan="2">
                    <b>Disi Oleh Jabatan Kewangan</b><br>
                  <br>
                   <b>No BP: </b><br>
                   <b>No Kontrak : </b><br>
                   <b>No Bil :</b><br>
                   <b>Tandantangan :</b><br>
                    <b>Tarikh :</b>

                  </td>
                </tr>

</table>
</body>


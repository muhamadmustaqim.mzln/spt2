<!DOCTYPE html>
<html>
    <head>
        <title>Borang Rumusan Permohonan</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>

        @page {
                margin-top: 25px;
            }
            body{
                font-family: arial, sans-serif;
            }

            .page-break {
                page-break-before: always;
            }

            .kosong{
                margin-bottom: 0;
                font-size: 9px;
            }

            .standard11{
                font-size: 11px;
                margin-bottom: 0;
            }

            .standard12{
                font-size: 12px;
                margin-bottom: 0;
            }

            .standard13{
                font-size: 13px;
                margin-bottom: 0;
                padding-bottom: 0;
            }
            .standard11b{
                font-size: 11px;
                margin-bottom: 0;
                font-weight: bold;
            }

            .standard12b{
                font-size: 12px;
                margin-bottom: 0;
                font-weight: bold;
            }

            .standard13b{
                font-size: 13px;
                margin-bottom: 0;
                padding-bottom: 0;
                font-weight: bold;
            }

            .tajuk{
                font-size: 14px;
                font-weight: bold;
            }

            .table td, .table th{
                border-top: 0;
            }

            .table-bordered{
                border: 1px solid black !important;
            }
            
            .table-bordered th {
                border: 1px solid black !important;
                background-color: #DEE2E6;
            }

            .table-bordered td {
                border: 1px solid black !important;
            }

            .buang{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
                padding-left: 3px !important;
            }

            .buang1{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
            }
            .marginpaddingzero{
                padding: 0px !important;
                margin: 0px !important;
            }
            p {
                padding: 0;
                margin: 0;
            }
            .pm-0 {
                padding: 0;
                margin: 0;
            }
            .verticalTop {
                vertical-align: top
            }
            .text-align-center {
                text-align: center
            }
            span.lines {
                display: inline-block;
                width: 70%;
                height: 20px;
                border: 1px solid blue;
                background-color: yellow;
                margin-left: auto; 
            }

            .kotakGambar{
                margin: auto;
                /* border: 0.5px solid #DADADA; */
                width: 60px;
                height: 70px
            }
        </style>
    </head>
    <body>
        <main> 
            {{-- Page 1 --}}
            <div class="page1">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="marginpaddingzero" style="text-align: center; width: 100%; height: 90px">
                                <img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" alt="" style="height: 90px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="marginpaddingzero tajuk" style="text-align: center; width: 100%; font-weight: bold">
                                SENARAI SEMAK PERMOHONAN ACARA
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table-bordered" style="margin-bottom: 10px">
                    <thead>
                        <tr class="tajuk" style="text-align: center">
                            <th class="buang" style="width: 5%">Bil.</th>
                            <th class="buang">PERKARA</th>
                            <th class="buang">TINDAKAN (/)</th>
                            <th class="buang">KEGUNAAN PPj</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="standard12">
                            <td class="buang" style="width: 5%; text-align: center">1.</td>
                            <td class="buang" style="width: 55%">Surat Iringan(Cover letter)Permohonan Daripada Penganjur Acara</td>
                            <td class="buang text-align-center" style="width: 15%">
                                @if($data['booking_attachment']->contains('fk_lkp_spa_filename', 1))
                                    /
                                @endif
                            </td>
                            <td class="buang"></td>
                        </tr>
                        <tr class="standard12">
                            <td class="buang" style="width: 5%; text-align: center">2.</td>
                            <td class="buang" style="width: 55%">Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</td>
                            <td class="buang text-align-center" style="width: 15%">
                                @if($data['booking_attachment']->contains('fk_lkp_spa_filename', 2))
                                    /
                                @endif
                            </td>
                            <td class="buang"></td>
                        </tr>
                        <tr class="standard12">
                            <td class="buang" style="width: 5%; text-align: center">3.</td>
                            <td class="buang" style="width: 55%">Surat Pelantikan Pengurus Acara (jika berkaitan)</td>
                            <td class="buang text-align-center" style="width: 15%">
                                @if($data['booking_attachment']->contains('fk_lkp_spa_filename', 3))
                                    /
                                @endif
                            </td>
                            <td class="buang"></td>
                        </tr>
                        <tr class="standard12">
                            <td class="buang" style="width: 5%; text-align: center">4.</td>
                            <td class="buang" style="width: 55%">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</td>
                            <td class="buang text-align-center" style="width: 15%">
                                @if($data['booking_attachment']->contains('fk_lkp_spa_filename', 4))
                                    /
                                @endif
                            </td>
                            <td class="buang"></td>
                        </tr>
                    </tbody>
                </table>

                <div style="background-color: #DEE2E6; border: 1px solid black">
                    <table style="padding-right: 5px">
                        <tbody>
                            <tr class="">
                                <td colspan="2"><p class="tajuk" style="text-align: center; text-decoration: underline">SYARAT-SYARAT PERMOHONAN ACARA </p></td>
                            </tr>
                            <tr class="standard13">
                                <td style="vertical-align: top; text-align: right; padding-right: 20px">1.</td>
                                <td style="line-height: 130%">Pemohon acara perlu menghantar semua dokumen yang lengkap selewat-lewatnya 60 hari sebelum tarikh penganjuran acara kepada: </td>
                            </tr>
                            <tr class="standard13">
                                <td></td>
                                <td style="padding: 0px 20px; line-height: 130%">Pengarah,<br>
                                    Bahagian Komunikasi Korporat,<br>
                                    Jabatan Perkhidmatan Korporat,<br>
                                    Kompleks Perbadanan Putrajaya,<br>
                                    Aras 7, Blok B,<br>
                                    24, Persiaran Perdana,<br>
                                    Presint 3, 62675 Putrajaya.<br>
                                    Telefon: 03-8887 7583<br>
                                    E-mel: shima@ppj.gov.my / ashmar@ppj.gov.my / izza.nina@ppj.gov.my
                                </td>
                            </tr>
                            <tr class="standard13" style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">2.</td>
                                <td>Pemohon acara perlu membuat pembentangan cadangan acara kepada Panel Mesyuarat <span style="font-weight: bold"> Jawatankuasa Kelulusan Acara Perbadanan Putrajaya</span>(JKA-PPj) yang akan bersidang setiap 2 minggu sekali. </td>
                            </tr>
                            <tr class="standard13"style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">3.</td>
                                <td>Kelulusan dan kadar bayaran untuk menganjurkan acara tertakluk kepada kelulusan JKA-PPj. </td>
                            </tr>
                            <tr class="standard13"style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">4.</td>
                                <td>Kadar bayaran sewa tapak sebanyak 50% perlu dibayar dalam tempoh 7 hari dari tarikh Surat Kelulusan Acara dikeluarkan. Baki bayaran dan lain-lain caj yang dikenakan perlu dijelaskan 14 hari dari tarikh masuk tapak. </td>
                            </tr>
                            <tr class="standard13"style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">5.</td>
                                <td>Kadar sewa tapak dikira dari tarikh masuk tapak sehingga tarikh keluar tapak. </td>
                            </tr>
                            <tr class="standard13"style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">6.</td>
                                <td>Penganjur acara yang dilantik perlu mengemukakan surat lantikan dari penganjur utama. </td>
                            </tr>
                            <tr class="standard13"style="line-height: 130%">
                                <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">7.</td>
                                <td>Sila pastikan maklumat yang diberikan adalah tepat dan betul. </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div style="border: 1px solid black; margin: 10px 0px">
                    <p class="standard13" style="font-weight: bold; text-align: center; padding: 0px; margin: 0px; line-height: 130%">UNTUK SEMAKAN BAHAGIAN KOMUNIKASI KORPORAT,<br>JABATAN PERKHIDMATAN KORPORAT, PERBADANAN PUTRAJAYA</p>
                </div>
                
                <div style="">
                    <table class="table" style="border: 1px solid black;">
                        <tbody >
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%; padding-top: 20px">Ulasan </td>
                                <td style="width: 50%; padding-top: 20px">: __________________________________________</td>
                                <td style="width: 30%; padding-top: 20px; border-left: 1px solid black;">Diterima: </td>
                            </tr>
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%">&nbsp;</td>
                                <td style="width: 50%">&nbsp;&nbsp;__________________________________________</td>
                                <td style="width: 30%; border-left: 1px solid black"></td>
                            </tr>
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%">Nama Pegawai</td>
                                <td style="width: 50%; ">: __________________________________________</td>
                                <td style="width: 30%; border-left: 1px solid black"></td>
                            </tr>
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%; padding-top: 50px">Tandatangan</td>
                                <td style="width: 50%; padding-top: 50px">: __________________________________________</td>
                                <td style="width: 30%; padding-top: 50px; border-left: 1px solid black"></td>
                            </tr>
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%">Jawatan</td>
                                <td style="width: 50%; ">: __________________________________________</td>
                                <td style="width: 30%; border-left: 1px solid black"></td>
                            </tr>
                            <tr class="standard13" style="line-height: 0%;">
                                <td style="width: 15%">Tarikh</td>
                                <td style="width: 50%; ">: __________________________________________</td>
                                <td style="width: 30%; border-left: 1px solid black"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- Page 1 --}}

            {{-- Page 2 --}}
            <div class="page2">
                <div class="kotakGambar"></div>
                <div class="marginpaddingzero tajuk" style="text-align: center; width: 100%; font-weight: bold">
                    BORANG PERMOHONAN ACARA DI PUTRAJAYA
                </div>

                <table class="table-bordered" style="width: 100%">
                    <thead>
                        <tr class="tajuk" style="text-align: center">
                            <th colspan="3" class="buang">BUTIRAN PEMOHON:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NAMA PEMOHON:</td>
                            <td colspan="2" class="buang standard12">{{ strtoupper($data['booking_person']->name) }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%; vertical-align: top">ALAMAT PEMOHON:</td>
                            <td colspan="2" class="buang standard12" style="height: 70px;">{{ $data['booking_person']->address }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NO. GST:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->GST_no }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NO. KAD PENGENALAN / NO PENDAFTARAN SYARIKAT:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->ic_no }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NO. TELEFON PEJABAT:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->office_no }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NO. TELEFON BIMBIT:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->hp_no }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NO. FAKS:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->fax_no }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">E-MEL:</td>
                            <td colspan="2" class="buang standard12">{{ $data['booking_person']->email }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%" rowspan="3">MAKLUMAT PENGANJUR UTAMA:<br>*Sekiranya permohonan dibuat oleh pengurus acara yang dilantik</td>
                            <td class="buang standard12" style="width: 20%">Nama Agensi: </td>
                            <td class="buang standard12">@if (isset($data['booking_organiser'])) {{ $data['booking_organiser']->agency_name }} @endif</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12" style="width: 20%">Alamat: </td>
                            <td class="buang standard12">@if (isset($data['booking_organiser'])) {{ $data['booking_organiser']->address }} @endif</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12" style="width: 20%">No Tel: </td>
                            <td class="buang standard12">@if (isset($data['booking_organiser'])) {{ $data['booking_organiser']->telephone_no }} @endif</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table-bordered" style="margin-bottom: 10px; width: 100%">
                    <thead>
                        <tr class="tajuk" style="text-align: center">
                            <th colspan="5" class="buang">BUTIRAN ACARA:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="">
                            <td class="buang standard12b" style="width: 30%">NAMA ACARA:</td>
                            <td colspan="4" class="buang standard12">{{ $data['booking_event']->event_name }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" style="vertical-align: top">TARIKH ACARA:</td>
                            <td colspan="4" class="buang standard12">{{ $data['booking_event']->event_date }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">MASA PENGANJURAN:</td>
                            <td colspan="4" class="buang standard12">{{ $data['booking_event']->event_time }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">LOKASI ACARA:</td>
                            <td colspan="4" class="buang standard12">{{ Helper::locationspa($data['booking_event']->fk_spa_location) }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">JUMLAH PESERTA/PENGUNJUNG:</td>
                            <td colspan="4" class="buang standard12">{{ $data['booking_event']->visitor_amount }}</td>
                        </tr>
                        <tr class="">
                            <td rowspan="2" class="buang standard12b" style="vertical-align: top">PERINGKAT PENGANJURAN:</td>
                            <td class="buang standard12b">i. Antarabangsa ( @if($data['booking_event']->rank_host == 1) / @endif )</td>
                            <td colspan="3" class="buang standard12b">ii. Kebangsaan ( @if($data['booking_event']->rank_host == 2) / @endif )</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">iii. Putrajaya ( @if($data['booking_event']->rank_host == 3) / @endif )</td>
                            <td colspan="3" class="buang standard12b">iv. Lain-lain (Nyatakan) {{ $data['booking_event']->other_rank }}</td>
                        </tr>
                        <tr class="">
                            <td rowspan="2" class="buang standard12b" style="vertical-align: top">MASUK TAPAK</td>
                            <td class="buang standard12b">Tarikh:</td>
                            <td colspan="3" class="buang standard12">{{ $data['booking_event']->enter_date }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">Masa:</td>
                            <td colspan="3" class="buang standard12">{{ $data['booking_event']->enter_time }}</td>
                        </tr>
                        <tr class="">
                            <td rowspan="2" class="buang standard12b" style="vertical-align: top">KELUAR TAPAK</td>
                            <td class="buang standard12b">Tarikh:</td>
                            <td colspan="3" class="buang standard12">{{ $data['booking_event']->exit_date }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b">Masa:</td>
                            <td colspan="3" class="buang standard12">{{ $data['booking_event']->exit_time }}</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard11">Melibatkan Penggunaan Drone atau Alat Kawalan Jauh (terbang)-Tandakan (/):</td>
                            <td class="buang text-align-center">@if(($data['booking_event']->drone) == 1) / @endif</td>
                            <td class="buang standard12" style="text-align: center">Ya</td>
                            <td class="buang text-align-center">@if(($data['booking_event']->drone) == 0) / @endif </td>
                            <td class="buang standard12" style="text-align: center">Tidak</td>
                        </tr>
                        <tr class="">
                            <td class="buang standard12b" colspan="5">NAMA TETAMU KEHORMAT UTAMA (SEKIRANYA ADA):<br><span class="buang standard12">{{ $data['booking_event']->vip_name }}</span>&nbsp;</td>
                        </tr>
                        <tr class="">
                            <td class="buang" style="height: 80px; vertical-align: top" colspan="5"><span class="buang standard12b">MAKLUMAT TAMBAHAN:</span><br><span class="buang standard12">{{ $data['booking_event']->remark }}</span>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- Page 2 --}}
            
            {{-- Page 3 --}}
            <div class="page3">
                <div class="kotakGambar"></div>
                <div class="marginpaddingzero tajuk" style="text-align: center; width: 100%; font-weight: bold; text-decoration: underline">
                    GARIS PANDUAN PEMBENTANGAN KEPADA <br>PANEL MESYUARAT JAWATANKUASA KELULUSAN ACARA PERBADANAN PUTRAJAYA (JKA-PPj)
                </div>
    
                <table style="margin-top: 15px; padding-right: 5px">
                    <tbody>
                        <tr class="standard13">
                            <td style="vertical-align: top; text-align: right; padding-right: 20px">1.</td>
                            <td style="line-height: 130%">Berpakaian kemas </td>
                        </tr>
                        <tr class="standard13" style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">2.</td>
                            <td>Penganjur acara yang menggunakan khidmat pengurus acara perlu hadir bersama untuk pembentangan dan tidak melebihi dari 2 orang pembentang. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">3.</td>
                            <td>Bahan pembentangan disarankan dalam Bahasa Melayu. Walau bagaimanapun penggunaan Bahasa Inggeris dibenarkan.
                            </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">4.</td>
                            <td>Slaid pembentangan dalam format "powerpoint presentation" dan tidak melebihi dari 30 slaid untuk setiap pembentangan termasuk "hyperlink" serta penggunaan tambahan video/audio tidak melebihi 2 minit </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">5.</td>
                            <td>Berada di lokasi 20 minit sebelum masa pembentangan. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">6.</td>
                            <td>Mengikut giliran yang telah ditetapkan. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">7.</td>
                            <td>Semua kos yang terlibat untuk pembentangan tidak akan ditanggung oleh PPj. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">8.</td>
                            <td>Dokumen yang tidak lengkap tidak akan dikemukakan ke jawatankuasa. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">9.</td>
                            <td>Tidak menyentuh sensitiviti agama, kaum, politik atau isu-isu sensitif. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">10.</td>
                            <td> PPj berhak menunda/meminda/membatalkan tarikh pembentangan dengan mengemukakan notis kepada pemohon. </td>
                        </tr>
                        <tr class="standard13"style="line-height: 130%">
                            <td style="width: 15%; vertical-align: top; text-align: right; padding-right: 20px">11.</td>
                            <td>Maklumat wajib untuk dibentangkan: </td>
                        </tr>
                    </tbody>
                </table>
                
                <table class="table-bordered" style="width: 100%; margin: 20px 0px">
                    <thead>
                        <tr class="tajuk" style="text-align: center">
                            <th class="buang" style="width: 5%">Maklumat Pemohon</th>
                            <th class="buang">Maklumat Acara</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="standard12">
                            <td class="buang" style="width: 35%; vertical-align: top">
                                <ul>
                                    <li>Nama Pemohon/Syarikat/Agensi</li>
                                    <li>Nama Penganjur</li>
                                    <li>Alamat</li>
                                    <li>No. Telefon (P)</li>
                                    <li>No. Telefon (H/P)</li>
                                    <li>E-mel</li>
                                </ul>
                            </td>
                            <td class="buang">
                                <ul>
                                    <li>Nama Acara</li>
                                    <li>Tarikh/Hari Acara</li>
                                    <li>Masa Acara</li>
                                    <li>Lokasi</li>
                                    <li>Tarikh/Masa Masuk Tapak</li>
                                    <li>Tarikh/Masa Keluar Tapak</li>
                                    <li>Jumlah Jam Penggunaan</li>
                                    <li>Jumlah Peserta</li>
                                    <li>Jumlah Pelawat/Penonton/Pengunjung</li>
                                    <li>Peringkat Penganjuran (Antarabangsa / Kebangsaan /Putrajaya)</li>
                                    <li>Pelan Tapak</li>
                                    <li>Laluan & Jarak (Acara Jalan Kaki/Larian/Berbasikal dll)</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- Page 3 --}}
            
            <div class="page-break"></div>
            {{-- Page 4 --}}
            <div class="page4">
                <div class="marginpaddingzero tajuk" style="text-align: center; width: 100%; font-weight: bold; text-decoration: underline">
                    KADAR BAYARAN SEWA TAPAK DAN DEPOSIT ACARA
                </div>
                
                <table class="table-bordered" style="margin-bottom: 10px; width: 100%">
                    <thead>
                        <tr class="tajuk" style="text-align: center">
                            <th class="buang" style="width: 3%">Bil.</th>
                            <th class="buang" style="width: 10%">Kategori</th>
                            <th class="buang" style="width: 50%">Lokasi</th>
                            <th class="buang" style="width: 30%">Caj Sewaan Sehari (RM)</th>
                            <th class="buang" style="width: 7%">Deposit (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="verticalTop" >
                            <td class="buang text-align-center" rowspan="2">i.</td>
                            <td class="buang standard12" rowspan="2">Platinum</td>
                            <td class="buang standard12"><ul><li>Dataran Putrajaya</li></ul></td>
                            <td class="buang standard12 text-align-center">RM15,000.00</td>
                            <td class="buang standard12 text-align-center" rowspan="2">RM20,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12"><ul><li>Dataran Putra</li><li>Dataran Wawasan</li><li>Persiaran Perdana</li></ul></td>
                            <td class="buang standard12 text-align-center">RM10,000.00</td>
                        </tr>

                        <tr class="verticalTop" >
                            <td class="buang text-align-center" rowspan="4">ii.</td>
                            <td class="buang standard12" rowspan="4">Emas</td>
                            <td class="buang standard12"><ul><li>Dataran Gemilang</li><li>Dataran Rakyat</li></ul></td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                            <td class="buang standard12 text-align-center">RM8,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Kompleks Sukan Air, Presint 6 (Lot PPj)</li></ul></td>
                            <td class="buang standard12 text-align-center">RM3,000.00</td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Tasik Putrajaya</li></ul></td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                            <td class="buang standard12 text-align-center">RM10,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Lot Parkir Istana Kehakiman (Pemilik: Putrajaya Holdings)</li></ul></td>
                            <td class="buang standard12 text-align-center">Kadar Harga Mengikut Pemilik Lot Tanah</td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                        </tr>

                        
                        <tr class="verticalTop" >
                            <td class="buang text-align-center" rowspan="4">iii.</td>
                            <td class="buang standard12" rowspan="4">Perak</td>
                            <td class="buang standard12"><ul><li>Taman Seri Empangan</li></ul></td>
                            <td class="buang standard12 text-align-center" style="width: 20%">RM3,000.00</td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Pentas Dataran Wawasan</li></ul></td>
                            <td class="buang standard12 text-align-center">RM500.00</td>
                            <td class="buang standard12 text-align-center">RM1,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Lain-lain Kawasan (Milik PPj)</li></ul></td>
                            <td class="buang standard12 text-align-center">RM3,000.00</td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                        </tr>
                        <tr class="verticalTop">
                            <td class="buang standard12 "><ul><li>Lain-lain Kawasan Milik Persendirian</li></ul></td>
                            <td class="buang standard12 text-align-center">Kadar Harga Mengikut Pemilik Lot Tanah</td>
                            <td class="buang standard12 text-align-center">RM5,000.00</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table-bordered" style="margin-bottom: 10px; width: 100%">
                    <thead>
                        <tr class="tajuk" style="">
                            <th class="buang" colspan="3">*Caj Pengurusan Acara</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="buang text-align-center" style="width: 4%">iv.</td>
                            <td class="buang standard12" style="width: 61%">Penggunaan Laluan Di Putrajaya (Berbasikal, Jalan Kaki,Larian Dll)
                                <br>- Bawah 50 km
                                <br>- 51 km ke atas
                            </td>
                            <td class="buang standard12 text-align-center" style="width: 37%"><br>RM500.00 (12 Jam)<br>RM1,000.00 (12 Jam)</td>
                        </tr>
                        <tr>
                            <td class="buang text-align-center">v.</td>
                            <td class="buang standard12">Penggunaan Kawasan/Tapak Bukan Milik PPj</td>
                            <td class="buang standard12 text-align-center">RM500 (12 Jam)
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="standard11 pd-0" style="line-height: 0">*Caj tidak termasuk GST</div>
            </div>
            {{-- Page 4--}}
        </main>
    </body>
</html>
@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Dalaman</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Senarai Acara</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-3">
                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3" >
                                                <option value="">Sila Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}" @if($l->id == $data['id']) selected @endif>{{ $l->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
											<div class="input-icon">
												<input type="text" class="form-control form-control-lg font-weight-bold mt-3" value="{{ Helper::date_format($data['startDate']) }}" id="kt_daterangepicker_5" name="tarikhDari" placeholder="Pilih Tarikh" autocomplete="off" required />
												<span>
													<i class="flaticon-calendar-3 icon-xl"></i>
												</span>
											</div>
                                            {{-- <div class='input-group' id="">
                                                <input required type="text" value="{{ Helper::date_format($data['startDate']) }}" class="form-control mt-3" name="tarikhDari" id="tarikhAcaraDari" placeholder="Tarikh Acara Dari" autocomplete="off" />
                                                <div class="input-group-append mt-3">
                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="col-lg-3">
											<div class="input-icon">
												<input type="text" class="form-control form-control-lg font-weight-bold mt-3" value="{{ Helper::date_format($data['endDate']) }}" id="kt_daterangepicker_6" name="tarikhHingga" placeholder="Pilih Tarikh" autocomplete="off" required />
												<span>
													<i class="flaticon-calendar-3 icon-xl"></i>
												</span>
											</div>
                                            {{-- <div class='input-group' id="">
                                                <input required type="text" value="{{ Helper::date_format($data['endDate']) }}" class="form-control mt-3" name="tarikhHingga" id="tarikhAcaraHingga" placeholder="Tarikh Acara Hingga" autocomplete="off" />
                                                <div class="input-group-append mt-3">
                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                </div>
                                            </div> --}}
                                        </div>
                                        {{-- <div class="col-xl-3">
                                            <input type="text" class="form-control mt-3" name="tarikhDari" id="tarikhAcaraDari" placeholder="Tarikh Acara Dari" autocomplete="off" />
                                        </div> --}}
                                        <div class="col-lg-3">
                                            <div class="float-right">
                                                <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-12">
                        @if ($data['post'])
                            <!--begin::Card-->
                            <form action="" method="post" id="form">
                                @csrf
                                <div class="card card-custom gutter-b">
                                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                        <div class="card-toolbar">
                                            <!--begin::Button-->
                                            {{-- <a href="{{ url('/event/internal/internaladd') }}" class="btn btn-primary font-weight-bolder mt-2"> --}}
                                            @if(count($data['event']) == 0)
                                                <a href="{{ url('/event/internal/form', [Crypt::encrypt($data['id']), Crypt::encrypt($data['startDate']), Crypt::encrypt($data['endDate'])]) }}" class="btn btn-primary font-weight-bolder mt-2">
                                                    <i class="flaticon-plus"></i> Tambah Acara
                                                </a>
                                            @endif
                                            <!--end::Button-->
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <!--begin: Datatable-->
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <th> </th>
                                                        <th>Tarikh Permohonan</th>
                                                        <th>No Permohonan</th>
                                                        <th>Pemohon</th>
                                                        <th>Nama Acara</th>
                                                        <th>Lokasi</th>
                                                        <th>Tarikh Acara Dari</th>
                                                        <th>Tarikh Acara Hingga</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @foreach ($data['event'] as $e)
                                                        <tr>
                                                            <td style="width: 4.5%;">
                                                                {{ $i }}
                                                            </td>
                                                            <td>{{ Helper::date_format($e->created_at) }}</td>
                                                            {{-- <td><a href="{{ url('event/internal/reservation', Crypt::encrypt($e->bmb_booking_no)) }}">{{ $e->bmb_booking_no }}</a></td> --}}
                                                            <td><a href="{{ url('event/internal/reservation', Crypt::encrypt($e->bmb_booking_no)) }}">{{ $e->bmb_booking_no }}</a></td>
                                                            <td>{{ Helper::get_nama($e->fk_users) }}<br>@if($e->contact_person != '')({{($e->contact_person)}})@endif</td>
                                                            <td>{{ $e->event_name }}</td>
                                                            <td>{{ Helper::locationspa($e->fk_spa_location) }}</td>
                                                            <td style="width: 9%;">{{ Helper::date_format($e->event_date) }}</td>
                                                            <td style="width: 9%;">{{ Helper::date_format($e->event_date_end) }}</td>
                                                            <td style="width: 5%;">{{ Helper::get_status_tempahan($e->fk_lkp_status) }}</td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--end: Datatable-->
                                    </div>
                                </div>
                            </form>
                            <!--end::Card-->
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('js_content')
    @include('event.internal.js.index')
@endsection




<script>
    function uploadFile(inputId, id) {
        var fileInput = document.getElementById(inputId);
        var file = fileInput.files[0];
        var formData = new FormData();
        formData.append('file', file);

        $.ajax({
            url: '/upload/filePembayaranAcara/' + id,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                // Handle success response
                console.log('File uploaded successfully');
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error('Error uploading file:', error);
            }
        });
    }

    $(document).ready(function() {
        // $('.muatNaikFail').on('click', function () {
        //     console.log("Button clicked!");
        // });
        $('.muatNaikFail').on('click', function () {
            console.log("Button clicked!");
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var bookingId = $('#bookingId').val();
            
            $.ajax({
                url: '/upload/fileAcaraPembayaran/' + response.bookingId,
                type: 'POST',
                processData: false, 
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(response2) {
                    window.location.href = response.redirectUrl;
                },
                error: function(error) {
                    console.log('error: ' + error);
                }
            });
        })

        // Tab Tindakan||Maklumat Bayaran Calculation
        $("[name='sewaTapak_kadar'], [name='sewaTapak_kuantiti'], [name='sewaTapak_diskaun'], [name='sewaTapak_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'sewaTapak_kadar', 'sewaTapak_kuantiti', 'sewaTapak_diskaun', 'sewaTapak_diskaunPer', 'sewaTapak_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        $("[name='sewaTasik_kadar'], [name='sewaTasik_kuantiti'], [name='sewaTasik_diskaun'], [name='sewaTasik_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'sewaTasik_kadar', 'sewaTasik_kuantiti', 'sewaTasik_diskaun', 'sewaTasik_diskaunPer', 'sewaTasik_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        $("[name='laluanBawah50_kadar'], [name='laluanBawah50_kuantiti'], [name='laluanBawah50_diskaun'], [name='laluanBawah50_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'laluanBawah50_kadar', 'laluanBawah50_kuantiti', 'laluanBawah50_diskaun', 'laluanBawah50_diskaunPer', 'laluanBawah50_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        $("[name='laluanAtas50_kadar'], [name='laluanAtas50_kuantiti'], [name='laluanAtas50_diskaun'], [name='laluanAtas50_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'laluanAtas50_kadar', 'laluanAtas50_kuantiti', 'laluanAtas50_diskaun', 'laluanAtas50_diskaunPer', 'laluanAtas50_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        $("[name='bukanMilikPPj_kadar'], [name='bukanMilikPPj_kuantiti'], [name='bukanMilikPPj_diskaun'], [name='bukanMilikPPj_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'bukanMilikPPj_kadar', 'bukanMilikPPj_kuantiti', 'bukanMilikPPj_diskaun', 'laluanAtas50_diskaunPer', 'bukanMilikPPj_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        $("[name='bukanMilikPPj_kadar'], [name='bukanMilikPPj_kuantiti'], [name='bukanMilikPPj_diskaun'], [name='bukanMilikPPj_diskaunPer'], [name='cajDeposit']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'bukanMilikPPj_kadar', 'bukanMilikPPj_kuantiti', 'bukanMilikPPj_diskaun', 'laluanAtas50_diskaunPer', 'bukanMilikPPj_jumlah', 'cajDeposit');
            updateTotalAll();
        });
        function updateCalculations(diskaunType, kadar, kuantiti, diskaun, diskaunPer, jumlahSewa, cajdeposit) {
            var kadarValue = parseFloat($('[name='+ kadar +']').val()) || 0;
            var kuantitiValue = parseInt($('[name='+ kuantiti +']').val()) || 0;
            // var applyDiscount = $("[name='applyDiscount']").is(":checked");
            var diskaunValue = parseFloat($('[name='+ diskaun +']').val()) || 0;
            var diskaunPerValue = parseInt($('[name='+ diskaunPer +']').val()) || 0;
            var cajdeposit = parseInt($('[name='+ cajdeposit +']').val()) || 0;


            var total = kadarValue * kuantitiValue;
            if (diskaunType == diskaun) {
                diskaunPerUpdated = diskaunValue/total
                $('[name='+ diskaunPer +']').val(diskaunPerUpdated);
            } else if (diskaunType == diskaunPer){
                diskaunValueUpdated = total * (diskaunPerValue/100)
                $('[name='+ diskaun +']').val(diskaunValueUpdated.toFixed(2));
            }
            if (diskaunValue > 0 || diskaunPerValue > 0) {
                total -= diskaunValue;
                total -= (diskaunPerValue / 100) * total;
            }

            $('[name='+ jumlahSewa +']').val(total.toFixed(2));
        }
        function updateTotalAll() {
            var tapakTotal = parseFloat($('[name=sewaTapak_jumlah]').val()) || 0;
            var tasikTotal = parseInt($('[name=sewaTasik_jumlah]').val()) || 0;
            var laluanBawah50 = parseFloat($('[name=laluanBawah50_jumlah]').val()) || 0;
            var laluanAtas50 = parseInt($('[name=laluanAtas50_jumlah]').val()) || 0;
            var bukanMilikPpj = parseInt($('[name=bukanMilikPPj_jumlah]').val()) || 0;
            var cajDeposit = parseInt($('[name=cajDeposit]').val()) || 0;
            
            var total = tapakTotal + tasikTotal + laluanBawah50 + laluanAtas50 + bukanMilikPpj + cajDeposit;
            $('[id=jumlahKeseluruhan]').val(total.toFixed(2));
        }
        // Tab Tindakan -> Maklumat Bayaran Calculation

        $('.peringkatPenganjuran').on('change', function() {
            var val = $(this).val()
            if (val == 1) {
                $('#peringkatPenganjuranDiterima').prop('hidden', false)
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', true)
                $('#peringkatPenganjuranDitolak').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').attr('required', true);
                $('#peringkatPenganjuranDiterimaTarikh').attr('required', true);
            } else if (val == 2) {
                $('#peringkatPenganjuranDiterima').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').removeAttr('required');
                $('#kt_daterangepicker_5').removeAttr('required');
                $('#peringkatPenganjuranDiterimaMasa').removeAttr('required');
                $('.masaSemakan').removeAttr('required');
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', false)
                $('#peringkatPenganjuranDitolak').prop('hidden', true)
            } else if (val == 3) {
                $('#peringkatPenganjuranDiterima').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').removeAttr('required');
                $('#kt_daterangepicker_5').removeAttr('required');
                $('#peringkatPenganjuranDiterimaMasa').removeAttr('required');
                $('.masaSemakan').removeAttr('required');
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', true)
                $('#peringkatPenganjuranDitolak').prop('hidden', false)
            }
        });

        $('#statusKeputusanMsyt').on('change', function() {
            var val = $(this).val()
            if (val == 1) {
                $('#pembentanganSemula').prop('hidden', false)
                $('#pembentanganSemulaTarikh').attr('required', true);
                $('#pembentanganSemulaTempat').attr('required', true);
            } else {
                $('#pembentanganSemula').prop('hidden', true)
                $('#pembentanganSemulaTarikh').attr('required', false);
                $('#pembentanganSemulaTempat').attr('required', false);
            }
        })

        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        
        $('#kt_daterangepicker_4').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true
        });
        $('#kt_daterangepicker_5').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true
        });
        $('#kt_daterangepicker_6').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true
        });
        
        // var datePickerDari = $('#kt_datepicker_1').datepicker({
        //     format: 'yyyy-mm-dd',
        //     todayHighlight: true,
        //     autoclose: true
        // });
        var datePickerHingga = $('#kt_datepicker_2').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
        datePickerDari.on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        datePickerHingga.datepicker('setStartDate', minDate);
        });

        checkStatusAndToggle();
        $('#statusPemohon').on('change', function () {
            checkStatusAndToggle();
        });
        function checkStatusAndToggle() {
            var selectedStatus = $('#statusPemohon').val();
            if (selectedStatus == 'penganjurUtama') {
                $('#maklumatPenganjurUtamaNavItem').hide();
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
            }
        }
    });
</script>
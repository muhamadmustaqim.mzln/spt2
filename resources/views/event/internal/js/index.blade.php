<script>
    // date picker
    var initialStartDate = '{{ \Carbon\Carbon::parse($data['startDate'])->format('d-m-Y') }}';

    var minDate = moment().add(3, 'month'); // Calculate the maximum date
    var maxDate = moment().add(6, 'months');

    $('#kt_daterangepicker_5').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: minDate.toDate(),
        maxDate: maxDate.toDate(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });
    
    $('#kt_daterangepicker_6').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: initialStartDate,
        minDate: minDate.toDate(),
        maxDate: maxDate.toDate(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });
    
    $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
        var minDate1 = picker.startDate;
        
        $('#kt_daterangepicker_6').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: minDate1,
            maxDate: maxDate,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });
    $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });


    // $('#kt_daterangepicker_5').daterangepicker({
    //     autoApply: true,
    //     autoclose: true,
    //     // minDate: new Date(),
    // }, function(start, end, label) {
    //     $('#kt_daterangepicker_5 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    // });
    $('#tarikhAcaraDari').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        autoclose: true,
    });
    $('#tarikhAcaraHingga').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        autoclose: true,
    });
</script>
<script>
        $('#kt_select2_4').on('change', function(){
        var location_id = $(this).val();
        if (location_id == ''){
            $('#kt_select2_5').prop('disabled', true);
        }
        else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('sport/ajax') }}",
                type: "POST",
                data: {'location_id' : location_id},
                dataType: 'json',
                success: function(data){
                    $('#kt_select2_5').html(data);
                }
            });
        }
    });
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    var table = $('table').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        },
        // rowGroup: {
        //     dataSrc: 1,
        // },
    } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function() {
        var groupColumn = 4;
        var table = $('#example').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );
    
        // Order by the grouping
        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
    });
</script>
<script>
    $(document).ready(function(){
        $("#form").submit(function( event ) {
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }else{
                $("#form").submit();
            }

        });
    });
    
</script>
@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Fasiliti</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Acara</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Fasiliti</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/event/admin/facility/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i> Tambah Fasiliti
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    {{-- <th>Lokasi</th>
                                    <th>Presint</th> --}}
                                    <th>Nama Fasiliti</th>
                                    {{-- <th>Status</th> --}}
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($data['facility'] as $l)
                                <tr>
                                    <td style="width: 5%;">{{ $i++ }}</td>
                                    <td></td>
                                    {{-- <td>{{ Helper::location($l->fk_lkp_location) }}</td> --}}
                                    <td></td>
                                    {{-- <td>{{ $l->lc_addr_town }}</td> --}}
                                    <td>{{ $l->name }}</td>
                                    {{-- <td>{{ Helper::status($l->lc_status) }}</td> --}}
                                    <td style="width: 15%; text-align: center;">
                                        <a class="btn btn-outline-primary btn-sm m-1" href="{{url('/event/admin/facility/edit',Crypt::encrypt($l->id))}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Kemaskini Fasiliti
                                        </a>
                                        <a class="btn btn-outline-info btn-sm m-1" href="{{url('/event/admin/facility/image',Crypt::encrypt($l->id))}}">
                                            <i class="fas fa-images">
                                            </i>
                                            Gambar Fasiliti
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('/event/admin/facility/delete',Crypt::encrypt($l->id))}}">
                                            <i class="fas fa-trash">
                                            </i>
                                            Padam Fasiliti
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('event.facility.js.lists')
@endsection
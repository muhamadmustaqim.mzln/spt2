@extends('layouts.master')

@section('container')

    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Acara</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Acara</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Lokasi</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/event/admin/facility/update', Crypt::encrypt($data['list']->id)) }}" id="form" method="post" enctype="multipart/form-data">
            {{-- <form action="" id="form" method="post" enctype="multipart/form-data"> --}}
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Lokasi</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Lokasi :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="namaLokasi" placeholder="Nama Lokasi" value="{{ $data['list']->name }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori :</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" id="kt_select2_1" name="kategori">
                                    <option value="" disabled>Sila Pilih</option>
                                    <option value="1" @if ($data['list']->category == 1) selected @endif>Platinum</option>
                                    <option value="2" @if ($data['list']->category == 2) selected @endif>Emas</option>
                                    <option value="3" @if ($data['list']->category == 3) selected @endif>Perak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kapasiti :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="kapasiti" placeholder="Kapasiti" value="{{ $data['list']->capacity }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Deposit (RM):</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="deposit" placeholder="Deposit" value="{{ $data['list']->deposit }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Latitud :</label>
                            <div class="col-lg-4">
                                <input type="number" class="form-control" name="latitude" placeholder="Latitud" value="{{ isset($data['list']->latitude) ? $data['list']->latitude : '' }}"/>
                            </div>
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Longitud :</label>
                            <div class="col-lg-4">
                                <input type="number" class="form-control" name="longitude" placeholder="Longitud" value="{{ isset($data['list']->longitude) ? $data['list']->longitude : '' }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Sorotan :</label>
                            <div class="col-lg-10">
                                <textarea name="remark" class="form-control" rows="7">{{ $data['list']->remark }}</textarea>
                                <span class="form-text text-muted">*Penerangan Fasiliti secara terperinci.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Gambar Muka Hadapan :</label>
                            <div class="col-lg-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="file" accept="image/*" onchange="loadFile(event)"/>
                                    <label class="custom-file-label" for="customFile">Pilih Fail</label>
                                </div>
                                <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                                {{-- <img id="output" src="{{ URL::asset("assets/upload/main/{$data['list']->eft_uuid}/{$data['list']->cover_img}") }}" style="width: 250px; height: 200px;"/> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lawatan Maya :</label>
                            <div class="col-lg-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="file1" accept="image/*" onchange="loadFile1(event)"/>
                                    <label class="custom-file-label" for="customFile">Pilih Fail</label>
                                </div>
                                <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                                {{-- <img id="output1" src="{{ URL::asset("assets/upload/virtual/{$data['list']->eft_uuid}/{$data['list']->virtual_img}") }}" style="width: 250px; height: 200px;"/> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" id="kt_select2_3" name="status">
                                    <option value="">Sila Pilih Status</option>
                                    <option value="1" {{($data['list']->status == 1) ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{($data['list']->status == 0) ? 'selected' : ''}}>Tidak Aktif</option>
                                    <option value="2" {{($data['list']->status == 2) ? 'selected' : ''}}>Tutup Sementara</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('event/admin/location') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('event.facility.js.form')
@endsection
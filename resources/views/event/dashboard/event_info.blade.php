@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan {{ $data['title'] ?? '' }}</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Senarai Acara</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <form action="" method="post" id="form">
                            @csrf
                            <div class="card card-custom gutter-b">
                                <div class="card-header flex-wrap border-0 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Senarai Tempahan</h3>
                                    </div>
                                </div>                                
                                <div class="card-body pt-0">
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th> </th>
                                                    <th>No Permohonan</th>
                                                    <th>Tarikh Permohonan</th>
                                                    <th>Pemohon</th>
                                                    <th>Nama Acara</th>
                                                    <th>Lokasi</th>
													<th>Tarikh Acara</th>
													<th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach ($data['event'] as $e)
                                                    @if ($e->fk_lkp_status != null)
                                                        <tr>
                                                            <td class="text-right" style="width: 4.5%;">{{ $i }}</td>
                                                            <td><a href="{{ url('event/dashboard/reservation', Crypt::encrypt($e->bmb_booking_no)) }}">{{ $e->bmb_booking_no }}</a></td>
                                                            <td class="text-center">{{ \Carbon\Carbon::parse($e->event_date)->format('d-m-Y') }}</td>
                                                            <td>{{ Helper::get_nama($e->fk_users) }}</td>
                                                            <td>{{ $e->event_name }}</td>
                                                            <td>@if($e->fk_spa_location){{ Helper::locationspa($e->fk_spa_location) }}@endif</td>
                                                            {{-- <td>{{ ($e->event_date) }} {{ ($e->event_date_end) }}</td> --}}
                                                            <td style="width: 9%">{{ Helper::date_format($e->event_date) }} {{ Helper::date_format($e->event_date_end) }}</td>
                                                            <td>@if($e->fk_lkp_status != null){{ Helper::get_status_tempahan($e->fk_lkp_status) }}@endif</td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        </form>
                        <!--end::Card-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('js_content')
    @include('event.dashboard.js.event_info')
@endsection



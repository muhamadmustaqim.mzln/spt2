@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Saya</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPA</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Saya</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                            <!--begin::List Widget 21-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Body-->
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Tempahan Saya</h3>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                        <thead> 
                                            <tr>
                                                <th>Bil</th>
                                                <th>No. Tempahan</th>
                                                <th>Nama Pemohon</th>
                                                <th>No. Telefon</th>
                                                <th>Lokasi</th>
                                                <th>Status</th>
                                                <th>Tarikh Tempahan</th>
                                                <th>Operasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;  
                                            @endphp
                                            @foreach ($data['laporan_user'] as $s)
                                                <tr>
                                                    <td style="width: 5%;">{{ $i++ }}</td>
                                                    <td><a href="{{ url('event/tempahan', Crypt::encrypt($s->id)) }}" class="">{{ $s->bmb_booking_no }}</a></td>
                                                    <td>{{ $s->name }}</td>
                                                    <td>{{ $s->hp_no }}</td>
                                                    <td>@if ($s->fk_spa_location != null) {{ Helper::locationspa($s->fk_spa_location) }} @else {{ ($s->fk_spa_location) }} @endif</td>
                                                    <td>{{ Helper::get_status_tempahan($s->fk_lkp_status) }}</td>
                                                    <td>{{ $s->bmb_booking_date }}</td>
                                                    <td>
                                                        <a class="btn btn-outline-primary btn-sm m-1" href="{{url('event/tempahan',Crypt::encrypt($s->bmb_booking_no))}}">
                                                            <i class="fas fa-pencil-alt">
                                                            </i>
                                                            Kemaskini
                                                        </a>
                                                        <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('event/tempahan/delete',Crypt::encrypt($s->bmb_booking_no))}}">
                                                            <i class="fas fa-trash">
                                                            </i>
                                                            Batal
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::List Widget 21-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.index')
@endsection
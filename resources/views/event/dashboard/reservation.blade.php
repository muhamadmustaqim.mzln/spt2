@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }

    .font-size-custom14 {
        font-size:14px;
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                  				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                {{-- <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p> --}}
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered font-size-custom14">
                                        <tr>
                                            <th width="30%" class="font-size-md text-light" style="background-color: #242a4c; ">Nama Pemohon</th>
                                            <td>{{ $data['user']->fullname }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                            <td>{{ $data['main_booking']->bmb_booking_no }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                            <td>{{ date("d-m-Y", strtotime($data['main_booking']->bmb_booking_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                            <td>
                                                {{ $data['user_detail']->bud_phone_no }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <table class="table table-bordered font-size-custom14">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                            <td>{{ $data['user']->email }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                            <td>{{ $data['main_booking']->bmb_subtotal }}</td>
                                        </tr>
                                        <tr>
                                            {{-- <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                            <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td> --}}
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                            <td>
                                                @if($data['main_booking']->bmb_deposit_rm == null)
                                                    0.00
                                                @else 
                                                    {{ ($data['main_booking']->bmb_deposit_rm) }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                            <td>{{ Helper::get_status_tempahan($data['main_booking']->fk_lkp_status) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-title font-weight-bolder">Semakan Permohonan Acara</h4>
                                </div>
                            </div>
                            <!-- Main Navigation Bar::Start -->
                            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                                <li class="nav-item position-relative" style="width: 33%">
                                    <a class="nav-link pr-1" id="maklumatPermohonanAcara-tab" data-toggle="tab" href="#maklumatPermohonanAcara">
                                        <span class="nav-text">Maklumat Permohonan Acara</span>
                                        <i hidden id="exclamation-maklumatPermohonanAcara" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                    </a>
                                </li>
                                <li class="nav-item position-relative" style="width: 33%">
                                    <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [19, 22, 23, 28, 30])) active @endif pr-1" id="tindakan-tab" data-toggle="tab" aria-controls="#tindakan" href="#tindakan">
                                        <span class="nav-text">Tindakan</span>
                                        <i hidden id="exclamation-tindakan" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                    </a>
                                </li>
                                <li class="nav-item position-relative" style="width: 33%"> {{--  [24, 25, 26, 27, 20, 21, 29, 28] --}}
                                    <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [20, 21, 29, 31, 31, 24, 25, 26, 27, 5])) active @else disabled @endif pr-1" id="bayaran-tab" data-toggle="tab" aria-controls="#bayaran" href="#bayaran">
                                        <span class="nav-text">Bayaran</span>
                                        <i hidden id="exclamation-bayaran" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content mt-5" id="myTabContent">
                                <input type="text" id="applicant_type" value="{{$data['booking_person']->applicant_type}}" hidden>
                                <!-- Maklumat Permohonan Acara::Start -->
                                <div class="tab-pane fade show" id="maklumatPermohonanAcara" role="tabpanel" aria-labelledby="maklumatPermohonanAcara-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item position-relative" id="butiranPemohonNavItem">
                                            <a class="nav-link active pr-1" id="butiranPemohon-tab" data-toggle="tab" href="#butiranPemohon">
                                                <span class="nav-text">Butiran Pemohon</span>
                                                <i hidden id="exclamation-butiranPemohon" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="butiranAcaraNavItem">
                                            <a class="nav-link pr-1" id="butiranAcara-tab" data-toggle="tab" href="#butiranAcara" aria-controls="butiranAcara">
                                                <span class="nav-text">Butiran Acara</span>
                                                <i hidden id="exclamation-butiranAcara" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2)
                                            <li class="nav-item position-relative" id="maklumatPenganjurUtamaNavItem">
                                                <a class="nav-link pr-1" id="maklumatPenganjurUtama-tab" data-toggle="tab" href="#maklumatPenganjurUtama" aria-controls="maklumatPenganjurUtama">
                                                    <span class="nav-text">Maklumat Penganjur Utama</span>
                                                    <i hidden id="exclamation-maklumatPenganjurUtama" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item position-relative" id="muatNaikDokumenNavItem">
                                            <a class="nav-link pr-1" id="muatNaikDokumen-tab" data-toggle="tab" href="#muatNaikDokumen" aria-controls="muatNaikDokumen">
                                                <span class="nav-text">Muat Naik Dokumen</span>
                                                <i hidden id="exclamation-muatNaikDokumen" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Butiran Pemohon::Start -->
                                        <div class="tab-pane fade show active" id="butiranPemohon" role="tabpanel" aria-labelledby="butiranPemohon-tab">
                                            <form class="myForm" id="form1">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="mb-10 font-weight-bolder text-dark bg-secondary py-3 px-8">Maklumat Am</div>
                                                        <!-- begin:No Tempahan & Status -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Tempahan</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['main_booking']->bmb_booking_no }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['status_tempahan'] }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end:No Tempahan & Status -->
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Pemohon</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Jenis Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jenis Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="jenisPemohon" class="jenisPemohon form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected></option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) selected @endif> Individu</option>
                                                                                <option value="2 "@if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 2) selected @endif> Syarikat/Organisasi</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jenis Pemohon-->
                                                                <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Pemohon (Individu / Syarikat / Organisasi) <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->name ? $data['booking_person']->name : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <!--begin::Nama Pegawai untuk dihubungi-->
                                                                <div @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) hidden @endif class="form-group" id="contactPersonForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Pegawai untuk dihubungi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg contactPerson" name="contactPerson" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->contact_person ? $data['booking_person']->contact_person : '' }}"/>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::Status Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="statusPemohon" class="statusPemohon form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected></option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 1) selected @endif> Penganjur Utama</option>
                                                                                <option value="2" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2) selected @endif> Penganjur Acara</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_status == 1) Penganjur Utama @else Penganjur Acara @endif" /> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Status Pemohon-->
                                                                <!--begin::Jawatan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jawatan <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="jawatan" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->post ? $data['booking_person']->post : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jawatan-->
                                                                <!--begin::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Kad Pengenalan / No Pendaftaran Syarikat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="kadPengenalanPendaftaran" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->ic_no ? $data['booking_person']->ic_no : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <!--begin::Alamat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->address ? $data['booking_person']->address : '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Alamat-->
                                                                <!--begin::Poskod & Bandar-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->postcode ? $data['booking_person']->postcode : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->city ? $data['booking_person']->city : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Poskod & Bandar-->
                                                                <!--begin::Negeri & Negara-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                <div class="col-9">
                                                                                    <select disabled name="negeri" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="" disabled selected></option>
                                                                                        @foreach ($data['negeri'] as $st)
                                                                                            <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->fk_lkp_state == $st->id) selected @endif>{{ $st->ls_description }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->fk_lkp_state ? $data['booking_person']->fk_lkp_state : '' }}" />  --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->fk_lkp_country ? Helper::negara($data['booking_person']->fk_lkp_country) : '' }}" /> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Negeri & Negara-->
                                                                <!--begin::No GST-->
                                                                <div hidden class="form-group" id="nogstForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No. GST</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->GST_no ? $data['booking_person']->GST_no : '' }}" /> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Pejabat <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->office_no ? $data['booking_person']->office_no : '' }}" /> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="noBimbit" placeholder="" value="{{ $data['booking_person'] != null && $data['booking_person']->hp_no ? $data['booking_person']->hp_no : '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <!--begin::No. Faks && Email-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Email <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="email" placeholder="" value="{{ $data['booking_person']->email }}" />
                                                                                    <div hidden class="text-danger" id="invalid-email">* Sila isi Email.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Faks</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="noFaks" placeholder="" value="{{ $data['booking_person']->fax_no ?? '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12 d-flex justify-content-end">
                                                                        <a href="{{ url('/event/cancel/bookingreservation', Crypt::encrypt($data['main_booking']->id)) }}" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Butiran Pemohon::End -->
                                        <!-- Butiran Acara::Start -->
                                        <div class="tab-pane fade show" id="butiranAcara" role="tabpanel" aria-labelledby="butiranAcara-tab">
                                            <form class="myForm" id="form2">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Acara</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Nama Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event'] != null && $data['booking_event']->event_name ? $data['booking_event']->event_name : '' }}" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                    <!--end::Nama Acara-->
                                                                <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->event_date) }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->event_date_end) }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <!--begin::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time_to)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <!--begin::Lokasi Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Lokasi Acara </label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::locationspa($data['booking_event']->fk_spa_location)  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Lokasi Acara-->
                                                                <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jumlah Peserta / Pengunjung <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->visitor_amount  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jumlah Peserta/ Pengunjung-->
                                                                <!--begin::Peringkat Penganjuran-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->event_name  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Peringkat Penganjuran-->
                                                                <!--begin::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Masuk Tapak </label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->enter_date) }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <!--begin::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->exit_date) }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_time)->format('H:i') }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <!--begin::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Melibatkan penggunaan Drone atau Alat Kawalan Jauh (terbang)</label></div>
                                                                        <div class="col-9">
                                                                            <select disabled name="libatPenggunaanDrone" class="form-control form-control-lg font-weight-bold mt-3" id="kt_select2_5">
                                                                                <option value=""> </option>
                                                                                <option value="ya" @if ($data['booking_event'] != null && $data['booking_event']->drone == 1) selected @endif> Ya</option>
                                                                                <option value="tidak" @if ($data['booking_event'] != null && $data['booking_event']->drone == 0) selected @endif> Tidak</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->drone  }}" /> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <!--begin::Nama Tetamu kehormat Utama-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Tetamu kehormat Utama (jika ada)</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->vip_name  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Tetamu kehormat Utama-->
                                                                <!--begin::Maklumat Tambahan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Maklumat Tambahan </label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->remark  }} @endif" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Maklumat Tambahan-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Butiran Acara::End -->
                                        <!-- Butiran Penganjur Utama::Start -->
                                        {{-- @if ($data['booking_event'] != null && $data['booking_person']->applicant_type != 1) --}}
                                        <div class="tab-pane fade show" id="maklumatPenganjurUtama" role="tabpanel" aria-labelledby="maklumatPenganjurUtama-tab">
                                            <form class="myForm" id="form3">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Penganjur Utama</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Agensi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->agency_name  }} @endif" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control form-control-solid form-control-lg" name="alamatPenganjur" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->address }} @endif" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod</label></div>
                                                                                <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="poskod" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->postcode }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar</label></div>
                                                                                <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="bandar" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->city }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="negeri" placeholder="" value="@if ($data['booking_organiser'] != null) {{ Helper::negeri($data['booking_organiser']->fk_lkp_state) }} @endif" />
                                                                                    {{-- <select disabled name="negeri" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                        <option value="" disabled selected></option>
                                                                                        <option value="Johor">Johor</option>
                                                                                        <option value="Kedah">Kedah</option>
                                                                                        <option value="Kelantan">Kelantan</option>
                                                                                        <option value="Melaka">Melaka</option>
                                                                                        <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                                                        <option value="Pahang">Pahang</option>
                                                                                        <option value="Pulau Pinang">Pulau Pinang</option>
                                                                                        <option value="Perak">Perak</option>
                                                                                        <option value="Perlis">Perlis</option>
                                                                                        <option value="Sabah">Sabah</option>
                                                                                        <option value="Sarawak">Sarawak</option>
                                                                                        <option value="Selangor">Selangor</option>
                                                                                        <option value="Terengganu">Terengganu</option>
                                                                                        <option value="Wilayah Persekutuan">Wilayah Persekutuan</option>
                                                                                    </select> --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="negara" placeholder="" value="@if ($data['booking_organiser'] != null) {{ Helper::negara($data['booking_organiser']->fk_lkp_country) }} @endif" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon</label></div>
                                                                                <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="telBimbit" placeholder="" value="@if ($data['booking_organiser'] != null) {{ ($data['booking_organiser']->telephone_no) }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        {{-- @endif --}}
                                        <!-- Butiran Penganjur Utama::End -->
                                        <!-- Muat Naik Dokumen::Start -->
                                        <div class="tab-pane fade show" id="muatNaikDokumen" role="tabpanel" aria-labelledby="muatNaikDokumen-tab">
                                            <form class="myForm" id="form4">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Muat Naik Dokumen</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 3%">Bil.</th>
                                                                                <th style="width: 30%">Senarai Semak</th>
                                                                                <th style="width: 20%">Tindakan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php
                                                                                $foundAttachment1 = false;
                                                                                $foundAttachment2 = false;
                                                                                $foundAttachment3 = false;
                                                                                $foundAttachment4 = false;
                                                                            @endphp
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">1</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div>
                                                                                    </div>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 1)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment1 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment1)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">2</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div>
                                                                                    </div>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 2)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment2 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment2)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">3</th>
                                                                                <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 3)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment3 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach

                                                                                @if (!$foundAttachment3)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">4</th>
                                                                                <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 4)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment4 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment4)
                                                                                    <th></th>
                                                                                @endif
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Muat Naik Dokumen::End -->             
                                    </div>
                                </div>
                                <!-- Maklumat Permohonan Acara::End -->
                                <!-- Tindakan::Start -->
                                <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [19, 22, 23, 28, 30])) active @endif" id="tindakan" role="tabpanel" aria-labelledby="tindakan-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <li class="nav-item position-relative">
                                                <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [19, 28,29])) active @endif pr-1" id="tindakanSemakan-tab" data-toggle="tab" href="#tindakanSemakan">
                                                    <span class="nav-text">Semakan</span>
                                                    <i hidden id="exclamation-tindakanSemakan" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [22, 23, 24, 25, 26, 27])) active @elseif(in_array($data['main_booking']->fk_lkp_status, [19, 29])) disabled @endif pr-1" id="tindakanKeputusanMesyuarat-tab" data-toggle="tab" href="#tindakanKeputusanMesyuarat" aria-controls="tindakanKeputusanMesyuarat">
                                                <span class="nav-text">Keputusan Mesyuarat</span>
                                                <i hidden id="exclamation-tindakanKeputusanMesyuarat" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <li class="nav-item position-relative">
                                                <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [30, 5])) active @elseif(in_array($data['main_booking']->fk_lkp_status, [19, 22, 29])) disabled @endif pr-1" id="tindakanMaklumatBayaran-tab" data-toggle="tab" href="#tindakanMaklumatBayaran" aria-controls="tindakanMaklumatBayaran">
                                                    <span class="nav-text">Maklumat Bayaran</span>
                                                    <i hidden id="exclamation-tindakanMaklumatBayaran" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Tindakan-Semakan::Start -->
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [19, 20, 28, 29])) active @endif" id="tindakanSemakan" role="tabpanel" aria-labelledby="tindakanSemakan-tab">
                                                <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input hidden type="text" name="tab" id="" value="tindakanSemakan">
                                                    <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                        <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Semakan Permohonan</div>
                                                        <div class="form-group">
                                                            <div class="row d-flex justify-content-center">
                                                                <div class="col-2 my-auto"><label>Status Semakan Permohonan</label></div>
                                                                <div class="col-6">
                                                                    <input hidden type="text" class="form-control form-control-lg" name="fk_user" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_person']->id }} @endif"/>
                                                                    {{-- <select @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 17 || $data['main_booking']->fk_lkp_status == 20 || $data['main_booking']->fk_lkp_status == 28) disabled @endif @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 5) disabled @endif name="statusSemakanPermohonan" class="peringkatPenganjuran form-control form-control-lg font-weight-bold mt-3" > --}}
                                                                    <select @if ($data['main_booking']->fk_lkp_status != 19) disabled @endif name="statusSemakanPermohonan" class="peringkatPenganjuran form-control form-control-lg font-weight-bold mt-3" >
                                                                        <option value=""> </option>
                                                                        <option value="1" @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 5) selected @endif> Diterima</option>
                                                                        <option value="2" @if ($data['spa_booking_review'] && $data['main_booking']->fk_lkp_status == 28) selected @endif> Tidak Lengkap</option>
                                                                        <option value="3" @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 17) selected @endif> Gagal/ Ditolak</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                        <div @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 17) hidden @endif id="peringkatPenganjuranDiterima">
                                                            <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Pembentangan</div>
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tempat <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 19) disabled @endif type="text" class="form-control form-control-lg" name="location" id="peringkatPenganjuranDiterimaTempat" autocomplete="off" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->location }} @endif"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tarikh <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9"> 
                                                                        <input @if($data['main_booking']->fk_lkp_status != 19) disabled @endif type="text" class="form-control form-control-lg" id="kt_daterangepicker_5" name="tarikhSemakan"  autocomplete="off" required value="@if ($data['spa_booking_review']) {{ Helper::date_format($data['spa_booking_review']->date) }} @endif" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Nama Pegawai untuk dihubungi-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Masa</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 19) disabled @endif class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaSemakan" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->time }} @else '09:00 AM' @endif" type="text"/>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                            <!--end::Nama Pegawai untuk dihubungi-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Nama Pengerusi Mesyuarat</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 19) disabled @endif type="text" class="form-control form-control-lg" name="namaPengerusiMesyuarat" placeholder="" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->meeting_chairman }} @endif" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Maklumat Tambahan-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3"><label>Maklumat Tambahan</label></div>
                                                                    <div class="col-9">
                                                                        <textarea @if($data['main_booking']->fk_lkp_status != 19) disabled @endif name="maklumatTambahan" class="form-control form-control-lg contactPerson" id="" cols="30" rows="10">@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->remark }} @endif</textarea>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                            <!--end::Maklumat Tambahan-->
                                                        </div>
                                                        <div hidden id="peringkatPenganjuranTidakLengkap">
                                                            <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Perlu Dikemas kini</div>
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3"><label>Maklumat Perlu Dikemas kini</label></div>
                                                                    <div class="col-9">
                                                                        <textarea name="maklumatTambahanTidakLengkap" class="form-control form-control-lg" id="" cols="30" rows="10">{{ $data['spa_booking_review']->remark ?? '' }}</textarea>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review != 17 || $data['spa_booking_review'] == null) hidden @endif id="peringkatPenganjuranDitolak">
                                                            <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Permohonan Gagal/Ditolak <span class="text-danger">*</span></div>
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3"><label>Ulasan <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <textarea @if($data['main_booking']->fk_lkp_status != 19) disabled @endif name="maklumatTambahanDitolak" class="form-control form-control-lg" id="" cols="30" rows="10">{{ $data['spa_booking_review']->remark ?? '' }}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="row py-4">
                                                                    <div class="col-3"></div>
                                                                    <div class="col-9">
                                                                        Notifikasi Pemberitahuan Gagal/Ditolak akan dihantar melalui SMS dan email
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 d-flex justify-content-end">
                                                                @if($data['main_booking']->fk_lkp_status == 19)  
                                                                    <button type="submit" class="btn btn-primary font-weight-bold mb-7">Simpan</button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @endif
                                        <!-- Tindakan-Semakan::End -->
                                        <!-- Tindakan-Keputusan Mesyuarat::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [22, 23, 24, 25, 26, 27])) active @endif" id="tindakanKeputusanMesyuarat" role="tabpanel" aria-labelledby="tindakanKeputusanMesyuarat-tab">
                                            <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                    <input hidden type="text" name="tab" id="" value="tindakanKeputusanMesyuarat">
                                                    <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Mesyuarat JKA PPj</div>
                                                    @if (in_array($data['main_booking']->fk_lkp_status, [22]))
                                                        <!--begin::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                        <div class="row">
                                                            <div class="col-xl-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-4 my-auto"><label>Tarikh Mesyuarat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-8">
                                                                            <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg tarikhAcaraDari" id="kt_daterangepicker_6" autocomplete="off" name="date_meeting" value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-4 my-auto"><label>Bil Mesyuarat</label></div>
                                                                        <div class="col-8">
                                                                            <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg " id="" name="meeting_no" value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                        <!--begin::Catatan-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2"><label>Catatan </label></div>
                                                                <div class="col-10">
                                                                    <textarea @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif name="remark" class="form-control form-control-lg" id="" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Catatan-->
                                                        <!--begin::Status Keputusan Mesyuarat-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2 my-auto"><label>Status Keputusan Mesyuarat</label></div>
                                                                <div class="col-10">
                                                                    <select required @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif name="fk_lkp_status_meeting" id="statusKeputusanMsyt" class="form-control form-control-lg font-weight-bold mt-3" >
                                                                        <option value=""> Sila Pilih</option>
                                                                        <option value="1"> Pembentangan Semula</option>
                                                                        <option value="2"> Gagal/ Ditolak</option>
                                                                        <option value="3"> Lulus (Perlu Maklumat Bayaran)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Status Keputusan Mesyuarat-->
                                                        <!--begin::Minit Mesyuarat-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2 my-auto"><label>Minit Mesyuarat </label></div>
                                                                <div class="col-10">
                                                                    {{-- <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif  name="minitMesyuarat" type="file" class="form-control" id="" accept=".pdf" style="display:none;"> --}}
                                                                    {{-- <label for="" class="btn btn-primary btn-file-upload">Muat Naik</label> --}}
                                                                    <input  name="minitMesyuarat" type="file" class="form-control" id="minitMesyuaratId" accept=".pdf" style="display:none;">
                                                                    <label for="minitMesyuaratId" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                    <div class="font-size-sm font-weight-normal" id="minitMesyuaratDisplay"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Minit Mesyuarat-->
                                                        <div hidden id="pembentanganSemula">
                                                            <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Pembentangan Semula</div>
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tempat <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif  type="text" class="form-control form-control-lg" name="location" id="pembentanganSemulaTempat" autocomplete="off" value=""/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tarikh <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif type="text" class="form-control form-control-lg" id="kt_daterangepicker_4" name="pembentanganSemulaTarikh" autocomplete="off" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Nama Pegawai untuk dihubungi-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Masa</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif class="form-control" id="kt_timepicker_1" readonly="readonly" name="pembentanganSemulaMasa" value="09:00 AM" type="text"/>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                            <!--end::Nama Pegawai untuk dihubungi-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Nama Pengerusi Mesyuarat</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif type="text" class="form-control form-control-lg" name="namaPengerusiMesyuarat" placeholder="" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Maklumat Tambahan-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3"><label>Maklumat Tambahan</label></div>
                                                                    <div class="col-9">
                                                                        <textarea @if($data['main_booking']->fk_lkp_status != 22) disabled @endif name="maklumatTambahan" class="form-control form-control-lg contactPerson" id="" cols="30" rows="10"></textarea>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary font-weight-bold mb-7">Simpan</button>
                                                            </div>
                                                        </div>
                                                    @elseif (in_array($data['main_booking']->fk_lkp_status, [23]))
                                                        <!--begin::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                        <div class="row">
                                                            <div class="col-xl-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-4 my-auto"><label>Tarikh Mesyuarat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-8">
                                                                            <input required @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg tarikhAcaraDari" id="kt_daterangepicker_6" autocomplete="off" name="date_meeting" value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-4 my-auto"><label>Bil Mesyuarat</label></div>
                                                                        <div class="col-8">
                                                                            <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg " id="" name="meeting_no" value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                        <!--begin::Catatan-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2"><label>Catatan </label></div>
                                                                <div class="col-10">
                                                                    <textarea @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif name="remark" class="form-control form-control-lg" id="" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Catatan-->
                                                        <!--begin::Status Keputusan Mesyuarat-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2 my-auto"><label>Status Keputusan Mesyuarat</label></div>
                                                                <div class="col-10">
                                                                    <select required @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif name="fk_lkp_status_meeting" id="statusKeputusanMsyt" class="form-control form-control-lg font-weight-bold mt-3" >
                                                                        <option value=""> Sila Pilih</option>
                                                                        <option value="1"> Pembentangan Semula</option>
                                                                        <option value="2"> Gagal/ Ditolak</option>
                                                                        <option value="3"> Lulus (Perlu Maklumat Bayaran)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Status Keputusan Mesyuarat-->
                                                        <!--begin::Minit Mesyuarat-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-2 my-auto"><label>Minit Mesyuarat </label></div>
                                                                <div class="col-10">
                                                                    {{-- <input @if($data['main_booking']->fk_lkp_status != 22) disabled @endif  name="minitMesyuarat" type="file" class="form-control" id="" accept=".pdf" style="display:none;"> --}}
                                                                    {{-- <label for="" class="btn btn-primary btn-file-upload">Muat Naik</label> --}}
                                                                    {{-- <input  name="minitMesyuarat" type="file" class="form-control" id="minitMesyuaratId2" accept=".pdf" style="display:none;">
                                                                    <label for="minitMesyuaratId2" class="btn btn-primary btn-file-upload">Muat Naik</label> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Minit Mesyuarat-->
                                                        <div hidden id="pembentanganSemula">
                                                            <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Pembentangan Semula</div>
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tempat <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif  type="text" class="form-control form-control-lg" name="location" id="pembentanganSemulaTempat" autocomplete="off" value=""/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Tarikh <span class="text-danger">*</span></label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg" id="kt_daterangepicker_4" name="pembentanganSemulaTarikh" autocomplete="off" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Nama Pegawai untuk dihubungi-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Masa</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif class="form-control" id="kt_timepicker_1" readonly="readonly" name="pembentanganSemulaMasa" value="" type="text"/>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                            <!--end::Nama Pegawai untuk dihubungi-->
                                                            <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-3 my-auto"><label>Nama Pengerusi Mesyuarat</label></div>
                                                                    <div class="col-9">
                                                                        <input @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif type="text" class="form-control form-control-lg" name="namaPengerusiMesyuarat" placeholder="" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                            <!--begin::Maklumat Tambahan-->
                                                            <div class="form-group" id="contactPersonForm">
                                                                <div class="row">
                                                                    <div class="col-3"><label>Maklumat Tambahan</label></div>
                                                                    <div class="col-9">
                                                                        <textarea @if($data['main_booking']->fk_lkp_status != 22 && $data['main_booking']->fk_lkp_status != 23) disabled @endif name="maklumatTambahan" class="form-control form-control-lg contactPerson" id="" cols="30" rows="10"></textarea>
                                                                    </div>
                                                                <div class="col-3"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Bil.</th>
                                                                            <th>Tarikh Mesyuarat</th>
                                                                            <th>Bil. Mesyuarat</th>
                                                                            <th>Catatan</th>
                                                                            <th>Status Keputusan Mesyuarat</th>
                                                                            <th>Minit Mesyuarat</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php
                                                                            $i = 1;
                                                                        @endphp
                                                                        @if ($data['spa_meeting_result'])
                                                                            @foreach ($data['spa_meeting_result'] as $s)
                                                                                <tr class="font-size-lg">
                                                                                    <td>{{ $i }}</td>
                                                                                    <td>{{ $s->date_meeting }}</td>
                                                                                    <td>{{ $s->meeting_no }}</td>
                                                                                    <td>{{ $s->remark }}</td>
                                                                                    <td>{{ Helper::get_status_tempahan($s->fk_lkp_status_meeting) }}</td>
                                                                                    @php
                                                                                        $foundAttachment8 = false;
                                                                                    @endphp
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 8)
                                                                                            <td class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </td>
                                                                                            @php
                                                                                                $foundAttachment8 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment8)
                                                                                        <td></td>
                                                                                    @endif
                                                                                </tr>
                                                                                @php
                                                                                    $i++
                                                                                @endphp
                                                                            @endforeach
                                                                        @else
                                                                            <td colspan="7" class="text-center">Tiada Data</td>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary font-weight-bold mb-7">Simpan</button>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <!--begin::Status Keputusan Mesyuarat-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 d-flex justify-content-end my-auto"><label>Status Keputusan Mesyuarat</label></div>
                                                                <div class="col-9">
                                                                    <select disabled class="form-control form-control-md" >
                                                                        <option value=""> Sila Pilih</option>
                                                                        <option value="1"> Pembentangan Semula</option>
                                                                        <option value="2"> Gagal/ Ditolak</option>
                                                                        <option value="3" @if ($data['main_booking']->fk_lkp_status == 30) selected @endif> Lulus (Perlu Maklumat Bayaran)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Status Keputusan Mesyuarat-->
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Bil.</th>
                                                                            <th style="width: 10%">Tarikh Mesyuarat</th>
                                                                            <th>Bil. Mesyuarat</th>
                                                                            <th>Catatan</th>
                                                                            <th>Status Keputusan Mesyuarat</th>
                                                                            <th>Minit Mesyuarat</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php
                                                                            $i = 1;
                                                                        @endphp
                                                                        @if ($data['spa_meeting_result'])
                                                                            @foreach ($data['spa_meeting_result'] as $s)
                                                                                <tr class="font-size-lg">
                                                                                    <td>{{ $i }}</td>
                                                                                    <td>{{ Helper::date_format($s->date_meeting) }}</td>
                                                                                    <td>{{ $s->meeting_no }}</td>
                                                                                    <td>{{ $s->remark }}</td>
                                                                                    <td>{{ Helper::get_status_tempahan($s->fk_lkp_status_meeting) }}</td>
                                                                                    @php
                                                                                        $foundAttachment8 = false;
                                                                                    @endphp
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 8)
                                                                                            <td class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </td>
                                                                                            @php
                                                                                                $foundAttachment8 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment8)
                                                                                        <td></td>
                                                                                    @endif
                                                                                </tr>
                                                                                @php
                                                                                    $i++
                                                                                @endphp
                                                                            @endforeach
                                                                        @else
                                                                            <td colspan="7" class="text-center">Tiada Data</td>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Tindakan-Keputusan Mesyuarat::End -->
                                        <!-- Tindakan-Maklumat Bayaran::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [30, 5]) ) active @endif" id="tindakanMaklumatBayaran" role="tabpanel" aria-labelledby="tindakanMaklumatBayaran-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                {{-- <form action="{{ url('event/internal/reservation', ['id' => Crypt::encrypt($data['bookingId']), 'tab' => 'tindakanMaklumatBayaran']) }}" method="post" enctype="multipart/form-data"> --}}
                                                <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input hidden type="text" name="tab" id="" value="tindakanMaklumatBayaran">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" id="kt_datatable_2">
                                                            <thead>
                                                                <tr>
                                                                    <th>Bil.</th>
                                                                    <th style="width: 30%">Butiran</th>
                                                                    <th>Kadar</th>
                                                                    <th>Kuantiti</th>
                                                                    <th>Dikecualikan</th>
                                                                    <th>Diskaun RM</th>
                                                                    <th>Diskaun %</th>
                                                                    <th>GST RM</th>
                                                                    <th>Jumlah (RM)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="font-size-sm">
                                                                <tr>
                                                                    <td>1. </td>
                                                                    <td>Sewa Tapak</td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTapak_kadar" value="0.00" step="0.01" min="0.00" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTapak_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="checkbox" class="" name="" id=""></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTapak_diskaun" value="0.00" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTapak_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="sewaTapak_GST" value="0.00" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="sewaTapak_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2. </td>
                                                                    <td>Sewa Tasik</td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTasik_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTasik_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="checkbox" class="" name="" id=""></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTasik_diskaun" value="0.00" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="sewaTasik_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="sewaTasik_GST" value="0.00" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="sewaTasik_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3. </td>
                                                                    <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) bawah 50km </td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanBawah50_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanBawah50_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="checkbox" class="" name="" id=""></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanBawah50_diskaun" value="0.00" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanBawah50_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="laluanBawah50_GST" value="0.00" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="laluanBawah50_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4. </td>
                                                                    <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) atas 51km</td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanAtas50_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanAtas50_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="checkbox" class="" name="" id=""></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanAtas50_diskaun" value="0.00" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="laluanAtas50_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="laluanAtas50_GST" value="0.00" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="laluanAtas50_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5. </td>
                                                                    <td>Penggunaan Kawasan/ Tapak Bukan milik PPj</td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="bukanMilikPPj_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="bukanMilikPPj_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="checkbox" class="" name="" id=""></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="bukanMilikPPj_diskaun" value="0.00" /></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="number" class="form-control form-control-sm" name="bukanMilikPPj_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="bukanMilikPPj_GST" value="0.00" /></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="bukanMilikPPj_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>6. </td>
                                                                    <td>Jumlah Amaun Tempahan</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapakKadar" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>7. </td>
                                                                    <td>GST 0%</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="jumlahGst" value="0.00" /></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>8. </td>
                                                                    <td>Caj Deposit</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input @if ($data['main_booking']->fk_lkp_status != 30) readonly @endif type="number" class="form-control form-control-sm" id="cajDepositID" name="cajDeposit" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>9. </td>
                                                                    <td>Pelarasan / Pengenapan Sen</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="jumlahPelarasanSen" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>10. </td>
                                                                    <td>Jumlah Keseluruhan</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input readonly type="number" class="form-control form-control-solid form-control-sm" name="jumlahKeseluruhan" id="jumlahKeseluruhan" value="0.00" /></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="row pt-5">
                                                        <div class="col-4 my-auto"><label>Jumlah 50% dari Jumlah Amaun Tempahan dalam perkataan <span class="text-danger">*</span></label></div>
                                                        <div class="col-8">
                                                            <input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="text" class="form-control form-control-sm" id="" name="jumlahDeposit" value="" />
                                                        </div>
                                                    </div>
                                                    <div class="row py-3">
                                                        <div class="col-4"><label>Kemaskini Ayat No.5 Surat Kelulusan</label></div>
                                                        <div class="col-8">
                                                            <textarea @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif name="ayatSuratKelulusan" class="form-control form-control-lg" id="" cols="30" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 d-flex justify-content-end">
                                                            {{-- <button type="button" class="btn btn-success font-weight-bold mb-7">Simpan & Preview</button> --}}
                                                            <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2">Hantar</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- Tindakan-Maklumat Bayaran::End -->
                                    </div>
                                </div>
                                <!-- Tindakan::End -->
                                <!-- Bayaran::Start --> {{-- [24, 20, 21, 29, 28] --}}
                                <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [20, 21, 29, 31, 31, 24, 25, 26, 27, 5])) active @endif" id="bayaran" role="tabpanel" aria-labelledby="bayaran-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item position-relative">
                                            <a class="nav-link pr-1" id="bayaranMaklumatBayaran-tab" data-toggle="tab" href="#bayaranMaklumatBayaran">
                                                <span class="nav-text">Maklumat Bayaran</span>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [24, 25])) active @endif pr-1" id="bayaranPembayaranPendahuluan-tab" data-toggle="tab" href="#bayaranPembayaranPendahuluan" aria-controls="bayaranPembayaranPendahuluan">
                                                <span class="nav-text">Pembayaran Pendahuluan</span>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [20, 21, 26, 27, 28, 29, 5])) active @endif pr-1" id="bayaranPembayaranPenuh-tab" data-toggle="tab" href="#bayaranPembayaranPenuh" aria-controls="bayaranPembayaranPenuh">
                                                <span class="nav-text">Pembayaran Penuh</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Bayaran-Maklumat Bayaran::Start -->
                                        <div class="tab-pane fade" id="bayaranMaklumatBayaran" role="tabpanel" aria-labelledby="bayaranMaklumatBayaran-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th>Bil.</th>
                                                                <th style="width: 30%">Butiran</th>
                                                                <th>Kadar</th>
                                                                <th>Kuantiti</th>
                                                                <th>Dikecualikan</th>
                                                                <th>Diskaun RM</th>
                                                                <th>Diskaun %</th>
                                                                <th>GST RM</th>
                                                                <th>Jumlah (RM)</th>
                                                            </tr>
                                                        </thead>
                                                        @php
                                                            $item10_total = $data['quotation_detail_item10'] ? $data['quotation_detail_item10']->total : 0.00;                                                            
                                                            $item22_total = $data['quotation_detail_item22'] ? $data['quotation_detail_item22']->total : 0.00;                                                            
                                                            $item11_total = $data['quotation_detail_item11'] ? $data['quotation_detail_item11']->total : 0.00;                                                            
                                                            $item12_total = $data['quotation_detail_item12'] ? $data['quotation_detail_item12']->total : 0.00;                                                            
                                                            $item13_total = $data['quotation_detail_item13'] ? $data['quotation_detail_item13']->total : 0.00;   
                                                            $sum = $item10_total + $item22_total + $item11_total + $item12_total + $item13_total;
                                                        @endphp
                                                        <tbody class="font-size-sm">
                                                            <tr>
                                                                <td>1. </td>
                                                                <td>Sewa Tapak</td>
                                                                <td><input @if ($data['main_booking']->fk_lkp_status != 30) disabled @endif type="text" class="form-control form-control-sm text-right" name="sewaTapak_kadar" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_kuantiti" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ ($data['quotation_detail_item10']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_diskaun" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_diskaunPer" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ ($data['quotation_detail_item10']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_GST" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_jumlah" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2. </td>
                                                                <td>Sewa Tasik</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_kadar" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_kuantiti" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ ($data['quotation_detail_item22']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_diskaun" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_diskaunPer" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ ($data['quotation_detail_item22']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_GST" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_jumlah" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3. </td>
                                                                <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) bawah 50km</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_kadar" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_kuantiti" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ ($data['quotation_detail_item11']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_diskaun" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_diskaunPer" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ ($data['quotation_detail_item11']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_GST" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_jumlah" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>4. </td>
                                                                <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) atas 51km</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_kadar" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_kuantiti" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ ($data['quotation_detail_item12']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_diskaun" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_diskaunPer" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ ($data['quotation_detail_item12']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_GST" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_jumlah" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>5. </td>
                                                                <td>Penggunaan Kawasan/ Tapak Bukan milik PPj</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_kadar" value="@if ($data['quotation_detail_item13']&& $data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_kuantiti" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ ($data['quotation_detail_item13']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_diskaun" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_diskaunPer" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ ($data['quotation_detail_item13']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_GST" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_jumlah" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>6. </td>
                                                                <td>Jumlah Amaun Tempahan</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapakKadar" value="{{ Helper::moneyhelper($sum) }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>7. </td>
                                                                <td>GST 0%</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="number" class="form-control form-control-sm text-right" name="jumlahGst" value="0.00" /></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>8. </td>
                                                                <td>Caj Deposit</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="number" class="form-control form-control-sm text-right" name="cajDeposit" value="{{ Helper::moneyhelper($data['main_booking']->bmb_deposit_rm) }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>9. </td>
                                                                <td>Pelarasan / Pengenapan Sen</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="number" class="form-control form-control-sm text-right" name="jumlahPelarasanSen" value="0.00" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>10. </td>
                                                                <td>Jumlah Keseluruhan</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" id="" value="{{ Helper::moneyhelper($data['main_booking']->bmb_subtotal + $data['main_booking']->bmb_deposit_rm) }}" />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row pt-5">
                                                    <div class="col-4 my-auto"><label>Jumlah 50% dari Jumlah Amaun Tempahan dalam perkataan <span class="text-danger">*</span></label></div>
                                                    <div class="col-8">
                                                        <input disabled type="text" class="form-control form-control-sm" id="" name="" value="{{ $data['main_booking']->bmb_total_word }}" />
                                                    </div>
                                                </div>
                                                <div class="row py-3">
                                                    <div class="col-4"><label>Kemaskini Ayat No.5 Surat Kelulusan</label></div>
                                                    <div class="col-8">
                                                        <textarea disabled name="ayatSuratKelulusan" class="form-control form-control-lg" id="" cols="30" rows="5">@if ($data['booking_event'] != null) {{ $data['booking_event']->letter_no_five }} @endif</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Bayaran-Maklumat Bayaran::End -->
                                        <!-- Bayaran-Pembayaran Pendahuluan::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [24, 25])) active @endif" id="bayaranPembayaranPendahuluan" role="tabpanel" aria-labelledby="bayaranPembayaranPendahuluan-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <div class="card card-custom">
                                                    <div class="card-body">
                                                        <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            <input hidden type="text" name="tab" id="" value="bayaranPendahuluan">
                                                            <div class="card p-5 mb-10">
                                                                <div class="row pb-3">
                                                                    <div class="col-12">
                                                                        <p class="font-weight-bolder">Pengesahan Kelulusan (Perlu Deposit)</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        Permohonan telah diluluskan oleh JKA PPj, Bayaran wang pendahuluan sebanyak 50% dari keseluruhan sewa tapak perlu dijelaskan 7 hari setelah surat kelulusan acara dikeluarkan.
                                                                    </div>
                                                                </div>
                                                                <div class="row pb-7 d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        Notifikasi Pemberitahuan Gagal/Ditolak akan dihantar melalui SMS dan email.
                                                                    </div>
                                                                </div>
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        <a href="{{url('event/janasurat', ['id' => $data['bookingId']])}}" id="" target="_blank" class="btn btn-success font-weight-bold mb-7">Muat Turun Surat Kelulusan, Surat Akuan Terima dan Aku Janji, Borang BPSK-1/2015</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card p-5 mb-10">
                                                                <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered" id="kt_datatable_2">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Senarai Semak</th>
                                                                                    @if (Session::get('user.isAdmin') == 1)
                                                                                        <th style="width: 20%">Muat Naik</th>
                                                                                    @endif
                                                                                    <th>Fail</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @php
                                                                                    $foundAttachment5 = false;
                                                                                    $foundAttachment6 = false;
                                                                                    $foundAttachment7 = false;
                                                                                    $foundAttachment8 = false;
                                                                                @endphp
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Akuan Terima dan Aku Janji Penganjuran Acara di Putrajaya</th>
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 14)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment5 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment5)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Resit Pembayaran Pendahuluan</th>
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 19)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment6 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment6)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">Surat Jaminan (rujuk surat kelulusan acara jika berkaitan)</th>
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 18)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment7 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment7)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">Lain-lain</th>
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 21)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment8 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment8)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-10" data-wizard-type="step-content" data-wizard-state="current">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No.</th>
                                                                                <th>Jenis Bayaran</th>
                                                                                <th>No Transaksi</th>
                                                                                <th>Tarikh Transaksi</th>
                                                                                <th>Jumlah bayaran (RM)</th>
                                                                                <th>Tindakan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php
                                                                                $j = 1;
                                                                            @endphp
                                                                            @if (count($data['spa_payment']) > 0)
                                                                                @foreach($data['spa_payment'] as $p)
                                                                                    <tr>
                                                                                        <td class="font-size-sm" style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                                                        <td class="font-size-sm">BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                                                        <td class="font-size-sm">{{ $p->application_no ?? '' }}</td>
                                                                                        {{-- <td>{{ $p->fpx_trans_id ?? '' }}</td> --}}
                                                                                        {{-- <td>{{ $data['main_booking']->sap_no ?? "" }}</td> --}}
                                                                                        <td class="font-size-sm">{{ Helper::datetime_format($p->created_at ?? '') }}</td>
                                                                                        {{-- <td>{{ Helper::datetime_format($p->fpx_date ?? '') }}</td> --}}
                                                                                        <td class="font-size-sm text-right">{{ $p->paid_amount ?? '' }}</td>
                                                                                        {{-- <td>{{ $p->amount_paid }}</td> --}}
                                                                                        <td class="font-size-sm" style="width: 15%;">
                                                                                            <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['main_booking']->id), 'type' => $p->fk_lkp_payment_type]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @else
                                                                                <tr><td colspan="6" class="text-center">Tiada Data</td></tr>
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Jumlah Amaun Tempahan (RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'], 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Bayaran Deposit(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['deposit'], 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Baki Bayaran 50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div class="row">
                                                            <div class="col-12 d-flex justify-content-center">
                                                                @if (in_array($data['main_booking']->fk_lkp_status, [24])) 
                                                                    <form action="{{ url('event/bayaran', Crypt::encrypt($data['id'])) }}" method="post">
                                                                        @csrf
                                                                        <a href="{{ url('/event/cancel/bookingreservation', Crypt::encrypt($data['main_booking']->id)) }}" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</a>
                                                                        <button type="button" id="kemaskiniStatusBayaranPend" class="btn btn-primary font-weight-bold mx-1" data-toggle="modal" data-target="#staticBackdrop">Kemaskini Status Bayaran</button>
                                                                        <input type="hidden" name="total" value="{{ $data['total_v'] }}">
                                                                        <a href="{{ url('event/dashboard') }}" class="btn btn-primary font-weight-bold mx-1">Kembali</a>
                                                                    </form>
                                                                @elseif ($data['main_booking']->fk_lkp_status == 25)
                                                                    <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <input hidden type="text" name="tab" id="" value="sah">
                                                                        <a href="{{ url('/event/cancel/bookingreservation', Crypt::encrypt($data['main_booking']->id)) }}" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</a>
                                                                        {{-- <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</button> --}}
                                                                        <button type="submit" class="btn btn-primary font-weight-bold mx-1">Sah</button>
                                                                        <a href="{{ url('event/dashboard') }}" class="btn btn-primary font-weight-bold mx-1">Kembali</a>
                                                                    </form>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Bayaran-Pembayaran Pendahuluan::End -->
                                        <!-- Bayaran-Pembayaran Penuh::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [20, 21, 26, 27, 28, 29, 5])) active @endif" id="bayaranPembayaranPenuh" role="tabpanel" aria-labelledby="bayaranPembayaranPenuh-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <div class="card card-custom">
                                                    <div class="card-body">
                                                        <form action="{{ url('event/dashboard/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            <input hidden type="text" name="tab" id="" value="bayaranPenuh">
                                                            <div class="card p-5 mb-10">
                                                                <div class="row pb-3">
                                                                    <div class="col-12">
                                                                        <div class="py-2 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                                    </div>
                                                                </div>
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Perkara</th>
                                                                            <th style="width: 40%">Tindakan</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <th class="font-size-sm font-weight-normal">Resit Pembayaran Penuh</th>
                                                                            <th class="font-size-sm font-weight-normal">
                                                                                {{-- <input type="text" class="form-control form-control-solid form-control-sm" name="namaPemohonPerakuan" placeholder="" value="" /> --}}
                                                                            </th>
                                                                        </tr>
                                                                        {{-- <tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">Resit Deposit</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                </th>
                                                                            </tr>
                                                                        </tr> --}}
                                                                    </tbody>
                                                                </table>
                                                                
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No.</th>
                                                                            <th>Jenis Bayaran</th>
                                                                            <th>No Transaksi</th>
                                                                            <th>Tarikh Transaksi</th>
                                                                            <th>Jumlah Bayaran (RM)</th>
                                                                            <th>Tindakan</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php
                                                                            $j = 1;
                                                                        @endphp
                                                                        @if (count($data['spa_payment']) > 0)
                                                                            @foreach($data['spa_payment'] as $p)
                                                                                <tr>
                                                                                    <td class="font-size-sm" style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                                                    <td class="font-size-sm">BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                                                    <td class="font-size-sm">{{ $p->application_no ?? '' }}</td>
                                                                                    {{-- <td>{{ $p->fpx_trans_id ?? '' }}</td> --}}
                                                                                    {{-- <td>{{ $data['main_booking']->sap_no ?? "" }}</td> --}}
                                                                                    <td class="font-size-sm">{{ Helper::datetime_format($p->created_at ?? '') }}</td>
                                                                                    {{-- <td>{{ Helper::datetime_format($p->fpx_date ?? '') }}</td> --}}
                                                                                    <td class="font-size-sm text-right">{{ $p->paid_amount ?? '' }}</td>
                                                                                    {{-- <td>{{ $p->amount_paid }}</td> --}}
                                                                                    <td class="font-size-sm" style="width: 15%;">
                                                                                        <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['main_booking']->id), 'type' => $p->fk_lkp_payment_type]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr><td colspan="6" class="text-center">Tiada Data</td></tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                                
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Jumlah Amaun Tempahan (RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'], 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Bayaran Deposit(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['deposit'], 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Baki Bayaran 50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="row">
                                                                <div class="col-12 d-flex justify-content-center">
                                                                    @if (in_array($data['main_booking']->fk_lkp_status, [26]))
                                                                        <form action="{{ url('event/bayaran', Crypt::encrypt($data['id'])) }}" method="post">
                                                                            @csrf
                                                                            <a href="{{ url('/event/cancel/bookingreservation', Crypt::encrypt($data['main_booking']->id)) }}" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</a>
                                                                            {{-- <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</button> --}}
                                                                            {{-- <button type="button" id="kemaskiniStatusBayaranPend" class="btn btn-primary font-weight-bold mx-1" data-toggle="modal" data-target="#staticBackdrop">Kemaskini Status Bayaran</button> --}}
                                                                            <button type="button" id="kemaskiniStatusBayaranPenuh" name="kemaskiniStatusBayaranPenuh" class="btn btn-primary font-weight-bold mx-1" data-toggle="modal" data-target="#staticBackdrop">Kemaskini Status Bayaran</button>
                                                                            <input type="hidden" name="total" value="{{ $data['total'] / 2 }}">
                                                                            <a href="{{ url('event/dashboard') }}" class="btn btn-primary font-weight-bold mx-1">Kembali</a>
                                                                        </form>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Bayaran-Pembayaran Penuh::End -->
                                        <!-- Modal-->
                                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <form action="{{ url('/event/dashboard/updatePayment', Crypt::encrypt($data['main_booking']->id)) }}" id="myForm" method="post">
                                                    @csrf
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Kemas kini Status Bayaran</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <i aria-hidden="true" class="ki ki-close"></i>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body"><div class="card-body pt-5">
                                                            @if(in_array($data['main_booking']->fk_lkp_status, [13]))
                                                                <div class="form-group row">
                                                                    <label class="col-lg-4 col-form-label font-weight-bold pt-0">Pengesahan</label>
                                                                    <div class="col-lg-8">
                                                                        <select name="pengesahan"  class="form-control" required>
                                                                            <option value="" selected disabled>Sila Pilih </option>
                                                                            <option value="2">Lulus</option>
                                                                            <option value="29">Tolak</option>
                                                                            <option value="10">Batal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Jenis Bayaran</label>
                                                                <div class="col-lg-8">
                                                                    <select name="jenis_bayaran" id="jenis_bayaran_penuh" class="form-control jenis_bayaran" required>
                                                                        <option value="" disabled selected>Sila pilih</option>
                                                                        @if ($data['main_booking']->fk_lkp_status == 24)
                                                                            <option value="1" >DEPOSIT & PENDAHULUAN</option>
                                                                            {{-- <option value="2" >PENUH</option> --}}
                                                                        @elseif ($data['main_booking']->fk_lkp_status == 26)
                                                                            <option value="2" >PENUH</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Jumlah Tempahan(RM)</label>
                                                                <div class="col-lg-8">
                                                                    <input disabled type="text" class="form-control " id="jumlahTempahanId" name="jumlahTempahan" placeholder="Jumlah Tempahan(RM)" value="{{ Helper::moneyhelper($data['total']) }}" />
                                                                    <input hidden type="text" class="form-control " id="jumlahTempahanId" name="jumlahTempahan" placeholder="Jumlah Tempahan(RM)" value="{{ ($data['total']) }}" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Pendahuluan (RM)</label>
                                                                <div class="col-lg-8">
                                                                    <input disabled ="text" class="form-control" name="jumlahPendahuluan" id="pendahuluan" placeholder="Jumlah Pendahuluan Tempahan(RM)" value="{{ Helper::moneyhelper($data['downpymt']) }}"/>
                                                                    <input hidden ="text" class="form-control" name="jumlahPendahuluan" id="pendahuluan" placeholder="Jumlah Pendahuluan Tempahan(RM)" value="{{ ($data['downpymt']) }}"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Deposit(RM)</label>
                                                                <div class="col-lg-8">
                                                                    <input disabled type="text" class="form-control" name="deposit" placeholder="Deposit(RM)" value="{{ Helper::moneyhelper($data['main_booking']->bmb_deposit_rm) }}"/>
                                                                    <input hidden type="text" class="form-control" name="deposit" placeholder="Deposit(RM)" value="{{ ($data['main_booking']->bmb_deposit_rm) }}"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Cara Bayaran</label>
                                                                <div class="col-lg-8">
                                                                    <select required name="payment_mode" id="carabayaran" class="form-control" required>
                                                                        <option value="" selected disabled>Sila Pilih</option>
                                                                        @foreach ($data['payment_mode'] as $pm)
                                                                            <option value="{{ $pm->id }}" >{{ $pm->lpm_description }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div id="nocekDiv" class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold">No Cek</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text" class="form-control" name="nocek" placeholder="" value=""/>
                                                                </div>
                                                            </div>
                                                            <div id="namabankDiv" class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold">Nama Bank</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text" class="form-control" name="namabank" placeholder="" value=""/>
                                                                </div>
                                                            </div>
                                                            <div id="lopo" class="form-group row">
                                                                <label class="col-lg-4 col-form-label font-weight-bold">Nombor LO/PO</label>
                                                                <div class="col-lg-8">
                                                                    <input type="text" class="form-control" name="lopo" placeholder="" value=""/>
                                                                </div>
                                                            </div>
                                                            {{-- <div id="lopo" class="form-group row">
                                                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Nombor LO/PO</label>
                                                                <div class="col-lg-8">
                                                                    <input required type="text" class="form-control" name="lopo" placeholder="" value=""/>
                                                                </div>
                                                            </div> --}}
                                                            <div class="form-group row" id="norujukanbayaran">
                                                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">No Rujukan Bayaran</label>
                                                                <div class="col-lg-8">
                                                                    <input required type="text" class="form-control" name="rujukanBayaran" placeholder="No Rujukan Bayaran" value=""/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Bayaran (RM)</label>
                                                                <div class="col-lg-8">
                                                                    <input disabled type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value="{{ Helper::moneyhelper($data['total_v']) }}"/>
                                                                    <input hidden type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value="{{ $data['total_v'] }}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input hidden type="text" id="mbid" value="">
                                                            <div class="d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary" id="submitBtn" ><i class="
                                                                    fas fa-check-circle"></i> Simpan</button>
                                                                <input type="hidden" name="total" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Bayaran::End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    
    @include('event.dashboard.js.reservation')
@endsection


<script>
    $(document).ready(function() {
        var applicant_Type = $('#applicant_type').val();
        updateApplicantType(applicant_Type);
        function updateApplicantType(val){
            if (val == '1') {
                $('#maklumatPenganjurUtamaNavItem').hide();
                $('.namaAgensi').removeAttr('required');
                $('.namaAgensi').attr('required', false);
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
                $('.namaAgensi').attr('required', true);
            }
        }


        $('.jenis_bayaran').on('change', function() {
            var selectedJenisBayaran = $(this).val();
            
            console.log($(this).val());
        });

        $('#kemaskiniStatusBayaranPend').on('click', function() {
            console.log('test4');
        })
        $('[name=kemaskiniStatusBayaranPenuh]').on('click', function() {
            // console.log('test2');
        })
        $('#kemaskiniStatusBayaranPenuh').on('click', function() {
            // console.log('test3');
        })
        
        // Tab Tindakan||Maklumat Bayaran Calculation
        $("[name='sewaTapak_kadar'], [name='sewaTapak_kuantiti'], [name='sewaTapak_diskaun'], [name='sewaTapak_diskaunPer']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'sewaTapak_kadar', 'sewaTapak_kuantiti', 'sewaTapak_diskaun', 'sewaTapak_diskaunPer', 'sewaTapak_jumlah');
            updateTotalAll();
        });
        $("[name='sewaTasik_kadar'], [name='sewaTasik_kuantiti'], [name='sewaTasik_diskaun'], [name='sewaTasik_diskaunPer']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'sewaTasik_kadar', 'sewaTasik_kuantiti', 'sewaTasik_diskaun', 'sewaTasik_diskaunPer', 'sewaTasik_jumlah');
            updateTotalAll();
        });
        $("[name='laluanBawah50_kadar'], [name='laluanBawah50_kuantiti'], [name='laluanBawah50_diskaun'], [name='laluanBawah50_diskaunPer']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'laluanBawah50_kadar', 'laluanBawah50_kuantiti', 'laluanBawah50_diskaun', 'laluanBawah50_diskaunPer', 'laluanBawah50_jumlah');
            updateTotalAll();
        });
        $("[name='laluanAtas50_kadar'], [name='laluanAtas50_kuantiti'], [name='laluanAtas50_diskaun'], [name='laluanAtas50_diskaunPer']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'laluanAtas50_kadar', 'laluanAtas50_kuantiti', 'laluanAtas50_diskaun', 'laluanAtas50_diskaunPer', 'laluanAtas50_jumlah');
            updateTotalAll();
        });
        $("[name='bukanMilikPPj_kadar'], [name='bukanMilikPPj_kuantiti'], [name='bukanMilikPPj_diskaun'], [name='bukanMilikPPj_diskaunPer']").on("input", function () {
            var fieldName = $(this).attr('name');
            updateCalculations(fieldName, 'bukanMilikPPj_kadar', 'bukanMilikPPj_kuantiti', 'bukanMilikPPj_diskaun', 'laluanAtas50_diskaunPer', 'bukanMilikPPj_jumlah');
            updateTotalAll();
        });
        $("#cajDepositID").on('input', function() {
            updateTotalAll();
        })
        function updateCalculations(diskaunType, kadar, kuantiti, diskaun, diskaunPer, jumlahSewa) {
            var kadarValue = parseFloat($('[name='+ kadar +']').val()) || 0;
            var kuantitiValue = parseInt($('[name='+ kuantiti +']').val()) || 0;
            // var applyDiscount = $("[name='applyDiscount']").is(":checked");
            var diskaunValue = parseFloat($('[name='+ diskaun +']').val()) || 0;
            var diskaunPerValue = parseInt($('[name='+ diskaunPer +']').val()) || 0;
            var total = kadarValue * kuantitiValue;

            console.log(diskaunType, diskaun, diskaunPer, diskaunValue);
            if (diskaunType == diskaun) {
                diskaunPerUpdated = Math.round((diskaunValue/total) * 100)
                $('[name='+ diskaunPer +']').val(diskaunPerUpdated);
                diskaunPerValue = diskaunPerUpdated;
            } else if (diskaunType == diskaunPer){
                diskaunValueUpdated = total * (diskaunPerValue/100)
                $('[name='+ diskaun +']').val(diskaunValueUpdated.toFixed(2));
            }
            if (diskaunValue > 0 || diskaunPerValue > 0) {
                total -= (diskaunPerValue / 100) * total;
            }

            $('[name='+ jumlahSewa +']').val(total.toFixed(2));
        }
        function updateTotalAll() {
            var tapakTotal = parseFloat($('[name=sewaTapak_jumlah]').val()) || 0;
            var tasikTotal = parseInt($('[name=sewaTasik_jumlah]').val()) || 0;
            var laluanBawah50 = parseFloat($('[name=laluanBawah50_jumlah]').val()) || 0;
            var laluanAtas50 = parseInt($('[name=laluanAtas50_jumlah]').val()) || 0;
            var bukanMilikPpj = parseInt($('[name=bukanMilikPPj_jumlah]').val()) || 0;
            
            var total = tapakTotal + tasikTotal + laluanBawah50 + laluanAtas50 + bukanMilikPpj;

            $('[name=sewaTapakKadar]').val(total.toFixed(2));

            var depo = parseFloat($("#cajDepositID").val());
            total = total + depo;

            $('[id=jumlahKeseluruhan]').val(total.toFixed(2));
        }
        // Tab Tindakan -> Maklumat Bayaran Calculation

        $('.peringkatPenganjuran').on('change', function() {
            var val = $(this).val()
            if (val == 1) {
                $('#peringkatPenganjuranDiterima').prop('hidden', false)
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', true)
                $('#peringkatPenganjuranDitolak').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').attr('required', true);
                $('#peringkatPenganjuranDiterimaTarikh').attr('required', true);
            } else if (val == 2) {
                $('#peringkatPenganjuranDiterima').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').removeAttr('required');
                $('#kt_daterangepicker_5').removeAttr('required');
                $('#peringkatPenganjuranDiterimaMasa').removeAttr('required');
                $('.masaSemakan').removeAttr('required');
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', false)
                $('#peringkatPenganjuranDitolak').prop('hidden', true)
            } else if (val == 3) {
                $('#peringkatPenganjuranDiterima').prop('hidden', true)
                $('#peringkatPenganjuranDiterimaTempat').removeAttr('required');
                $('#kt_daterangepicker_5').removeAttr('required');
                $('#peringkatPenganjuranDiterimaMasa').removeAttr('required');
                $('.masaSemakan').removeAttr('required');
                $('#peringkatPenganjuranTidakLengkap').prop('hidden', true)
                $('#peringkatPenganjuranDitolak').prop('hidden', false)
            }
        });

        $('#statusKeputusanMsyt').on('change', function() {
            var val = $(this).val()
            if (val == 1) {
                $('#pembentanganSemula').prop('hidden', false)
                $('#pembentanganSemulaTarikh').attr('required', true);
                $('#pembentanganSemulaTempat').attr('required', true);
            } else {
                $('#pembentanganSemula').prop('hidden', true)
                $('#pembentanganSemulaTarikh').attr('required', false);
                $('#pembentanganSemulaTempat').attr('required', false);
            }
        })
        $('#statusKeputusanMsyt2').on('change', function() {
            var val = $(this).val()
            if (val == 1) {
                $('#pembentanganSemula2').prop('hidden', false)
                $('#pembentanganSemulaTarikh2').attr('required', true);
                $('#pembentanganSemulaTempat2').attr('required', true);
            } else {
                $('#pembentanganSemula2').prop('hidden', true)
                $('#pembentanganSemulaTarikh2').attr('required', false);
                $('#pembentanganSemulaTempat2').attr('required', false);
            }
        })

        $('#minitMesyuaratId').on('change', function (event) {
            console.log('test')
            handleFileUpload(event, 'JKK PPj Minit Mesyuarat.pdf', 'JKK PPj Minit Mesyuarat.pdf');
        });

        // Function to handle file upload
        function handleFileUpload(event, fixedName, name) {
            const input = event.target;
            const fileId = input.id;
            const fileDisplayId = 'fileDisplay' + fileId.charAt(fileId.length - 1);
            const fileName = input.name;
            const fileDisplay = $('#minitMesyuaratDisplay');
            if (input.files.length > 0) {
                // $('#minitMesyuaratDisplay').hide();
                $('#minitMesyuaratDisplay').prop('hidden', false);
                fileDisplay.text(fixedName);
                fileDisplay.css('color', 'green');
            } else {
                // $('#minitMesyuaratDisplay').hide();
                $('#minitMesyuaratDisplay').prop('hidden', false);
                fileDisplay.text('Tiada fail dimuat naik');
                fileDisplay.css('color', 'warning');
            }
        }
        // File Upload::End

        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        
        $('#kt_daterangepicker_4').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true
        });
        // $('#kt_daterangepicker_5').datepicker({
        //     autoApply: true,
        //     autoclose: true,
        //     showDropdowns: true,
        //     autoUpdateInput: false,
        //     singleDatePicker: true,
        //     minDate: new Date(),
        //     maxDate: maxDate.toDate(), // Set the maximum date
        //     locale: {
        //         cancelLabel: 'Clear',
        //         format: 'YYYY-MM-DD'
        //     }
        // });
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            // minDate: moment().add(1, 'day'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_6').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true,
        });
        
        var datePickerHingga = $('#kt_datepicker_2').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
        // datePickerDari.on('changeDate', function (selected) {
        //     var minDate = new Date(selected.date.valueOf());
        //     datePickerHingga.datepicker('setStartDate', minDate);
        // });

        checkStatusAndToggle();
        $('#statusPemohon').on('change', function () {
            checkStatusAndToggle();
        });
        function checkStatusAndToggle() {
            var selectedStatus = $('#statusPemohon').val();
            if (selectedStatus == 'penganjurUtama') {
                $('#maklumatPenganjurUtamaNavItem').hide();
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
            }
        }
        
        // Hide lopo element when the document is ready
        $('#lopo').attr('hidden', true);
        $('#norujukanbayaran').attr('hidden', true);
        $('#nocekDiv').attr('hidden', true);
        $('#nocek').attr('hidden', true);
        $('#namabankDiv').attr('hidden', true);
        $('#namabank').attr('hidden', true);
        
        $('#carabayaran').on('change', function() {
            var selectedDescription = $(this).find(':selected').text().toLowerCase();
            var caraBayaranVal = $(this).val();
            console.log(caraBayaranVal)
            if (caraBayaranVal == 2){           // LO/PO
                $('#lopo').removeAttr('hidden');
                $('input[name="lopo"]').attr('required', true);
                $('#nocekDiv').attr('hidden', true);
                $('input[name="nocek"]').removeAttr('required');
                $('#namabankDiv').attr('hidden', true);
                $('input[name="namabank"]').removeAttr('required');
                $('#norujukanbayaran').removeAttr('hidden');
            } else if (caraBayaranVal == 3){    // Bank Draft
                $('#lopo').attr('hidden', true);
                $('input[name="lopo"]').removeAttr('required');
                $('#nocekDiv').removeAttr('hidden');
                $('input[name="nocek"]').attr('required', true);
                $('#namabankDiv').removeAttr('hidden');
                $('input[name="namabank"]').attr('required', true);
                $('#norujukanbayaran').removeAttr('hidden');
                $('#lopo').attr('hidden', true);
            } else {                            // Others
                $('#lopo').attr('hidden', true);
                $('input[name="lopo"]').removeAttr('required');
                $('#nocekDiv').attr('hidden', true);
                $('input[name="nocek"]').removeAttr('required');
                $('#namabankDiv').attr('hidden', true);
                $('input[name="namabank"]').removeAttr('required');
                $('#norujukanbayaran').removeAttr('hidden');
                $('#lopo').attr('hidden', true);
            }
        });

    });
</script>
@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
    
    .highlight {
        background-color: #F1F3FF; /* Set your desired background color for highlighting */
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <h3 class="card-title font-weight-bolder">Borang Permohonan Acara di Putrajaya</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 px-0">
                                    <!-- Navigation Bar::Start -->
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item position-relative" id="butiranPemohonNavItem">
                                            <a class="nav-link active pr-1" id="butiranPemohon-tab" data-toggle="tab" href="#butiranPemohon">
                                                <span class="nav-text">Butiran Pemohon</span>
                                                <i hidden id="exclamation-butiranPemohon" style="position: absolute; top: 0%; left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="butiranAcaraNavItem">
                                            <a class="nav-link pr-1" id="butiranAcara-tab" data-toggle="tab" href="#butiranAcara" aria-controls="butiranAcara">
                                                <span class="nav-text">Butiran Acara</span>
                                                <i hidden id="exclamation-butiranAcara" style="position: absolute; top: 0%; left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="maklumatPenganjurUtamaNavItem">
                                            <a class="nav-link pr-1" id="maklumatPenganjurUtama-tab" data-toggle="tab" href="#maklumatPenganjurUtama" aria-controls="maklumatPenganjurUtama">
                                                <span class="nav-text">Maklumat Penganjur Utama</span>
                                                <i hidden id="exclamation-maklumatPenganjurUtama" style="position: absolute; top: 0%;left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="muatNaikDokumenNavItem">
                                            <a class="nav-link pr-1" id="muatNaikDokumen-tab" data-toggle="tab" href="#muatNaikDokumen" aria-controls="muatNaikDokumen">
                                                <span class="nav-text">Muat Naik Dokumen</span>
                                                <i hidden id="exclamation-muatNaikDokumen" style="position: absolute; top: 0%;left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="perakuanPemohonNavItem">
                                            <a class="nav-link pr-1" id="perakuanPemohon-tab" data-toggle="tab" href="#perakuanPemohon" aria-controls="perakuanPemohon">
                                                <span class="nav-text">Perakuan Pemohon</span>
                                                <i hidden id="exclamation-perakuan" style="position: absolute; top: 0%;left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- Navigation Bar::End -->
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <!-- Navigation Content::Start -->
                                    <div id="myId" data-id="{{ Crypt::encrypt($data['id']) }}"></div>
                                    <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 mt-3 py-3">Maklumat Am</div>
                                    <div class="row px-5">
                                        <div class="col-xl-6">
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label class="font-weight-bolder">Status</label></div>
                                                    <div class="col-9"><input disabled type="text" class="form-control form-control-solid form-control-lg" name="" placeholder="" value="Draf Permohonan Acara" /></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                    </div>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Butiran Pemohon::Start -->
                                        <div class="tab-pane fade show active" id="butiranPemohon" role="tabpanel" aria-labelledby="butiranPemohon-tab">
                                            <form class="myForm" id="form1">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Pemohon</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Jenis Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jenis Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select required name="jenisPemohon" class="jenisPemohon form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected> Sila Pilih</option>
                                                                                <option value="1"> Individu</option>
                                                                                <option value="2"> Syarikat/Organisasi</option>
                                                                            </select>
                                                                            <div hidden class="text-danger" id="invalid-jenisPemohon">* Sila pilih Jenis Pemohon.</div>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jenis Pemohon-->
                                                                <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Pemohon (Individu / Syarikat / Organisasi) <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="namaPemohon" placeholder="" value="{{ session('user.name') }}" />
                                                                            <div hidden class="text-danger" id="invalid-namaPemohon">* Sila isi nama Pemohon.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <!--begin::Nama Pegawai untuk dihubungi-->
                                                                <div hidden class="form-group" id="contactPersonForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Pegawai untuk dihubungi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="form-control form-control-lg contactPerson" name="contactPerson" placeholder="" value=""/>
                                                                            <div hidden class="text-danger" id="invalid-contactPerson">* Sila isi maklumat ini.</div>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::Status Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select required name="statusPemohon" class="statusPemohon form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected> Sila Pilih</option>
                                                                                <option value="1"> Penganjur Utama</option>
                                                                                <option value="2"> Penganjur Acara</option>
                                                                            </select>
                                                                            <div hidden class="text-danger" id="invalid-statusPemohon">* Sila pilih Status Pemohon.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Status Pemohon-->
                                                                <!--begin::Jawatan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jawatan <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="jawatan" placeholder="" value="" />
                                                                            <div hidden class="text-danger" id="invalid-jawatan">* Sila isi Jawatan.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jawatan-->
                                                                <!--begin::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Kad Pengenalan / No Pendaftaran Syarikat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="kadPengenalanPendaftaran" placeholder="" value="{{ session('user.ic') }}" />
                                                                            <div hidden class="text-danger" id="invalid-kadPengenalanPendaftaran">* Sila isi nombor Kad Pengenalan / Pendaftaran Syarikat.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <!--begin::Alamat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="alamat" placeholder="" value="{{ session('user.address') }}" />
                                                                            <div hidden class="text-danger" id="invalid-alamat">* Sila isi Alamat.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Alamat-->
                                                                <!--begin::Poskod & Bandar-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" name="poskod" placeholder="" value="{{ session('user.postcode') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-poskod">* Sila isi Poskod.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" name="bandar" placeholder="" value="{{ session('user.city') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-bandar">* Sila isi Bandar.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Poskod & Bandar-->
                                                                <!--begin::Negeri & Negara-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <select required name="negeri" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="">Sila Pilih</option>
                                                                                        @foreach ($data['negeri'] as $n)
                                                                                            <option value="{{ $n->id }}" @if (session('user.state') == $n->id) selected @endif>{{ ucfirst(strtolower($n->ls_description)) }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <div hidden class="text-danger" id="invalid-negeri">* Sila pilih Negeri.</div>        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <select required name="negara" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="">Sila Pilih</option>
                                                                                        @foreach ($data['negara'] as $m)
                                                                                            <option value="{{ $m->id }}" @if (session('user.country') == $m->id) selected @endif>{{ ucfirst(strtolower($m->lc_description)) }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{-- <input readonly type="text" class="form-control form-control-solid form-control-lg text-muted" name="negara" placeholder="" value="" /> --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Negeri & Negara-->
                                                                <!--begin::No GST-->
                                                                <div hidden class="form-group" id="nogstForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No. GST </label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="form-control form-control-lg" name="nogst" placeholder="" value=""/>
                                                                            <div hidden class="text-danger" id="invalid-nogst">* Sila isi maklumat ini.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Pejabat <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" name="noPejabat" placeholder="" value="" />
                                                                                    <div hidden class="text-danger" id="invalid-noPejabat">* Sila isi Nombor Telefon Pejabat.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" name="noBimbit" placeholder="" value="{{ session('user.mobile_no') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-noBimbit">* Sila Iii Nombor Telefon Bimbit.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <!--begin::No. Faks && Email-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Email <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required type="text" class="form-control form-control-lg" name="email" placeholder="" value="{{ session('user.email') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-email">* Sila isi Email.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Faks</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" class="form-control form-control-lg" name="noFaks" placeholder="" value="{{ $data['formPemohon']['noFaks'] ?? '' }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Butiran Pemohon::End -->
                                        <!-- Butiran Acara::Start -->
                                        <div class="tab-pane fade" id="butiranAcara" role="tabpanel" aria-labelledby="butiranAcara-tab">
                                            <form class="myForm" id="form2">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <input type="text" hidden class="form-control form-control-solid form-control-lg" name="jumlahHariPenggunaanPerakuan" id="" value="" />
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Acara</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Nama Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="namaAcara" placeholder="" value="{{ $data['formAcara']['namaAcara'] ?? '' }}" />
                                                                            <div hidden class="text-danger" id="invalid-namaAcara">* Sila isi Nama Acara.</div>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Acara-->
                                                                <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhAcaraDari" id="kt_datepicker_1" name="tarikhAcaraDari" readonly="readonly" value="{{ \Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') }}" />
                                                                                    <input type="text" hidden class="form-control form-control-solid form-control-lg " id="" name="tarikhAcaraDari" value="{{ \Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhAcaraDari">* Sila pilih Tarikh Acara Bermula.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input placeholder="Pilih Tarikh Acara Dari.." disabled type="text" class="form-control form-control-solid form-control-lg tarikhAcaraHingga" id="kt_datepicker_1" name="tarikhAcaraHingga" readonly="readonly" value="{{ \Carbon\Carbon::parse($data['carian']->tarikhAkhir)->format('d-m-Y') }}" />
                                                                                    <input placeholder="Pilih Tarikh Acara Dari.." hidden type="text" class="form-control form-control-solid form-control-lg tarikhAcaraHingga" id="" name="tarikhAcaraHingga"  value="{{ \Carbon\Carbon::parse($data['carian']->tarikhAkhir)->format('d-m-Y') }}" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhAcaraHingga">* Sila pilih Tarikh Acara Bermula.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <!--begin::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Dari <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaPenganjuranDari" value="09:00 AM" type="text" />
                                                                                    <div hidden class="text-danger" id="invalid-masaPenganjuranDari">* Sila pilih Masa Penganjuran Dari.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Penganjuran Hingga <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaPenganjuranHingga" value="05:00 PM" type="text" />
                                                                                    <div hidden class="text-danger" id="invalid-masaPenganjuranHingga">* Sila pilih Masa Penganjuran Hingga.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                <!--begin::Lokasi Acara-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Lokasi Acara </label></div>
                                                                        <div class="col-9">
                                                                            <input readonly type="text" class="form-control form-control-solid form-control-lg" name="lokasiAcara" placeholder="" value="{{ $data['event']->name ?? '' }}" />
                                                                            <div hidden class="text-danger" id="invalid-lokasiacara">* Sila pilih Lokasi Acara.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Lokasi Acara-->
                                                                <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jumlah Peserta / Pengunjung <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input required type="text" class="form-control form-control-lg" name="jumlahPesertaPengunjung" placeholder="" value="{{ $data['formAcara']['jumlahPesertaPengunjung'] ?? '' }}" />
                                                                            <div hidden class="text-danger" id="invalid-jumlahPesertaPengunjung">* Sila pilih Jumlah Peserta / Pengunjung.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jumlah Peserta/ Pengunjung-->
                                                                <!--begin::Peringkat Penganjuran-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select required name="peringkatPenganjuran" id="peringkatPenganjuranId" class="form-control form-control-lg mt-3" >
                                                                                <option value=""> Sila Pilih</option>
                                                                                <option value="1"> Antarabangsa</option>
                                                                                <option value="2"> Kebangsaan</option>
                                                                                <option value="3"> Putrajaya</option>
                                                                                <option value="4"> Lain-lain</option>
                                                                            </select>
                                                                            <div hidden class="text-danger" id="invalid-peringkatPenganjuran">* Sila pilih Peringkat Penganjuran.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Peringkat Penganjuran-->
                                                                <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                <div hidden class="form-group" id="peringkatPenganjuranLainLain">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nyatakan Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9"><input type="text" class="form-control form-control-lg" name="nyatakanPeringkatPenganjuran" placeholder="" value=""/>
                                                                            <div hidden class="text-danger" id="invalid-nyatakanPeringkatPenganjuran">* Sila Isi Maklumat.</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jumlah Peserta/ Pengunjung-->
                                                                <!--begin::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required @if (\Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') == null) disabled @endif type="text" class="form-control form-control-lg tarikhMasukTapak" id="kt_datepicker_1" name="tarikhMasukTapak" autocomplete="off" placeholder="@if (\Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') == null) Pilih Tarikh Acara Dari.. @endif" value="" />
                                                                                    <input hidden type="text" class="form-control form-control-lg tarikhMasukTapak" id="kt_datepicker_1" name="tarikhMasukTapak" autocomplete="off" value="" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhMasukTapak">* Sila pilih Tarikh Masuk Tapak.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaMasukTapak" value="{{ $data['formAcara']['masaMasukTapak'] ?? '09:00 AM' }}" type="text" />
                                                                                    <div hidden class="text-danger" id="invalid-masaMasukTapak">* Sila pilih Masa Masuk Tapak.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                <!--begin::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input required readonly type="text" class="form-control form-control-lg tarikhKeluarTapak" id="kt_datepicker_1" name="tarikhKeluarTapak" placeholder=""  value="" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhKeluarTapak">* Sila pilih Tarikh Keluar Tapak.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaKeluarTapak" value="{{ $data['formAcara']['masaKeluarTapak'] ?? '05:00 PM' }}" type="text" />
                                                                                    <div hidden class="text-danger" id="invalid-masaKeluarTapak">* Sila pilih Masa Keluar Tapak.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                <!--begin::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Melibatkan penggunaan Drone atau Alat Kawalan Jauh (terbang)</label></div>
                                                                        <div class="col-9">
                                                                            <select name="libatPenggunaanDrone" class="form-control form-control-lg mt-3" id="kt_select2_5">
                                                                                <option value=""> Sila Pilih</option>
                                                                                <option value="1" {{ ($data['formAcara'] ?? null) && $data['formAcara']['libatPenggunaanDrone'] == 'ya' ? 'selected' : '' }}> Ya</option>
                                                                                <option value="0" {{ ($data['formAcara'] ?? null) && $data['formAcara']['libatPenggunaanDrone'] == 'tidak' ? 'selected' : '' }}> Tidak</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                <!--begin::Nama Tetamu kehormat Utama-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Tetamu kehormat Utama (jika ada)</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="form-control form-control-lg" name="namaTetamuKehormat" placeholder="" value="{{ $data['formAcara']['namaTetamuKehormat'] ?? '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Tetamu kehormat Utama-->
                                                                <!--begin::Maklumat Tambahan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Maklumat Tambahan </label></div>
                                                                        <div class="col-9"><input type="text" class="form-control form-control-lg" name="maklumatTambahan" placeholder="" value="@if($data['fasility1']->rent_perday == "0.00" || $data['fasility1']->rent_perday == "1.00"){{ $data['fasility1']->remark }} @else{{ $data['formAcara']['maklumatTambahan'] ?? 'TIADA' }}@endif" /></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Maklumat Tambahan-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Butiran Acara::End -->
                                        <!-- Butiran Penganjur Utama::Start -->
                                        <div class="tab-pane fade" id="maklumatPenganjurUtama" role="tabpanel" aria-labelledby="maklumatPenganjurUtama-tab">
                                            <form class="myForm" id="form3">
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Penganjur Utama</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Agensi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="namaAgensi form-control form-control-lg" name="namaAgensi" placeholder="" value=""/>
                                                                            <div hidden class="text-danger" id="invalid-namaAgensi">* Sila isi Nama Agensi.</div>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" class="form-control form-control-lg" name="alamatAgensi" placeholder="" value="" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod</label></div>
                                                                                <div class="col-9"><input type="text" class="form-control form-control-lg" name="poskodAgensi" placeholder="" value="" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar</label></div>
                                                                                <div class="col-9"><input type="text" class="form-control form-control-lg" name="bandarAgensi" placeholder="" value="" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <select name="negeriAgensi" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="">Sila Pilih</option>
                                                                                        @foreach ($data['negeri'] as $n)
                                                                                            <option value="{{ $n->id }}" @if (session('user.state') == $n->id) selected @endif>{{ ucfirst(strtolower($n->ls_description)) }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <div hidden class="text-danger" id="invalid-negeriAgensi">* Sila pilih Negeri.</div>        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <select name="negaraAgensi" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="">Sila Pilih</option>
                                                                                        @foreach ($data['negara'] as $m)
                                                                                            <option value="{{ $m->id }}" @if (session('user.country') == $m->id) selected @endif>{{ ucfirst(strtolower($m->lc_description)) }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <div hidden class="text-danger" id="invalid-negaraAgensi">* Sila pilih Negeri.</div>  
                                                                                    {{-- <input readonly type="text" class="form-control form-control-solid form-control-lg text-muted" name="negara" placeholder="" value="" /> --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon</label></div>
                                                                                <div class="col-9"><input type="text" class="form-control form-control-lg" name="telBimbitAgensi" placeholder="" value="" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Butiran Penganjur Utama::End -->
                                        <!-- Muat Naik Dokumen::Start -->
                                        <div class="tab-pane fade" id="muatNaikDokumen" role="tabpanel" aria-labelledby="muatNaikDokumen-tab">
                                            <div class="card card-custom">
                                                <div class="card-body p-0">
                                                    <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                    <div class="card mx-7">
                                                        <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Muat Naik Dokumen</div>
                                                        <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                            <div class="table-responsive">
                                                                <form class="myForm" id="form4" enctype="multipart/form-data">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 3%">Bil.</th>
                                                                                <th style="width: 30%">Senarai Semak</th>
                                                                                <th style="width: 10%">Muat Naik</th>
                                                                                <th style="width: 20%">Tindakan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">1</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input required name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">2</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input required name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">3</th>
                                                                                <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="suratPelantikan" type="file" class="form-control" id="fileUpload3" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload3" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay3"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">4</th>
                                                                                <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="petaLaluan" type="file" class="form-control" id="fileUpload4" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload4" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay4"></th>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Muat Naik Dokumen::End -->
                                        <!-- Perakuan Pemohon::Start -->
                                        <div class="tab-pane fade" id="perakuanPemohon" role="tabpanel" aria-labelledby="perakuanPemohon-tab">
                                            {{-- <form class="myForm" id="exclamation"> --}}
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Input-->
                                                                <div class="form-group py-5 my-auto">
                                                                    <div class="row">
                                                                        <div class="col-12 d-flex justify-content-center align-items-center">
                                                                            <label class="">
                                                                                <input type="checkbox" id="perakuanCheckbox" name="perakuanPemohon"/>
                                                                                <span class="px-5">Dengan ini saya mengesahkan maklumat yang diberikan adalah benar.</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                            <div class="card mx-7" id="rumusanCard" style="display: none;">
                                                                @if ($data['config_depoForm'] == 1)
                                                                    <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Perbankan</div>
                                                                    <div class="px-7">
                                                                        <form action="" id="form5">
                                                                            <!--begin: Bank Info-->
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">Nama Penuh <span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="namaPenuh"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">No. Kad Pengenalan<span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="kadPengenalan"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">Email <span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="emel"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">No. Telefon Bimbit <span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="phoneNo"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">Nama Bank <span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="namaBank"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label  class="col-2 col-form-label">No. Akaun Bank <span class="text-danger">*</span></label>
                                                                                <div class="col-10">
                                                                                    <input required class="form-control" type="text" name="akaunBank"/>
                                                                                </div>
                                                                            </div>
                                                                            <!--end: Bank Info-->
                                                                        </form>
                                                                    </div>
                                                                @endif
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Rumusan Permohonan</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Nama Pemohon-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Pemohon</label></div>
                                                                            <div class="col-9">
                                                                                <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohonPerakuan" placeholder="" value="{{ session('user.name' )}}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Pemohon-->
                                                                    <!--begin::Nama Acara-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Acara</label></div>
                                                                            <div class="col-9">
                                                                                <input disabled type="text" class="form-control form-control-lg" name="namaAcaraPerakuan" placeholder="" value="" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Acara-->
                                                                    <!--begin::Tarikh Acara Dari & Tarikh Acara Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Dari</label></div>
                                                                                    <div class="col-9"><input disabled type="text" class="tarikhAcaraDariPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraDariPerakuan" placeholder="" value="{{ \Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') }}" /></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Hingga</label></div>
                                                                                    <div class="col-9"><input disabled type="text" class="tarikhAcaraHinggaPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraHinggaPerakuan" placeholder="" value="{{ \Carbon\Carbon::parse($data['carian']->tarikhAkhir)->format('d-m-Y') }}" /></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Acara Dari & Tarikh Acara Hingga-->
                                                                <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Masuk Tapak</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhMasukTapak" id="kt_datepicker_1" name="tarikhMasukTapak" readonly="readonly" value="" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhAcaraDari"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Tarikh Keluar Tapak</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhKeluarTapak" id="kt_datepicker_1" name="tarikhKeluarTapak" readonly="readonly" value="" />
                                                                                    <div hidden class="text-danger" id="invalid-tarikhAcaraHingga"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Lokasi</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="lokasiPerakuan" placeholder="{{ $data['event']->name ?? '' }}" value="{{ $data['event']->name ?? '' }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jumlah Hari Penggunaan Tapak</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="jumlahHariPenggunaanPerakuan" id="jumlahHariPenggunaanPerakuan" value="" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Sewa Tapak Sehari (RM)</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="sewaTapakSehariPerakuan" id="sewaTapakSehariPerakuan" placeholder="" value="{{ number_format($data['event']->rent_perday, 2, '.', ',') }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>GST (0%)</label></div>
                                                                            <div class="col-9">
                                                                                <input readonly type="text" class="form-control form-control-solid form-control-lg" name="gstPerakuan" placeholder="" value="0" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Deposit (RM)</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="depositPerakuan" placeholder="" value="{{ number_format(floatval(str_replace(',', '', $data['event']->deposit)), 2, '.', ',') }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jumlah (RM)</label></div>
                                                                            <div class="col-9">
                                                                                {{-- {{ number_format($data['total'], 2, '.', ',') }} --}}
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="jumlahPerakuan" placeholder="" id="jumlahPerakuan" value="" />
                                                                                <div class="" id="">* Jumlah tidak termasuk deposit & lain-lain caj.</div>
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                    <div class="row">
                                                                        <div class="col-12 d-flex justify-content-end">
                                                                            <button type="button" id="submitForms" class="btn btn-light-primary font-weight-bold mb-7">Hantar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            {{-- </form> --}}
                                        </div>            
                                        <!-- Perakuan Pemohon::End -->                   
                                    </div>
                                    <!-- Navigation Content::End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('event.public.js.reservation')
@endsection

<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
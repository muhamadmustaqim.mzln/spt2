@extends('layouts.master')
<style>
.active1 {
	background-color: #3445E5 !important;
	color: #FFFFFF !important;
	border-color: transparent;
}
.column {
	display: none; /* Hide all elements by default */
}
.show {
	display: block;
}
</style>
@section('container')

<!--begin::Content-->
	@if(Session::has('flash'))
		<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
	@endif
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
		<!--begin::Subheader-->
		<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
			<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Acara</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Acara</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
			</div>
		</div>
		<!--end::Subheader-->
		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<script src="https://unpkg.com/@dotlottie/player-component@latest/dist/dotlottie-player.mjs" type="module"></script> 
			
			<center>
			<dotlottie-player src="https://lottie.host/e3b76582-3859-4125-a42e-d2bfa3da47e7/E5Y2t6CHGG.json" background="transparent" speed="1" style="width: 300px; height: 300px;" loop autoplay></dotlottie-player>
			Background Update In Progress
			</center>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
		
	</div>
	<!--end::Content-->
	<!-- Button trigger modal-->
	<!-- Modal-->
	
@endsection



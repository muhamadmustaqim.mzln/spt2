@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->created_at)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>
                                        {{-- {{ $data['user_detail']->bud_phone_no }} --}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ Helper::moneyhelper($data['main']->bmb_subtotal) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Jenis Bayaran</th>
                                    <th>No. Transaksi</th>
                                    @if(count($data['online']) > 0)
                                    {{-- <th>No. SAP</th> --}}
                                    @endif
                                    <th>Tarikh Transaksi</th>
                                    <th class="text-right">Jumlah Bayaran (RM)</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $paid = 0.00;
                            @endphp
                            <tbody>
                                @if(count($data['kaunter']) > 0)
                                    @foreach($data['kaunter'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>{{ $p->bp_receipt_number }}</td>
                                        {{-- <td>{{ $p->bp_receipt_date }}</td> --}}
                                        <td class="text-right">{{ $p->bp_paid_amount }}</td>
                                        <td style="width: 15%;">
                                            <a href="#" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                        </td>
                                    </tr>
                                    @php
                                        $paid = $paid + $p->bp_paid_amount
                                    @endphp
                                    @endforeach
                                @elseif(count($data['spa_payment']) > 0)
                                    @foreach($data['spa_payment'] as $p)
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                            <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                            <td>{{ $p->application_no ?? '' }}</td>
                                            {{-- <td>{{ $p->fpx_trans_id ?? '' }}</td> --}}
                                            {{-- <td>{{ $data['main']->sap_no ?? "" }}</td> --}}
                                            <td>{{ Helper::datetime_format($p->created_at ?? '') }}</td>
                                            {{-- <td>{{ Helper::datetime_format($p->fpx_date ?? '') }}</td> --}}
                                            <td class="text-right">{{ $p->paid_amount ?? '' }}</td>
                                            {{-- <td>{{ $p->amount_paid }}</td> --}}
                                            <td style="width: 15%;">
                                                <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['booking']), 'type' => $p->fk_lkp_payment_type]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                            </td>
                                        </tr>
                                    @php
                                        $paid = $paid + $p->paid_amount
                                    @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Tiada Resit Bayaran</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Acara</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Lokasi</th>
                                    <th>Harga Sewaan (RM)</th>
                                    @if($data['config_gst'] == 1)
                                        <th>Kod GST</th>
                                        <th>GST (RM)</th>
                                    @endif
                                    <th>Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $total = 0.00;
                                    $colspan = 5;
                                @endphp
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                        <td>{{ ($data['spa_booking_event']->event_name) }}</td>
                                        <td>{{ Helper::date_format($data['spa_booking_event']->event_date) }}</td>
                                        <td>{{ Helper::locationspa($data['spa_booking_event']->fk_spa_location) }}</td>
                                        <td style="text-align: end">{{ $data['main']->bmb_subtotal }}</td>
                                        @if($data['config_gst'] == 1)
                                            <td style="text-align: end"></td>
                                            <td style="text-align: end"></td>
                                        @endif
                                        <td style="text-align: end">{{ $data['main']->bmb_subtotal }}</td>
                                        @php
                                            // $total = $total + number_format($s->est_subtotal, 2)
                                        @endphp
                                    </tr>
                                {{-- @foreach($data['slot'] as $s)
                                <tr>
                                    <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                    <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                    <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                    <td>{{$s->ebf_start_date}}</td>
                                    <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                    <td style="text-align: end">{{number_format($s->est_total, 2)}}</td>
                                    <td style="text-align: end">{{ Helper::tempahanSportGST($s->est_gst_code)}}</td>
                                    <td style="text-align: end">{{$s->est_gst_rm}}</td>
                                    <td style="text-align: end">{{number_format($s->est_subtotal, 2)}}</td>
                                    @php
                                        $total = $total + number_format($s->est_subtotal, 2)
                                    @endphp
                                </tr>
                                @endforeach --}}
                                <tr>
                                    @if($data['config_gst'] == 1)
                                        @php
                                            $colspan = 7;
                                        @endphp
                                    @endif
                                    <td colspan="{{$colspan}}" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            {{-- <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">E</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">I</td>
                                <td class="text-right">0.00</td>
                            </tr> --}}
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_subtotal }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Deposit</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_deposit_rm }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                <td></td>
                                <td class="text-right">0.00</td>
                                {{-- {{ $data['main']->bmb_rounding }} --}}
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                <td></td>
                                <td class="text-right">{{ number_format($paid, 2) }}</td>
                            </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            {{-- <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2">Kembali</a> --}}
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

					



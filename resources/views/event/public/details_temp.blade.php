@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
    
    .highlight {
        background-color: #F1F3FF; /* Set your desired background color for highlighting */
    }
    
    .panoramacss {
        width: 100%;
        height: 300px
    }
    
    #panorama {
        width: 100%;
        height: 300px;
        background-color: rgb(222, 222, 222);
        position: relative;
        border-radius: 10px; /* Rounded corners for a smooth look */
        overflow: hidden; /* To contain the shine effect within the div */
    }

    /* Shiny effect using a pseudo-element */
    #panorama::before {
        content: '';
        position: absolute;
        top: 0;
        left: -75%;
        width: 50%;
        height: 100%;
        background: linear-gradient(
            to right,
            rgba(255, 255, 255, 0.1) 0%,
            rgba(255, 255, 255, 0.7) 50%,
            rgba(255, 255, 255, 0.1) 100%
        );
        transform: skewX(-25deg);
        transition: left 1s;
        animation: shine 2s infinite;
    }

    @keyframes shine {
        0% {
            left: -75%;
        }
        100% {
            left: 100%;
        }
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		
    <!--begin::Subheader-->
    <div class="subheader py-5 py-lg-10 gutter-b subheader-transparent mt-0" id="kt_subheader" style="background-color: #20274d; background-position: right bottom; background-size: auto 100%; background-repeat: no-repeat; background-image: url('{{ asset('assets/media/svg/patterns/taieri.svg') }}')">
        <div class="container ">
            <div class="bg-white rounded p-4 ">
                <form action="" method="post">
                    <div class="row">
                        @csrf
                        <div class="col-lg-4">
                            <div class="input-icon">
                                <select name="lokasi" class="form-control form-control-lg border-0 font-weight-bold" id="kt_select2_5" required>
                                    <option value=""> Pilih Lokasi Acara</option>
                                    @foreach ($data['location1'] as $l)				
                                        <option value="{{ $l->id }}" @if ($l->name == $data['fasility1']->name) selected @endif>{{ $l->name }}</option>
                                    @endforeach
                                </select>
                                <span>
                                    <i class="flaticon-search-1 icon-xl"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-icon">
                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold" id="kt_datepicker1" name="tarikhMula" placeholder="Tarikh Mula" autocomplete="off" required value="@if($data['carian'] != null) {{ Helper::date_format($data['carian']->tarikhMula) }} @endif" />
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-icon">
                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold" id="kt_datepicker2" name="tarikhAkhir" placeholder="Tarikh Akhir" autocomplete="off" required value="@if($data['carian'] != null) {{ Helper::date_format($data['carian']->tarikhAkhir) }} @endif" />
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                            </div>
                        </div>
                        {{-- <div class="col-lg-5">
                            <div class="input-icon" id="kt_daterangepicker_5">
                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold" name="tarikh" placeholder="{{$data['date']}}" autocomplete="off" value="{{$data['date']}}" readonly required />
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                <span class="bullet bullet-ver h-45px d-none d-sm-flex mr-2"></span>
                            </div>
                        </div> --}}
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-lg btn-primary font-weight-bold">CARIAN @if ($data['carian'] != null)  @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Subheader-->                        				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility1']->name }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-5">
                    {{-- <div id="panorama" class="mb-6" style="width: 100%; height: 300px"></div> --}}
				    <div id="panorama" class=" d-flex justify-content-center" style="width: 100%; height: 300px; background-color: rgb(222, 222, 222);">
                        <input type="text" id="panoramaID" value="{{ $data['fasility2']->virtual_img }}" hidden>
                        <input type="text" id="locationID" value="{{ $data['fasility2']->id }}" hidden>
                        <img src="{{asset('/assets/media/logos/ppj.png')}}" max-width="100%" height="100%"/>
                    </div>
                </div>
                <div class="col-lg-7 desk-ver">
                    <div class="row" id="aniimated-thumbnials">
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1542665952-14513db15293?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1542665952-14513db15293?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1549895058-36748fa6c6a7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1549895058-36748fa6c6a7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1577887546572-144f62fceb9e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1577887546572-144f62fceb9e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1541559476210-2c7094bc222b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1541559476210-2c7094bc222b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1560184611-ff3e53f00e8f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1629&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1560184611-ff3e53f00e8f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1629&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="https://images.unsplash.com/photo-1630435037688-74c9ec61ffa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" class="img-container text-white">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1630435037688-74c9ec61ffa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px; " alt="">
                                <div class="centered" ><h5 class="font-weight-bolder">+ 1 Gambar</h5></div>
                            </a>
                        </div>
                        <div class="col-lg-4" style="display:none;">
                            <a href="https://images.unsplash.com/photo-1583787035686-91b82ad5d811?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80"  data-sub-html=".caption7">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1583787035686-91b82ad5d811?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                                <div class="caption7">
                                    <h6>Dewan</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                        <div class="card-header">
                            <div class="card-title">
                            {{-- <h4 class="card-label font-weight-bolder text-dark">{{ $data['fasility1']->name }}</h4>
                            <span class="label label-lg font-weight-bold {{ Helper::status_color($data['fasility']->bh_status) }} label-inline">{{ Helper::status($data['fasility']->bh_status) }}</span> --}}
                            </div>
                            <div class="card-toolbar">
                            <h7 class="card-label text-dark  mr-2">bermula dari</h7>
                            <h3 class="card-label font-weight-boldest mr-2 text-danger">RM {{ Helper::moneyhelper($data['fasility1']->rent_perday) }}</h3>
                            <h7 class="card-label text-dark mr-4">per hari</h7>
                                @if(Session::has('user')) 
                                {{-- <button class="btn btn-primary font-weight-bolder btn-sm float-right mb-1" data-toggle="modal" data-target="#myModal">
                                    Teruskan Tempahan 
                                </button> --}}
                                <a href="{{ url('event/reservation', [
                                    Crypt::encrypt($data['id']),
                                    ($data['carian'] != null) ? Crypt::encrypt($data['carian']->tarikhMula) : null,
                                    ($data['carian'] != null) ? Crypt::encrypt($data['carian']->tarikhAkhir) : null
                                ]) }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan </a>
                                {{-- <a href="{{ url('event/reservation', [Crypt::encrypt($data['id'])]) }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a> --}}
                                @else
                                <a href="{{ url('auth') }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a>
                                @endif
                                <a href="{{ url('event') }}" class="btn btn-light-danger font-weight-bolder btn-sm float-right mb-1 ml-2">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="card-label font-weight-bolder text-dark mb-3 mt-4">Perihal</h4>
                                    <hr>
                                    <h6 class="card-label font-weight-bolder text-dark mb-3">Kadar Harga /hari</h6>
                                    <table class="table mt-2">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th style="width: 33%">Kadar Sewaan Sehari (RM)</th>
                                                <th style="width: 33%">Deposit</th>
                                                <th style="width: 33%">Kapasiti</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 13px">
                                            <tr>
                                                <td>RM {{ Helper::moneyhelper($data['fasility1']->rent_perday) }}</td>
                                                <td>RM {{ $data['fasility1']->deposit }}</td>
                                                <td>{{ $data['fasility1']->capacity }} orang</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                                <div class="col-lg-12">
                                    <h6 class="card-label font-weight-bolder text-dark mb-3">Fasiliti</h6>
                                        <table class="table table-borderless" style="font-size: 13px">
                                            <tr>
                                                <td><i class="fas fa-wifi text-dark mr-5"></i>Wifi</td>
                                                <td><i class="fas fa-toilet text-dark mr-5"></i>Tandas</td>
                                                <td><i class="fas fa-utensils text-dark mr-5"></i>Kedai Makan</td>
                                                <td><i class="fas fa-mosque text-dark mr-5"></i>Surau</td>
                                                <td><i class="fas fa-parking text-dark mr-5"></i>Parkir Kereta</td>
                                            </tr> 
                                        </table>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 pb-0">
                            <h6 class="card-title align-items-start flex-column mb-2">
                                <span class="card-label font-weight-bolder text-dark">Kekosongan Slot & Takwim</span>
                                <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['date'] }}</span>
                                
                            </h6>                          
                        </div>
                        <div class="card-header border-0 pt-5 pb-0">
                            <ul class="nav nav-dark nav-bold nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_5">Takwim</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="kt_tab_pane_7_5" role="tabpanel" aria-labelledby="kt_tab_pane_7_5">
                                    <div id="kt_calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        {{-- <div class="col-lg-6">
                            <a href='https://www.waze.com/ul?ll={{ $data['fasility']->latitude }}%2C{{ $data['fasility']->longitude }}&navigate=yes&zoom=17' class="btn btn-primary btn-block btn-lg font-weight-bolder mb-4"><i class="fab fa-waze"></i> Waze</a>
                        </div>
                        <div class="col-lg-6">
                            <a href='https://www.google.com/maps?layer=c&cbll={{ $data['fasility']->latitude }},{{ $data['fasility']->longitude }}' class="btn btn-dark btn-block btn-lg font-weight-bolder mb-4"><i class="fas fa-map-marker-alt"></i> Google Maps</a>
                        </div> --}}
                    </div>
                    <!--begin::List Widget 21-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 pb-0">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-dark">Cadangan Lokasi Lain</span>
                                <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['date'] }}</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            @foreach($data['others1'] as $o)
                            <!--begin::Item-->
                            <div class="d-flex flex-wrap align-items-center mb-10">
                                {{-- <div class="row">
                                    <div class="col-5"> --}}
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-90 symbol-2by3 flex-shrink-0 mr-4">
                                            <div class="symbol-label" style="background-image: url('{{ URL::asset("{$o->file_name}") }}')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                    {{-- </div>
                                    <div class="col-7"> --}}
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                            <a href="{{ url('event/details',Crypt::encrypt($o->id)) }}" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">{{ $o->name }}</a>
                                            <span class="text-muted font-weight-bold font-size-sm my-1"></span>
                                            <p><span class="label font-weight-bolder {{ Helper::status_color($o->status) }} label-inline">{{ Helper::status($o->status) }}</span></p>
                                            <p><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star text-warning icon-nm mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i></p>
                                        </div>
                                        <!--end::Title-->
                                    {{-- </div>
                                </div> --}}
                            </div>
                            <!--end::Item-->
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Sahkan Tarikh Tempahan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row mb-3">
                                    <div class="col-3 my-auto">
                                        Lokasi
                                    </div>
                                    <div class="col-9">
                                        <input readonly type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['fasility1']->name }}" />
                                    </div>
                                </div>
                                <input type="text" id="booked_date" value="{{ $data['booked_date1'] }}" >
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-3 my-auto">
                                                Dari
                                            </div>
                                            <div class="col-9">
                                                <input disabled type="text" class="form-control form-control-solid form-control-lg tarikhAcaraDari" id="kt_datepicker_1" name="tarikhAcaraDari" readonly="readonly" value="@if ($data['carian'] != null) {{ \Carbon\Carbon::parse($data['carian']->tarikhMula)->format('d-m-Y') }} @else {{ \Carbon\Carbon::now()->format('d M Y') }} @endif" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-3 my-auto">
                                                Hingga
                                            </div>
                                            <div class="col-9">
                                                <input disabled type="text" class="form-control form-control-solid form-control-lg tarikhAcaraDari" id="kt_datepicker_1" name="tarikhAcaraHingga" readonly="readonly" value="@if ($data['carian'] != null) {{ \Carbon\Carbon::parse($data['carian']->tarikhAkhir)->format('d-m-Y') }} @else {{ \Carbon\Carbon::now()->format('d M Y') }} @endif" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ url('event/reservation', [
                                    Crypt::encrypt($data['id']),
                                    ($data['carian'] != null) ? Crypt::encrypt($data['carian']->tarikhMula) : null,
                                    ($data['carian'] != null) ? Crypt::encrypt($data['carian']->tarikhAkhir) : null
                                ]) }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('event.public.js.details')
@endsection

<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
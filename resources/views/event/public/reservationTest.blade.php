@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                  				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                {{-- <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p> --}}
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-title font-weight-bolder">Semakan Permohonan Acara</h4>
                                </div>
                            </div>
                            <!-- Main Navigation Bar::Start -->
                            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                                <li class="nav-item position-relative" style="width: 33%">
                                    <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [19])) active @endif pr-1" id="maklumatPermohonanAcara-tab" data-toggle="tab" href="#maklumatPermohonanAcara">
                                        <span class="nav-text">Maklumat Permohonan Acara</span>
                                        @if ($data['main_booking']->fk_lkp_status == 28)
                                            <i style="position: absolute; top: 0%; left: 0%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                        @endif
                                    </a>
                                </li>
                                <li class="nav-item position-relative" style="width: 33%">
                                    <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [18, 22, 23, 28, 30])) active @endif pr-1" id="tindakan-tab" data-toggle="tab" aria-controls="#tindakan" href="#tindakan">
                                        <span class="nav-text">Tindakan</span>
                                        <i hidden id="exclamation-tindakan" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                    </a>
                                </li>
                                <li class="nav-item position-relative" style="width: 33%">
                                    <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [20, 21, 29, 31, 31, 24, 25, 26, 27, 34])) active @else disabled @endif pr-1" id="bayaran-tab" data-toggle="tab" aria-controls="#bayaran" href="#bayaran">
                                        <span class="nav-text">Bayaran</span>
                                        <i hidden id="exclamation-bayaran" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content mt-5" id="myTabContent">
                                <!-- Maklumat Permohonan Acara::Start -->
                                <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [19])) active @endif" id="maklumatPermohonanAcara" role="tabpanel" aria-labelledby="maklumatPermohonanAcara-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item position-relative" id="butiranPemohonNavItem">
                                            <a class="nav-link active pr-1" id="butiranPemohon-tab" data-toggle="tab" href="#butiranPemohon">
                                                <span class="nav-text">Butiran Pemohon</span>
                                                <i hidden id="exclamation-butiranPemohon" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="butiranAcaraNavItem">
                                            <a class="nav-link pr-1" id="butiranAcara-tab" data-toggle="tab" href="#butiranAcara" aria-controls="butiranAcara">
                                                <span class="nav-text">Butiran Acara</span>
                                                <i hidden id="exclamation-butiranAcara" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        <input type="text" name="initialApplicationStatus" id="initialApplicationStatus" hidden value="{{$data['booking_person']->applicant_status}}">
                                        @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2 || $data['main_booking']->fk_lkp_status == 28)
                                            <li class="nav-item position-relative" id="maklumatPenganjurUtamaNavItem">
                                                <a class="nav-link pr-1" id="maklumatPenganjurUtama-tab" data-toggle="tab" href="#maklumatPenganjurUtama" aria-controls="maklumatPenganjurUtama">
                                                    <span class="nav-text">Maklumat Penganjur Utama</span>
                                                    <i hidden id="exclamation-maklumatPenganjurUtama" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item position-relative" id="muatNaikDokumenNavItem">
                                            <a class="nav-link pr-1" id="muatNaikDokumen-tab" data-toggle="tab" href="#muatNaikDokumen" aria-controls="muatNaikDokumen">
                                                <span class="nav-text">Muat Naik Dokumen</span>
                                                <i hidden id="exclamation-muatNaikDokumen" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        @if ($data['main_booking']->fk_lkp_status == 28)
                                            <li class="nav-item position-relative" id="perakuanPemohonNavItem">
                                                <a class="nav-link pr-1" id="perakuanPemohon-tab" data-toggle="tab" href="#perakuanPemohon" aria-controls="perakuanPemohon">
                                                    <span class="nav-text">Perakuan Pemohon</span>
                                                    <i hidden id="exclamation-perakuanPemohon" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                    @php
                                        $foundAttachment1 = false;
                                        $foundAttachment2 = false;
                                        $foundAttachment3 = false;
                                        $foundAttachment4 = false;
                                    @endphp
                                    @if($data['main_booking']->fk_lkp_status != 28)
                                        <div class="tab-content mt-5" id="myTabContent">
                                            <!-- Butiran Pemohon::Start -->
                                            <div class="tab-pane fade show active" id="butiranPemohon" role="tabpanel" aria-labelledby="butiranPemohon-tab">
                                                {{-- <form class="myForm" id="form1"> --}}
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="mb-10 font-weight-bolder text-dark bg-secondary py-3 px-8">Maklumat Am</div>
                                                        <!-- begin:No Tempahan & Status -->
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Tempahan</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['main_booking']->bmb_booking_no }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group px-8">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status</label></div>
                                                                        <div class="col-9">
                                                                            <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['status_tempahan'] }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end:No Tempahan & Status -->
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Pemohon</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Jenis Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jenis Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="jenisPemohon" class="jenisPemohon form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected>Sila Pilih</option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) selected @endif> Individu</option>
                                                                                <option value="2 "@if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 2) selected @endif> Syarikat/Organisasi</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jenis Pemohon-->
                                                                <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Pemohon (Individu / Syarikat / Organisasi) <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['booking_person']->name }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                <!--begin::Nama Pegawai untuk dihubungi-->
                                                                <div @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) hidden @endif class="form-group" id="contactPersonForm">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Pegawai untuk dihubungi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg contactPerson" name="contactPerson" placeholder="" value="@if($data['booking_person'] != null && $data['booking_person']->contact_person){{ $data['booking_person']->contact_person }}@endif"/>
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::Status Pemohon-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Status Pemohon <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="statusPemohon" class="statusPemohon form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                <option value="" disabled selected></option>
                                                                                <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 1) selected @endif> Penganjur Utama</option>
                                                                                <option value="2" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2) selected @endif> Penganjur Acara</option>
                                                                            </select>
                                                                            {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_status == 1) Penganjur Utama @else Penganjur Acara @endif" /> --}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Status Pemohon-->
                                                                <!--begin::Jawatan-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Jawatan <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="jawatan" placeholder="" value="{{ $data['booking_person']->post ?? '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Jawatan-->
                                                                <!--begin::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No Kad Pengenalan / No Pendaftaran Syarikat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="kadPengenalanPendaftaran" placeholder="" value="{{ $data['booking_person']->ic_no }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                <!--begin::Alamat-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->address }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Alamat-->
                                                                <!--begin::Poskod & Bandar-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->postcode }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->city }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Poskod & Bandar-->
                                                                <!--begin::Negeri & Negara-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                <div class="col-9">
                                                                                    <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif disabled name="negeri" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                        <option value="" disabled selected></option>
                                                                                        @foreach ($data['negeri'] as $st)
                                                                                            <option value="" @if ($data['booking_person'] != null && $data['booking_person']->fk_lkp_state == $st->id) selected @endif>{{ $st->ls_description }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->fk_lkp_state }}" />  --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                        <select required name="negara" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                            <option value="">Sila Pilih</option>
                                                                                            @foreach ($data['negara'] as $m)
                                                                                                <option value="{{ $m->id }}" @if (session('user.country') == $m->id) selected @endif>{{ ucfirst(strtolower($m->lc_description)) }}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    @else
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ Helper::negara($data['booking_person']->fk_lkp_country) }}" /> 
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Negeri & Negara-->
                                                                <!--begin::No GST-->
                                                                <div hidden class="form-group" id=""> <!-- id="nogstForm" -->
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>No. GST</label></div>
                                                                        <div class="col-9">
                                                                            <input disabled type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{$data['booking_person']->GST_no }}" /> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Nama Pegawai untuk dihubungi-->
                                                                <!--begin::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Pejabat <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{$data['booking_person']->office_no }}" /> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="noBimbit" placeholder="" value="{{$data['booking_person']->hp_no }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                <!--begin::No. Faks && Email-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Email <span class="text-danger">*</span></label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="email" placeholder="" value="{{$data['booking_person']->email }}" />
                                                                                    <div hidden class="text-danger" id="invalid-email">* Sila isi Email.</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Faks</label></div>
                                                                                <div class="col-9">
                                                                                    <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="noFaks" placeholder="" value="{{$data['booking_person']->fax_no }}" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12 d-flex justify-content-end">
                                                                        <button type="button" class="btn btn-danger font-weight-bold mb-7">Batal Permohonan</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- </form> --}}
                                            </div>
                                            <!-- Butiran Pemohon::End -->
                                            <!-- Butiran Acara::Start -->
                                            <div class="tab-pane fade show" id="butiranAcara" role="tabpanel" aria-labelledby="butiranAcara-tab">
                                                {{-- <form class="myForm" id="form2"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Acara</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Nama Acara-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->event_name  }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                        <!--end::Nama Acara-->
                                                                    <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Dari <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->event_date) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->event_date_end) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <!--begin::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Penganjuran Dari <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Penganjuran Hingga <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time_to)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                    <!--begin::Lokasi Acara-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Lokasi Acara </label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::locationspa($data['booking_event']->fk_spa_location)  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Lokasi Acara-->
                                                                    <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jumlah Peserta / Pengunjung <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->visitor_amount  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Jumlah Peserta/ Pengunjung-->
                                                                    <!--begin::Peringkat Penganjuran-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->event_name  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Peringkat Penganjuran-->
                                                                    <!--begin::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Masuk Tapak </label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->enter_date) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_time)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                    <!--begin::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->exit_date) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_time)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                    <!--begin::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Melibatkan penggunaan Drone atau Alat Kawalan Jauh (terbang)</label></div>
                                                                            <div class="col-9">
                                                                                <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="libatPenggunaanDrone" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5">
                                                                                    <option value=""> </option>
                                                                                    <option value="ya" @if ($data['booking_event'] != null && $data['booking_event']->drone == 1) selected @endif> Ya</option>
                                                                                    <option value="tidak" @if ($data['booking_event'] != null && $data['booking_event']->drone == 0) selected @endif> Tidak</option>
                                                                                </select>
                                                                                {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->drone  }}" /> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                    <!--begin::Nama Tetamu kehormat Utama-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Tetamu kehormat Utama (jika ada)</label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->vip_name  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Tetamu kehormat Utama-->
                                                                    <!--begin::Maklumat Tambahan-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Maklumat Tambahan </label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->remark  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Maklumat Tambahan-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {{-- </form> --}}
                                            </div>
                                            <!-- Butiran Acara::End -->
                                            <!-- Butiran Penganjur Utama::Start -->
                                            <input type="text" id="applicant_type" value="{{$data['booking_person']->applicant_type}}" hidden>
                                            {{-- @if ($data['booking_event'] != null && $data['booking_person']->applicant_type != 1) --}}
                                            <div class="tab-pane fade show" id="maklumatPenganjurUtama" role="tabpanel" aria-labelledby="maklumatPenganjurUtama-tab">
                                                {{-- <form class="myForm" id="form3"> --}}
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Penganjur Utama</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Nama Agensi <span class="text-danger">*</span></label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="namaAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->agency_name  }} @endif" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-3 my-auto"><label>Alamat</label></div>
                                                                        <div class="col-9">
                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="alamatPenganjur" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->address  }} @endif" />
                                                                        </div>
                                                                    <div class="col-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Input-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Poskod</label></div>
                                                                                <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="poskod" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->postcode  }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Bandar</label></div>
                                                                                <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="bandar" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->city  }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                <div class="col-9">
                                                                                    <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif disabled name="negeriAgensi" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                        <option value="" disabled selected></option>
                                                                                        @foreach ($data['negeri'] as $item)
                                                                                            <option value=""@if($data['booking_organiser'] != null && $item->id == $data['booking_organiser']->fk_lkp_state) selected @endif>{{$item->ls_description}}</option>
                                                                                        @endforeach
                                                                                        {{-- <option value="Johor">Johor</option>
                                                                                        <option value="Kedah">Kedah</option>
                                                                                        <option value="Kelantan">Kelantan</option>
                                                                                        <option value="Melaka">Melaka</option>
                                                                                        <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                                                        <option value="Pahang">Pahang</option>
                                                                                        <option value="Pulau Pinang">Pulau Pinang</option>
                                                                                        <option value="Perak">Perak</option>
                                                                                        <option value="Perlis">Perlis</option>
                                                                                        <option value="Sabah">Sabah</option>
                                                                                        <option value="Sarawak">Sarawak</option>
                                                                                        <option value="Selangor">Selangor</option>
                                                                                        <option value="Terengganu">Terengganu</option>
                                                                                        <option value="Wilayah Persekutuan">Wilayah Persekutuan</option> --}}
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Negara</label></div>
                                                                                <div class="col-9">
                                                                                    {{-- <option value="" disabled selected></option> --}}
                                                                                    <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif disabled name="negaraAgensi" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                        @foreach ($data['negara'] as $item)
                                                                                            <option value=""@if($data['booking_organiser'] != null && $item->id == $data['booking_organiser']->fk_lkp_country) selected @endif>{{$item->lc_description}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="negaraAgensi" placeholder="" value="" /> --}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>No. Telefon</label></div>
                                                                                <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="telBimbit" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->telephone_no  }} @endif" /></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- </form> --}}
                                            </div>
                                            {{-- @endif --}}
                                            <!-- Butiran Penganjur Utama::End -->
                                            <!-- Muat Naik Dokumen::Start -->
                                            <div class="tab-pane fade show" id="muatNaikDokumen" role="tabpanel" aria-labelledby="muatNaikDokumen-tab">
                                                {{-- <form class="myForm" id="form4"> --}}
                                                <div class="card card-custom">
                                                    <div class="card-body p-0">
                                                        <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                        <div class="card mx-7">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Muat Naik Dokumen</div>
                                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 3%">Bil.</th>
                                                                                <th style="width: 30%">Senarai Semak</th>
                                                                                <th style="width: 10%">Muat Naik</th>
                                                                                {{-- <th style="width: 20%">Tindakan</th> --}}
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">1</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div>
                                                                                    </div>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 1)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment1 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment1)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">2</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <div class="input-group">
                                                                                        <input  name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                        <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                        <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div>
                                                                                    </div>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 2)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment2 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment2)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">3</th>
                                                                                <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 3)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment3 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach

                                                                                @if (!$foundAttachment3)
                                                                                    <th></th>
                                                                                @endif
                                                                                {{-- <th class="font-size-sm font-weight-normal"></th> --}}
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">4</th>
                                                                                <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                {{-- <th class="font-size-sm font-weight-normal">
                                                                                    <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                </th> --}}
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 4)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                        @php
                                                                                            $foundAttachment4 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment4)
                                                                                    <th></th>
                                                                                @endif
                                                                            </tr>
                                                                        </tbody>
                                                                        {{-- <tbody>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">1</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                    <th class="font-size-sm font-weight-normal">
                                                                                        <div class="input-group">
                                                                                            <input  name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                            <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                            <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div>
                                                                                        </div>
                                                                                    </th>
                                                                                    <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th>
                                                                                @endif
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">2</th>
                                                                                <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                    <th class="font-size-sm font-weight-normal">
                                                                                        <div class="input-group">
                                                                                            <input  name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                            <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                            <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div>
                                                                                        </div>
                                                                                    </th>
                                                                                @endif
                                                                                <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">3</th>
                                                                                <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                    <th class="font-size-sm font-weight-normal">
                                                                                        <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                    </th>
                                                                                @endif
                                                                                <th class="font-size-sm font-weight-normal"></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">4</th>
                                                                                <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                    <th class="font-size-sm font-weight-normal">
                                                                                        <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                    </th>
                                                                                @endif
                                                                                <th class="font-size-sm font-weight-normal"></th>
                                                                            </tr>
                                                                        </tbody> --}}
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- </form> --}}
                                            </div>
                                            <!-- Muat Naik Dokumen::End -->              
                                            </div>
                                    @else
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="tab-content mt-5" id="myTabContent">
                                                <!-- Butiran Pemohon::Start -->
                                                <div class="tab-pane fade show active" id="butiranPemohon" role="tabpanel" aria-labelledby="butiranPemohon-tab">
                                                    {{-- <form class="myForm" id="form1"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="mb-10 font-weight-bolder text-dark bg-secondary py-3 px-8">Maklumat Am</div>
                                                            <!-- begin:No Tempahan & Status -->
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group px-8">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>No Tempahan</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['main_booking']->bmb_booking_no }}" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group px-8">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Status</label></div>
                                                                            <div class="col-9">
                                                                                <input type="text" readonly class="form-control form-control-solid form-control-lg" name="" placeholder="" value="{{ $data['status_tempahan'] }}" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end:No Tempahan & Status -->
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Pemohon</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Jenis Pemohon-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jenis Pemohon <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="jenisPemohon" class="jenisPemohon form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                    <option value="" disabled selected>Sila Pilih</option>
                                                                                    <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) selected @endif> Individu</option>
                                                                                    <option value="2 "@if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 2) selected @endif> Syarikat/Organisasi</option>
                                                                                </select>
                                                                                {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Jenis Pemohon-->
                                                                    <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Pemohon (Individu / Syarikat / Organisasi) <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['booking_person']->name }}" />
                                                                                {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaPemohon" placeholder="" value="{{ $data['spa_booking_person']->namaPemohon }}" /> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                                    <!--begin::Nama Pegawai untuk dihubungi-->
                                                                    <div @if ($data['booking_person'] != null && $data['booking_person']->applicant_type == 1) hidden @endif class="form-group" id="contactPersonForm">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Pegawai untuk dihubungi <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg contactPerson" name="contactPerson" placeholder="" value="@if($data['booking_person'] != null && $data['booking_person']->contact_person){{ $data['booking_person']->contact_person }}@endif"/>
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Pegawai untuk dihubungi-->
                                                                    <!--begin::Status Pemohon-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Status Pemohon <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="statusPemohon" class="statusPemohon form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                    <option value="" disabled selected></option>
                                                                                    <option value="1" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 1) selected @endif> Penganjur Utama</option>
                                                                                    <option value="2" @if ($data['booking_person'] != null && $data['booking_person']->applicant_status == 2) selected @endif> Penganjur Acara</option>
                                                                                </select>
                                                                                {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_status == 1) Penganjur Utama @else Penganjur Acara @endif" /> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Status Pemohon-->
                                                                    <!--begin::Jawatan-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jawatan <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="jawatan" placeholder="" value="{{ $data['booking_person']->post ?? '' }}" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Jawatan-->
                                                                    <!--begin::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>No Kad Pengenalan / No Pendaftaran Syarikat <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="kadPengenalanPendaftaran" placeholder="" value="{{ $data['booking_person']->ic_no }}" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::No Kad Pengenalan / No Pendaftaran Syarikat-->
                                                                    <!--begin::Alamat-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Alamat <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->address }}" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Alamat-->
                                                                    <!--begin::Poskod & Bandar-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Poskod <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="poskod" placeholder="" value="{{ $data['booking_person']->postcode }}" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Bandar <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="bandar" placeholder="" value="{{ $data['booking_person']->city }}" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Poskod & Bandar-->
                                                                    <!--begin::Negeri & Negara-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                    <div class="col-9">
                                                                                        <select name="negeri" @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5" >
                                                                                            @foreach ($data['negeri'] as $st)
                                                                                                <option value="{{$st->id}}" @if ($data['booking_person'] != null && $data['booking_person']->fk_lkp_state == $st->id) selected @endif>{{ $st->ls_description }}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                        {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="alamat" placeholder="" value="{{ $data['booking_person']->fk_lkp_state }}" />  --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negara <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                            <select  name="negara" class="form-control form-control-lg mt-3" id="kt_select2_5" >
                                                                                                <option value="">Sila Pilih</option>
                                                                                                @foreach ($data['negara'] as $m)
                                                                                                    <option value="{{ $m->id }}"@if ($data['booking_person'] != null && $data['booking_person']->fk_lkp_country == $m->id) selected @endif>{{ ucfirst(strtolower($m->lc_description)) }}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        @else
                                                                                            <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="alamat" placeholder="" value="{{ Helper::negara($data['booking_person']->fk_lkp_country) }}" /> 
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Negeri & Negara-->
                                                                    <!--begin::No GST-->
                                                                    <div hidden class="form-group" id=""> <!-- id="nogstForm" -->
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>No. GST</label></div>
                                                                            <div class="col-9">
                                                                                <input disabled type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="nogst" placeholder="" value="{{$data['booking_person']->GST_no }}" /> 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Pegawai untuk dihubungi-->
                                                                    <!--begin::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>No. Telefon Pejabat <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="noPejabat" placeholder="" value="{{$data['booking_person']->office_no }}" /> 
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="noBimbit" placeholder="" value="{{$data['booking_person']->hp_no }}" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::No. Telefon Pejabat && No. Telefon Bimbit-->
                                                                    <!--begin::No. Faks && Email-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Email <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="email" placeholder="" value="{{$data['booking_person']->email }}" />
                                                                                        <div hidden class="text-danger" id="invalid-email">* Sila isi Email.</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>No. Faks</label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="noFaks" placeholder="" value="{{$data['booking_person']->fax_no }}" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12 d-flex justify-content-end">
                                                                            <button type="button" class="btn btn-danger font-weight-bold mb-7">Batal Permohonan</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- </form> --}}
                                                </div>
                                                <!-- Butiran Pemohon::End -->
                                                <!-- Butiran Acara::Start -->
                                                <div class="tab-pane fade show" id="butiranAcara" role="tabpanel" aria-labelledby="butiranAcara-tab">
                                                    {{-- <form class="myForm" id="form2"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Butiran Acara</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Nama Acara-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->event_name  }}" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                        <!--end::Nama Acara-->
                                                                    <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Dari <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhAcaraDari" readonly="readonly" value="{{ Helper::date_format($data['booking_event']->event_date) }}" />
                                                                                        <input type="text" hidden class="form-control form-control-solid form-control-lg tarikhAcaraDari" id="kt_datepicker_1" name="tarikhAcaraDari" value="{{ Helper::date_format($data['booking_event']->event_date) }}" />
                                                                                        <div hidden class="text-danger" id="invalid-tarikhAcaraDari">* Sila pilih Tarikh Acara Bermula.</div>
                                                                                        {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_date)->format('d M Y') }} @endif" /> --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input disabled placeholder="Pilih Tarikh Acara Dari.." type="text" class="form-control form-control-solid tarikhAcaraHingga" readonly="readonly" value="{{ Helper::date_format($data['booking_event']->event_date_end) }}" />
                                                                                        <input hidden placeholder="Pilih Tarikh Acara Dari.." type="text" class="form-control form-control-solid tarikhAcaraHingga" id="kt_datepicker_1" name="tarikhAcaraHingga" readonly="readonly" value="{{ Helper::date_format($data['booking_event']->event_date_end) }}" />
                                                                                        <div hidden class="text-danger" id="invalid-tarikhAcaraHingga">* Sila pilih Tarikh Acara Bermula.</div>
                                                                                        {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_date)->format('d M Y') }} @endif" /> --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <!--begin::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Penganjuran Dari <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input class="form-control" id="kt_timepicker_1" readonly="" name="masaPenganjuranDari" value="{{ $data['booking_event']->event_time }}" type="text" />
                                                                                        <div hidden class="text-danger" id="invalid-masaPenganjuranDari">* Sila pilih Masa Penganjuran Dari.</div>
                                                                                        {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time)->format('H:i') }} @endif" /> --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Penganjuran Hingga <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaPenganjuranHingga" value="{{ $data['booking_event']->event_time_to }}" type="text" />
                                                                                        <div hidden class="text-danger" id="invalid-masaPenganjuranHingga">* Sila pilih Masa Penganjuran Hingga.</div>
                                                                                        {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->event_time_to)->format('H:i') }} @endif" /> --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Masa Penganjuran Dari|Masa Penganjuran Hingga-->
                                                                    <!--begin::Lokasi Acara-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Lokasi Acara </label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="lokasiAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::locationspa($data['booking_event']->fk_spa_location)  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Lokasi Acara-->
                                                                    <!--begin::Jumlah Peserta/ Pengunjung-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Jumlah Peserta / Pengunjung <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input required type="text" class="form-control form-control-lg" name="jumlahPesertaPengunjung" placeholder="" value="{{ $data['booking_event']->visitor_amount }}" />
                                                                                <div hidden class="text-danger" id="invalid-jumlahPesertaPengunjung">* Sila pilih Jumlah Peserta / Pengunjung.</div>
                                                                                {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaAcara" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->visitor_amount  }} @endif" /> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Jumlah Peserta/ Pengunjung-->
                                                                    <!--begin::Peringkat Penganjuran-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="peringkatPenganjuran" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->event_name  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Peringkat Penganjuran-->
                                                                    <!--begin::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" id="kt_datepicker_1" name="tarikhMasukTapak" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->enter_date) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="masaMasukTapak" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->enter_time)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Masuk Tapak|Masa Masuk Tapak-->
                                                                    <!--begin::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="tarikhKeluarTapak" placeholder="" value="@if ($data['booking_event'] != null) {{ Helper::date_format($data['booking_event']->exit_date) }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                                                                    <div class="col-9">
                                                                                        <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="masaKeluarTapak" placeholder="" value="@if ($data['booking_event'] != null) {{ \Carbon\Carbon::parse($data['booking_event']->exit_time)->format('H:i') }} @endif" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Keluar Tapak|Masa Keluar Tapak-->
                                                                    <!--begin::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Melibatkan penggunaan Drone atau Alat Kawalan Jauh (terbang)</label></div>
                                                                            <div class="col-9">
                                                                                <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="libatPenggunaanDrone" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg mt-3" id="kt_select2_5">
                                                                                    <option value=""> </option>
                                                                                    <option value="ya" @if ($data['booking_event'] != null && $data['booking_event']->drone == 1) selected @endif> Ya</option>
                                                                                    <option value="tidak" @if ($data['booking_event'] != null && $data['booking_event']->drone == 0) selected @endif> Tidak</option>
                                                                                </select>
                                                                                {{-- <input type="text" class="form-control form-control-solid form-control-lg" name="namaAcara" placeholder="" value="{{ $data['booking_event']->drone  }}" /> --}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Melibatkan penggunaan Drone atau Alat Kawalan Jauh-->
                                                                    <!--begin::Nama Tetamu kehormat Utama-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Tetamu kehormat Utama (jika ada)</label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="namaTetamuKehormat" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->vip_name  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Nama Tetamu kehormat Utama-->
                                                                    <!--begin::Maklumat Tambahan-->
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Maklumat Tambahan </label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control @if ($data['main_booking']->fk_lkp_status != 28) form-control-solid @endif form-control-lg" name="maklumatTambahan" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_event']->remark  }} @endif" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Maklumat Tambahan-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- </form> --}}
                                                </div>
                                                <!-- Butiran Acara::End -->
                                                <!-- Butiran Penganjur Utama::Start -->
                                                <input type="text" id="applicant_type" value="{{$data['booking_person']->applicant_type}}" hidden>
                                                {{-- @if ($data['booking_event'] != null && $data['booking_person']->applicant_type != 1) --}}
                                                <div class="tab-pane fade show" id="maklumatPenganjurUtama" role="tabpanel" aria-labelledby="maklumatPenganjurUtama-tab">
                                                    {{-- <form class="myForm" id="form3"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Maklumat Penganjur Utama</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Nama Agensi <span class="text-danger">*</span></label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-lg" name="namaAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->agency_name  }} @endif" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-3 my-auto"><label>Alamat</label></div>
                                                                            <div class="col-9">
                                                                                <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-lg" name="alamatAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->address  }} @endif" />
                                                                            </div>
                                                                        <div class="col-3"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Poskod</label></div>
                                                                                    <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-lg" name="poskodAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->postcode  }} @endif" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Bandar</label></div>
                                                                                    <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-lg" name="bandarAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->city  }} @endif" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negeri</label></div>
                                                                                    <div class="col-9">
                                                                                        <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="negeriAgensi" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                            <option value="" disabled selected></option>
                                                                                            @foreach ($data['negeri'] as $item)
                                                                                                <option value="{{$item->id}}"@if($data['booking_organiser'] != null && $item->id == $data['booking_organiser']->fk_lkp_state) selected @endif>{{$item->ls_description}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Negara</label></div>
                                                                                    <div class="col-9">
                                                                                        <select @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif name="negaraAgensi" class="form-control form-control-lg font-weight-bold mt-3">
                                                                                            @foreach ($data['negara'] as $item)
                                                                                                <option value="{{$item->id}}"@if($data['booking_organiser'] != null && $item->id == $data['booking_organiser']->fk_lkp_country) selected @endif>{{$item->lc_description}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                        {{-- <input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-solid form-control-lg" name="negaraAgensi" placeholder="" value="" /> --}}
                                                                                        {{-- <select name="" id=""></select> --}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>No. Telefon</label></div>
                                                                                    <div class="col-9"><input @if ($data['main_booking']->fk_lkp_status != 28) disabled @endif type="text" class="form-control form-control-lg" name="telBimbitAgensi" placeholder="" value="@if ($data['booking_organiser'] != null) {{ $data['booking_organiser']->telephone_no  }} @endif" /></div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- </form> --}}
                                                </div>
                                                {{-- @endif --}}
                                                <!-- Butiran Penganjur Utama::End -->
                                                <!-- Muat Naik Dokumen::Start -->
                                                <div class="tab-pane fade show" id="muatNaikDokumen" role="tabpanel" aria-labelledby="muatNaikDokumen-tab">
                                                    {{-- <form class="myForm" id="form4"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                            <div class="card mx-7">
                                                                <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Muat Naik Dokumen</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered" id="kt_datatable_2">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style="width: 3%">Bil.</th>
                                                                                    <th style="width: 30%">Senarai Semak</th>
                                                                                    <th style="width: 10%">Muat Naik</th>
                                                                                    <th style="width: 20%">Tindakan</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">1</th>
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Iringan (Cover letter) Permohonan Daripada Penganjur Acara</th>
                                                                                    @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <div class="input-group">
                                                                                                <input  name="suratIringan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="display:none;">
                                                                                                <label for="fileUpload1" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                                {{-- <div class="text-danger" id="invalid-suratIringan">* Sila muat naik Surat Iringan.</div> --}}
                                                                                            </div>
                                                                                        </th>
                                                                                        @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                            @if ($attachment->fk_lkp_spa_filename === 1)
                                                                                                <th class="font-size-sm font-weight-normal">
                                                                                                    <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                                </th>
                                                                                                @php
                                                                                                    $foundAttachment1 = true;
                                                                                                @endphp
                                                                                            @endif
                                                                                        @endforeach
                                                                                        @if (!$foundAttachment1)
                                                                                            <th></th>
                                                                                        @endif
                                                                                        {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay1"></th> --}}
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">2</th>
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan)</th>
                                                                                    @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <div class="input-group">
                                                                                                <input  name="kertasCadangan" type="file" class="form-control" id="fileUpload2" accept=".pdf" style="display:none;">
                                                                                                <label for="fileUpload2" class="btn btn-primary btn-file-upload">Muat Naik</label>
                                                                                                {{-- <div class="text-danger" id="invalid-kertasCadangan">* Sila muat naik Kertas Cadangan Penganjuran Acara.</div> --}}
                                                                                            </div>
                                                                                        </th>
                                                                                    @endif
                                                                                    {{-- <th class="font-size-sm font-weight-normal" id="fileDisplay2"></th> --}}
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 2)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment2 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment2)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">3</th>
                                                                                    <th class="font-size-sm font-weight-normal">Surat Pelantikan Pengurus Acara (jika berkaitan)</th>
                                                                                    @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                        </th>
                                                                                    @endif
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 3)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment3 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
    
                                                                                    @if (!$foundAttachment3)
                                                                                        <th></th>
                                                                                    @endif
                                                                                    {{-- <th class="font-size-sm font-weight-normal"></th> --}}
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">4</th>
                                                                                    <th class="font-size-sm font-weight-normal">Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya)</th>
                                                                                    @if ($data['main_booking']->fk_lkp_status == 28)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <button type="button" class="btn btn-primary ">Muat Naik</button>
                                                                                        </th>
                                                                                    @endif
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 4)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment4 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment4)
                                                                                        <th></th>
                                                                                    @endif
                                                                                    {{-- <th class="font-size-sm font-weight-normal"></th> --}}
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- </form> --}}
                                                </div>
                                                <!-- Muat Naik Dokumen::End -->  
                                                <!-- Perakuan Pemohon::Start -->
                                                <div class="tab-pane fade" id="perakuanPemohon" role="tabpanel" aria-labelledby="perakuanPemohon-tab">
                                                    {{-- <form class="myForm" id="form5"> --}}
                                                    <div class="card card-custom">
                                                        <div class="card-body p-0">
                                                            <div class="card mx-7">
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <!--begin::Input-->
                                                                    <div class="form-group py-5 my-auto">
                                                                        <div class="row">
                                                                            <div class="col-12 d-flex justify-content-center align-items-center">
                                                                                <label class="">
                                                                                    <input type="checkbox" id="perakuanCheckbox" name="perakuanPemohon"/>
                                                                                    <span class="px-5">Dengan ini saya mengesahkan maklumat yang diberikan adalah benar.</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>
                                                                <div class="card mx-7" id="rumusanCard" style="display: none;">
                                                                    <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Rumusan Permohonan</div>
                                                                    <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                        <!--begin::Nama Pemohon-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Nama Pemohon</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohonPerakuan" placeholder="" value="{{ session('user.name' )}}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Nama Pemohon-->
                                                                        <!--begin::Nama Acara-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Nama Acara</label></div>
                                                                                <div class="col-9">
                                                                                    <input disabled type="text" class="form-control form-control-lg" name="namaAcaraPerakuan" placeholder="" value="{{ $data['booking_event']->event_name  }}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Nama Acara-->
                                                                        <!--begin::Tarikh Acara Dari & Tarikh Acara Hingga-->
                                                                        <div class="row">
                                                                            <div class="col-xl-6">
                                                                                <div class="form-group">
                                                                                    <div class="row">
                                                                                        <div class="col-3 my-auto"><label>Tarikh Acara Dari</label></div>
                                                                                        <div class="col-9"><input disabled type="text" class="tarikhAcaraDariPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraDariPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->event_date) }}" /></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-6">
                                                                                <div class="form-group">
                                                                                    <div class="row">
                                                                                        <div class="col-3 my-auto"><label>Tarikh Acara Hingga</label></div>
                                                                                        <div class="col-9"><input disabled type="text" class="tarikhAcaraHinggaPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraHinggaPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->event_date_end) }}" /></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Tarikh Acara Dari & Tarikh Acara Hingga-->
                                                                    <!--begin::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                    <div class="row">
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Masuk Tapak</label></div>
                                                                                    <div class="col-9">
                                                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhMasukTapak" id="kt_datepicker_1" name="tarikhMasukTapak" readonly="readonly" value=" {{ \Carbon\Carbon::parse($data['booking_event']->event_time)->format('H:i') }}" />
                                                                                        <div hidden class="text-danger" id="invalid-tarikhAcaraDari"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                        <div class="col-xl-6">
                                                                            <!--begin::Input-->
                                                                            <div class="form-group">
                                                                                <div class="row">
                                                                                    <div class="col-3 my-auto"><label>Tarikh Keluar Tapak</label></div>
                                                                                    <div class="col-9">
                                                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg tarikhKeluarTapak" id="kt_datepicker_1" name="tarikhKeluarTapak" readonly="readonly" value=" {{ \Carbon\Carbon::parse($data['booking_event']->event_time_to)->format('H:i') }}" />
                                                                                        <div hidden class="text-danger" id="invalid-tarikhAcaraHingga"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--end::Input-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Tarikh Acara Dari|Tarikh Acara Hingga-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Lokasi</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" readonly class="form-control form-control-solid form-control-lg" name="lokasiPerakuan" placeholder="" value="{{ Helper::locationspa($data['booking_event']->fk_spa_location)  }}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Jumlah Hari Penggunaan Tapak</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" readonly class="form-control form-control-solid form-control-lg" name="jumlahHariPenggunaanPerakuan" id="jumlahHariPenggunaanPerakuan" value="{{ $data['booking_event']->total_day }}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Sewa Tapak Sehari (RM)</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" readonly class="form-control form-control-solid form-control-lg" name="sewaTapakSehariPerakuan" id="sewaTapakSehariPerakuan" placeholder="" value="{{Helper::locationspa_rent_perday($data['booking_event']->fk_spa_location)}}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>GST (0%)</label></div>
                                                                                <div class="col-9">
                                                                                    <input readonly type="text" class="form-control form-control-solid form-control-lg" name="gstPerakuan" placeholder="" value="0" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Deposit (RM)</label></div>
                                                                                <div class="col-9">
                                                                                    <input type="text" readonly class="form-control form-control-solid form-control-lg" name="depositPerakuan" placeholder="" value="{{Helper::locationspa_depoRate($data['booking_event']->fk_spa_location)}}" />
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-3 my-auto"><label>Jumlah (RM)</label></div>
                                                                                <div class="col-9">
                                                                                    {{-- {{ number_format($data['total'], 2, '.', ',') }} --}}
                                                                                    <input type="text" readonly class="form-control form-control-solid form-control-lg" name="jumlahPerakuan" placeholder="" id="jumlahPerakuan" value="{{ Helper::moneyhelper($data['booking_event']->total_day * Helper::locationspa_rent_perday($data['booking_event']->fk_spa_location)) }}" />
                                                                                    <div class="" id="">* Jumlah tidak termasuk deposit & lain-lain caj.</div>
                                                                                </div>
                                                                            <div class="col-3"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                        <div class="row">
                                                                            <div class="col-12 d-flex justify-content-end">
                                                                                <button type="submit" id="" class="btn btn-light-primary font-weight-bold mb-7">Hantar</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- </form> --}}
                                                </div>            
                                                <!-- Perakuan Pemohon::End -->               
                                            </div>
                                        </form>
                                    @endif
                                </div>
                                <!-- Maklumat Permohonan Acara::End -->
                                <!-- Tindakan::Start -->
                                <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [22, 23, 28, 30])) active @endif" id="tindakan" role="tabpanel" aria-labelledby="tindakan-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <li class="nav-item position-relative">
                                                <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [19, 28, 29])) active @endif pr-1" id="tindakanSemakan-tab" data-toggle="tab" href="#tindakanSemakan">
                                                    <span class="nav-text">Semakan</span>
                                                    <i hidden id="exclamation-tindakanSemakan" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [22, 23, 24, 25, 26, 27])) active @elseif(in_array($data['main_booking']->fk_lkp_status, [19, 28, 29])) disabled @endif pr-1" id="tindakanKeputusanMesyuarat-tab" data-toggle="tab" href="#tindakanKeputusanMesyuarat" aria-controls="tindakanKeputusanMesyuarat">
                                                <span class="nav-text">Keputusan Mesyuarat</span>
                                                <i hidden id="exclamation-tindakanKeputusanMesyuarat" style="position: absolute; top: 0%; right: -10%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                            </a>
                                        </li>
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <li class="nav-item position-relative">
                                                <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [30])) active @elseif(in_array($data['main_booking']->fk_lkp_status, [19, 22, 28, 29])) disabled @endif pr-1" id="tindakanMaklumatBayaran-tab" data-toggle="tab" href="#tindakanMaklumatBayaran" aria-controls="tindakanMaklumatBayaran">
                                                    <span class="nav-text">Maklumat Bayaran</span>
                                                    <i hidden id="exclamation-tindakanMaklumatBayaran" style="position: absolute; top: 0%; right: -5%; transform: translateY(-50%);" class="fas fa-exclamation-circle text-warning mr-5"></i>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Tindakan-Semakan::Start -->
                                        @if (!in_array($data['main_booking']->fk_lkp_status, [24, 25, 26, 27]))
                                            <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [19, 20, 28, 29])) active @endif" id="tindakanSemakan" role="tabpanel" aria-labelledby="tindakanSemakan-tab">
                                                <input hidden type="text" name="tab" id="" value="tindakanSemakan">
                                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                    <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Semakan Permohonan</div>
                                                    <div class="form-group">
                                                        <div class="row d-flex justify-content-center">
                                                            <div class="col-2 my-auto"><label>Status Semakan Permohonan</label></div>
                                                            <div class="col-6">
                                                                <input hidden type="text" class="form-control form-control-lg" name="fk_user" placeholder="" value="@if ($data['booking_event'] != null) {{ $data['booking_person']->id }} @endif"/>
                                                                <select disabled @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 5) disabled @endif name="statusSemakanPermohonan" class="peringkatPenganjuran form-control form-control-lg font-weight-bold mt-3" >
                                                                    <option value=""> </option>
                                                                    <option value="1" @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 5) selected @endif> Diterima</option>
                                                                    <option value="2" @if ($data['spa_booking_review'] && $data['main_booking']->fk_lkp_status == 28) selected @endif> Tidak Lengkap</option>
                                                                    <option value="3" @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 17) selected @endif> Gagal/ Ditolak</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                    <div @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review == 17) hidden @endif id="peringkatPenganjuranDiterima">
                                                        <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Pembentangan</div>
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Tempat <span class="text-danger">*</span></label></div>
                                                                <div class="col-9">
                                                                    <input disabled type="text" class="form-control form-control-lg" name="location" id="peringkatPenganjuranDiterimaTempat" autocomplete="off" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->location }} @endif"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Tarikh <span class="text-danger">*</span></label></div>
                                                                <div class="col-9">
                                                                    <input disabled type="text" class="form-control form-control-lg" id="kt_daterangepicker_5" name="tarikhSemakan" autocomplete="off" value="@if ($data['spa_booking_review']) {{ Helper::date_format($data['spa_booking_review']->date) }} @endif" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Nama Pegawai untuk dihubungi-->
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Masa</label></div>
                                                                <div class="col-9">
                                                                    <input disabled class="form-control" id="kt_timepicker_1" readonly="readonly" name="masaSemakan" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->time }} @else '' @endif" type="text"/>
                                                                </div>
                                                            <div class="col-3"></div>
                                                            </div>
                                                        </div>
                                                        <!--end::Nama Pegawai untuk dihubungi-->
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Nama Pengerusi Mesyuarat</label></div>
                                                                <div class="col-9">
                                                                    <input disabled type="text" class="form-control form-control-lg" name="namaPengerusiMesyuarat" placeholder="" value="@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->meeting_chairman }} @endif" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Maklumat Tambahan-->
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3"><label>Maklumat Tambahan</label></div>
                                                                <div class="col-9">
                                                                    <textarea disabled name="maklumatTambahan" class="form-control form-control-lg contactPerson" id="" cols="30" rows="10">@if ($data['spa_booking_review']) {{ $data['spa_booking_review']->remark }} @endif</textarea>
                                                                </div>
                                                            <div class="col-3"></div>
                                                            </div>
                                                        </div>
                                                        <!--end::Maklumat Tambahan-->
                                                    </div>
                                                    <div hidden id="peringkatPenganjuranTidakLengkap">
                                                        <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Perlu Dikemas kini</div>
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3"><label>Maklumat Perlu Dikemas kini</label></div>
                                                                <div class="col-9">
                                                                    <textarea name="" class="form-control form-control-solid form-control-lg" id="" cols="30" rows="10">{{ $data['spa_booking_review']->remark ?? '' }}</textarea>
                                                                </div>
                                                            <div class="col-3"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div @if ($data['spa_booking_review'] && $data['spa_booking_review']->fk_lkp_spa_review != 17 || $data['spa_booking_review'] == null) hidden @endif id="peringkatPenganjuranDitolak">
                                                        <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Permohonan Gagal/Ditolak <span class="text-danger">*</span></div>
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3"><label>Ulasan <span class="text-danger">*</span></label></div>
                                                                <div class="col-9">
                                                                    <textarea name="" class="form-control form-control-solid form-control-lg" id="" cols="30" rows="10">{{ $data['spa_booking_review']->remark ?? '' }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="row py-4">
                                                                <div class="col-3"></div>
                                                                <div class="col-9">
                                                                    Notifikasi Pemberitahuan Gagal/Ditolak akan dihantar melalui SMS dan email
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <!-- Tindakan-Semakan::End -->
                                        <!-- Tindakan-Keputusan Mesyuarat::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [22, 23, 24, 25, 26, 27])) active @endif" id="tindakanKeputusanMesyuarat" role="tabpanel" aria-labelledby="tindakanKeputusanMesyuarat-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <input hidden type="text" name="tab" id="" value="tindakanKeputusanMesyuarat">
                                                <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Mesyuarat JKA PPj</div>
                                                @if (in_array($data['main_booking']->fk_lkp_status, [22]))
                                                    <!--begin::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                    <div class="row">
                                                        <div class="col-xl-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-4 my-auto"><label>Tarikh Mesyuarat <span class="text-danger">*</span></label></div>
                                                                    <div class="col-8">
                                                                        <input disabled type="text" class="form-control form-control-lg tarikhAcaraDari" id="kt_datepicker_1" name="date_meeting" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-4 my-auto"><label>Bil Mesyuarat</label></div>
                                                                    <div class="col-8">
                                                                        <input disabled type="text" class="form-control form-control-lg " id="" name="meeting_no" value="" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Tarikh Mesyuarat|Bil Mesyuarat-->
                                                    <!--begin::Catatan-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-2"><label>Catatan </label></div>
                                                            <div class="col-10">
                                                                <textarea disabled name="remark" class="form-control form-control-lg" id="" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Catatan-->
                                                    <!--begin::Status Keputusan Mesyuarat-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-2 my-auto"><label>Status Keputusan Mesyuarat</label></div>
                                                            <div class="col-10">
                                                                <select disabled name="fk_lkp_status_meeting" id="statusKeputusanMsyt" class="form-control form-control-lg font-weight-bold mt-3" >
                                                                    <option value=""> Sila Pilih</option>
                                                                    <option value="1"> Pembentangan Semula</option>
                                                                    <option value="2"> Gagal/ Ditolak</option>
                                                                    <option value="3"> Lulus (Perlu Maklumat Bayaran)</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Status Keputusan Mesyuarat-->
                                                    <!--begin::Minit Mesyuarat-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-2 my-auto"><label>Minit Mesyuarat </label></div>
                                                            <div class="col-10">
                                                                {{-- <input disabled  name="minitMesyuarat" type="file" class="form-control" id="" accept=".pdf" style="display:none;"> --}}
                                                                {{-- <label for="" class="btn btn-primary btn-file-upload">Muat Naik</label> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Minit Mesyuarat-->
                                                {{-- @elseif (in_array($data['main_booking']->fk_lkp_status, [23]))  --}}
                                                    <div hidden id="pembentanganSemula">
                                                        <div class="font-weight-bolder text-dark bg-secondary px-8 py-3 mb-5">Maklumat Pembentangan Semula</div>
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Tempat <span class="text-danger">*</span></label></div>
                                                                <div class="col-9">
                                                                    <input disabled  type="text" class="form-control form-control-lg" name="location" id="pembentanganSemulaTempat" autocomplete="off" value=""/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Tarikh <span class="text-danger">*</span></label></div>
                                                                <div class="col-9">
                                                                    <input disabled type="text" class="form-control form-control-lg" id="kt_daterangepicker_4" name="pembentanganSemulaTarikh" autocomplete="off" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Nama Pegawai untuk dihubungi-->
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Masa</label></div>
                                                                <div class="col-9">
                                                                    <input disabled class="form-control" id="kt_timepicker_1" readonly="readonly" name="pembentanganSemulaMasa" value="" type="text"/>
                                                                </div>
                                                            <div class="col-3"></div>
                                                            </div>
                                                        </div>
                                                        <!--end::Nama Pegawai untuk dihubungi-->
                                                        <!--begin::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-3 my-auto"><label>Nama Pengerusi Mesyuarat</label></div>
                                                                <div class="col-9">
                                                                    <input disabled type="text" class="form-control form-control-lg" name="namaPengerusiMesyuarat" placeholder="" value="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Pemohon (Individu / Syarikat / Organisasi)-->
                                                        <!--begin::Maklumat Tambahan-->
                                                        <div class="form-group" id="contactPersonForm">
                                                            <div class="row">
                                                                <div class="col-3"><label>Maklumat Tambahan</label></div>
                                                                <div class="col-9">
                                                                    <textarea disabled name="maklumatTambahan" class="form-control form-control-lg contactPerson" id="" cols="30" rows="10"></textarea>
                                                                </div>
                                                            <div class="col-3"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else 
                                                    <!--begin::Status Keputusan Mesyuarat-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-3 d-flex justify-content-end my-auto"><label>Status Keputusan Mesyuarat</label></div>
                                                            <div class="col-9">
                                                                <select class="form-control" disabled >
                                                                    <option value=""> Sila Pilih</option>
                                                                    <option value="1"> Pembentangan Semula</option>
                                                                    <option value="2"> Gagal/ Ditolak</option>
                                                                    <option value="3" @if ($data['main_booking']->fk_lkp_status == 30) selected @endif> Lulus (Perlu Maklumat Bayaran)</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Status Keputusan Mesyuarat-->
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <table class="table table-bordered" id="kt_datatable_2">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Bil.</th>
                                                                        <th>Tarikh Mesyuarat</th>
                                                                        <th>Bil. Mesyuarat</th>
                                                                        <th>Catatan</th>
                                                                        <th>Status Keputusan Mesyuarat</th>
                                                                        <th>Minit Mesyuarat</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @php
                                                                        $i = 1;
                                                                                $foundAttachment8 = false;
                                                                    @endphp
                                                                    @if ($data['spa_meeting_result'])
                                                                        @foreach ($data['spa_meeting_result'] as $s)
                                                                            <tr class="font-size-lg">
                                                                                <td>{{ $i++ }}</td>
                                                                                <td>{{ Helper::date_format($s->date_meeting) }}</td>
                                                                                <td>{{ $s->meeting_no }}</td>
                                                                                <td>{{ $s->remark }}</td>
                                                                                <td>{{ Helper::get_status_tempahan($s->fk_lkp_status_meeting) }}</td>
                                                                                @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                    @if ($attachment->fk_lkp_spa_filename === 8)
                                                                                        <td class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </td>
                                                                                        @php
                                                                                            $foundAttachment8 = true;
                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach
                                                                                @if (!$foundAttachment8)
                                                                                    <td></td>
                                                                                @endif
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <td colspan="7" class="text-center">Tiada Data</td>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Tindakan-Keputusan Mesyuarat::End -->
                                        <!-- Tindakan-Maklumat Bayaran::Start -->
                                        <div class="tab-pane fade show @if (($data['main_booking']->fk_lkp_status) == 30 ) active @endif" id="tindakanMaklumatBayaran" role="tabpanel" aria-labelledby="tindakanMaklumatBayaran-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                {{-- <form action="{{ url('event/internal/reservation', ['id' => Crypt::encrypt($data['bookingId']), 'tab' => 'tindakanMaklumatBayaran']) }}" method="post" enctype="multipart/form-data"> --}}
                                                <form action="{{ url('event/internal/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input hidden type="text" name="tab" id="" value="tindakanMaklumatBayaran">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" id="kt_datatable_2">
                                                            <thead>
                                                                <tr>
                                                                    <th>Bil.</th>
                                                                    <th style="width: 30%">Butiran</th>
                                                                    <th>Kadar</th>
                                                                    <th>Kuantiti</th>
                                                                    <th>Dikecualikan</th>
                                                                    <th>Diskaun RM</th>
                                                                    <th>Diskaun %</th>
                                                                    <th>GST RM</th>
                                                                    <th>Jumlah (RM)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="font-size-sm">
                                                                <tr>
                                                                    <td>1. </td>
                                                                    <td>Sewa Tapak</td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_kadar" value="0.00" step="0.01" min="0.00" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_diskaun" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_GST" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapak_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2. </td>
                                                                    <td>Sewa Tasik</td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_diskaun" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_GST" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTasik_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3. </td>
                                                                    <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) bawah 50km</td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_diskaun" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_GST" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanBawah50_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4. </td>
                                                                    <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) atas 51km</td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_diskaun" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_GST" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="laluanAtas50_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5. </td>
                                                                    <td>Penggunaan Kawasan/ Tapak Bukan milik PPj</td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_kadar" value="0.00" step="0.01" oninput="this.value = parseFloat(this.value).toFixed(2)" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_kuantiti" value="0" /></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_diskaun" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_diskaunPer" value="0" step="0.01" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_GST" value="0.00" /></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="bukanMilikPPj_jumlah" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>6. </td>
                                                                    <td>Jumlah Amaun Tempahan</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="sewaTapakKadar" value="" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>7. </td>
                                                                    <td>GST 0%</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="jumlahGst" value="0.00" /></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>8. </td>
                                                                    <td>Caj Deposit</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="cajDeposit" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>9. </td>
                                                                    <td>Pelarasan / Pengenapan Sen</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="jumlahPelarasanSen" value="0.00" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>10. </td>
                                                                    <td>Jumlah Keseluruhan</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input disabled type="number" class="form-control form-control-sm" name="jumlahKeseluruhan" id="jumlahKeseluruhan" value="0.00" /></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="row pt-5">
                                                        <div class="col-4 my-auto"><label>Jumlah 50% dari Jumlah Amaun Tempahan dalam perkataan <span class="text-danger">*</span></label></div>
                                                        <div class="col-8">
                                                            <input disabled type="text" class="form-control form-control-sm" id="" name="jumlahDeposit" value="" />
                                                        </div>
                                                    </div>
                                                    <div class="row py-3">
                                                        <div class="col-4"><label>Kemaskini Ayat No.5 Surat Kelulusan</label></div>
                                                        <div class="col-8">
                                                            <textarea disabled name="ayatSuratKelulusan" class="form-control form-control-lg" id="" cols="30" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 d-flex justify-content-end">
                                                            <button type="button" class="btn btn-success font-weight-bold mb-7">Simpan & Preview</button>
                                                            <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2">Hantar</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- Tindakan-Maklumat Bayaran::End -->
                                    </div>
                                </div>
                                <!-- Tindakan::End -->
                                <!-- Bayaran::Start --> {{-- [24, 20, 21, 29, 28] --}}
                                <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [6, 20, 21, 29, 31, 31, 24, 25, 26, 27, 34])) active @endif" id="bayaran" role="tabpanel" aria-labelledby="bayaran-tab">
                                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <li class="nav-item position-relative">
                                            <a class="nav-link pr-1 @if(in_array($data['main_booking']->fk_lkp_status, [28, 29])) disabled @endif" id="bayaranMaklumatBayaran-tab" data-toggle="tab" href="#bayaranMaklumatBayaran">
                                                <span class="nav-text">Maklumat Bayaran</span>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [6, 24, 25])) active @elseif(in_array($data['main_booking']->fk_lkp_status, [28, 29])) disabled @endif pr-1" id="bayaranPembayaranPendahuluan-tab" data-toggle="tab" href="#bayaranPembayaranPendahuluan" aria-controls="bayaranPembayaranPendahuluan">
                                                <span class="nav-text">Pembayaran Pendahuluan</span>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative">
                                            <a class="nav-link @if (in_array($data['main_booking']->fk_lkp_status, [34, 20, 21, 26, 27, 29])) active @else disabled @endif pr-1" id="bayaranPembayaranPenuh-tab" data-toggle="tab" href="#bayaranPembayaranPenuh" aria-controls="bayaranPembayaranPenuh">
                                                <span class="nav-text">Pembayaran Penuh</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <!-- Bayaran-Maklumat Bayaran::Start -->
                                        <div class="tab-pane fade" id="bayaranMaklumatBayaran" role="tabpanel" aria-labelledby="bayaranMaklumatBayaran-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th>Bil.</th>
                                                                <th style="width: 30%">Butiran</th>
                                                                <th>Kadar</th>
                                                                <th>Kuantiti</th>
                                                                <th>Dikecualikan</th>
                                                                <th>Diskaun RM</th>
                                                                <th>Diskaun %</th>
                                                                <th>GST RM</th>
                                                                <th>Jumlah (RM)</th>
                                                            </tr>
                                                        </thead>
                                                        @php
                                                            $item10_total = $data['quotation_detail_item10'] ? $data['quotation_detail_item10']->total : 0.00;                                                            
                                                            $item22_total = $data['quotation_detail_item22'] ? $data['quotation_detail_item22']->total : 0.00;                                                            
                                                            $item11_total = $data['quotation_detail_item11'] ? $data['quotation_detail_item11']->total : 0.00;                                                            
                                                            $item12_total = $data['quotation_detail_item12'] ? $data['quotation_detail_item12']->total : 0.00;                                                            
                                                            $item13_total = $data['quotation_detail_item13'] ? $data['quotation_detail_item13']->total : 0.00;   
                                                            $sum = $item10_total + $item22_total + $item11_total + $item12_total + $item13_total;
                                                        @endphp
                                                        <tbody class="font-size-sm">
                                                            <tr>
                                                                <td>1. </td>
                                                                <td>Sewa Tapak</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_kadar" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="test" class="form-control form-control-sm text-right" name="sewaTapak_kuantiti" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ ($data['quotation_detail_item10']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_diskaun" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_diskaunPer" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ ($data['quotation_detail_item10']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_GST" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapak_jumlah" value="@if ($data['quotation_detail_item10'] != null && $data['quotation_detail_item10']->fk_lkp_spa_payitem == '10') {{ number_format($data['quotation_detail_item10']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2. </td>
                                                                <td>Sewa Tasik</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_kadar" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="test" class="form-control form-control-sm text-right" name="sewaTasik_kuantiti" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ ($data['quotation_detail_item22']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_diskaun" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_diskaunPer" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ ($data['quotation_detail_item22']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_GST" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTasik_jumlah" value="@if ($data['quotation_detail_item22'] != null && $data['quotation_detail_item22']) {{ number_format($data['quotation_detail_item22']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3. </td>
                                                                <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) bawah 50km</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_kadar" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="test" class="form-control form-control-sm text-right" name="laluanBawah50_kuantiti" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ ($data['quotation_detail_item11']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_diskaun" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_diskaunPer" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ ($data['quotation_detail_item11']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_GST" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanBawah50_jumlah" value="@if ($data['quotation_detail_item11']!= null && $data['quotation_detail_item11']) {{ number_format($data['quotation_detail_item11']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>4. </td>
                                                                <td>Penggunaan Laluan di Putrajaya (Berbasikal, Jalan Kaki, Larian dll) atas 51km</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_kadar" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="test" class="form-control form-control-sm text-right" name="laluanAtas50_kuantiti" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ ($data['quotation_detail_item12']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_diskaun" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_diskaunPer" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ ($data['quotation_detail_item12']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_GST" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="laluanAtas50_jumlah" value="@if ($data['quotation_detail_item12'] != null && $data['quotation_detail_item12']) {{ number_format($data['quotation_detail_item12']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>5. </td>
                                                                <td>Penggunaan Kawasan/ Tapak Bukan milik PPj</td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_kadar" value="@if ($data['quotation_detail_item13']&& $data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->rate, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="test" class="form-control form-control-sm text-right" name="bukanMilikPPj_kuantiti" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ ($data['quotation_detail_item13']->quantity) }} @else 0 @endif" /></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_diskaun" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->discount_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_diskaunPer" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ ($data['quotation_detail_item13']->discount_percentage) }} @else 0 @endif" step="0.01" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_GST" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->gst_rm, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="bukanMilikPPj_jumlah" value="@if ($data['quotation_detail_item13']!= null && $data['quotation_detail_item13']->fk_lkp_spa_payitem == '13') {{ number_format($data['quotation_detail_item13']->total, 2, '.', '') }} @else 0.00 @endif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>6. </td>
                                                                <td>Jumlah Amaun Tempahan</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="sewaTapakKadar" value="{{ Helper::moneyhelper($sum) }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>7. </td>
                                                                <td>GST 0%</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="number" class="form-control form-control-sm text-right" name="jumlahGst" value="0.00" /></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>8. </td>
                                                                <td>Caj Deposit</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" name="cajDeposit" value="{{ Helper::moneyhelper($data['main_booking']->bmb_deposit_rm) }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>9. </td>
                                                                <td>Pelarasan / Pengenapan Sen</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center  "><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="number" class="form-control form-control-sm text-right" name="jumlahPelarasanSen" value="0.00" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>10. </td>
                                                                <td>Jumlah Keseluruhan</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-center"><input disabled type="checkbox" class="" name="" id=""></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input disabled type="text" class="form-control form-control-sm text-right" id="jumlahKeseluruhan" value="{{ Helper::moneyhelper($data['main_booking']->bmb_subtotal + $data['main_booking']->bmb_deposit_rm) }}" />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row pt-5">
                                                    <div class="col-4 my-auto"><label>Jumlah 50% dari Jumlah Amaun Tempahan dalam perkataan <span class="text-danger">*</span></label></div>
                                                    <div class="col-8">
                                                        <input disabled type="text" class="form-control form-control-sm" id="" name="" value="{{ $data['main_booking']->bmb_total_word }}" />
                                                    </div>
                                                </div>
                                                <div class="row py-3">
                                                    <div class="col-4"><label>Kemaskini Ayat No.5 Surat Kelulusan</label></div>
                                                    <div class="col-8">
                                                        <textarea disabled name="ayatSuratKelulusan" class="form-control form-control-lg" id="" cols="30" rows="5">@if ($data['booking_event'] != null) {{ $data['booking_event']->letter_no_five }} @endif</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Bayaran-Maklumat Bayaran::End -->
                                        <!-- Bayaran-Pembayaran Pendahuluan::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [6, 24, 25])) active @endif" id="bayaranPembayaranPendahuluan" role="tabpanel" aria-labelledby="bayaranPembayaranPendahuluan-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                {{-- <form action="{{ url('event/internal/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                    @csrf --}}
                                                    <input hidden type="text" name="tab" id="" value="bayaranPendahuluan">
                                                    <div class="card card-custom">
                                                        <div class="card-body">
                                                            <div class="card p-5 mb-10">
                                                                <div class="row pb-3">
                                                                    <div class="col-12">
                                                                        <p class="font-weight-bolder">Pengesahan Kelulusan (Perlu Deposit)</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        Permohonan telah diluluskan oleh JKA PPj, Bayaran wang pendahuluan sebanyak 50% dari keseluruhan sewa tapak perlu dijelaskan 7 hari setelah surat kelulusan acara dikeluarkan.
                                                                    </div>
                                                                </div>
                                                                <div class="row pb-7 d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        Notifikasi Pemberitahuan Gagal/Ditolak akan dihantar melalui SMS dan email.
                                                                    </div>
                                                                </div>
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-10 text-center">
                                                                        <a href="{{url('event/janasurat', ['id' => $data['bookingId']])}}" id="" target="_blank" class="btn btn-success font-weight-bold mb-7">Muat Turun Surat Kelulusan, Surat Akuan Terima dan Aku Janji, Borang BPSK-1/2015</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card p-5 mb-10">
                                                                <div class="py-2 px-4 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                                <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-bordered" id="kt_datatable_2">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Senarai Semak</th>
                                                                                    {{-- @if (Session::get('user.isAdmin') == 1) --}}
                                                                                        <th style="width: 20%">Muat Naik</th>
                                                                                    {{-- @endif --}}
                                                                                    <th style="width: 30%">Fail</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    @php
                                                                                        $foundAttachment5 = false;
                                                                                        $foundAttachment6 = false;
                                                                                        $foundAttachment7 = false;
                                                                                        $foundAttachment8 = false;
                                                                                        $akuan_exist = false;
                                                                                    @endphp
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 14)
                                                                                            @php
                                                                                                $foundAttachment5 = true;
                                                                                                $akuan_exist = true;
                                                                                            @endphp
                                                                                        @elseif ($attachment->fk_lkp_spa_filename === 19)
                                                                                            @php
                                                                                                $foundAttachment6 = true;
                                                                                            @endphp
                                                                                        @elseif ($attachment->fk_lkp_spa_filename === 18)
                                                                                            @php
                                                                                                $foundAttachment7 = true;
                                                                                            @endphp
                                                                                        @elseif ($attachment->fk_lkp_spa_filename === 21)
                                                                                            @php
                                                                                                $foundAttachment7 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Surat Akuan Terima dan Aku Janji Penganjuran Acara di Putrajaya</th>
                                                                                    @if (!$foundAttachment5)
                                                                                        <th>
                                                                                            <form action="{{ url('/upload/filePembayaranAcara', $data['main_booking']->id) }}" method="post" enctype="multipart/form-data">
                                                                                                @csrf   
                                                                                                <input hidden type="text" name="name" value="Surat Akuan.pdf">
                                                                                                <input name="suratAkuan" type="file" class="form-control" id="fileUpload1" accept=".pdf">
                                                                                                <button type="submit" class="btn btn-primary btn-sm">Muat Naik</button>
                                                                                            </form>
                                                                                        </th>
                                                                                    @else
                                                                                        <th></th>
                                                                                    @endif
                                                                                    @foreach ($data['spa_booking_attachment'] as $attachment)
                                                                                        @if ($attachment->fk_lkp_spa_filename === 14)
                                                                                            <th class="font-size-sm font-weight-normal">
                                                                                                <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                            </th>
                                                                                            @php
                                                                                                $foundAttachment5 = true;
                                                                                            @endphp
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @if (!$foundAttachment5)
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal"><span class="text-danger">*</span>Resit Pembayaran Pendahuluan</th>
                                                                                    @if ($foundAttachment6 == true || $data['main_booking']->fk_lkp_status != 24)
                                                                                        <th></th>
                                                                                    @else
                                                                                        <th>
                                                                                            <form action="{{ url('/upload/filePembayaranAcara', $data['main_booking']->id) }}" method="post" enctype="multipart/form-data">
                                                                                                @csrf   
                                                                                                <input hidden type="text" name="name" value="Resit Pendahuluan.pdf">
                                                                                                <input name="resitPembayaran" type="file" class="form-control" id="fileUpload2" accept=".pdf">
                                                                                                <button type="submit" class="btn btn-primary btn-sm">Muat Naik</button>
                                                                                            </form>
                                                                                        </th>
                                                                                    @endif
                                                                                    @if ($foundAttachment6 == true)
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <a href="{{ asset($attachment->location) }}" download="{{ $attachment->name }}">{{ $attachment->name }}</a>
                                                                                        </th>
                                                                                    @elseif (in_array($data['main_booking']->fk_lkp_status, [26, 5]))
                                                                                        <th>
                                                                                            <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['main_booking']), 'type' => 3]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                                                        </th>
                                                                                    @else 
                                                                                        <th></th>
                                                                                    @endif
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">Surat Jaminan (rujuk surat kelulusan acara jika berkaitan)</th>
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <input name="suratJaminan" type="file" class="form-control" id="fileUpload3" accept=".pdf">
                                                                                            <button type="button" class="btn btn-primary btn-sm">Muat Naik</button>
                                                                                        </th>
                                                                                    <th class="font-size-sm font-weight-normal"></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="font-size-sm font-weight-normal">Lain-lain</th>
                                                                                        <th class="font-size-sm font-weight-normal">
                                                                                            <input  name="lainLain" type="file" class="form-control" id="fileUpload4" accept=".pdf">
                                                                                            <button type="button" class="btn btn-primary btn-sm">Muat Naik</button>
                                                                                        </th>
                                                                                    <th class="font-size-sm font-weight-normal"></th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mb-10" data-wizard-type="step-content" data-wizard-state="current">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No.</th>
                                                                                <th>Jenis Bayaran</th>
                                                                                <th>No Transaksi</th>
                                                                                <th>Tarikh Transaksi</th>
                                                                                <th>Jumlah bayaran (RM)</th>
                                                                                <th>Tindakan</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php
                                                                                $j = 1;
                                                                            @endphp
                                                                            @if (count($data['spa_payment']) > 0)
                                                                                @foreach($data['spa_payment'] as $p)
                                                                                    <tr>
                                                                                        <td class="font-size-sm" style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                                                        <td class="font-size-sm">BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                                                        <td class="font-size-sm">{{ $p->application_no ?? '' }}</td>
                                                                                        {{-- <td>{{ $p->fpx_trans_id ?? '' }}</td> --}}
                                                                                        {{-- <td>{{ $data['main']->sap_no ?? "" }}</td> --}}
                                                                                        <td class="font-size-sm">{{ Helper::datetime_format($p->created_at ?? '') }}</td>
                                                                                        {{-- <td>{{ Helper::datetime_format($p->fpx_date ?? '') }}</td> --}}
                                                                                        <td class="font-size-sm text-right">{{ $p->paid_amount ?? '' }}</td>
                                                                                        {{-- <td>{{ $p->amount_paid }}</td> --}}
                                                                                        <td class="font-size-sm" style="width: 15%;">
                                                                                            <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['main_booking']->id), 'type' => $p->fk_lkp_payment_type]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @else
                                                                                <tr><td colspan="6" class="text-center">Tiada Data</td></tr>
                                                                            @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Jumlah Amaun Tempahan (RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'], 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Bayaran Deposit(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ Helper::moneyhelper($data['deposit']->amount) ?? '' }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row d-flex justify-content-center">
                                                                    <div class="col-3 my-auto"><label>Baki Bayaran 50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                    <div class="col-6">
                                                                        <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ Helper::moneyhelper($data['total'] / 2) }}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {{-- </form> --}}
                                                @if (in_array($data['main_booking']->fk_lkp_status, [24])) 
                                                    <div class="row">
                                                        <div class="col-12 d-flex justify-content-center">
                                                            <form action="{{ url('event/bayaran', Crypt::encrypt($data['id'])) }}" method="post">
                                                                @csrf
                                                                <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</button>
                                                                {{-- <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Kemaskini Status Bayaran</button> --}}
                                                                @if ($akuan_exist == true)
                                                                    <a disabled href="{{ url('event/bayaran/acara', ['booking' => Crypt::encrypt($data['id']), 'value' => ($data['total_v'])]) }}" type="button" class="btn btn-primary font-weight-bold mx-1">
                                                                        <i class="fas fa-check-circle"></i> Bayar Online
                                                                    </a>    
                                                                @else
                                                                    <button disabled type="button" class="btn btn-primary font-weight-bold mx-1" title="Sila selesaikan Surat Akuan Terima dan Aku Janji sebelum membuat pembayaran.">
                                                                        <i class="fas fa-check-circle"></i> Bayar Online
                                                                    </button>    
                                                                @endif                                                              
                                                                    <input type="hidden" name="total" value="{{ $data['total_v'] }}">
                                                                <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Kembali</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Bayaran-Pembayaran Pendahuluan::End -->
                                        <!-- Bayaran-Pembayaran Penuh::Start -->
                                        <div class="tab-pane fade show @if (in_array($data['main_booking']->fk_lkp_status, [34, 20, 21, 26, 27, 29])) active @endif" id="bayaranPembayaranPenuh" role="tabpanel" aria-labelledby="bayaranPembayaranPenuh-tab">
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <form action="{{ url('event/internal/reservation/'.Crypt::encrypt($data['bookingId'])) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input hidden type="text" name="tab" id="" value="bayaranPenuh">
                                                    <div class="card card-custom">
                                                        <div class="card-body">
                                                            <div class="card p-5 mb-10">
                                                                <div class="row pb-3">
                                                                    <div class="col-12">
                                                                        <div class="py-2 text-danger font-weight-bolder">Maksimum saiz dokumen 10 MB dan format dokumen PDF sahaja yang dibenarkan untuk dimuat naik</div>
                                                                    </div>
                                                                </div>
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Perkara</th>
                                                                            <th>Tindakan</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <th class="font-size-sm font-weight-normal">Resit Pembayaran Penuh</th>
                                                                            <th class="font-size-sm font-weight-normal"><input type="text" class="form-control form-control-solid form-control-sm" name="namaPemohonPerakuan" placeholder="" value="" /></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <tr>
                                                                                <th class="font-size-sm font-weight-normal">Resit Deposit</th>
                                                                                <th class="font-size-sm font-weight-normal"><input type="text" class="form-control form-control-solid form-control-sm" name="namaPemohonPerakuan" placeholder="" value="" /></th>
                                                                            </tr>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                
                                                                <table class="table table-bordered" id="kt_datatable_2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No.</th>
                                                                            <th>Jenis Bayaran</th>
                                                                            <th>No Transaksi</th>
                                                                            <th>Tarikh Transaksi</th>
                                                                            <th>Jumlah Bayaran (RM)</th>
                                                                            <th>Tindakan</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @php
                                                                            $j = 1;
                                                                        @endphp
                                                                        @if (count($data['spa_payment']) > 0)
                                                                            @foreach($data['spa_payment'] as $p)
                                                                                <tr>
                                                                                    <td class="font-size-sm" style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                                                    <td class="font-size-sm">BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                                                    <td class="font-size-sm">{{ $p->application_no ?? '' }}</td>
                                                                                    {{-- <td>{{ $p->fpx_trans_id ?? '' }}</td> --}}
                                                                                    {{-- <td>{{ $data['main']->sap_no ?? "" }}</td> --}}
                                                                                    <td class="font-size-sm">{{ Helper::datetime_format($p->created_at ?? '') }}</td>
                                                                                    {{-- <td>{{ Helper::datetime_format($p->fpx_date ?? '') }}</td> --}}
                                                                                    <td class="font-size-sm text-right">{{ $p->paid_amount ?? '' }}</td>
                                                                                    {{-- <td>{{ $p->amount_paid }}</td> --}}
                                                                                    <td class="font-size-sm" style="width: 15%;">
                                                                                        <a href="{{ url('event/resitonline', ['id' => Crypt::encrypt($data['main_booking']->id), 'type' => $p->fk_lkp_payment_type]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr><td colspan="6" class="text-center">Tiada Data</td></tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                                
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Jumlah Amaun Tempahan (RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'], 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Bayaran Deposit(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['deposit']->amount, 2, '.', '') ?? '' }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row d-flex justify-content-center">
                                                                        <div class="col-3 my-auto"><label>Baki Bayaran 50% dari Jumlah Amaun Tempahan(RM)</label></div>
                                                                        <div class="col-6">
                                                                            <input type="text" readonly class="form-control form-control-solid" name="lokasiPerakuan" placeholder="" value="{{ number_format($data['total'] / 2, 2, '.', '') }}" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-10">
                                                                    <div class="col-12 d-flex justify-content-center">
                                                                        <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Batal Permohonan</button>
                                                                        @if (in_array($data['main_booking']->fk_lkp_status, [26, 34]))                               
                                                                            {{-- <button type="button" id="submitForms" class="btn btn-primary font-weight-bold mx-1">Kemaskini Status Bayaran</button> --}}
                                                                            <a href="{{ url('event/bayaran/acara', ['booking' => Crypt::encrypt($data['id']), 'value' => $data['total'] / 2]) }}" type="button" class="btn btn-primary font-weight-bold mx-1">
                                                                                <i class="fas fa-check-circle"></i> Bayar Online
                                                                            </a>                                    
                                                                        @endif
                                                                        <a href="{{ url('event') }}" class="btn btn-primary font-weight-bold mx-1">Kembali</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- Bayaran-Pembayaran Penuh::End --> 
                                    </div>
                                </div>
                                <!-- Bayaran::End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    @include('event.internal.js.reservation')
    @include('event.public.js.reservation')
@endsection

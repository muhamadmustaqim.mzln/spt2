@extends('layouts.master')
<style>
.active1 {
	background-color: #3445E5 !important;
	color: #FFFFFF !important;
	border-color: transparent;
}
.column {
	display: none; /* Hide all elements by default */
}
.show {
	display: block;
}
.vertical-line {
    border-left: 0.5px solid	#777; /* Width and color of the line */
    height: 100px; /* Height of the line */
    margin: 0 10px; /* Optional: Adds some space around the line */
}
</style>
@section('container')

<!--begin::Content-->
	@if(Session::has('flash'))
		<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
	@endif
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
		<!--begin::Subheader-->
		<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
			<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Acara</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Acara</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
			</div>
		</div>
		<!--end::Subheader-->
		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="owl-carousel owl-theme">
							@foreach($data['pengumuman'] as $p)
							<div class="item">
								<!--begin::Notice-->
								<div class="alert alert-custom alert-primary" style="min-height:" role="alert">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
									<h3 class="card-label font-weight-bolder text-light mb-1">{{ $p->ba_name }}</h3>
									<hr>
									{{ $p->ba_detail }}
									</div>
								</div>
								<!--end::Notice-->
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b" style="background-color: #20274d; background-position: right bottom; background-size: auto 50%; background-repeat: no-repeat; background-image: url({{ asset('assets/media/svg/patterns/rhone-2.svg') }})">
							<!--begin::Header-->
							<div class="card-header border-0 pt-5">
								<h3 class="card-title align-items-start flex-column mb-5">
									<span class="card-label font-weight-bolder text-white mb-1">Tapisan Data</span>
									<span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
								</h3>
							</div>
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body pt-2">
								{{-- "{{ url('event/details',Crypt::encrypt($f->id)), $data['carian']->tarikhMula, $data['carian']->tarikhAkhir }} --}}
								<form action="{{ url('event/details') }}" method="post" id="searchForm">
									@csrf
									<div class="form-group row">
										<div class="col-lg-12">
											<div class="input-icon">
												<select name="lokasi" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_select2_5" required>
													<option value=""> Pilih Lokasi Acara </option>
													@if ($data['carian'] != null)
														@foreach ($data['event'] as $l)
															<option value="{{ $l->id }}" @if ($l->id == $data['carian']->lokasi) selected @endif> {{ $l->name }} </option>
														@endforeach
													@else
														@foreach ($data['event'] as $l)
															<option value="{{ $l->id }}">{{ $l->name }}</option>
														@endforeach
													@endif
												</select>
												<span>
													<i class="flaticon-search-1 icon-xl"></i>
												</span>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="input-icon">
												<input type="text" disabled class="form-control form-control-lg font-weight-bold mt-3" id="kt_datepicker1" name="tarikhMula" placeholder="Tarikh Mula" autocomplete="off" required value="@if($data['carian'] != null) {{$data['carian']->tarikhMula}} @endif" />
												<span>
													<i class="flaticon-calendar-3 icon-xl"></i>
												</span>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="input-icon">
												<input type="text" disabled class="form-control form-control-lg font-weight-bold mt-3" id="kt_datepicker2" name="tarikhAkhir" placeholder="Tarikh Akhir" autocomplete="off" required value="@if($data['carian'] != null) {{$data['carian']->tarikhAkhir}} @endif" />
												<span>
													<i class="flaticon-calendar-3 icon-xl"></i>
												</span>
											</div>
										</div>
									</div>
									<div class="float-right">
										<button type="submit" id="submit_form" class="btn btn-light-primary  font-weight-bold mr-2">Carian</button>
									</div>
								</form>
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b">
							<!--begin::Header-->
							<div class="card-header border-0 pt-5">
								<h3 class="card-title align-items-start flex-column mb-5">
									<span class="card-label font-weight-bolder text-dark mb-1">Kalendar Takwim</span>
								</h3>
							</div>
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body pt-2">
								<div class="row">
									<div class="col-lg-12">
										<div id="kt_calendar"></div>
									</div>
								</div>
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
					</div>
					<div class="col-lg-8">
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b">
							<div class="card-header">
							<br>
							<h3 class="card-label font-weight-bolder mt-5">KADAR BAYARAN SEWA TAPAK DAN DEPOSIT ACARA</h3>
							<br>
							<div class="card-title">
								<h5 class="card-label text-center">Permohonan Acara hendaklah dihantar bersama dokumen yang lengkap selewat-lewatnya <span class="text-danger">60 hari</span> sebelum tarikh penganjuran.</h5>
							</div>
							<div class="card-body pt-2">
								<div class="row mb-8 mt-6">
									<div class="col-lg-12">
										<table width="100%" border="1" cellpadding="1" >
											<tr>
												<td align="center" bgcolor="#ebf5fb"><b>Bil.</b></td>
												<td align="center" bgcolor="#ebf5fb"><b>Kategori</b></td>
												<td align="center" bgcolor="#ebf5fb"><b>Lokasi</b></td>
												<td align="center" bgcolor="#ebf5fb"><b>Caj Sewaan Sehari</b></td>
												<td align="center" bgcolor="#ebf5fb"><b>Deposit Acara</b></td>
											</tr>
											@php
												$i =1;
												$initialCat = '';
												$rent = '';
											@endphp
											@foreach ($data['kadarTapak'] as $key => $w)
												<tr>
													@if ($initialCat != $w->category)
														<td rowspan="{{ $w->rowspan }}" style="vertical-align:top" align="center" bgcolor="#ffffff">
															{{$i++}}
														</td>
														<td rowspan="{{ $w->rowspan }}" style="vertical-align:top" align="center" bgcolor="#ffffff">
															{{ Helper::location_cat($w->category) }}
														</td>
														@php
															$initialCat = $w->category;
														@endphp
													@endif
													<td style="vertical-align:top">
														<ul>
															<li type="square">
																{{ $w->name }}
															</li>
														</ul>
													</td>
													<td style="vertical-align:top" align="center" >RM {{ Helper::moneyhelper($w->rent_perday) }}</td>
													<td style="vertical-align:top" align="center" >RM {{ Helper::moneyhelper($w->deposit) }}</td>
												</tr>
											@endforeach
											<tr>
												<td  bgcolor="#ebf5fb" colspan="5" bgcolor="#ffffff"><b>*Caj Pengurusan Acara</b></td>
											</tr>
											<tr>
												<td style="vertical-align:top" align="center" bgcolor="#ffffff">4</td>
												<td style="vertical-align:top" colspan="2" bgcolor="#ffffff">Penggunaan Laluan Di Putrajaya (Berbasikal, Jalan Kaki,Larian Dll)<br>&nbsp;&nbsp;&nbsp;- Bawah 50 km<br>&nbsp;&nbsp;&nbsp;- 51 km ke atas</td>
												<td style="vertical-align:top" colspan="2" align="center" bgcolor="#ffffff">&nbsp;<br>RM500.00 (12 Jam)<br>RM1,000.00 (12 Jam)</td>
											</tr>
												<tr>
												<td style="vertical-align:top" align="center" bgcolor="#ffffff">5</td>
												<td style="vertical-align:top" colspan="2" bgcolor="#ffffff">Penggunaan Kawasan/Tapak Bukan Milik PPj</td>
												<td style="vertical-align:top" colspan="2" align="center" bgcolor="#ffffff">RM500 (12 Jam)</td>
											</tr> 
											</tr>
										</table>
									</div>
									</div>
								</div>
							</div>
							{{-- @if(isset($data['kadarTapak']) && !empty($data['kadarTapak']) && is_object($data['kadarTapak']) && $data['kadarTapak']->category == 1)
								<div class="card card-custom" id="kt_card_3">
									<div class="card-header">
										<div class="card-title">
											<h3 class="card-label font-weight-bolder">{{ Helper::location_cat($w->category) }}</h3>
										</div>
										<div class="card-toolbar">
											<a href="#" class="btn btn-icon btn-circle btn-sm btn-light-primary mr-1" data-card-tool="toggle">
												<i class="ki ki-arrow-down icon-nm"></i>
											</a>
										</div>
									</div>
									@foreach($data['kadarTapak'] as $key => $w)
									<div class="card-body">
										<h6>Penggunaan Laluan Di Putrajaya (Berbasikal, Jalan Kaki,Larian Dll)</h6>
										<div class="row">
											<div class="col">
												<span>
													- Bawah 50 km<br>- 51 km ke atas
												</span>
											</div>
											<div class="vertical-line"></div>
											<div class="col">
												<span>
													RM500.00 (12 Jam)<br>RM1,000.00 (12 Jam)
												</span>
											</div>
										</div>
										<hr>
										<h6>Penggunaan Kawasan/Tapak Bukan Milik PPj</h6>
										<div class="row">
											<div class="col">
												<span>
													RM500 (12 Jam)
												</span>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							@else
								<p>No data available or invalid category.</p>
							@endif
							<div class="card card-custom" id="kt_card_3">
								<div class="card-header">
									<div class="card-title">
										<h3 class="card-label font-weight-bolder">Caj Pengurusan Acara</h3>
									</div>
									<div class="card-toolbar">
										<a href="#" class="btn btn-icon btn-circle btn-sm btn-light-primary mr-1" data-card-tool="toggle">
										<i class="ki ki-arrow-down icon-nm"></i>
										</a>
									</div>
								</div>
								<div class="card-body">
									<h6>Penggunaan Laluan Di Putrajaya (Berbasikal, Jalan Kaki,Larian Dll)</h6>
									<div class="row">
										<div class="col">
											<span>
												- Bawah 50 km<br>- 51 km ke atas
											</span>
										</div>
										<div class="vertical-line"></div>
										<div class="col">
											<span>
												RM500.00 (12 Jam)<br>RM1,000.00 (12 Jam)
											</span>
										</div>
									</div>
									<hr>
									<h6>Penggunaan Kawasan/Tapak Bukan Milik PPj</h6>
									<div class="row">
										<div class="col">
											<span>
												RM500 (12 Jam)
											</span>
										</div>
									</div>
								</div>
							</div>--}}
						</div> 
						<div class="card card-custom gutter-b">
							<div class="card-header">
								<div class="card-title">
									<h3 class="card-label">Senarai Permohonan Acara</h3>
								</div>
								<div class="card-toolbar">
									<ul class="nav nav-success nav-bold nav-pills">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1"  data-toggle="tooltip" data-theme="dark" title="Grid"><i class="flaticon-grid-menu text-dark"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2" data-toggle="tooltip" data-theme="dark" title="List"><i class="la la-list text-dark icon-xl"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3"  data-toggle="tooltip" data-theme="dark" title="Peta"><i class="flaticon2-map text-dark"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<!--begin::Body-->
							<div class="card-body pt-2">
								<div class="row mb-8 mt-6">
									<div class="col-lg-12">
										<p>
											<div id="myBtnContainer">
												<button type="button" class="btn btn-light-primary font-weight-bold mr-2 mt-2 active1" onclick="filterSelection('all')">Semua</button>
												@foreach ($data['location'] as $l)
													<button type="button" class="btn btn-light-primary font-weight-bold mr-2 mt-2" onclick="filterSelection('<?= $l->name ?>')">{{ $l->name }}</button>												
												@endforeach
											</div>
										</p>
									</div>
									{{-- <div class="col-lg-3">
										<div class="input-icon">
											<input type="text" class="form-control mt-2" placeholder="Carian..." />
											<span>
												<i class="flaticon2-search-1 icon-md"></i>
											</span>
										</div>
									</div> --}}
								</div>
								<div class="tab-content">
									<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">
										<div class="row">
											@php
												$i = 0;
											@endphp
											@if ($data['carian'] != null)
												@foreach ($data['event'] as $f)
													@if ($f->id == $data['carian']->lokasi)
														<!--begin::Product-->
														<div class="col-md-4 col-xxl-4 col-lg-12 column {{ $f->id }}">
															<!--begin::Card-->
															<div class="card card-custom gutter-b" style="box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 50%)">
																<div class="overlay">
																	<div class="overlay-wrapper bg-light text-center">
																		<!--begin:<img src="<?php /*{{ URL::asset("{$f->bh_file_location}/{$f->bh_filename}") }}*/?>" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />:-->
																		<img src="#" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />
																	</div>
																	<div class="overlay-layer">
																		<a href="{{ url('event/details',Crypt::encrypt($f->id)), Helper::date_format($data['carian']->tarikhMula), $data['carian']->tarikhAkhir }}" class="btn font-weight-bolder btn-sm btn-light-primary">Teruskan Tempahan</a>
																	</div>
																</div>
																<div class="card-body">
																	<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="font-size-h6 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{ $f->name }}</a>
																	<!--<p class="text-muted font-weight-bold"><?php /*{{ $f->bh_percint }}*/?></p>-->
																	<p><b>Harga:</b> <span class="float-right"><span class="font-weight-boldest text-danger font-size-h6">RM </span> / Hari</span></p>
																	<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->status) }} label-inline">{{ Helper::status($f->status) }}</span></p>
																	<br>
																	<p class="text-center"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
																	<p class="text-center"><a href="{{ url('event/details',[Crypt::encrypt($f->id), Crypt::encrypt($data['carian']->tarikhMula), Crypt::encrypt($data['carian']->tarikhAkhir)]) }}" class="btn font-weight-bolder btn-primary mt-4">Teruskan Tempahan </a></p>
																</div>
															</div>
															<!--end::Card-->
														</div>
														<!--end::Product-->
													@endif
												@endforeach
											@else
												@foreach ($data['location'] as $f)
													<!--begin::Product-->
													<div class="col-md-4 col-xxl-4 col-lg-12 column {{ $f->id }}">
														<!--begin::Card-->
														<div class="card card-custom gutter-b" style="box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 50%)">
															<div class="overlay">
																<div class="overlay-wrapper bg-light text-center">
																	<!--begin:<img src="<?php /*{{ URL::asset("{$f->bh_file_location}/{$f->bh_filename}") }}*/?>" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />:-->
																	@if (isset($f->file_name))
																		<img  id="main_{{ $i }}" src="{{ URL::asset($f->file_name) }}" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />
																		<div id="tempdiv_{{ $i }}" style="height: 220px; overflow: hidden; position: relative;">
																			<img id="temp_{{ $i }}" src="{{ asset('/assets/media/logos/ppj.png') }}" alt="" class="card-img-top" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto; height: 50%; width: auto;" />
																		</div>
																	@endif
																</div>
																<div class="overlay-layer">
																	<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-sm btn-light-primary">Teruskan Tempahan</a>
																</div>
															</div>
															<div class="card-body">
																<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="font-size-h6 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{ $f->name }}</a>
																<!--<p class="text-muted font-weight-bold"><?php /*{{ $f->bh_percint }}*/?></p>-->
																<p><b>Harga:</b> <span class="float-right"><span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::moneyhelper($f->rent_perday) }}</span> / Hari</span></p>
																<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->status) }} label-inline">{{ Helper::status($f->status) }}</span></p>
																<br>
																<p class="text-center"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
																<p class="text-center"><a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-primary mt-4">Teruskan Tempahan</a></p>
															</div>
														</div>
														<!--end::Card-->
													</div>
													@php
														$i++;
													@endphp
													<!--end::Product-->
												@endforeach
												{{-- @foreach ($data['event'] as $l)
													<option value="{{ $l->id }}">{{ $l->name }}</option>
												@endforeach --}}
												{{-- @foreach ($data['event'] as $f)
													<!--begin::Product-->
													<div class="col-md-4 col-xxl-4 col-lg-12 column {{ $f->id }}">
														<!--begin::Card-->
														<div class="card card-custom gutter-b" style="box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 50%)">
															<div class="overlay">
																<div class="overlay-wrapper bg-light text-center">
																	<!--begin:<img src="<?php /*{{ URL::asset("{$f->bh_file_location}/{$f->bh_filename}") }}*/?>" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />:-->
																	<img src="#" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />
																</div>
																<div class="overlay-layer">
																	<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-sm btn-light-primary">Teruskan Tempahan</a>
																</div>
															</div>
															<div class="card-body">
																<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="font-size-h6 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{ $f->name }}</a>
																<!--<p class="text-muted font-weight-bold"><?php /*{{ $f->bh_percint }}*/?></p>-->
																<p><b>Harga:</b> <span class="float-right"><span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::moneyhelper($f->rent_perday) }}</span> / Hari</span></p>
																<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->status) }} label-inline">{{ Helper::status($f->status) }}</span></p>
																<br>
																<p class="text-center"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
																<p class="text-center"><a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-outline-primary mt-4">Teruskan Tempahan</a></p>
															</div>
														</div>
														<!--end::Card-->
													</div>
													<!--end::Product-->
												@endforeach --}}
											@endif
										</div>
									</div>
									<div class="tab-pane fade" id="kt_tab_pane_7_2" role="tabpanel" aria-labelledby="kt_tab_pane_7_2">
										<div class="row">
											@foreach ($data['event'] as $f)
												<div class="col-lg-12 column ">
												<!--begin::Item-->
												<div class="d-flex align-items-center pb-9">
													<!--begin::Symbol-->
													<div class="symbol symbol-150 symbol-2by3 flex-shrink-0 mr-4">
														<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}"><div class="symbol-label" style="background-image:"></div></a>
														{{-- <img id="main_{{ $i }}" src="{{ URL::asset($f->file_name) }}" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" /> --}}
													</div>
													<!--end::Symbol-->
													<!--begin::Section-->
													<div class="d-flex flex-column flex-grow-1">
														<!--begin::Title-->
														<a href="{{ url('event/details',Crypt::encrypt($f->id)) }}" class="text-dark-75 font-weight-bolder font-size-lg text-hover-primary mb-1">{{ $f->name }}</a>
														<p><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
														<p class="mb-0"><b>Harga:</b> <span class="float-right"><span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::moneyhelper($f->rent_perday) }}</span> / Hari</span></span></p>
														<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->status) }} label-inline">{{ Helper::status($f->status) }}</span></p>
														<!--end::Title-->
													</div>
													<!--end::Section-->
												</div>
												<!--end::Item-->
												</div>
											@endforeach
										</div>
									</div>
									<div class="tab-pane fade" id="kt_tab_pane_7_3" role="tabpanel" aria-labelledby="kt_tab_pane_7_3">
										<div id="map" style="height:500px;"></div>
									</div>
								</div>											
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
					</div>
				</div>
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
		
	</div>
	<!--end::Content-->
	<!-- Button trigger modal-->
	<!-- Modal-->
	
	<div class="modal fade" id="exampleModalCustomScrollable" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
			<div class="modal-content" style="background-position: right top; background-size: auto 50%; background-repeat: no-repeat; background-image: url({{ asset('assets/media/svg/shapes/abstract-2.svg') }})">
				<div class="modal-header border-0">
					<h5 class="modal-title font-weight-bolder" id="exampleModalLabel">Pengumuman</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div data-scroll="true" data-height="200">
						<ul>						
							@foreach($data['pengumuman'] as $p)
							<li>
								<b>{{ $p->ba_name }}</b> <br> {{ $p->ba_detail }}
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js_content')
    @include('event.public.js.lists')
@endsection



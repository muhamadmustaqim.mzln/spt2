@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		
    <!--begin::Subheader-->
    {{-- <div class="subheader py-5 py-lg-10 gutter-b subheader-transparent mt-0" id="kt_subheader" style="background-color: #20274d; background-position: right bottom; background-size: auto 100%; background-repeat: no-repeat; background-image: url('{{ asset('assets/media/svg/patterns/taieri.svg') }}')">
        <div class="container ">
            <div class="bg-white rounded p-4 ">
                <form action="" method="post">
            <div class="row">
                    @csrf
                <div class="col-lg-5">
                    <div class="input-icon">
                        <select name="lokasi" class="form-control form-control-lg border-0 font-weight-bold" id="kt_select2_5" required>
                            <option value=""> Pilih Fasiliti Dewan</option>
                            @foreach ($data['location'] as $l)
							<?php
							$varSelected = " ";
							if ($data['fasility']->bh_name == $l->bh_name)
							{
								$varSelected = "selected";
							}	
							?>							
							
                            <option value="{{ $l->id }}" {{ $varSelected }}>{{ $l->bh_name }}</option>
                        @endforeach
                        </select>
                        <span>
                            <i class="flaticon-search-1 icon-xl"></i>
                        </span>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="input-icon" id="kt_daterangepicker_5">
                        <input type="text" class="form-control form-control-lg border-0 font-weight-bold"   name="tarikh" placeholder="Tarikh" autocomplete="off" value="{{$data['date']}}" readonly required />
                        <span>
                            <i class="flaticon-calendar-3 icon-xl"></i>
                        </span>
                        <span class="bullet bullet-ver h-45px d-none d-sm-flex mr-2"></span>
                    </div>
                </div>
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-block btn-lg btn-primary font-weight-bold">CARIAN</button>
                </div>
            </form>
            </div>
            </div>

        </div>
    </div> --}}
    <!--end::Subheader-->                        				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                {{-- <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p> --}}
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                {{-- <div class="col-lg-5">
                    <div id="panorama" class="mb-6" style="width: 100%; height: 300px"></div>
                </div>
                <div class="col-lg-7 desk-ver">
                    <div class="row" id="aniimated-thumbnials">
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1542665952-14513db15293?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1542665952-14513db15293?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1549895058-36748fa6c6a7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1549895058-36748fa6c6a7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1577887546572-144f62fceb9e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1577887546572-144f62fceb9e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1541559476210-2c7094bc222b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1541559476210-2c7094bc222b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1560184611-ff3e53f00e8f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1629&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1560184611-ff3e53f00e8f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1629&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="https://images.unsplash.com/photo-1630435037688-74c9ec61ffa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" class="img-container text-white">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1630435037688-74c9ec61ffa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px; " alt="">
                                <div class="centered" ><h5 class="font-weight-bolder">+ 1 Gambar</h5></div>
                            </a>
                        </div>
                        <div class="col-lg-4" style="display:none;">
                            <a href="https://images.unsplash.com/photo-1583787035686-91b82ad5d811?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80"  data-sub-html=".caption7">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1583787035686-91b82ad5d811?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80" style="width:100%; height: 137.54px" alt="">
                                <div class="caption7">
                                    <h6>Dewan</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="col-lg-12">
                    <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                        <div class="card-header">
                            <div class="card-title">
                            <h4 class="card-label font-weight-bolder text-dark">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h4>
                            <span class="label label-lg font-weight-bold {{ Helper::status_color($data['fasility']->bh_status) }} label-inline">{{ Helper::status($data['fasility']->bh_status) }}</span>
                            </div>
                            <div class="card-toolbar">
                            <h7 class="card-label text-dark  mr-2">bermula dari</h7>
                            <h3 class="card-label font-weight-boldest mr-2 text-danger">RM {{ Helper::get_harga_dewan($data['fasility']->id) }}</h3>
                            <h7 class="card-label text-dark mr-4">per jam</h7>
                                @if(Session::has('user')) 
                                <a href="{{ url('hall/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a>
                                @else
                                <a href="{{ url('auth') }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a>
                                @endif
                                <a href="{{ url('hall') }}" class="btn btn-light-danger font-weight-bolder btn-sm float-right mb-1 ml-2">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body ">
                            <div class="card-title h4">Tempahan telah disimpan dan dihantar ke pihak perbadanan.</div>
                            <ul>
                                <li>Emel pengesahan tempahan akan dihantar ke emel anda selepas disemak oleh pentadbir sistem.</li>
                            </ul>
                        </div>
                        {{-- <div class="card-body">
                        </div> --}}
                    </div>
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-end pb-7">
                                    <a href="{{ url('event') }}" class="btn btn-secondary mx-1">Kembali</a>
                                    <a href="{{ url('event/muatturunborang', Crypt::encrypt($data['bookingId'])) }}" class="btn btn-light-warning mx-1" target="_blank">Muat Turun Borang</a>
                                    <a href="{{ url('event/booking', Crypt::encrypt($data['main_booking']->bmb_booking_no)) }}" class="btn btn-primary mx-1">Ke Maklumat Tempahan</a>
                                </div>
                            </div>
                            {{-- <div class="card card-custom">
                                <div class="card-body p-0"> --}}
                                    <div class="card mx-7" id="rumusanCard" style="">
                                        <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Rumusan Permohonan</div>
                                        <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>No Permohonan</label></div>
                                                    <div class="col-9">
                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohonPerakuan" placeholder="" value="{{ Helper::get_noTempahan($data['bookingId']) }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Nama Pemohon</label></div>
                                                    <div class="col-9">
                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaPemohonPerakuan" placeholder="" value="{{ $data['booking_person']->name }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Nama Acara</label></div>
                                                    <div class="col-9">
                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="namaAcaraPerakuan" placeholder="" value="{{ $data['booking_event']->event_name }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-3 my-auto"><label>Tarikh Acara Dari</label></div>
                                                            <div class="col-9"><input disabled type="text" class="tarikhAcaraDariPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraDariPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->event_date) }}" /></div>
                                                        </div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-3 my-auto"><label>Tarikh Acara Hingga</label></div>
                                                            <div class="col-9"><input disabled type="text" class="tarikhAcaraHinggaPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraHinggaPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->event_date_end) }}" /></div>
                                                        </div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-3 my-auto"><label>Tarikh Masuk Tapak</label></div>
                                                            <div class="col-9"><input disabled type="text" class="tarikhAcaraDariPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraDariPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->enter_date) }}" /></div>
                                                        </div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-3 my-auto"><label>Tarikh Keluar Tapak</label></div>
                                                            <div class="col-9"><input disabled type="text" class="tarikhAcaraHinggaPerakuan form-control form-control-solid form-control-lg" name="tarikhAcaraHinggaPerakuan" placeholder="" value="{{ Helper::date_format($data['booking_event']->exit_date) }}" /></div>
                                                        </div>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Lokasi</label></div>
                                                    <div class="col-9">
                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg" name="lokasiPerakuan" placeholder="" value="{{ Helper::locationspa($data['booking_event']->fk_spa_location) }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Jumlah Hari Penggunaan Tapak</label></div>
                                                    <div class="col-9">
                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg" name="jumlahHariPenggunaanPerakuan" id="jumlahHariPenggunaanPerakuan" value="{{ $data['booking_event']->total_day }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Sewa Tapak Sehari (RM)</label></div>
                                                    <div class="col-9">
                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg" name="sewaTapakSehariPerakuan" placeholder="" value="{{ Helper::moneyhelper($data['fasility1']->rent_perday) }}" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>GST (0%)</label></div>
                                                    <div class="col-9">
                                                        <input disabled type="text" class="form-control form-control-solid form-control-lg" name="gstPerakuan" placeholder="" value="0" />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Deposit (RM)</label></div>
                                                    <div class="col-9">
                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg" name="depositPerakuan" placeholder="" value="{{ Helper::moneyhelper(floatval(str_replace(',', '', $data['fasility1']->deposit)), 2, '.', ',') }} " />
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-3 my-auto"><label>Jumlah (RM)</label></div>
                                                    <div class="col-9">
                                                        <input type="text" disabled class="form-control form-control-solid form-control-lg" name="jumlahPerakuan" placeholder="" value="@if($data['total_type'] == 0){{ $data['total'] }} @else {{ number_format($data['fasility1']->rent_perday * $data['booking_event']->total_day, 2, '.', ',') }} @endif" />
                                                        {{-- <input type="text" disabled class="form-control form-control-solid form-control-lg" name="jumlahPerakuan" placeholder="" value="{{ number_format($data['fasility1']->rent_perday * $data['booking_event']->total_day, 2, '.', ',') }}" /> --}}
                                                        <div class="" id="">* Jumlah tidak termasuk deposit & lain-lain caj.</div>
                                                    </div>
                                                <div class="col-3"></div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                    </div>
                                {{-- </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    {{-- <div class="row">
                        <div class="col-lg-6">
                            <a href='https://www.waze.com/ul?ll={{ $data['fasility']->latitude }}%2C{{ $data['fasility']->longitude }}&navigate=yes&zoom=17' class="btn btn-primary btn-block btn-lg font-weight-bolder mb-4"><i class="fab fa-waze"></i> Waze</a>
                        </div>
                        <div class="col-lg-6">
                            <a href='https://www.google.com/maps?layer=c&cbll={{ $data['fasility']->latitude }},{{ $data['fasility']->longitude }}' class="btn btn-dark btn-block btn-lg font-weight-bolder mb-4"><i class="fas fa-map-marker-alt"></i> Google Maps</a>
                        </div>
                    </div> --}}
                    <!--begin::List Widget 21-->
                    {{-- <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 pb-0">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-dark">Cadangan Lokasi Lain</span>
                                <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['date'] }}</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            @foreach($data['others'] as $o)
                            <!--begin::Item-->
                            <div class="d-flex flex-wrap align-items-center mb-10">
                                <!--begin::Symbol-->
                                <div class="symbol symbol-90 symbol-2by3 flex-shrink-0 mr-4">
                                    <div class="symbol-label" style="background-image: url('{{ URL::asset("{$o->bh_file_location}/{$o->bh_filename}") }}')"></div>
                                </div>
                                <!--end::Symbol-->
                                <!--begin::Title-->
                                <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                    <a href="{{ url('hall/details',Crypt::encrypt($o->id)) }}" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">{{ $o->bh_name }}</a>
                                    <span class="text-muted font-weight-bold font-size-sm my-1">{{ Helper::location($o->fk_lkp_location) }}</span>
                                    <p><span class="label font-weight-bolder {{ Helper::status_color($o->bh_status) }} label-inline">{{ Helper::status($o->bh_status) }}</span></p>
                                    <p><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star text-warning icon-nm mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i></p>
                                </div>
                                <!--end::Title-->
                            </div>
                            <!--end::Item-->
                            @endforeach
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

<script>
    $(document).ready(function() {
        $('#perakuanCheckbox').on('change', function() {
            console.log('test')
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        
        var datePickerDari = $('#kt_datepicker_1').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
        var datePickerHingga = $('#kt_datepicker_2').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true
        });
        datePickerDari.on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        datePickerHingga.datepicker('setStartDate', minDate);
        });

        checkStatusAndToggle();
        $('#statusPemohon').on('change', function () {
            checkStatusAndToggle();
        });
        function checkStatusAndToggle() {
            var selectedStatus = $('#statusPemohon').val();
            if (selectedStatus == 'penganjurUtama') {
                $('#maklumatPenganjurUtamaNavItem').hide();
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
            }
        }
    });
</script>
@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    {{-- @include('hall.public.js.details') --}}
@endsection

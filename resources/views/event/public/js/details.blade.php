{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script> --}}

<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>

{{-- @if(isset($data['fasility'])) --}}
<script>
    "use strict";

    var KTCalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('DD-MM-YYYY');
            var TODAY = todayDate.format('DD-MM-YYYY');
            var TOMORROW = todayDate.clone().add(1, 'day').format('DD-MM-YYYY');

            var calendarEl = document.getElementById('kt_calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                themeSystem: 'bootstrap',

                isRTL: KTUtil.isRTL(),

                header: {
                    left: 'prev,next ',
                    center: 'title',
                    right: 'today'
                },

                height: 800,
                contentHeight: 780,
                aspectRatio: 3, 

                nowIndicator: true,
                now: TODAY + 'T09:25:00', // just for demo

                views: {
                    dayGridMonth: { buttonText: 'month' }
                },

                defaultView: 'dayGridMonth',
                defaultDate: TODAY,
                locale: 'ms-my',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: events,

                eventRender: function(info) {
                    var element = $(info.el);

                    if (info.event.extendedProps && info.event.extendedProps.description) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', info.event.extendedProps.description);
                            element.data('placement', 'top');
                            KTApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        }
                    }
                }
            });

            calendar.render();
        }
    };
    }();

    jQuery(document).ready(function() {
        KTCalendarBasic.init();
    });
</script>
<script>
    // Get the file path from the input field
    var img = $('#panoramaID').val();
    var id = $('#locationID').val();
    var panoramaPath = "{{ asset('assets') }}" + img;
    var placeholderImage = "{{ asset('/assets/media/logos/ppj.png') }}";

    // Function to check if the file exists
    function checkFileExists(filePath) {
        return $.ajax({
            url: filePath,
            type: 'HEAD',
            success: function() {
                return true;
            },
            error: function() {
                return false;
            }
        });
    }

    // Check if the file exists and initialize Pannellum viewer if it does
    checkFileExists(panoramaPath).done(function() {
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": panoramaPath,
            "preview": panoramaPath,
            "autoLoad": true,
            "autoRotate": 2,
            "autoRotateInactivityDelay": 2,
            "title": "{{ Helper::location($data['fasility2']->name) }}",
        });
        // console.log('File exists and Pannellum viewer initialized.');
    }).fail(function() {
        // console.log('File not found. Pannellum viewer not initialized.');
    });

    // Check if normal image files exist and toggle placeholder/actual images
    $('[data-img]').each(function(index) {
        var link = $(this);
        var imagePath = link.data('img');
        var placeholderImg = $('#plh_' + index);
        var placeholderDiv = $('#plhd_' + index);
        var actualImg = $('#img_' + index);

        checkFileExists(imagePath).done(function() {
            actualImg.show();
            placeholderImg.hide();
            placeholderDiv.hide(); 
        }).fail(function() {
            actualImg.hide();
            placeholderImg.show();
            placeholderDiv.show(); 
        });
    });
    // pannellum.viewer('panorama', {
    //     "type": "equirectangular",
    //     "panorama": "{{ URL::asset($data['fasility']->bh_file_location.'/'.$data['fasility']->bh_filename) }}",
    //     "preview": "{{ URL::asset($data['fasility']->bh_file_location.'/'.$data['fasility']->bh_filename) }}",
    //     "title": "{!! $data['fasility']->bh_name !!}, {!! Helper::location($data['fasility']->fk_lkp_location) !!}",
    // });
</script>
{{-- @endif --}}

<script>
    $('#kt_select2_4').on('change', function(){
    var location_id = $(this).val();
    if (location_id == ''){
        $('#kt_select2_5').prop('disabled', true);
    }
    else{
        $('#kt_select2_5').prop('disabled', false);
        $.ajax({
            url:"{{ url('sport/ajax') }}",
            type: "POST",
            data: {'location_id' : location_id},
            dataType: 'json',
            success: function(data){
                $('#kt_select2_5').html(data);
            }
        });
    }
});
</script>
<script>
    // date picker
    $(function() {

        var blockedDates = ['2023-09-10', '2023-09-15', '2023-09-20'];

        $('#kt_daterangepicker_5').daterangepicker({
            autoApply: true,
            autoclose: true,
            minDate: moment().add(1, 'month'),
            // minDate: new Date(),
            isInvalidDate: function(date) {
                var formattedDate = date.format('DD-MM-YYYY');
                
                // Check if the date is in the blockedDates array
                return blockedDates.indexOf(formattedDate) !== -1;
            }
        }, function(start, end, label) {
        var isValidRange = true;

        // Iterate through each day in the selected range
        for (var d = start.clone(); d.isBefore(end); d.add(1, 'day')) {
            var formattedDate = d.format('DD-MM-YYYY');
            if (blockedDates.indexOf(formattedDate) !== -1) {
                isValidRange = false; // Range contains a blocked date
                break;
            }
        }

        if (!isValidRange) {
            // Show an error message or take any other action
            Swal.fire(
                'Harap Maaf!',
                'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                'error'
                );
            $('#kt_daterangepicker_5').data('daterangepicker').setStartDate(moment());
            $('#kt_daterangepicker_5').data('daterangepicker').setEndDate(moment());
        } else {
            // Handle the valid range here
            $('#kt_daterangepicker_5 .form-control').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
        }
        });
    });
</script>
<script>
    $(document).ready(function() {
        // $('#kt_select2_5').on('change', function(){
        //     var location_id = $(this).val();
        //     var bookedDates = <?php echo $data['booked_date1'] ?>;

        //     if (bookedDates.hasOwnProperty(location_id)) {
        //         var datesArray = bookedDates[location_id];

        //         var blockedDates = datesArray.map(function(date) {
        //             return moment(date, 'YYYY-MM-DD'); // Assuming date is in 'YYYY-MM-DD' format
        //         });

        //         // Initialize or update daterangepicker
        //         $('#kt_datepicker1').daterangepicker({
        //             buttonClasses: 'btn',
        //             applyClass: 'btn-primary',
        //             cancelClass: 'btn-secondary',
        //             showDropdowns: true,
        //             autoUpdateInput: false,
        //             singleDatePicker: true,
        //             autoApply: true,
        //             minDate: moment().add(1, 'month'),
        //             isInvalidDate: function(date) {
        //                 return blockedDates.some(function(blockedDate) {
        //                     return date.isSame(blockedDate, 'day');
        //                 });
        //             },
        //             locale: {
        //                 cancelLabel: 'Clear',
        //                 format: 'DD-MM-YYYY'
        //             }
        //         });

        //         // Update input value on apply
        //         $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        //             $(this).val(picker.startDate.format('DD-MM-YYYY'));

        //             var startDate1 = picker.startDate;
        //             var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

        //             // If startDate1 is greater than startDate2, update startDate2
        //             if (startDate1.isAfter(startDate2)) {
        //                 $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
        //                 $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
        //             }
        //         });
        //     } else {
        //         console.log("Location ID " + location_id + " not found in bookedDates.");
        //     }
        // });
        // function fetchBlockedDates(location_id) {
        //     $.ajax({
        //         url:"{{ url('event/carianSekatan') }}",
        //         type: "POST",
        //         data: {'location_id' : location_id},
        //         dataType: 'json',
        //         success: function(data){
        //             var blockedDates = [];

        //             if (data && data.length > 0) {
        //                 var blockedDates = data.map(function(item) {
        //                     return moment(item.date_booking, 'YYYY-MM-DD');  // Adjust date format if needed
        //                 });

        //                 var minDate = moment();

        //                 $('#kt_datepicker1').daterangepicker({
        //                     autoApply: true,
        //                     autoclose: true,
        //                     showDropdowns: true,
        //                     autoUpdateInput: false,
        //                     singleDatePicker: true,
        //                     minDate: minDate,
        //                     // maxDate: maxDate.toDate(), // Set the maximum date
        //                     isInvalidDate: function(date) {
        //                         // Check if the date is in blockedDates
        //                         return blockedDates.some(function(blockedDate) {
        //                             return date.isSame(blockedDate, 'day');
        //                         });
        //                     },
        //                     locale: {
        //                         cancelLabel: 'Clear',
        //                         format: 'DD-MM-YYYY'
        //                     }
        //                 });

        //                 $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        //                     $(this).val(picker.startDate.format('DD-MM-YYYY'));
        //                     $('#kt_datepicker2').prop('disabled', false);

        //                     var startDate1 = picker.startDate;
        //                     var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

        //                     // If startDate1 is greater than startDate2, update startDate2
        //                     if (startDate1.isAfter(startDate2)) {
        //                         $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
        //                         $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
        //                     }
        //                     $('#kt_datepicker2').daterangepicker({
        //                         buttonClasses: ' btn',
        //                         applyClass: 'btn-primary',
        //                         cancelClass: 'btn-secondary',
        //                         showDropdowns: true,
        //                         autoUpdateInput: false,
        //                         singleDatePicker: true,
        //                         autoApply: true,
        //                         minDate: picker.startDate, 
        //                         isInvalidDate: function(date) {
        //                             // Check if the date is in blockedDates
        //                             return blockedDates.some(function(blockedDate) {
        //                                 return date.isSame(blockedDate, 'day');
        //                             });
        //                         },
        //                         // minDate: new Date(),
        //                         // minDate: $('#kt_daterangepicker_5').val(),
        //                         locale: {
        //                             cancelLabel: 'Clear',
        //                             format: 'DD-MM-YYYY'
        //                         }
        //                     });
        //                     $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
        //                         $(this).val(picker.startDate.format('DD-MM-YYYY'));

        //                         var dateAfter = picker.startDate;
        //                         var dateBefore = moment($('#kt_datepicker1').val(), 'DD-MM-YYYY');

        //                         // Check if any blocked dates fall between dateBefore and dateAfter
        //                         var hasBlockedDatesInRange = blockedDates.some(function(blockedDate) {
        //                             return blockedDate.isBetween(dateBefore, dateAfter, 'day', '[]');
        //                         });

        //                         if (hasBlockedDatesInRange) {
        //                             Swal.fire(
        //                                 'Harap Maaf!',
        //                                 'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
        //                                 'error'
        //                                 );
        //                             $('#submit_form').prop('disabled', true);
        //                         } else {
        //                             $('#submit_form').prop('disabled', false);
        //                         }
        //                     });
        //                     $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
        //                         $(this).val('');
        //                     });
        //                 });

        //                 $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
        //                     $(this).val('');
        //                 });
        //             }
        //         }
        //     });
        // }
        // var startDate = new Date();
        // var bookedDates = <?php echo $data['booked_date'] ?>;
        // // console.log(bookedDates) // = ["2024-07-24", "2024-07-25","2024-07-26"]

        // var blockedDates = bookedDates.map(function(item) {
        //     return moment(item.date_booking, 'YYYY-MM-DD'); // Adjust date format if needed
        // });

        // $('#kt_datepicker1').daterangepicker({
        //     buttonClasses: ' btn',
        //     applyClass: 'btn-primary',
        //     cancelClass: 'btn-secondary',
        //     showDropdowns: true,
        //     autoUpdateInput: false,
        //     singleDatePicker: true,
        //     autoApply: true,
        //     minDate: moment().add(1, 'month'),
        //     isInvalidDate: function(date) {
        //         // Check if the date is in blockedDates
        //         return blockedDates.some(function(blockedDate) {
        //             return date.isSame(blockedDate, 'day');
        //         });
        //     },
        //     // minDate: new Date(),
        //     // minDate: $('#kt_daterangepicker_5').val(),
        //     locale: {
        //         cancelLabel: 'Clear',
        //         format: 'DD-MM-YYYY'
        //     },
        // });
        // $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        //     // $(this).val(picker.startDate.format('DD-MM-YYYY'));

        //     // var startDate1 = picker.startDate;
        //     // var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

        //     // // If startDate1 is greater than startDate2, update startDate2
        //     // if (startDate1.isAfter(startDate2)) {
        //     //     $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
        //     //     $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
        //     // }
        //     // $('#kt_datepicker2').daterangepicker({
        //     //     buttonClasses: ' btn',
        //     //     applyClass: 'btn-primary',
        //     //     cancelClass: 'btn-secondary',
        //     //     showDropdowns: true,
        //     //     autoUpdateInput: false,
        //     //     singleDatePicker: true,
        //     //     autoApply: true,
        //     //     minDate: picker.startDate, 
        //     //     // minDate: new Date(),
        //     //     // minDate: $('#kt_daterangepicker_5').val(),
        //     //     locale: {
        //     //         cancelLabel: 'Clear',
        //     //         format: 'DD-MM-YYYY'
        //     //     },
        //     //     isInvalidDate: function(date) {
        //     //         // Convert date to YYYY-MM-DD format to match blockedDates format
        //     //         var dateString = date.format('YYYY-MM-DD');
        //     //         // Check if the date is in blockedDates array
        //     //         return blockedDates.includes(dateString);
        //     //     }
        //     // });
        //     // $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
        //     //     $(this).val(picker.startDate.format('DD-MM-YYYY'));
        //     // });
        //     // $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
        //     //     $(this).val('');
        //     // });
        // });
        // $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });

        // $('#kt_datepicker2').daterangepicker({
        //     buttonClasses: ' btn',
        //     applyClass: 'btn-primary',
        //     cancelClass: 'btn-secondary',
        //     showDropdowns: true,
        //     autoUpdateInput: false,
        //     singleDatePicker: true,
        //     autoApply: true,
        //     minDate: moment().add(1, 'month'),
        //     isInvalidDate: function(date) {
        //         // Check if the date is in blockedDates
        //         return blockedDates.some(function(blockedDate) {
        //             return date.isSame(blockedDate, 'day');
        //         });
        //     },
        //     // minDate: new Date(),
        //     // minDate: $('#kt_daterangepicker_5').val(),
        //     locale: {
        //         cancelLabel: 'Clear',
        //         format: 'DD-MM-YYYY'
        //     },
        // });
        // $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('DD-MM-YYYY'));
        // });
        // $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });

        var minDate = moment().add(3, 'month');

        $('#kt_datepicker1').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: minDate,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            },
        });
        $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));

            var startDate1 = picker.startDate;
            var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

            // If startDate1 is greater than startDate2, update startDate2
            if (startDate1.isAfter(startDate2)) {
                $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
                $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
            }
            $('#kt_datepicker2').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                showDropdowns: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate, 
                // minDate: new Date(),
                // minDate: $('#kt_daterangepicker_5').val(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
            });
            $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
            $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
        $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('#kt_datepicker2').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: minDate,
            // minDate: new Date(),
            // minDate: $('#kt_daterangepicker_5').val(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            },
        });
        $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });


        // $('#kt_select2_5').on('change', function(){
        //     var location_id = $(this).val();

        //     fetchBlockedDates(location_id);
        // })

        // $('#kt_select2_5').on('change', function(){
        //     var location_id = $(this).val();
        //     $('#kt_datepicker1').prop('disabled', false);
        //     $('#kt_datepicker2').prop('disabled', false);
        //     // if (location_id == ''){
        //     //     $('#kt_select2_5').prop('disabled', true);
        //     // }
        //     // else{
        //     $('#kt_select2_5').prop('disabled', false);
        //     $.ajax({
        //         url:"{{ url('event/carianSekatan') }}",
        //         type: "POST",
        //         data: {'location_id' : location_id},
        //         dataType: 'json',
        //         success: function(data){
        //             if (data && data.length > 0) {
        //                 var blockedDates = data.map(function(item) {
        //                     return moment(item.date_booking, 'YYYY-MM-DD');  // Adjust date format if needed
        //                 });

        //                 var minDate = moment();

        //                 $('#kt_datepicker1').daterangepicker({
        //                     autoApply: true,
        //                     autoclose: true,
        //                     showDropdowns: true,
        //                     autoUpdateInput: false,
        //                     singleDatePicker: true,
        //                     minDate: minDate,
        //                     // maxDate: maxDate.toDate(), // Set the maximum date
        //                     isInvalidDate: function(date) {
        //                         // Check if the date is in blockedDates
        //                         return blockedDates.some(function(blockedDate) {
        //                             return date.isSame(blockedDate, 'day');
        //                         });
        //                     },
        //                     locale: {
        //                         cancelLabel: 'Clear',
        //                         format: 'DD-MM-YYYY'
        //                     }
        //                 });

        //                 $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        //                     $(this).val(picker.startDate.format('DD-MM-YYYY'));
        //                     $('#kt_datepicker2').prop('disabled', false);

        //                     var startDate1 = picker.startDate;
        //                     var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

        //                     // If startDate1 is greater than startDate2, update startDate2
        //                     if (startDate1.isAfter(startDate2)) {
        //                         $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
        //                         $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
        //                     }
        //                     $('#kt_datepicker2').daterangepicker({
        //                         buttonClasses: ' btn',
        //                         applyClass: 'btn-primary',
        //                         cancelClass: 'btn-secondary',
        //                         showDropdowns: true,
        //                         autoUpdateInput: false,
        //                         singleDatePicker: true,
        //                         autoApply: true,
        //                         minDate: picker.startDate, 
        //                         isInvalidDate: function(date) {
        //                             // Check if the date is in blockedDates
        //                             return blockedDates.some(function(blockedDate) {
        //                                 return date.isSame(blockedDate, 'day');
        //                             });
        //                         },
        //                         // minDate: new Date(),
        //                         // minDate: $('#kt_daterangepicker_5').val(),
        //                         locale: {
        //                             cancelLabel: 'Clear',
        //                             format: 'DD-MM-YYYY'
        //                         }
        //                     });
        //                     $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
        //                         $(this).val(picker.startDate.format('DD-MM-YYYY'));

        //                         var dateAfter = picker.startDate;
        //                         var dateBefore = moment($('#kt_datepicker1').val(), 'DD-MM-YYYY');

        //                         // Check if any blocked dates fall between dateBefore and dateAfter
        //                         var hasBlockedDatesInRange = blockedDates.some(function(blockedDate) {
        //                             return blockedDate.isBetween(dateBefore, dateAfter, 'day', '[]');
        //                         });

        //                         if (hasBlockedDatesInRange) {
        //                             Swal.fire(
        //                                 'Harap Maaf!',
        //                                 'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
        //                                 'error'
        //                                 );
        //                             $('#submit_form').prop('disabled', true);
        //                         } else {
        //                             $('#submit_form').prop('disabled', false);
        //                         }
        //                     });
        //                     $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
        //                         $(this).val('');
        //                     });
        //                 });

        //                 $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
        //                     $(this).val('');
        //                 });
        //             }
        //         }
        //     });
        //     // }
        // });
        // Miscellaneous::Start
        $('.statusPemohon').on('change', function() {
            var selectedStatus = $('.statusPemohon').val();
            if (selectedStatus == '1') {
                $('#maklumatPenganjurUtamaNavItem').hide();
                $('.namaAgensi').removeAttr('required');
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
                $('.namaAgensi').attr('required', true);
            }
        });
        $('.jenisPemohon').on('change', function() {
            var selectedContactPerson = $('.jenisPemohon').val();
            if (selectedContactPerson == '1') {
                $('#contactPersonForm').prop('hidden', true);
                $('#nogstForm').prop('hidden', true);
                $('#contactPersonForm').attr('required', false);
                $('#nogstForm').attr('required', false);
            } else {
                $('#contactPersonForm').prop('hidden', false);
                $('#nogstForm').prop('hidden', false);
                $('#contactPersonForm').attr('required', true);
                $('#nogstForm').attr('required', true);
            }
        })

        $('input[name="namaPemohon"]').on('input', function() {
            $('input[name="namaPemohonPerakuan"]').val($(this).val());
        });
        $('input[name="namaAcara"]').on('input', function() {
            $('input[name="namaAcaraPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraDari"]').on('change', function() {
            $('input[name="tarikhAcaraDariPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraHingga"]').on('change', function() {
            $('input[name="tarikhAcaraHinggaPerakuan"]').val($(this).val());
        $('input[name="tarikhKeluarTapak"]').on('change', function() {
            var tarikhKeluarTapak = new Date($(this).val());
            var tarikhMasukTapak = tarikhMasukTapakPicker.datepicker('getDate');
            var timeDifference = tarikhKeluarTapak.getTime() - tarikhMasukTapak.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;

            $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
        });
        })
        // Miscellaneous::End

        // File Upload::Start
        $('#fileUpload1').on('change', handleFileUpload);
        $('#fileUpload2').on('change', handleFileUpload);
        $('#fileUpload3').on('change', handleFileUpload);
        $('#fileUpload4').on('change', handleFileUpload);

        // Function to handle file upload
        function handleFileUpload(event) {
            const input = event.target;
            const fileId = input.id;
            const fileDisplayId = 'fileDisplay' + fileId.charAt(fileId.length - 1);
            const fileName = input.name;
            const fileDisplay = $('#' + fileDisplayId);

            if (input.files.length > 0) {
                $('#invalid-' + fileName).hide();
                fileDisplay.text(input.files[0].name);
                fileDisplay.css('color', 'green');
            } else {
                $('#invalid-' + fileName).prop('hidden', false);
                fileDisplay.text('No file selected');
                fileDisplay.css('color', 'red');
            }
        }
        // File Upload::End
        // Datepicker Format::Start
        var tarikhAcaraDariPicker = $('.tarikhAcaraDari').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var startDate = new Date();
                startDate.setHours(0, 0, 0, 0); 

                return {
                    enabled: date.valueOf() >= startDate.valueOf(),
                    classes: date.valueOf() >= startDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhAcaraHinggaPicker = $('.tarikhAcaraHingga').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhMasukTapakPicker = $('.tarikhMasukTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() <= selectedDate.valueOf(),
                    classes: date.valueOf() <= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhKeluarTapakPicker = $('.tarikhKeluarTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        // Update startDate for Tarikh Acara Hingga when a date is selected in Tarikh Acara Dari
        tarikhAcaraDariPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            $('.tarikhAcaraHingga').val('');
            $('.tarikhMasukTapak').val('');
            $('.tarikhKeluarTapak').val('');
            tarikhAcaraHinggaPicker.datepicker('setStartDate', selectedDate);
            tarikhMasukTapakPicker.datepicker('setEndDate', selectedDate);
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhAcaraHingga').prop('disabled', false);
            $('.tarikhAcaraHingga').removeAttr('placeholder');
            $('.tarikhMasukTapak').prop('disabled', false);
            $('.tarikhMasukTapak').removeAttr('placeholder');
        });
        // Update startDate for Tarikh Acara Hingga when a date is selected in Tarikh Acara Dari
        tarikhMasukTapakPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhKeluarTapak').prop('disabled', false);
            $('.tarikhKeluarTapak').removeAttr('placeholder');
        });
        // Datepicker Format::End
        
        // Form Switch::Start
        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        // Form Switch::End

        function checkRequiredInput(formId, formValidation) {
            $(formId).each(function() {
                var requiredFields = $(this).find('[name][required]');
                requiredFields.each(function() {
                    var fieldName = $(this).prop('name'); 
                    var fieldValue = $(this).val();
                    if ($(this).is('select')) {
                        if (fieldValue === null || fieldValue === "" || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    } else {
                        // Handle other input elements
                        if (fieldValue === null || fieldValue.trim() === '' || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    }
                });
            })
            return formValidation
        }

        // Form 1 Data::Start
        $('#submitForms').on('click', function(event) {
            var allFormData = '';
            var dataId = $('#myId').data('id');
            var isValid = true;
            var form1IsValid = true;
            var form2IsValid = true;
            var form3IsValid = true;
            var form4IsValid = true;

            var form1IsValid = checkRequiredInput('#form1', form1IsValid)
            var form2IsValid = checkRequiredInput('#form2', form2IsValid)
            var form3IsValid = checkRequiredInput('#form3', form3IsValid)
            var form4IsValid = checkRequiredInput('#form4', form4IsValid)
            var form5IsValid = checkRequiredInput('#form4', form5IsValid)
            if (!form1IsValid) {
                $('#exclamation-butiranPemohon').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranPemohon').hide();
            }
            if (!form2IsValid) {
                $('#exclamation-butiranAcara').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranAcara').hide();
            }
            if (!form3IsValid) {
                $('#exclamation-maklumatPenganjurUtama').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-maklumatPenganjurUtama').hide();
            }
            if (!form4IsValid) {
                $('#exclamation-muatNaikDokumen').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-muatNaikDokumen').hide();
            }
            if (!isValid) {
                event.preventDefault();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;  
            }
            allFormData += $('#form1').serialize() + '&' + $('#form2').serialize() + '&' + $('#form3').serialize() + '&' + $('#form4').serialize() + '&' + $('#form5').serialize();
    
            allFormData = allFormData.slice(0, -1);
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var mainBookingId = 0;
            
            $.ajax({
                url: 'reservation/summary/' + dataId + '/' + mainBookingId,
                type: 'POST',
                data: allFormData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(response) {
                    window.location.href = response.redirectUrl;
                },
                error: function(error) {
                    console.error(error);
                }
            });
        });
        // Form 1 Data::End
    });
</script>
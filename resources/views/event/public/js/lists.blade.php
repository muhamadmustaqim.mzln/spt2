<script src="{{ asset('assets/plugins/custom/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/leaflet/leaflet.bundle.js') }}"></script>
<script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
<!--end::Page Vendors-->
<script>

    filterSelection("all")
    function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("column");
    if (c == "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
    }

    function w3RemoveClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
        arr1.splice(arr1.indexOf(arr2[i]), 1);     
        }
    }
    element.className = arr1.join(" ");
    }


    // Add active class to the current button (highlight it)
    var btnContainer = document.getElementById("myBtnContainer");
    var btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function(){
        var current = document.getElementsByClassName("active1");
        current[0].className = current[0].className.replace(" active1", "");
        this.className += " active1";
    });
    }
</script>
<script>
    $('#searchForm').submit(function (e) {
        // e.preventDefault(); 

        // // Get selected option and input values
        // var lokasi = $('#kt_select2_5').val();
        // var tarikhMula = $('#kt_daterangepicker_5').val();
        // var tarikhAkhir = $('#kt_daterangepicker_4').val();

        // // Encrypt the id
        // var encryptedId = "{{ Crypt::encrypt($data['carian']->id ?? '') }}";
        // var encryptedTarikhMula = "{{ Crypt::encrypt($data['carian']->tarikhMula ?? '') }}";
        // var encryptedTarikhAkhir = "{{ Crypt::encrypt($data['carian']->tarikhAkhir ?? '') }}";


        // // Construct the dynamic URL
        // var dynamicUrl = "{{ url('event/details') }}/" + encodeURIComponent(encryptedId) + "/" + encodeURIComponent(encryptedTarikhMula) + "/" + encodeURIComponent(encryptedTarikhAkhir);

        // // Set the form action and submit
        // $(this).attr('action', dynamicUrl);
        // console.log(lokasi)
        // this.submit();
    });
</script>
<script>
    @if(count($data['pengumuman']) != 0)	
    $(window).on('load', function() {
        $('#exampleModalCustomScrollable').modal('show');

    });
    @endif
</script>
<script>
    function imageExists(url, callback) {
        var img = new Image();
        img.onload = function() {
            callback(true);
        };
        img.onerror = function() {
            callback(false);
        };
        img.src = url;
    }

    $(document).ready(function() {
        $('#kt_select2_5').on('change', function(){
            var location_id = $(this).val();
            $('#kt_datepicker1').prop('disabled', false);
            $('#kt_datepicker2').prop('disabled', false);
            // if (location_id == ''){
            //     $('#kt_select2_5').prop('disabled', true);
            // }
            // else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('event/carianSekatan') }}",
                type: "POST",
                data: {'location_id' : location_id},
                dataType: 'json',
                success: function(data){
                    if (data && data.length > 0) {
                        var blockedDates = data.map(function(item) {
                            return moment(item.date_booking, 'YYYY-MM-DD');  // Adjust date format if needed
                        });

                        var minDate = moment().add(3,'month');

                        $('#kt_datepicker1').daterangepicker({
                            autoApply: true,
                            autoclose: true,
                            showDropdowns: true,
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            minDate: minDate,
                            // maxDate: maxDate.toDate(), // Set the maximum date
                            isInvalidDate: function(date) {
                                // Check if the date is in blockedDates
                                return blockedDates.some(function(blockedDate) {
                                    return date.isSame(blockedDate, 'day');
                                });
                            },
                            locale: {
                                cancelLabel: 'Clear',
                                format: 'DD-MM-YYYY'
                            }
                        });

                        $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
                            $(this).val(picker.startDate.format('DD-MM-YYYY'));
                            $('#kt_datepicker2').prop('disabled', false);

                            var startDate1 = picker.startDate;
                            var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

                            // If startDate1 is greater than startDate2, update startDate2
                            if (startDate1.isAfter(startDate2)) {
                                $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
                                $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
                            }
                            $('#kt_datepicker2').daterangepicker({
                                buttonClasses: ' btn',
                                applyClass: 'btn-primary',
                                cancelClass: 'btn-secondary',
                                showDropdowns: true,
                                autoUpdateInput: false,
                                singleDatePicker: true,
                                autoApply: true,
                                minDate: picker.startDate, 
                                isInvalidDate: function(date) {
                                    // Check if the date is in blockedDates
                                    return blockedDates.some(function(blockedDate) {
                                        return date.isSame(blockedDate, 'day');
                                    });
                                },
                                // minDate: new Date(),
                                // minDate: $('#kt_daterangepicker_5').val(),
                                locale: {
                                    cancelLabel: 'Clear',
                                    format: 'DD-MM-YYYY'
                                }
                            });
                            $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
                                $(this).val(picker.startDate.format('DD-MM-YYYY'));

                                var dateAfter = picker.startDate;
                                var dateBefore = moment($('#kt_datepicker1').val(), 'DD-MM-YYYY');

                                // Check if any blocked dates fall between dateBefore and dateAfter
                                var hasBlockedDatesInRange = blockedDates.some(function(blockedDate) {
                                    return blockedDate.isBetween(dateBefore, dateAfter, 'day', '[]');
                                });

                                if (hasBlockedDatesInRange) {
                                    Swal.fire(
                                        'Harap Maaf!',
                                        'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                                        'error'
                                        );
                                    $('#submit_form').prop('disabled', true);
                                } else {
                                    $('#submit_form').prop('disabled', false);
                                }
                            });
                            $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
                                $(this).val('');
                            });
                        });

                        $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
                            $(this).val('');
                        });
                        
                        $('#kt_datepicker2').daterangepicker({
                            autoApply: true,
                            autoclose: true,
                            showDropdowns: true,
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            minDate: minDate,
                            // maxDate: maxDate.toDate(), // Set the maximum date
                            isInvalidDate: function(date) {
                                // Check if the date is in blockedDates
                                return blockedDates.some(function(blockedDate) {
                                    return date.isSame(blockedDate, 'day');
                                });
                            },
                            locale: {
                                cancelLabel: 'Clear',
                                format: 'DD-MM-YYYY'
                            }
                        });
                    }
                }
            });
            // }
        });
        $('[id^="main_"]').each(function() {
            var mainImage = $(this);
            var id = mainImage.attr('id').split('_')[1]; // Extract the numeric part of the ID
            var tempImage = $('#temp_' + id);
            var tempDivImage = $('#tempdiv_' + id);

            imageExists(mainImage.attr('src'), function(exists) {
                if (exists) {
                    tempImage.hide();
                    tempDivImage.hide();
                    mainImage.show();
                } else {
                    tempImage.show();
                    tempDivImage.show();
                    mainImage.hide();
                }
            });
        });

        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay: true,
            dots: false,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 1,
                }
            }
        });
    });
</script>
<script>
        var peta = <?php echo json_encode($data['fasility']) ?>;
        function pop(gambar, path, nama, p, desc, link){
            var data = "<img style='width: 250px; height: 150px' src='{{ URL::asset('/') }}"+path+"/"+gambar+"'>";
            data += "<p style='text-align: center'>";
            data += "<b>"+nama+"</b><br>";
            data += '<i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><br><br>';
            data += desc;
            data += "<br>";
            data += "<a href='/event/details/"+link+"' class='btn font-weight-bolder btn-outline-primary mt-4'>Teruskan Tempahan</a>";
            data += "</p>";
            return data;
        }

        var cities = L.layerGroup();
        var markers = L.markerClusterGroup({
            iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
                className: 'marker-cluster' + ' marker-cluster-small', iconSize: new L.Point(40, 40) });
            }
        }).addTo(cities);

        var leaflet = L.map('map', {
			center: [2.9264, 101.6964],
			zoom: 11
		});

		// set leaflet tile layer
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		}).addTo(leaflet);

        leaflet.addLayer(markers);

		// set custom SVG icon marker
		var leafletIcon = L.divIcon({
			html: `<span class="svg-icon svg-icon-primary svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
			bgPos: [10, 10],
			iconAnchor: [20, 37],
			popupAnchor: [0, -37],
			className: 'leaflet-marker'
		});

		// bind marker with popup
        for (var p of peta)
        {
            var marker = L.marker(new L.LatLng(p.latitude,p.longitude), { icon: leafletIcon })
            .bindPopup(pop(p.bh_filename, p.bh_file_location, p.name, p.bh_percint, p.remark, p.encrypt));
            markers.addLayer(marker);
        }
                        // pop(gambar, path, nama, p, desc, link)
        var homeTab = document.getElementById('kt_tab_pane_7_3');
        var observer1 = new MutationObserver(function(){
            if(homeTab.style.display != 'none'){
                leaflet.invalidateSize();
            }
        });
        observer1.observe(homeTab, {attributes: true}); 

</script>

<script>
    var events = "{{ url('takwim/call') }}";
    console.log(events);
    "use strict";

var KTCalendarListView = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            var calendarEl = document.getElementById('kt_calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'ms-my',
                plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],

                isRTL: KTUtil.isRTL(),
                header: {
                    left: 'prev,next',
                    center: '',
                    right: 'title'
                },

                height: 500,
                contentHeight: 550,
                aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

                views: {
                    dayGridMonth: { buttonText: 'month' },
                    listWeek: { buttonText: 'list' }
                },

                defaultView: 'listWeek',
                defaultDate: TODAY,
                allDayText: '1 Hari',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: events,

                eventRender: function(info) {
                    var element = $(info.el);

                    if (info.event.extendedProps && info.event.extendedProps.description) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', info.event.extendedProps.description);
                            element.data('placement', 'top');
                            KTApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        }
                    }
                }
            });

            calendar.render();
        }
    };
}();

jQuery(document).ready(function() {
    KTCalendarListView.init();
});
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment().add(1, 'month'),
            // minDate: new Date(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            $('#kt_daterangepicker_4').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                }
            });
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        // $('#kt_daterangepicker_5').on('change.daterangepicker', function(ev, picker) {
        //     $('#kt_daterangepicker_4').val('');
        // });

        $('#kt_daterangepicker_4').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment().add(1, 'month'),
            // minDate: new Date(),
            // minDate: $('#kt_daterangepicker_5').val(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_4').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_4').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
</script>
<script>
   $(function() {
        var minDate = moment().add(3, 'month'); // Calculate the maximum date
        var maxDate = moment().add(6, 'months'); // Calculate the maximum date

        $('#kt_datepicker1').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: minDate.toDate(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            
            var startDate1 = picker.startDate;
            var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;

            // If startDate1 is greater than startDate2, update startDate2
            if (startDate1.isAfter(startDate2)) {
                $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
                $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
            }
            $('#kt_datepicker2').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                showDropdowns: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate, 
                // minDate: new Date(),
                // minDate: $('#kt_daterangepicker_5').val(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                }
            });
            $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
            $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });

        $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker2').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: maxDate,
            // maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
var card = new KTCard('kt_card_3');
card.on('reload', function (card) {
	KTApp.block(card.getSelf(), {
		overlayColor: '#ffffff',
		type: 'loader',
		state: 'primary',
		opacity: 0.3,
		size: 'lg'
	});
	setTimeout(function () {
		KTApp.unblock(card.getSelf());
	}, 2000);
});    
</script>
<script>
    // set the dropzone container id
    var id = '#kt_dropzone_4';

    // set the preview element template
    var previewNode = $(id + " .dropzone-item");
    previewNode.id = "";
    var previewTemplate = previewNode.parent('.dropzone-items').html();
    previewNode.remove();

    var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
        url: "{{ url('file', $data['book']) }}", // Set the url for your upload script location
        //parallelUploads: 20,
        acceptedFiles:"application/pdf, image/*",
        previewTemplate: previewTemplate,
        maxFileSize: 1024,
        //autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: id + " .dropzone-items", // Define the container to display the previews
        clickable: id + " .dropzone-select", // Define the element that should be used as click trigger to select files.
    });

    myDropzone4.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
        $(document).find( id + ' .dropzone-item').css('display', '');
        $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
    });

    // Update the total progress bar
    myDropzone4.on("totaluploadprogress", function(progress) {
        $(this).find( id + " .progress-bar").css('width', progress + "%");
    });

    myDropzone4.on("sending", function(file) {
        // Show the total progress bar when upload starts
        $( id + " .progress-bar").css('opacity', '1');
        // And disable the start button
        file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
    });

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone4.on("complete", function(progress) {
        var thisProgressBar = id + " .dz-complete";
        setTimeout(function(){
            $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
        }, 300)

    });

    // Setup the buttons for all transfers
    document.querySelector( id + " .dropzone-upload").onclick = function() {
        myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
    };

    // Setup the button for remove all files
    document.querySelector(id + " .dropzone-remove-all").onclick = function() {
        $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
        myDropzone4.removeAllFiles(true);
    };

    // On all files completed upload
    myDropzone4.on("queuecomplete", function(progress){
        $( id + " .dropzone-upload").css('display', 'none');
    });

    // On all files removed
    myDropzone4.on("removedfile", function(file){
        var name = file.name; 
        $.ajax({
            type: 'POST',
            //url: "{{ url('sport/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}",
            url: "{{ url('file', $data['book']) }}",
            data: {name: name},
            sucess: function(data){
                console.log('success: ' + data);
            }
        });
        if(myDropzone4.files.length < 1){
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
        }
    });
</script>
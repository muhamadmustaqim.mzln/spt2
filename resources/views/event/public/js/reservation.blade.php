{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script> --}}

<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>
<script>
    $('#kt_select2_4').on('change', function(){
    var location_id = $(this).val();
    if (location_id == ''){
        $('#kt_select2_5').prop('disabled', true);
    }
    else{
        $('#kt_select2_5').prop('disabled', false);
        $.ajax({
            url:"{{ url('sport/ajax') }}",
            type: "POST",
            data: {'location_id' : location_id},
            dataType: 'json',
            success: function(data){
                $('#kt_select2_5').html(data);
            }
        });
    }
    });
</script>
<script>
    // date picker


        $(function() {

            var blockedDates = ['2023-09-10', '2023-09-15', '2023-09-20'];

            // $('#kt_daterangepicker_5').daterangepicker({
            //     autoApply: true,
            //     autoclose: true,
            //     minDate: new Date(),
            //     isInvalidDate: function(date) {
            //         // Convert the date to the format YYYY-MM-DD for comparison
            //         var formattedDate = date.format('YYYY-MM-DD');
                    
            //         // Check if the date is in the blockedDates array
            //         return blockedDates.indexOf(formattedDate) !== -1;
            //     }
            // }, function(start, end, label) {
            // var isValidRange = true;

            // // Iterate through each day in the selected range
            // for (var d = start.clone(); d.isBefore(end); d.add(1, 'day')) {
            //     var formattedDate = d.format('YYYY-MM-DD');
            //     if (blockedDates.indexOf(formattedDate) !== -1) {
            //         isValidRange = false; // Range contains a blocked date
            //         break;
            //     }
            // }

            if (!isValidRange) {
                // Show an error message or take any other action
                Swal.fire(
                    'Harap Maaf!',
                    'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                    'error'
                    );
                $('#kt_daterangepicker_5').data('daterangepicker').setStartDate(moment());
                $('#kt_daterangepicker_5').data('daterangepicker').setEndDate(moment());
            } else {
                // Handle the valid range here
                $('#kt_daterangepicker_5 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
            }
        });
    // });
</script>
<script>
    $(document).ready(function() {
        var tarikhAcaraDariTemp = new Date($('input[name="tarikhAcaraDari"]').val());
        var tarikhAcaraHinggaTemp = new Date($('input[name="tarikhAcaraHingga"]').val());
        if (tarikhAcaraDariTemp != null && tarikhAcaraHinggaTemp != null) {
            var timeDifference = tarikhAcaraHinggaTemp.getTime() - tarikhAcaraDariTemp.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;
            var sewaTapakSehariPerakuan = $('#sewaTapakSehariPerakuan').val();
            // $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
            // $('#jumlahPerakuan').val(numberOfDays * sewaTapakSehariPerakuan);
        }

        var applicant_Type = $('#applicant_type').val();
        updateApplicantType(applicant_Type);
        // Miscellaneous::Start
        function updateApplicantType(val){
            if (val == '1') {
                $('#maklumatPenganjurUtamaNavItem').hide();
                $('.namaAgensi').removeAttr('required');
                $('.namaAgensi').attr('required', false);
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
                $('.namaAgensi').attr('required', true);
            }
        }
        var initialApplicationStatus = $('#initialApplicationStatus').val();
        updateApplicantType(initialApplicationStatus);
        
        $('.statusPemohon').on('change', function() {
            var selectedStatus = $('.statusPemohon').val();
            updateApplicantType(selectedStatus);
        });
        
        $('.jenisPemohon').on('change', function() {
            var selectedContactPerson = $('.jenisPemohon').val();
            if (selectedContactPerson == '1') {
                $('#contactPersonForm').prop('hidden', true);
                $('#nogstForm').prop('hidden', true);
                $('#invalid-contactPerson').prop('hidden', true);
                $('#invalid-nogst').prop('hidden', true);
                $('#contactPersonForm').removeAttr('required');
                $('#nogstForm').removeAttr('required');
            } else {
                $('#contactPersonForm').prop('hidden', false);
                $('#nogstForm').prop('hidden', false);
                $('#contactPersonForm').attr('required', true);
                // $('#nogstForm').attr('required', true);
            }
        })
        $('#peringkatPenganjuranId').on('change', function() {
            var selectedPeringkatPenganjuran = $('#peringkatPenganjuranId').val();
            if (selectedPeringkatPenganjuran == '4') {
                $('#peringkatPenganjuranLainLain').attr('hidden', false);
                $('#peringkatPenganjuranLainLain').attr('required', true);
            } else {
                $('#peringkatPenganjuranLainLain').prop('hidden', true);
                $('#peringkatPenganjuranLainLain').attr('required', false);
            }
        })

        $('input[name="namaPemohon"]').on('input', function() {
            $('input[name="namaPemohonPerakuan"]').val($(this).val());
        });
        $('input[name="namaAcara"]').on('input', function() {
            $('input[name="namaAcaraPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraDari"]').on('change', function() {
            $('input[name="tarikhAcaraDariPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraHingga"]').on('change', function() {
            $('input[name="tarikhAcaraHinggaPerakuan"]').val($(this).val());
            var tarikhAcaraHingga = new Date($(this).val());
        })
        $('input[name="tarikhMasukTapak"]').on('change', function() {
            $('input[name="tarikhMasukTapak"]').val($(this).val());
        })
        $('.tarikhKeluarTapak').on('change', function() {
            $('.tarikhKeluarTapak').val($(this).val());
            var tarikhKeluarTapak = new Date(dateSplit($(this).val()));
            var tarikhMasukTapak = new Date(dateSplit(tarikhMasukTapakPicker.val()));
            var timeDifference = tarikhKeluarTapak.getTime() - tarikhMasukTapak.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;
            var sewaTapakSehariPerakuan = $('#sewaTapakSehariPerakuan').val(); 
            sewaTapakSehariPerakuan = parseFloat(sewaTapakSehariPerakuan.replace(/,/g, ''));
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan * numberOfDays;
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            $('input[name="jumlahHariPenggunaanPerakuan"]').val(numberOfDays);
            $('#jumlahPerakuan').val(sewaTapakSehariPerakuan);
        })
        function dateSplit(dateString){
            const dateParts = dateString.split("-");
            return (dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0])
        }
        $('input[name="masaPenganjuranDari"]').on('change', function() {
            var dari = $(this).val();
            var hingga = $('input[name="masaPenganjuranHingga"]').val();
            $('input[name="masaPenganjuranHingga"]').val(dari)
        })
        $('input[name="masaPenganjuranHingga"]').on('change', function() {
            var dari = $('input[name="masaPenganjuranDari"]').val();
            var hingga = $(this).val();

            // Extract hours and minutes from dari and hingga
            var dariParts = dari.split(':');
            var hinggaParts = hingga.split(':');

            var dariType = dari.split(' ')[1];
            var hinggaType = hingga.split(' ')[1];

            // Parse hours and minutes as integers
            var dariHours = parseInt(dariParts[0]);
            var dariMinutes = parseInt(dariParts[1]);
            var hinggaHours = parseInt(hinggaParts[0]);
            var hinggaMinutes = parseInt(hinggaParts[1]);

            // Compare the hours and minutes
            if (hinggaHours < dariHours || (hinggaHours === dariHours && hinggaMinutes < dariMinutes)) {
                $('input[name="masaPenganjuranDari"]').val(hingga);
            } else if (dariType == 'PM' && hinggaType == 'PM' && hinggaHours == 12) {
                $('input[name="masaPenganjuranDari"]').val(hingga);
            } else if (dariType == 'PM' && hinggaType == 'AM') {
                $('input[name="masaPenganjuranDari"]').val(hingga);
            }
        })
        // Miscellaneous::End

        // File Upload::Start
        $('#fileUpload1').on('change', function (event) {
            handleFileUpload(event, 'Surat Iringan(Cover letter) Permohonan Daripada Penganjur Acara.pdf', 'suratIringan');
        });
        $('#fileUpload2').on('change', function (event) {
            handleFileUpload(event, 'Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan).pdf', 'kertasCadangan');
        });
        $('#fileUpload3').on('change', function (event) {
            handleFileUpload(event, 'Surat Pelantikan Pengurus Acara (jika berkaitan).pdf');
        });
        $('#fileUpload4').on('change', function (event) {
            handleFileUpload(event, 'Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya).pdf');
        });

        // Function to handle file upload
        function handleFileUpload(event, fixedName, invalidName) {
            const input = event.target;
            const fileId = input.id;
            const fileDisplayId = 'fileDisplay' + fileId.charAt(fileId.length - 1);
            const fileName = input.name;
            const fileDisplay = $('#' + fileDisplayId);
            if (input.files.length > 0) {
                $('#invalid-' + invalidName).hide();
                fileDisplay.text(fixedName);
                fileDisplay.css('color', 'green');
            } else {
                $('#invalid-' + invalidName).prop('hidden', false);
                fileDisplay.text('Tiada Fail');
                fileDisplay.css('color', 'red');
            }
        }
        // File Upload::End

        // Datepicker Format::Start
        var startDate = new Date();
        var tarikhAcaraDariPicker = $('.tarikhAcaraDari').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var startDate = new Date();
                startDate.setHours(0, 0, 0, 0); 

                return {
                    enabled: date.valueOf() >= startDate.valueOf(),
                    classes: date.valueOf() >= startDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhAcaraHinggaPicker = $('.tarikhAcaraHingga').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhAcaraDari = $('[name="tarikhAcaraDari"]').val();
        var tarikhAcaraHingga = $('[name="tarikhAcaraHingga"]').val();
        $('.tarikhMasukTapak').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            // minDate: moment().add(1, 'month').subtract(6, 'day'),
            // maxDate: moment().add(1, 'month'),
            minDate: moment(tarikhAcaraDari, 'DD-MM-YYYY').subtract(6, 'day'),
            maxDate: moment(tarikhAcaraHingga, 'DD-MM-YYYY'),
            startDate: moment(tarikhAcaraDari, 'DD-MM-YYYY'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('.tarikhMasukTapak').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            $('.tarikhMasukTapak').val(picker.startDate.format('DD-MM-YYYY'));
            $('.tarikhKeluarTapak').val('');
        });
        $('.tarikhKeluarTapak').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment(tarikhAcaraHingga, 'DD-MM-YYYY'),
            maxDate: moment(tarikhAcaraHingga, 'DD-MM-YYYY').add(1, 'day'),
            startDate: moment(tarikhAcaraHingga, 'DD-MM-YYYY'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('.tarikhKeluarTapak').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            $('.tarikhKeluarTapak').val(picker.startDate.format('DD-MM-YYYY'));
            var tarikhMasukTapakVar = $('.tarikhMasukTapak').val();
            var tarikhKeluarTapakVar = $('.tarikhKeluarTapak').val();
            var tarikhMasukTapak = moment(tarikhMasukTapakVar, 'DD-MM-YYYY');
            var tarikhKeluarTapak = moment(tarikhKeluarTapakVar, 'DD-MM-YYYY');
            var timeDifference = tarikhKeluarTapak.diff(tarikhMasukTapak, 'days');

            var sewaTapakSehariPerakuan = $('#sewaTapakSehariPerakuan').val();
            sewaTapakSehariPerakuan = parseFloat(sewaTapakSehariPerakuan.replace(/,/g, ''));
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan * (timeDifference + 1);
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            $('input[name="jumlahHariPenggunaanPerakuan"]').val(timeDifference + 1);
            $('#jumlahPerakuan').val(sewaTapakSehariPerakuan);
        });

        tarikhAcaraDariPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            $('.tarikhAcaraHingga').val('');
            $('.tarikhMasukTapak').val('');
            $('.tarikhKeluarTapak').val('');
            tarikhAcaraHinggaPicker.datepicker('setStartDate', selectedDate);
            tarikhMasukTapakPicker.datepicker('setEndDate', selectedDate);
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhAcaraHingga').prop('disabled', false);
            $('.tarikhAcaraHingga').removeAttr('placeholder');
            $('.tarikhMasukTapak').prop('disabled', false);
            $('.tarikhMasukTapak').removeAttr('placeholder');
        });
        // tarikhMasukTapakPicker.on('changeDate', function (e) {
        //     var selectedDate = e.date;
        //     tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
        //     $('.tarikhKeluarTapak').prop('disabled', false);
        //     $('.tarikhKeluarTapak').removeAttr('placeholder');
        // });
        // Datepicker Format::End
        
        // Form Switch::Start
        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        // Form Switch::End

        function checkRequiredInput(formId, formValidation) {
            $(formId).each(function() {
                var requiredFields = $(this).find('[name][required]');
                requiredFields.each(function() {
                    var fieldName = $(this).prop('name'); 
                    var fieldValue = $(this).val();
                    if ($(this).is('select')) {
                        if (fieldValue === null || fieldValue === "" || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            console.log(fieldName, 'false1')
                            formValidation = false;
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    } else {
                        // Handle other input elements
                        if (fieldValue === null || fieldValue.trim() === '' || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            console.log(fieldName, 'false2')
                            formValidation = false;
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    }
                });
            })
            return formValidation
        }

        // Form 1 Data::Start
        $('#submitForms').on('click', function(event) {
            var allFormData = '';
            var dataId = $('#myId').data('id');
            var isValid = true;
            var form1IsValid = true;
            var form2IsValid = true;
            var form3IsValid = true;
            var form4IsValid = true;
            var form5IsValid = true;

            var form1IsValid = checkRequiredInput('#form1', form1IsValid)
            var form2IsValid = checkRequiredInput('#form2', form2IsValid)
            var form3IsValid = checkRequiredInput('#form3', form3IsValid)
            var form4IsValid = checkRequiredInput('#form4', form4IsValid)
            var form5IsValid = checkRequiredInput('#form5', form5IsValid)
            
            if (!form1IsValid) {
                $('#exclamation-butiranPemohon').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranPemohon').hide();
            }
            if (!form2IsValid) {
                $('#exclamation-butiranAcara').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranAcara').hide();
            }
            if (!form3IsValid) {
                $('#exclamation-maklumatPenganjurUtama').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-maklumatPenganjurUtama').hide();
            }
            if (!form4IsValid) {
                $('#exclamation-muatNaikDokumen').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-muatNaikDokumen').hide();
            }
            if (!form5IsValid) {
                $('#exclamation-perakuan').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-perakuan').hide();
            }
            allFormData += $('#form1').serialize() + '&' + $('#form2').serialize() + '&' + $('#form3').serialize() + '&' + $('#form4').serialize() + '&' + $('#form5').serialize();
            
            if (!isValid) {
                event.preventDefault();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;  
            }

            var form4Data = new FormData($("#form4")[0]);
            // var hasFiles = false;
            // for (var pair of form4Data.entries()) {
            //     if (pair[1] instanceof File) {
            //         hasFiles = true;
            //         break;
            //     }
            // }
            allFormData = allFormData.slice(0, -1);
            var csrfToken = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '',
                type: 'POST',
                data: allFormData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(response) {
                    $.ajax({
                        // url: '/upload/fileAcara/' + response,
                        url: '/upload/fileAcara/' + response.bookingId,
                        type: 'POST',
                        data: form4Data,
                        processData: false, 
                        contentType: false,
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        success: function(response2) {
                            window.location.href = response.redirectUrl;
                        },
                        error: function(error) {
                        }
                    });
                },
                error: function(error) {
                }
            });
        });
        // Form 1 Data::End
    });
</script>
@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('sport/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
            @csrf
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark">Tempahan Fasiliti</span>
                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                        <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Lokasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($data['datesBetween'] as $s)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $data['fasility']->bh_name }}</td>
                                        <td>08:00 am - 04:00 pm</td>
                                        <td>{{ $s }}</td>
                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark">Maklumat Tempahan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Kategori Kapasiti Dewan</label>
                        <div class="col-10 col-form-label">
                            <div class="radio-list">
                                @if ($data['details']->bhd_option == 0)
                                    <label class="radio">
                                        <input type="radio" value="{{ $data['details']->id }}" checked  name="radios4"/>
                                        <span></span>
                                        Dewan - {{ $data['details']->bhd_capasity }}
                                    </label>  
                                @else
                                    @foreach ($data['capacity'] as $c)
                                        <label class="radio">
                                            <input type="radio" value="{{ $c->id }}" name="radios4"/>
                                            <span></span>
                                            {{ ($c->type == 1) ? "Bankuet" : "Seminar" }} - {{$c->capasity}}
                                        </label>                         
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
					<div class="form-group row">
						<label  class="col-2 col-form-label">Jenis Acara</label>
						<div class="col-10">
						<select class="form-control" id="exampleSelect1">
							<option value="">Sila Pilih Jenis Acara</option>
                            @foreach ($data['event'] as $e)
                                <option value="{{ $e->id }}">{{ $e->le_description }}</option>
                            @endforeach
						</select>
						</div>
					</div>
                    <div class="form-group row">
						<label  class="col-2 col-form-label">Nama Acara</label>
						<div class="col-10">
							<input class="form-control" type="text" />
						</div>
					</div>
                    <div class="form-group row">
						<label  class="col-2 col-form-label">Keterangan Acara</label>
						<div class="col-10">
							<input class="form-control" type="text" />
						</div>
					</div>
                    <div class="form-group row">
						<label  class="col-2 col-form-label">Bilangan Peserta</label>
						<div class="col-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">VVIP</span></div>
                                <input type="text" class="form-control" placeholder="Jumlah" />
                            </div>
						</div>
                        <div class="col-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">VIP</span></div>
                                <input type="text" class="form-control" placeholder="Jumlah" />
                            </div>
						</div>
                        <div class="col-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">Peserta Lain</span></div>
                                <input type="text" class="form-control" placeholder="Jumlah" />
                            </div>
						</div>
					</div>
                    <div class="form-group">
						<div class="alert alert-custom alert-default" role="alert">
							<div class="alert-icon"><span class="svg-icon svg-icon-warning svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo12/dist/../src/media/svg/icons/Code/Warning-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"/>
                                    <rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"/>
                                    <rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"/>
                                </g>
                            </svg><!--end::Svg Icon--></span></div>
							<div class="alert-text">Sekiranya Anda tergolong pada kategori ini:
                                <ul>
                                    <li>Anggota PPj, K/Tangan & Pesara PPj, Anggota PPj, Pegawai Kader & Kontrak PPj</li>
                                    <li>Warga Kerja Kementerian Wilayah Persekutuan</li>
                                    <li>Kementerian & Agensi Kerajaan IPTA/ Sekolah</li>
                                </ul>
                                Sila lampirkan dokumen sokongan dengan muat naik dokumen tersebut dibawah untuk tujuan semakan diskaun sekiranya memenuhi syarat-syarat yang ditetapkan oleh Perbadanan Putrajaya.
                            </div>
						</div>
					</div>
                    <div class="form-group row">
						<label  class="col-2 col-form-label">Jenis Tempahan</label>
						<div class="col-10">
						<select class="form-control" id="exampleSelect1">
							<option value="">Sila Pilih Jenis Tempahan</option>
                            @foreach ($data['discount'] as $d)
                                <option value="{{ $d->id }}">{{ $d->ldt_user_cat }}</option>
                            @endforeach
						</select>
						</div>
					</div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Dokumen Sokongan</label>
                        <div class="col-lg-10">
                                <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                    <div class="dropzone-panel mb-lg-0 mb-2">
                                        <a class="dropzone-select btn btn-light-primary font-weight-bold btn-sm">Lampiran Dokumen</a>
                                        <a class="dropzone-upload btn btn-light-primary font-weight-bold btn-sm">Upload All</a>
                                        <a class="dropzone-remove-all btn btn-light-primary font-weight-bold btn-sm">Remove All</a>
                                    </div>
                                    <div class="dropzone-items">
                                        <div class="dropzone-item" style="display:none">
                                            <div class="dropzone-file">
                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                    <span data-dz-name="">some_image_file_name.jpg</span>
                                                    <strong>( 
                                                    <span data-dz-size="">340kb</span>)</strong>
                                                </div>
                                                <div class="dropzone-error" data-dz-errormessage=""></div>
                                            </div>
                                            <div class="dropzone-progress">
                                                <div class="progress">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                </div>
                                            </div>
                                            <div class="dropzone-toolbar">
                                                <span class="dropzone-start">
                                                    <i class="flaticon2-arrow"></i>
                                                </span>
                                                <span class="dropzone-Cancel" data-dz-remove="" style="display: none;">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                                <span class="dropzone-delete" data-dz-remove="">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="form-text text-muted">Fail dalam bentuk PDF dan gambar sahaja.</span>
                            </div>
                    </div>
                </div>
            </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('hall.public.js.slot')
@endsection




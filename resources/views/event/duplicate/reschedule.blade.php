@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Bertindan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Bertindan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                            <div class="card-body pt-2">
                                <hr>
                                <div class="form-group row">
                                    <div class="col-lg-4">
										<select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
											<option value="">Sila Pilih Lokasi</option>
											{{-- @foreach ($data['location'] as $l)
												<option value="{{ $l->id }}">{{ $l->name }}</option>
											@endforeach --}}
										</select>
									</div>
                                    <div class="col-lg-3">
                                        <input type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh" autocomplete="off" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="tarikhHingga" class="form-control mt-3" id="kt_daterangepicker_4" placeholder="Tarikh Hingga" autocomplete="off" required>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="float-right">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    {{-- @if ($data['post'] == true) --}}
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <h3 class="card-label">Senarai Tempahan Bertindan</h3>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <p><b>Lokasi : {{ Helper::locationspa($data['lokasi']) }}</b></p>
                                <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                    <thead> 
                                        <tr>
                                            <th>Bil</th>
                                            <th>No Tempahan</th>
                                            <th>Nama Acara</th>
                                            <th>Status Tempahan</th>
                                            <th>Tarikh Mula Acara</th>
                                            <th>Tarikh Tamat Acara</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;  
                                        @endphp
                                        @foreach ($data['slot'] as $s)
                                            <tr>
                                                <td style="width: 5%;">{{ $i++ }}</td>
                                                <td><a href="{{url('/event/reschedule/event',Crypt::encrypt($s->bmb_booking_no))}}">{{ $s->bmb_booking_no }}</a></td>
                                                <td>{{ $s->event_name }}</td>
                                                <td>{{ Helper::get_status_tempahan($s->fk_lkp_status) }}</td>
                                                <td>{{ Helper::date_format($s->event_date) }}</td>
                                                <td>{{ Helper::date_format($s->event_date_end) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    {{-- @endif --}}
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        
    </div>
    <!--end::Content-->
    <!-- Button trigger modal-->	

@endsection

@section('js_content')
    @include('event.duplicate.js.index')
@endsection
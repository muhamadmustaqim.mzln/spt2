@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Lokasi</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Acara</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Lokasi</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/event/admin/location/add') }}" id="form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Tambah Lokasi</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Lokasi :</label>
                            <div class="col-lg-10">
                                <input required type="text" class="form-control" name="location" placeholder="Nama Lokasi" />
                            </div>
                        </div><div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori :</label>
                            <div class="col-lg-10">
                                <select required class="form-control select2" id="kt_select2_1" name="category">
                                    <option value="" disabled selected>Sila Pilih</option>
                                    <option value="1">Platinum</option>
                                    <option value="2">Emas</option>
                                    <option value="3">Perak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kapasiti :</label>
                            <div class="col-lg-10">
                                <input required type="text" class="form-control" name="capacity" placeholder="Kapasiti" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Deposit (RM):</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control" name="deposit" id="deposit" placeholder="Deposit" step="0.01" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kadar Sewa Sehari (RM):</label>
                            <div class="col-lg-10">
                                <input type="number" class="form-control" name="rent_perday" id="deposit" placeholder="Kadar Sewa Sehari" step="0.01" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Latitud :</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="latitude" placeholder="Latitud" value=""/>
                            </div>
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Longitud :</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="longitude" placeholder="Longitud" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Sorotan :</label>
                            <div class="col-lg-10">
                                <textarea name="remark" class="form-control" rows="7"></textarea>
                                <span class="form-text text-muted">*Penerangan Fasiliti secara terperinci.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Gambar Muka Hadapan :</label>
                            <div class="col-lg-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="file" accept="image/*" onchange="loadFile(event)"/>
                                    <label class="custom-file-label" for="customFile">Pilih Fail</label>
                                </div>
                                <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                                {{-- <img id="output" src="{{ URL::asset("assets/upload/main/{$data['list']->eft_uuid}/{$data['list']->cover_img}") }}" style="width: 250px; height: 200px;"/> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lawatan Maya :</label>
                            <div class="col-lg-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="file1" accept="image/*" onchange="loadFile1(event)"/>
                                    <label class="custom-file-label" for="customFile">Pilih Fail</label>
                                </div>
                                <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                                {{-- <img id="output1" src="{{ URL::asset("assets/upload/virtual/{$data['list']->eft_uuid}/{$data['list']->virtual_img}") }}" style="width: 250px; height: 200px;"/> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" checked="checked" id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    <label class="ml-2 text-muted" id="textbox1">Aktif</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/event/admin/location') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('event.location.js.form')
@endsection
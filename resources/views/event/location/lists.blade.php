@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Lokasi</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Acara</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Lokasi</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Lokasi</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/event/admin/location/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i> Tambah Lokasi
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Lokasi</th>
                                    <th>Kategori Lokasi</th>
                                    <th>Sewa Tapak (RM)</th>
                                    <th>Deposit (RM)</th>
                                    <th>Kapasiti</th>
                                    <th>Status Lokasi</th>
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($data['location'] as $l)
                                <tr>
                                    <td style="width: 5%;">{{ $i++ }}</td>
                                    <td>{{ $l->name }}</td>
                                    <td>{{ Helper::categoryspa($l->category) }}</td>
                                    <td>{{ Helper::moneyhelper($l->rent_perday) }}</td>
                                    <td>{{ ($l->deposit) }}</td>
                                    {{-- <td>{{ Helper::moneyhelper($l->deposit) }}</td> --}}
                                    <td>{{ $l->capacity }}</td>
                                    <td>{{ Helper::status($l->status) }}</td>
                                    <td style="width: 15%;">
                                        <a class="btn btn-outline-primary btn-sm m-1" href="{{url('/event/admin/location/edit',Crypt::encrypt($l->id))}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Kemaskini
                                        </a>
                                        {{-- @if ($l->status == 1)
                                            <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('/event/admin/location/delete',Crypt::encrypt($l->id))}}">
                                                <i class="fas fa-trash">
                                                </i>
                                                Nyah Aktif
                                            </a>
                                        @else
                                            <a class="btn btn-outline-success btn-sm btn-delete m-1" href="{{url('/event/admin/location/delete',Crypt::encrypt($l->id))}}">
                                                <i class="fas fa-trash">
                                                </i>
                                                Aktif
                                            </a>
                                        @endif --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('event.location.js.lists')
@endsection
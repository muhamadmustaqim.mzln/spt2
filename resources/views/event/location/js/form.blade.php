
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                },
                category: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Kategori'
                        }
                    }
                },
                capacity: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Kapasiti Lokasi'
                        }
                    }
                },
                deposit: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Jumlah Deposit'
                        }
                    }
                },
                rent_perday: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Kadar Sewa Sehari'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $('#deposit, #rent_perday').on('input', function(e) {
        var inputValue = this.value;
        if (inputValue.indexOf('.') !== -1 && inputValue.split('.')[1].length > 2) {
            this.value = inputValue.slice(0, -1);
        }
    })
</script>
<script>
    $('#checkbox1').on('change', function() { 
        // From the other examples
        if (!this.checked) {
            $('#textbox1').text("Tidak Aktif");
        }else{
            $('#textbox1').text("Aktif");
        }
    });
</script>
<script>
        $('#kt_select2_1').select2({
            placeholder: 'Sila Pilih Kategori'
        });

        $('#kt_select2_2').select2({
            placeholder: 'Sila Pilih Lokasi'
        });

        $('#kt_select2_3').select2({
            placeholder: 'Sila Pilih Status'
        });

        $('#kt_select2_4').select2({
            placeholder: 'Sila Pilih Fasiliti Berkait'
        });
</script>
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        $("#output").css('display', 'block');
      }
    };
</script>
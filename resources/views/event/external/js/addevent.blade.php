{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script> --}}

<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>
<script>
    // console.log(events);
    "use strict";

    var KTCalendarBasic = function() {

        return {
            //main function to initiate the module
            init: function() {
                var todayDate = moment().startOf('day');
                var YM = todayDate.format('YYYY-MM');
                var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
                var TODAY = todayDate.format('YYYY-MM-DD');
                var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

                var calendarEl = document.getElementById('kt_calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                    themeSystem: 'bootstrap',

                    isRTL: KTUtil.isRTL(),

                    header: {
                        left: 'prev,next ',
                        center: 'title',
                        right: 'today'
                    },

                    height: 800,
                    contentHeight: 780,
                    aspectRatio: 3, 

                    nowIndicator: true,
                    now: TODAY + 'T09:25:00', // just for demo

                    views: {
                        dayGridMonth: { buttonText: 'month' }
                    },

                    defaultView: 'dayGridMonth',
                    defaultDate: TODAY,
                    locale: 'ms-my',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    navLinks: true,
                    events: events,

                    eventRender: function(info) {
                        var element = $(info.el);

                        if (info.event.extendedProps && info.event.extendedProps.description) {
                            if (element.hasClass('fc-day-grid-event')) {
                                element.data('content', info.event.extendedProps.description);
                                element.data('placement', 'top');
                                KTApp.initPopover(element);
                            } else if (element.hasClass('fc-time-grid-event')) {
                                element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            } else if (element.find('.fc-list-item-title').lenght !== 0) {
                                element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                            }
                        }
                    }
                });

                calendar.render();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTCalendarBasic.init();
    });
</script>
<script>
    // date picker


    $(function() {

        var blockedDates = ['2023-09-10', '2023-09-15', '2023-09-20'];

        $('#kt_daterangepicker_5').daterangepicker({
            autoApply: true,
            autoclose: true,
            minDate: new Date(),
            isInvalidDate: function(date) {
                // Convert the date to the format YYYY-MM-DD for comparison
                var formattedDate = date.format('YYYY-MM-DD');
                
                // Check if the date is in the blockedDates array
                return blockedDates.indexOf(formattedDate) !== -1;
            }
        }, function(start, end, label) {
        var isValidRange = true;

        // Iterate through each day in the selected range
        for (var d = start.clone(); d.isBefore(end); d.add(1, 'day')) {
            var formattedDate = d.format('YYYY-MM-DD');
            if (blockedDates.indexOf(formattedDate) !== -1) {
                isValidRange = false; // Range contains a blocked date
                break;
            }
        }

        if (!isValidRange) {
            // Show an error message or take any other action
            Swal.fire(
                'Harap Maaf!',
                'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                'error'
                );
            $('#kt_daterangepicker_5').data('daterangepicker').setStartDate(moment());
            $('#kt_daterangepicker_5').data('daterangepicker').setEndDate(moment());
        } else {
            // Handle the valid range here
            $('#kt_daterangepicker_5 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        }
    });
});
</script>
<script>
    $(document).ready(function() {
        var tarikhAcaraDariTemp = new Date($('input[name="tarikhAcaraDari"]').val());
        var tarikhAcaraHinggaTemp = new Date($('input[name="tarikhAcaraHingga"]').val());
        if (tarikhAcaraDariTemp != null && tarikhAcaraHinggaTemp != null) {
            var timeDifference = tarikhAcaraHinggaTemp.getTime() - tarikhAcaraDariTemp.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;
            $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
            // $('#jumlahPerakuan').val(numberOfDays);
        }
        // Miscellaneous::Start
        $('.statusPemohon').on('change', function() {
            var selectedStatus = $('.statusPemohon').val();
            if (selectedStatus == '1') {
                $('#maklumatPenganjurUtamaNavItem').hide();
                $('.namaAgensi').removeAttr('required');
                $('.namaAgensi').attr('required', false);
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
                $('.namaAgensi').attr('required', true);
            }
        });
        $('.jenisPemohon').on('change', function() {
            var selectedContactPerson = $('.jenisPemohon').val();
            if (selectedContactPerson == '1') {
                $('#contactPersonForm').prop('hidden', true);
                $('#nogstForm').prop('hidden', true);
                $('#contactPersonForm').attr('required', false);
                $('#nogstForm').attr('required', false);
            } else {
                $('#contactPersonForm').prop('hidden', false);
                $('#nogstForm').prop('hidden', false);
                $('#contactPersonForm').attr('required', true);
                $('#nogstForm').attr('required', true);
            }
        })
        $('#peringkatPenganjuranId').on('change', function() {
            var selectedPeringkatPenganjuran = $('#peringkatPenganjuranId').val();
            if (selectedPeringkatPenganjuran == '4') {
                $('#peringkatPenganjuranLainLain').attr('hidden', false);
                $('#peringkatPenganjuranLainLain').attr('required', true);
            } else {
                $('#peringkatPenganjuranLainLain').prop('hidden', true);
                $('#peringkatPenganjuranLainLain').attr('required', false);
            }
        })

        $('input[name="namaPemohon"]').on('input', function() {
            $('input[name="namaPemohonPerakuan"]').val($(this).val());
        });
        $('input[name="namaAcara"]').on('input', function() {
            $('input[name="namaAcaraPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraDari"]').on('change', function() {
            $('input[name="tarikhAcaraDariPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraHingga"]').on('change', function() {
            $('input[name="tarikhAcaraHinggaPerakuan"]').val($(this).val());
            var tarikhAcaraHingga = new Date($(this).val());
            var tarikhAcaraDari = tarikhAcaraDariPicker.datepicker('getDate');
            var timeDifference = tarikhAcaraHingga.getTime() - tarikhAcaraDari.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;
            var sewaTapakSehariPerakuan = $('#sewaTapakSehariPerakuan').val();
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan * numberOfDays;
            sewaTapakSehariPerakuan = sewaTapakSehariPerakuan.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
            $('#jumlahPerakuan').val(sewaTapakSehariPerakuan);
            $('input[name="tarikhKeluarTapak"]').on('change', function() {
            });
        })
        // Miscellaneous::End

        // File Upload::Start
        $('#fileUpload1').on('change', function (event) {
            handleFileUpload(event, 'Surat Iringan(Cover letter) Permohonan Daripada Penganjur Acara.pdf', 'suratIringan');
        });
        $('#fileUpload2').on('change', function (event) {
            handleFileUpload(event, 'Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan).pdf'), 'kertasCadangan';
        });
        $('#fileUpload3').on('change', function (event) {
            handleFileUpload(event, 'Surat Pelantikan Pengurus Acara (jika berkaitan).pdf');
        });
        $('#fileUpload4').on('change', function (event) {
            handleFileUpload(event, 'Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya).pdf');
        });

        // Function to handle file upload
        function handleFileUpload(event, fixedName, invalidName) {
            const input = event.target;
            const fileId = input.id;
            const fileDisplayId = 'fileDisplay' + fileId.charAt(fileId.length - 1);
            const fileName = input.name;
            const fileDisplay = $('#' + fileDisplayId);
            console.log(invalidName)
            if (input.files.length > 0) {
                $('#invalid-' + invalidName).hide();
                fileDisplay.text(fixedName);
                fileDisplay.css('color', 'green');
            } else {
                $('#invalid-' + invalidName).prop('hidden', false);
                fileDisplay.text('No file selected');
                fileDisplay.css('color', 'red');
            }
        }
        // File Upload::End

        // Datepicker Format::Start
        var startDate = new Date();
        startDate.setHours(0, 0, 0, 0); 
        startDate.setMonth(startDate.getMonth() + 1);

        var minDate = moment().add(3, 'month'); // Calculate the maximum date
        var maxDate = moment().add(6, 'months'); // Calculate the maximum date

        $('#kt_datepicker1').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: minDate.toDate(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            $('#kt_datepicker3').val(picker.startDate.format('DD-MM-YYYY'));
            
            var startDate1 = picker.startDate;
            var startDate2 = $('#kt_datepicker2').data('daterangepicker').startDate;
            var minDate3 = picker.startDate.clone().subtract(7, 'days').format('DD-MM-YYYY');
            var maxDate3 = picker.startDate.clone().add(2, 'days').format('DD-MM-YYYY');

            if (startDate1.isAfter(startDate2)) {
                $('#kt_datepicker2').data('daterangepicker').setStartDate(startDate1);
                $('#kt_datepicker2').val(startDate1.format('DD-MM-YYYY'));
                var maxDate2 = picker.startDate.clone().add(2, 'days').format('DD-MM-YYYY');
            }else {
                var maxDate2 = picker.startDate.clone().add(2, 'days').format('DD-MM-YYYY');
            }
            $('#kt_datepicker2').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                showDropdowns: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate, 
                maxDate: maxDate2, 
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                }
            });
            $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
                
                var minDate4 = picker.startDate;
                var maxDate4 = picker.startDate.clone().add(2, 'days').format('DD-MM-YYYY');
                
                $('#kt_datepicker4').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',
                    showDropdowns: true,
                    autoUpdateInput: false,
                    singleDatePicker: true,
                    autoApply: true,
                    minDate: minDate4, 
                    maxDate: maxDate4, 
                    // minDate: new Date(),
                    // minDate: $('#kt_daterangepicker_5').val(),
                    locale: {
                        cancelLabel: 'Clear',
                        format: 'DD-MM-YYYY'
                    }
                });
                $('#kt_datepicker4').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY'));
                });
                $('#kt_datepicker4').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
            });
            $('#kt_datepicker2').on('cancel.daterangepicker'
            , function(ev, picker) {
                $(this).val('');
            });
            
            $('#kt_datepicker3').daterangepicker({
                autoApply: true,
                autoclose: true,
                showDropdowns: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                minDate: minDate3, 
                maxDate: maxDate3, 
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                }
            });
            $('#kt_datepicker3').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));

                var minDate4 = $('#kt_datepicker2').val();
                var maxDate4 = $('#kt_datepicker2').val().add(2, 'days').format('DD-MM-YYYY');
                
                $('#kt_datepicker4').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',
                    showDropdowns: true,
                    autoUpdateInput: false,
                    singleDatePicker: true,
                    autoApply: true,
                    minDate: minDate4, 
                    maxDate: maxDate4, 
                    // minDate: new Date(),
                    // minDate: $('#kt_daterangepicker_5').val(),
                    locale: {
                        cancelLabel: 'Clear',
                        format: 'DD-MM-YYYY'
                    }
                });
                $('#kt_datepicker4').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY'));
                });
                $('#kt_datepicker4').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
            });
            $('#kt_datepicker3').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
        $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('#kt_datepicker2').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: minDate.toDate(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        
        var kt_datepicker1 = moment($('#kt_datepicker1').val(), 'DD-MM-YYYY');
        var kt_datepicker1minus6 = kt_datepicker1.clone().subtract(6, 'days');
        var kt_datepicker2 = moment($('#kt_datepicker2').val(), 'DD-MM-YYYY');
        var kt_datepicker2add1 = kt_datepicker2.clone().add(1, 'day');

        $('#kt_datepicker3').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: kt_datepicker1minus6.toDate(),
            maxDate: kt_datepicker1.toDate(),
            // minDate: kt_datepicker1.subtract(6, 'days'),
            // maxDate: maxDate.toDate(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_datepicker3').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
            
            var startDate1 = picker.startDate;
            var startDate2 = $('#kt_datepicker4').data('daterangepicker').startDate;

            // If startDate1 is greater than startDate2, update startDate2
            if (startDate1.isAfter(startDate2)) {
                $('#kt_datepicker4').data('daterangepicker').setStartDate(startDate1);
                $('#kt_datepicker4').val(startDate1.format('DD-MM-YYYY'));
            }
            $('#kt_datepicker4').daterangepicker({
                buttonClasses: ' btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary',
                showDropdowns: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate, 
                // minDate: new Date(),
                // minDate: $('#kt_daterangepicker_5').val(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                }
            });
            $('#kt_datepicker4').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
            $('#kt_datepicker4').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
        $('#kt_datepicker3').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('#kt_datepicker4').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: kt_datepicker2.toDate(),
            maxDate: kt_datepicker2add1.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });
        $('#kt_datepicker4').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('#kt_datepicker4').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        // Datepicker Format::Start
        var startDate = new Date();
        var tarikhAcaraDariPicker = $('.tarikhAcaraDari').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var startDate = new Date();
                startDate.setHours(0, 0, 0, 0); 

                return {
                    enabled: date.valueOf() >= startDate.valueOf(),
                    classes: date.valueOf() >= startDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhAcaraHinggaPicker = $('.tarikhAcaraHingga').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhMasukTapakPicker = $('.tarikhMasukTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }

                var maxDate = new Date(selectedDate);
                maxDate.setDate(selectedDate.getDate() - 2);
                return {
                    enabled: date.valueOf() <= selectedDate.valueOf() && date.valueOf() >= maxDate.valueOf(),
                    classes: date.valueOf() <= selectedDate.valueOf() && date.valueOf() >= maxDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhKeluarTapakPicker = $('.tarikhKeluarTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraHinggaPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }

                var maxDate = new Date(selectedDate);
                maxDate.setDate(selectedDate.getDate() + 1);
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf() && date.valueOf() <= maxDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() && date.valueOf() <= maxDate.valueOf() ? 'highlight' : ''
                };
            }
        })

        tarikhAcaraDariPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            $('.tarikhAcaraHingga').val('');
            $('.tarikhMasukTapak').val('');
            $('.tarikhKeluarTapak').val('');
            tarikhAcaraHinggaPicker.datepicker('setStartDate', selectedDate);
            tarikhMasukTapakPicker.datepicker('setEndDate', selectedDate);
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhAcaraHingga').prop('disabled', false);
            $('.tarikhAcaraHingga').removeAttr('placeholder');
            $('.tarikhMasukTapak').prop('disabled', false);
            $('.tarikhMasukTapak').removeAttr('placeholder');
        });
        tarikhMasukTapakPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhKeluarTapak').prop('disabled', false);
            $('.tarikhKeluarTapak').removeAttr('placeholder');
        });
        // Datepicker Format::End
        
        // Form Switch::Start
        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        // Form Switch::End

        function checkRequiredInput(formId, formValidation) {
            $(formId).each(function() {
                var requiredFields = $(this).find('[name][required]');
                requiredFields.each(function() {
                    var fieldName = $(this).prop('name'); 
                    var fieldValue = $(this).val();
                    if ($(this).is('select')) {
                        if (fieldValue === null || fieldValue === "" || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                            console.log(fieldName)
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    } else {
                        // Handle other input elements
                        if (fieldValue === null || fieldValue.trim() === '' || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                            console.log(fieldName)
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    }
                });
            })
            return formValidation
        }

        $('#hantar').on('click', function() {
            console.log('test');
        })

        // Form 1 Data::Start
        $('#submitForms').on('click', function(event) {
            var allFormData = '';
            var dataId = $('#myId').data('id');
            var isValid = true;
            var form1IsValid = true;
            var form2IsValid = true;
            var form3IsValid = true;
            var form4IsValid = true;

            var form1IsValid = checkRequiredInput('#form1', form1IsValid)
            var form2IsValid = checkRequiredInput('#form2', form2IsValid)
            var form3IsValid = checkRequiredInput('#form3', form3IsValid)
            var form4IsValid = checkRequiredInput('#form4', form4IsValid)
            var form5IsValid = checkRequiredInput('#form4', form5IsValid)
            if (!form1IsValid) {
                $('#exclamation-butiranPemohon').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranPemohon').hide();
            }
            if (!form2IsValid) {
                $('#exclamation-butiranAcara').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranAcara').hide();
            }
            if (!form3IsValid) {
                $('#exclamation-maklumatPenganjurUtama').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-maklumatPenganjurUtama').hide();
            }
            if (!form4IsValid) {
                $('#exclamation-muatNaikDokumen').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-muatNaikDokumen').hide();
            }
            if (!isValid) {
                event.preventDefault();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;  
            }
            var form4Data = new FormData($("#form4")[0]);
            allFormData += $('#form1').serialize() + '&' + $('#form2').serialize() + '&' + $('#form3').serialize() + '&' + $('#form4').serialize() + '&' + $('#form5').serialize();
            
            allFormData = allFormData.slice(0, -1);
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var mainBookingId = 0;

            $.ajax({
                url: '',
                type: 'POST',
                data: allFormData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(response) {
                    $.ajax({
                        url: '/upload/fileAcara/' + response.bookingId,
                        type: 'POST',
                        data: form4Data,
                        processData: false, 
                        contentType: false,
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        success: function(response2) {
                            window.location.href = response.redirectUrl;
                        },
                        error: function(error) {
                            console.log('error: ' + error);
                        }
                    });
                }
                ,
                error: function(error) {
                    console.log(error);
                }
            });
        });
        // Form 1 Data::End
    });
</script>
{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>
<script>

jQuery(document).ready(function() {
    KTCalendarBasic.init();
});
</script>
<script>
    $('#kt_select2_4').on('change', function(){
    var location_id = $(this).val();
    if (location_id == ''){
        $('#kt_select2_5').prop('disabled', true);
    }
    else{
        $('#kt_select2_5').prop('disabled', false);
        $.ajax({
            url:"{{ url('sport/ajax') }}",
            type: "POST",
            data: {'location_id' : location_id},
            dataType: 'json',
            success: function(data){
                $('#kt_select2_5').html(data);
            }
        });
    }
});
</script>
<script>
    // date picker


    $(function() {

        var blockedDates = ['2023-09-10', '2023-09-15', '2023-09-20'];

        $('#kt_daterangepicker_5').daterangepicker({
            autoApply: true,
            autoclose: true,
            minDate: new Date(),
            isInvalidDate: function(date) {
                // Convert the date to the format YYYY-MM-DD for comparison
                var formattedDate = date.format('YYYY-MM-DD');
                
                // Check if the date is in the blockedDates array
                return blockedDates.indexOf(formattedDate) !== -1;
            }
        }, function(start, end, label) {
        var isValidRange = true;

        // Iterate through each day in the selected range
        for (var d = start.clone(); d.isBefore(end); d.add(1, 'day')) {
            var formattedDate = d.format('YYYY-MM-DD');
            if (blockedDates.indexOf(formattedDate) !== -1) {
                isValidRange = false; // Range contains a blocked date
                break;
            }
        }

        if (!isValidRange) {
            // Show an error message or take any other action
            Swal.fire(
                'Harap Maaf!',
                'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                'error'
                );
            $('#kt_daterangepicker_5').data('daterangepicker').setStartDate(moment());
            $('#kt_daterangepicker_5').data('daterangepicker').setEndDate(moment());
        } else {
            // Handle the valid range here
            $('#kt_daterangepicker_5 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        }
    });
});
</script>
<script>
    $(document).ready(function() {
        var tarikhAcaraDariTemp = new Date($('input[name="tarikhAcaraDari"]').val());
        var tarikhAcaraHinggaTemp = new Date($('input[name="tarikhAcaraHingga"]').val());
        if (tarikhAcaraDariTemp != null && tarikhAcaraHinggaTemp != null) {
            var timeDifference = tarikhAcaraHinggaTemp.getTime() - tarikhAcaraDariTemp.getTime();
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;
            $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
            $('#jumlahPerakuan').val(numberOfDays);
        }
        // Miscellaneous::Start
        $('.statusPemohon').on('change', function() {
            var selectedStatus = $('.statusPemohon').val();
            if (selectedStatus == '1') {
                $('#maklumatPenganjurUtamaNavItem').hide();
                $('.namaAgensi').removeAttr('required');
                $('.namaAgensi').attr('required', false);
            } else {
                $('#maklumatPenganjurUtamaNavItem').show();
                $('.namaAgensi').attr('required', true);
            }
        });
        $('.jenisPemohon').on('change', function() {
            var selectedContactPerson = $('.jenisPemohon').val();
            if (selectedContactPerson == '1') {
                $('#contactPersonForm').prop('hidden', true);
                $('#nogstForm').prop('hidden', true);
                $('#contactPersonForm').attr('required', false);
                $('#nogstForm').attr('required', false);
            } else {
                $('#contactPersonForm').prop('hidden', false);
                $('#nogstForm').prop('hidden', false);
                $('#contactPersonForm').attr('required', true);
                $('#nogstForm').attr('required', true);
            }
        })
        $('#peringkatPenganjuranId').on('change', function() {
            var selectedPeringkatPenganjuran = $('#peringkatPenganjuranId').val();
            console.log('test peringkatPenganjuran', selectedPeringkatPenganjuran)
            if (selectedPeringkatPenganjuran == '4') {
                $('#peringkatPenganjuranLainLain').attr('hidden', false);
                $('#peringkatPenganjuranLainLain').attr('required', true);
            } else {
                $('#peringkatPenganjuranLainLain').prop('hidden', true);
                $('#peringkatPenganjuranLainLain').attr('required', false);
            }
        })

        $('input[name="namaPemohon"]').on('input', function() {
            $('input[name="namaPemohonPerakuan"]').val($(this).val());
        });
        $('input[name="namaAcara"]').on('input', function() {
            $('input[name="namaAcaraPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraDari"]').on('change', function() {
            $('input[name="tarikhAcaraDariPerakuan"]').val($(this).val());
        });
        $('input[name="tarikhAcaraHingga"]').on('change', function() {
            $('input[name="tarikhAcaraHinggaPerakuan"]').val($(this).val());
            var tarikhAcaraHingga = new Date($(this).val());
            var tarikhAcaraDari = tarikhAcaraDariPicker.datepicker('getDate');
            var timeDifference = tarikhAcaraHingga.getTime() - tarikhAcaraDari.getTime();
            console.log(tarikhAcaraDari, tarikhAcaraHingga)
            var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;

            $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
            $('#jumlahPerakuan').val(numberOfDays);
            $('input[name="tarikhKeluarTapak"]').on('change', function() {
                // var tarikhKeluarTapak = new Date($(this).val());
                // var tarikhMasukTapak = tarikhMasukTapakPicker.datepicker('getDate');
                // var timeDifference = tarikhKeluarTapak.getTime() - tarikhMasukTapak.getTime();
                // var numberOfDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1;

                // $('#jumlahHariPenggunaanPerakuan').val(numberOfDays);
                // $('#jumlahPerakuan').val(numberOfDays);
            });
        })
        // Miscellaneous::End

        // File Upload::Start
        $('#fileUpload1').on('change', function (event) {
            handleFileUpload(event, 'Surat Iringan(Cover letter) Permohonan Daripada Penganjur Acara.pdf', 'suratIringan');
        });
        $('#fileUpload2').on('change', function (event) {
            handleFileUpload(event, 'Kertas Cadangan Penganjuran Acara (pelan tapak, jadual acara dan lain-lain yang berkaitan).pdf'), 'kertasCadangan';
        });
        $('#fileUpload3').on('change', function (event) {
            handleFileUpload(event, 'Surat Pelantikan Pengurus Acara (jika berkaitan).pdf');
        });
        $('#fileUpload4').on('change', function (event) {
            handleFileUpload(event, 'Peta Laluan (sekiranya melibatkan aktiviti jalan kaki/ larian/ berbasikal dan sebagainya).pdf');
        });

        // Function to handle file upload
        function handleFileUpload(event, fixedName, invalidName) {
            const input = event.target;
            const fileId = input.id;
            const fileDisplayId = 'fileDisplay' + fileId.charAt(fileId.length - 1);
            const fileName = input.name;
            const fileDisplay = $('#' + fileDisplayId);
            console.log(invalidName)
            if (input.files.length > 0) {
                $('#invalid-' + invalidName).hide();
                fileDisplay.text(fixedName);
                fileDisplay.css('color', 'green');
            } else {
                $('#invalid-' + invalidName).prop('hidden', false);
                fileDisplay.text('No file selected');
                fileDisplay.css('color', 'red');
            }
        }
        // File Upload::End

        // Datepicker Format::Start
        var startDate = new Date();
        var tarikhAcaraDariPicker = $('.tarikhAcaraDari').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var startDate = new Date();
                startDate.setHours(0, 0, 0, 0); 

                return {
                    enabled: date.valueOf() >= startDate.valueOf(),
                    classes: date.valueOf() >= startDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhAcaraHinggaPicker = $('.tarikhAcaraHingga').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhMasukTapakPicker = $('.tarikhMasukTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }

                var maxDate = new Date(selectedDate);
                maxDate.setDate(selectedDate.getDate() - 2);
                return {
                    enabled: date.valueOf() <= selectedDate.valueOf() && date.valueOf() >= maxDate.valueOf(),
                    classes: date.valueOf() <= selectedDate.valueOf() && date.valueOf() >= maxDate.valueOf() ? 'highlight' : ''
                };
            }
        });
        var tarikhKeluarTapakPicker = $('.tarikhKeluarTapak').datepicker({
            format: 'dd M yyyy',
            // todayHighlight: true,
            autoclose: true,
            beforeShowDay: function(date) {
                var selectedDate = tarikhAcaraDariPicker.datepicker('getDate');
                if (!selectedDate) {
                    return true;
                }

                var maxDate = new Date(selectedDate);
                maxDate.setDate(selectedDate.getDate() + 1);
                return {
                    enabled: date.valueOf() >= selectedDate.valueOf() && date.valueOf() <= maxDate.valueOf(),
                    classes: date.valueOf() >= selectedDate.valueOf() && date.valueOf() <= maxDate.valueOf() ? 'highlight' : ''
                };
            }
        })

        tarikhAcaraDariPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            $('.tarikhAcaraHingga').val('');
            $('.tarikhMasukTapak').val('');
            $('.tarikhKeluarTapak').val('');
            tarikhAcaraHinggaPicker.datepicker('setStartDate', selectedDate);
            tarikhMasukTapakPicker.datepicker('setEndDate', selectedDate);
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhAcaraHingga').prop('disabled', false);
            $('.tarikhAcaraHingga').removeAttr('placeholder');
            $('.tarikhMasukTapak').prop('disabled', false);
            $('.tarikhMasukTapak').removeAttr('placeholder');
        });
        tarikhMasukTapakPicker.on('changeDate', function (e) {
            var selectedDate = e.date;
            tarikhKeluarTapakPicker.datepicker('setStartDate', selectedDate);
            $('.tarikhKeluarTapak').prop('disabled', false);
            $('.tarikhKeluarTapak').removeAttr('placeholder');
        });
        // Datepicker Format::End
        
        // Form Switch::Start
        $('#perakuanCheckbox').on('change', function() {
            if ($(this).is(':checked')) {
                $('#rumusanCard').show();
            } else {
                $('#rumusanCard').hide();
            }
        });
        // Form Switch::End

        function checkRequiredInput(formId, formValidation) {
            $(formId).each(function() {
                var requiredFields = $(this).find('[name][required]');
                requiredFields.each(function() {
                    var fieldName = $(this).prop('name'); 
                    var fieldValue = $(this).val();
                    if ($(this).is('select')) {
                        if (fieldValue === null || fieldValue === "" || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                            console.log(fieldName)
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    } else {
                        // Handle other input elements
                        if (fieldValue === null || fieldValue.trim() === '' || fieldValue.length === 0) {
                            $('#invalid-' + fieldName).prop('hidden', false);
                            formValidation = false;
                            console.log(fieldName)
                        } else {
                            $('#invalid-' + fieldName).hide();
                        }
                    }
                });
            })
            return formValidation
        }

        // Form 1 Data::Start
        $('#submitForms').on('click', function(event) {
            var allFormData = '';
            var dataId = $('#myId').data('id');
            var isValid = true;
            var form1IsValid = true;
            var form2IsValid = true;
            var form3IsValid = true;
            var form4IsValid = true;

            var form1IsValid = checkRequiredInput('#form1', form1IsValid)
            var form2IsValid = checkRequiredInput('#form2', form2IsValid)
            var form3IsValid = checkRequiredInput('#form3', form3IsValid)
            var form4IsValid = checkRequiredInput('#form4', form4IsValid)
            var form5IsValid = checkRequiredInput('#form4', form5IsValid)
            if (!form1IsValid) {
                $('#exclamation-butiranPemohon').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranPemohon').hide();
            }
            if (!form2IsValid) {
                $('#exclamation-butiranAcara').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-butiranAcara').hide();
            }
            if (!form3IsValid) {
                $('#exclamation-maklumatPenganjurUtama').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-maklumatPenganjurUtama').hide();
            }
            if (!form4IsValid) {
                $('#exclamation-muatNaikDokumen').prop('hidden', false);
                isValid = false;
            } else {
                $('#exclamation-muatNaikDokumen').hide();
            }
            if (!isValid) {
                event.preventDefault();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;  
            }
            var form4Data = new FormData($("#form4")[0]);
            allFormData += $('#form1').serialize() + '&' + $('#form2').serialize() + '&' + $('#form3').serialize() + '&' + $('#form4').serialize() + '&' + $('#form5').serialize();
            
            allFormData = allFormData.slice(0, -1);
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var mainBookingId = 0;

            $.ajax({
                url: '',
                type: 'POST',
                data: allFormData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function(response) {
                    $.ajax({
                        url: '/upload/fileAcara/' + response.bookingId,
                        type: 'POST',
                        data: form4Data,
                        processData: false, 
                        contentType: false,
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        success: function(response2) {
                            window.location.href = response.redirectUrl;
                        },
                        error: function(error) {
                            console.log('error: ' + error);
                        }
                    });
                }
                ,
                error: function(error) {
                    console.log(error);
                }
            });
        });
        // Form 1 Data::End
    });
</script> --}}
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        var table = $('table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
    });
</script>
<script>
    // date picker
    // $('#kt_daterangepicker_2').daterangepicker({
    //     autoApply: true,
    //     autoclose: true,
    // }, function(start, end, label) {
    //     $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    // });
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_1').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment().add(1, 'months'),
            locale: {
                cancelLabel: 'Clear',
                format: 'D-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D-MM-YYYY'));
            $('#kt_daterangepicker_2').daterangepicker({
                singleDatePicker: true,
                autoApply: true,
                minDate: picker.startDate,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'D-MM-YYYY'
                }
            });
        });

        $('#kt_daterangepicker_1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('#kt_daterangepicker_2').daterangepicker({
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: moment().add(1, 'months'),
            locale: {
                cancelLabel: 'Clear',
                format: 'D-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('D-MM-YYYY'));
        });

        $('#kt_daterangepicker_2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
{{-- <script>
    $(document).ready(function() {
    var groupColumn = 1;
    var table = $('#kt_datatable_2').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        },
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
});
</script>
<script>
    $(document).ready(function() {
    var groupColumn = 4;
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
} );
</script>
<script>
    // $(document).ready(function(){
    //     $("#form").submit(function( event ) {
    //         var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
    //         if(atLeastOneIsChecked == false){
    //             event.preventDefault();
    //             Swal.fire(
    //             'Harap Maaf!',
    //             'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
    //             'error'
    //             );
    //         }else{
    //             $("#form").submit();
    //         }

    //     });
    // });
</script> --}}
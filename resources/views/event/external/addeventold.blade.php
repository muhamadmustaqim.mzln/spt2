@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
    
    .highlight {
        background-color: #F1F3FF; /* Set your desired background color for highlighting */
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Permohonan Acara Dalaman</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Dalaman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->	   
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::List Widget 21-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <div class="row">
                            <div class="col-12">
                                <h4 class="card-label font-weight-bolder text-dark p-5 mb-1">Butiran Acara</h4>
                            </div>
                        </div>
                        <!--end::Header-->
                        <form action="" method="post">
                            @csrf
                            <!--begin::Jenis Acara-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Jenis Acara <span class="text-danger">*</span></label></div>
                                    <div class="col-5">
                                        <select disabled name="jenisAcara" class="form-control  form-control-solid form-control-sm font-weight-bold mt-3" id="kt_select2_5" required>
                                            <option value="1" selected>Luaran</option>
                                            <option value="2">Dalaman</option>
                                        </select>
                                        {{-- <input type="text" class="form-control form-control-sm" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                    </div>
                                <div class="col-3"></div>
                                </div>
                            </div>
                            <!--end::Jenis Acara-->
                            <!--begin::Header-->
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-label font-weight-bolder text-dark p-5 mb-1">Butiran Pemohon</h4>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Nama Pegawai Memohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Nama Pegawai Memohon <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input required type="text" class="form-control form-control-sm" name="namaPegawaiPemohon" placeholder="Masukkan Nama Penuh" value="{{ $data['user']->bud_name ?? '' }}" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Nama Pegawai Memohon-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Jawatan</label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="jawatan" placeholder="Masukkan Jawatan" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Penganjur-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Penganjur</label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="penganjur" placeholder="Masukkan Nama Penganjur" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Penganjur-->
                            <!--begin::No. Telefon Bimbit-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input required type="text" class="form-control form-control-sm" name="noBimbit" placeholder="Masukkan No. Tel. Bimbit" value="{{ $data['user']->bud_phone_no ?? '' }}" />
                                    </div>
                                </div>
                            </div>
                            <!--end::No. Telefon Bimbit-->
                            <!--begin::No. Telefon Pejabat-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>No. Telefon Pejabat</label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="noTelPejabat1" placeholder="Masukkan No. Tel. Pejabat" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="noTelPejabat2" placeholder="Masukkan No. Tel. Pejabat" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::No. Telefon Pejabat-->
                            <!--begin::No. Faks-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>No. Faks </label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="noFaks" placeholder="Masukkan No. Faks" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::No. Faks-->
                            <!--begin::No. Faks-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Emel</label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="emel" placeholder="Masukkan Emel" value="{{ $data['user']->bud_email ?? '' }}" />
                                    </div>
                                </div>
                            </div>
                            <!--end::No. Faks-->

                            <!--begin::Header-->
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-label font-weight-bolder text-dark p-5 mb-1">Butiran Acara</h4>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Nama Pegawai Memohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input required type="text" class="form-control form-control-sm" name="namaAcara" placeholder="Masukkan Nama Acara" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Nama Pegawai Memohon-->
                            <!--begin::Nama Pegawai Memohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Tarikh Acara <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input required type="text" class="form-control form-control-sm" name="tarikhAcaraDari" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Nama Pegawai Memohon-->
                            <!--begin::Nama Pegawai Memohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input required type="text" class="form-control form-control-sm" name="tarikhAcaraHingga" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Nama Pegawai Memohon-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Masa Penganjuran <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="masaPenganjuran" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jenis Pemohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Lokasi Acara <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input disabled type="text" class="form-control form-control-sm" name="namaPemohon" placeholder="" value="{{ Helper::locationspa($data['lokasi']) }}" />
                                    </div>
                                <div class="col-3"></div>
                                </div>
                            </div>
                            <!--end::Jenis Pemohon-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Jumlah Peserta/Pengunjung <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="jumlahPesertaPengunjung" placeholder="Nyatakan Jumlah Peserta/Pengunjung" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jenis Pemohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                    <div class="col-5">
                                        <select name="peringkatPenganjuran" class="form-control form-control-sm font-weight-bold mt-3" id="peringkatPenganjuranId" required>
                                            <option value="" disabled selected> Sila Pilih</option>
                                            <option value="1">Antarabangsa</option>
                                            <option value="2">Kebangsaan</option>
                                            <option value="3">Putrajaya</option>
                                            <option value="4">Lain-lain</option>
                                        </select>
                                        {{-- <input type="text" class="form-control form-control-sm" name="namaPemohon" placeholder="" value="@if ($data['booking_person']->applicant_type == 1) Individu @else Syarikat/Organisasi @endif" /> --}}
                                    </div>
                                <div class="col-3"></div>
                                </div>
                            </div>
                            <!--end::Jenis Pemohon-->
                            <!--begin::Jumlah Peserta/ Pengunjung-->
                            <div hidden class="form-group" id="peringkatPenganjuranLainLain">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Nyatakan Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-lg tarikhMasukTapak" id="kt_datepicker_1" name="tarikhMasukTapak" placeholder="" value="" required/>

                                        {{-- <input type="text" class="form-control form-control-lg" name="nyatakanPeringkatPenganjuran" placeholder="" value=""/> --}}
                                        <div hidden class="text-danger" id="invalid-nyatakanPeringkatPenganjuran">* Sila Isi Maklumat.</div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Jumlah Peserta/ Pengunjung-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Tarikh Masuk Tapak <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="tarikhMasukTapak" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="tarikhKeluarTapak" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="masaMasukTapak" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="masaKeluarTapak" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jenis Pemohon-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Melibatkan Penggunaan Drone atau Alat Kawalan Jauh (terbang). <span class="text-danger">*</span></label></div>
                                    <div class="col-5">
                                        <input type="text" class="form-control form-control-sm" name="drone" placeholder="" value="" />
                                    </div>
                                <div class="col-3"></div>
                                </div>
                            </div>
                            <!--end::Jenis Pemohon-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Nama Tetamu Kehormat Utama (jika ada) <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="namaTetamuKehormat" placeholder="" value="" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <!--begin::Jawatan-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3 my-auto text-right"><label>Maklumat Tambahan <span class="text-danger">*</span></label></div>
                                    <div class="col-7">
                                        <input type="text" class="form-control form-control-sm" name="maklumatTambahan" placeholder="" value="{{ $data['formAcara']['maklumatTambahan'] ?? 'TIADA' }}" />
                                    </div>
                                </div>
                            </div>
                            <!--end::Jawatan-->
                            <div class="row mr-2">
                                <div class="col-12 d-flex justify-content-end">
                                    <button type="button" class="btn btn-success font-weight-bold mb-7">Simpan Draf</button>
                                    <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2" id="hantar">Hantar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    {{-- @include('event.internal.js.form') --}}
    @include('event.external.js.addevent')
@endsection

<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
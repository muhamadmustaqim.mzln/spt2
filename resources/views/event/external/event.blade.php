@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Luaran</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Luaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
         <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::List Widget 21-->
                <div class="card card-custom gutter-b">
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <table class="table table-bordered">
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                <td>{{ strtoupper($data['user']->fullname) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                <td>{{ $data['user_detail']->bud_phone_no }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                <td>{{ $data['user']->email }}</td>
                            </tr>
                        </table>
                        <hr>
                        <form action="" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                        <option value="">Sila Pilih Lokasi</option>
                                        @foreach ($data['location'] as $l)
                                        <option value="{{ $l->id }}" @if ($data['lokasi'] == $l->id) selected @endif>{{ $l->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tarikhDari" class="form-control mt-3" id="kt_daterangepicker_1" placeholder="Tarikh Acara Dari" value="{{ Helper::date_format($data['tarikhDari']) }}" autocomplete="off" required>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tarikhHingga" class="form-control mt-3" id="kt_daterangepicker_2" placeholder="Tarikh Acara Hingga" value="{{ Helper::date_format($data['tarikhHingga']) }}" autocomplete="off" required>
                                </div>
                                <div class="col-lg-2">
                                <div class="float-left">
                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mr-2 mt-3">Carian</button>
                                </div>
                                </div>
                            </div>
                        </form>
                        <hr>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::List Widget 21-->
                <div class="col-lg-12">
                    @if ($data['post'] == true)
                    <!--begin::Card-->
                    <form action="{{ url('event/external/addevent', [Crypt::encrypt($data['lokasi']), Crypt::encrypt($data['tarikhDari']), Crypt::encrypt($data['tarikhHingga'])]) }}" method="post" id="form">
                        @csrf
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['tarikhDari'])) }} - {{ date("d F Y", strtotime($data['tarikhHingga'])) }}</span>
                                </h3>
                                <div class="card-toolbar">
                                    <!--begin::Button-->
                                    {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i>Teruskan Tempahan</button> --}}
                                    @if (count($data['slot']) == 0) 
                                        <a href="{{ url('event/external/addevent', [Crypt::encrypt($data['lokasi']), Crypt::encrypt($data['tarikhDari']), Crypt::encrypt($data['tarikhHingga']), Crypt::encrypt($data['user']->email)]) }}" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i>Teruskan Tempahan</a>
                                    @else 
                                        <button disabled type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i>Teruskan Tempahan</button>
                                    @endif
                                    <a href="{{ url('/event/cancel') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                        <i class="flaticon-circle"></i> Batal Tempahan
                                    </a>
                                    <!--end::Button-->
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed"  id="body"> 
                                        <thead>
                                            <tr>
                                                <th class="text-center th-sortable active"><b>Bil.</b></th>
                                                <th class="th-sortable active"><b>Nama Acara</b></th>
                                                <th class="th-sortable active"><b>Jenis Acara</b></th>
                                                <th class="th-sortable active"><b>Status Tempahan</b></th>
                                                <th class="th-sortable active"><b>Lokasi</b></th>
                                                <th class="th-sortable active"><b>Tarikh Mula Acara</b></th>
                                                <th class="th-sortable active"><b>Tarikh Akhir Acara</b></th>
                                            </tr>
                                        </thead>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($data['slot'] as $s)
                                            <tr>
                                                <td class="text-center" style="width: 4.5%;">{{ $i }}</td>
                                                <td>{{ $s->event_name }}</td>
                                                <td>
                                                    @php
                                                        $internal = "@ppj.gov.my";
                                                        $jenis = '';
                                                        $atPosition = strpos($data['user_detail']->bud_email, "@");
                                                        $domain = substr($data['user_detail']->bud_email, $atPosition)
                                                    @endphp
                                                    {{ $data['main_booking']->bmb_type_user ??  ($domain === $internal) ? 'Dalaman' : 'Luaran'; }}
                                                </td>
                                                <td>{{ Helper::get_status_tempahan($data['main_booking']->fk_lkp_status) }}</td>
                                                <td>{{ Helper::locationspa($s->fk_spa_location) }}</td>
                                                <td class="">{{ date("d M Y", strtotime($s->event_date)) }}</td>
                                                <td class="">{{ date("d M Y", strtotime($s->event_date_end)) }}</td>
                                            </tr>
                                            @php
                                                $i++;
                                            @endphp
                                        @endforeach
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                    {{-- </form> --}}
                    <!--end::Card-->
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
</div>	

@endsection

@section('js_content')
    @include('event.external.js.event')
@endsection
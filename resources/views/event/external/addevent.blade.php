@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
    
    .highlight {
        background-color: #F1F3FF; /* Set your desired background color for highlighting */
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Permohonan Acara Dalaman/Luaran</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Dalaman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->	   
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{-- @if ($data['user'] != null)
                        <form action="" method="post">
                    @else --}}
                    <form action="{{ url('event/external/addeventsubmit') }}" method="post">
                    {{-- @endif --}}
                        @csrf
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <div class="card-body">
                                <!--begin::Header-->
                                <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="card-label font-weight-bolder text-dark py-5 mb-1">Butiran Acara</h4>
                                        </div>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Jenis Acara-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Jenis Acara <span class="text-danger">*</span></label></div>
                                            <div class="col-5">
                                                <input hidden type="text" class="form-control form-control-sm" name="jenisAcara" placeholder="" value="2" />
                                                <select disabled name="jenisAcara" class="form-control form-control-sm font-weight-bold mt-3" id="kt_select2_5" >
                                                    <option value="1" selected>Luaran</option>
                                                </select>
                                            </div>
                                        <div class="col-3"></div>
                                        </div>
                                    </div>
                                    <!--end::Jenis Acara-->
                                <hr>
                                <!--begin::Header-->
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="card-label font-weight-bolder text-dark py-5 mb-1">Butiran Pemohon</h4>
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Nama Pegawai Memohon-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>Nama Pegawai Memohon <span class="text-danger">*</span></label></div>
                                        <div class="col-7">
                                            <input required type="text" class="form-control form-control-sm" name="namaPegawai" placeholder="Masukkan Nama Penuh" value="{{ $data['user']->bud_name ?? '' }}" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Nama Pegawai Memohon-->
                                <!--begin::Jawatan-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>Jawatan</label></div>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-sm" name="jawatan" placeholder="Masukkan Jawatan" value="" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Jawatan-->
                                <!--begin::Penganjur-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>Penganjur</label></div>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-sm" name="penganjur" placeholder="Masukkan Nama Penganjur" value="" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::Penganjur-->
                                <!--begin::No. Telefon Bimbit-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>No. Telefon Bimbit <span class="text-danger">*</span></label></div>
                                        <div class="col-7">
                                            <input required type="text" class="form-control form-control-sm" name="noBimbit" placeholder="Masukkan No. Tel. Bimbit" value="{{ $data['user']->bud_phone_no ?? '' }}" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::No. Telefon Bimbit-->
                                <!--begin::No. Telefon Pejabat-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>No. Telefon Pejabat</label></div>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-sm" name="noTelPejabat1" placeholder="Masukkan No. Tel. Pejabat" value="{{ $data['user']->office_no ?? '' }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"></div>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-sm" name="noTelPejabat2" placeholder="Masukkan No. Tel. Pejabat" value="" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::No. Telefon Pejabat-->
                                <!--begin::No. Faks-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>No. Faks </label></div>
                                        <div class="col-7">
                                            <input type="text" class="form-control form-control-sm" name="noFaks" placeholder="Masukkan No. Faks" value="{{ $data['user']->fax_no ?? '' }}" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::No. Faks-->
                                <!--begin::No. Faks-->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>E-mail  <span class="text-danger">*</span></label></div>
                                        <div class="col-7">
                                            <input required type="text" class="form-control form-control-sm" name="email" placeholder="Masukkan Emel" value="{{ $data['user']->bud_email ?? '' }}" />
                                        </div>
                                    </div>
                                </div>
                                <!--end::No. Faks-->
                                <input hidden type="text" class="form-control form-control-sm" name="user_id" value="{{ $data['user_id'] ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="alamat" value="{{ $data['user']->address1 ?? '' }}, {{ $data['user']->address2 ?? '' }},{{ $data['user']->address3 ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="poskod" value="{{ $data['user']->postcode ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="bandar" value="{{ $data['user']->city ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="negeri" value="{{ $data['user']->fk_lkp_state ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="negara" value="{{ $data['user']->fk_lkp_country ?? '' }}" />
                                <input hidden type="text" class="form-control form-control-sm" name="reference_id" value="{{ $data['user']->reference_id ?? '' }}" />

                                <hr>
                                    <!--begin::Header-->
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="card-label font-weight-bolder text-dark py-5 mb-1">Butiran Acara</h4>
                                        </div>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Nama Acara-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Nama Acara <span class="text-danger">*</span></label></div>
                                            <div class="col-7">
                                                <input required type="text" class="form-control form-control-sm" name="namaAcara" placeholder="Masukkan Nama Acara" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Nama Acara-->
                                    <!--begin::Tarikh Acara-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Tarikh Acara <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker1" name="tarikhAcaraDari" placeholder="" autocomplete="off" disabled value="{{Helper::date_format($data['dateFrom'])}}" />
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker1" name="tarikhAcaraDari" placeholder="" autocomplete="off" hidden value="{{Helper::date_format($data['dateFrom'])}}" />
                                                {{-- <input required type="text" class="form-control form-control-sm tarikhAcaraDari" name="tarikhAcaraDari" placeholder="" autocomplete="off" value="" /> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Tarikh Acara-->
                                    <!--begin::Tarikh Acara Hingga-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Tarikh Acara Hingga <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker2" name="tarikhAcaraHingga" placeholder="" autocomplete="off" disabled value="{{Helper::date_format($data['dateUntil'])}}" />
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker2" name="tarikhAcaraHingga" placeholder="" autocomplete="off" hidden value="{{Helper::date_format($data['dateUntil'])}}" />
                                                {{-- <input required type="text" class="form-control form-control-sm tarikhAcaraHingga" name="tarikhAcaraHingga" placeholder=""autocomplete="off"  value="" /> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Tarikh Acara Hingga-->
                                    <!--begin::Masa Penganjuran-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Masa Penganjuran <span class="text-danger">*</span></label></div>
                                            <div class="col-7">
                                                <input required class="form-control" id="kt_timepicker_1" name="masaPenganjuran" value="" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Masa Penganjuran-->
                                    <!--begin::Jenis Pemohon-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Lokasi Acara <span class="text-danger">*</span></label></div>
                                            <div class="col-5">
                                                <select disabled name="lokasiAcara" class="jenisPemohon form-control form-control-sm font-weight-bold mt-3" id="kt_select2_5" >
                                                    <option value="1" selected disabled>Sila Pilih</option>
                                                    @foreach ($data['location'] as $l)
                                                        <option value="{{ $l->id }}" @if($l->id == $data['lokasi']) selected @endif>{{ $l->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input value="" name="" type="text" hidden>
                                            </div>
                                        <div class="col-3"></div>
                                        </div>
                                    </div>
                                    <!--end::Jenis Pemohon-->
                                    <!--begin::Jumlah Peserta/Pengunjung-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Jumlah Peserta/Pengunjung <span class="text-danger">*</span></label></div>
                                            <div class="col-7">
                                                <input required type="text" class="form-control form-control-sm" name="jumlahPesertaPengunjung" placeholder="Nyatakan Jumlah Peserta/Pengunjung" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Jumlah Peserta/Pengunjung-->
                                    <!--begin::Jenis Pemohon-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                            <div class="col-5">
                                                <select required name="peringkatPenganjuran" class="form-control form-control-sm font-weight-bold mt-3" id="peringkatPenganjuranId" >
                                                    <option value="" readonly selected> Sila Pilih</option>
                                                    <option value="1">Antarabangsa</option>
                                                    <option value="2">Kebangsaan</option>
                                                    <option value="3">Putrajaya</option>
                                                    <option value="4">Lain-lain</option>
                                                </select>
                                            </div>
                                        <div class="col-3"></div>
                                        </div>
                                    </div>
                                    <!--end::Jenis Pemohon-->
                                    <!--begin::Jumlah Peserta/ Pengunjung-->
                                    <div hidden class="form-group" id="peringkatPenganjuranLainLain">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Nyatakan Peringkat Penganjuran <span class="text-danger">*</span></label></div>
                                            <div class="col-7">
                                                <input type="text" class="form-control form-control-sm " id="kt_datepicker_1" name="peringkatPenganjuran" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Jumlah Peserta/ Pengunjung-->
                                    <!--begin::Tarikh Masuk Tapak-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Tarikh Masuk Tapak <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker3" name="tarikhMasukTapak" placeholder="" autocomplete="off" required value="" />
                                                {{-- <input required type="text" class="form-control form-control-sm tarikhMasukTapak" name="tarikhMasukTapak" placeholder="" autocomplete="off" value="" /> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Tarikh Masuk Tapak-->
                                    <!--begin::Tarikh Keluar Tapak-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Tarikh Keluar Tapak <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input type="text" class="form-control form-control-sm" id="kt_datepicker4" name="tarikhKeluarTapak" placeholder="" autocomplete="off" required value="" />
                                                {{-- <input required type="text" class="form-control form-control-sm tarikhKeluarTapak" name="tarikhKeluarTapak" placeholder="" autocomplete="off" value="" /> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Tarikh Keluar Tapak-->
                                    <!--begin::Masa Masuk Tapak-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Masa Masuk Tapak <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input required class="form-control" id="kt_timepicker_1" name="masaMasukTapak" value="" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Masa Masuk Tapak-->
                                    <!--begin::Masa Keluar Tapak-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Masa Keluar Tapak <span class="text-danger">*</span></label></div>
                                            <div class="col-3">
                                                <input required class="form-control" id="kt_timepicker_1" name="masaKeluarTapak" value="" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Masa Keluar Tapak-->
                                    <!--begin::Jenis Pemohon-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Melibatkan Penggunaan Drone atau Alat Kawalan Jauh (terbang).</label></div>
                                            <div class="col-3">
                                                {{-- <input type="text" class="form-control form-control-sm" name="drone" placeholder="" value="" /> --}}
                                                <select name="drone" class="form-control form-control-lg mt-3" id="kt_select2_5">
                                                    <option value=""> Sila Pilih</option>
                                                    <option value="1" {{ ($data['formAcara'] ?? null) && $data['formAcara']['libatPenggunaanDrone'] == 'ya' ? 'selected' : '' }}> Ya</option>
                                                    <option value="0" {{ ($data['formAcara'] ?? null) && $data['formAcara']['libatPenggunaanDrone'] == 'tidak' ? 'selected' : '' }}> Tidak</option>
                                                </select>
                                            </div>
                                        <div class="col-3"></div>
                                        </div>
                                    </div>
                                    <!--end::Jenis Pemohon-->
                                    <!--begin::Nama Tetamu Kehormat Utama-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Nama Tetamu Kehormat Utama (jika ada)</label></div>
                                            <div class="col-7">
                                                <input type="text" class="form-control form-control-sm" name="namaTetamuKehormat" placeholder="" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Nama Tetamu Kehormat Utama-->
                                    <!--begin::Maklumat Tambahan-->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-3 my-auto"><label>Maklumat Tambahan</label></div>
                                            <div class="col-7">
                                                <input type="text" class="form-control form-control-sm" name="maklumatTambahan" placeholder="" value="{{ $data['formAcara']['maklumatTambahan'] ?? 'TIADA' }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Maklumat Tambahan-->
                                <hr>
                                <div class="row mr-2">
                                    <div class="col-12 d-flex justify-content-end">
                                        <button type="button" class="btn btn-success font-weight-bold mb-7">Simpan Draf</button>
                                        <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2" id="hantar">Hantar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('event.external.js.addevent')
@endsection

<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
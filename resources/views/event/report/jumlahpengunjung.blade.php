@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Senarai Acara</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Acara</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-3">
                                            <input type="text" name="mula" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Penggunaan Dari" autocomplete="off" required value="{{ isset($data['mula']) ? Helper::date_format($data['mula']) : '' }}">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="tamat" class="form-control mt-3" id="kt_daterangepicker_6" placeholder="Tarikh Penggunaan Hingga" autocomplete="off" required value="{{ isset($data['tamat']) ? Helper::date_format($data['tamat']) : '' }}">
                                        </div>
                                        {{-- <div class="col-lg-3">
                                            <div class='input-group' id="kt_daterangepicker_2">
                                                <div class="input-icon">
                                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" required />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div><div class="input-group-append mt-3">
                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="col-lg-1">
                                            <div class="float-right">
                                                <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 d-flex justify-content-center">
                                        <b>PERBADANAN PUTRAJAYA</b>
                                    </div>
                                    <div class="col-lg-12 d-flex justify-content-center">
                                        <b>LAPORAN JUMLAH PENGUNJUNG/PESERTA</b>
                                    </div>
                                    <div class="col-lg-12 d-flex justify-content-center">
                                        <b>Dari : {{$data['mula']}}</b>&nbsp;<b> Hingga : {{ $data['tamat']}} </b>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-condensed"  id="body"> 
                                                <thead>
                                                    <tr>
                                                        <th class="th-sortable active"><b>Bil.</b></th>
                                                        <th class="th-sortable active"><b>Lokasi</b></th>
                                                        <th class="th-sortable active"><b>Antarabangsa</b></th>
                                                        <th class="th-sortable active"><b>Kebangsaan</b></th>
                                                        <th class="th-sortable active"><b>Putrajaya</b></th>
                                                        <th class="th-sortable active"><b>Lain-lain</b></th>
                                                        <th class="th-sortable active"><b>Jumlah</b></th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $i = 1;
                                                    $dataspalocation = $data['dataspalocation'];
                                                    $rankhost = array(1 => 'Antarabangsa', 2 => 'Kebangsaan', 3 => 'Putrajaya', 4 => 'Lain-lain');
                                                    $tarikh_from = $data['mula'];
                                                    $tarikh_to = $data['tamat'];
                                                @endphp
                                                @foreach($dataspalocation as $key => $value)
                                                    <tr style="page-break-inside:avoid;">
                                                        <td width="50px">@php echo $i @endphp</td>
                                                        <td width="300px">{{ $value->name }}</td>
                                                        @php 
                                                            $sum=0; 
                                                        @endphp
                                                        @foreach($rankhost as $num => $name)
                                                            @php $jumvisitor= DB::table('main_booking')
                                                                ->Join('spa_booking_event','spa_booking_event.fk_main_booking','=','main_booking.id')
                                                                ->leftJoin('spa_location','spa_location.id','=','spa_booking_event.fk_spa_location')
                                                                ->select( DB::raw('SUM(spa_booking_event.visitor_amount) AS sumvisitor'))
                                                                ->where('spa_booking_event.event_date','>=', array( $tarikh_from))
                                                                ->where('spa_booking_event.event_date' ,'<', array( $tarikh_to))
                                                                ->where('spa_booking_event.fk_spa_location',$value->id)
                                                                ->where('spa_booking_event.rank_host',$num)
                                                                ->whereIn('main_booking.fk_lkp_status',array(5,20,24,25,26,27,30))
                                                                // ->whereIn('main_booking.fk_lkp_status',array(20,24,25,26,27,30))
                                                                ->first();
                                                                // dd($jumvisitor);
                                                            @endphp
                                                            @if($jumvisitor->sumvisitor=='')
                                                                <td width="300px">0</td>
                                                            @else
                                                                <td width="300px">{{ $jumvisitor->sumvisitor }}</td>
                                                            @endif
                                                            
                                                            @php $sum +=$jumvisitor->sumvisitor;@endphp
                                                        @endforeach
                                                        <td width="300px"><b>@php echo $sum;@endphp</b></td>
                                                    </tr>
                                                    @php $i++;@endphp
                                                @endforeach
                                                <!--start jumlah bawah-->
                                                <tr style="page-break-inside:avoid;">
                                                    <td></td>
                                                    <td><b>Jumlah<b></td>
                                                    @foreach($rankhost as $num => $name)
                                                        @php $jumvisitorhost= DB::table('main_booking')
                                                            ->Join('spa_booking_event','spa_booking_event.fk_main_booking','=','main_booking.id')
                                                            ->leftJoin('spa_location','spa_location.id','=','spa_booking_event.fk_spa_location')
                                                            ->select( DB::raw('SUM(spa_booking_event.visitor_amount) AS sumvisitor'))
                                                            ->where('spa_booking_event.event_date','>=', array( $tarikh_from))
                                                            ->where('spa_booking_event.event_date' ,'<', array( $tarikh_to))
                                                            ->where('spa_booking_event.rank_host',$num)
                                                            ->whereIn('main_booking.fk_lkp_status',array(5,20,24,25,26,27,30))
                                                            ->first();
                                                        @endphp
                                                        @if($jumvisitorhost->sumvisitor=='')
                                                            <td width="300px"><b>0</b></td>
                                                        @else
                                                            <td width="300px"><b>{{ $jumvisitorhost->sumvisitor }}</b></td>
                                                        @endif
                                                    @endforeach
                                                    <!--end jumlah bawah-->
                                
                                                    <!--start jum semua-->
                                                    @php $totalvisitor= DB::table('main_booking')
                                                        ->Join('spa_booking_event','spa_booking_event.fk_main_booking','=','main_booking.id')
                                                        ->leftJoin('spa_location','spa_location.id','=','spa_booking_event.fk_spa_location')
                                                        ->select( DB::raw('SUM(spa_booking_event.visitor_amount) AS sumvisitor'))
                                                        ->where('spa_booking_event.event_date','>=', array( $tarikh_from))
                                                        ->where('spa_booking_event.event_date' ,'<', array( $tarikh_to))
                                                        ->whereIn('main_booking.fk_lkp_status',array(20,24,25,26,27,30))
                                                        ->first();
                                                    @endphp
                                                    @if($totalvisitor->sumvisitor=='')
                                                        <td width="300px"><b>0</b></td>
                                                    @else
                                                        <td width="300px"><b>{{ $totalvisitor->sumvisitor }}</b></td>
                                                    @endif
                                                    <!--end jum semua-->
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    {{-- @include('event.report.js.index') --}}
    @include('event.report.js.jumlahpengunjung')
@endsection
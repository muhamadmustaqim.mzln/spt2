@extends('layouts.master')
@section('container')
<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    font-family: "Poppins", sans-serif;
}

.step-progress {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
}

.step-progress-list {
    background: #fff;
    box-shadow: 0 15px 25px rgba(0,0,0,0.1);
    color: #333;
    list-style-type: none;
    border-radius: 10px;
    display: flex;
    padding: 20px 10px;
    position: relative;
    z-index: 10;
}

.step-progress-item {
    padding: 0 20px;
    flex-basis: 0;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    max-width: 100%;
    display: flex;
    flex-direction: column;
    text-align: center;
    min-width: 170px;
    position: relative;
}

.step-progress-item + .step-progress-item::after {
    content: "";
    position: absolute;
    left: 0;
    top: 19px;
    background: #1d9000;
    width: 100%;
    height: 2px;
    transform: translateX(-50%);
    z-index: -10;
}

.progress-count {
    height: 40px;
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    font-weight: 600;
    margin: 0 auto;
    position: relative;
    z-index: 10;
    color: transparent;
}

.progress-count::after {
    content: "";
    height: 40px;
    width: 40px;
    background: #1d9000;
    color: #fff;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    border-radius: 50%;
    z-index: -10;
}

.progress-count::before {
    content: "";
    height: 10px;
    width: 20px;
    border-left: 3px solid #fff;
    border-bottom: 3px solid #fff;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -60%) rotate(-45deg);
    transform-origin: center center;
}

.progress-label {
    color: #34b913;
    font-size: 14px;
    font-weight: 600;
    margin-top: 10px;
}

.current-item .progress-count::before,
.current-item ~ .step-progress-item .progress-count::before {
    display: none;
}

.current-item ~ .step-progress-item .progress-count::after {
    height: 10px;
    width: 10px;
}

.current-item ~ .step-progress-item .progress-label {
    color: #474747;
}

.current-item .progress-count::after {
    background: #fff;
    border: 2px solid #1d9000;
}

.current-item .progress-count {
    color: #1d9000;
}

</style>
    @if(Session::has('flash'))
        <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
    @endif
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Maklumat Audit</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Maklumat Audit</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <!--begin::List Widget 21-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Header-->
                                    <div class="card-header border-0 pt-5">
                                        <h3 class="card-title align-items-start flex-column mb-5">
                                            <span class="card-label font-weight-bolder mb-1">Tapisan Data</span>
                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                        </h3>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Body-->
                                    <form action="" method="post">
                                        @csrf
                                        <div class="card-body pt-2">
                                            <hr>
                                            <h4 class="mt-2">No. Tempahan</h4>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <input required class="form-control" type="text" name="id" placeholder="No. Tempahan" autocomplete="off" required value="{{ $data['id'] }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="float-right">
                                                        <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </hr>
                                        </div>
                                    </form>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 21-->
                            </div>
                        @if ($data['post'] == true)
                                <div class="col-lg-6">
                                    <div class="card card-custom gutter-b">
                                        <!--begin::Body-->
                                        <div class="card-header d-flex justify-content-center flex-wrap pt-3">
                                            <h3 class="card-title text-center">
                                                <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Audit </span>
                                            </h3>
                                        </div>
                                        <div class="card-body pt-1 mt-5">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        Nama Pengguna :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>{{ Helper::get_nama($data['main']->fk_users) }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        No. Tempahan :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>{{ $data['main']->bmb_booking_no }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        Emel :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>{{ $data['users']->email }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        No. Telefon :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>{{ $data['user_detail']->bud_phone_no }}</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        Lokasi :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>
                                                        {{ Helper::location($data['main']->fk_lkp_location) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        Fasiliti :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div>
                                                        {{ Helper::tempahanSportDetail($data['et_booking']->fk_et_facility_detail) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <div class="font-weight-bolder text-dark text-right">
                                                        Tarikh :
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            {{ Helper::date_format($data['et_booking']->ebf_start_date) }}
                                                        </div>
                                                        <div class="col-lg-3">
                                                            sehingga
                                                        </div>
                                                        <div class="col-lg-3">
                                                            {{ Helper::date_format($data['et_booking']->ebf_end_date) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card gutter-b">
                                        <div class="card-body">
                                            <section class="step-progress">
                                                @php
                                                    $i = 1;
                                                @endphp
                                                <!-- Sukan::Begin -->
                                                <ul class="step-progress-list">
                                                    @if (count($data['et_sport_book']) > 0)
                                                        <li class="step-progress-item mb-5">
                                                            <span class="progress-count @if($data['main']->fk_lkp_status == 1) current-item @endif">1</span>
                                                            <span class="progress-label">Baharu</span>
                                                        </li>
                                                        @if ($data['main']->fk_lkp_status == 2)
                                                            <li class="step-progress-item mb-5 current-item">
                                                                <span class="progress-count">2</span>
                                                                <span class="progress-label">Tempahan Diproses - Sedia untuk Bayaran</span>
                                                            </li>
                                                        @else
                                                            <li class="step-progress-item mb-5">
                                                                <span class="progress-count">2</span>
                                                                <span class="progress-label">Tempahan Diproses - Sedia untuk Bayaran</span>
                                                            </li>
                                                        @endif
                                                        @if ($data['main']->fk_lkp_status == 5)
                                                            <li class="step-progress-item mb-5">
                                                                <span class="progress-count">3</span>
                                                                <span class="progress-label">Tempahan Selesai</span>
                                                            </li>
                                                        @elseif ($data['main']->fk_lkp_status == 6)
                                                            <li class="step-progress-item mb-5">
                                                                <span class="progress-count">3</span>
                                                                <span class="progress-label">Bayaran Gagal</span>
                                                            </li>
                                                        @else 
                                                            <li class="step-progress-item mb-5 current-item">
                                                                <span class="progress-count">3</span>
                                                                <span class="progress-label">Tempahan Selesai</span>
                                                            </li>
                                                        @endif
                                                    <!-- Sukan::End -->
                                                    <!-- Dewan::Begin -->
                                                    @elseif (count($data['et_hall_book']) > 0)
                                                        <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 1 || $data['main']->fk_lkp_status == 13) current-item @endif">
                                                            <span class="progress-count">{{ $i++ }}</span>
                                                            <span class="progress-label">Baharu</span>
                                                        </li>
                                                        @if ($data['main']->fk_lkp_status == 10 && count($data['et_payment_fpx']) == 0 && count($data['bh_payment']) == 0)
                                                            <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 1) current-item @endif">
                                                                <span class="progress-count">{{ $i++ }}</span>
                                                                <span class="progress-label">Batal Tempahan</span>
                                                            </li>
                                                        @elseif ($data['main']->fk_lkp_status == 14 && count($data['et_payment_fpx']) == 0 && count($data['bh_payment']) == 0)
                                                            <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 1) current-item @endif">
                                                                <span class="progress-count">{{ $i++ }}</span>
                                                                <span class="progress-label">Permohonan Tidak Diluluskan</span>
                                                            </li>
                                                        @else
                                                            <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 2) current-item @endif">
                                                                <span class="progress-count">{{ $i++ }}</span>
                                                                <span class="progress-label">Tempahan Diproses - Sedia untuk Bayaran</span>
                                                            </li>
                                                            <!-- Dewan::FPX::Begin -->
                                                            @if (count($data['et_payment_fpx']) > 0)
                                                                <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 4) current-item @endif">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Berjaya (Bayaran Deposit)</span>
                                                                </li>
                                                                @if ($data['main']->fk_lkp_status == 5)
                                                                    <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 5) current-item @endif">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Tempahan Selesai</span>
                                                                    </li>
                                                                @elseif ($data['main']->fk_lkp_status == 6)
                                                                    <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 6) current-item @endif">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Bayaran Gagal</span>
                                                                    </li>
                                                                @else 
                                                                    <li class="step-progress-item mb-5">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Tempahan Selesai</span>
                                                                    </li>
                                                                @endif
                                                            <!-- Dewan::FPX::End -->
                                                            <!-- Dewan::LO/PO::Begin -->
                                                            @elseif (count($data['bh_payment']) > 0)
                                                                <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 3) current-item @endif">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Diproses (LO/PO)</span>
                                                                </li>
                                                                <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 4) current-item @endif">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Berjaya (Bayaran Deposit)</span>
                                                                </li>
                                                                @if ($data['main']->fk_lkp_status == 5)
                                                                    <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 5) current-item @endif">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Tempahan Selesai</span>
                                                                    </li>
                                                                @elseif ($data['main']->fk_lkp_status == 6)
                                                                    <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 6) current-item @endif">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Bayaran Gagal</span>
                                                                    </li>
                                                                @elseif ($data['main']->fk_lkp_status == 9)
                                                                    <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 6) current-item @endif">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Sebut Harga baru Dijana</span>
                                                                    </li>
                                                                @endif
                                                                @if ($data['main']->fk_lkp_status != 5)
                                                                    <li class="step-progress-item mb-5">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Tempahan Selesai</span>
                                                                    </li>
                                                                @elseif ($data['main']->fk_lkp_status == 5)
                                                                    <li class="step-progress-item mb-5 current-item">
                                                                        <span class="progress-count">{{ $i++ }}</span>
                                                                        <span class="progress-label">Tempahan Selesai</span>
                                                                    </li>
                                                                @endif
                                                            <!-- Dewan::LO/PO::End -->
                                                            @else
                                                                <li class="step-progress-item mb-5 @if($data['main']->fk_lkp_status == 3) current-item @endif">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Diproses (LO/PO)</span>
                                                                </li>
                                                                <li class="step-progress-item mb-5">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Berjaya (Bayaran Deposit)</span>
                                                                </li>
                                                                <li class="step-progress-item mb-5">
                                                                    <span class="progress-count">{{ $i++ }}</span>
                                                                    <span class="progress-label">Tempahan Selesai</span>
                                                                </li>
                                                            @endif
                                                        @endif
                                                    <!-- Dewan::End -->
                                                    @endif
                                                </ul>
                                            </section>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card card-custom gutter-b">
                                        <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                            <div class="card-title text-center">
                                                <span class="card-label font-weight-bolder text-dark mb-1">Senarai Audit Trail </span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                @php
                                                    $i = 1;
                                                @endphp
                                                <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                                    <thead> 
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Nama Pengguna</th>
                                                            <th>Tindakan</th>
                                                            <th>Aktiviti</th>
                                                            <th>Tarikh Tindakan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($data['audit_log'] as $a)
                                                            <tr>
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ Helper::get_name($a->fk_users) }}</td>
                                                                <td>{{ Helper::audit_task_desc($a->fk_lkp_task) }}</td>
                                                                <td>{{ ($a->task) }}</td>
                                                                <td>{{ Helper::datetime_format($a->created_at) }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div> 
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.audit')
@endsection
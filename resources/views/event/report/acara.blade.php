@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Senarai Acara</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Acara</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Acara</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-3">
                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3">
                                                <option value="">Sila Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}" {{($l->id== $data['id']) ? 'selected' : ''}}>{{ $l->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <select name="status" id="kt_select2_4" class="form-control mt-3">
                                                <option value="">Sila Pilih Status</option>
                                                @foreach ($data['status'] as $b)
                                                    <option value="{{ $b->id }}" {{($b->id == $data['mode']) ? 'selected' : ''}}>{{ $b->ls_description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="mula" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Penggunaan Dari" autocomplete="off" required value="{{ isset($data['mula']) ? Helper::date_format($data['mula']) : '' }}">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="tamat" class="form-control mt-3" id="kt_daterangepicker_6" placeholder="Tarikh Penggunaan Hingga" autocomplete="off" required value="{{ isset($data['tamat']) ? Helper::date_format($data['tamat']) : '' }}">
                                        </div>
                                        {{-- <div class="col-lg-3">
                                            <div class='input-group' id="kt_daterangepicker_2">
                                                <div class="input-icon">
                                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" required />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div><div class="input-group-append mt-3">
                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="col-lg-1">
                                            <div class="float-right">
                                                <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <b>PERBADANAN PUTRAJAYA</b>
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <b>LAPORAN SENARAI ACARA</b>
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <b>Dari : {{Helper::date_format($data['mula'])}}</b>&nbsp;<b> Hingga : {{ Helper::date_format($data['tamat'])}} </b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-condensed"  id="body"> 
                                                <thead>
                                                    <tr>
                                                        <th class="th-sortable active"><b>Bil.</b></th>
                                                        <th class="th-sortable active"><b>No Tempahan</b></th>
                                                        <th class="th-sortable active"><b>Tarikh Acara</b></th>
                                                        <th class="th-sortable active"><b>Nama Acara</b></th>
                                                        <th class="th-sortable active"><b>Agensi</b></th>
                                                        <th class="th-sortable active"><b>Lokasi</b></th>
                                                        <th class="th-sortable active"><b>Peserta</b></th>
                                                        <th class="th-sortable active"><b>Peringkat</b></th>
                                                        <th class="th-sortable active"><b>JKA-PPj</b></th>
                                                        <th class="th-sortable active"><b>Sewa Tapak (Bayar)</b></th>
                                                        <th class="th-sortable active"><b>Sewa Tapak (Baki)</b></th>
                                                        <th class="th-sortable active"><b>Deposit</b></th>
                                                        <th class="th-sortable active"><b>Status</b></th>
                                                    </tr>
                                                </thead>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @forelse($data['dataacara'] as $key => $value)
                                                    <tr style="page-break-inside:avoid;">
                                                    <td width="30px"><?php echo $i ?></td>
                                                    <td width="300px">{{ $value->bmb_booking_no }}</td>
                                                    <td width="300px">{{ date('d-m-Y',strtotime($value->event_date)) }}</td>
                                                    <td width="300px">{{ $value->event_name }}</td>
                                                    <td width="300px">{{ $value->agency_name }}</td>
                                                    <td width="300px">{{ $value->name }}</td>
                                                    <td width="300px">{{ $value->visitor_amount }} orang</td>
                                                    <?php $rank='';
                                                    if($value->rank_host==1){

                                                    $rank='Antarabangsa';

                                                    }else if($value->rank_host==2){
                                                    $rank='Kebangsaan';

                                                    }else if($value->rank_host==3){
                                                    $rank='Putrajaya';

                                                    }else{
                                                    if($value->other_rank==''){
                                                        $rank='';

                                                    }else{
                                                        $rank=$value->other_rank;

                                                    }
                                                    
                                                    }?>
                                                    <td width="300px"><?php echo $rank;?></td>
                                                    <td width="100px">{{ $value->meeting_no }}</td>
                                                    <td width="300px">{{ number_format($value->bayar,2) }}</td>
                                                    <?php $baki=0;
                                                        $baki=($value->bmb_rounding-$value->bayar);
                                                    ?>
                                                    <td width="300px">{{ number_format($baki,2) }}</td>
                                                    <td width="300px">{{ number_format($value->bmb_deposit_rm,2) }}</td>
                                                    <td width="300px">{{ $value->ls_description }}</td>

                                                    <?php $i++;?>
                                                    @empty
                                                    <tr><td colspan='12'></td></tr>
                                                @endforelse
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('event.report.js.acara')
@endsection
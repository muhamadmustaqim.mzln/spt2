@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Log Audit Acara</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Acara</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Laporan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Log Audit Acara</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-custom gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0 pt-5">
                                    <h3 class="card-title align-items-start flex-column mb-5">
                                        <span class="card-label font-weight-bolder text-dark mb-1">Carian Pengguna</span>
                                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                    </h3>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <form action="" method="post">
                                    @csrf
                                    <input hidden type="text" name="type" value="2">
                                    <div class="card-body pt-2">
                                        <h5 class="mt-5">Masukkan No. Kad Pengenalan Pengguna / Nama</h5>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <input required class="form-control" type="text" name="search" placeholder="Kad Pengenalan/ Nama - Pengguna" autocomplete="off" required value="{{ $data['search'] }}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col">
                                                <div class="float-right">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Body-->
                            </div>
                        </div>
                    </div>
                    <!--begin::Card-->
                    @if ($data['post'] == true)
                        @if ($data['type'] == 2)
                            <div class="card card-custom">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Log Audit</h3>
                                    </div>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        <!--<a href="{{ url('/admin/roleform') }}" class="btn btn-primary font-weight-bolder">
                                            <i class="flaticon-add-circular-button"></i> Tambah Peranan
                                        </a>-->
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Pengguna</th>
                                                    <th>Tindakan</th>									
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($data['users'] as $t)
                                                <tr>
                                                    <td style="width: 5%;">{{ $i++ }}</td>
                                                    <td>{{ $t->fullname }}</td>
                                                    <td><a href="{{ url('event/report/penggunaan', Crypt::encrypt($t->id)) }}" class="btn btn-primary btn-sm">Lihat</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--end: Datatable-->
                                </div>
                            </div>
                        @elseif ($data['type'] == 3)
                            <div class="card card-custom">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Log Audit</h3>
                                    </div>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        <!--<a href="{{ url('/admin/roleform') }}" class="btn btn-primary font-weight-bolder">
                                            <i class="flaticon-add-circular-button"></i> Tambah Peranan
                                        </a>-->
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Pengguna</th>
                                                    <th>Aktiviti</th>									
                                                    <th>Tarikh & Masa</th>      
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($data['log'] as $t)
                                                <tr>
                                                    <td style="width: 5%;">{{ $i++ }}</td>
                                                    <td>{{ Helper::get_nama($t->fk_users) }}</td>
                                                    {{-- <td>{{ $t->fk_lkp_task }} <br> {{ $t->task }}</td> --}}
                                                    <td>{{ $t->task }}</td>
                                                    <td>{{ Helper::datetime_format2($t->created_at) }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--end: Datatable-->
                                </div>
                            </div>
                        @endif

                    @endif
                <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('event.report.js.usage')
@endsection
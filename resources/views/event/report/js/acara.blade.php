<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        var table = $('table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    orientation: 'landscape',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        // Reduce font size
                        doc.defaultStyle.fontSize = 8;

                        doc.content[1].table.headerRows = 1; // Ensure only the first row is treated as a header
                        doc.styles.tableHeader.fontSize = 8;
                        // Adjust table column widths
                        // var colWidths = [];
                        // $('table').find('thead th').each(function() {
                        //     colWidths.push('*');
                        // });
                        // doc.content[1].table.widths = colWidths;
                        // doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
    });
</script>
<script>
    // date picker
    $('#kt_daterangepicker_2').daterangepicker({
        autoApply: true,
        autoclose: true,
    }, function(start, end, label) {
        $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });
</script>
<script>
    $('#jeniskutipan').on('input', function() {
        var jeniskutipan = $(this).val();
        console.log(jeniskutipan);
        if(jeniskutipan == 1) {
            $('#jenisBayaranCol').removeAttr('hidden');
        } else {
            $('#jenisBayaranCol').attr('hidden', true);
        }
    })
    $('#kt_daterangepicker_4').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        autoApply: true,
        // minDate: new Date(),
        // minDate: moment().add(6, 'months'), 
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_4').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_4').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#kt_daterangepicker_5').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        autoApply: true,
        // minDate: new Date(),
        // minDate: selectedDate,
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    // date picker
    $(function() {
        $('#kt_daterangepicker_6').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
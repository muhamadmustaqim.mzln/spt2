@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pemulangan Deposit</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Pemulangan Deposit</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                                <tr>
                                    <th style="width: 20%;">Nama</th>
                                    <td>NORDIN BIN ABU BAKAR</td>
                                </tr>
                                <tr>
                                    <th>No. Kad Pengenalan</th>
                                    <td>550414-03-5091</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Deposit (RM)</th>
                                    <td>1000.00</td>
                                </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="5" style="text-center">Pemotongan Deposit</th>
                                </tr>
                                <tr>
                                    <th style="width: 2%;"></th>
                                    <th style="width: 20%;">Akaun</th>
                                    <th>Keterangan</th>
                                    <th style="width: 10%;">Tarikh</th>
                                    <th style="width: 10%;">Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>                                    
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked" name="Checkboxes1"/>
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>Pemotongan Deposit</td>
                                    <td>Pemotongan Deposit (Kebersihan)</td>
                                    <td>2022-11-12</td>
                                    <td class="text-right">50.00</td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="checkbox">
                                            <input type="checkbox" checked="checked" name="Checkboxes1"/>
                                            <span></span>
                                        </label>
                                    </td>
                                    <td>Pemotongan Deposit</td>
                                    <td>Pemotongan Deposit (Peralatan)</td>
                                    <td>2022-11-12</td>
                                    <td class="text-right">20.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="5" style="text-center">Tunggakan</th>
                                </tr>
                                <tr>
                                    <th style="width: 2%;"></th>
                                    <th style="width: 20%;">Akaun</th>
                                    <th>Keterangan</th>
                                    <th style="width: 10%;">Tarikh</th>
                                    <th style="width: 10%;">Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['tunggak'] as $l)
                                    @foreach($l as $d)
                                    <tr>
                                        <td>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes1"/>
                                                <span></span>
                                            </label>
                                        </td>
                                        <td>{{ $d['Akaun'] }}</td>
                                        <td>{{ $d['Keterangan'] }}</td>
                                        <td>{{ date('Y-m-d', strtotime($d['Tarikh'])) }}</td>
                                        <td class="text-right">{{ number_format((float)$d['Jumlah'], 2, '.', ''); }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                                <tr>
                                    <th>Jumlah Penolakan Deposit (RM)</th>
                                    <td class="text-right" style="width: 10%;">70.00</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Pemulangan Deposit (RM)</th>
                                    <td class="text-right" style="width: 10%;">0.00</td>
                                </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('bapi.js.lists')
@endsection
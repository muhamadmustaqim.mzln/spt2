<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.2.9') }}"></script>
<script>
    var state = 1;
    $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
        if($('#TheCheckBox').bootstrapSwitch('state') == true){
            state = 1
        }else{
            state = 0
        }
    });
    $('input[name="status"]').val(state);

    function addTarikhBukaFields() {
        var clone = $('#tarikhBukaFields').clone();
        clone.find('input[type="text"]').val(''); // Clear the input value
        clone.appendTo('#formContainer'); // Append to the form container
        clone.show(); // Show the cloned fields
    }

    var counter = 1;
    function addTarikhBukaFields() {
        var clone = $('#tarikhBukaFields').clone(); // Clone the template
        clone.attr('id', 'tarikhBukaFields_' + counter); // Set a unique ID
        clone.find('.date-field').attr('name', 'date_' + counter); // Set a unique name for the date field
        clone.find('.status-field').attr('name', 'status_' + counter); // Set a unique name for the status field
        clone.appendTo('#formContainer'); // Append the cloned fields to the form container
        clone.show(); // Show the cloned fields // Set a unique name for the status field
        clone.find('.status-switch').remove();
        initializeDatepicker(clone.find('.date-field'));// Initialize datepicker for the new field
        // initializeSwitch(clone.find('.status-switch'));
        counter++; // Increment the counter
    }
    // Function to initialize datepicker
    function initializeDatepicker(element) {
        element.daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        element.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        element.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    }
    // Function to initialize switch
    function initializeSwitch(element) {
        element.bootstrapSwitch({
            onText: 'Aktif',
            offText: 'Tidak Aktif',
            onColor: 'success',
            offColor: 'warning'
        });
    }
    // Event listener for the Tambah button
    $('#tambahTarikhBuka').click(function() {
        addTarikhBukaFields();
    });
</script>
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
                fasiliti: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Pilih Fasiliti'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $('#kt_select2_1').select2({
        placeholder: 'Sila Pilih Fasiliti'
    });

    $('#kt_select2_2').select2({
        placeholder: 'Sila Pilih Hari'
    });

    $('#jenis').on('change', function() {
        var type =($(this).val())
        if(type == 0){
            $('#bulan').prop('hidden', false);
            $('#tarikh').prop('hidden', true);
            $('#bulan input').prop('required', true);
            $('#tarikh input').prop('required', false);
        } else if(type == 1){
            $('#bulan').prop('hidden', true);
            $('#tarikh').prop('hidden', false);
            $('#bulan input').prop('required', false);
            $('#tarikh input').prop('required', true);
        }
    })

    function disableZero(event) {
        var input = event.target;
        console.log(input)
        if (input.value === "0") {
            input.value = 1;
        }
    }

    $('.kt_daterangepicker_c').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        autoApply: true,
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('.kt_daterangepicker_c').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('.kt_daterangepicker_c').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#kt_daterangepicker_4').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        autoApply: true,
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_4').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_4').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
</script>
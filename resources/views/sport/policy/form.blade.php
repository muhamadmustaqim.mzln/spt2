@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Polisi</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Polisi</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/sport/admin/policy/add') }}" id="form" method="post">
                @csrf
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Tambah Polisi</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Fasiliti :</label>
                            <div class="col-lg-9">
                                <select required name="fasiliti" class="form-control" id="kt_select2_1">
                                    <option value="">Sila Pilih Fasiliti</option>
                                    @foreach($data['facility'] as $f)
                                        <option value="{{ $f->id }}">{{ Helper::getSportFacility($f->id) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jenis Halangan :</label>
                            <div class="col-lg-8">
                                <select required name="jenis" class="form-control" id="jenis">
                                    <option value="0">Halangan Sebulan</option>
                                    <option value="1">Halangan Tarikh</option>
                                </select>
                            </div>
                        </div>
                        <div id="bulan">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Slot :</label>
                                <div class="col-lg-9">
                                    <input required type="number" min="1" oninput="disableZero(event)" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" name="slot" class="form-control" placeholder="Bilangan Slot" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Gelanggang :</label>
                                <div class="col-lg-9">
                                    <input required type="number" min="1" oninput="disableZero(event)" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" name="max_court" class="form-control" placeholder="Bilangan Slot" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Slot Per Gelanggang :</label>
                                <div class="col-lg-9">
                                    <input required type="number" min="1" oninput="disableZero(event)" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" name="max_per_court" class="form-control" placeholder="Bilangan Slot" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Tempahan Per Hari :</label>
                                <div class="col-lg-9">
                                    <input required type="number" min="1" oninput="disableZero(event)" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" name="max_per_day" class="form-control" placeholder="Bilangan Slot" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Hari Per Bulan :</label>
                                <div class="col-lg-9">
                                    <input required type="number" min="0" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" name="tempoh" class="form-control" placeholder="Tempoh (Hari)" value="">
                                </div>
                            </div>
                        </div>
                        <div hidden id="tarikh">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Buka:</label>
                                <div class="col-lg-3">
                                    <input type="text" name="date" class="form-control mt-3" id="kt_daterangepicker_4" placeholder="Tarikh" autocomplete="off" value="">
                                </div>
                                <label class="col-lg-3 col-form-label text-right font-weight-bold">Status:</label>
                                <div class="col-lg-3">
                                    {{-- <input type="hidden" name="status" value="0"> --}}
                                    <input data-switch="true" type="checkbox" id="TheCheckBox" name="status" value="1" data-on-text="Aktif" data-off-text="Tidak Aktif" data-on-color="success"/>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Fasiliti :</label>
                            <div class="col-lg-10">
                                <select name="fasiliti" class="form-control" id="kt_select2_1">
                                    <option value="">Sila Pilih Fasiliti</option>
                                    @foreach($data['facility'] as $f)
                                        <option value="{{ $f->id }}">{{ Helper::getSportFacility($f->id) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Bilangan Slot :</label>
                            <div class="col-lg-10">
                                <input type="text" name="slot" class="form-control" placeholder="Bilangan Slot">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tempoh (Hari) Ahad - Sabtu :</label>
                            <div class="col-lg-10">
                                <input type="text" name="tempoh" class="form-control" placeholder="Tempoh (Hari) ">
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/sport/admin/policy') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.policy.js.form')
@endsection
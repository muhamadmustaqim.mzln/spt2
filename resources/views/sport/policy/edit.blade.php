@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Polisi</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Polisi</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="form-group row" id="tarikhBukaFields" style="display:none;">
                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Buka:</label>
                <div class="col-lg-3">
                    <input type="text" class="form-control kt_daterangepicker_c date-field" placeholder="Tarikh" autocomplete="off" required>
                </div>
                <label class="col-lg-2 col-form-label text-right font-weight-bold">Status:</label>
                <div class="col-lg-4">
                    <span class="switch switch-icon">
                        <input hidden class="status-field" value="0">
                        <label>
                            <input type="checkbox" class="status-field" id="checkbox"  />
                            <span></span>
                        </label>
                        <label class="ml-2 text-muted" id="textbox">Aktif</label>
                    </span>
                </div>
            </div>
            <!--begin::Card-->
            <form action="{{ url('/sport/admin/policy/update',[Crypt::encrypt($data['list']->id), Crypt::encrypt($data['type'])]) }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Polisi</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Fasiliti :</label>
                            <div class="col-lg-9">
                                <select disabled name="fasiliti" class="form-control" id="kt_select2_1">
                                    <option value="">Sila Pilih Fasiliti</option>
                                    @foreach($data['facility'] as $f)
                                        <option value="{{ $f->id }}" {{($data['list']->fk_et_facility_type == $f->id) ? 'selected' : ''}}>{{ Helper::getSportFacility($f->id) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jenis Halangan :</label>
                            <div class="col-lg-8">
                                <select required name="jenis" class="form-control" id="jenis">
                                    <option value="" selected>Sila Pilih Jenis Halangan</option>
                                    <option value="0" @if($data['list']->type == 0) selected @endif>Halangan Sebulan</option>
                                    <option value="1" @if($data['list']->type == 1) selected @endif>Halangan Tarikh</option>
                                </select>
                            </div>
                        </div>
                        @if ($data['type'] == 1)
                            <div class="" id="tarikh">
                                <div class="form-group row">
                                    <div class="col-4 d-flex justify-content-end">
                                        <button id="tambahTarikhBuka" class="btn btn-light-info">Tambah</button>
                                    </div>
                                </div>
                                @if(count($data['sintetik']) > 0)
                                    @foreach ($data['sintetik'] as $v)
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Buka:</label>
                                            <div class="col-lg-3">
                                                <input type="text" name="date_{{$v->id}}" class="form-control kt_daterangepicker_c" id="kt_daterangepicker_4" placeholder="Tarikh" autocomplete="off" required value="{{ Helper::date_format($v->efsob_open_date) }}">
                                            </div>
                                            <label class="col-lg-2 col-form-label text-right font-weight-bold">Status:</label>
                                            <div class="col-lg-4">
                                                <span class="switch switch-icon">
                                                    <input hidden name="status_{{$v->id}}" value="0">
                                                    <label>
                                                        <input type="checkbox" {{($v->efsob_status == 1) ? 'checked' : ''}} id="checkbox{{$v->id}}" name="status_{{$v->id}}" value="1"/>
                                                        <span></span>
                                                    </label>
                                                    <label class="ml-2 text-muted" id="textbox{{$v->id}}" style="{{($v->efsob_status == 1) ? 'display: inline;' : 'display: none;'}}">{{($v->efsob_status == 1) ? 'Aktif' : ''}}</label>
                                                </span>
                                                {{-- <input hidden name="status_{{$v->id}}" value="0">
                                                <input data-switch="true" type="checkbox" id="TheCheckBox" name="status_{{$v->id}}" value="1" {{($v->efsob_status == 1) ? 'checked="checked"' : ''}}  data-on-text="Aktif" data-off-text="Tidak Aktif" data-on-color="success"/> --}}
                                            </div>
                                        </div>
                                    @endforeach
                                {{-- @else
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Buka:</label>
                                        <div class="col-lg-4">
                                            <input type="text" name="date_0" class="form-control kt_daterangepicker_c" id="kt_daterangepicker_4" placeholder="Tarikh" autocomplete="off" required value="">
                                        </div>
                                        <label class="col-lg-2 col-form-label text-right font-weight-bold">Status:</label>
                                        <div class="col-lg-4">
                                            <input data-switch="true" type="checkbox" id="TheCheckBox" name="status_0" value="1" data-on-text="Aktif" data-off-text="Tidak Aktif" data-on-color="success"/>
                                        </div>
                                    </div> --}}
                            </div>
                            @endif
                            <div id="formContainer"></div>
                        @else
                            <div class="" id="bulan">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Slot :</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="slot" class="form-control" placeholder="Bilangan Slot" value="{{ $data['list']->max_slot }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Gelanggang :</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="max_court" class="form-control" placeholder="Bilangan Slot" value="{{ $data['list']->max_court }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Slot Per Gelanggang :</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="max_per_court" class="form-control" placeholder="Bilangan Slot" value="{{ $data['list']->max_per_court }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Tempahan Per Hari :</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="max_per_day" class="form-control" placeholder="Bilangan Slot" value="{{ $data['list']->max_per_day }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Maksimum Hari Per Bulan :</label>
                                    <div class="col-lg-9">
                                        <input type="text" name="tempoh" class="form-control" placeholder="Tempoh (Hari)" value="{{ $data['list']->expiry_by_month }}">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tempoh (Hari) :<br>*Minggu adalah Isnin - Ahad</label>
                                <div class="col-lg-9">
                                    <input type="text" name="tempoh" class="form-control" placeholder="Tempoh (Hari)" value="{{ $data['list']->expiry_by_month }}">
                                </div>
                            </div> --}}
                        @endif
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('sport/admin/policy') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary btn-confirm font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.policy.js.form')
@endsection
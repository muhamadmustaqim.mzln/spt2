@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            {{-- <div class="card card-custom gutter-b">
                <div class="card-body pt-2">
                    <div class="row">
                            <h3>TEMPAHAN YANG TIDAK DIAMBIL TINDAKAN PENGESAHAN AKAN DISAHKAN DALAM TEMPOH 3 HARI DARI TARIKH PERMOHONAN</h3>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                {{-- <div class="col-lg-5">
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 bg-dark">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-white mb-1">Kutipan Bayaran</span>
                            </h3>
                        </div>
                        <div class="card-body pt-5">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jenis Bayaran</label>
                                <div class="col-lg-8">
                                    <input hidden type="text" name="lkp_status" id="lkp_status" value="{{ $data['main']->fk_lkp_status }}">
                                    <input hidden type="text" name="hall_val" id="hall_val" value="{{ $data['main']->bmb_total_book_hall }}">
                                    <input hidden type="text" name="equipment_val" id="equipment_val" value="{{ $data['main']->bmb_total_equipment }}">
                                    <input hidden type="text" name="deposit" id="deposit" value="{{ $data['main']->bmb_deposit_rm }}">
                                    <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4, 13])) disabled @endif name="jenis_bayaran" id="jenis_bayaran" class="form-control mt-3" required>
                                        <option value="" selected disabled>Sila Pilih </option>
                                        @if ($data['main']->fk_lkp_status == 13 || $data['main']->fk_lkp_status == 2)
                                            <option value="1">DEPOSIT & PENDAHULUAN</option>
                                            <option value="2">PENUH</option>
                                        @elseif ($data['main']->fk_lkp_status == 3 || $data['main']->fk_lkp_status == 4)
                                            <option value="2">PENUH</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jumlah Tempahan(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control " id="jumlahTempahanId" name="jumlahTempahan" placeholder="Jumlah Tempahan(RM)" value="{{ $data['main']->bmb_subtotal }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jumlah Pendahuluan Tempahan(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled ="text" class="form-control" name="jumlahPendahuluan" id="pendahuluan" placeholder="Jumlah Pendahuluan Tempahan(RM)" value="{{ Helper::moneyhelper($data['main']->bmb_total_book_hall/2) }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Deposit(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="deposit" placeholder="Deposit(RM)" value="{{ Helper::moneyhelper($data['main']->bmb_deposit_rm) }}"/>
                                </div>
                            </div>
                            @php
                                $jumlahdahdibayar = 0;
                            @endphp
                            @if (count($data['kaunter']) > 0)
                                @foreach ($data['kaunter'] as $k)
                                    @php
                                        $jumlahdahdibayar += $k->amount_received;
                                    @endphp
                                @endforeach
                            @endif
                            @if (count($data['online']) > 0)
                                @foreach ($data['online'] as $k)
                                    @php
                                        $jumlahdahdibayar += $k->amount_paid;
                                    @endphp
                                @endforeach
                            @endif
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jumlah Telah Dibayar(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="jumlahtelahdibayar" placeholder="Jumlah Telah Dibayar(RM)" value="@if ($data['main']->fk_lkp_status == 2)0.00 @elseif($data['main']->fk_lkp_status == 4 || $data['main']->fk_lkp_status == 5) {{ Helper::moneyhelper($jumlahdahdibayar) }} @endif"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jumlah Perlu Dibayar(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="jumlahPerluDibayar" id="jumlahperludibayar" placeholder="Jumlah Perlu Dibayar(RM)" value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">No. Sebutharga</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="sebutharga" placeholder="No Sebutharga" value="{{ $data['bh_quotation']->bq_quotation_no }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label font-weight-bold">Cara Bayaran</label>
                                <div class="col-lg-8">
                                    <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4, 13])) disabled @endif required name="payment_mode" id="carabayaran" class="form-control mt-3" required>
                                        <option value="" selected disabled>Sila Pilih</option>
                                        @foreach ($data['payment_mode'] as $pm)
                                            <option value="{{ $pm->id }}">{{ $pm->lpm_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div hidden id="nocek" class="form-group row">
                                <label class="col-lg-3 col-form-label  font-weight-bold">No Cek</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="nocek" placeholder="" value=""/>
                                </div>
                            </div>
                            <div hidden id="namabank" class="form-group row">
                                <label class="col-lg-3 col-form-label  font-weight-bold">Nama Bank</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="namabank" placeholder="" value=""/>
                                </div>
                            </div>
                            <div id="lopo" class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Nombor LO/PO</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="lopo" placeholder="" value=""/>
                                </div>
                            </div>
                            <div class="form-group row" id="norujukanbayaran">
                                <label class="col-lg-4 col-form-label  font-weight-bold">No Rujukan Bayaran</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="rujukanBayaran" placeholder="No Rujukan Bayaran" value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold">Jumlah Bayaran (RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value=""/>
                                    <input hidden type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-12">
                    <!--begin::Maklumat Pemohon-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                            </h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2">
                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                            <td>{{ $data['user']->fullname }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                            <td>{{ $data['main']->bmb_booking_no }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                            <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                            <td>{{ $data['user']->email }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                            <td>{{ $data['main']->bmb_subtotal }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                            <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                            <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 d-flex justify-content-end">
                                    {{-- <a href="{{ url('hall/borangbpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Borang BPSK</a> --}}
                                    <a href="{{ url('sport/janasebutharga', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary">Jana Sebut Harga</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Maklumat Pemohon-->
                    
                    <!--start::Maklumat Acara-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Fasiliti / Lokasi</th>
                                            <th>Tarikh Mula</th>
                                            <th>Tarikh Akhir</th>
                                            <th>Tujuan Tempahan</th>
                                            <th>Acara</th>
                                            <th>Maklumat Acara</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $i = 0;
                                        $j = 1;
                                    @endphp
                                    <tbody>
                                        @if(count($data['et_booking_facility_detail']) > 0)
                                            @foreach($data['et_booking_facility_detail'] as $p)
                                            <tr>
                                                <td>{{ Helper::tempahanSportDetail(Helper::get_et_booking_facility($p->fk_et_booking_facility)->fk_et_facility_detail) }}, {{ $p->ebfd_venue }}</td>
                                                <td>{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                <td>{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                                <td>{{ $p->ebfd_event_name }}</td>
                                                <td>{{ $p->ebfd_event_desc }}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                </div>
            </div>
            @php
                $paid = number_format(0, 2);
            @endphp
            <form action="" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <!--start::Senarai Pecahan Slot-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bolder text-dark">Senarai Pecahan Slot</span>
                                </h3>
                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Bil</th>
                                                <th>Nama Dewan / Fasiliti</th>
                                                <th>Tarikh</th>
                                                <th>Jenis Penggunaan</th>
                                                <th>Slot Masa</th>
                                                <th>Harga (RM)<br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th>
                                                {{-- <th>Kod GST</th>
                                                <th>GST (0%)</th> --}}
                                                <th>Diskaun <br>Khas (%)</th>
                                                <th>Diskaun <br>Khas (RM)</th>
                                                {{-- <th>GST</th> --}}
                                                <th class="text-right">Jumlah(RM)</th>
                                            </tr>
                                        </thead>
                                        @php
                                            $j = 1;
                                            $jumlahslot = 0;
                                            $jumlahkeseluruhanslot = 0;
                                        @endphp
                                        <tbody>
                                            @foreach ($data['eht_ehb_ebf'] as $eht)
                                                <tr>
                                                    <td>{{ $j }}</td>
                                                    <td>{{ Helper::tempahanSportDetail(Helper::get_et_booking_facility($p->fk_et_booking_facility)->fk_et_facility_detail) }}</td>
                                                    <td>{{ Helper::date_format($eht->ehb_booking_date) }}</td>
                                                    <td>{{ Helper::typeFunction($eht->fk_et_function) }}</td>
                                                    <td>{{ Helper::get_slot_masa($eht->fk_et_slot_time) }}</td>
                                                    <td class="text-right" id="total_{{ $j }}">{{ $eht->eht_total }}</td>
                                                    <td><input value="{{ $eht->eht_special_disc }}" class="form-control form-control-sm per_{{ $j }} percentage-input" name="perc[]" data-perctr="{{ $j }}" value="0" type="number" min="0" max="100" step="0.01"></td>
                                                    <td><input value="{{ $eht->eht_special_disc_rm }}" class="form-control form-control-sm val_{{ $j }} fixed-value-input" name="val[]" data-valctr="{{ $j }}" value="0.0" type="number" min="0" max="{{ $eht->eht_total }}" step="0.1"></td>
                                                    <td id=""><input type="text" disabled class="form-control form-control-sm text-right subtotal_{{ $j }}" name="subtotal[]" value="{{ $eht->eht_subtotal }}"></td>
                                                    <input hidden type="text" class="form-control form-control-sm text-right hdsubtotal_{{ $j }}" name="hdsubtotal[]" value="{{ $eht->eht_subtotal }}">
                                                </tr>
                                                @php
                                                    $j++;
                                                    $jumlahslot += $eht->eht_total;
                                                    $jumlahkeseluruhanslot += $eht->eht_subtotal;
                                                @endphp
                                            @endforeach
                                            <tr>
                                                <td colspan="5" class="text-right font-weight-bolder">Jumlah (RM)</td>
                                                <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahslot) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right font-weight-bolder" id="subtotal">{{ Helper::moneyhelper($jumlahkeseluruhanslot) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Kadar Sewaan Dewan-->
                        <!--start::Kadar Sewaan Peralatan-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                                </h3>
                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Tarikh Kegunaan</th>
                                                <th>Penggunaan</th>
                                                <th>Item</th>
                                                <th>Kuantiti</th>
                                                <th>Kadar Seunit (RM)</th>
                                                <th>Jumlah (RM)</th>
                                                <th>Diskaun Khas (%)</th>
                                                <th>Diskaun Khas (RM)</th>
                                                {{-- <th>Kod GST</th>
                                                <th>GST (6%)</th> --}}
                                                <th>Jumlah Sewaan (RM)</th>
                                            </tr>
                                        </thead>
                                        @php
                                            $k = 1;
                                            $jumlahperalatan = 0;
                                            $jumlahsewaanperalatan = 0;
                                        @endphp
                                        <tbody>
                                            @foreach ($data['et_equipment_book'] as $eeb)
                                                <tr>
                                                    <td>{{ Helper::date_format($eeb->eeb_booking_date) }}</td>
                                                    <td>{{ Helper::typeFunction($eeb->fk_et_function) }}</td>
                                                    <td>{{ Helper::get_equipment($eeb->fk_et_equipment) }}</td>
                                                    <td>{{ $eeb->eeb_quantity }}</td>
                                                    <td class="text-right">{{ $eeb->eeb_unit_price }}</td>
                                                    <td class="text-right" id="eqtotal_{{ $k }}">{{ $eeb->eeb_total }}</td>
                                                    <td><input value="{{ ($eeb->eeb_special_disc == null) ? Helper::moneyhelper(0.00) : $eeb->eeb_special_disc }}" class="form-control form-control-sm eqper_{{ $k }} eqpercentage-input" name="eqperc[]" data-eqperctr="{{ $k }}" value="0" type="number" min="0" max="100" step="0.01"></td>
                                                    <td><input value="{{ ($eeb->eeb_special_disc_rm == null) ? Helper::moneyhelper(0.00) : $eeb->eeb_special_disc_rm }}" class="form-control form-control-sm eqval_{{ $k }} eqfixed-value-input" name="eqval[]" data-eqvalctr="{{ $k }}" value="0.0" type="number" min="0" max="{{ $eeb->eeb_total }}" step="0.1"></td>
                                                    <td id=""><input type="text" disabled class="form-control form-control-sm text-right eqsubtotal_{{ $k }}" name="eqsubtotal[]" value="{{ $eeb->eeb_subtotal }}"></td>
                                                    <input hidden type="text" class="form-control form-control-sm text-right hdeqsubtotal_{{ $k }}" name="hdeqsubtotal[]" value="{{ $eeb->eeb_subtotal }}">
                                                </tr>
                                            @php
                                                $k++;
                                                $jumlahperalatan += $eeb->eeb_total;
                                                $jumlahsewaanperalatan += $eeb->eeb_subtotal;
                                            @endphp
                                            @endforeach
                                                <tr>
                                                    <td colspan="5" class="text-right font-weight-bolder">Jumlah (RM)</td>
                                                    <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahperalatan) }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-right font-weight-bolder" id="eqsubtotal">{{ Helper::moneyhelper($jumlahsewaanperalatan) }}</td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Kadar Sewaan Peralatan-->
                        <!--start::Jumlah Keseluruhan-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bolder text-dark">Rumusan</span>
                                </h3>
                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                            <td class="text-center">E</td>
                                            <td class="text-right">0.00</td>
                                        </tr>
                                        <tr>
                                            <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                            <td class="text-center">I</td>
                                            <td class="text-right">0.00</td>
                                        </tr>
                                        <tr>
                                            <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                            <td></td>
                                            <td class="text-right">{{ number_format($data['main']->bmb_subtotal,2) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                            <td></td>
                                            <td class="text-right">
                                                <?php
                                                if(isset($data['main']->bmb_rounding)) {
                                                    echo $data['main']->bmb_rounding;
                                                } else {
                                                    echo number_format(0, 2);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                            <td></td>
                                            <td class="text-right">{{ number_format($paid, 2) }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                        <!--end::Jumlah Keseluruhan-->
                        <div class="float-right">
                            <button type="submit" class="btn btn-success float-right font-weight-bolder mt-2 mr-2">Simpan</button>
                            <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('hall.dashboard.js.pengesahan')
@endsection				



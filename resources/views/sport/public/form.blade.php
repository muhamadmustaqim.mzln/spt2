@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Padang Sintetik</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Sukan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Pengisian Maklumat Penggunaan Padang Sintetik</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lokasi :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control bg-secondary" name="lokasi" placeholder="Lokasi" value="{{ $data['et_facility_type']->eft_type_desc }}" readonly/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Penggunaan :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control bg-secondary" name="tarikh" placeholder="Tarikh Penggunaan" value="{{ $data['date'] }}" readonly/>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jenis Penggunaan :</label>
                            <div class="col-lg-10">
                                <select name="jenis" class="form-control">
                                    <option value="">Sila Pilih Jenis Penggunaan</option>
                                    <option value="1">Dewan / Sukan</option>
                                    <option value="2">Peralatan & Kelengkapan</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control bg-secondary" name="nama" placeholder="Nama" value="{{ $data['user_detail']->bud_name }}" readonly/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kad Pengenalan :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control bg-secondary" name="ic" placeholder="No Kad Pengenalan" value="{{ $data['user_detail']->bud_reference_id }}" readonly/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Emel :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control bg-secondary" name="emel" placeholder="Emel" value="{{ $data['user_detail']->bud_email }}" readonly/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Organisasi<span class="text-danger">*</span> :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="organisasi" placeholder="Nama Organisasi" autocomplete="off" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">No Telefon Organisasi<span class="text-danger">*</span> :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="notelorganisasi" placeholder="No Telefon Organisasi" autocomplete="off"  required/>
                            </div>
                        </div>
                        <div class="form-group row" id="docSokongan">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Dokumen Sokongan<span class="text-danger">*</span> :</label>
                            <div class="col-lg-10">
                                <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                    <div class="dropzone-panel mb-lg-0 mb-2">
                                        <a class="dropzone-select btn btn-light-primary font-weight-bold btn-sm">Lampiran Dokumen</a>
                                    </div>
                                    <div class="dropzone-items">
                                        <div class="dropzone-item" style="display:none">
                                            <div class="dropzone-file">
                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                    <span data-dz-name="">some_image_file_name.jpg</span>
                                                    <strong>( 
                                                    <span data-dz-size="">340kb</span>)</strong>
                                                </div>
                                                <div class="dropzone-error" data-dz-errormessage=""></div>
                                            </div>
                                            <div class="dropzone-progress">
                                                <div class="progress">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                </div>
                                            </div>
                                            <div class="dropzone-toolbar">
                                                <span class="dropzone-start">
                                                    <i class="flaticon2-arrow"></i>
                                                </span>
                                                <span class="dropzone-Cancel" data-dz-remove="" style="display: none;">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                                <span class="dropzone-delete" data-dz-remove="">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="form-text text-muted">Fail dalam bentuk PDF dan gambar sahaja.</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/sport/details/{id}/{date?}/{fasility?}') }}" class="btn btn-outline-danger font-weight-bold">Kembali</a>
                            {{-- <button class="btn btn-primary font-weight-bold">Simpan</button> --}}
                            <a href="{{ url('sport/dashboard/validation', Crypt::encrypt($data['etbooking'])) }}" class="btn btn-primary float-right font-weight-bolder mx-2" ><i class="fas fa-check-circle"></i> Teruskan</a>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.public.js.form')
@endsection
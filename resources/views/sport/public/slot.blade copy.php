@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b" style="background-position: right top; background-size: auto 80%; background-repeat: no-repeat; background-image: url('{{ asset('assets/media/svg/shapes/abstract-3.svg') }}')">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <select name="" id="" class="form-control mt-2">
                                <option value="">Sila Pilih Lokasi</option>
                                <option value="">Ping Pong</option>
                                <option value="">Badminton</option>
                                <option value="">Bola Tampar</option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <select name="" id="" class="form-control mt-2">
                                <option value="">Sila Pilih Jenis Fasiliti</option>
                                <option value="">Ping Pong</option>
                                <option value="">Badminton</option>
                                <option value="">Bola Tampar</option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control mt-2" placeholder="Tarikh">
                        </div>
                        <div class="col-lg-6">
                            <select name="" id="" class="form-control mt-2">
                                <option value="">Sila Pilih Fasiliti</option>
                                <option value="">9:00am - 10:00am</option>
                                <option value="">10:00am - 11:00am</option>
                                <option value="">11:00am - 12:00pm</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <select name="" id="" class="form-control mt-2">
                                <option value="">Sila Pilih Slot Masa</option>
                                <option value="">Presint 8</option>
                                <option value="">Presint 9</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer border-0">
                    <div class="float-right">
                        <button type="submit" class="form-control btn btn-light-info font-weight-bold"><i class="flaticon-search-magnifier-interface-symbol"></i>  Carian</button>
                    </div>
                </div>
            </div>
            <!--end::List Widget 21-->
            <!--begin::Card-->
            <form action="{{ url('sport/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
            @csrf
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                        <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Jenis Fasiliti</th>
                                    <th>Lokasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['slot'] as $s)
                                    <tr>
                                        <td style="width: 4.5%;">
                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                <input type="checkbox" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                <span></span>
                                            </label>
                                        </td>
                                        <td>{{ $s->efd_name }}</td>
                                        <td>{{ $s->est_slot_time }}</td>
                                        <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('sport.public.js.slot')
@endsection



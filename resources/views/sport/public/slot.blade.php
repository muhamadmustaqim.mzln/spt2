@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('sport/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
            @csrf
            <div class="card card-custom">
                @if(Session::has('flash'))
                    <div class="alert alert-danger">
                        {{ Session::get('flash') }}
                    </div>
                @endif
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                        @if ($data['policy']->max_court > 0 && $data['policy']->max_court != null)
                            <span class="text-danger mt-2 font-weight-bold font-size-sm">*Hanya {{ $data['policy']->max_court }} gelanggang dibenarkan untuk ditempah.</span>
                        @endif
                        <span class="text-danger mt-2 font-weight-bold font-size-sm">*Anda hanya boleh pilih sehingga {{ ($data['policy']->max_per_court == null) ? 2 : $data['policy']->max_per_court }} slot sahaja bagi setiap gelanggang</span>
                        @if ($data['policy']->expiry_by_month > 0)
                            <span class="text-danger mt-2 font-weight-bold font-size-sm">*Anda hanya dibenarkan membuat tempahan maksimum sebanyak {{ $data['policy']->expiry_by_month }} tempahan dalam sebulan</span>
                        @endif
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                        <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>
                                        {{-- <label class="checkbox checkbox-primary checkbox-lg">
                                            <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                            <span></span>
                                        </label> --}}
                                    </th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Jenis Fasiliti</th>
                                    <th>Lokasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <input hidden type="text" id="maxAllowed" value="{{ $data['maxAllowed'] }}">
                                <input hidden type="text" id="max_court" value="{{ $data['policy']->max_court }}">
                                <input hidden type="text" id="max_per_court" value="{{ $data['policy']->max_per_court }}">
                                @foreach($data['slot'] as $s)
                                @php
                                    $current_datetime = new DateTime();
                                    $target_time = new DateTime($s->start);
                                @endphp
                                <tr>
                                    @if ($data['bookingMonthly'] > 3)
                                        <td style="width: 4.5%;">
                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                <input type="checkbox" disabled class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                <span></span>
                                            </label>
                                        </td>
                                        <td><del>{{ $s->efd_name }}</del></td>
                                        <td><del>{{ $s->est_slot_time }}</del></td>
                                        <td><del>{{ date("d-m-Y", strtotime($data['date'])) }}</del></td>
                                        <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                        <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                    @else
                                        @if ($data['date'] > $current_datetime->format('Y-m-d'))
                                            <td style="width: 4.5%;">
                                                <label class="checkbox checkbox-primary checkbox-lg">
                                                    <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $s->efd_name }}</td>
                                            <td>{{ $s->est_slot_time }}</td>
                                            <td>{{ date("d-m-Y", strtotime($data['date'])) }} </td>
                                            <td>{{ $data['fasility']->eft_type_desc }}</td>
                                            <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                        @elseif ($current_datetime > $target_time)
                                            <td style="width: 4.5%;">
                                                <label class="checkbox checkbox-primary checkbox-lg">
                                                    <input type="checkbox" disabled class="initially-deleted chk {{ $s->fidd }} " name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td><del>{{ $s->efd_name }}</del></td>
                                            <td><del>{{ $s->est_slot_time }}</del></td>
                                            <td><del>{{ date("d-m-Y", strtotime($data['date'])) }}</del></td>
                                            <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                            <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                        @else
                                            <td style="width: 4.5%;">
                                                <label class="checkbox checkbox-primary checkbox-lg">
                                                    <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $s->efd_name }}</td>
                                            <td>{{ $s->est_slot_time }}</td>
                                            <td>{{ date("d-m-Y", strtotime($data['date'])) }} </td>
                                            <td>{{ $data['fasility']->eft_type_desc }}</td>
                                            <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                        @endif
                                    @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('sport.public.js.slot')
@endsection



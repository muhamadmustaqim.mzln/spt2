@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }

    
    .panoramacss {
        width: 100%;
        height: 300px
    }
    
    #panorama {
        width: 100%;
        height: 300px;
        background-color: rgb(222, 222, 222);
        position: relative;
        border-radius: 10px; /* Rounded corners for a smooth look */
        overflow: hidden; /* To contain the shine effect within the div */
    }

    /* Shiny effect using a pseudo-element */
    #panorama::before {
        content: '';
        position: absolute;
        top: 0;
        left: -75%;
        width: 50%;
        height: 100%;
        background: linear-gradient(
            to right,
            rgba(255, 255, 255, 0.1) 0%,
            rgba(255, 255, 255, 0.7) 50%,
            rgba(255, 255, 255, 0.1) 100%
        );
        transform: skewX(-25deg);
        transition: left 1s;
        animation: shine 2s infinite;
    }

    @keyframes shine {
        0% {
            left: -75%;
        }
        100% {
            left: 100%;
        }
    }
</style>
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		
    <!--begin::Subheader-->
    <div class="subheader py-5 py-lg-10 gutter-b subheader-transparent mt-0" id="kt_subheader" style="background-color: #20274d; background-position: right bottom; background-size: auto 100%; background-repeat: no-repeat; background-image: url('{{ asset('assets/media/svg/patterns/taieri.svg') }}')">
        <div class="container ">
            <div class="bg-white rounded p-4 ">
                <form action="" method="post" id="carianForm">
                    <div class="row">
                        @csrf
                        <div class="col-lg-3">
                            <div class="input-icon">
                                <select name="" class="form-control form-control-lg border-0 font-weight-bold" id="kt_select2_4" >
                                    {{-- <option value=""> {{ Helper::location($data['fasility']->fk_lkp_location) }} <hr class="m-2"/> </option> --}}
                                    <option value="" disabled selected>Pilih Lokasi</option>
                                        @foreach ($data['location'] as $l)
                                            <option value="{{ $l->id }}" @if($data['fasility']->fk_lkp_location == $l->id) selected @endif>{{ $l->lc_description }}</option>
                                        @endforeach
                                </select>
                                <span>
                                    <i class="flaticon-placeholder-1 icon-xl"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4 ">
                            <div class="input-icon">
                                <input type="hidden" name="lokasi" value="{{ $data['fasility']->id }}" />
                                <select name="fasility" class="form-control form-control-lg border-0 font-weight-bold" id="kt_select2_5" required  >
                                    {{-- <option value="{{ $data['id'] }}" selected>{{ $data['fasility']->eft_type_desc }}</option> --}}
                                    @foreach ($data['facility2'] as $l)
                                        <option value="{{ $l->id }}" @if($data['id'] == $l->id) selected @endif>{{ $l->eft_type_desc }}</option>
                                    @endforeach
                                    {{-- <option value=""> Pilih Jenis Kemudahan Sukan</option> --}}
                                </select>
                                <span>
                                    <i class="flaticon-search-1 icon-xl"></i>
                                </span>
                                <span class="bullet bullet-ver h-45px d-none d-sm-flex mr-2"></span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-icon">
                                <input type="text" class="form-control form-control-lg border-0 font-weight-bold" id="kt_daterangepicker_5" value="{{ Helper::date_format($data['date']) }}" name="tarikh" placeholder="Tarikh" autocomplete="off" required />
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                <span class="bullet bullet-ver h-45px d-none d-sm-flex mr-2"></span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-block btn-lg btn-primary font-weight-bold">CARIAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Subheader-->                        				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h2 class="text-dark font-weight-bolder mt-2 mb-2 mr-5">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h2>
                <p class="text-right mt-2 mb-2 mr-5"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset("assets/upload/main/{$data['fasility']->cover_img}") }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-5">
				    <div id="panorama" class=" d-flex justify-content-center" style="width: 100%; height: 300px; background-color: rgb(222, 222, 222);">
                        <input type="text" id="panoramaID" value="{{ $data['fasility']->virtual_img }}" hidden>
                        <img src="{{asset('/assets/media/logos/ppj.png')}}" max-width="100%" height="100%"/>
                    </div>
                </div>
                <div class="col-lg-7 desk-ver">
                    <div class="row" id="aniimated-thumbnials">
                        @php
                            $i = 0;
                        @endphp
                        @if(count($data['et_fac_type_img']) > 0 && count($data['et_fac_type_img']) > 5)
                            @foreach($data['et_fac_type_img'] as $l)
                                @if($i < 6) 
                                    <div class="col-lg-4 d-flex justify-content-center mb-2">
                                        <a style="width: 210px; height: 137.54px; background-color: rgb(222, 222, 222);" href="{{ asset("assets/upload/pic/{$data['id']}/{$l->img}") }}" data-img="{{ asset("assets/upload/pic/{$data['id']}/{$l->img}") }}">
                                            <div  class="d-flex justify-content-center" id="plhd_{{$i}}">
                                                <img id="plh_{{$i}}" style="max-width: 100%; max-height: 100%" src="{{asset('/assets/media/logos/ppj.png')}}"/>
                                            </div>
                                            <img  id="img_{{$i}}" class="gutter-b img-img" src="{{ asset("assets/upload/pic/{$data['id']}/{$l->img}") }}" style="width:100%; height: 137.54px;" alt="" style="background-color: rgb(222, 222, 222);">
                                        </a>
                                    </div>
                                {{-- @else
                                    <div class="col-lg-4" style="display:none;">
                                        <a href="https://images.unsplash.com/photo-1626926938421-90124a4b83fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=451&q=80"  data-sub-html=".caption7">
                                            <img class="gutter-b" src="https://images.unsplash.com/photo-1626926938421-90124a4b83fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=451&q=80" style="width:100%; height: 137.54px" alt="">
                                            <div class="caption7">
                                                <h6></h6>
                                            </div>
                                        </a>
                                    </div>--}}
                                @endif 
                                @php
                                    $i++
                                @endphp
                            @endforeach
                        @else
                            @for ($i = 0; $i < 6; $i++)
                                <div class="col-lg-4 d-flex justify-content-center mb-2">
                                    @if(isset($data['et_fac_type_img'][$i]))
                                        <a style="width: 210px; height: 137.54px; background-color: rgb(222, 222, 222);" href="{{ asset("assets/upload/pic/{$data['id']}/{$data['et_fac_type_img'][$i]->img}") }}" data-img="{{ asset("assets/upload/pic/{$data['id']}/{$data['et_fac_type_img'][$i]->img}") }}">
                                            <div  class="d-flex justify-content-center" id="plhd_{{$i}}">
                                                <img id="plh_{{$i}}" style="max-width: 50%; max-height: 100px" src="{{asset('/assets/media/logos/ppj.png')}}"/>
                                            </div>
                                            <img  id="img_{{$i}}" class="gutter-b img-img" src="{{ asset("assets/upload/pic/{$data['id']}/{$data['et_fac_type_img'][$i]->img}") }}" style="width:100%; height: 137.54px;" alt="" style="background-color: rgb(222, 222, 222);">
                                        </a>
                                    @else
                                        <div style="width: 210px; height: 137.54px; background-color: rgb(222, 222, 222);" class="d-flex justify-content-center align-items-center">
                                            <div class="d-flex justify-content-center align-items-center" id="plhd_{{$i}}">
                                                <img id="plh_{{$i}}" style="max-width: 50%; max-height: 100px" src="{{asset('/assets/media/logos/ppj.png')}}"/>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endfor
                        @endif
                        {{-- <div class="col-lg-4">
                            <a href="https://images.unsplash.com/photo-1626721105368-a69248e93b32?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1626721105368-a69248e93b32?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1521537634581-0dced2fee2ef?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1521537634581-0dced2fee2ef?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1564226803380-91139fdcb4d0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1564226803380-91139fdcb4d0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                        <a href="https://images.unsplash.com/photo-1617696618050-b0fef0c666af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1617696618050-b0fef0c666af?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" style="width:100%; height: 137.54px" alt="">
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="https://images.unsplash.com/photo-1613918431703-aa50889e3be9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" class="img-container text-white">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1613918431703-aa50889e3be9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" style="width:100%; height: 137.54px; " alt="">
                                <div class="centered" ><h5 class="font-weight-bolder">+ 1 Gambar</h5></div>
                            </a>
                        </div>
                        <div class="col-lg-4" style="display:none;">
                            <a href="https://images.unsplash.com/photo-1626926938421-90124a4b83fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=451&q=80"  data-sub-html=".caption7">
                                <img class="gutter-b" src="https://images.unsplash.com/photo-1626926938421-90124a4b83fa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=451&q=80" style="width:100%; height: 137.54px" alt="">
                                <div class="caption7">
                                    <h6></h6>
                                </div>
                            </a>
                        </div> --}}
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
                        <div class="card-header">
                            <div class="card-title">
                            <h4 class="card-label font-weight-bolder text-dark">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h4>
                            <span class="label label-lg font-weight-bold {{ Helper::status_color($data['fasility']->eft_status) }} label-inline">{{ Helper::status($data['fasility']->eft_status) }}</span>
                            </div>
                            <div class="card-toolbar">
                            <h7 class="card-label text-dark  mr-2">dari</h7>
                            <h3 class="card-label font-weight-boldest mr-2 text-danger">RM {{ Helper::get_harga($data['fasility']->fk_et_facility, $data['date']) }}</h3>
                            <h7 class="card-label text-dark mr-4">per jam</h7>
                                @if(Session::has('user')) 
                                    {{-- @if($data['id'] == 65 )
                                        <a href="{{ url('sport/tempahanForm', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a> --}}
                                    @if(!in_array($data['fasility']->eft_status, [1]))
                                        {{-- <div class="text-danger">{{ Helper::status($data['fasility']->eft_status )}}</div> --}}
                                    @else
                                        <a href="{{ url('sport/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" id="btnTeruskanTempahan" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a>
                                        <button hidden disabled href="{{ url('sport/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" id="btnTeruskanTempahan2" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</button>
                                    @endif
                                @else
                                    <a href="{{ url('auth') }}" class="btn btn-primary font-weight-bolder btn-sm float-right mb-1">Teruskan Tempahan</a>
                                @endif
                                    <a href="{{ url('sport') }}" class="btn btn-light-danger font-weight-bolder btn-sm float-right mb-1 ml-2">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">	
                    <div class="card card-custom gutter-b">
                        <div class="card-body pb-5">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="card-label font-weight-bolder text-dark mb-3 mt-4">Sorotan</h6>
                                    <p>{{ $data['fasility']->highlight }}</p>
                                    <hr>
                                    <h6 class="card-label font-weight-bolder text-dark mb-3">Kadar Harga</h6>
                                    <table class="table mt-2 text-center">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th></th>
                                                <th colspan="2">Isnin - Jumaat</th>
                                                <th colspan="2">Sabtu - Ahad</th>
                                            </tr>
                                            <tr>
                                                <th>Gelanggang</th>
                                                <th>Pagi</th>
                                                <th>Malam</th>
                                                <th>Pagi</th>
                                                <th>Malam</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 13px">
                                            @php
                                                $compare = "";
                                                $siang = "";
                                                $malam = "";
                                                $siang1 = "";
                                                $malam1 = "";
                                            @endphp
                                            @if ($data['post'] = true)
                                                @if(count($data['price']) < 4)
                                                    @foreach($data['price'] as $p)
                                                        @if ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1 && $p->efp_status == 1)
                                                            @php
                                                                $siang = $p->efp_unit_price;
                                                            @endphp
                                                        @elseif ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 1 && $p->efp_status == 1)
                                                            @php
                                                                $malam = $p->efp_unit_price;
                                                            @endphp
                                                        @elseif ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 2 && $p->efp_status == 1)
                                                            @php
                                                                $siang1 = $p->efp_unit_price;
                                                            @endphp
                                                        @elseif ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1 && $p->efp_status == 2)
                                                            @php
                                                                $malam1 = $p->efp_unit_price;
                                                            @endphp
                                                        @endif
                                                        @if($p->efd_name != $compare)
                                                            @php
                                                                $compare = $p->efd_name;
                                                                echo "<tr>";
                                                                echo "<td name='gelanggang'>$compare</td>";
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    <td>{{ !empty($siang) ? 'RM ' . $siang : '-' }}</td>
                                                    <td>{{ !empty($malam) ? 'RM ' . $malam : '-' }}</td>
                                                    <td>{{ !empty($siang1) ? 'RM ' . $siang1 : '-' }}</td>
                                                    <td>{{ !empty($malam1) ? 'RM ' . $malam1 : '-' }}</td>
                                                @else
                                                    @foreach($data['price'] as $p)
                                                        @if($p->efd_name != $compare)
                                                            @php
                                                                $compare = $p->efd_name;
                                                                echo "<tr>";
                                                                echo "<td name='gelanggang'>$compare</td>";
                                                            @endphp
                                                        @endif
                                                        {{-- @if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1 && $p->efp_status == 1) <td>{{$p->efp_unit_price}} </td>@endif
                                                        @if($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 1 && $p->efp_status == 1) <td>{{$p->efp_unit_price}} </td>@endif
                                                        @if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 2 && $p->efp_status == 1) <td>{{$p->efp_unit_price}} </td>@endif
                                                        @if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1 && $p->efp_status == 2) <td>{{$p->efp_unit_price}} </td>@endif --}}
                                                        @php
                                                            if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1 && $p->efp_status == 1){
                                                                $siang = $p->efp_unit_price;
                                                                echo "<td>RM $siang</td>";
                                                            } elseif ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 1 && $p->efp_status == 1){
                                                                $malam = $p->efp_unit_price;
                                                                echo "<td>RM $malam</td>";
                                                            } elseif ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 2 && $p->efp_status == 1){
                                                                $siang1 = $p->efp_unit_price;
                                                                echo "<td>RM $siang1</td>";
                                                            } elseif ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 2 && $p->efp_status == 1){
                                                                $malam1 = $p->efp_unit_price;
                                                                echo "<td>RM $malam1</td>";
                                                                // echo "</tr>"; // Close the row here
                                                            }
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            @endif
                                        </tbody>
                                        
                                        {{-- <tbody style="font-size: 13px">
                                            @php
                                                $compare = "";
                                                $siang = "";
                                                $malam = "";
                                                $siang1 = "";
                                                $malam1 = "";
                                            @endphp
                                            @if ( $data['post'] = true)
                                                @foreach($data['price'] as $p)
                                                @if($p->efd_name != $compare)
                                                    @php
                                                        $compare = $p->efd_name;
                                                        echo "<tr>";
                                                        echo "<td name='gelanggang'>$compare</td>";
                                                        if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1){
                                                            $siang = $p->efp_unit_price;
                                                            echo "<td>RM $siang</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 1){
                                                            $malam = $p->efp_unit_price;
                                                            echo "<td>RM $malam</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 2){
                                                            $siang1 = $p->efp_unit_price;
                                                            echo "<td>RM $siang1</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 2){
                                                            $malam1 = $p->efp_unit_price;
                                                            echo "<td>RM $malam1</td>";
                                                        }
                                                    @endphp
                                                @else
                                                    @php
                                                        if($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 1){
                                                            $siang = $p->efp_unit_price;
                                                            echo "<td>RM $siang</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 1){
                                                            $malam = $p->efp_unit_price;
                                                            echo "<td>RM $malam</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 1 && $p->efp_day_cat == 2){
                                                            $siang1 = $p->efp_unit_price;
                                                            echo "<td>RM $siang1</td>";
                                                        }else if ($p->fk_lkp_slot_cat == 2 && $p->efp_day_cat == 2){
                                                            $malam1 = $p->efp_unit_price;
                                                            echo "<td>RM $malam1</td>";
                                                            echo "</tr>";
                                                        }
                                                    @endphp
                                                @endif
                                                @endforeach
                                            @endif
                                        </tbody> --}}
                                    </table>
                                    <hr>
                                </div>
                                <div class="col-lg-12">
                                    <h6 class="card-label font-weight-bolder text-dark mb-3">Slot Masa</h6>
                                    <table class="table mt-2 text-center">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Pagi / Petang </th>
                                                <th>Malam</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 13px">
                                            <tr>
                                                <td>09:00am - 07:00pm</td>
                                                <td>07:00pm - 12:00am</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                                <div class="col-lg-12">
                                    <h6 class="card-label font-weight-bolder text-dark mb-3">Fasiliti</h6>
                                        <table class="table table-borderless" style="font-size: 13px">
                                            <tr>
                                                <td><i class="fas fa-wifi text-dark mr-5"></i>Wifi</td>
                                                <td><i class="fas fa-toilet text-dark mr-5"></i>Tandas</td>
                                                <td><i class="fas fa-utensils text-dark mr-5"></i>Kedai Makan</td>
                                                <td><i class="fas fa-mosque text-dark mr-5"></i>Surau</td>
                                                <td><i class="fas fa-parking text-dark mr-5"></i>Parkir Kereta</td>
                                            </tr> 
                                        </table>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 pb-0">
                            <h6 class="card-title align-items-start flex-column mb-2">
                                <span class="card-label font-weight-bolder text-dark">Kekosongan Slot & Takwim</span>
                                <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['date'] }}</span>
                                
                            </h6>                          
                        </div>
                        <div class="card-header border-0 pt-5 pb-0">
                            <ul class="nav nav-dark nav-bold nav-pills">
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($data['details'] as $d)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $i == 0 ? 'active' : '' }}" data-toggle="tab" href="#tab{{$i}}">{{$d->efd_name}}</a>
                                    </li>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_5">Takwim</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                @php
                                    $j = 0;
                                @endphp
                                @foreach($data['details'] as $d)
                                    <div class="tab-pane fade show {{ $j == 0 ? 'active' : '' }}" id="tab{{$j}}" role="tabpanel" aria-labelledby="tab{{$j}}">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="option bg-info">
                                                            <span class="option-label">
                                                                <span class="option-head">
                                                                    <span class="option-title text-center text-light">Sesi Pagi / Petang</span>
                                                                </span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    @foreach($data['slot'] as $s)
                                                        @if($s->efd_name == $d->efd_name)
                                                            @if($s->sesi == 1)
                                                            <div class="col-lg-12">
                                                                <label class="option">
                                                                    <span class="option-label">
                                                                        <span class="option-head">
                                                                            <span class="option-title"><i class="fa fas fa-dot-circle text-success mr-5"></i>{{ $s->est_slot_time }}</span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="option bg-info">
                                                            <span class="option-label">
                                                                <span class="option-head">
                                                                    <span class="option-title text-center text-light">Sesi Malam</span>
                                                                </span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    @foreach($data['slot'] as $s)
                                                        @if($s->efd_name == $d->efd_name)
                                                            @if($s->sesi == 2)
                                                            <div class="col-lg-12">
                                                                <label class="option">
                                                                    <span class="option-label">
                                                                        <span class="option-head">
                                                                            <span class="option-title"><i class="fa fas fa-dot-circle text-success mr-5"></i>{{ $s->est_slot_time }}</span>
                                                                        </span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @php
                                    $j++;
                                @endphp
                                @endforeach
                                <div class="tab-pane fade" id="kt_tab_pane_7_5" role="tabpanel" aria-labelledby="kt_tab_pane_7_5">
                                    <div id="kt_calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-6">
                            <a href='https://www.waze.com/ul?ll={{ $data['fasility']->latitude }}%2C{{ $data['fasility']->longitude }}&navigate=yes&zoom=17' class="btn btn-primary btn-block btn-lg font-weight-bolder mb-4"><i class="fab fa-waze"></i> Waze</a>
                        </div>
                        <div class="col-lg-6">
                            <a href='https://www.google.com/maps?layer=c&cbll={{ $data['fasility']->latitude }},{{ $data['fasility']->longitude }}' class="btn btn-dark btn-block btn-lg font-weight-bolder mb-4"><i class="fas fa-map-marker-alt"></i> Google Maps</a>
                        </div>
                    </div>
                    <!--begin::List Widget 21-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 pb-0">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-dark">Cadangan Lokasi Lain</span>
                                <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['date'] }}</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            @foreach($data['others'] as $o)
                            <!--begin::Item-->
                            <div class="d-flex flex-wrap align-items-center mb-10">
                                <!--begin::Symbol-->
                                <div class="symbol symbol-90 symbol-2by3 flex-shrink-0 mr-4">
                                    <div class="symbol-label" style="background-image: url('{{ asset('assets/media/panaroma_2.jpg') }}')"></div>
                                </div>
                                <!--end::Symbol-->
                                <!--begin::Title-->
                                <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                    <a href="{{ url('sport/details',Crypt::encrypt($o->id)) }}" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">{{ $o->eft_type_desc }}</a>
                                    <span class="text-muted font-weight-bold font-size-sm my-1">{{ Helper::location($o->fk_lkp_location) }}</span>
                                    <p><span class="label font-weight-bolder {{ Helper::status_color($o->eft_status) }} label-inline">{{ Helper::status($o->eft_status) }}</span></p>
                                    <p><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star text-warning icon-nm mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i><i class="flaticon-star icon-nm text-warning mr-2"></i></p>
                                </div>
                                <!--end::Title-->
                            </div>
                            <!--end::Item-->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('sport.public.js.details')
@endsection



@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            {{-- <div class="card card-custom gutter-b">
                <div class="card-body pt-2">
                    <div class="row">
                            <h3>TEMPAHAN YANG TIDAK DIAMBIL TINDAKAN PENGESAHAN AKAN DISAHKAN DALAM TEMPOH 3 HARI DARI TARIKH PERMOHONAN</h3>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-lg-5">
                    <div class="card card-custom gutter-b">
                        <div class="card-header border-0 pt-5 bg-dark">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-white mb-1">Kutipan Bayaran</span>
                            </h3>
                        </div>
                        <form action="" method="post">
                            @csrf
                        <div class="card-body pt-5">
                            @if(in_array($data['main']->fk_lkp_status, [13]))
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label font-weight-bold pt-0">Pengesahan</label>
                                    <div class="col-lg-8">
                                        <select @if(!in_array($data['main']->fk_lkp_status, [13])) disabled @endif name="pengesahan"  class="form-control" required>
                                            <option value="" selected disabled>Sila Pilih </option>
                                            @if ($data['main']->fk_lkp_status == 13)
                                                <option value="2">Lulus</option>
                                                <option value="29">Tolak</option>
                                                <option value="10">Batal</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Jenis Bayaran</label>
                                <div class="col-lg-8">
                                    <input hidden type="text" name="lkp_status" id="lkp_status" value="{{ $data['main']->fk_lkp_status }}">
                                    <input hidden type="text" name="hall_val" id="hall_val" value="{{ $data['main']->bmb_total_book_hall }}">
                                    <input hidden type="text" name="equipment_val" id="equipment_val" value="{{ $data['main']->bmb_total_equipment }}">
                                    <input hidden type="text" name="deposit" id="deposit" value="{{ $data['main']->bmb_deposit_rm ?? 0.00 }}">
                                    <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4])) disabled @endif name="jenis_bayaran" id="jenis_bayaran" class="form-control" required>
                                        <option value="" selected disabled>@if(in_array($data['main']->fk_lkp_status, [2, 3, 4])) Sila Pilih @endif</option>
                                        @if ($data['main']->fk_lkp_status == 13 || $data['main']->fk_lkp_status == 2)
                                            @if ($data['et_hall_book'][0]->fk_et_function == 10)
                                                <option value="1">DEPOSIT</option>
                                            @endif
                                            <option value="2">PENUH</option>
                                        @elseif ($data['main']->fk_lkp_status == 3 || $data['main']->fk_lkp_status == 4)
                                            <option value="2">PENUH</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Jumlah Tempahan(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control " id="jumlahTempahanId" name="jumlahTempahan" placeholder="Jumlah Tempahan(RM)" value="{{ $data['main']->bmb_subtotal }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Pendahuluan (RM)</label>
                                <div class="col-lg-8">
                                    <input disabled ="text" class="form-control" name="jumlahPendahuluan" id="pendahuluan" placeholder="Jumlah Pendahuluan Tempahan(RM)" value="{{ Helper::moneyhelper($data['main']->bmb_total_book_hall/2) }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Deposit(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="deposit" placeholder="Deposit(RM)" value="{{ Helper::moneyhelper($data['main']->bmb_deposit_rm) }}"/>
                                </div>
                            </div>
                            @php
                                $jumlahdahdibayar = 0;
                            @endphp
                            @if (count($data['kaunter']) > 0)
                                @foreach ($data['kaunter'] as $k)
                                    @php
                                        $jumlahdahdibayar += $k->amount_received;
                                    @endphp
                                @endforeach
                            @endif
                            @if (count($data['online']) > 0)
                                @foreach ($data['online'] as $k)
                                    @php
                                        $jumlahdahdibayar += $k->amount_paid;
                                    @endphp
                                @endforeach
                            @endif
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Telah Dibayar(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="jumlahtelahdibayar" placeholder="Jumlah Telah Dibayar(RM)" value="@if ($data['main']->fk_lkp_status == 2)0.00 @elseif($data['main']->fk_lkp_status == 4 || $data['main']->fk_lkp_status == 5) {{ Helper::moneyhelper($jumlahdahdibayar) }} @endif"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Perlu Dibayar(RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="jumlahPerluDibayar" id="jumlahperludibayar" placeholder="Jumlah Perlu Dibayar(RM)" value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">No. Sebutharga</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control" name="sebutharga" placeholder="No Sebutharga" value="{{ $data['bh_quotation']->bq_quotation_no }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label font-weight-bold pt-0">Cara Bayaran</label>
                                <div class="col-lg-8">
                                    <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4])) disabled @endif required name="payment_mode" id="carabayaran" class="form-control" required>
                                        <option value="" selected disabled>Sila Pilih</option>
                                        @foreach ($data['payment_mode'] as $pm)
                                            <option value="{{ $pm->id }}">{{ $pm->lpm_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- <div id="nocek" class="form-group row">
                                <label class="col-lg-3 col-form-label  font-weight-bold">No Cek</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="nocek" placeholder="" value=""/>
                                </div>
                            </div>
                            <div id="namabank" class="form-group row">
                                <label class="col-lg-3 col-form-label  font-weight-bold">Nama Bank</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="namabank" placeholder="" value=""/>
                                </div>
                            </div> --}}
                            <div id="lopo" class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Nombor LO/PO</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="lopo" placeholder="" value=""/>
                                </div>
                            </div>
                            <div class="form-group row" id="norujukanbayaran">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">No Rujukan Bayaran</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="rujukanBayaran" placeholder="No Rujukan Bayaran" value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label  font-weight-bold pt-0">Jumlah Bayaran (RM)</label>
                                <div class="col-lg-8">
                                    <input disabled type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value=""/>
                                    <input hidden type="text" class="form-control jumlahBayaran" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!--begin::Maklumat Pemohon-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column mb-5">
                                <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                            </h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2">
                            <div class="row">
                                {{-- <div class="col-lg-6"> --}}
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                            <td>{{ $data['user']->fullname }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                            <td>{{ $data['main']->bmb_booking_no }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                            <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                        </tr>
                                    </table>
                                {{-- </div>
                                <div class="col-lg-6"> --}}
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                            <td>{{ $data['user']->email }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                            <td>{{ $data['main']->bmb_subtotal }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                            <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                            <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                        </tr>
                                    </table>
                                {{-- </div> --}}
                            </div>
                            <div class="row">
                                <div class="col-12 d-flex justify-content-end">
                                    {{-- <a href="{{ url('hall/borangbpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Borang BPSK</a> --}}
                                    <a href="{{ url('sport/janasebutharga', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary">Jana Sebut Harga</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Maklumat Pemohon-->
                    
                    <!--start::Maklumat Acara-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Fasiliti / Lokasi</th>
                                            <th>Tarikh Mula</th>
                                            <th>Tarikh Akhir</th>
                                            <th>Tujuan Tempahan</th>
                                            <th>Acara</th>
                                            <th>Maklumat Acara</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $i = 0;
                                        $j = 1;
                                    @endphp
                                    <tbody>
                                        @if(count($data['et_booking_facility_detail']) > 0)
                                            @foreach($data['et_booking_facility_detail'] as $p)
                                            <tr>
                                                <td>{{ Helper::tempahanSportDetail(Helper::get_et_booking_facility($p->fk_et_booking_facility)->fk_et_facility_detail) }}, {{ $p->ebfd_venue }}</td>
                                                <td>{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                <td>{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                                <td>{{ $p->ebfd_event_name }}</td>
                                                <td>{{ $p->ebfd_event_desc }}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!--start::Senarai Resit-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>Jenis Bayaran</th>
                                            <th>No. Resit</th>
                                            @if(count($data['online']) > 0)
                                            <th>No. SAP</th>
                                            @endif
                                            <th>Tarikh Transaksi</th>
                                            <th>Jumlah Bayaran (RM)</th>
                                            <th>Resit</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $j = 1;
                                        $paid = number_format(0, 2);
                                    @endphp
                                    <tbody>
                                        @if(count($data['kaunter']) > 0)
                                            @foreach($data['kaunter'] as $p)
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                <td>{{ $p->bp_receipt_number }}</td>
                                                <td>{{ Helper::date_format($p->created_at) }}</td>
                                                <td class="text-right">{{ $p->bp_paid_amount }}</td>
                                                <td style="width: 15%;">
                                                    <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                    {{-- <a href="#" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a> --}}
                                                </td>
                                            </tr>
                                            @php
                                                $paid = $paid + $p->bp_paid_amount
                                            @endphp
                                            @endforeach
                                        @elseif(count($data['online']) > 0)
                                            @foreach($data['online'] as $p)
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                <td>{{ $p->fpx_trans_id }}</td>
                                                <td>{{ $data['main']->sap_no ?? "" }}</td>
                                                <td>{{ $p->fpx_date }}</td>
                                                <td>{{ $p->amount_paid }}</td>
                                                <td><?php ?>6625.00<?php ?></td>
                                                <td style="width: 15%;">
                                                    <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                    <a href="https://spt2.ppj.gov.my/hall/bayaran/eyJpdiI6InN0dEFyNXI2T3p6SnJKK01jdWgrRmc9PSIsInZhbHVlIjoiRkFRMmJHSUZQc1BMNGNkejFWQUMydz09IiwibWFjIjoiYzJlMjgzOTJhOWNjMzg5MzdiZTE1YzQwMjAyM2NmODAwNDllNjNhMTg0NmFjZjdkNDY2NzgyZGIxMGUxMmJkZiIsInRhZyI6IiJ9" class="btn btn-sm btn-primary">Bayar Baki</a>
                                                </td>
                                            </tr>
                                            @php
                                                $paid = $paid + $p->amount_paid
                                            @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">Tiada Resit Bayaran</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                <!--end::Senarai Resit-->
                <!--start::Senarai Pecahan Slot-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Senarai Pecahan Slot</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Nama Dewan</th>
                                        <th>Lokasi</th>
                                        <th>Tarikh Mula</th>
                                        <th>Tarikh Akhir</th>
                                        <th>Jenis Kegunaan</th>
                                        <th>Sesi</th>
                                        <th>Harga (RM)</th>
                                        {{-- <th>Kod GST</th>
                                        <th>GST (0%)</th> --}}
                                        <th>Jumlah(RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                    $jumlahslot = 0;
                                    $jumlahkeseluruhanslot = 0;
                                @endphp
                                <tbody>
                                    @foreach ($data['bookingData'] as $eht)
                                        <tr>
                                            <td>{{ $j++ }}</td>
                                            <td>{{ Helper::typeSportFacility(Helper::get_harga_data($eht->fk_et_facility_price)->fk_et_facility) }}</td>
                                            <td>{{ Helper::location($eht->fk_lkp_location) }}</td>
                                            <td>{{ Helper::date_format($eht->start) }}</td>
                                            <td>{{ Helper::date_format($eht->end) }}</td>
                                            <td>{{ Helper::typeFunction($eht->fk_et_function) }}</td>
                                            <td>{{ Helper::get_slot_masa($eht->fk_et_slot_time) }}</td>
                                            <td class="text-right">{{ $eht->eht_total }}</td>
                                            <td class="text-right">{{ $eht->eht_subtotal }}</td>
                                        </tr>
                                        @php
                                            $jumlahslot += $eht->eht_total;
                                            $jumlahkeseluruhanslot += $eht->eht_subtotal;
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td colspan="7" class="text-right font-weight-bolder">Jumlah (RM)</td>
                                        <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahslot) }}</td>
                                        <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahkeseluruhanslot) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Kadar Sewaan Dewan-->
                <!--start::Kadar Sewaan Peralatan-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Tarikh Kegunaan</th>
                                        <th>Penggunaan</th>
                                        <th>Item</th>
                                        <th>Kuantiti</th>
                                        <th>Kadar Seunit (RM)</th>
                                        <th>Jumlah (RM)</th>
                                        {{-- <th>Kod GST</th>
                                        <th>GST (6%)</th> --}}
                                        <th>Jumlah Sewaan (RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                    $jumlahperalatan = 0;
                                    $jumlahsewaanperalatan = 0;
                                @endphp
                                <tbody>
                                    @foreach ($data['et_equipment_book'] as $eeb)
                                        <tr>
                                            <td>{{ Helper::date_format($eeb->eeb_booking_date) }}</td>
                                            <td>{{ Helper::typeFunction($eeb->fk_et_function) }}</td>
                                            <td>{{ Helper::get_equipment($eeb->fk_et_equipment) }}</td>
                                            <td>{{ $eeb->eeb_quantity }}</td>
                                            <td class="text-right">{{ $eeb->eeb_unit_price }}</td>
                                            <td class="text-right">{{ $eeb->eeb_total }}</td>
                                            <td class="text-right">{{ $eeb->eeb_subtotal }}</td>
                                        </tr>
                                    @php
                                        $jumlahperalatan += $eeb->eeb_total;
                                        $jumlahsewaanperalatan += $eeb->eeb_subtotal;
                                    @endphp
                                    @endforeach
                                        <tr>
                                            <td colspan="5" class="text-right font-weight-bolder">Jumlah (RM)</td>
                                            <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahperalatan) }}</td>
                                            <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahsewaanperalatan) }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Kadar Sewaan Peralatan-->
                <!--start::Jumlah Keseluruhan-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Rumusan</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                        <td class="text-center">E</td>
                                        <td class="text-right">0.00</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                        <td class="text-center">I</td>
                                        <td class="text-right">0.00</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                        <td></td>
                                        <td class="text-right">{{ number_format($data['main']->bmb_subtotal,2) }}</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                        <td></td>
                                        <td class="text-right">
                                            <?php
                                            if(isset($data['main']->bmb_rounding)) {
                                                echo $data['main']->bmb_rounding;
                                            } else {
                                                echo number_format(0, 2);
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                        <td></td>
                                        <td class="text-right">{{ number_format($paid, 2) }}</td>
                                    </tr>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                    <!--end::Jumlah Keseluruhan-->
                    <div class="float-right">
                        @if (in_array($data['main']->fk_lkp_status, [2, 4, 13]))
                            <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</button>
                        @endif
                        {{-- <a href="" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</a> --}}
                        {{-- <a href="{{ url('hall/dashboard/maklumatkaunter', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</a> --}}
                        {{-- <a href="{{ url('hall/bayaran', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Bayar Online</a> --}}
                        @if ($data['main']->fk_lkp_status != 5)
                            <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>
                        @endif
                    </div>
                </form>
                </div>
            </div>
            <div class="modal" id="customModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            Adakah Anda Mempunyai Diskaun Khas atau baucer?
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="modalTidak" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                            <a href="{{ url('sport/booking/pelarasan', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary">Ya</a>
                            {{-- <button type="button" id="modalYa" class="btn btn-primary" data-dismiss="modal">Ya</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('sport.public.js.pengesahan')
@endsection				



@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. BP</th>
                                    <td>0010030631</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Bill SAP</th>
                                    <td>600000018718</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Jenis Bayaran</th>
                                    <th>No. Resit</th>
                                    <th>No. Resit SAP</th>
                                    <th>Tarikh Transaksi</th>
                                    <th>Jumlah Bayaran (RM)</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $paid = 0.00;
                            @endphp
                            <tbody>
                                <tr>
                                    <td style="width: 2%; text-align: center">1</td>
                                    <td>BAYARAN DEPOSIT</td>
                                    <td>SPS230622782870001</td>
                                    <td>8800000073</td>
                                    <td>2023-06-22 06:15:53</td>
                                    <td>1000.00</td>
                                    <td style="width: 15%;">
                                        <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 2%; text-align: center">2</td>
                                    <td>BAYARAN KESELURUHAN</td>
                                    <td>SPS230622782870001</td>
                                    <td>8500109914</td>
                                    <td>2023-06-22 06:15:53</td>
                                    <td>600.00</td>
                                    <td style="width: 15%;">
                                        <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Lokasi</th>
                                    <th>Harga Sewaan (RM)</th>
                                    <th>Kod GST</th>
                                    <th>GST (RM)</th>
                                    <th>Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $total = 0.00;
                                @endphp
                                <tr>
                                    <td style="width: 2%; text-align: center">1</td>
                                    <td>Dewan Utama</td>
                                    <td>9:00 AM - 10:00 AM</td>
                                    <td>2023-06-22</td>
                                    <td>Presint 9</td>
                                    <td style="text-align: end">200.00</td>
                                    <td style="text-align: end">ZRL</td>
                                    <td style="text-align: end">0.00</td>
                                    <td style="text-align: end">200.00</td>
                                </tr>
                                <tr>
                                    <td style="width: 2%; text-align: center">2</td>
                                    <td>Dewan Utama</td>
                                    <td>10:00 AM - 11:00 AM</td>
                                    <td>2023-06-22</td>
                                    <td>Presint 9</td>
                                    <td style="text-align: end">200.00</td>
                                    <td style="text-align: end">ZRL</td>
                                    <td style="text-align: end">0.00</td>
                                    <td style="text-align: end">200.00</td>
                                </tr>
                                <tr>
                                    <td style="width: 2%; text-align: center">3</td>
                                    <td>Dewan Utama</td>
                                    <td>11:00 AM - 12:00 PM</td>
                                    <td>2023-06-22</td>
                                    <td>Presint 9</td>
                                    <td style="text-align: end">200.00</td>
                                    <td style="text-align: end">ZRL</td>
                                    <td style="text-align: end">0.00</td>
                                    <td style="text-align: end">200.00</td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">600.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">E</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">I</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Deposit (RM)</th>
                                <td></td>
                                <td class="text-right">1000.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                <td></td>
                                <td class="text-right">600.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_rounding }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar (RM)</th>
                                <td></td>
                                <td class="text-right">1600.00</td>
                            </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            {{-- <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2">Kembali</a> --}}
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

					



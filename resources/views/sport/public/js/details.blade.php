<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>
<script>
    var events = "{{ url('takwim/sport', $data['fasility']->id) }}";
    console.log(events);
    "use strict";

    var KTCalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            var calendarEl = document.getElementById('kt_calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                themeSystem: 'bootstrap',

                isRTL: KTUtil.isRTL(),

                header: {
                    left: 'prev,next ',
                    center: 'title',
                    right: 'today'
                },

                height: 800,
                contentHeight: 780,
                aspectRatio: 3, 

                nowIndicator: true,
                now: TODAY + 'T09:25:00', // just for demo

                views: {
                    dayGridMonth: { buttonText: 'month' }
                },

                defaultView: 'dayGridMonth',
                defaultDate: TODAY,
                locale: 'ms-my',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: events,

                eventRender: function(info) {
                    var element = $(info.el);

                    if (info.event.extendedProps && info.event.extendedProps.description) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', info.event.extendedProps.description);
                            element.data('placement', 'top');
                            KTApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        }
                    }
                }
            });

            calendar.render();
        }
    };
}();

jQuery(document).ready(function() {
    KTCalendarBasic.init();
});
</script>
<script>
$(document).ready(function() {
    $('#carianSubmit').on('submit', function() {
        console.log('test')
        $('#kt_select2_5').removeAttr('disabled');
    })
    // Get the file path from the input field
    var img = $('#panoramaID').val();
    var panoramaPath = "{{ asset('assets/upload/virtual/360_Image_PPj/Panorama') }}/" + img;
    var placeholderImage = "{{ asset('/assets/media/logos/ppj.png') }}";

    // Function to check if the file exists
    function checkFileExists(filePath) {
        return $.ajax({
            url: filePath,
            type: 'HEAD',
            success: function() {
                return true;
            },
            error: function() {
                return false;
            }
        });
    }

    // Check if the file exists and initialize Pannellum viewer if it does
    checkFileExists(panoramaPath).done(function() {
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": panoramaPath,
            "preview": panoramaPath,
            "autoLoad": true,
            "autoRotate": 2,
            "autoRotateInactivityDelay": 2,
            "title": "{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}",
        });
        // console.log('File exists and Pannellum viewer initialized.');
    }).fail(function() {
        // console.log('File not found. Pannellum viewer not initialized.');
    });

    // Check if normal image files exist and toggle placeholder/actual images
    $('[data-img]').each(function(index) {
        var link = $(this);
        var imagePath = link.data('img');
        var placeholderImg = $('#plh_' + index);
        var placeholderDiv = $('#plhd_' + index);
        var actualImg = $('#img_' + index);

        checkFileExists(imagePath).done(function() {
            actualImg.show();
            placeholderImg.hide();
            placeholderDiv.hide(); 
        }).fail(function() {
            actualImg.hide();
            placeholderImg.show();
            placeholderDiv.show(); 
        });
    });

    // var img = $('#panoramaID').val();
    // var panoramaPath = "{{ asset('assets/upload/virtual/360_Image_PPj/Panorama') }}/" + img;

    // function checkFileExists(filePath) {
    //     console.log(filePath);
    //     fetch(filePath, { method: 'HEAD' })
    //     .then(response => {
    //         if (response.ok) {
    //             console.log('File exists.');
    //         } else {
    //             console.log('File not found.');
    //         }
    //     })
    //     .catch(error => {
    //       console.error('Error:', error);
    //     });
    // }
    // checkFileExists(img);

    // if(img){
    //     pannellum.viewer('panorama', {
    //         "type": "equirectangular",
    //         "panorama": "{{ asset('assets/upload/virtual/360_Image_PPj/Panorama') }}/" + img,
    //         "preview": "{{ asset('assets/upload/virtual/360_Image_PPj/Panorama') }}/" + img,
    //         "autoLoad": true,
    //         "autoRotate": 2,
    //         "autoRotateInactivityDelay": 2,
    //         "title": "{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}",
    //     });
    // } else {
    //     pannellum.viewer('panorama', {
    //         "type": "equirectangular",
    //         "autoLoad": true,
    //         "autoRotate": 2,
    //         "autoRotateInactivityDelay": 2,
    //         "title": "{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}",
    //     });
    // }
});
</script>
<script>
    $(function() {
    var maxDate = moment().add(1, 'months'); // Calculate the maximum date

    $('#kt_daterangepicker_5').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        // maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
    $('#kt_select2_4').on('change', function(){
        var location_id = $(this).val();
        if (location_id == ''){
            $('#kt_select2_5').prop('disabled', true);
        }
        else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('sport/ajax') }}",
                type: "POST",
                data: {'location_id' : location_id},
                dataType: 'json',
                success: function(data){
                    if (data.flash_message) {
                        $('#kt_select2_5').prop('disabled', true);
                        $('#btnTeruskanTempahan').prop('hidden', true);
                        $('#btnTeruskanTempahan2').prop('hidden', false);
                        Swal.fire(
                            'Harap Maaf!',
                            data.flash_message,
                            'error'
                        )
                    } else {
                        $('#btnTeruskanTempahan').prop('hidden', false);
                        $('#btnTeruskanTempahan2').prop('hidden', true);
                        $('#kt_select2_5').html(data);
                    }
                }
            });
        }
    });
</script>
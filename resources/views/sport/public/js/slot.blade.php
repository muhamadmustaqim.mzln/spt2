<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    // var table = $('table').DataTable({
    //     "bPaginate": false,
    //     "bLengthChange": false,
    //     "bFilter": true,
    //     "bInfo": false,
    //     language: {
    //         "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
    //         "zeroRecords": "Harap maaf, tiada rekod ditemui",
    //         "info": "Halaman _PAGE_ dari _PAGES_",
    //         "infoEmpty": "Tiada rekod dalam sistem",
    //         "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
    //         "sSearch": "Carian:"
    //     },
    //     rowGroup: {
    //         dataSrc: 1,
    //     },
    // } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function(){
        var groupColumn = 1;
        var table = $('#kt_datatable_2').DataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            },
            // "columnDefs": [
            //     { "visible": false, "targets": groupColumn }
            // ],
            // "drawCallback": function ( settings ) {
            //     var api = this.api();
            //     var rows = api.rows( {page:'current'} ).nodes();
            //     var last=null;
    
            //     api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
            //         if ( last !== group ) {
            //             $(rows).eq( i ).before(
            //                 '<tr class="group"><td colspan="8">'+group+'</td></tr>'
            //             );
    
            //             last = group;
            //         }
            //     });
            // }
        } );
        $('#JenisAcaraTempDewan').on('change', function() {
            var selectedText = $(this).find('option:selected').text();
            var selectedValue = $(this).val();
            if (selectedText.trim() === 'Majlis Perkahwinan') {
                $('#pakejPerkahwinan').prop('hidden', false);
                $('#pakejPerkahwinanSelect').attr('required', true);
            } else {
                $('#pakejPerkahwinan').prop('hidden', true);
                $('#pakejPerkahwinanSelect').prop('required', false).removeAttr('required');
            }
        })
        $("#form").submit(function( event ) {
            var $form = $(this);
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }
        });
    });
    
    // var maxAllowed = 2;
    var maxAllowed = $('#maxAllowed').val();
    var max_court = $('#max_court').val();
    var max_per_court = $('#max_per_court').val();
    var max_per_day = $('#max_per_day').val();
    var maxAllowedPerFacility = 2;
    var facilityArr = [];

    function countUniqueValues(arr) {
        const uniqueValues = new Set(arr);
        return uniqueValues.size;
    }

    $('.chk').on('change', function () {
        var checkedCount = $('.chk:checked').length;
        var fiddValue = $(this).closest('td').find('.chk').attr('value').split(',')[0];
        var maxPerCourt_count = 0;

        // If the checkbox is checked
        if ($(this).prop('checked')) {
            // Maksimum Slot
            if (facilityArr.length < maxAllowed) {
                // Maksimum Gelanggang
                facilityArr.push(fiddValue);
                
                var currentUniqueCourt = countUniqueValues(facilityArr);
                console.log('fiddValue: '+ fiddValue, 'facilityArr: '+facilityArr, 'max_court: '+max_court, 'currentUniqueCourt: '+currentUniqueCourt);

                if (currentUniqueCourt > max_court){
                    var index = facilityArr.indexOf(fiddValue);
                    facilityArr.pop();

                    Swal.fire({
                        title: 'Tempahan telah mencapai had.',
                        text: 'Anda telah mencapai had '+ max_court +' gelanggang yang dibenarkan.',
                        icon: 'warning',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $(this).prop('checked', false);
                        }
                    });
                } else {
                    for (let i = 0; i < facilityArr.length; i++) {
                        if (facilityArr[i] == fiddValue){
                            maxPerCourt_count++;
                        }
                    }
                    console.log('maxPerCourt_count' + maxPerCourt_count);
                    if (maxPerCourt_count > max_per_court){
                        var index = facilityArr.indexOf(fiddValue);
                        facilityArr.pop();
                        
                        Swal.fire({
                            title: 'Tempahan telah mencapai had.',
                            text: 'Anda telah mencapai had ('+ max_per_court +') per gelanggang  yang dibenarkan.',
                            icon: 'warning',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $(this).prop('checked', false);
                            }
                        });
                    }
                }

                // if (facilityArr.filter(function(value) { return value === fiddValue; }).length >= 2) {
                //     $('.chk.' + fiddValue + ':not(:checked)').prop('disabled', true);
                //     $('.chk.' + fiddValue + ':not(:checked)').closest('tr').find('td').not(':first-child').each(function() {
                //         $(this).wrapInner('<del></del>');
                //     });
                // }
                // if (facilityArr.length >= 2 && [...new Set(facilityArr)].length === 2) {
                //     $('.chk').each(function() {
                //         var chkFiddValue = $(this).closest('td').find('.chk').attr('value').split(',')[0];
                //         if (!facilityArr.includes(chkFiddValue)) {
                //             $(this).prop('disabled', true);
                //             $(this).closest('tr').find('td').not(':first-child').each(function() {
                //                 if (!$(this).parent().hasClass('initially-deleted')) {
                //                     $(this).wrapInner('<del></del>');
                //                 }
                //             });
                //         }
                //     });
                // }

                // Maksimum Gelanggang
                // facilityArr.push(fiddValue);
                // if (facilityArr.filter(function(value) { return value === fiddValue; }).length >= 2) {
                //     $('.chk.' + fiddValue + ':not(:checked)').prop('disabled', true);
                //     $('.chk.' + fiddValue + ':not(:checked)').closest('tr').find('td').not(':first-child').each(function() {
                //         $(this).wrapInner('<del></del>');
                //     });
                // }
                // if (facilityArr.length >= 2 && [...new Set(facilityArr)].length === 2) {
                //     $('.chk').each(function() {
                //         var chkFiddValue = $(this).closest('td').find('.chk').attr('value').split(',')[0];
                //         if (!facilityArr.includes(chkFiddValue)) {
                //             $(this).prop('disabled', true);
                //             $(this).closest('tr').find('td').not(':first-child').each(function() {
                //                 if (!$(this).parent().hasClass('initially-deleted')) {
                //                     $(this).wrapInner('<del></del>');
                //                 }
                //             });
                //         }
                //     });
                // }

                // var within_maxCourt = true;
                // if (facilityArr.length >= 3){
                //     console.log('test: ' + facilityArr.includes(fiddValue));
                //     within_maxCourt = facilityArr.includes(fiddValue);
                //     if (!facilityArr.includes(fiddValue)) {
                //         Swal.fire({
                //             title: 'Tempahan telah mencapai had.',
                //             text: 'Anda telah melebihi pilihan ('+max_court+') gelanggang  dibenarkan.',
                //             icon: 'warning',
                //             confirmButtonText: 'OK'
                //         }).then((result) => {
                //             if (result.isConfirmed) {
                //                 $(this).prop('checked', false);
                //             }
                //         });
                //     }
                // }
                // for (var i = 0; i < facilityArr.length; i++) {
                //     if (facilityArr[i] === fiddValue) {
                //         count_maxPerCourt++;
                //     }
                // }
                
                // if (count_maxPerCourt > max_per_court){
                //     count_maxPerCourt = 0;
                //     Swal.fire({
                //         title: 'Tempahan telah mencapai had.',
                //         text: 'Anda telah mencapai had tempahan bagi gelanggang ini.',
                //         icon: 'warning',
                //         confirmButtonText: 'OK'
                //     }).then((result) => {
                //         if (result.isConfirmed) {
                //             $(this).prop('checked', false);
                //         }
                //     });
                // } else {
                //     count_maxPerCourt = 0;
                //     if (within_maxCourt == true){
                //         facilityArr.push(fiddValue);
                //     }
                // }
            } 
        } else { // If the checkbox is unchecked
            // Remove the unchecked fiddValue from facilityArr
            var index = facilityArr.indexOf(fiddValue);
            if (facilityArr.length == maxAllowed){
                var isMax = true;
            }
            if (index !== -1) {
                facilityArr.splice(index, 1);
            }
            console.log(facilityArr);
            // Remove disabled state and <del> tag from the unchecked fiddValue, except those with initially-deleted class
            $('.chk.' + fiddValue + ':not(:checked)').each(function() {
                if (!$(this).hasClass('initially-deleted')) {
                    $(this).prop('disabled', false);
                    $(this).closest('tr').find('td').not(':first-child').each(function() {
                        $(this).find('del').contents().unwrap();
                    });
                }
            });
            let uniqueValues = new Set();

            // // Iterate through the array and add each value to the Set
            // for (let i = 0; i < facilityArr.length; i++) {
            //     uniqueValues.add(facilityArr[i]);
            // }
            // let uniqueCount = uniqueValues.size;
            // console.log(uniqueCount, max_court);
            // if (uniqueCount == (max_court - 1)){
            //     $('.chk:not(:checked)').each(function() {
            //         if (!$(this).hasClass('initially-deleted')) {
            //             $(this).prop('disabled', false);
            //             $(this).closest('tr').find('td').not(':first-child').each(function() {
            //                 $(this).find('del').contents().unwrap();
            //             });
            //         }
            //     });
            // }
        }
        
        if (checkedCount > maxAllowed) {
            $(this).prop('checked', false);
            Swal.fire(
                'Harap Maaf!',
                'Anda telah memilih terlalu banyak slot tempahan. Sila pilih maksimum ' + maxAllowed + ' slot.',
                'error'
            );
        }
    });
</script>
@if ($data['bookingMonthly'] > 3)
    <script>
        // Display Swal.fire
        Swal.fire({
            title: 'Tempahan telah mencapai had.',
            text: 'Anda telah mencapai had tempahan bagi bulan ini.',
            icon: 'warning',
            confirmButtonText: 'OK'
        }).then((result) => {
            if (result.isConfirmed) {
                // Uncheck all checkboxes
                $('input[name="slot[]"]').prop('checked', false);
            }
        });
    </script>
@endif
<script src="{{ asset('assets/plugins/custom/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/leaflet/leaflet.bundle.js') }}"></script>
<script src="https://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js"></script>
<!--end::Page Vendors-->
<script>
    filterSelection("all")
    function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("column");
    if (c == "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
    }

    function w3RemoveClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
        arr1.splice(arr1.indexOf(arr2[i]), 1);     
        }
    }
    element.className = arr1.join(" ");
    }


    // Add active class to the current button (highlight it)
    var btnContainer = document.getElementById("myBtnContainer");
    var btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function(){
        var current = document.getElementsByClassName("active1");
        current[0].className = current[0].className.replace(" active1", "");
        this.className += " active1";
    });
    }
</script>
<script>
    $('#kt_select2_4').on('change', function(){
        var location_id = $(this).val();
        if (location_id == ''){
            $('#kt_select2_5').prop('disabled', true);
        }
        else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('sport/ajaxpublic') }}",
                type: "POST",
                data: {'location_id' : location_id},
                dataType: 'json',
                success: function(data){
                    if (data.flash_message) {
                        $('#kt_select2_5').prop('disabled', true);
                        $('#submit_form').prop('disabled', true).html('Pilih Lokasi');
                        Swal.fire(
                            'Harap Maaf!',
                            data.flash_message,
                            'error'
                        )
                    } else {
                        $('#submit_form').prop('disabled', false).html('Carian');
                        $('#kt_select2_5').html(data);
                    }
                }
            });
        }
    });
    $('#kt_select2_5').on('change', function(){
        var kemudahan = $(this).val();
        console.log(kemudahan)
        if(kemudahan == 0){
            $('#submit_form').attr('disabled', true);
        } else {
            $('#submit_form').removeAttr('disabled');
        }
    });
</script>
<script>
    @if(count($data['pengumuman']) != 0)	
    $(window).on('load', function() {
        $('#exampleModalCustomScrollable').modal('show');

    });
    @endif
</script>
<script>
        var peta = <?php echo json_encode($data['fasility']) ?>;
        console.log(peta);
        function pop(gambar, nama, p, desc, link){
            var data = "<img style='width: 250px; height: 150px' src='{{ URL::asset('/assets/upload/main') }}/"+gambar+"'>";
            data += "<p style='text-align: center'>";
            data += "<b>"+nama+"</b>, "+p+"<br>";
            data += '<i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><br><br>';
            data += desc;
            data += "<br>";
            data += "<a href='/sport/details/"+link+"' class='btn font-weight-bolder btn-outline-primary mt-4'>Teruskan Tempahan</a>";
            data += "</p>";
            return data;
        }

        var cities = L.layerGroup();
        var markers = L.markerClusterGroup({
            iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', 
                className: 'marker-cluster' + ' marker-cluster-small', iconSize: new L.Point(40, 40) });
            }
        }).addTo(cities);

        var leaflet = L.map('map', {
			center: [2.9264, 101.6964],
			zoom: 11
		});

		// set leaflet tile layer
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		}).addTo(leaflet);

        leaflet.addLayer(markers);

		// set custom SVG icon marker
		var leafletIcon = L.divIcon({
			html: `<span class="svg-icon svg-icon-primary svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
			bgPos: [10, 10],
			iconAnchor: [20, 37],
			popupAnchor: [0, -37],
			className: 'leaflet-marker'
		});

        function presint($id)
        {
            if($id == 4)
            {
                $id = "Presint 8";
            }else if($id == 5)
            {
                $id = "Presint 9"; 
            }
            else if($id == 6)
            {
                $id = "Presint 11"; 
            }else if($id == 7)
            {
                $id = "Presint 16"; 
            }else if($id == 8)
            {
                $id = "Presint Diplomatik, Presint P15"; 
            }else if($id == 9)
            {
                $id = "Kompleks Sukan Taman Pancarona"; 
            }
            else{
                $id = "Tempahan Sukan";
            }
            return $id;
        }

		// bind marker with popup
        for (var p of peta)
        {
            var marker = L.marker(new L.LatLng(p.latitude,p.longitude), { icon: leafletIcon })
            .bindPopup(pop(p.cover_img, p.eft_type_desc, presint(p.fk_lkp_location), p.highlight, p.encrypt));
            //marker.addTo(leaflet);
            markers.addLayer(marker);
        }

        var homeTab = document.getElementById('kt_tab_pane_7_3');
        var observer1 = new MutationObserver(function(){
            if(homeTab.style.display != 'none'){
                leaflet.invalidateSize();
            }
        });
        observer1.observe(homeTab, {attributes: true}); 

</script>
<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay: true,
            dots: false,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 1,
                }
            }
        });
    });
</script>

<script>
    var events = "{{ url('takwim/call') }}";
    console.log(events);
    "use strict";

    var KTCalendarListView = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            var calendarEl = document.getElementById('kt_calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'ms-my',
                plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],

                isRTL: KTUtil.isRTL(),
                header: {
                    left: 'prev,next',
                    center: '',
                    right: 'title'
                },

                height: 500,
                contentHeight: 550,
                aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

                views: {
                    dayGridMonth: { buttonText: 'month' },
                    listWeek: { buttonText: 'list' }
                },

                defaultView: 'listWeek',
                defaultDate: TODAY,
                allDayText: '1 Hari',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: events,

                eventRender: function(info) {
                    var element = $(info.el);

                    if (info.event.extendedProps && info.event.extendedProps.description) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', info.event.extendedProps.description);
                            element.data('placement', 'top');
                            KTApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        }
                    }
                }
            });

            calendar.render();
        }
    };
}();

jQuery(document).ready(function() {
    KTCalendarListView.init();
});
</script>
<script>
    $(function() {
    var maxDate = moment().add(1, 'months'); // Calculate the maximum date

    $('#kt_daterangepicker_5').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), 
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
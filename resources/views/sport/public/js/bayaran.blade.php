
<script>
    $(document).ready(function() {
        $('#paymentButton').click(function(e) {
            e.preventDefault();
            var token = $('input[name="_token"]').val();
            var mbid = $('input[name="mbid"]').val();
            $.ajax({
                url: "{{ url('sport/ajaxPaymentStatus') }}",
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    mbid: mbid 
                },
                success: function(response) {
                    // Handle the response here
                    if (response.status === 'success') {
                        $('#paymentButton').closest('form').submit(); 
                    } else {
                        Swal.fire(
                            'Harap Maaf!',
                            'Tempahan telah dibatalkan secara automatik oleh sistem. Sila buat tempahan baharu.',
                            'error'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{{ url('sport') }}"; // Replace with your desired URL
                            }
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>

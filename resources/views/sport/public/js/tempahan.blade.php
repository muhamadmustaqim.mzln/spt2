<script>
    $(document).ready(function(){
        var numberOfSlots = {{ count($data['slot']) }};

        $(".btnDeleteCustom").on('click', function(event) {
            event.preventDefault();
            const href = $(this).attr('href');
            if (numberOfSlots == 1) {
                var text = "Rekod yang dipadam tidak dapat dikembalikan!<br>(Ke Paparan Tempahan Sukan)"
            } else {
                var text = "Rekod yang dipadam tidak dapat dikembalikan!"
            }
            console.log(href);
            Swal.fire({
                title: 'Anda pasti?',
                html: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                CancelButtonColor: '#d33',
                cancelButtonText: 'Kembali',
                confirmButtonText: 'Saya Pasti'
                }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            })
        });
    });
</script>
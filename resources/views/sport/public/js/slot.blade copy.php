<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
    var groupColumn = 1;
    var table = $('table').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        },
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
});
</script>
<script>
    $(document).ready(function() {
    var groupColumn = 4;
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
} );
</script>
<script>
    $(document).ready(function(){
        $("#form").submit(function( event ) {
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }else{
                $("#form").submit();
            }

        });
    });
</script>
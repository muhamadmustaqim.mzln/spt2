@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Saya</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Papan Pemuka</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Saya</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    @if ($data['jenis'] != null)
                        <div class="col-lg-12">
                            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        {{-- <a href="{{ url('/event/internal/form') }}" class="btn btn-primary font-weight-bolder mt-2"> --}}
                                        {{-- <a href="#" class="btn btn-primary font-weight-bolder mt-2">
                                            <i class="flaticon-plus"></i> Tambah Dewan
                                        </a> --}}
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-condensed" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>No Tempahan</th>
                                                    <th>Nama Pemohon</th>
                                                    <th>No Telefon</th>
                                                    <th>Nama Kemudahan</th>
                                                    <th>Lokasi</th>
                                                    <th>Tarikh tempahan</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @if ($data['jenis'] == 'my')
                                                    @foreach ($data['laporan_user'] as $e)
                                                        <tr>
                                                            <td style="width: 4.5%;">
                                                                {{ $i }}
                                                            </td>
                                                            <td><a href="{{ url('sport/maklumatbayaran', [Crypt::encrypt($e->id)]) }}">{{ $e->bmb_booking_no }}</a></td>
                                                            <td>{{ Helper::get_nama($e->fk_users) }}</td>
                                                            <td>{{ Helper::get_no($e->fk_users) }}</td>
                                                            <td>{{ Helper::typeSportFacility(Helper::getEtFromMb($e->id)) }}</td>
                                                            <td>{{ Helper::location($e->fk_lkp_location) }}</td>
                                                            <td>{{ ($e->created_at) }}</td>
                                                            <td>{{ Helper::get_status_tempahan($e->fk_lkp_status) }}</td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                @else
                                                    @foreach ($data['hall'] as $e)
                                                        <tr>
                                                            <td style="width: 4.5%;">
                                                                {{ $i }}
                                                            </td>
                                                            <td><a href="{{ url('hall/maklumatbayaran', [Crypt::encrypt($e->id), 'draf']) }}">{{ $e->bmb_booking_no }}</a></td>
                                                            <td>{{ $e->bud_name }}</td>
                                                            <td>{{ $e->bud_phone_no }}</td>
                                                            <td>{{ $e->bh_name }}</td>
                                                            <td>{{ Helper::date_format($e->bb_start_date) }}</td>
                                                            <td>{{ Helper::date_format($e->bb_end_date) }}</td>
                                                            <td>{{ Helper::get_status_tempahan($e->statusid) }}</td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-4">
                                    <!--begin::Mixed Widget 14-->
                            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b" style="height: 160px; background-image: url({{ asset('assets/media/stock-600x600/img-16.jpg') }})">
                                <!--begin::Body-->
                                <div class="card-body d-flex flex-column align-items-start justify-content-start">
                                    <div class="p-1 flex-grow-1">
                                        <h3 class="text-white font-weight-bolder line-height-lg mb-5">Tempahan
                                        <br />Sukan</h3>
                                    </div>
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Mixed Widget 14-->
                        </div>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card card-custom bg-primary gutter-b" style="height: 160px">
                                        <div class="card-body">
                                            <span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
                                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                                        <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                            <div class="text-inverse-primary font-weight-bolder font-size-h2 mt-3">{{ count($data['laporan_user']) }}</div>
                                            <a href="{{ url('sport/my-bookings/my') }}" class="text-inverse-primary font-weight-bold font-size-lg mt-1">Tempahan Saya</a>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-lg-4">
                                    <div class="card card-custom bg-info gutter-b" style="height: 160px">
                                        <div class="card-body">
                                            <span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
                                                <!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5" />
                                                        <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                            <div class="text-inverse-primary font-weight-bolder font-size-h2 mt-3">{{ count($data['hall']) }}</div>
                                            <a href="{{ url('hall/my-bookings/my') }}" class="text-inverse-primary font-weight-bold font-size-lg mt-1">Pemulangan Deposit</a>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    @endif

                </div>
                <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        
    </div>
					

@endsection

@section('js_content')
    @include('hall.public.js.tempahansaya')
@endsection
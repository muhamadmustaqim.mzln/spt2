@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
    <!--begin::Entry-->
    <style>
        .rate {
            float:left;
        }
        .rate:not(:checked) > input {
            position:absolute;
            top:-9999px;
        }
        .rate:not(:checked) > label {
            float:right;
            width:1em;
            overflow:hidden;
            white-space:nowrap;
            cursor:pointer;
            font-size:40px;
            color:#ccc;
        }
        .rate:not(:checked) > label:before {
            content: '★ ';
        }
        .rate > input:checked ~ label {
            color: #ffc700;    
        }
        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;  
        }
        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }
        /** end rating **/
        lottie-player {
            margin: 0 auto;
        }
    </style>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="card card-custom gutter-b">
                        <div class="card-body">
                            @if($data['status'] == "SUCCESS")
                                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                                <lottie-player src="https://assets4.lottiefiles.com/packages/lf20_pqnfmone.json"  background="transparent"  speed="0.8"  style="width: 200px; height: 200px;"  loop  autoplay></lottie-player>
                            @elseif($data['status'] == "PENDING_APPROVAL")
                                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                                <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_qbuxqwzg.json"  background="transparent"  speed="0.4"  style="width: 200px; height: 200px;"  loop  autoplay></lottie-player>
                            @else
                                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                                <lottie-player src="https://assets9.lottiefiles.com/packages/lf20_tl52xzvn.json"  background="transparent"  speed="0.8"  style="width: 200px; height: 200px;"  loop  autoplay></lottie-player>
                            @endif
                            <table class="table table-bordered">
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ substr($data['orderNo'], 0 ,14) }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">Nama Bank</th>
                                    <td>{{ $data['bankname'] }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">No. Transaksi Bank</th>
                                    <td>{{ $data['orderNo'] }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Transaksi</th>
                                    <td>{{ $data['txnTime'] }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">Status Bayaran</th>
                                    <td>{{ Helper::statusbayaran($data['status']) }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                                <tr>
                                    <th width="20%" class="text-light" style="background-color: #242a4c">Lokasi</th>
                                    <td>{{ ($data['et_facility_type']->eft_type_desc) }}, {{ Helper::location($data['main']->fk_lkp_location) }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                @if($data['status'] == "SUCCESS")
                                    <button type="button" class="btn btn-dark font-weight-bold" data-toggle="modal" data-target="#exampleModal">
                                        Lihat Tempahan Saya
                                    </button>
                                @else
                                    <a href="{{ url('/sport/maklumatbayaran', Crypt::encrypt($data['main']->id)) }}" class="btn btn-outline-dark font-weight-bold">Lihat Tempahan Saya</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <!--end::Card-->
            <!-- Modal-->
            <div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Rating Tempahan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <form action="{{ url('/sport/ratingtempahan', Crypt::encrypt($data['main']->id)) }}" method="post">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="rate">
                                            <input type="radio" id="star5" name="rate" value="5" checked="checked" />
                                            <label for="star5" title="text">5 stars</label>
                                            <input type="radio" id="star4" name="rate" value="4" />
                                            <label for="star4" title="text">4 stars</label>
                                            <input type="radio" id="star3" name="rate" value="3" />
                                            <label for="star3" title="text">3 stars</label>
                                            <input type="radio" id="star2" name="rate" value="2" />
                                            <label for="star2" title="text">2 stars</label>
                                            <input type="radio" id="star1" name="rate" value="1" />
                                            <label for="star1" title="text">1 star</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-4 mb-1">
                                    <textarea class="form-control" name="comment" id="exampleTextarea" rows="5" required></textarea>
                                    <span class="form-text text-muted">Jika ada sebarang maklumbalas, pandangan ataupun penambahbaikan berkenaan tempahan. Sila isi pada ruangan disediakan</span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-dark font-weight-bold">Simpan</button>
                                {{-- <a href="{{ url('/sport/maklumatbayaran', Crypt::encrypt($data['main']->id)) }}" class="btn btn-outline-dark font-weight-bold">Simpan</a> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
@endsection

@section('js_content')

@endsection
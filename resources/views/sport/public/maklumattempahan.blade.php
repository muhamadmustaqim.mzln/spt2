@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif

<style>
    .rate {
        float:left;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:40px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;    
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;  
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }
    /** end rating **/
    lottie-player {
        margin: 0 auto;
    }
</style>

<input type="text" name="bookingStatus" id="bookingStatus" value="{{ $data['main']->fk_lkp_status }}" hidden>
<input type="text" name="ratingStatus" id="ratingStatus" value="{{ $data['rating'] }}" hidden>
<input type="text" name="currentUserId" id="userId" value="{{ Session::get('user.id') }}" hidden>
<input type="text" name="bookingUserId" id="bookingUserId" value="{{ $data['main']->fk_users }}" hidden>
@section('js_content')
    <script>
        $(document).ready(function() {
            var booking_status = document.getElementById('bookingStatus').value;
            var ratingStatus = document.getElementById('ratingStatus').value;
            var userId = document.getElementById('userId').value;
            var bookingUserId = document.getElementById('bookingUserId').value;
            if (booking_status == 5 && ratingStatus == 0) {
                if (userId == bookingUserId){
                    var ratingModal = document.getElementById('ratingModal');
                    $('#ratingModal').modal('show');
                }
            }
        });
    </script>
@endsection

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!-- Modal-->
    <div class="modal fade" id="ratingModal" data-backdrop="static" role="dialog" aria-labelledby="ratingModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ratingModalLabel">Rating Tempahan</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button> --}}
                </div>
                <form action="{{ url('/sport/ratingtempahan', Crypt::encrypt($data['main']->id)) }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="rate">
                                    <input type="radio" id="star5" name="rate" value="5" checked="checked" />
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="rate" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="rate" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="rate" value="2" />
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="rate" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4 mb-1">
                            <textarea class="form-control" name="comment" id="exampleTextarea" rows="5" required></textarea>
                            <span class="form-text text-muted">Jika ada sebarang maklumbalas, pandangan ataupun penambahbaikan berkenaan tempahan. Sila isi pada ruangan disediakan</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-dark font-weight-bold">Simpan</button>
                        {{-- <a href="{{ url('/sport/maklumatbayaran', Crypt::encrypt($data['main']->id)) }}" class="btn btn-outline-dark font-weight-bold">Simpan</a> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>
                                        {{ $data['user_detail']->bud_phone_no }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                                <tr>
                                    {{-- <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td> --}}
                                        <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                        <td>
                                            @if($data['main']->bmb_deposit_rm == null)
                                                0.00
                                            @else 
                                                {{ ($data['main']->bmb_deposit_rm) }}
                                            @endif
                                        </td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    @if ($data['type'] == 2)
                        <div class="row d-flex justify-content-end">
                            <div class="col-lg-2" align="right">
                                <a href="{{ url('/sport/janabpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary btn-sm" target="_blank">Cetak Borang BPSK</a>
                            </div>
                        </div>
                    @endif
                    @if ($data['et_attachment'])
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="14.5%" class="text-light" style="background-color: #242a4c">Dokumen Sokongan</th>
                                        <td><a href="{{ public_path($data['et_attachment']->eta_dir . '/' . $data['et_attachment']->eta_file_name) }}" class="btn btn-primary" download>Dokumen Sokongan</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <!--start::Senarai Resit-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Jenis Bayaran</th>
                                    <th>No. Resit</th>
                                    @if(count($data['online']) > 0)
                                        <th>No. SAP</th>
                                    @endif
                                    <th>Tarikh Transaksi</th>
                                    @foreach($data['tax'] as $taxItem)
                                        @if($taxItem->lt_status == 1)
                                            <th>Kod GST</th>
                                            <th>GST (RM)</th>
                                        @endif
                                    @endforeach
                                    <th class="text-right">Jumlah Bayaran (RM)</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $paid = 0.00;
                                $success = false;
                            @endphp
                            <tbody>
                                @if(count($data['kaunter']) > 0)
                                    @foreach($data['kaunter'] as $p)
                                    @php
                                        $kaunterDetails = isset($data['kaunterdetail'][$p->id]) ? $data['kaunterdetail'][$p->id] : [];
                                        $hasEquipment = false;

                                        // Check if any kaunter detail has fk_et_equipment
                                        foreach ($kaunterDetails as $detail) {
                                            if ($detail->fk_et_equipment != null) {
                                                $hasEquipment = true;
                                                break;
                                            }
                                        }
                                    @endphp
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        @if($hasEquipment)
                                            <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }} PERALATAN</td>
                                        @elseif($data['type'] == 1)
                                            <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }} SUKAN</td>
                                        @else
                                            <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }} DEWAN</td>
                                        @endif
                                        <td>{{ $p->bp_receipt_number }}</td>
                                        <td>{{ Helper::datetime_format($p->bp_receipt_date) }}</td>
                                        @foreach($data['tax'] as $taxItem)
                                            @if($taxItem->lt_status == 1)
                                                <td>{{ $taxItem->lt_code }}</td>
                                                <td>{{ $taxItem->lt_rate }}</td>
                                            @endif
                                        @endforeach
                                        <td align="right">{{ Helper::moneyhelper($p->bp_paid_amount) }}</td>
                                        <td style="width: 15%;">
                                            <a href="{{ url('sport/resitonline', [Crypt::encrypt($data['booking']), Crypt::encrypt($hasEquipment)]) }}" target="_blank" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                        </td>
                                    </tr>
                                    @php
                                        $paid = $paid + $p->bp_paid_amount
                                    @endphp
                                    @endforeach
                                @elseif(count($data['online']) > 0)
                                    @foreach($data['online'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>
                                            @if ($p->fpx_status == 1)
                                                {{ $p->fpx_trans_id }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $p->sap_doc_no ?? "-" }}</td>
                                        <td>{{ $p->fpx_trans_date ?? "-" }}</td>
                                        @foreach($data['tax'] as $taxItem)
                                            @if($taxItem->lt_status == 1)
                                                <td>{{ $taxItem->lt_code }}</td>
                                                <td>{{ $taxItem->lt_rate }}</td>
                                            @endif
                                        @endforeach
                                        <td class="text-right">{{ Helper::moneyhelper($p->amount_paid) }}</td>
                                        <td style="width: 15%;">
                                            @if ($p->fpx_status == 1)
                                                @php
                                                    $success = true;
                                                @endphp
                                                <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                            @elseif ($p->fpx_status == 6)
                                                Pembayaran Gagal
                                            @elseif ($p->fpx_status == 0)
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                        $paid = $paid + $p->amount_paid;
                                    @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="text-center" colspan="6">Tiada Resit Bayaran</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if (!$success)
                            @if ($data['main']->fk_lkp_status != 1)
                                <div class="d-flex justify-content-end">
                                    {{-- <form action="{{ url('sport/bayaran', Crypt::encrypt($data['booking'])) }}" method="post">
                                        @csrf
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fas fa-check-circle"></i> 
                                                Buat Pembayaran</button>
                                            <input type="hidden" name="total" value="{{ $p->amount_paid }}">
                                    </form> --}}
                                </div>
                            @endif
                        @endif
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Senarai Resit-->
            <!--start::Senarai Tempahan Fasiliti-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Fasiliti</th>
                                    <th>Lokasi</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Slot Masa</th>
                                    {{-- <th>Harga Sewaan (RM)</th> --}}
                                    {{-- <th>Kod GST</th>
                                    <th>GST (RM)</th> --}}
                                    <th style="text-align: end">Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $total = 0.00;
                                @endphp
                                {{-- Sukan --}}
                                @if ($data['type'] == 1)
                                    @foreach($data['slot'] as $s)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                        <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                        <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                        <td>{{ Helper::date_format($s->ebf_start_date) }}</td>
                                        <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                        {{-- <td style="text-align: end">{{number_format($s->est_total, 2)}}</td> --}}
                                        {{-- <td style="text-align: end">{{ Helper::tempahanSportGST($s->est_gst_code)}}</td>
                                        <td style="text-align: end">{{$s->est_gst_rm}}</td> --}}
                                        <td style="text-align: end">{{number_format($s->est_subtotal, 2)}}</td>
                                        @php
                                            $total = $total + number_format($s->est_subtotal, 2)
                                        @endphp
                                    </tr>
                                    @endforeach
                                    @if ($data['main']->fk_lkp_status == 11)
                                        @php
                                            // $paid += $total;
                                        @endphp
                                    @endif
                                {{-- Dewan --}}
                                @else
                                    @foreach ($data['slot'] as $s)
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                            <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                            <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                            <td>{{ Helper::date_format($s->ebf_start_date) }}</td>
                                            {{-- Sport --}}
                                            @if ($data['type'] == 1) 
                                                <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                            {{-- Hall --}}
                                            @else
                                                <td>{{ Helper::tempahanHallTime($s->fk_et_facility_price) }}</td>
                                            @endif
                                            {{-- Sport --}}
                                            @if ($data['type'] == 1) 
                                                {{-- <td style="text-align: end">{{number_format($s->est_total, 2)}}</td> --}}
                                                {{-- <td style="text-align: end">{{ Helper::tempahanSportGST($s->est_gst_code)}}</td>
                                                <td style="text-align: end">{{$s->est_gst_rm}}</td> --}}
                                                <td style="text-align: end">{{number_format($s->est_subtotal, 2)}}</td>
                                                @php
                                                    $total = $total + number_format($s->est_subtotal, 2)
                                                @endphp
                                                {{-- @if ($data['main']->fk_lkp_status )
                                                    @php
                                                        $paid = $total + number_format($s->est_subtotal, 2)
                                                    @endphp
                                                @endif --}}
                                            {{-- Hall --}}
                                            @else
                                                {{-- <td style="text-align: end">{{number_format($s->eht_total, 2)}}</td> --}}
                                                {{-- <td style="text-align: end">{{ Helper::tempahanSportGST($s->eht_gst_code)}}</td>
                                                <td style="text-align: end">{{$s->eht_gst_rm}}</td> --}}
                                                <td style="text-align: end">{{number_format($s->eht_subtotal, 2)}}</td>
                                                @php
                                                    $total = $total + number_format($s->eht_subtotal, 2)
                                                @endphp
                                                {{-- @if ($data['main']->fk_lkp_status )
                                                    @php
                                                        $paid = $total + number_format($s->eht_subtotal, 2)
                                                    @endphp
                                                @endif --}}
                                            @endif
                                        </tr>
                                    @endforeach
                                    {{-- @if ($data['main']->fk_lkp_status == 11)
                                        @php
                                            $paid += $total;
                                        @endphp
                                    @endif --}}
                                @endif
                                <tr>
                                    <td colspan="5" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">{{ $total_v = number_format($total, 2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Senarai Tempahan Fasiliti-->
            <!--start::Kadar Sewaan Peralatan-->
            @if ($data['type'] == 2)
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Tarikh Kegunaan</th>
                                        <th>Penggunaan</th>
                                        <th>Item</th>
                                        <th>Kuantiti</th>
                                        {{-- <th>Jumlah (RM)</th> --}}
                                        {{-- <th>Kod GST</th>
                                        <th>GST (6%)</th> --}}
                                        <th>Jumlah Sewaan (RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                    $jumlahperalatan = 0;
                                    $jumlahsewaanperalatan = 0;
                                @endphp
                                <tbody>
                                    @foreach ($data['et_equipment_book'] as $eeb)
                                        <tr>
                                            <td>{{ Helper::date_format($eeb->eeb_booking_date) }}</td>
                                            <td>{{ Helper::typeFunction($eeb->fk_et_function) }}</td>
                                            <td>{{ Helper::get_equipment($eeb->fk_et_equipment) }}</td>
                                            <td>{{ $eeb->eeb_quantity }}</td>
                                            {{-- <td class="text-right">{{ $eeb->eeb_total }}</td> --}}
                                            <td class="text-right">{{ $eeb->eeb_subtotal }}</td>
                                        </tr>
                                    @php
                                        $jumlahperalatan += $eeb->eeb_total;
                                        $jumlahsewaanperalatan += $eeb->eeb_subtotal;
                                    @endphp
                                    @endforeach
                                        <tr>
                                            <td colspan="4" class="text-right font-weight-bolder">Jumlah (RM)</td>
                                            {{-- <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahperalatan) }}</td> --}}
                                            <td class="text-right font-weight-bolder">{{ Helper::moneyhelper($jumlahsewaanperalatan) }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
            @endif
            <!--end::Kadar Sewaan Peralatan-->
            <!--start::Jumlah Keseluruhan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            {{-- <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">E</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">I</td>
                                <td class="text-right">0.00</td>
                            </tr> --}}
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_subtotal }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_rounding }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar (RM)</th>
                                <td></td>
                                <td class="text-right">
                                    @if ($data['main']->fk_lkp_status != 6)
                                        {{ number_format($paid, 2) }}
                                    @else
                                        0.00
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-end">
                    @if (Session::get('user.role') == 10)
                        <a href="{{ url('/sport/my-bookings') }}" class="btn btn-primary float-right">Kembali</a>
                    @elseif (Session::get('user.role') == 1)
                        <a href="{{ url('/sport/dashboard') }}" class="btn btn-primary float-right">Kembali</a>
                    @else
                        <a href="{{ url('/sport/dashboard') }}" class="btn btn-primary float-right">Kembali</a>
                    @endif
                    {{-- @if (!in_array($data['main']->fk_lkp_status, [5, 11])) --}}
                    @if (in_array($data['main']->fk_lkp_status, [2]))
                        {{-- @if ($data['type'] == 1)
                            <div class="d-flex justify-content-end">
                                <a href="{{ url('/sport/bayaran', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary mx-2">Teruskan</a>
                            </div>
                        @else --}}
                        @if(in_array(Session::get('user.role'), [1]))
                            <div class="d-flex justify-content-end">
                                <a href="{{ url('/sport/dashboard/validation', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary mx-2">Teruskan</a>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <!--end::Jumlah Keseluruhan-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection
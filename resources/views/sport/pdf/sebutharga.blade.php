<style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:10px;
    }
     #footer{
      font-size:8px;
    }
    #back{
       
       width:100%; 
       height:99%;
      /*  background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
       background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
       background-repeat: no-repeat;
       background-position: center;
       /*background-attachment:fixed;*/
    }
    
    /* Force all children to have a specified font-size */
</style>

<div id="back">
    <table  width="100%" border="0" cellpadding="0">
        <tr>
            <!--<td width="20%"><img src="{{asset('packages/threef/entree/img/logo.png')}}" width="120" height="127" />
            <td width="20%"><img src="https://www.ppj.gov.my/web/images/inverted-logo.png" width="120" height="127" />-->
            <td width="20%"><img src="{{public_path('assets/media/Logo Perbadanan Putrajaya.png')}}" width="110" height="127" />
            </td>
            {{-- <td>
                &nbsp;
            </td> --}}
            <td>
                <table>
                    <tr>
                        <td id="title"><strong>PERBADANAN PUTRAJAYA</strong>
                        </td>
                    </tr>
                    <tr>
                        <td id="title"><strong>KOMPLEKS KEJIRANAN <?php echo strtoupper(Helper::location($data['getbmbno']->fk_lkp_location))?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td id="title"><strong>{{Helper::location_addr($data['getbmbno']->fk_lkp_location)}},{{Helper::location_town($data['getbmbno']->fk_lkp_location)}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td id="title"><strong>{{Helper::location_postcode($data['getbmbno']->fk_lkp_location)}},{{Helper::negeri(Helper::location_data($data['getbmbno']->fk_lkp_location)->fk_lkp_state)}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td id="title"><strong>Tel: {{Helper::location_contact_no($data['getbmbno']->fk_lkp_location)}} / 03-8000 8000</strong>
                        </td>
                    </tr>
                    <tr>
                        <td id="title"><strong>Fax: {{Helper::location_fax_no($data['getbmbno']->fk_lkp_location)}}</strong>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table id="title" width="100%" border="0" cellpadding="0">
        <tr>
            <td><center><strong>SEBUTHARGA / QUOTATIONS</strong></center></td>
        </tr>
    </table>
    <table id="title" width="100%" border="0" cellpadding="0">
        <tr>
            <td width="9%"><strong><font size="-2">NO. GST :</font></strong></td>
            
            <td width="90%"><strong><font size="-2">00139960320</td>
        </tr>
    </table>
    <table id="title" width="100%" border="2" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <strong>BUTIRAN PENYEWAAN<em></em></strong>
            </td>
        </tr>
    </table>

    <br>

    <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr>
            <td style="vertical-align: top;" width="20%"><b>No. Sebutharga</b></td>
            <td style="vertical-align: top;" width="2%">:</td>
            <td style="vertical-align: top;" width="40%">&nbsp;{{$data['bh_quotation']->bq_quotation_no}}</td>
            <td style="vertical-align: top;" width="20%"><b>Jenis Tempahan</b></td>
            <td style="vertical-align: top;" width="2%">:</td>
            @if($data['getbmbno']->fk_lkp_discount_type =='')
                <td>&nbsp;</td>
            @else
                <td>{{Helper::getJenisTempahan($data['getbmbno']->fk_lkp_discount_type)}}</td>
            @endif
        </tr>
        <tr>
            <td width="12%"><b>Tarikh Sebutharga</b></td>
            <td>:</td>
            <td>{{$data['bh_quotation']->bq_quotation_date}}</td>
            <td width="12%"><b>Jumlah Tempahan (RM)</b></td>
            <td>:</td>
            <td>{{$data['getbmbno']->bmb_subtotal}}</td>
        </tr>
        <tr>
            <td width="12%"><b>No Tempahan</b></td>
            <td>:</td>
            <td>{{$data['getbmbno']->bmb_booking_no}}</td>
            <td width="12%" ><b>Deposit (RM)</b></td>
            <td>:</td>
            {{-- <td>{{$data['getbmbno']->bmb_deposit_rounding}}</td> --}}
            <td>
                <?php
                if(isset($data['getbmbno']->bmb_deposit_rounding)) {
                    echo $data['getbmbno']->bmb_deposit_rounding;
                } else {
                    echo number_format(0, 2);
                }
                ?>
            </td>
        </tr>

        <tr>
            <td width="12%"><b>Nama Pemohon</b></td>
            <td>:</td>
            <td><?php echo strtoupper(Helper::get_name($data['getbmbno']->fk_users))?></td>
        </tr>
        <tr>
            <td width="20%"><b>Alamat Pemohon</b></td>
            <td>:</td>
            <td colspan="3"><?php echo strtoupper(Helper::get_address($data['getbmbno']->fk_users))?>,<br>
                <?php echo strtoupper(Helper::get_town($data['getbmbno']->fk_users))?>,
            <?php echo strtoupper(Helper::get_postcode($data['getbmbno']->fk_users))?>,
            <?php echo strtoupper(Helper::negeri(Helper::location_data($data['getbmbno']->fk_lkp_location)->fk_lkp_state))?>,
            <?php echo strtoupper(Helper::negara(Helper::location_data($data['getbmbno']->fk_lkp_location)->fk_lkp_country))?>
            </td>
        </tr>
        <!--   <tr>
            <td width="12%"><b>Jenis Tempahan</b></td>
            <td>:</td>
            @if($data['getbmbno']->fk_lkp_discount_type =='')
                <td>&nbsp;</td>
            @else
                <td>&nbsp;{{Helper::get_permohonan($data['getbmbno']->fk_lkp_discount_type)}}</td>
            @endif
        </tr>
            <tr>
            <td width="12%"><b>Jumlah Tempahan (RM)</b></td>
            <td>:</td>
            <td>&nbsp;{{$data['getbmbno']->bmb_rounding}}</td>
        </tr>
            <tr>
            <td width="12%" ><b>Deposit (RM)</b></td>
            <td>:</td>
            <td>&nbsp;{{$data['getbmbno']->bmb_deposit_rounding}}</td>
        </tr> -->
    </table>
        <div id="title"><br><b>KETERANGAN :</b><br><b>SEBUTHARGA BAGI PENGGUNAAN DEWAN / FASILITI KOMPLEKS KEJIRANAN <?php echo strtoupper(Helper::location($data['getbmbno']->fk_lkp_location))?></b></div>
        <br>
        @if($data['checkfacility']==1)
            <div id="title"><b>TUJUAN TEMPAHAN </b></div>
        
            <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
            <thead>
            <th width="2%" align="center"><b>Bil.</b></th>
            <th width="17%"align="center"><b>Item</b></th>
            <th width="12%" align="center"><b>Tarikh Mula</b></th>
            <th width="12%" align="center"><b>Tarikh Tamat</b></th>
            <th width="12%" align="center"><b>Tujuan Tempahan</b></th>
            </thead>
                <?php  
                        $i=1; 
                        
                        ?>
        @forelse($data['et_booking_facility'] as $key => $value)
            <tr>
                <td width="5px" align="center"><?php echo $i ?></a></td>
                <td width="50px" align="left">{{ Helper::tempahanSportDetail($value->fk_et_facility_detail) }}</a></td>
                <td width="50px" align="center">{{ Helper::date_format($value->ebf_start_date) }}</a></td>
                <td width="50px" align="center">{{ Helper::date_format($value->ebf_end_date) }}</a></td>
                @if((Helper::get_maklumat_tempahan_terperinci($value->id)) =='' || (Helper::get_maklumat_tempahan_terperinci($value->id)) ==13)
                    <td width="50px" align="left"></a></td>
                @else
                    <td width="50px" align="center">{{ Helper::get_lkp_event(Helper::get_kemudahan_terperinci($value->id)->fk_lkp_event) }}</a></td>
                @endif 
            </tr>
            <?php $i++;?>
        @empty
            <tr><td colspan='10'></td></tr>
            <tr><td colspan='10'>Tiada Data</td></tr>
        @endforelse
   </table>

    @endif
 
   {{-- <div id="title"><b>{{trans('sportfinance::sportfinance.bayarandetail.sewaanbangunan')}}</b></div> --}}
   <div id="title"><b>Kadar Sewaan Bangunan</b></div>
        <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
            <thead>
                <tr>
                    <th width="1px" align="center"><b>Bil.</b></th>
                    <th width="5px" align="center"><b>Nama Dewan/ Fasiliti</b></th>
                    <th width="5px" align="center"><b>Tarikh</b></th>
                    <th width="5px" align="center"><b>Slot Masa</b></th>
                    <th width="5px" align="center"><b>Harga (RM)</b></th>
                    <th width="5px" align="center"><b>Diskaun Kategori (RM)</b></th>
                    <th width="5px" align="center"><b>Diskaun Khas (RM)</b></th>
                    <!-- <th width="12" align="center"><b>{{ trans('sportfinance::sportfinance.bayarandetail.kuantiti') }}</b></th> -->
                    <th width="5px" align="center"><b>Harga(RM)</b><br><font size="1">*selepas diskaun</font></th>
                    @if($data['lkp_gst']->gst_status == 1)
                        <th width="5px" align="center"><b>Kod GST</b></th>
                        <th width="5px" align="center"><b>GST (0%)</b></th>
                    @endif
                    <th width="5px" align="center"><b>Jumlah Sewaan (RM)</b></th>
                </tr>
            </thead>
            <?php  
                $a=1; 
                $sumselepas=0;
            ?>
            @forelse($data['datatambahan'] as $key => $value)
                @if($data['checkfacility']==1)
                    <tr style="page-break-inside:avoid;">
                        <td width="5px" align="center"><?php echo $a ?></a></td>
                        <td width="70px">{{ Helper::tempahanSportDetail(Helper::get_et_booking_facility($value->fk_et_booking_facility)->fk_et_facility_detail) }}</td>
                        <td width="40px" align="center"><?php echo date('d-m-Y',strtotime($value->ehb_booking_date)) ?></td>
                        <td width="70px" align="center">{{ Helper::tempahanSportTime($value->fk_et_slot_time) }}</td>
                        <td width="" align="right">{{ $value->eht_price }}</td>
                        <td width="" align="right">
                            <?php
                            if(isset($value->eht_discount_type_rm)) {
                                echo $value->eht_discount_type_rm;
                            } else {
                                echo number_format(0, 2);
                            }
                            ?>
                        </td>
                        <td width="" align="right">
                            <?php
                            if(isset($value->eht_special_disc_rm)) {
                                echo $value->eht_special_disc_rm;
                            } else {
                                echo number_format(0, 2);
                            }
                            ?>
                        </td>
                        <!-- <td width="200px" align="center">{{ Helper::get_et_booking_facility($value->fk_et_booking_facility)->ebf_no_of_day }}</td> -->
                        <td width="" align="right">{{ $value->eht_total }}</td>
                        @if($data['lkp_gst']->gst_status == 1)
                            <td width="" align="center">{{ Helper::tempahanSportGST($value->eht_gst_code) }}</td>
                            <td width="" align="right">{{ $value->eht_gst_rm }}</td> 
                        @endif
                        <td width="" align="right">{{ $value->eht_subtotal }}</td>
                    </tr>
                @else
                    <tr style="page-break-inside:avoid;">
                        <td width=""><?php echo $a ?></a></td>
                        <td width="">{{ $value->etsportbook->etbookingfacility->etfacilitydetail->efd_name }}</td>
                        <td width=""><?php echo date('d-m-Y',strtotime($value->etsportbook->esb_booking_date)) ?></td>
                        <!-- <td width="200px">{{ $value->etsportbook->etbookingfacility->mainbooking->lkplocation->lc_description }}</td> -->
                        <td width="">{{ $value->etslotprice->etslottime->est_slot_time }}</td>
                        <td width="" align="right">{{ $value->est_price }}</td>
                        <td width="" align="right">{{ $value->est_discount_type_rm }}</td>
                        <td width="" align="right">{{ $value->est_special_disc_rm }}</td>
                        <!-- <td width="200px" align="center">{{ $value->etsportbook->etbookingfacility->ebf_no_of_day }}</td> -->
                        <td width="" align="right">{{ $value->est_total }}</td>
                        @if($data['lkp_gst']->gst_status == 1)
                            <td width="" align="center">{{ $value->ehtgstcode->lgr_gst_code }}</td>
                            <td width="" align="right">{{ $value->est_gst_rm }}</td>
                        @endif
                        <td width="" align="right">{{ $value->est_subtotal }}</td>
                    </tr>
                @endif
            <?php $a++;?>
                  
    
                    <?php 
                    if($data['checkfacility']==1){
                      $sumselepas+= $value->eht_total;

                    }else{
                      $sumselepas+= $value->est_total;


                    }

                    

                    ?>
               
                    @empty
                      <tr><td colspan='11'>Tiada Data</td></tr>
                 @endforelse
      
        </table>
        {{-- @if($data['getbmbno']->fk_lkp_discount_type !=1) 
            <div id="footer">* Selepas diskaun {{($data['getbmbno']->fk_lkp_discount_type)}}%</div>
        @else --}}
            <div>&nbsp;</div>
        {{-- @endif --}}
    <?php 
            //if($gstSR->gst==''){
                //$gstSR=0.00;
            //}else{
                //$gstSR=$gstSR->gst;
            // }
            //if($gstSRI->gst==''){
                //$gstSRI=0.00;
        //  }else{
                //$gstSRI=$gstSRI->gst;
            // }
            ?>
        <?php 
        //  if($gsteqpSR->gst==''){
        //       $gsteqpSR=0.00;
        //  }else{
        //       $gsteqpSR=$gsteqpSR->gst;
        //  }
            //  if($gsteqpSRI->gst==''){
        //       $gsteqpSRI=0.00;
        //  }else{
            //      $gsteqpSRI=$gsteqpSRI->gst;
        //  }
        $gstSR = 0;
        $gstSRI = 0;
            ?>

    <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
        {{-- <tr style="page-break-inside:avoid;">
            <th width="50%" scope="col">{{ trans('sales::sales.bayarandetails.gst') }} </th>
            <th width="36%" scope="col" align="right">{{$gstSR}}</th>
            <th width="20%" scope="col" align="center">E</th>
        </tr>
        <tr style="page-break-inside:avoid;">
            <th width="50%" scope="col">{{ trans('sales::sales.bayarandetails.gst') }} </th>
            <th width="36%" scope="col" align="right">{{$gstSRI}}</th>
            <th width="20%" scope="col" align="center">I</th>
        </tr> --}}
        <tr>
            <th scope="row">Jumlah Sewaan Dewan</th>
            <td align="right"><b><?php echo number_format($sumselepas,2);?></b></td>
            <td>&nbsp;</td>
        </tr>
        @if($data['lkp_gst']->gst_status == 1)
            <tr>
                <th scope="row">Jumlah Sewaan Dewan + GST (0%)(RM)</th>
                <td align="right"><b><?php echo number_format($sumselepas+$gstSR+$gstSRI,2);?></b></td>
                <td>&nbsp;</td>
            </tr>
        @endif
        <tr>
            @if($data['getbmbno']->fk_lkp_status==9)
                <th scope="row">Jumlah Deposit telah dibayar (RM)</th>
            @else
                <th scope="row">Jumlah Deposit (RM)</th>
            @endif
            <td align="right"><b><?php echo number_format($data['getbmbno']->bmb_deposit_rounding,2);?></b></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    @if($data['checkfacility']==1)
        <div id ="back" style="page-break-before:always;">
    
        <table width="100%" border="0"><tr style="page-break-inside: avoid"><td></td></tr></table>
            <div id="title"><b>&nbsp;KADAR SEWAAN PERALATAN</b></div>
            <br>
            <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr style="page-break-inside: avoid">
                    <th width="4%" align="center"><b>Bil.</b></th>
                    <th width="17%" align="center"><b>Nama Dewan/ Fasiliti</b></th>
                    <th width="12%" align="center"><b>Item</b></th>
                    <th width="12%" align="center"><b>Tarikh</b></th>
                    <th width="12%" align="center"><b>Jenis Penggunaan</b></th>
                    <th width="12%" align="center"><b>Kuantiti</b></th>
                    <th width="8%" align="center"><b>Harga (RM)</b></th>
                    <th width="8%" align="center"><b>Diskaun Kategori (RM)</b></th>
                    <th width="8%" align="center"><b>Diskaun Khas (RM)</b></th>
                    <th style="text-align:right" width="8%"><b>Harga(RM)</b><br><font size="1">*selepas diskaun</font></th>
                    {{-- <th style="text-align:center" width="5%"><b>Kod GST</b></th>
                    <th style="text-align:right" width="8%"><b>GST (0%)</b></th> --}}
                    <th style="text-align:right" width="8%"><b>Jumlah sewaan (RM)</b></th>
                </tr> 
                <?php  
                    $i=1; 
                    $sumselepaseqp=0;
                    $gsteqpSR = 0;
                    $gsteqpSRI = 0;
                ?>
            @forelse($data['datatambahaneqp'] as $key => $value)
                <tr style="page-break-inside:avoid;">
                    <td width=""><?php echo $i ?></a></td>
                    <td width="">{{ Helper::tempahanSportDetail(Helper::get_et_booking_facility($value->fk_et_booking_facility)->fk_et_facility_detail) }}</td>
                    <td width="">{{ Helper::get_equipment($value->fk_et_equipment) }}</td>
                    <td width="" align="center">{{ Helper::date_format($value->eeb_booking_date) }}</td>
                    <td width="">{{ Helper::typeFunction($value->fk_et_function) }}</td>
                    <td width="" align="center">{{ $value->eeb_quantity }}</td>
                    <td width="" align="right">{{ $value->eeb_unit_price }}</td>
                    <td width="" align="center"><?php echo number_format( $value->eeb_discount_type_rm *  $value->eeb_quantity,2);?></td>
                    <td width="" align="right">
                        <?php
                        if(isset($value->eeb_special_disc_rm)) {
                            echo $value->eeb_special_disc_rm;
                        } else {
                            echo number_format(0, 2);
                        }
                        ?>
                    </td>
                    <td width="" align="right">{{ $value->eeb_total }}</td>
                    {{-- <td width="" align="center">{{ Helper::tempahanSportGST($value->fk_lkp_gst_rate) }}</td>
                    <td width="" align="right">{{ $value->eeb_gst_rm }}</td> --}}
                    <td width="" align="right">{{ $value->eeb_subtotal }}</td>
                </tr>
                    <?php $i++;?>
                    <?php 
                        $sumselepaseqp+= $value->eeb_total; 
                    ?>
                @empty
                    <tr>
                        <td colspan='11'>Tiada Data</td>
                    </tr>
                @endforelse    
            </table>
        <br>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
            @if($data['lkp_gst']->gst_status == 1)
                <tr style="page-break-inside: avoid">
                    <th align="left" width="50%"><b>GST (0%) </b></th>
                    <th width="36%" align="right">{{$gsteqpSR}}</th>
                    <th width="20%" align="center"><b>E</b></th>
                </tr>
                <tr style="page-break-inside: avoid">
                    <th align="left" width="50%" ><b>GST (0%) </b></th>
                    <th width="36%" align="right">{{$gsteqpSRI}}</th>
                    <th width="20%" align="center"><b>I</b></th>
                </tr>
            @endif
            <tr style="page-break-inside:avoid;">
                <th align="left" scope="row">Jumlah Sewaan Peralatan (RM)</th>
                <td align="right"><b><?php echo number_format(($sumselepaseqp),2)?></b></td>
                <td>&nbsp;</td>
            </tr>
            {{-- <tr style="page-break-inside: avoid">
                <th align="left" scope="row">Jumlah Sewaan Peralatan + GST (0%)(RM)</th>
                <td align="right"><b><?php echo number_format($sumselepaseqp+$gsteqpSR+$gsteqpSRI,2);?></b></td>
                <td>&nbsp;</td>
            </tr> --}}
        </table>
        @endif

            <br>
            <table id="title" width="100%" border="0" align="right"  cellpadding="2" cellspacing="0">
                <tr style="page-break-inside: avoid">
                <td>
                </td>
                </tr>
            </table>
            <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
         
                <tr style="page-break-inside: avoid">
                    <th width="50%" align="left"><b>Jumlah Keseluruhan ( RM )</b></th>
                    <th width="36%" align="right">{{ number_format($data['getbmbno']->bmb_subtotal,2) }}</th>
                    <td width="20%"></td>
                </tr>
                <tr style="page-break-inside:avoid;">
                    <td width="50%"><b>Pengenapan</b></td>
                    <?php $pengenapan=($data['getbmbno']->bmb_rounding)?>
                    <td width="36%" align="right"><b><?php echo number_format($pengenapan,2)?></b></td>
                    <td width="20%"></td>
                </tr>



                @if($data['getbmbno']->fk_lkp_status==9)
                    <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Telah Dibayar(RM)</b></td>
                        <td width="36%" align="right"><b><?php echo number_format($amount_paid,2)?></b></td>
                        <td width="20%"></td>
                        </tr>
                        <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                        <?php $jumbayar=($data['getbmbno']->bmb_subtotal-$data['getbmbno']->bmb_rounding)?>
                        <td width="36%" align="right"><b><?php echo number_format($bayaranpenuh,2)?></b></td>
                        <td width="20%"></td>
                    </tr>
                @else
                    <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                        <?php $jumbayar=($data['getbmbno']->bmb_subtotal-$data['getbmbno']->bmb_rounding)?>
                        <td width="36%" align="right"><b><?php echo number_format($jumbayar,2)?></b></td>
                        <td width="20%"></td>
                    </tr>
                @endif
            </table>
            <table id="footer" width="100%" border="0" cellpadding="10">
                <tr>
                    <td align="center" style="text-align:center"></td>
                    <td align="center" style="text-align:center">Sebut harga ini cetakan komputer, tiada tandatangan diperlukan.</td>
                </tr>
            </table>
<!DOCTYPE html>
<html>
    <head>
        <style>
            /** 
            * Define the width, height, margins and position of the watermark.
            **/
            #watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
                opacity: .2;
            }
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" height="100%" width="100%" />
        </div>

        <main> 
            <!-- The content of your PDF here -->

            <h2>Hello World</h2>
        </main>
    </body>
</html>
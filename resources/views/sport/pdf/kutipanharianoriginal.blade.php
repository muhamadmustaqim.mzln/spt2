<style>
    #body{
        font-size:10px;
      }
  </style>
  <div align="center">
    <b>PERBADANAN PUTRAJAYA</b>
  </div>
  <div align="center">
    <?php 
      // if(Auth::isAny(['Super Admin'])==true || Auth::isAny(['Administrator'])==true || Auth::isAny(['Multi Operasi'])==true)
      // {
    ?>
      @if(isset($_GET['id']) && $_GET['id']!='')
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN <?php echo strtoupper($location->lc_description) ?></b>
      @else
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN</b>
      @endif
    <?php 
      // }
      // else
      // {
    ?>
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN <?php echo strtoupper($location->lc_description) ?></b>
    <?php 
      // }
    ?>
  </div>
  <div>&nbsp;</div>
  <table style="width:100%;padding-top:50px;text-align:center">
  <tr>
  <td style="width:48%;border:0px" id="body">
      @if(isset($_GET['tarikh'])) 
        @if($_GET['tarikh']!='')
          <b>TARIKH KUTIPAN : <?php 
          setlocale(LC_TIME, 'ms');
          $tarikh=strtotime($_GET['tarikh']);
          echo strtoupper(strftime("%d %B %Y",$tarikh));
          ?></b>
        @endif
      @else
        <b>TARIKH KUTIPAN : <?php echo date('d/m/Y');?></b>
      @endif
    </td>
    <td style="width:48%;text-align:center;border:0px" id="body">
      @if(isset($_GET['tarikh'])) 
        @if($_GET['tarikh']!='')
          <b>HARI KUTIPAN : <?php           
          echo strtoupper(strftime("%A",$tarikh));
          ?></b>
        @endif
      @else
        <b>HARI KUTIPAN : <?php 
        setlocale(LC_TIME, 'ms');
        echo strtoupper(strftime("%A",date('d')));
        ?></b>
      @endif
    </td>
  </tr>
  </table>
  <div>&nbsp;</div>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body"> 
  <?php 
    $a = URL::current();
    $url = Request::fullUrl();
    $trim = str_replace($a,'', $url); 
    $equip=array("Resit"=>"-","Name"=>"-","Mode"=>"-");
  ?>
  <thead>
    <tr>
      <th nowrap style="text-align:center;vertical-align: middle;" rowspan=2><b>Bil.</b></th>
      <th nowrap style="text-align:center;vertical-align: middle;" rowspan=2><b>No. Resit</b></th>
      <th nowrap style="text-align:center;vertical-align: middle;" rowspan=2><b>Nama Pembayar</b></th>
      <th nowrap style="text-align:center;vertical-align: middle;" rowspan=2><b>Mod Bayaran</b></th>
      <th nowrap style="text-align:center" colspan={{ count($kutipanharianheader) }}><b>Kod Hasil</b></th>
    </tr>
    <tr>
      @foreach($kutipanharianheader as $header)
      <th nowrap style="text-align:center"><b>{{ $header->efd_fee_code }}</b><br/>{{ $header->efd_name }}</th>
      <?php 
        $column = $header->efd_name;
        $equip[$column] = "0.00"; 
      ?>
      @endforeach
    </tr>
  </thead>
  <?php
    $count = 0;
    $name = "-";
    $newName = 1;
  ?>
  @foreach($kutipanharian as $data)                      
    @if($name == $data->bud_name)
      <?php
        $equip[$data->efd_name] = $data->total;
      ?>
    @else    
      <?php
        if ($count == 0 || $newName == 1)
        {
          $name = $data->bud_name;
          $equip["Name"] = strtoupper($name);
          $equip["Mode"] = $data->lpm_description;
          $equip["Resit"] = $data->bp_receipt_number;
          $equip[$data->efd_name] = $data->total;
          $count += 1;
          $newName = 0;
        }
        else
        {
          $newName = 2; 
        }
  
        if ($newName == 2)
        {
          echo "<tr>";
          echo "<td style='text-align:center;padding:5px'>".$count."</td>";                  
          foreach($equip as $key => $value) {
            switch ($key) {
                case "Name":
                    echo "<td style='padding:5px'>";
                    print_r($value);
                    echo "</td>";
                    break;
                case "Mode":
                    echo "<td style='text-align:center'>";
                    print_r($value);
                    echo "</td>";
                    break;
                case "Resit":
                    echo "<td style='padding:5px'>";
                    print_r($value);
                    echo "</td>";
                    break;
                default:
                    echo "<td style='text-align:right;padding:5px'> RM ";
                    print_r($value);
                    echo "</td>";
            }
            
            $equip[$key] = "0.00";
          }
          echo "</tr>";
  
          $name = $data->bud_name;
          $equip["Name"] = strtoupper($name);
          $equip["Mode"] = $data->lpm_description;
          $equip["Resit"] = $data->bp_receipt_number;
          $equip[$data->efd_name] = $data->total;
          $count += 1;
          $newName = 0;
        }   
      ?>
    @endif          
  @endforeach
  <?php
    if($count > 0)
    {
      echo "<tr>";
      echo "<td style='text-align:center;padding:5px'>".$count."</td>";                  
      foreach($equip as $key => $value) {
        switch ($key) {
            case "Name":
                echo "<td style='padding:5px'>";
                print_r($value);
                echo "</td>";
                break;          
            case "Mode":
                echo "<td style='text-align:center'>";
                print_r($value);
                echo "</td>";
                break;
            case "Resit":
                echo "<td style='padding:5px'>";
                print_r($value);
                echo "</td>";
                break;
            default:
                echo "<td style='text-align:right;padding:5px'> RM ";
                print_r($value);
                echo "</td>";
        }
  
        $equip[$key] = "0.00";
      }
      echo "</tr>";  
      echo "<tfoot>";
      echo "<tr>";
      echo "<td class='th-sortable active' style='text-align:center;padding:5px' colspan=4><b>Jumlah</b></td>";
      $sumTotal = 0.00;
      foreach($kutipanharianheader as $header)
      {
        $sumTotal += $header->total;
        echo "<td class='th-sortable active' style='text-align:right'><b>RM ".$header->total."</b></td>";
      }
      echo "</tr>";
      echo "</tfoot>";          
    }
    else
    {
      echo "<tr><td colspan='5' style='text-align:center'>Tiada data</td></tr>";
    }
  ?>          
  </table>
  <?php
    echo "<div style='width:100%; text-align:center; padding:10px'><strong>JUMLAH KESELURUHAN: RM ".number_format($sumTotal, 2)."</strong></div>";
  ?>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
      <tr>
        <th nowrap style="width:25%; text-align:center;">
          Disediakan Oleh:
          <br/>(PETUGAS KAUNTER)
        </th>
        <th nowrap style="width:25%; text-align:center;">
          Disemak Oleh:
          <br/>(PENYELIA OPERASI)
        </th>
        <th nowrap colspan=2 style="width:50%; text-align:center;">
          &nbsp;
        </th>
      </tr>
      <tr>
        <td rowspan=3 style="vertical-align:top">
        <div style="padding:5px">
        <?php
          date_default_timezone_set('Asia/Kuala_Lumpur');
          echo "<strong>Tarikh Cetakan:</strong> ".date('d/m/Y H:i A');
        ?>
        </div>
        &nbsp;
        </td>
        <td rowspan=3>
        &nbsp;
        </td>
        <td>
          <br/>
          TEMPOH LEWAT SERAH: ................hari
          <br/>(Diisi Oleh Petugas Kaunter Kewangan)
          <br/><br/>
        </td>
        <td>
          &nbsp;
        </td>
      </tr>
      <tr>
        <td style="width:25%; text-align:center;">
          DISERAH OLEH:
        </th>
        <td style="width:25%; text-align:center;">
          &nbsp;
        </td>
      </tr>
      <tr>
        <td style="padding:60px">&nbsp;</td>
        <td style="padding:60px">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center;">NAMA & TANDATANGAN</td>
        <td style="text-align:center;">NAMA & TANDATANGAN</td>
        <td style="text-align:center;">NAMA & TANDATANGAN</td>
        <td style="text-align:center;">NAMA & TANDATANGAN</td>
      </tr>
      <tr>
        <td style="text-align:center;">{{ date('d/m/Y') }}</td>
        <td style="text-align:center;">{{ date('d/m/Y') }}</td>
        <td style="text-align:center;">{{ date('d/m/Y') }}</td>
        <td style="text-align:center;">{{ date('d/m/Y') }}</td>
      </tr>
    </table>
  <?php 
    $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = str_replace("&","|",$url);
  ?>
  </div>
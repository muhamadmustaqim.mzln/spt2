<!DOCTYPE html>
<html>
    <head>
        <title>Resit Online</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>

        @page {
                margin-top: 25px;
            }
            body{
                font-family: arial, sans-serif;
            }

            .kosong{
                margin-bottom: 0;
                font-size: 9px;
            }

            .standard{
                font-size: 11px;
                margin-bottom: 0;
            }
            .tajuk{
                font-size: 14px;
            }

            .table td, .table th{
                border-top: 0;
            }
            .buang{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
                padding-left: 3px !important;
            }

            .buang1{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
            }
        </style>
    </head>
    <body>
        <main> 
            <table class="table">
                <tbody>
                    <tr>
                        <td style="text-align: center; width: 10%">
                            <img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" alt="" style="width: 60px; height: 70px">
                        </td>
                        <td style="font-size: small;">
                            <p class="kosong"><b>PERBADANAN PUTRAJAYA</b> </p>
                            <p class="kosong" style="text-transform: uppercase;"><b>{{ $location->name }}</b> </p>
                            <p class="kosong">62300, WP-PUTRAJAYA</p>
                            {{-- <p class="kosong">Tel: {{ $location->lc_contact_no }}</p>
                            <p class="kosong">Fax: {{ $location->lc_fax_no }}</p> --}}
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <table class="table table-bordered" style="background-color:#dee2e6">
                <tbody>
                    <tr>
                        <th class="tajuk" style="text-align: center">
                            RESIT RASMI
                        </th>
                    </tr>
                </tbody>
            </table>
            <table class="table">
                <tbody>
                    <tr class="standard">
                        <th class="buang" style="width: 20%">MOD :</th>
                        <td class="buang">ONLINE</td>
                        <th class="buang" style="width: 20%">Tarikh Transaksi :</th>
                        <td class="buang">{{ $online->fpx_date }}</td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">BANK :</th>
                        <td class="buang">{{ $online->bank }}</td>
                        <th class="buang">No Transaksi FPX :</th>
                        <td class="buang">{{ $online->fpx_trans_id }}</td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">Nama Pelanggan :</th>
                        <td class="buang">{{ $user->fullname }}</td>
                        <th class="buang">No K/P Pelanggan :</th>
                        <td class="buang">{{ $user_detail->bud_reference_id }}</td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">No. Tempahan :</th>
                        <td class="buang">{{ $main->bmb_booking_no }}</td>
                        <th class="buang">Jumlah Bayaran :</th>
                        <td class="buang">RM {{ $online->amount_paid }}</td>
                    </tr>
                </tbody>
            </table>
            <p class="standard">KETERANGAN: </p>
            <p class="standard">BAYARAN PENUH BAGI PENGGUNAAN {{ strtoupper($location->name) }}</p>
            {{-- <p class="standard">BAYARAN PENUH BAGI PENGGUNAAN DEWAN / FASILITI KOMPLEKS KEJIRANAN {{ strtoupper($location->lc_description) }}</p> --}}
            <table class="table table-bordered">
                <thead>
                    <tr class="standard">
                        <th>Bil</th>
                        <th>Nama Fasiliti</th>
                        <th>Tarikh Penggunaan</th>
                        {{-- <th>Slot Masa</th> --}}
                        <th>Harga</th>
                        <th>Kod GST</th>
                        <th>GST (RM)</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                        $total = 0.00;
                    @endphp
                    {{-- @foreach($slot as $s) --}}
                    <tr class="standard">
                        <td class="buang1" style="width: 2%; text-align: center">{{ $i++ }}</td>
                        <td class="buang1">{{ ($location->name) }}</td>
                        <td class="buang1">{{($slot->event_date)}} - {{($slot->event_date_end)}}</td>
                        {{-- <td class="buang1">{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td> --}}
                        <td class="buang1" style="text-align: right">{{ number_format(floatval($location->rent_perday) / 2, 2) }}</td>
                        <td class="buang1" style="text-align: right"></td>
                        <td class="buang1" style="text-align: right">0</td>
                        <td class="buang1" style="text-align: right">{{ number_format(floatval($location->rent_perday) / 2, 2) }}</td>
                        @php
                            $total += floatval($location->rent_perday) / 2;
                        @endphp
                    </tr>
                    {{-- @endforeach --}}
                </tbody>
            </table>
            <table class="table table-bordered" style="float: right">
                <tbody>
                    <tr class="standard">
                        <th colspan="2">Rumusan Harga</th>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Jumlah Keseluruhan (RM)</th>
                        <td class="buang1" style="text-align: right">{{ $main->bmb_subtotal }}</td>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Pengenapan</th>
                        <td class="buang1" style="text-align: right">{{ $main->bmb_rounding }}</td>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Jumlah Keseluruhan Telah Dibayar (RM)</th>
                        <td class="buang1" style="text-align: right">{{ $online->amount_paid }}</td>
                    </tr>
                </tbody>
            </table>
            <p class="kosong">NOTA : </p>
            <ol class="kosong">
                <li>Bayaran Deposit sebanyak adalah TIDAK TERMASUK jumlah bayaran tempahan</li>
                <li>Kegagalan membayar jumlah tempahan dalam tempoh empat belas (14) hari sebelum tarikh penggunaan akan menyebabkan tempahan anda dibatalkan.</li>
                <li>Resit Deposit/ Cagaran hendaklah disimpan dengan selamat dan dikemukakan semula apabila membuat tuntutan balik deposit/ cagaran.
                </li>
            </ol>
        </main>
    </body>
</html>
<!DOCTYPE html>
<html>
<head>
    <style>
        #body {
            font-size: 10px;
        }
    </style>
</head>
<body>
    <div align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div align="center">
        @if(isset($data['id']) && $data['id']!='')
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN {{ strtoupper(Helper::location($data['id'])) }}   </b>
        @else
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN</b>
        @endif
    </div>
    <div>&nbsp;</div>
    <table style="width:100%;padding-top:50px;text-align:center">
        <tr>
            <td style="width:48%;border:0px" id="body">
                @if(isset($data['tarikh'])) 
                @if($data['tarikh']!='')
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d F Y', strtotime($data['tarikh']))) }}</b>
                @endif
                @else
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d/m/Y')) }}</b>
                @endif
            </td>
            <td style="width:48%;text-align:center;border:0px" id="body">
                @if(isset($data['tarikh'])) 
                @if($data['tarikh']!='')
                <b>HARI KUTIPAN : {{ strtoupper(date('l', strtotime($data['tarikh']))) }}</b>
                @endif
                @else
                <b>HARI KUTIPAN : {{ strtoupper(date('l')) }}</b>
                @endif
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body"> 
        <thead>
            <tr>
                <th nowrap style="text-align:center;vertical-align: middle;"><b>Bil.</b></th>
                <th nowrap style="text-align:center;vertical-align: middle;"><b>No. Resit</b></th>
                <th nowrap style="text-align:center;vertical-align: middle;"><b>Nama Pembayar</b></th>
                <th nowrap style="text-align:center;vertical-align: middle;"><b>Mod Bayaran</b></th>
                <th nowrap style="text-align:center;vertical-align: middle;">Kod Hasil & Gelanggang</th>
                <th nowrap style="text-align:center;vertical-align: middle;">Jumlah (RM)</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i = 1;  
            $total = 0;
            @endphp
            @if (count($data['harian']) > 0)
            @foreach ($data['harian'] as $harian)
            <tr>
                <td style="width: 5%;">{{ $i++ }}</td>
                <td>
                    @if(($harian->lpm_description) === 'LO/PO')
                    {{ $harian->bp_receipt_number }}
                    @elseif(($harian->lpm_description) === 'FPX')
                    {{ Helper::getEtPaymentFpx($harian->id) }}
                    @endif
                </td>
                <td>{{ $harian->fullname }}</td>
                <td>{{ $harian->lpm_description }}</td>
                <td>{{ $harian->efd_fee_code }}
                  <br>{{ $harian->efd_name }}</td>
                  <td class="text-right">{{ $harian->ebf_subtotal }}</td> 
              </tr>
              @php
              $total += $harian->ebf_subtotal;
              @endphp
              @endforeach
              @else
              <tr>
                  <td colspan="6" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
              </tr>
              @endif
          </tbody>
          <tfoot>
              <tr class="fw-bold fs-6 bg-dark">
                  <td colspan="5" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                  <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
              </tr>
          </tfoot>
      </table>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
          <tr>
              <th nowrap style="width:25%; text-align:center;">
                  Disediakan Oleh:
                  <br/>(PETUGAS KAUNTER)
              </th>
              <th nowrap style="width:25%; text-align:center;">
                  Disemak Oleh:
                  <br/>(PENYELIA OPERASI)
              </th>
              <th nowrap colspan=2 style="width:50%; text-align:center;">
                  &nbsp;
              </th>
          </tr>
          <!-- Rest of the table -->
          {{-- <tr>
            <th class="th-sortable active" style="width:25%; text-align:center;">
              Disediakan Oleh:
              <br/>(PETUGAS KAUNTER)
            </th>
            <th class="th-sortable active" style="width:25%; text-align:center;">
              Disemak Oleh:
              <br/>(PENYELIA OPERASI)
            </th>
            <th class="th-sortable active" colspan=2 style="width:50%; text-align:center;">
              &nbsp;
            </th>
          </tr>
          <tr> --}}
            <td rowspan=3>
            <?php
              date_default_timezone_set('Asia/Kuala_Lumpur');
              echo "<strong>Tarikh Cetakan:</strong> ".date('d/m/Y H:i A');
            ?>
            &nbsp;
            </td>
            <td rowspan=3>
            &nbsp;
            </td>
            <td>
              <br/>
              TEMPOH LEWAT SERAH: ................hari
              <br/>(Diisi Oleh Petugas Kaunter Kewangan)
              <br/><br/>
            </td>
            <td>
              &nbsp;
            </td>
          </tr>
          <tr>
            <td style="width:25%; text-align:center;">
              DISERAH OLEH:
            </th>
            <td style="width:25%; text-align:center;">
              &nbsp;
            </td>
          </tr>
          <tr>
            <td style="padding:60px">&nbsp;</td>
            <td style="padding:60px">&nbsp;</td>
          </tr>
          <tr>
            <td style="text-align:center;">NAMA & TANDATANGAN</td>
            <td style="text-align:center;">NAMA & TANDATANGAN</td>
            <td style="text-align:center;">NAMA & TANDATANGAN</td>
            <td style="text-align:center;">NAMA & TANDATANGAN</td>
          </tr>
          <tr>
            <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
            <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
            <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
            <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
          </tr>
      </table>
      <!-- Footer or any other content -->
  </body>
  </html>
  
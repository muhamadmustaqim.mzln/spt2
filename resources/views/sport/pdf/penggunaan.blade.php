<!DOCTYPE html>
<html>
<head>
    <style>
        #body {
            font-size: 10px;
        }
    </style>
</head>
<body>
    <table align="right" width="15%" id="body">
        <tr>
            <td><strong>Tarikh</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo date("d-m-Y"); ?></td>
        </tr>
        <tr>
            <td><strong>Masa</strong></td>
            <td><strong>:</strong></td>
            <td><?php echo date("h:i"); ?></td>
        </tr>
    </table>
    <br>
    <div>&nbsp;</div>
    <div align="center">
      <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div align="center">
        @if(isset($data['id']) && $data['id']!='')
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN {{ strtoupper(Helper::location($data['id'])) }}   </b>
        @else
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN</b>
        @endif
    </div>
    <div>&nbsp;</div>
    <table style="width:100%;padding-top:50px;text-align:center">
        <tr>
            <td style="width:48%;border:0px" id="body">
                @if(isset($data['tarikh'])) 
                @if($data['tarikh']!='')
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d F Y', strtotime($data['tarikh']))) }}</b>
                @endif
                @else
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d/m/Y')) }}</b>
                @endif
            </td>
            <td style="width:48%;text-align:center;border:0px" id="body">
                @if(isset($data['tarikh'])) 
                @if($data['tarikh']!='')
                <b>HARI KUTIPAN : {{ strtoupper(date('l', strtotime($data['tarikh']))) }}</b>
                @endif
                @else
                <b>HARI KUTIPAN : {{ strtoupper(date('l')) }}</b>
                @endif
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    @if ($data['search_type'] == 1) {{--  Tarikh --}}
        <table class="table table-bordered table-hover table-checkable" id="myTable">
            <thead> 
                <tr>
                    <th>Bil</th>
                    <th class="text-center">Penggunaan</th>
                    <th class="text-center">Jumlah</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $first_day_this_month = date('01-m-Y');
                    $last_day_this_month  = date('t-m-Y');
                    $defaultrange = [date('Y-m-d', strtotime($first_day_this_month)), date('Y-m-d', strtotime($last_day_this_month))];
                    if (isset($data['start']) || isset($data['end'])) {
                        $range = [date('Y-m-d', strtotime($data['start'])), date('Y-m-d', strtotime($data['end']))];
                        $tarikh = ($data['start'] != '' || $data['end'] != '') ? $range : $defaultrange;
                    } else {
                        $tarikh = $defaultrange;
                    }
                    $sum = 0;
                    $sum_amaun = 0.00;
                @endphp
                @if ($data['usage_type'] == 1)
                    @foreach ($data['list_facility'] as $key => $s)
                        @php
                            $data_et_hall_sport = Helper::laporan_penggunaan($tarikh, $key);
                        @endphp
                        <tr>
                            <td style="width: 5%">{{ $i++ }}</td>
                            <td>{{ $s }}</td>
                            <td>
                                @if ($data_et_hall_sport[0]->bil_hall + $data_et_hall_sport[1]->bil_sport == 0)
                                    0 (RM0.00)
                                @else
                                    {{ ($data_et_hall_sport[0]->bil_hall + $data_et_hall_sport[1]->bil_sport) }} (RM {{ Helper::moneyhelper($data_et_hall_sport[0]->amaun + $data_et_hall_sport[1]->amaun) }})
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    @php
                        $ebf = Helper::laporan_penggunaan_peralatan($tarikh);
                    @endphp
                    @foreach ($data['list_equipment'] as $key => $s)
                        @php
                            $data_et_equipment = Helper::laporan_penggunaan_peralatan2($tarikh, $key, $ebf);
                        @endphp
                        <tr>
                            <td style="width: 5%">{{ $i++ }}</td>
                            <td>{{ Helper::get_equipment($s) }}</td>
                            <td align="left">
                                @if ($data_et_equipment->bil_eqp == 0)
                                    0 (RM 0.00)
                                @else
                                    {{ $data_et_equipment->bil_eqp }} (RM <?php echo number_format($data_et_equipment->amaun, 2); ?>)
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @else {{--  Tahun --}}
        <table class="table table-bordered table-hover table-checkable" id="myTable">
            @php
                $months = [1 => 'Januari', 2 => 'Februari', 3 => 'March', 4 => 'April',
                          5 => 'Mei', 6 => 'Jun', 7 => 'Julai', 8 => 'Ogos', 9 => 'September', 10 => 'Oktober',
                          11 => 'November', 12 => 'Disember'];
            @endphp
            <thead> 
                <tr>
                    <th>Bil</th>
                    <th class="text-center">Penggunaan</th>
                    @foreach ($months as $num => $name)
                        <th width="18%"><b>{{ $name }}</b></th>
                    @endforeach
                    <th class="text-center">Jumlah</th>
                </tr>
            </thead>
            <tbody>
                @if ($data['usage_type'] == 1)
                    @foreach ($data['list_facility'] as $key => $s)
                        <tr>
                            <td><b>{{ $i++ }}</b></td>
                            <td><b>{{ $s }}</b></td>
                            @foreach ($months as $num => $name)
                                @php
                                    $data_facility_year = Helper::laporan_penggunaan_tahun($num, $year, $key);
                                @endphp
                                <td align="right">
                                    @if ($data_facility_year[0]->bil_hall + $data_facility_year[1]->bil_sport == 0)
                                        0 (RM 0.00)
                                    @else
                                        {{ $data_facility_year[0]->bil_hall + $data_facility_year[1]->bil_sport }} (RM <?php echo number_format($data_facility_year[0]->amaun + $data_facility_year[1]->amaun, 2); ?>)
                                    @endif
                                </td>
                            @endforeach
                            <td align="right">
                                @if ($data_facility_year[2]->bil_hall_year + $data_facility_year[3]->bil_sport_year == '')
                                    0 (RM 0.00)
                                @else
                                    {{ $data_facility_year[2]->bil_hall_year + $data_facility_year[3]->bil_sport_year }} (RM <?php echo number_format($data_facility_year[2]->amaun_year + $data_facility_year[3]->amaun_year, 2); ?>)
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    @php
                        $ebf = Helper::laporan_penggunaan_peralatan_tahun('', '', '', '', 1);
                    @endphp
                    @foreach ($data['list_equipment'] as $key => $s)
                        <tr>
                            <td><b>{{ $i++ }}</b></td>
                            <td><b>{{ Helper::get_equipment($s) }}</b></td>
                            @foreach ($months as $num => $name)
                                @php
                                    $data_facility_year = Helper::laporan_penggunaan_peralatan_tahun($num, $data['year'], $key, $ebf, 2);
                                @endphp
                                <td align="right">
                                    @if ($data_facility_year[0]->bil_eqp == 0)
                                        0 (RM 0.00)
                                    @else
                                        {{ $data_facility_year[0]->bil_eqp }} (RM <?php echo number_format($data_facility_year[0]->amaun, 2); ?>)
                                    @endif
                                </td>
                            @endforeach
                            <td align="right">
                                @if ($data_facility_year[1]->bil_eqp_year == '')
                                    0 (RM 0.00)
                                @else
                                    {{ $data_facility_year[1]->bil_eqp_year }} (RM <?php echo number_format($data_facility_year[1]->bil_eqp_year, 2); ?>)
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @endif
</body>
</html>
<style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:10px;
    }
      #title2{
      font-size:8px;
    }
     #footer{
      font-size:8px;
    }
    #back{
  
   width:100%; 
   height:99%; 
  /*  background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
    background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }
   #back1{
  
   width:100%; 
   height:99%; 
  /*   background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
    background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat:repeat;
  
      
  }
  
    /* Force all children to have a specified font-size */
  </style>
  <div id="back">
  <table  width="100%" border="0" cellpadding="0">
    <tr>
      <td align="right" id="footer">
      </td>
      <td>
      </td>
      <td align="right" id="footer">BPSK-1/2015
      </td>
    </tr>
  <tr>
  <td width="20%"><img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" width="120" height="127" />
  <!--
  <td width="20%"><img src="https://www.ppj.gov.my/web/images/inverted-logo.png" width="120" height="127" />
  <td width="20%"><img src="{{asset('packages/threef/entree/img/logo.png')}}" width="120" height="127" />
  -->
   <!-- 
  <td width="20%"><img src="{{public_path('images/logo.png')}}" width="120" height="127" />
  -->   
  </td>
    {{-- <td>
      &nbsp;
    </td> --}}
    <td>
     <table>
      <tr>
        <td id="title"><strong>PERBADANAN PUTRAJAYA </strong>
        </td>
      </tr>
       <tr>
        <td id="title"><strong>KOMPLEKS KEJIRANAN <?php echo strtoupper(Helper::location($data['getbmbno']->fk_lkp_location))?></strong>
        </td>
      </tr>
      <tr>
          <td id="title"><strong>{{Helper::location_data($data['getbmbno']->fk_lkp_location)->lc_addr}},{{Helper::location_town($data['getbmbno']->fk_lkp_location)}}</strong>
        </td>
      </tr>
      <tr>
          <td id="title"><strong>{{Helper::location_data($data['getbmbno']->fk_lkp_location)->lc_addr_postcode}},{{Helper::negeri(Helper::location_data($data['getbmbno']->fk_lkp_location)->fk_lkp_state)}}</strong>
        </td>
      </tr>
       <tr>
          <td id="title"><strong>Tel: {{Helper::location_data($data['getbmbno']->fk_lkp_location)->lc_contact_no}} / 03-8000 8000</strong>
        </td>
      </tr>
       <tr>
          <td id="title"><strong>Fax: {{Helper::location_data($data['getbmbno']->fk_lkp_location)->lc_fax_no}}</strong>
        </td>
      </tr>
     </table>
    </td>
  
  </tr>
  
  </table>
  <table id="title" width="100%" border="0" cellpadding="0">
  <tr>
    <td><center><strong>BORANG PERMOHONAN TEMPAHAN PERKHIDMATAN/PROGRAM/AKTIVITI/MAJLIS/SEWAAN DAN LAIN-LAIN</strong></center></td>
  </tr>
    <tr>
    <td><center>(Tempahan melalui: Surat / Telefon / Faks / E-Mel / Kaunter)</center></td>
  </tr>
  </table>
  <table id="title" width="100%" border="1" cellpadding="5" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>A. BUTIRAN PEMOHON</strong>
                  </td>
                </tr>
  </table>
  <br> 
  <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
  @if($data['getbmbno']->internal_indi!=1 && $data['getbmbno']->internal_indi!=2)
                <tr>
                  <td width="15%"><b>No Tempahan</b></td>
                  <td>:&nbsp;{{$data['getbmbno']->bmb_booking_no}}</td>
                  <td width="15%"><b>Jenis Tempahan</b></td>
                  <td>: @if($data['getbmbno']->fk_lkp_discount_type=='')
                    @else
                      {{Helper::get_permohonan($data['getbmbno']->fk_lkp_discount_type)}}
                    @endif
                  </td>
                </tr>
  
                <tr>
                  <td width="15%"><b>Nama Pemohon</b></td>
                  <td >:
                  @if($data['getbmbno']->internal_indi!=1 && $data['getbmbno']->internal_indi!=2)
                      &nbsp;<?php echo strtoupper($data['user_profiles']->bud_name)?>
                    @else
                      &nbsp;<?php echo strtoupper($ebfd_user_apply->ebfd_user_apply)?>
                    @endif
                  </td>
                  <td width="15%"><b>Email</b></td>
                  <td style="white-space:nowrap">: {{$data['user']->email}}</td>
                </tr>
  
                <tr>
                  <td width="15%"><b>No.Kad Pengenalan / No. Pendaftaran Syarikat</b></td>
                  <td style="white-space:nowrap">: {{$data['user_profiles']->bud_reference_id}}</td>
                  <td width="15%"><b>No. Telefon (P)</b></td>
                  <td style="white-space:nowrap">: {{$data['user_profiles']->bud_office_no}}</td>
                </tr>
                
                <tr>
                  <td width="15%"><b>Alamat Pemohon</b></td>
                  <td style="white-space:nowrap">:&nbsp;<?php echo strtoupper($data['user_profiles']->bud_address)?>,
                    <br>&nbsp;<?php echo strtoupper($data['user_profiles']->bud_town)?>,
                    <?php echo strtoupper($data['user_profiles']->bud_poscode)?>,
                    <?php echo strtoupper(Helper::negeri($data['user_profiles']->fk_lkp_state))?>,
                    <?php echo strtoupper(Helper::negara($data['user_profiles']->fk_lkp_country))?>
                  </td>
                   <td width="15%"><b>No. Telefon (H/P)</b></td>
                  <td style="white-space:nowrap">: {{$data['user_profiles']->bud_phone_no}}</td>
                </tr>
              
                 <tr>
                  <td width="15%"></td>
                  <td>&nbsp;</td>
                  <td width="15%"><b>No. Faks</b></td>
                  <td style="white-space:nowrap">: {{$data['user_profiles']->bud_fax_no}}</td>
                </tr>
                  
                 
                  <tr>
                  <td width="15%"></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="15%"></td>
                  <td></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
               
  
  @else
                <tr>
                  <td width="20%"><b>No Tempahan</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['getbmbno']->bmb_booking_no}}</td>
                  <td width="15%"><b>Nama Pemohon</b></td>
                  <td>:</td>
                  @if($data['getbmbno']->internal_indi != 1 && $data['getbmbno']->internal_indi != 2)
                  <td>&nbsp;{{ strtoupper($data['user_profiles']->bud_name) }}</td>
                  @else
                  <td>&nbsp;{{ strtoupper($data['ebfd'][0]->ebfd_user_apply ?? '' ) }}</td>
                  @endif
                </tr>
                <tr>
                  <td width="15%"><b>No. Telefon (H/P)</b></td>
                  <td>:</td>
                   <?php $et_booking_facility_tel= DB::table('et_booking_facility')
                                                   ->selectRaw('ebfd_contact_no')
                                                   ->leftJoin('et_booking_facility_detail','et_booking_facility_detail.fk_et_booking_facility','=','et_booking_facility.id')
                                                    ->where('et_booking_facility.fk_main_booking',$data['getbmbno']->id)
                                                    ->first();
  
                    ?>
                  <td>&nbsp;{{$et_booking_facility_tel->ebfd_contact_no}}</td>
                  <td width="15%"><b>Nama Pegawai</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['user_profiles']->bud_name}}</td>
                </tr>
              
               
               
  
                  
   @endif
               
           
              </table>
  
     <table id="title" width="100%" border="1" cellpadding="5" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>B. SENARAI PERKHIDMATAN / TAPAK / PERALATAN / KELENGKAPAN YANG DIPERLUKAN</strong>
                  </td>
                </tr>
  </table>
  <br> 
  @if($data['ebf']->first()->ebf_facility_indi == 1)
    <div id="title"><b>TUJUAN TEMPAHAN</b></div>
  
    <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
      <thead>
        <th width="2%" align="center"><b>Bil.</b></th>
        <th width="17%"align="center"><b>Item</b></th>
        <th width="12%" align="center"><b>Tarikh Mula</b></th>
        <th width="12%" align="center"><b>Tarikh Tamat</b></th>
        <th width="12%" align="center"><b>Tujuan Tempahan</b></th>
      </thead>
      <?php  
        $i=1; 
      ?>
      @forelse($data['ebf'] as $key => $value)
        <tr>
          <td width="10px" align="center"><?php echo $i ?></a></td>
          <td width="50px" align="left">{{ Helper::tempahanSportDetail($value->fk_et_facility_detail) }}</a></td>
          <td width="50px" align="center">{{ Helper::date_format($value->ebf_start_date) }}</a></td>
          <td width="50px" align="center">{{ Helper::date_format($value->ebf_end_date) }}</a></td>
          @if((Helper::get_maklumat_tempahan_terperinci($value->id)) =='' || (Helper::get_maklumat_tempahan_terperinci($value->id)) ==13)
              <td width="50px" align="left"></a></td>
          @else
              <td width="50px" align="center">{{ Helper::get_lkp_event(Helper::get_kemudahan_terperinci($value->id)->fk_lkp_event) }}</a></td>
          @endif 
          {{-- @if(is_object($data['ebfd']) && !empty($data['ebfd']->fk_lkp_event) && Helper::get_et_booking_facility($data['ebfd']->fk_lkp_event))
                <td width="50px" align="left">{{ Helper::get_et_booking_facility($data['ebfd']->fk_lkp_event) }}</td>
          @else
              <td width="50px" align="left"></td>
          @endif --}}
        </tr>
        <?php $i++;?>
        @empty
          <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
      @endforelse
    </table>
  
  @endif
  <div id="title"><b>Kadar Sewaan Bangunan</b></div>
  
  <table id="title" idth="100%" border="1" cellpadding="2" cellspacing="0">
    <thead>
      <tr>
        <th width="1%" align="center"><b>Bil.</b></th>
        <th width="6%" align="center"><b>Nama Dewan/ Fasiliti</b></th>
        <th width="5%" align="center"><b>Tarikh</b></th>
        <th width="5%" align="center"><b>Jenis Penggunaan</b></th>
        <th width="5%" align="center"><b>Slot Masa</b></th>
        <!-- <th width="12%" align="center"><b>{{ trans('sportfinance::sportfinance.bayarandetail.kuantiti') }}</b></th> -->
        <th width="5%" align="center"><b>Harga (RM)</b></th>
        <th width="5%" align="center"><b>Diskaun Kategori (RM)</b></th>
        <th width="5%" align="center"><b>Diskaun Khas (RM)</b></th>
        <th width="5%" align="center"><b>Harga(RM)</b><br><font size="1">*selepas diskaun</font></th>
        {{-- <th width="5%" align="center"><b>Kod GST</b></th>
        <th width="5%" align="center"><b>GST (0%)</b></th> --}}
        <th width="5%" align="center"><b>Jumlah Sewaan (RM)</b></th>
      </tr>
    </thead>
    <?php  
      $a=1;
      $sumselepas=0;
    ?>
  
      {{-- @forelse($datatambahan as $key => $value) --}}
    @if($data['ebf']->first()->ebf_facility_indi == 1)
      @foreach ($data['eht'] as $value)
        <tr style="page-break-inside:avoid;">
          <td width="5px"><?php echo $a ?></a></td>
          <td width="">{{ Helper::tempahanSportDetail($data['ebf'][0]->fk_et_facility_detail) }}</td>
          <td width="" align="center"><?php echo date('d-m-Y',strtotime(Helper::hall_booking($value->fk_et_hall_book)->ehb_booking_date)) ?></td>
          <td width="">{{ Helper::typeFunction(Helper::hall_booking($value->fk_et_hall_book)->fk_et_function) }}</td>
          <td width="">{{ Helper::get_slot_masa($value->fk_et_slot_time) }}</td>
          <td width="" align="right">{{ $value->eht_price }}</td>
          <td width="" align="right">{{ $value->eht_discount_type_rm ?? '0.00' }}</td>
          <td width="" align="right">{{ $value->eht_special_disc_rm ?? '0.00' }}</td>
          {{-- <!--  <td width="200px" align="center">{{ $value->ethallbook->etbookingfacility->ebf_no_of_day }}</td> --> --}}
          <td width="" align="right">{{ $value->eht_total }}</td>
          {{-- <td width="" align="center">{{ Helper::tempahanSportGST($value->eht_gst_code) }}</td>
          <td width="" align="right">{{ $value->eht_gst_rm }}</td>  --}}
          <td width="" align="right">{{ $value->eht_subtotal }}</td>
        </tr>
        @php
            $sumselepas += $value->eht_subtotal;
        @endphp
      @endforeach
    @else
        @foreach ($data['est'] as $key)
            <tr style="page-break-inside:avoid;">
                <td width="5%"><?php echo $a ?></a></td>
                <td width="20%">{{ Helper::tempahanSportDetail($data['ebf'][0]->fk_et_facility_detail) }}</td>
                <td width="20%" align="center"><?php echo date('d-m-Y',strtotime(Helper::sport_booking($key->fk_et_sport_book)->esb_booking_date)) ?></td>
                <td width="20%">Tiada</td>
                {{-- <td width="200px">{{ $key->etsportbook->etbookingfacility->data['getbmbno']->lkplocation->lc_description }}</td> --}}
                <td width="20%">{{ Helper::tempahanSportTime($key->fk_et_slot_price) }}</td>
                <td width="20%" align="right">{{ $key->est_price }}</td>
                <td width="20%" align="right">{{ $key->est_discount_type_rm ?? '0.00' }}</td>
                <td width="20%" align="right">{{ $key->est_special_disc_rm ?? '0.00' }}</td>
                {{-- <!--     <td width="200px" align="center">{{ $value->etsportbook->etbookingfacility->ebf_no_of_day }}</td> --> --}}
                <td width="20%" align="right">{{ $key->est_total }}</td>
                {{-- <td width="200px" align="center">{{ $key->ehtgstcode->lgr_gst_code }}</td>
                <td width="200px" align="right">{{ $key->est_gst_rm }}</td> --}}
                <td width="20%" align="right">{{ $key->est_subtotal }}</td>
            </tr>
            @php
                $sumselepas += $value->eht_subtotal;
            @endphp
        @endforeach
    @endif
                    <?php $a++;?>
                    {{-- <?php 
                    if ($data['ebf']->first()->ebf_facility_indi == 1) {
                        if ($data['eht']->isNotEmpty()) {
                            $sumselepas += $data['eht']->first()->eht_total;
                        }
                    } else {
                        if ($data['est']->isNotEmpty()) {
                            $sumselepas += $data['est']->first()->est_total;
                        }
                    }
                    ?> --}}
                    {{-- @empty
                      <tr><td colspan='11'>{{ trans('sales::sales.nodata') }}</td></tr>
                 @endforelse --}}
          </table>
          @if($data['getbmbno']->fk_lkp_discount_type !=1) 
             <div id="footer">* Selepas diskaun {{Helper::getDiscountRate($data['getbmbno']->fk_lkp_discount_type)}} %</div>
          @else
             <div>&nbsp;</div>
          @endif 
              <?php 
                      // if($gstSR->gst==''){
                      //      $gstSR=0.00;
                      // }else{
                      //      $gstSR=$gstSR->gst;
                      // }
                      //  if($gstSRI->gst==''){
                      //      $gstSRI=0.00;
                      // }else{
                      //      $gstSRI=$gstSRI->gst;
                      // }
                      ?>
                  <?php 
                      // if($gsteqpSR->gst==''){
                      //      $gsteqpSR=0.00;
                      // }else{
                      //      $gsteqpSR=$gsteqpSR->gst;
                      // }
                      //  if($gsteqpSRI->gst==''){
                      //      $gsteqpSRI=0.00;
                      // }else{
                      //      $gsteqpSRI=$gsteqpSRI->gst;
                      // }
                      ?>
     <table width="50%" id="title"  border="1" align="right"  cellpadding="2" cellspacing="0">
              {{-- <tr style="page-break-inside: avoid">
                <td width="40%"><b>GST (0%) </b></td>
                <td width="19%"  align="right"><b></b></td> --}}
                {{-- {{$gstSR}} --}}
                {{-- <td width="20%"  align="center"><b>E</b></td>
              </tr>
                <tr style="page-break-inside: avoid">
                <td width="40%" ><b>GST (0%) <b></td>
                <td width="19%" align="right"><b></b></td> --}}
                {{-- {{$gstSRI}} --}}
                {{-- <td width="20%" align="center"><b>I</b></td>
              </tr> --}}
                <tr style="page-break-inside: avoid">
                <th width="50%" align="left">Jumlah Sewaan Dewan (RM)</th>
                <td width="35%" align="right"><b><?php echo number_format($sumselepas,2);?></b></td>
                <td>&nbsp;</td>
              </tr>
              @if($data['lkp_gst']->status == 1)
                <tr style="page-break-inside: avoid">
                  <th width="50%" align="left">Jumlah Sewaan Dewan + GST (0%)(RM)</th>
                  <td width="35%" align="right"><b></b></td>
                  <?php echo number_format($sumselepas+$gstSR+$gstSRI,2);?>
                  <td>&nbsp;</td>
                </tr>
              @endif
               <tr style="page-break-inside: avoid">
                <th width="50%" scope="row" align="left">Jumlah Deposit (RM)</th>
                <td width="35%" align="right"><b><?php echo number_format($data['getbmbno']->bmb_deposit_rounding,2);?></b></td>
                <td>&nbsp;</td>
              </tr>
        </table>
         @if($data['ebf']->first()->ebf_facility_indi == 1)
      <div style="page-break-before:always;">
     <table width="100%" border="0"><tr><td</td></tr></table>
      <br>
       <div id="title"><b>&nbsp;KADAR SEWAAN PERALATAN</b></div>
            <br>
      <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
       <tr style="page-break-inside: avoid">
       <th width="4%" align="center"><b>Bil.</b></th>
       <th width="17%" align="center"><b>Nama Dewan/ Fasiliti</b></th>
       <th width="12%" align="center"><b>Item</b></th>
       <th width="12%" align="center"><b>Tarikh</b></th>
       <th width="12%" align="center"><b>Jenis Penggunaan</b></th>
       <th width="12%" align="center"><b>Kuantiti</b></th>
       <th width="12%" align="center"><b>Harga (RM)</b></th>
       <th width="12%" align="center"><b>Diskaun Kategori (RM)</b></th>
       <th width="12%" align="center"><b>Diskaun Khas (RM)</b></th>
       <th style="text-align:right"><b>Harga(RM)</b><br><font size="1">*selepas diskaun</font></th>
       {{-- <th style="text-align:center"><b>Kod GST</b></th>
       <th style="text-align:right"><b>GST (0%)</b></th> --}}
       <th style="text-align:right"><b>Jumlah Sewaan (RM)</b></th>
       </tr> 
        <?php  
                      $i=1; 
                      $sumselepaseqp=0;
                      ?>
        @forelse($data['eeb'] as $key => $value)
                  <tr style="page-break-inside:avoid;">
                      <td width=""><?php echo $i ?></a></td>
                      <td width="">{{ Helper::tempahanSportDetail($data['ebf'][0]->fk_et_facility_detail) }}</td>
                      <td width="">{{ Helper::get_equipment($value->fk_et_equipment) }}</td>
                      <td width="">{{ $value->eeb_booking_date }}</td>
                      <td width="">{{ Helper::typeFunction($value->fk_et_function) }}</td>
                      <td width="" align="center"></td>
                      {{-- {{ $value->sum_eeb_quantity }} --}}
                      <td width="" align="center"></td>
                      {{-- {{ $value->sum_eeb_total_price }} --}}
                      <td width="" align="center"></td>
                      {{-- <?php echo number_format( $value->sum_eeb_discount_type_rm *  $value->sum_eeb_quantity,2);?> --}}
                      <td width="" align="center"></td>
                      {{-- {{ $value->sum_eeb_special_disc_rm }} --}}
                      <td width="" align="right">{{Helper::moneyhelper($value->eeb_unit_price)}}</td>
                      {{-- {{ $value->sum_eeb_total }} --}}
                      {{-- <td width="" align="center"></td> --}}
                      {{-- {{ $value->ehtgstcode->lgr_gst_code }} --}}
                      {{-- <td width="" align="right"></td> --}}
                      {{-- {{ $value->sum_eeb_gst_rm }} --}}
                      <td width="" align="right"></td>
                      {{-- {{ $value->sum_eeb_subtotal }} --}}
                    </tr>
                      <?php $i++;?>
                    <?php 
                      $sumselepaseqp+= $value->eeb_total; 
                    ?>
                 @empty
                      <tr><td colspan='11'>{{ ('No Data') }}</td></tr>
                 @endforelse    
       </table>
      <br>
        <table id="title" width="50%" border="1" align="right"  cellpadding="2" cellspacing="0">
            @if($data['lkp_gst']->status == 1)
              <tr style="page-break-inside: avoid">
                <th width="40%"><b>GST (0%) </b></th>
                <th width="19%" align="right"></th>
                {{-- {{$gsteqpSR}} --}}
                <th width="20%" align="center"><b>E</b></th>
              </tr>
                <tr style="page-break-inside: avoid">
                <th width="40%" ><b>GST (0%) </b></th>
                <th width="19%" align="right"></th>
                {{-- {{$gsteqpSRI}} --}}
                <th width="20%" align="center"><b>I</b></th>
              </tr>
            @endif
            <tr style="page-break-inside:avoid;">
            <th scope="row">Jumlah Sewaan Peralatan (RM)</th>
            <td align="right"><b></b></td>
            {{-- <?php echo number_format(($sumselepaseqp),2)?> --}}
            <td>&nbsp;</td>
            </tr>
            @if($data['lkp_gst']->status == 1)
              <tr style="page-break-inside: avoid">
                  <th scope="row">Jumlah Sewaan Peralatan + GST (0%)(RM)</th>
                  <td align="right"><b></b></td>
                  {{-- <?php echo number_format($sumselepaseqp+$gsteqpSR+$gsteqpSRI,2);?> --}}
                  <td>&nbsp;</td>
              </tr>
            @endif
            </table>
                @endif
            <br>
             <table id="title" width="100%" border="0" align="right"  cellpadding="2" cellspacing="0">
            <tr>
              <td>
              </td>
            </tr>
        </table>
        <table id="title" width="50%" border="1" align="right"  cellpadding="2" cellspacing="0">
            <tr style="page-break-inside: avoid">
                <th width="109%" align="left"><b>Jumlah Kesuluruhan ( RM )</b></th>
                <th width="19%" align="right">{{$data['getbmbno']->bmb_subtotal}}</th>
            </tr>
            <tr style="page-break-inside:avoid;">
                <td width="109%"><b>Pengenapan</b></td>
                <?php $pengenapan=($data['getbmbno']->bmb_subtotal-$data['getbmbno']->bmb_rounding)?>
                <td width="19%" align="right"><b><?php echo number_format($pengenapan,2)?></b></td>
            </tr>
            <tr style="page-break-inside:avoid;">
                <td width="109%"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                <td width="19%" align="right"><b><?php echo number_format($data['getbmbno']->bmb_rounding,2)?></b></td>
            </tr>
        </table>
        <br>
        <table id="title" width="100%" border="0" align="right"  cellpadding="2" cellspacing="0">
            <tr>
              <td>
              </td>
            </tr>
        </table>
  </div>
  @if($data['ebf']->first()->ebf_facility_indi == 1)
  <div style="page-break-after:always;">&nbsp;</div>
  @endif
  
  <div id ="back" style="page-break-before:always;"> <table id="title" width="100%" border="1" cellpadding="10" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>C. PERAKUAN TEMPAHAN</strong>
                  </td>
                </tr>
              </table>
  
              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                  <td colspan="2">Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun yang dikenakan dan akan <b>dijelaskan tidak lewat daripada 14 hari sebelum </b> perkhidmatan / program / aktiviti / majis / Acara / Sewaan yang ditempah berlangsung<br><br>Tandatangan :</tr>
                <tr><td align="left">Nama & Jawatan Pemohon :<br>Tarikh :</td>
              </table>
              <br>
  <table id="title" width="100%" border="1" cellpadding="10" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>D. KEGUNAAN PEJABAT PERBADANAN PUTRAJAYA</strong>
                  </td>
                </tr>
              </table>
  
              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                <td colspan="2"><b>Diisi oleh Jabatan Pelaksana</b></td>
                <tr><td align="left">Nama Pegawai Melulus:<br>Bahagian/ Jabatan :<br>Tandatangan :<br>Tarikh :</td>
              </table>
  
              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                <td colspan="2"><b>Diisi oleh Jabatan Kewangan</b></td>
                <tr><td align="left">No. BP :<br>No. Kontrak :<br>No. Bil :<br>Tandatangan :<br>Tarikh :</td>
              </table>
              </div>
     </div>
   @if($data['ebf']->first()->ebf_facility_indi == 1)
   <div id ="back" style="page-break-before:always;">&nbsp;</div>
   @endif
  
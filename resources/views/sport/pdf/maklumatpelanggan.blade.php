<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        @page {
            size: landscape;
            margin: 1cm;
        }
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    

    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN MAKLUMAT PELANGGAN KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN MAKLUMAT PELANGGAN KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>Emel</b></th>
                <th class="th-sortable active"><b>No. Kad Pengenalan</b></th>
                <th class="th-sortable active"><b>No. Tel</b></th>
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Tarikh Penggunaan</b></th>
                <th class="th-sortable active"><b>Nama Fasiliti</b></th>
                <th class="th-sortable active"><b>Slot Masa</b></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;  
            @endphp
            
            @if (count($data['slot']) > 0)
            @foreach ($data['slot'] as $s)
                <tr>
                    <td style="width: 5%;">{{ $i++ }}</td>
                    <td>{{ ucfirst($s->fullname) }}</td>
                    <td>{{ $s->email }}</td>
                    <td>{{ $s->bud_reference_id }}</td>
                    <td>{{ $s->bud_phone_no }}</td>
                    <td>{{ $s->bmb_booking_no }}</td>
                    <td>{{ $s->ebf_start_date }}</td>
                    <td>{{ $s->efd_name }}</td>
                    <td class="text-right" width="15%">
                        @foreach ($data['umum'] as $u)
                            @if ($u->fk_et_booking_facility == $s->id)
                            <ul>
                                <li>{{ $u->est_slot_time }}</li>
                            </ul>
                            @endif
                        @endforeach
                    </td>
                </tr>
            @endforeach
            @else
                <tr>
                    <td colspan="10" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        @page {
            size: landscape;
            margin: 1cm;
        }
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    

    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN BAYARAN DEPOSIT KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN BAYARAN DEPOSIT KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>Nama Dewan / Kelengkapan</b></th>
                <th class="th-sortable active"><b>Kod Fee</b></th>
                <th class="th-sortable active"><b>Tarikh Transaksi</b></th>
                <th class="th-sortable active"><b>No. Resit</b></th>
                <th class="th-sortable active"><b>Jenis Bayaran</b></th>
                <th class="th-sortable active"><b>Jumlah (RM)</b></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;  
                $total = 0;
            @endphp
            @if (count($data['payment']) > 0)
            @foreach ($data['payment'] as $payment)
                <tr>
                    <td style="width: 5%;">{{ $i++ }}</td>
                    {{-- <td>{{ Helper::get_noTempahan(Helper::getBhPaymentFpx($payment->fk_bh_payment_fpx)->fk_main_booking) }}</td> --}}
                    <td>{{ $payment->bmb_booking_no }}</td>
                    {{-- <td>{{ Helper::get_nama(Helper::get_userTempahan(Helper::getBhPaymentFpx($payment->fk_bh_payment_fpx)->fk_main_booking)) }}</td> --}}
                    <td>{{ $payment->fullname }}</td>
                    {{-- <td>{{ Helper::getHall($payment->fk_bh_hall) }}</td> --}}
                    <td>{{ $payment->efd_name }}</td>
                    {{-- <td>{{ Helper::getBhCodeDeposit($payment->fk_bh_hall) }}</td> --}}
                    <td>{{ $payment->efd_fee_code }}</td>
                    <td>
                        @if(($payment->lpm_description) === 'LO/PO')
                            {{ $payment->bp_receipt_date }}
                        @elseif(($payment->lpm_description) === 'FPX')
                            {{ Helper::getEtPaymentFpxDate($payment->id) }}
                        @endif
                    </td>
                    <td>
                        @if(($payment->lpm_description) === 'LO/PO')
                            {{ $payment->bp_receipt_number }}
                        @elseif(($payment->lpm_description) === 'FPX')
                            {{ Helper::getEtPaymentFpx($payment->id) }}
                        @endif
                    </td>
                    {{-- <td>{{ Helper::get_lkp_event_string_laporan(Helper::get_kemudahan_terperinci_laporan(Helper::get_et_booking_facility($payment->id))) }}</td> --}}
                    <td>{{ $payment->lpm_description }}</td>
                    <td class="text-right">{{ $payment->ebf_subtotal }}</td>
                </tr>
                @php
                    $total += $payment->ebf_subtotal;
                @endphp
            @endforeach
            @else
            <tr>
                <td colspan="9" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
            </tr>
            @endif
            {{-- <tr class="bg-dark">
                <td colspan="9" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
            </tr> --}}
        </tbody>
        <tfoot>
            <tr class="bg-dark">
                <td colspan="8" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
            </tr>
        </tfoot>
    </table>
</body>
</html>

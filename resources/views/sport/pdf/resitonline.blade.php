<!DOCTYPE html>
<html>
<head>
    <title>Resit Online</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @page {
            margin-top: 25px;
        }
        body {
            font-family: Arial, sans-serif;
        }
        .kosong {
            margin-bottom: 0;
            font-size: 9px;
        }
        .standard {
            font-size: 11px;
            margin-bottom: 0;
        }
        .tajuk {
            font-size: 14px;
        }
        .table td, .table th {
            border-top: 0;
        }
        .buang {
            padding: 3px !important;
        }
        .buang1 {
            padding-top: 3px !important;
            padding-bottom: 3px !important;
        }
        tfoot tr td {
            margin-bottom: 0;
            border-bottom: 0;
            font-size: 10px; 
            font-weight: bold;
        }
        #title{
            font-size:10px;
        }
    </style>
</head>
<body>
    <main> 
        <table class="table">
            <tbody>
                <tr>
                    <td style="text-align: center; width: 10%">
                        <img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" alt="" style="width: 60px; height: 70px">
                    </td>
                    <td style="font-size: small;">
                        <p class="kosong"><b>PERBADANAN PUTRAJAYA</b></p>
                        <p class="kosong" style="text-transform: uppercase;"><b>KOMPLEKS KEJIRANAN {{ $location->lc_description }}</b></p>
                        <p class="kosong">62300, WP-PUTRAJAYA</p>
                        <p class="kosong">Tel: {{ $location->lc_contact_no }}</p>
                        <p class="kosong">Fax: {{ $location->lc_fax_no }}</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <table class="table table-bordered" style="background-color:#dee2e6">
            <tbody>
                <tr>
                    <th class="tajuk" style="text-align: center">
                        RESIT RASMI
                    </th>
                </tr>
            </tbody>
        </table>
        <table class="table">
            <tbody>
                <tr class="standard">
                    <th class="buang" style="width: 20%">MOD :</th>
                    <td class="buang">{{ $online ? "ONLINE" : "KAUNTER" }}</td>
                    <th class="buang" style="width: 20%">Tarikh Transaksi :</th>
                    <td class="buang">{{ $online ? Helper::datetime_format($online->fpx_trans_date) : Helper::datetime_format($counter->bp_receipt_date) }}</td>
                </tr>
                <tr class="standard">
                    <th class="buang">{{ $online ? "BANK :" : "Jenis Transaksi :" }}</th>
                    <td class="buang">{{ $online ? $online->bank : Helper::get_kutipan_bayaran($counter->fk_lkp_payment_mode) }}</td>
                    <th class="buang">No Transaksi {{ $online ? 'FPX :' : "" }}</th>
                    <td class="buang">{{ $online ? $online->fpx_serial_no : "No data" }}</td>
                </tr>
                <tr class="standard">
                    <th class="buang">Nama Pelanggan :</th>
                    <td class="buang">{{ $user->fullname }}</td>
                    <th class="buang">No K/P Pelanggan :</th>
                    <td class="buang">{{ $user_detail->bud_reference_id }}</td>
                </tr>
                <tr class="standard">
                    <th class="buang">No. Telefon :</th>
                    <td class="buang">{{ $user_detail->bud_phone_no }}</td>
                    <th class="buang">Jumlah Bayaran :</th>
                    <td class="buang">RM {{ $online ? $online->amount_paid : $counter->bp_paid_amount }}</td>
                </tr>
                <tr class="standard">
                    <th class="buang">
                        {{ $online ? "No. Tempahan :" : ($counter->bp_receipt_number != null ? "No. Resit :" : "No. Cek") }}
                    </th>
                    <td class="buang">
                        {{ $online ? $online->fpx_trans_id : ($counter->no_cek ?? $counter->bp_receipt_number ?? $counter->no_lopo) }}
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="standard">KETERANGAN: </p>
        <p class="standard">BAYARAN PENUH BAGI PENGGUNAAN DEWAN / FASILITI KOMPLEKS KEJIRANAN {{ strtoupper($location->lc_description) }}</p>
        @if($equiponly == false)
            <div id="title"><b>Kadar Sewaan Dewan</b></div>
            <table class="table table-bordered" style="font-size: 6px; padding: 0px; margin: 0px">
                <thead>
                    <tr class="standard">
                        <th class="text-center">Bil</th>
                        <th class="text-center">Nama Fasiliti</th>
                        <th class="text-center">Tarikh Penggunaan</th>
                        <th class="text-center">Slot Masa</th>
                        <th class="text-center">Harga (RM) <br><span class="text-primary">*selepas diskaun</span></th>
                        @foreach($tax as $taxItem)
                            @if($taxItem->lt_status == 1)
                                <th>Kod GST</th>
                                <th>GST (RM)</th>
                            @endif
                        @endforeach
                        <th class="text-center">Jumlah (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                        $total = 0.00;
                    @endphp
                    @if($type == 2)
                        @foreach($slot as $s)
                        <tr class="standard">
                            <td class="buang1" style="width: 2%; text-align: center">{{ $i++ }}</td>
                            <td class="buang1">{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                            <td class="buang1 text-center">{{ Helper::date_format($s->ebf_start_date) }}</td>
                            <td class="buang1 text-right" style="white-space:nowrap">{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                            <td class="buang1" style="text-align: right">{{ number_format($s->est_subtotal, 2) }}</td>
                            @foreach($tax as $taxItem)
                                @if($taxItem->lt_status == 1)
                                    <td class="buang1" style="text-align: right">{{ $taxItem->lt_code }}</td>
                                    <td class="buang1" style="text-align: right">RM {{ $taxItem->lt_rate }}</td>
                                @endif
                            @endforeach
                            <td class="buang1" style="text-align: right">{{ number_format($s->est_subtotal, 2) }}</td>
                            @php
                                $total += $s->est_subtotal;
                            @endphp
                        </tr>
                        @endforeach
                    @elseif($type == 1)
                        @foreach($dewan as $d)
                        <tr class="standard">
                            <td class="buang1" style="width: 2%; text-align: center">{{ $i++ }}</td>
                            <td class="buang1">{{ Helper::tempahanSportDetail($d->fk_et_facility_detail) }}</td>
                            <td class="buang1">{{ Helper::date_format($d->ebf_start_date) }}</td>
                            <td class="buang1">{{ $d->eht_price }}</td>
                            <td class="buang1" style="text-align: right">{{ number_format($d->eht_subtotal, 2) }}</td>
                            @foreach($tax as $taxItem)
                                @if($taxItem->lt_status == 1)
                                    <td class="buang1" style="text-align: right">{{ $taxItem->lt_code }}</td>
                                    <td class="buang1" style="text-align: right">RM {{ $taxItem->lt_rate }}</td>
                                @endif
                            @endforeach
                            <td class="buang1" style="text-align: right">{{ number_format($d->eht_subtotal, 2) }}</td>
                            @php
                                $total += $d->eht_subtotal;
                            @endphp
                        </tr>
                        @endforeach
                    @else
                        <td colspan="6" class="text-center">No data</td>
                    @endif
                </tbody>
            </table>
        @else
            <div style="font-size: 10px; margin-bottom: 15px">* Selepas diskaun {{ $perc }}%</div>
            <div id="title"><b>Kadar Sewaan Peralatan</b></div>
            <table class="table table-bordered" style="font-size: 6px; padding: 0px; margin: 0px">
                <thead>
                    <tr class="standard">
                        <th class="text-center">Bil</th>
                        <th class="text-center">Item</th>
                        <th class="text-center">Tarikh</th>
                        <th class="text-center">Jenis Penggunaan</th>
                        <th class="text-center">Kuantiti</th>
                        <th class="text-center">Jumlah Sewaan (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                        $totaleq = 0.00;
                    @endphp
                    @foreach($eeb as $eeb)
                    <tr class="standard">
                        <td class="buang1" style="width: 2%; text-align: center">{{ $i++ }}</td>
                        <td class="buang1">{{ Helper::get_equipment($eeb->fk_et_equipment) }}</td>
                        <td class="buang1">{{ Helper::date_format($eeb->eeb_booking_date) }}</td>
                        <td class="buang1">{{ Helper::typeFunction($eeb->fk_et_function) }}</td>
                        <td class="buang1" style="text-align: right">{{ $eeb->eeb_quantity }}</td>
                        <td class="buang1" style="text-align: right">{{ $eeb->eeb_subtotal }}</td>
                    </tr>
                    @php     
                        $totaleq = $eeb->eeb_subtotal 
                    @endphp
                    @endforeach
                </tbody>
            </table>
            @endif
        <br>
        <table class="table table-bordered" style="float: right">
            <tbody>
                <tr class="standard">
                    <th colspan="2">Rumusan Harga</th>
                </tr>
                <tr class="standard">
                    <th class="buang1">Jumlah Keseluruhan (RM)</th>
                    {{-- <td class="buang1" style="text-align: right">{{ $main->bmb_subtotal }}</td> --}}
                    @if($equiponly == false)
                        <td class="buang1" style="text-align: right">{{ number_format($total, 2) }}</td>
                    @else
                        <td class="buang1" style="text-align: right">{{ $totaleq }}</td>
                    @endif
                </tr>
                <tr class="standard">
                    <th class="buang1">Pengenapan (RM)</th>
                    <td class="buang1" style="text-align: right">{{ $main->bmb_rounding }}</td>
                </tr>
                <tr class="standard">
                    <th class="buang1">Jumlah Keseluruhan Telah Dibayar (RM)</th>
                    <td class="buang1" style="text-align: right">{{ $online ? $online->amount_paid : $counter->bp_paid_amount }}</td>
                </tr>
            </tbody>
        </table>
        <p class="kosong">NOTA :</p>
        <ol class="kosong">
            <li>Kegagalan membayar jumlah tempahan dalam tempoh empat belas (14) hari sebelum tarikh penggunaan akan menyebabkan tempahan anda dibatalkan.</li>
            <li>Resit Deposit/ Cagaran hendaklah disimpan dengan selamat dan dikemukakan semula apabila membuat tuntutan balik deposit/ cagaran.</li>
        </ol>
        <table id="footer" width="100%" border="0" cellpadding="10">
            <tr>
                <td align="center" style="text-align:center"></td>
                <td align="center" style="text-align:center; font-size: 10px">Resit ini cetakan komputer, tiada tandatangan diperlukan.</td>
            </tr>
        </table>
    </main>
</body>
</html>

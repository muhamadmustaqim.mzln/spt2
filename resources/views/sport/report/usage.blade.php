@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Jenis Penggunaan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Jenis Penggunaan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex justify-content-center">
            <!--begin::Container-->
            <div class="container mx-5 px-0">
            {{-- <div class="mx-0 px-15" style="width: 100%;">   --}}
                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b w-100">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row d-flex justify-content-center">
                                        <div class="col-lg-3">
                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Sila Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}" {{($l->id== $data['id']) ? 'selected' : ''}}>{{ $l->lc_description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <select name="usage_type" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Jenis Penggunaan</option>
                                                <option value="1" @if (isset($data['usage_type']) && $data['usage_type'] == 1) selected @endif>Fasiliti</option>
                                                <option value="2" @if (isset($data['usage_type']) && $data['usage_type'] == 2) selected @endif>Peralatan</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <select name="search_type" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Jenis Carian</option>
                                                <option value="1">Tarikh</option>
                                                <option value="2">Tahun</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="tarikh-inputs"> <!-- Added ID for 'Tarikh' inputs container -->
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="mula" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Penggunaan Dari" autocomplete="off" value="{{ isset($data['start']) ? Helper::date_format($data['start']) : '' }}">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="tamat" class="form-control mt-3" id="kt_daterangepicker_6" placeholder="Tarikh Penggunaan Hingga" autocomplete="off" value="{{ isset($data['end']) ? Helper::date_format($data['end']) : '' }}">
                                        </div>
                                        <div class="col-lg-3">
                                        </div>
                                    </div>
                                    <div class="form-group row" id="tahun-input"> <!-- Added ID for 'Tahun' input container -->
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-1">
                                        </div>
                                        <div class="col-lg-2">
                                            <select name="year" class="form-control mt-3">
                                                @php
                                                $year = date("Y") +1; 
                                                for ($i = 0; $i <= 6; $i++) { 
                                                    $year--; 
                                                    if(isset($_GET['year'])) {
                                                        echo "<option".($_GET['year'] == $year ? " selected='selected'": "").">$year</option>";
                                                    } else {
                                                        echo "<option>$year</option>";
                                                    }
                                                }
                                                @endphp
                                            </select>
                                        </div>
                                        <div class="col-lg-1">
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="form-group row d-flex justify-content-center">
                                        <div class="col"></div>
                                        <div class="col">
                                            <div class="">
                                                <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                            </div>
                                        </div>
                                        <div class="col"></div>
                                    </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12" style="width: 100%">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title" id="tableTitle">
                                    <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN JENIS PENGGUNAAN {{ strtoupper(Helper::location($data['id'])) }} TAHUN {{ $data['year'] }} <br>@if($data['search_type'] == 1)<span class="text-muted mt-2 font-weight-bold font-size-sm"> Dari : {{ Helper::date_format($data['start']) }} Hingga : {{ Helper::date_format($data['end']) }} </span></span> @endif
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                @php
                                    $i = 1;
                                    $totaleq = 0;
                                    $total = 0.00;
                                @endphp
                                <div class="table-responsive">
                                    @if ($data['search_type'] == 1)    {{--  Tarikh --}}
                                        <table class="table table-bordered table-hover table-checkable" id="myTable">
                                            <thead> 
                                                <tr>
                                                    <th>Bil</th>
                                                    <th class="text-center">Penggunaan</th>
                                                    <th class="text-center">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $first_day_this_month = date('01-m-Y');
                                                    $last_day_this_month  = date('t-m-Y');
                                                    $defaultrange = [date('Y-m-d',strtotime($first_day_this_month)), date('Y-m-d',strtotime($last_day_this_month))];
                                            
                                                    if(isset($data['start']) || isset($data['end'])){
                                                        $range = [date('Y-m-d',strtotime($data['start'])), date('Y-m-d',strtotime($data['end']))];
                                                        if($data['start']!='' ||$data['end']!=''){
                                                        $tarikh=$range;
                                                        }else{
                                                        $tarikh=$defaultrange;
                                                        }
                                                    }else{
                                                        $tarikh=$defaultrange;
                                                    }
                                                    $sum=0;
                                                    $sum_amaun=0.00;
                                                @endphp
                                                @if ($data['usage_type'] == 1)
                                                    @foreach ($data['list_facility'] as $key => $s)
                                                        @php
                                                            $data_et_hall_sport = Helper::laporan_penggunaan($tarikh, $key);
                                                        @endphp
                                                        <tr>
                                                            <td style="width: 5%">{{ $i++ }}</td>
                                                            <td>{{ $s }}</td>
                                                            <td>
                                                                @if ($data_et_hall_sport[0]->bil_hall + $data_et_hall_sport[1]->bil_sport == 0)
                                                                    0 (RM0.00)
                                                                @else
                                                                    @php
                                                                        $totaleq += $data_et_hall_sport[0]->bil_hall + $data_et_hall_sport[1]->bil_sport;
                                                                        $total += $data_et_hall_sport[0]->amaun + $data_et_hall_sport[1]->amaun;
                                                                    @endphp
                                                                    {{ ($data_et_hall_sport[0]->bil_hall + $data_et_hall_sport[1]->bil_sport) }} (RM {{ Helper::moneyhelper($data_et_hall_sport[0]->amaun + $data_et_hall_sport[1]->amaun) }})
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    @php
                                                        $ebf = Helper::laporan_penggunaan_peralatan($tarikh);
                                                    @endphp
                                                    @foreach ($data['list_equipment'] as $key => $s)
                                                        @php
                                                            $data_et_equipment = Helper::laporan_penggunaan_peralatan2($tarikh, $key, $ebf);
                                                        @endphp
                                                        <tr>
                                                            <td style="width: 5%">{{ $i++ }}</td>
                                                            <td>{{ Helper::get_equipment($s) }}</td>
                                                            @if($data_et_equipment->bil_eqp==0)
                                                                <td align="left">0 (RM 0.00)</td>
                                                            @else
                                                                @php
                                                                    $totaleq += $data_et_equipment->bil_eqp;
                                                                    $total += $data_et_equipment->amaun;
                                                                @endphp
                                                                <td align="left">{{($data_et_equipment->bil_eqp)}} (RM <?php echo number_format(($data_et_equipment->amaun),2);?>)</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                <tr>
                                                    <td></td>
                                                    <td align="right"><b>Jumlah</b></td>
                                                    <td>{{ $totaleq }} (RM {{ Helper::moneyhelper($total) }})</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    @else                              {{--  Tahun --}}
                                        <table class="table table-bordered table-hover table-checkable" id="myTable1">
                                            @php
                                            $months = array(1 => 'Januari', 2 => 'Februari', 3 => 'March', 4 => 'April',
                                                            5 => 'Mei', 6 => 'Jun', 7 => 'Julai', 8 => 'Ogos', 9 => 'September', 10 => 'Oktober',
                                                            11 => 'November', 12 => 'Disember');
                                            @endphp
                                            <thead> 
                                                <tr>
                                                    <th>Bil</th>
                                                    <th class="text-center">Penggunaan</th>
                                                    @foreach ($months as $num => $name)
                                                        <th width="18%"><b>{{$name;}}</b></th>
                                                    @endforeach
                                                    <th class="text-center">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($data['usage_type'] == 1)
                                                    @foreach ($data['list_facility'] as $key => $s)
                                                        <tr>
                                                            <td><b>{{ $i++ }}</b></td>
                                                            <td><b>{{ $s }}</b></td>
                                                            @foreach ($months as $num => $name)
                                                                @php
                                                                    $data_facility_year = Helper::laporan_penggunaan_tahun($num, $year, $key);
                                                                @endphp
                                                                @if(($data_facility_year[0]->bil_hall+$data_facility_year[1]->bil_sport)==0)
                                                                    <td align="right">0 (RM 0.00)</td>
                                                                @else
                                                                    <td align="right">{{($data_facility_year[0]->bil_hall+$data_facility_year[1]->bil_sport)}} (RM <?php echo number_format(($data_facility_year[0]->amaun + $data_facility_year[1]->amaun),2);?>)</td>
                                                                @endif
                                                            @endforeach
                                                            @if(($data_facility_year[2]->bil_hall_year +$data_facility_year[3]->bil_sport_year)=='')
                                                                <td align="right">0 (RM 0.00)</td>
                                                            @else
                                                                <td align="right">{{($data_facility_year[2]->bil_hall_year +$data_facility_year[3]->bil_sport_year)}} (RM <?php echo number_format(($data_facility_year[2]->amaun_year + $data_facility_year[3]->amaun_year),2);?>)</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    @php
                                                        $ebf = Helper::laporan_penggunaan_peralatan_tahun('', '', '', '', 1);
                                                    @endphp
                                                    @foreach ($data['list_equipment'] as $key => $s)
                                                        <tr>
                                                            <td><b>{{ $i++ }}</b></td>
                                                            <td><b>{{ Helper::get_equipment($s) }}</b></td>
                                                            @foreach ($months as $num => $name)
                                                                @php
                                                                    $data_facility_year = Helper::laporan_penggunaan_peralatan_tahun($num, $data['year'], $key, $ebf, 2);
                                                                @endphp
                                                                @if($data_facility_year[0]->bil_eqp==0)
                                                                    <td align="right">0 (RM 0.00)</td>
                                                                @else
                                                                    <td align="right">{{($data_facility_year[0]->bil_eqp)}} (RM <?php echo number_format(($data_facility_year[0]->amaun),2);?>)</td>
                                                                @endif
                                                            @endforeach
                                                            @if(($data_facility_year[1]->bil_eqp_year)=='')
                                                                <td align="right">0 (RM 0.00)</td>
                                                            @else
                                                                <td align="right">{{($data_facility_year[1]->bil_eqp_year)}} (RM <?php echo number_format(($data_facility_year[1]->bil_eqp_year),2);?>)</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                            <!--end::Body-->
                            @if($data['search_type'] == 2)
                            <div class="form-group row d-flex justify-content-center mb-5">
                                <div class="col-2">
                                    <div class="">
                                        {{-- <button onclick="generatePDF(tableData)">Export to PDF</button> --}}
                                        <button type="button" onclick="" id="exportToPDF" class="form-control btn btn-primary font-weight-bold mt-3">Muat Turun</button>
                                    </div>
                                </div>
                            </div>
                            @elseif ($data['search_type'] == 1)
                            <div class="form-group row d-flex justify-content-center mb-5">
                                <div class="col-2">
                                    <div class="">
                                        {{-- <button onclick="generatePDF(tableData)">Export to PDF</button> --}}
                                        <button type="button" onclick="" id="exportToPDF1" class="form-control btn btn-primary font-weight-bold mt-3">Muat Turun</button>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.usage')
@endsection
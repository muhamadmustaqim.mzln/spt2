@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Kutipan Harian</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Kutipan Harian</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                            <div class="card-body pt-2">
                                <hr>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-lg-3">
                                        <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Lokasi</option>
                                            @foreach ($data['location'] as $l)
                                                <option value="{{ $l->id }}" {{($l->id== $data['id']) ? 'selected' : ''}}>{{ $l->lc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select name="bayaran" id="kt_select2_4" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Bayaran</option>
                                            @foreach ($data['bayaran'] as $b)
                                                <option value="{{ $b->id }}" {{($b->id == $data['mode']) ? 'selected' : ''}}>{{ $b->lpm_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_4" placeholder="Tarikh" autocomplete="off" required value="{{ isset($data['tarikh']) ? $data['tarikh'] : '' }}">
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class="float-right">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col"></div>
                                    <div class="col">
                                        <div class="">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div>
                                    <div class="col"></div>
                                </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12">
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN {{ strtoupper(Helper::location($data['id'])) }}  <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ $data['tarikh'] }}</span><br><span class="text-muted mt-2 font-weight-bold font-size-sm">Hari Kutipan : {{ $data['dayOfWeek'] }}</span></span>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-checkable" style="margin-top: 13px !important">
                                        <thead> 
                                            <tr>
                                                <th>Bil</th>
                                                {{-- <th>No. Tempahan</th> --}}
                                                <th class="text-center">No. Resit</th>
                                                <th class="text-center">Nama Pembayar</th>
                                                <th class="text-center">Mod Bayaran</th>
                                                {{-- <th>Nama Dewan / Kelengkapan</th> --}}
                                                <th class="text-center">Kod Hasil & Gelanggang</th>
                                                {{-- <th>Tarikh Transaksi</th> --}}
                                                {{-- <th>No. Resit</th>
                                                <th>No. Ruj Transaksi</th>
                                                <th>Jenis Bayaran</th> --}}
                                                <th class="text-center">Jumlah (RM)</th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;  
                                                $total = 0;
                                            @endphp
                                            @if (count($data['harian']) > 0)
                                            @foreach ($data['harian'] as $harian)
                                                <tr>
                                                    <td style="width: 5%;">{{ $i++ }}</td>
                                                    <td>
                                                        @if(($harian->lpm_description) === 'LO/PO')
                                                            {{ $harian->bp_receipt_number }}
                                                        @elseif(($harian->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpx($harian->id) }}
                                                        @endif
                                                    </td>
                                                    {{-- <td>{{ $harian->bmb_booking_no }}</td> --}}
                                                    <td>{{ $harian->fullname }}</td>
                                                    <td>{{ $harian->lpm_description }}</td>
                                                    <td>{{ $harian->efd_fee_code }}<br>{{ $harian->efd_name }}</td>
                                                    {{-- <td>
                                                        @if(($harian->lpm_description) === 'LO/PO')
                                                            {{ $harian->bp_receipt_date }}
                                                        @elseif(($harian->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpxDate($harian->id) }}
                                                        @endif
                                                    </td> --}}
                                                    {{-- <td>
                                                        @if(($harian->lpm_description) === 'LO/PO')
                                                            {{ $harian->bp_receipt_number }}
                                                        @elseif(($harian->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpx($harian->id) }}
                                                        @endif
                                                    </td> --}}
                                                    {{-- <td>
                                                        @if(($harian->lpm_description) === 'LO/PO')
                                                            {{ $harian->bp_payment_ref_no }}
                                                        @elseif(($harian->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpxRef($harian->id) }}
                                                        @endif
                                                    </td> --}}
                                                    {{-- <td>{{ $harian->lpm_description }}</td>--}}
                                                    <td class="text-right">{{ $harian->ebf_subtotal }}</td> 
                                                </tr>
                                                @php
                                                    $total += $harian->ebf_subtotal;
                                                @endphp
                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
                                                </tr>
                                            @endif
                                                {{-- <tr class="bg-dark">
                                                    <td colspan="9" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                                                    <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                                                </tr> --}}
                                        </tbody>
                                        <tfoot>
                                            <tr class="fw-bold fs-6 bg-dark">
                                                <td colspan="5" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                                                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <table class="table table-bordered table-condensed" id="body">
                                    <tr>
                                      <th class="th-sortable active" style="width:25%; text-align:center;">
                                        Disediakan Oleh:
                                        <br/>(PETUGAS KAUNTER)
                                      </th>
                                      <th class="th-sortable active" style="width:25%; text-align:center;">
                                        Disemak Oleh:
                                        <br/>(PENYELIA OPERASI)
                                      </th>
                                      <th class="th-sortable active" colspan=2 style="width:50%; text-align:center;">
                                        &nbsp;
                                      </th>
                                    </tr>
                                    <tr>
                                      <td rowspan=3>
                                      <?php
                                        date_default_timezone_set('Asia/Kuala_Lumpur');
                                        echo "<strong>Tarikh Cetakan:</strong> ".date('d/m/Y H:i A');
                                      ?>
                                      &nbsp;
                                      </td>
                                      <td rowspan=3>
                                      &nbsp;
                                      </td>
                                      <td>
                                        <br/>
                                        TEMPOH LEWAT SERAH: ................hari
                                        <br/>(Diisi Oleh Petugas Kaunter Kewangan)
                                        <br/><br/>
                                      </td>
                                      <td>
                                        &nbsp;
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="width:25%; text-align:center;">
                                        DISERAH OLEH:
                                      </th>
                                      <td style="width:25%; text-align:center;">
                                        &nbsp;
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="padding:60px">&nbsp;</td>
                                      <td style="padding:60px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="text-align:center;">NAMA & TANDATANGAN</td>
                                      <td style="text-align:center;">NAMA & TANDATANGAN</td>
                                      <td style="text-align:center;">NAMA & TANDATANGAN</td>
                                      <td style="text-align:center;">NAMA & TANDATANGAN</td>
                                    </tr>
                                    <tr>
                                      <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
                                      <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
                                      <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
                                      <td style="text-align:center;">{{ date('d/m/Y', strtotime("+1 day")) }}</td>
                                    </tr>
                                  </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center gutter-b">
                                    <a href="{{ url('sport/eksportpdfkutipanharian',[Crypt::encrypt($data['id']),Crypt::encrypt($data['tarikh']),Crypt::encrypt($data['mode'])]) }}" class="btn btn-sm btn-primary">Cetak PDF</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.harian')
@endsection
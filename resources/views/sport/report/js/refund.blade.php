<script>
    // date picker
    $('#kt_daterangepicker_2').daterangepicker({
        autoApply: true,
        autoclose: true,
    }, function(start, end, label) {
        $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });
</script>
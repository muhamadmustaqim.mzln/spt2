<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"></script>
<script>
    $(document).ready(function() {
    var table = $('#table').DataTable({
        // dom: `<'row'<'col-sm-6 text-left'f>>
        dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
			<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        buttons: [
            {
                extend: 'colvis',
                text: 'Pilihan Lajur'
            },
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
        //     {
        //         extend: 'pdf',
        //         exportOptions: {
        //           columns: ':visible'
        //         },
        //         customize: function(doc) {
        //             doc.pageOrientation = 'landscape';
        //             // doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
        //             //doc.defaultStyle.alignment = 'center';
        //             //doc.styles.tableHeader.alignment = 'center';
        //         },
        //     },
        ],
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        }
    } );
    var table = $('#myTable').DataTable({
        // dom: `<'row'<'col-sm-6 text-left'f>>
        dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
			<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        buttons: [
            {
                extend: 'colvis',
                text: 'Pilihan Lajur'
            },
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
        //     {
        //         extend: 'pdf',
        //         exportOptions: {
        //           columns: ':visible'
        //         },
        //         customize: function(doc) {
        //             doc.pageOrientation = 'landscape';
        //             // doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
        //             //doc.defaultStyle.alignment = 'center';
        //             //doc.styles.tableHeader.alignment = 'center';
        //         },
        //     },
        ],
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        }
    } );
    $('#carianSukan').on('change', function () {
    })
});
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_6').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    window.jsPDF = window.jspdf.jsPDF;

    $(document).ready(function(){
        $('#exportToPDF').on('click', function() {
            // Get table element
            const table = document.getElementById('table');
            const titleElement = document.getElementById('tableTitle');

            // Extract the title text, handling <br> tags as new lines
            let titleHTML = titleElement.innerHTML;
            titleHTML = titleHTML.replace(/<br\s*\/?>/gi, '\n'); // Replace <br> tags with new lines

            // Create a temporary div to parse the HTML and extract text
            const tempDiv = document.createElement('div');
            tempDiv.innerHTML = titleHTML;
            const titleText = tempDiv.innerText; // Extract text content with new lines

            const titleLines = titleText.split('\n'); // Split the text into lines

            // A4 dimensions in pixels at 72 DPI
            const a4Width = 841.89;
            const a4Height = 595.28;
            const padding = 20; // Fixed padding around the table
            const titleFontSize = 16; // Font size for the title
            const titleMargin = 5; // Margin between the title and the table
            const dateFontSize = 12; // Font size for the date and time

            // Get current date and time
            const today = new Date();
            const date = today.toLocaleDateString();
            const time = today.toLocaleTimeString('en-GB'); // 24-hour format

            const doc = new jsPDF({
                orientation: 'landscape',
                unit: 'px',
                format: [a4Width, a4Height],
            });

            // Add the title lines
            doc.setFontSize(titleFontSize);
            let currentY = padding;
            titleLines.forEach((line, index) => {
                const trimmedLine = line.trim();
                const titleWidth = doc.getTextWidth(trimmedLine);
                const titleX = (a4Width - titleWidth) / 2;
                doc.text(trimmedLine, titleX, currentY);
                currentY += titleFontSize + 2; // Move to the next line, add some spacing
            });

            // Add date and time
            doc.setFontSize(dateFontSize);
            const dateText = `Tarikh: ${date}`;
            const timeText = `Masa: ${time}`;
            const dateX = a4Width - padding - doc.getTextWidth(dateText);
            const timeX = a4Width - padding - doc.getTextWidth(timeText);
            doc.text(dateText, dateX, padding);
            doc.text(timeText, timeX, padding + dateFontSize + 2);

            // Calculate the position for the table image below the title and date/time
            const tableStartY = currentY + titleMargin + dateFontSize + 2 + dateFontSize + 2;

            // Convert the table to an image using dom-to-image
            domtoimage.toPng(table)
                .then(function (dataUrl) {
                    // Create an image object to get the original dimensions
                    const img = new Image();
                    img.src = dataUrl;
                    img.onload = function() {
                        const imgWidth = img.width;
                        const imgHeight = img.height;

                        // Calculate the scaling factor to fit the image into A4 dimensions with padding
                        const scaleX = (a4Width - 2 * padding) / imgWidth;
                        const scaleY = (a4Height - tableStartY - padding) / imgHeight;
                        const scale = Math.min(scaleX, scaleY);

                        // Calculate the new dimensions
                        const scaledWidth = imgWidth * scale;
                        const scaledHeight = imgHeight * scale;

                        // Calculate the position to center the image
                        const x = (a4Width - scaledWidth) / 2;
                        const y = tableStartY;

                        // Add the image to the PDF
                        doc.addImage(dataUrl, 'PNG', x, y, scaledWidth, scaledHeight);

                        // Draw a border around the table
                        doc.setLineWidth(1);
                        doc.rect(x, y, scaledWidth, scaledHeight);

                        // Save the PDF
                        doc.save('Laporan Maklumat Tempahan.pdf');
                    };
                })
                .catch(function (error) {
                    console.error('Error exporting to PDF:', error);
                });
        });
    });
</script>
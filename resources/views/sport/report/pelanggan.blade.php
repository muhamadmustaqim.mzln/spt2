@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Maklumat Pelanggan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Maklumat Pelanggan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                            <div class="card-body pt-2">
                                <hr>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col-lg-3">
                                        <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Lokasi</option>
                                            @foreach ($data['location'] as $l)
                                                {{-- <option value="{{ $l->id }}">{{ $l->lc_description }}</option> --}}
                                                <option value="{{ $l->id }}"@if(isset($data['id'])) @if($data['id'] == $l->id) selected @endif @endif>{{ $l->lc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="mula" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Penggunaan Dari" autocomplete="off" value="{{ isset($data['mula']) ? $data['mula'] : '' }}" required>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="tamat" class="form-control mt-3" id="kt_daterangepicker_6" placeholder="Tarikh Penggunaan Hingga" autocomplete="off" value="{{ isset($data['tamat']) ? $data['tamat'] : '' }}" required>
                                    </div>
                                    
                                    {{-- <div class="col-lg-2">
                                        <div class="float-right">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col"></div>
                                    <div class="col">
                                        <div class="">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div>
                                    <div class="col"></div>
                                </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN MAKLUMAT PELANGGAN {{ strtoupper(Helper::location($data['id'])) }}  <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Dari : {{ $data['mula'] }} Hingga : {{ $data['tamat'] }}</span></span>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <table class="table table-bordered table-hover table-checkable" id="table" style="margin-top: 13px !important">
                                    <thead> 
                                        <tr>
                                            <th>Bil</th>
                                            <th>Nama Pelanggan</th>
                                            <th>Emel</th>
                                            <th>No. Kad Pengenalan</th>
                                            <th>No. Tel</th>
                                            <th>No. Tempahan</th>
                                            <th>Tarikh Penggunaan</th>
                                            <th>Nama Fasiliti</th>
                                            <th>Slot Masa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;  
                                        @endphp
                                        
                                        @if (count($data['slot']) > 0)
                                        @foreach ($data['slot'] as $s)
                                            <tr>
                                                <td style="width: 5%;">{{ $i++ }}</td>
                                                <td>{{ ucfirst($s->fullname) }}</td>
                                                <td>{{ $s->email }}</td>
                                                <td>{{ $s->bud_reference_id }}</td>
                                                <td>{{ $s->bud_phone_no }}</td>
                                                <td>{{ $s->bmb_booking_no }}</td>
                                                <td>{{ $s->ebf_start_date }}</td>
                                                <td>{{ $s->efd_name }}</td>
                                                <td class="text-right">
                                                    @foreach ($data['umum'] as $u)
                                                        @if ($u->fk_et_booking_facility == $s->id)
                                                            <li>{{ $u->est_slot_time }}</li>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                                <td colspan="10" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Body-->
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center gutter-b">
                                    <a href="{{ url('sport/eksportpdfmaklumatpelanggan',[Crypt::encrypt($data['id']),Crypt::encrypt($data['mula']),Crypt::encrypt($data['tamat'])]) }}" class="btn btn-sm btn-primary">Cetak PDF</a>
                                </div>
                            </div>
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.pelanggan')
@endsection
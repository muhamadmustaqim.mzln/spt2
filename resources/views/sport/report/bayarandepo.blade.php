@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Bayaran Deposit</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Bayaran Deposit</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                            <div class="card-body pt-2">
                                <hr>
                                <div class="form-group row">
                                    <div class="col-lg-3">
                                        <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Lokasi</option>
                                            @foreach ($data['location'] as $l)
                                                <option value="{{ $l->id }}" {{($l->id== $data['id']) ? 'selected' : ''}}>{{ $l->lc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select name="jeniskutipan" id="jeniskutipan" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Jenis Kutipan</option>
                                            <option value="1" {{ isset($data['jeniskutipan']) && $data['jeniskutipan'] == 1 ? 'selected' : '' }}>Kaunter</option>
                                            <option value="2" {{ isset($data['jeniskutipan']) && $data['jeniskutipan'] == 2 ? 'selected' : '' }}>Online</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select name="bayaran" id="kt_select2_4" class="form-control mt-3" required>
                                            <option value="">Sila Pilih Bayaran</option>
                                            @foreach ($data['bayaran'] as $b)
                                                <option value="{{ $b->id }}" {{($b->id == $data['mode']) ? 'selected' : ''}}>{{ $b->lpm_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class='input-group' id="kt_daterangepicker_2">
                                            <input type="text" name="tarikh" class="form-control mt-3"  placeholder="Tarikh Penggunaan" autocomplete="off" readonly required value="{{ $data['tarikh'] }}">
                                            <div class="input-group-append mt-3">
                                                <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-2">
                                        <input type="text" name="mula" class="form-control mt-3" id="kt_daterangepicker_4" placeholder="Tarikh Dari" autocomplete="off" required value="{{ isset($data['mula']) ? $data['mula'] : '' }}">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="tamat" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Hingga" autocomplete="off" required value="{{ isset($data['tamat']) ? $data['tamat'] : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row d-flex justify-content-center">
                                    <div class="col"></div>
                                    <div class="col">
                                        <div class="">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div>
                                    <div class="col"></div>
                                </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN BAYARAN DEPOSIT {{ strtoupper(Helper::location($data['id'])) }}  <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Dari : {{ $data['mula'] }} <br> Hingga : {{ $data['tamat'] }}</span></span>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                        <thead> 
                                            <tr>
                                                <th>Bil</th>
                                                <th>No. Tempahan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Nama Dewan/Kelengkapan</th>
                                                <th>Kod Fee</th>
                                                <th>Tarikh Transaksi</th>
                                                <th>No. Resit</th>
                                                {{-- <th>Tujuan</th> --}}
                                                <th>Jenis Bayaran</th>
                                                <th>Jumlah (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;  
                                                $total = 0;
                                            @endphp
                                            @if (count($data['payment']) > 0)
                                            @foreach ($data['payment'] as $payment)
                                                <tr>
                                                    <td style="width: 5%;">{{ $i++ }}</td>
                                                    {{-- <td>{{ Helper::get_noTempahan(Helper::getBhPaymentFpx($payment->fk_bh_payment_fpx)->fk_main_booking) }}</td> --}}
                                                    <td>{{ $payment->bmb_booking_no }}</td>
                                                    {{-- <td>{{ Helper::get_nama(Helper::get_userTempahan(Helper::getBhPaymentFpx($payment->fk_bh_payment_fpx)->fk_main_booking)) }}</td> --}}
                                                    <td>{{ $payment->fullname }}</td>
                                                    {{-- <td>{{ Helper::getHall($payment->fk_bh_hall) }}</td> --}}
                                                    <td>{{ $payment->efd_name }}</td>
                                                    {{-- <td>{{ Helper::getBhCodeDeposit($payment->fk_bh_hall) }}</td> --}}
                                                    <td>{{ $payment->efd_fee_code }}</td>
                                                    <td>
                                                        @if(($payment->lpm_description) === 'LO/PO')
                                                            {{ $payment->bp_receipt_date }}
                                                        @elseif(($payment->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpxDate($payment->id) }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(($payment->lpm_description) === 'LO/PO')
                                                            {{ $payment->bp_receipt_number }}
                                                        @elseif(($payment->lpm_description) === 'FPX')
                                                            {{ Helper::getEtPaymentFpx($payment->id) }}
                                                        @endif
                                                    </td>
                                                    {{-- <td>{{ Helper::get_lkp_event_string_laporan(Helper::get_kemudahan_terperinci_laporan(Helper::get_et_booking_facility($payment->id))) }}</td> --}}
                                                    <td>{{ $payment->lpm_description }}</td>
                                                    <td class="text-right">{{ $payment->ebf_subtotal }}</td>
                                                </tr>
                                                @php
                                                    $total += $payment->ebf_subtotal;
                                                @endphp
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="9" class="text-center text-danger">Harap Maaf, data tiada dalam sistem!</td>
                                            </tr>
                                            @endif
                                            {{-- <tr class="bg-dark">
                                                <td colspan="9" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                                                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                                                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                                            </tr> --}}
                                        </tbody>
                                        <tfoot>
                                            <tr class="bg-dark">
                                                <td colspan="8" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                                                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!--end::Body-->
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center gutter-b">
                                    <a href="{{ url('sport/eksportpdfdeposit',[Crypt::encrypt($data['id']),Crypt::encrypt($data['mode']),Crypt::encrypt($data['jeniskutipan']),Crypt::encrypt($data['mula']),Crypt::encrypt($data['tamat'])]) }}" class="btn btn-sm btn-primary">Cetak PDF</a>
                                </div>
                            </div>
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('sport.report.js.bayarandepo')
@endsection
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    // var table = $('table').DataTable({
    //     "bPaginate": false,
    //     "bLengthChange": false,
    //     "bFilter": true,
    //     "bInfo": false,
    //     language: {
    //         "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
    //         "zeroRecords": "Harap maaf, tiada rekod ditemui",
    //         "info": "Halaman _PAGE_ dari _PAGES_",
    //         "infoEmpty": "Tiada rekod dalam sistem",
    //         "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
    //         "sSearch": "Carian:"
    //     },
    //     rowGroup: {
    //         dataSrc: 1,
    //     },
    // } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function(){
        $("#form").submit(function(event) {
            console.log("Form submission event fired."); 
            var $form = $(this);
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            } else {
                form.submit();
            }
        });
        var groupColumn = 1;
        var table = $('#kt_datatable_2').DataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            },
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );
        $('#JenisAcaraTempDewan').on('change', function() {
            var selectedText = $(this).find('option:selected').text();
            var selectedValue = $(this).val();
            if (selectedText.trim() === 'Majlis Perkahwinan') {
                $('#pakejPerkahwinan').prop('hidden', false);
                $('#pakejPerkahwinanSelect').attr('required', true);
            } else {
                $('#pakejPerkahwinan').prop('hidden', true);
                $('#pakejPerkahwinanSelect').prop('required', false).removeAttr('required');
            }
        })
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: new Date(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
    
    // var maxAllowed = 2;
    var maxAllowed = 1;
    var current = 0;
    
    $('.chk').on('change', function () {
        var checkedCount = $('.chk:checked').length;

        if (checkedCount > maxAllowed) {
            $(this).prop('checked', false);
            Swal.fire(
                'Harap Maaf!',
                'Anda telah memilih terlalu banyak slot tempahan. Sila pilih maksimum ' + maxAllowed + ' slot.',
                'error'
            );
        }
    });
</script>
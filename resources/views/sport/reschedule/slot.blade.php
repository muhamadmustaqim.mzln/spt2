@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
		<!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body pt-0">
                    <form action="" method="post" id="">
                        @csrf
                        <div class="d-flex justify-content-between">
                            <div class="row">
                                <div class="col-6">
                                    <div class="input-icon">
                                        <input type="text" class="form-control form-control-lg font-weight-bold" id="kt_daterangepicker_5" name="tarikh" placeholder="Tukar Tarikh" autocomplete="off" required />
                                        <span>
                                            <i class="flaticon-calendar-3 icon-xl"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--begin: Datatable-->
                    <form action="{{ url('sport/reschedule/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date']), Crypt::encrypt($data['main']->bmb_booking_no), Crypt::encrypt($data['estid'])]) }}" method="post" id="form">
                        @csrf
                            <div class="float-right">
                                <button type="submit" id="submit_form" class="btn btn-primary font-weight-bolder mr-2 my-2"><i class="fas fa-check-circle"></i> Kemaskini Slot Masa</button>
                            </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>
                                            {{-- <label class="checkbox checkbox-primary checkbox-lg">
                                                <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                                <span></span>
                                            </label> --}}
                                        </th>
                                        <th>Nama Fasiliti</th>
                                        <th>Slot Masa</th>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Jenis Fasiliti</th>
                                        <th>Lokasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($data['slot']) > 0)
                                        @foreach($data['slot'] as $s)
                                            @php
                                                $current_datetime = new DateTime();
                                                $target_time = new DateTime($s->start);
                                                $dateToday = date('Y-m-d');
                                            @endphp
                                            <tr>
                                                @if ($data['date'] > $dateToday)
                                                    <td style="width: 4.5%;">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>{{ $s->efd_name }}</td>
                                                    <td>{{ $s->est_slot_time }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                                    <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                    <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                @elseif ($current_datetime > $target_time)
                                                    <td style="width: 4.5%;">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" disabled class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td><del>{{ $s->efd_name }}</del></td>
                                                    <td><del>{{ $s->est_slot_time }}</del></td>
                                                    <td><del>{{ date("d-m-Y", strtotime($data['date'])) }}</del></td>
                                                    <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                                    <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                                @else
                                                    <td style="width: 4.5%;">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>{{ $s->efd_name }}</td>
                                                    <td>{{ $s->est_slot_time }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                                    <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                    <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    {{-- @else
                                        @foreach($data['slot2'] as $s)
                                            @php
                                                $current_datetime = new DateTime();
                                                $target_time = new DateTime($s->start);
                                            @endphp
                                            <tr>
                                                @if ($current_datetime > $target_time)
                                                    <td style="width: 4.5%;">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" disabled class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td><del>{{ $s->efd_name }}</del></td>
                                                    <td><del>{{ $s->est_slot_time }}</del></td>
                                                    <td><del>{{ date("d-m-Y", strtotime($data['date'])) }}</del></td>
                                                    <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                                    <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                                @else
                                                    <td style="width: 4.5%;">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>{{ $s->efd_name }}</td>
                                                    <td>{{ $s->est_slot_time }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                                    <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                    <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                @endif
                                            </tr>
                                        @endforeach --}}
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    @include('sport.reschedule.js.slot')
@endsection



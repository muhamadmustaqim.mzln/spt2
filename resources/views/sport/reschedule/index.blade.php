@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Penjadualan Tempahan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
            <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <!--begin::List Widget 21-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Header-->
                                    <div class="card-header border-0 pt-5">
                                        <h3 class="card-title align-items-start flex-column mb-5">
                                            <span class="card-label font-weight-bolder text-dark mb-1">Carian Tempahan</span>
                                            <span class="text-muted mt-2 font-weight-bold font-size-sm"></span>
                                        </h3>
                                        <span class="mt-2 font-weight-bold font-size-sm">Masukkan No. Tempahan:</span>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Body-->
                                    <div class="card-body pt-2">
                                        <form action="" method="post">
                                            @csrf
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="text" name="id" class="form-control mt-3" autocomplete="off" required value="{{ strtoupper($data['id']) }}">
                                                </div>
                                            </div>
                                            <div class="float-right">
                                                <button type="submit" id="submit_form" class="btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 21-->
                            </div>
                            <div class="col-lg-8">
                                <!--begin::List Widget 21-->
                                @if ($data['post'] == true)
                                <div class="card card-custom gutter-b">
                                    <!--begin::Body-->
                                    <div class="card-body pt-2">
                                        <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                            <thead>
                                                <tr>
                                                    <th>No Tempahan</th>
                                                    <th>Tempahan Dibuat Oleh</th>
                                                    <th>Tarikh Tempahan</th>
                                                    <th>Status</th>
													<th>Tindakan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if (isset($data['main']) == true)
                                                <tr>
                                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                                    <td>{{ $data['user']->fullname }}</td>
                                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
													<td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                                    <td class="text-center">
                                                        <a href="{{url('/sport/reschedule/sport',Crypt::encrypt($data['main']->bmb_booking_no))}}" class="btn btn-dark mb-2">Penjadualan Semula</a>                                                        
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                @endif
                                <!--end::List Widget 21-->
                            </div>
                        </div>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->	
            </div>
@endsection

@section('js_content')
    @include('sport.reschedule.js.index')
@endsection
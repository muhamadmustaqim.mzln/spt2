<script>
        $('#kt_select2_1').select2({
            placeholder: 'Sila Pilih Fasiliti'
        });

        $('#kt_select2_2').select2({
            placeholder: 'Sila Pilih Lokasi'
        });

        $('#kt_select2_3').select2({
            placeholder: 'Sila Pilih Status'
        });
</script>
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        $("#output").css('display', 'block');
      }
    };
</script>
<script>
    var loadFile1 = function(event) {
      var output1 = document.getElementById('output1');
      output1.src = URL.createObjectURL(event.target.files[0]);
      output1.onload = function() {
        URL.revokeObjectURL(output1.src) // free memory
        $("#output1").css('display', 'block');
      }
    };
</script>
<script>
  $('#checkbox1').on('change', function() { 
      // From the other examples
      if (!this.checked) {
          $('#textbox1').text("Tidak Aktif");
      }else{
          $('#textbox1').text("Aktif");
      }
  });
</script>
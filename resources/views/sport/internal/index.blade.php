@extends('layouts.master')

@section('container')
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Dalaman</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPS</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dalaman</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <div class="row">
                                <div class="col-12">
                                    <!-- Navigation Bar::Start -->
                                    <ul class="nav nav-tabs nav-fill mx-2" id="myTab" role="tablist">
                                        <li class="nav-item position-relative" id="tempahanDewanNavItem">
                                            <a class="nav-link @if($data['tab'] == 1) active @endif pr-1" id="tempahanDewan-tab" data-toggle="tab" href="#tempahanDewan">
                                                <span class="nav-text">Tempahan Dewan</span>
                                            </a>
                                        </li>
                                        <li class="nav-item position-relative" id="tempahanSukanNavItem">
                                            <a class="nav-link @if($data['tab'] == 2) active @endif pr-1" id="tempahanSukan-tab" data-toggle="tab" href="#tempahanSukan" aria-controls="tempahanSukan">
                                                <span class="nav-text">Tempahan Sukan</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content mt-5" id="myTabContent">
                                        <div class="tab-pane fade show @if($data['tab'] == 1) active @endif" id="tempahanDewan" role="tabpanel" aria-labelledby="tempahanDewan-tab">
                                            <!--begin::Body-->
                                            <form action="" method="post">
                                                @csrf
                                                <input type="hidden" class="form-control form-control-lg" name="tab" value="1"/>
                                                <div class="card-body pt-2">
                                                    <hr>
                                                    <div class="form-group row">
                                                        <div class="col-lg-3">
                                                            <select name="jenis" id="" class="form-control mt-3" required>
                                                                <option selected disabled value="">Sila Pilih Jenis Tempahan</option>
                                                                <option value="1" @if($data['post'] == true) @if($data['tab'] == 1) @if($data['jenis'] == 1) selected @endif @endif @endif>Tempahan Rasmi</option>
                                                                <option value="2" @if($data['post'] == true) @if($data['tab'] == 1) @if($data['jenis'] == 2) selected @endif @endif @endif>Sekatan Tempahan</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <select name="lokasi" id="" class="form-control mt-3" required>
                                                                <option value="">Sila Pilih Lokasi</option>
                                                                @foreach ($data['location'] as $l)
                                                                    <option value="{{ $l->id }}" @if($data['post'] == true) @if($data['tab'] == 1) @if($l->id == $data['id']) selected @endif @endif @endif>{{ $l->lc_description }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="input-icon">
                                                                <input @if($data['post'] == true) @if($data['tab'] == 1) value="{{ Helper::date_format($data['date']) }}" @endif @endif type="text" class="form-control font-weight-bold mt-3" id="kt_daterangepicker_1" name="tarikh" placeholder="Tarikh" autocomplete="off" required />
                                                                <span>
                                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                        <div class="float-right">
                                                            <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </form>
                                            <!--end::Body-->
                                        </div>
                                        <div class="tab-pane fade show @if($data['tab'] == 2) active @endif" id="tempahanSukan" role="tabpanel" aria-labelledby="tempahanSukan-tab">
                                            <!--begin::Body-->
                                            <form action="" method="post">
                                                @csrf
                                                <input type="hidden" class="form-control form-control-lg" name="tab" value="2"/>
                                                <div class="card-body pt-2">
                                                    <hr>
                                                    <div class="form-group row">
                                                        <div class="col-lg-3">
                                                            <select name="jenis" id="" class="form-control mt-3" required>
                                                                <option selected disabled value="">Pilih Jenis Tempahan</option>
                                                                <option value="1" @if($data['post'] == true) @if($data['tab'] == 2) @if($data['jenis'] == 1) selected @endif @endif @endif>Tempahan Rasmi</option>
                                                                <option value="2" @if($data['post'] == true) @if($data['tab'] == 2) @if($data['jenis'] == 2) selected @endif @endif @endif>Sekatan Tempahan</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                                <option value="">Pilih Lokasi</option>
                                                                @foreach ($data['location'] as $l)
                                                                    <option value="{{ $l->id }}" @if($data['post'] == true) @if($data['tab'] == 2) @if(isset($data['id'])) @if($data['id'] == $l->id) selected @endif @endif @endif @endif>{{ $l->lc_description }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <select name="lokasi" class="form-control font-weight-bold mt-3" id="kt_select2_5" required disabled>
                                                                <option value=""> Pilih Jenis Kemudahan Sukan</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <div class="input-icon">
                                                                <input @if($data['post'] == true) @if($data['tab'] == 2) value="{{ Helper::date_format($data['date']) }}" @endif @endif type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_2" name="tarikh" placeholder="Tarikh" autocomplete="off" required />
                                                                <span>
                                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <div class="float-right">
                                                                <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </form>
                                            <!--end::Body-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        @if ($data['post'] == true)
                            @if ($data['tab'] == 1)
                                <!--begin::Card-->
                                <div id="tempahanDewanContent">
                                    {{-- <form action="{{ url('sport/internal/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
                                        @csrf --}}
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                                <h3 class="card-title align-items-start flex-column mb-5">
                                                    <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                                                </h3>
                                                <div class="card-toolbar">
                                                    <!--begin::Button-->
                                                    {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                                                    <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                                        <i class="flaticon-circle"></i> Batal Tempahan
                                                    </a> --}}
                                                    <!--end::Button-->
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <!--begin: Datatable-->
                                                <div class="table-responsive mb-5">
                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th>Senarai Dewan Telah Ditempah</th>
                                                                <th>Nama Penempah</th>
                                                                <th>No Tempahan</th>
                                                                <th style="width: 15%">Tindakan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['bookedList'] as $s)
                                                                <tr>
                                                                    <td style="width: 25%">{{ Helper::getSportFacility($s->fk_et_facility_type) }}</td>
                                                                    <td style="width: 25%">{{ $s->fullname }}</td>
                                                                    <td style="width: 20%"><a href="{{ url('sport/maklumatbayaran', Crypt::encrypt($s->fk_main_booking)) }}" target="_blank" rel="noopener noreferrer">{{ Helper::get_noTempahan($s->fk_main_booking) }}</a></td>
                                                                    <td class="text-center">
                                                                        <div class="row">
                                                                            <div class="col-6 p-0 d-flex justify-content-end">
                                                                                <a target="_blank" href="{{ url('sport/reschedule/sport', Crypt::encrypt(Helper::get_noTempahan($s->fk_main_booking))) }}" class="btn btn-sm font-weight-bold btn-light-primary m-1">Penjadualan Semula</a>
                                                                            </div>
                                                                            <div class="col-6 p-0 d-flex justify-content-start">
                                                                                <form action="{{url('/sport/cancel')}}" method="post">
                                                                                    @csrf
                                                                                    <input type="text" value="{{ $s->bmb_booking_no }}" name="id" hidden>
                                                                                    <button type="submit" class="btn btn-sm font-weight-bold btn-light-warning m-1">Batal Tempahan</button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th>Senarai Dewan Kosong</th>
                                                                <th class="w-25">Lokasi</th>
                                                                <th style="width: 15%">Pilih</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['availableList'] as $s)
                                                                <tr>
                                                                    <td>{{ $s->nama }}</td>
                                                                    <td>{{ $s->lokasi }}</td>
                                                                    <td>
                                                                        <a href="{{ url('/sport/internal/purpose', [Crypt::encrypt($s->id), Crypt::encrypt($data['date'])]) }}" class="btn btn-primary">Buat Tempahan</a>
                                                                    </td>                                                                
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--end: Datatable-->
                                            </div>
                                        </div>
                                </div>
                                <!--end::Card-->
                            @else
                                <!--begin::Card-->
                                <div id="tempahanSukanContent">
                                    <form action="{{ url('sport/internal/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date']), Crypt::encrypt($data['jenis'])]) }}" method="post" id="formSukan">
                                        @csrf
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                                <h3 class="card-title align-items-start flex-column mb-5">
                                                    <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                                                </h3>
                                                <div class="card-toolbar">
                                                    <!--begin::Button-->
                                                    <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                                                    <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                                        <i class="flaticon-circle"></i> Batal Tempahan
                                                    </a>
                                                    <!--end::Button-->
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <!--begin: Datatable-->
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <label class="checkbox checkbox-primary checkbox-lg">
                                                                        <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                                                        <span></span>
                                                                    </label>
                                                                </th>
                                                                <th>Nama Fasiliti</th>
                                                                <th>Slot Masa</th>
                                                                <th>Tarikh Penggunaan</th>
                                                                <th>Jenis Fasiliti</th>
                                                                <th>Lokasi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['slot'] as $s => $slot)
                                                                @foreach ($slot as $item)
                                                                @php
                                                                    $current_datetime = new DateTime();
                                                                    $target_time = new DateTime($item->start);
                                                                    $data_date = DateTime::createFromFormat('d-m-Y', $data['date'])->format('Y-m-d');
                                                                @endphp
                                                                @if ($data_date > $current_datetime->format('Y-m-d'))
                                                                    <tr>
                                                                        <td style="width: 4.5%;">
                                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                                <input type="checkbox" name="slot[]" value="{{ $item->fidd }},{{ $item->fkst }},{{ $s }},{{ $item->sesi }},{{ $item->harga }},{{ $item->gst }},{{ $item->cidd }}"/>
                                                                                <span></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>{{ $item->efd_name }}</td>
                                                                        <td>{{ $item->est_slot_time }}</td>
                                                                        <td>{{ date("d-m-Y", strtotime($s)) }}</td>
                                                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                                    </tr>
                                                                @elseif ($current_datetime > $target_time)
                                                                    <tr>
                                                                        <td style="width: 4.5%;">
                                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                                <input type="checkbox" disabled name="slot[]" value="{{ $item->fidd }},{{ $item->fkst }},{{ $s }},{{ $item->sesi }},{{ $item->harga }},{{ $item->gst }},{{ $item->cidd }}"/>
                                                                                <span></span>
                                                                            </label>
                                                                        </td>
                                                                        <td><del>{{ $item->efd_name }}</del></td>
                                                                        <td><del>{{ $item->est_slot_time }}</del></td>
                                                                        <td><del>{{ date("d-m-Y", strtotime($s)) }}</del></td>
                                                                        <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                                                        <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                                                    </tr>
                                                                @else
                                                                    <tr>
                                                                        <td style="width: 4.5%;">
                                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                                <input type="checkbox" name="slot[]" value="{{ $item->fidd }},{{ $item->fkst }},{{ $s }},{{ $item->sesi }},{{ $item->harga }},{{ $item->gst }},{{ $item->cidd }}"/>
                                                                                <span></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>{{ $item->efd_name }}</td>
                                                                        <td>{{ $item->est_slot_time }}</td>
                                                                        <td>{{ date("d-m-Y", strtotime($s)) }}</td>
                                                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                                    </tr>
                                                                @endif
                                                                @endforeach
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--end: Datatable-->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--end::Card-->
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('js_content')
    @include('sport.internal.js.index')
    {{-- @include('event.public.js.reservation') --}}
@endsection
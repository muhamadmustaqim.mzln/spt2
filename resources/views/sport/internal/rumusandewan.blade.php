@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    {{-- <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5> --}}
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Pengurusan Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Tempahan Dalaman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Maklumat Pemohon-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                    <td>
                                        @if($data['main']->bmb_deposit_rm == null)
                                            0.00
                                        @else 
                                            {{ ($data['main']->bmb_deposit_rm) }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                            {{-- <a href="{{ url('hall/borangbpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Borang BPSK</a> --}}
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Maklumat Pemohon-->
            <!--begin::Card-->
            <form action="" method="post" id="form">
                @csrf
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Tempahan Fasiliti</span>
                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                        </h3>
                        <div class="card-toolbar">
                            <a href="{{ url('/sport/cancel/submit', Crypt::encrypt($data['mbid'])) }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                <i class="flaticon-circle"></i> Batal Tempahan
                            </a>
                            <a type="submit" href="{{ url('/sport/internal/tambahkegunaandewan', Crypt::encrypt($data['mbid'])) }}" class="btn btn-light-warning font-weight-bolder mx-2 mt-2"><i class="fas fa-check-circle"></i> Tambah Kegunaan</a>
                            <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                            {{-- <a href="{{ url('/hall/batal', [Crypt::encrypt($data['book'])]) }}" class="btn btn-light-danger font-weight-bolder mt-2"> --}}
                            {{-- <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                <i class="flaticon-circle"></i> Batal Tempahan
                            </a> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Lokasi</th>
                                        @if ($data['et_hall_book'][0] != null && $data['et_hall_book'][0]->fk_et_function != 13)
                                            <th>Jenis Penggunaan</th>
                                        @endif
                                        <th>Tarikh Kegunaan Dari</th>
                                        <th>Tarikh Kegunaan Hingga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        {{-- <tr>
                                            <td>{{ Helper::tempahanSportType($data['et_booking_facility']->fk_et_facility_type) }}</td>
                                            <td>{{ Helper::location($data['et_facility_type']->fk_lkp_location) }}</td>
                                            @if ($data['et_hall_book'] != null && $data['et_hall_book']->fk_et_function != 13)
                                                <td>{{ Helper::typeFunction($data['et_hall_book']->fk_et_function) }}</td>
                                            @endif
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_start_date) }}</td>
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_end_date) }}</td>
                                        </tr> --}}
                                    @foreach($data['et_booking_facility'] as $s)
                                    <tr>
                                        <td>{{ Helper::tempahanSportType($s->fk_et_facility_type) }}</td>
                                        <td>{{ Helper::location($data['et_facility_type']->fk_lkp_location) }}</td>
                                        @if ($data['et_hall_book'][0] != null && $data['et_hall_book'][0]->fk_et_function != 13)
                                            <td>{{ Helper::typeFunction($data['et_hall_book'][0]->fk_et_function) }}</td>
                                        @endif
                                        <td>{{ Helper::date_format($s->ebf_start_date) }}</td>
                                        <td>{{ Helper::date_format($s->ebf_end_date) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Slot Masa</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="row d-flex justify-content-center">
                            {{-- <div class="col-8"> --}}
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Dewan</th>
                                                <th>Tarikh</th>
                                                <th>Jenis Kegunaan</th>
                                                <th>Slot Kegunaan</th>
                                                {{-- <th class="text-right">Harga (RM)</th> --}}
                                                @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                    <th class="text-right">Tindakan</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $totalHall = 0;
                                                $totalEq = 0;
                                            @endphp
                                            @foreach ($data['et_booking_facility'] as $item1)
                                                @php
                                                    $ebfid = $item1->id;
                                                    $count = 1;
                                                @endphp
                                                @foreach ($data['et_hall_book'] as $item)
                                                    @if($item->fk_et_booking_facility == $ebfid)
                                                        @php
                                                            $i = 0;
                                                            $ehbid = $item->id;
                                                        @endphp
                                                        {{-- @foreach ($data['et_hall_book'] as $w)
                                                            @foreach ($data['et_hall_time'] as $e)
                                                                @if ($e->fk_et_hall_book == $w->id)
                                                                    @if ($w->ehb_booking_date == $item1->ebf_start_date)
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endforeach --}}
                                                        @foreach($data['et_hall_time'] as $s)
                                                            @if ($s->fk_et_hall_book == $ehbid)
                                                                @php
                                                                    $totalHall += $s->eht_subtotal;
                                                                @endphp
                                                                <tr>
                                                                    {{-- <td>{{ Helper::tempahanSportType($item1->fk_et_facility_type) }}  {{ $count }}</td>
                                                                    <td>{{ Helper::date_format($item1->ebf_start_date) }}</td> --}}
                                                                    {{-- @if ($i == 0) --}}
                                                                    <td rowspan="{{$count}}">{{ Helper::tempahanSportDetail($item1->fk_et_facility_detail) }}</td>
                                                                        <td rowspan="{{$count}}">{{ Helper::date_format($item1->ebf_start_date) }}</td>
                                                                    {{-- @endif --}}
                                                                    <td>{{ Helper::typeFunction($item->fk_et_function) }}</td>
                                                                    <td>
                                                                        {{ Helper::get_timeslot($s->fk_et_slot_time) }}<br>
                                                                    </td>
                                                                    {{-- <td class="text-right">{{ $s->eht_subtotal }}</td> --}}
                                                                    @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                                        <td class="text-right">
                                                                            <a href="{{ url('/sport/external/delete/slotdewan', [Crypt::encrypt($data['mbid']), Crypt::encrypt($s->id)]) }}" class="btn btn-primary btn-sm mx-auto">Hapus</button>
                                                                        </td>
                                                                    @endif
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            {{-- <tr>
                                                <td class="text-right" colspan="4"><b>Jumlah</b></td>
                                                <td class="text-right"><b>{{ Helper::moneyhelper($totalHall) }}</b></td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                    {{-- <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Dewan</th>
                                                <th>Tarikh</th>
                                                <th>Slot Kegunaan</th>
                                                @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                    <th class="text-right">Tindakan</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 0;
                                            @endphp
                                            @foreach($data['et_hall_time'] as $s)
                                                <tr>
                                                    @if ($i == 0)
                                                    <td rowspan="{{count($data['et_hall_time'])}}">{{ Helper::tempahanSportType($data['et_booking_facility'][0]->fk_et_facility_type) }}</td>
                                                    <td rowspan="{{count($data['et_hall_time'])}}">{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                    @endif
                                                    <td>
                                                        {{ Helper::get_timeslot($s->fk_et_slot_time) }}<br>
                                                    </td>
                                                    @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                        <td class="text-right">
                                                            <a href="{{ url('/sport/external/delete/slotdewan', [Crypt::encrypt($data['mbid']), Crypt::encrypt($s->id)]) }}" class="btn btn-primary btn-sm mx-auto">Hapus</button>
                                                        </td>
                                                    @endif
                                                </tr>
                                                @php
                                                    $i++
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table> --}}
                                </div>
                            {{-- </div> --}}
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Kelengkapan Dewan</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Kegunaan</th>
                                        <th>Item</th>
                                        <th>Kuantiti</th>
                                        <th>Tarikh Penggunaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['et_equipment_book'] as $s)
                                        <tr>
                                            <td>{{ Helper::typeFunction($s->fk_et_function) }}</td>
                                            <td>{{ Helper::get_equipment($s->fk_et_equipment) }}</td>
                                            <td>{{ $s->eeb_quantity }}</td>
                                            <td>{{ Helper::date_format($s->eeb_booking_date) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    {{-- <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Kelengkapan Dewan</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Kegunaan</th>
                                        <th>Item</th>
                                        <th>Kuantiti</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data['et_equipment_book'] as $s)
                                        <tr>
                                            <td>{{ Helper::date_format($s->eeb_booking_date) }}</td>
                                            <td>{{ Helper::typeFunction($s->fk_et_function) }}</td>
                                            <td>{{ Helper::get_equipment($s->fk_et_equipment) }}</td>
                                            <td>{{ $s->eeb_quantity }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Pilihan Slot</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="row d-flex justify-content-center">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Dewan</th>
                                            <th>Lokasi</th>
                                            <th>Tarikh Kegunaan</th>
                                            <th>Waktu Kegunaan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['et_hall_time'] as $s)
                                            <tr>
                                                <td>{{ Helper::tempahanSportType($data['et_booking_facility']->fk_et_facility_type) }}</td>
                                                <td>{{ Helper::location($data['et_facility_type']->fk_lkp_location) }}</td>
                                                <td>{{ $s->eht_start }}</td>
                                                <td>{{ Helper::get_timeslot($s->fk_et_slot_time) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end: Datatable-->
                    </div> --}}
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    {{-- @include('hall.public.js.slot') --}}
    {{-- @include('sport.public.js.slot') --}}
@endsection




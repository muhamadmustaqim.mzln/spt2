@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dalaman</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <select disabled name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Sila Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}"@if(isset($data['lkp_location'])) @if($l->id == $data['lkp_location']) selected @endif @endif>{{ $l->lc_description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-icon">
                                                <input disabled type="text" class="form-control font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" required value="{{ $data['date'] }}" />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                        <div class="float-right">
                                            <button disabled type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                        </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-12">
                        @if ($data['post'] == true)
                        <!--begin::Card-->
                            <form action="" method="post" id="form" enctype="multipart/form-data">
                                @csrf
                                <input type="text" value="0" name="multipurpose" hidden>
                                <div class="card card-custom gutter-b">
                                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                        <h3 class="card-title align-items-start flex-column mb-5">
                                            <span class="card-label font-weight-bolder text-dark">Pilih Kegunaan</span>
                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ Helper::date_format($data['date']) }}</span>
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        {{-- <div class="form-group row">
                                            <label  class="col-2 col-form-label">Pilih Kategori</label>
                                            <div class="col-6">
                                                <select required class="form-control" id="JenisAcaraTempDewan" name="JenisAcaraTempDewan">
                                                    @foreach ($data['category'] as $c)
                                                        <option value="{{ $c->id }}">{{ $c->ldt_user_cat }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div> --}}
                                        <div class="form-group row kjCls" id="" hidden>
                                            <div class="col-2"></div>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Kementerian / Jabatan</span></div>
                                                    <input type="text" class="form-control" name="kementerianjabatan" placeholder="" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row sajCls" id="" hidden>
                                            <div class="col-2"></div>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Id Staf</span></div>
                                                    <input type="text" class="form-control" name="staffid" placeholder="" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row sajCls" id="staffidDiv" hidden>
                                            <div class="col-2"></div>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Agensi / Jabatan</span></div>
                                                    <input type="text" class="form-control" name="agensijabatan" placeholder="" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-2 col-form-label">Muat Naik Lampiran / Surat Sokongan</label>
                                            <div class="col-6">
                                                <div class="input-group">
                                                    <input name="suratAkuan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="">
                                                    {{-- <label for="fileUpload1" class="btn btn-primary btn-file-upload" >Muat Naik</label> --}}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <form action="" method="post">
                                            @csrf --}}
                                            <!--begin: Datatable-->
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="kt_datatable_2">
                                                    <thead>
                                                        <tr>
                                                            <th>Kegunaan</th>
                                                            <th style="width: 15%">Pilih</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if ($data['result'] == 1 || $data['result'] == 10)
                                                            <input hidden type="text" name="et_facility_type_id" value="{{ $data['checkroom'][0]->id }}">
                                                            @foreach ($data['slot1'] as $s)
                                                                <tr>
                                                                    <td>{{ $s['efd_name'] }}</td>
                                                                    <td class="text-center"><input required value="{{ $s['id'] }}" type="radio" id="star5" name="et_function_id" /></td>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            @foreach ($data['checkroom'] as $s)
                                                                <tr>
                                                                    <td>{{ $s->eft_type_desc }}</td>
                                                                    <td class="text-center"><input required value="{{ $s->id }}" type="radio" id="star5" name="et_facility_type_id" /></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                            <input hidden type="text" name="id" value="{{ $data['id'] }}">
                                            <input hidden type="text" name="date" value="{{ $data['date'] }}">
                                            <div class="float-right pt-5">
                                                {{-- {{ url('/sport/internal/slot', [Crypt::encrypt(1), Crypt::encrypt($data['checkroom'][0]->id)]) }} --}}
                                                {{-- <a href="{{ url('/sport/internal') }}" class="btn btn-primary font-weight-bold mx-1">Kembali</a> --}}
                                                <button type="button" id="kembali" class="btn btn-primary font-weight-bold mx-1">Kembali</button>
                                                @if (count($data['slot1']) > 0 || $data['checkroom'][0]->eft_type_desc == 'Tempahan Peralatan')
                                                    <button type="text" id="" class=" btn btn-primary font-weight-bold">Teruskan Tempahan</button>
                                                @endif
                                            </div>
                                        {{-- </form> --}}
                                        <!--end: Datatable-->
                                    </div>
                                </div>
                            </form>
                            <form action="{{ url('/sport/internal') }}" method="POST" id="mainForm" enctype="multipart/form-data">
                                @csrf <!-- Laravel CSRF protection token -->
                                <input type="hidden" name="tab" value="1">
                                <input type="hidden" name="jenis" value="1">
                                <input type="hidden" name="lokasi" value="{{$data['lkp_location']}}">
                                <input type="hidden" name="tarikh" value="{{$data['date']}}">
                            </form>
                        <!--end::Card-->
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        	

@endsection

@section('js_content')
    @include('sport.external.js.purpose')
@endsection
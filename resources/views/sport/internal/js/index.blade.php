<script>
    // date picker
    var maxDate = moment().add(12, 'months'); // Calculate the maximum date

    $('#kt_daterangepicker_1').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_daterangepicker_1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_daterangepicker_1').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#kt_daterangepicker_2').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#kt_daterangepicker_4').daterangepicker({
        autoApply: true,
        autoclose: true,
        minDate: new Date(),
    }, function(start) {
        $('#kt_daterangepicker_4 .form-control').val( start.format('YYYY-MM-DD'));
    });
    $('#kt_daterangepicker_5').daterangepicker({
        autoApply: true,
        autoclose: true,
        minDate: new Date(),
    }, function(start, end, label) {
        $('#kt_daterangepicker_5 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    });
</script>
<script>
    $('#kt_select2_4').on('change', function(){
        var location_id = $(this).val();
        var kaunter = 1;
        if (location_id == ''){
            $('#kt_select2_5').prop('disabled', true);
        }
        else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('sport/ajax') }}",
                type: "POST",
                data: {
                    'location_id': location_id,
                    'kaunter': kaunter
                },
                dataType: 'json',
                success: function(data){
                    $('#kt_select2_5').html(data);
                }
            });
        }
    });
        // $('#kt_select2_4').on('change', function(){
        //     var location_id = $(this).val();
        //     if (location_id == ''){
        //         $('#kt_select2_5').prop('disabled', true);
        //     }
        //     else{
        //         $('#kt_select2_5').prop('disabled', false);
        //         $.ajax({
        //             url:"{{ url('sport/ajax') }}",
        //             type: "POST",
        //             data: {'location_id' : location_id},
        //             dataType: 'json',
        //             success: function(data){
        //                 $('#kt_select2_5').html(data);
        //             }
        //         });
        //     }
        // });
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    var table = $('table').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        },
        // rowGroup: {
        //     dataSrc: 1,
        // },
    } );

    $('.chk').on('click', function() {
        var id = $(this).attr('class').split(' ')[1]; // Get the ID of the checkbox
        var selectElement = $('#jenispenggunaan_' + id);
        selectElement.prop('disabled', !$(this).prop('checked')); 
    })

    $('#example-select-all').on('click', function(){
        // Check/uncheck all checkboxes in the table
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);

        $('tbody .jenispenggunaan').prop('disabled', !this.checked);
   });
   
   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function() {
        var groupColumn = 4;
        var table = $('#example').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );

        $('#tempahanDewan-tab').on('click', function() {
            $('#tempahanDewanContent').removeAttr('hidden');
            $('#tempahanSukanContent').prop('hidden', true);
        })
        $('#tempahanSukan-tab').on('click', function() {
            $('#tempahanSukanContent').removeAttr('hidden');
            $('#tempahanDewanContent').prop('hidden', true);
        })
    
        // Order by the grouping
        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
    } );
</script>
<script>
    $(document).ready(function(){
        $("#formSukan").submit(function( event ) {
            event.preventDefault();

            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            // var isSelectEnabled = true;
        
            // $('input[name="slot[]"]:checked').each(function() {
            //     var id = $(this).attr('class').split(' ')[1]; 
            //     var selectElement = $('#jenispenggunaan_' + id);
                
            //     if (selectElement.prop('disabled') || !selectElement.val()) {
            //         isSelectEnabled = true;
            //         return; // Exit the loop early
            //     }
            // });

            if (!atLeastOneIsChecked ) {
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }else{
                this.submit();
            }
        });
    });
    
</script>
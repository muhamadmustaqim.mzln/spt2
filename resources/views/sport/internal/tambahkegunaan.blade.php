@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Tempahan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dalaman</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
         <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::List Widget 21-->
                <div class="card card-custom gutter-b">
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <table class="table table-bordered">
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                <td>{{ strtoupper($data['user']->fullname) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                <td>{{ $data['user_detail']->bud_phone_no }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                <td>{{ $data['user']->email }}</td>
                            </tr>
                        </table>
                        <hr>
                        <form action="" method="post">
                            @csrf
                            <input type="text" name="type" value="1" hidden>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <select disabled name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                        @foreach ($data['location'] as $l)
                                            <option selected value="{{ $l->id }}">{{ $l->lc_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <input disabled type="text" class="form-control mt-3" id="" value="{{ $data['fasility']->eft_type_desc }}">
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh" autocomplete="off" required>
                                </div>
                                <div class="col-lg-2">
                                <div class="float-right">
                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mr-2 mt-3">Carian</button>
                                </div>
                                </div>
                            </div>
                        </form>
                        <hr>
                    </div>
                    <!--end::Body-->
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Nama Fasiliti</th>
                                        <th>Slot Masa</th>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Jenis Fasiliti</th>
                                        <th>Lokasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($data['slotcurrent'] as $s)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                        <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                        <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                        <td>{{ Helper::date_format($s->ebf_start_date) }}</td>
                                        <td>{{ Helper::tempahanSportType($s->fk_et_facility_type) }}</td>
                                        <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::List Widget 21-->
                <div class="col-lg-12">
                    @if ($data['post'] == true)
                    <!--begin::Card-->
                    <form action="" method="post" id="form">
                        @csrf
                        <input type="text" name="type" value="2" hidden>
                        <div class="card card-custom gutter-b">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti - <span class="font-weight-bold font-size-md">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</span></span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ date("d F Y", strtotime($data['date'])) }}</span>
                                </h3>
                                <div class="card-toolbar">
                                    <!--begin::Button-->
                                    {{-- <a href="#" type="button" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Kembali</a> --}}
                                    <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                                    <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                        <i class="flaticon-circle"></i> Batal Tempahan
                                    </a>
                                    <!--end::Button-->
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Nama Fasiliti</th>
                                                <th>Slot Masa</th>
                                                <th>Tarikh Penggunaan</th>
                                                <th>Jenis Fasiliti</th>
                                                <th>Lokasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['slot'] as $s)
                                                @php
                                                    $current_datetime = new DateTime();
                                                    $target_time = new DateTime($s->start);
                                                @endphp
                                                <tr>
                                                    @if ($data['date'] > $current_datetime->format('Y-m-d'))
                                                        <td style="width: 4.5%;">
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td>{{ $s->efd_name }}</td>
                                                        <td>{{ $s->est_slot_time }}</td>
                                                        <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                    @elseif ($current_datetime > $target_time)
                                                        <td style="width: 4.5%;">
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" disabled class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td><del>{{ $s->efd_name }}</del></td>
                                                        <td><del>{{ $s->est_slot_time }}</del></td>
                                                        <td><del>{{ date("d-m-Y", strtotime($data['date'])) }}</del></td>
                                                        <td><del>{{ $data['fasility']->eft_type_desc }}</del></td>
                                                        <td><del>{{ Helper::location($data['fasility']->fk_lkp_location) }}</del></td>
                                                    @else
                                                        <td style="width: 4.5%;">
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" class="chk {{ $s->fidd }}" name="slot[]" value="{{ $s->fidd }},{{ $s->fkst }},{{ $data['date'] }},{{ $s->sesi }},{{ $s->harga }},{{ $s->gst }},{{ $s->cidd }}"/>
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                        <td>{{ $s->efd_name }}</td>
                                                        <td>{{ $s->est_slot_time }}</td>
                                                        <td>{{ date("d-m-Y", strtotime($data['date'])) }}</td>
                                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                    </form>
                    <!--end::Card-->
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
</div>	

@endsection

@section('js_content')
    @include('sport.external.js.sport')
@endsection
@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                            <td>{{ $data['user']->fullname }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                            <td>{{ $data['main']->bmb_booking_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                            <td>{{ date('d-m-Y') }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                            <td>{{ $data['user']->email }}</td>
                        </tr>
                    </table>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        {{-- <button type="button" class="btn btn-light-info font-weight-bolder mr-2 mt-2"><i class="flaticon-add-circular-button"></i> Tambah Kegunaan</button> --}}
                        <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <span class="card-label font-weight-bolder text-dark h5">Dewan Pilihan</span>
                    <div class="table-responsive pt-4 pb-8">
                        <table class="table table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama</th>
                                    <th>Lokasi</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Jenis Penggunaan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $data['hall']->eft_type_desc }}</td>
                                    <td>{{ Helper::location($data['hall']->fk_lkp_location) }}</td>
                                    <td>{{ Helper::date_format($data['date']) }}</td>
                                    <td>{{ $data['function']->ef_desc }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <form action="" method="post" id="form">
                        @csrf
                        <span class="card-label font-weight-bolder text-dark h5">Maklumat Acara</span>
                        <div class="row pt-4 pb-8 d-flex justify-content-center">
                            <div class="col-10">
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Jenis Acara</label>
                                    <div class="col-6">
                                        <select name="lkp_event" class="form-control form-control-md">
                                            @foreach ($data['event'] as $e)
                                                <option value="{{ $e->id }}">{{ $e->le_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Nama Acara</label>
                                    <div class="col-7">
                                        <input name="namaAcara" class="form-control" placeholder="" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Keterangan Acara</label>
                                    <div class="col-7">
                                        <textarea name="keteranganAcara" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Nama Pemohon</label>
                                    <div class="col-7">
                                        <input name="namaPemohon" class="form-control" placeholder="" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-3 col-form-label">Nombor Pemohon</label>
                                    <div class="col-3">
                                        <input name="nomborPemohon" class="form-control" placeholder="" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Bilangan Peserta</label>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">VVIP</span></div>
                                            <input name="vvip" class="form-control" placeholder="Jumlah" value="" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">VIP</span></div>
                                            <input name="vip" class="form-control" placeholder="Jumlah" value="" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Peserta Lain</span></div>
                                            <input name="peserta" class="form-control" placeholder="Jumlah" value="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <span class="card-label font-weight-bolder text-dark h5">Senarai Slot Kegunaan</span>
                        <div class="row pt-4 pb-8 d-flex justify-content-center">
                            <div class="col-10">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="table1">
                                        <thead>
                                            <tr>
                                                <th>Waktu</th>
                                                <th class="text-center">Pilih Semua :&nbsp;
                                                    <label class="checkbox checkbox-primary checkbox-lg" style="display: inline-block; vertical-align: middle;">
                                                        <input type="checkbox" id="slotAll" class="group-checkable"/>
                                                        <span></span>
                                                    </label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['slotTime'] as $st)
                                                <tr>
                                                    <td>{{ $st->masa }}</td>
                                                    <td class="d-flex justify-content-center">
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" name="slot[]" value="{{ $st->efpid }}" class="group-checkable slot"/>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <button type="submit" class="float-right btn btn-primary font-weight-bolder mr-2 mt-2"><i class="flaticon-add-circular-button"></i> Teruskan Tempahan</button>
                        <!--end: Datatable-->
                        {{-- <a href="{{ url('/sport/internal/rumusan', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary font-weight-bolder mt-2 float-right">
                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                        </a> --}}
                    </form>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection


@section('js_content')
    @include('sport.internal.js.tempahan')
@endsection
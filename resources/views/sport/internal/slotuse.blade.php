@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    {{-- <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5> --}}
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Pengurusan Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Tempahan Dalaman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="" method="post" id="form" enctype="multipart/form-data">
                @csrf
                {{-- <input type="hidden" name="bbd_start_date" value="{{ $data['start'] }}">
                <input type="hidden" name="bbd_end_date" value="{{ $data['end'] }}">
                <input type="hidden" name="book" value="{{ $data['book'] }}"> --}}
                <input type="hidden" name="filename" id="filename" value="">
                <input type="hidden" name="filesize" id="filesize" value="">
                <input type="hidden" name="filetype" id="filetype" value="">
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                        <td>{{ $data['user']->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                        <td>{{ $data['main']->bmb_booking_no }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                        <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                        <td>
                                            {{ $data['user_detail']->bud_phone_no }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                        <td>{{ $data['user']->email }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                        <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                        <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Tempahan Fasiliti</span>
                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                        </h3>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                            {{-- <a href="{{ url('/hall/batal', [Crypt::encrypt($data['book'])]) }}" class="btn btn-light-danger font-weight-bolder mt-2"> --}}
                            <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                <i class="flaticon-circle"></i> Batal Tempahan
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Lokasi</th>
                                        <th>Tarikh Kegunaan Dari</th>
                                        <th>Tarikh Kegunaan Hingga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach($data['et_booking_facility'] as $s) --}}
                                        <tr>
                                            <td>{{ Helper::tempahanSportType($data['et_booking_facility']->fk_et_facility_type) }}</td>
                                            <td>{{ Helper::location($data['et_facility_type']->fk_lkp_location) }}</td>
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_start_date) }}</td>
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_end_date) }}</td>
                                        </tr>
                                    {{-- @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-header flex-wrap border-0 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Maklumat Tempahan</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Jenis Acara</label>
                            <div class="col-6">
                                <select required class="form-control" id="JenisAcaraTempDewan" name="JenisAcaraTempDewan">
                                    <option selected disabled value="">Sila Pilih Jenis Acara</option>
                                    @foreach ($data['event'] as $e)
                                        <option value="{{ $e->id }}" @if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->fk_lkp_event == $e->id) selected @endif>{{ $e->le_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-2 col-form-label">Jenis Acara</label>
                            <div class="col-6">
                                <select required class="form-control" id="JenisAcaraTempDewan" name="JenisAcaraTempDewan">
                                    <option selected disabled value="">Sila Pilih Jenis Acara</option>
                                    <?php
                                    // Extract the options
                                    $options = [];
                                    foreach ($data['event'] as $e) {
                                        $options[$e->id] = $e->le_description;
                                    }
                                    
                                    // Sort the options alphabetically
                                    asort($options);
                                    
                                    // Render the sorted options
                                    foreach ($options as $id => $description) {
                                        echo '<option value="' . $id . '">' . $description . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> --}}
                        @php
                            $i = 1;
                        @endphp
                        <div hidden class="form-group row" id="pakejPerkahwinan">
                            <label  class="col-2 col-form-label">Pakej Perkahwinan</label>
                            <div class="col-10">
                                <select class="form-control" id="pakejPerkahwinanSelect" name="pakejPerkahwinan">
                                    <option selected disabled value="">Sila Pilih Pakej Perkahwinan</option>
                                    {{-- @foreach ($data['weddingPackage'] as $w)
                                        <option value="{{ $w->id }}">Pakej {{ $i++ }}: {{ $w->bpd_description }}</option>
                                    @endforeach --}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nama Acara <span class="text-danger">*</span></label>
                            <div class="col-10">
                                <input required class="form-control" type="text" name="namaAcara" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_event_name){{ $data['et_booking_facility_detail']->ebfd_event_name }}@endif"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Keterangan Acara </label>
                            <div class="col-10">
                                <textarea class="form-control" name="keteranganAcara" id="" cols="" rows="">@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_event_desc){{$data['et_booking_facility_detail']->ebfd_event_desc }}@endif</textarea>
                                {{-- <input class="form-control" type="text" name="keteranganAcara"/> --}}
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label  class="col-2 col-form-label">Sesi <span class="text-danger">*</span></label>
                            <div class="col-3">
                                <select required class="form-control" id="kt_select2_3" name="sesiAcara" >
                                    <option class="text-muted" value="" disabled selected>Sila Pilih Slot Masa</option>
                                    <option value="1" >Siang - (08:00 am - 04:00 pm)</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Bilangan Peserta</label>
                            <div class="col-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">VVIP</span></div>
                                    <input type="number" class="form-control" min="0" name="vvip" placeholder="Jumlah" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_vvip){{ $data['et_booking_facility_detail']->ebfd_vvip }}@endif" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">VIP</span></div>
                                    <input type="number" class="form-control" min="0" name="vip" placeholder="Jumlah" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_vip){{ $data['et_booking_facility_detail']->ebfd_vip }}@endif" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">Peserta Lain</span></div>
                                    <input required type="number" min="1" class="form-control" name="participant" placeholder="Jumlah" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_participant){{ $data['et_booking_facility_detail']->ebfd_participant }}@endif"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Nama Pemohon <span class="text-danger">*</span></label>
                            <div class="col-10">
                                <input required class="form-control" type="text" name="namaPemohon" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_user_apply){{ $data['et_booking_facility_detail']->ebfd_user_apply }}@endif"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">No. Telefon <span class="text-danger">*</span></label>
                            <div class="col-6">
                                <input class="form-control" type="text" pattern="[0-9]{10-11}" placeholder="Enter Phone Number" name="noTel" value="@if($data['et_booking_facility_detail'] != null && $data['et_booking_facility_detail']->ebfd_contact_no){{ $data['et_booking_facility_detail']->ebfd_contact_no }}@endif"/>
                                <small id="noTelefonHelp" class="form-text text-muted">Format: 0123456789</small>
                            </div>
                        </div>
                        <div class="form-group row pb-5">
                            <label  class="col-2 col-form-label">Muat Naik Lampiran / Surat Sokongan</label>
                            <div class="col-6">
                                <div class="input-group">
                                    <input name="suratAkuan" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="">
                                    {{-- <label for="fileUpload1" class="btn btn-primary btn-file-upload" >Muat Naik</label> --}}
                                </div>
                            </div>
                        </div>
                        
                        <div class="font-weight-bold h4 my-8">Senarai Peralatan</div>
                        <div class="py-3 h6"><u class="">Kegunaan Dalaman</u></div>
                        @foreach ($data['equipment_int'] as $ee)
                            <div class="row py-1">
                                <div class="col-5 my-auto">
                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }})</div>
                                </div>
                                <div class="col-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                        <input type="number" name="dalaman_{{ $ee->eid }}" class="form-control quantity-input" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="py-3 h6"><u class="">Kegunaan Luaran</u></div>
                        @foreach ($data['equipment_ext'] as $ee)
                            <div class="row py-1">
                                <div class="col-5 my-auto">
                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }}) </div>
                                </div>
                                <div class="col-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                        <input type="number" name="luaran_{{ $ee->eid }}" class="form-control quantity-input"onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{-- <div class="form-group row" id="docSokongan">
                            <label class="col-lg-2 col-form-label">Dokumen Sokongan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <div class="dropzone dropzone-multi" id="kt_dropzone_4">
                                    <div class="dropzone-panel mb-lg-0 mb-2">
                                        <a class="dropzone-select btn btn-light-primary font-weight-bold btn-sm">Lampiran Dokumen</a>
                                        <a class="dropzone-upload btn btn-light-primary font-weight-bold btn-sm">Upload All</a>
                                        <a class="dropzone-remove-all btn btn-light-primary font-weight-bold btn-sm">Remove All</a>
                                    </div>
                                    <div class="dropzone-items">
                                        <div class="dropzone-item" style="display:none">
                                            <div class="dropzone-file">
                                                <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                    <span data-dz-name="">some_image_file_name.jpg</span>
                                                    <strong>( 
                                                    <span data-dz-size="">340kb</span>)</strong>
                                                </div>
                                                <div class="dropzone-error" data-dz-errormessage=""></div>
                                            </div>
                                            <div class="dropzone-progress">
                                                <div class="progress">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                </div>
                                            </div>
                                            <div class="dropzone-toolbar">
                                                <span class="dropzone-start">
                                                    <i class="flaticon2-arrow"></i>
                                                </span>
                                                <span class="dropzone-Cancel" data-dz-remove="" style="display: none;">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                                <span class="dropzone-delete" data-dz-remove="">
                                                    <i class="flaticon2-cross"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="form-text text-muted">Fail dalam bentuk PDF dan gambar sahaja.</span>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    {{-- @include('hall.public.js.slot') --}}
    @include('sport.external.js.slot')
@endsection




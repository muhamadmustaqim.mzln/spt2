@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Harga Fasiliti</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Harga Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/sport/admin/facilityprice/add') }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Tambah Harga Fasiliti</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Fasiliti :</label>
                            <div class="col-lg-10">
                                <select required name="fasiliti" class="form-control" id="kt_select2_1">
                                    <option value="">Sila Pilih Fasiliti</option>
                                    @foreach($data['facility'] as $f)
                                        <option value="{{ $f->id }}">{{ $f->ef_desc }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Fungsi Penggunaan :</label>
                            <div class="col-lg-10">
                                <select required name="fungsi" class="form-control" id="kt_select2_2">
                                    <option value="">Sila Pilih Fungsi Penggunaan</option>
                                    @foreach($data['function'] as $f)
                                        <option value="{{ $f->id }}">{{ $f->ef_desc }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Slot Masa :</label>
                            <div class="col-lg-10">
                                <select required name="masa" class="form-control" id="kt_select2_3">
                                    <option value="">Sila Pilih Slot Masa</option>
                                    @foreach($data['time'] as $f)
                                        <option value="{{ $f->id }}">{{ $f->est_slot_time }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori GST :</label>
                            <div class="col-lg-10">
                                <select required name="gst" id="gst" class="form-control" id="kt_select2_4">
                                    <option value="">Sila Pilih Kategori GST</option>
                                    @foreach($data['gst'] as $f)
                                        <option value="{{ $f->id }}" data-taxvalue="{{$f->lgr_rate}}">{{ $f->lgr_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategoti Pakej :</label>
                            <div class="col-lg-10">
                                <select required name="pakej" class="form-control" id="kt_select2_5">
                                    <option value="">Sila Pilih Kategori Pakej</option>
                                    @foreach($data['package'] as $f)
                                        <option value="{{ $f->id }}">{{ $f->ep_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori Slot :</label>
                            <div class="col-lg-10">
                                <select required name="slot" class="form-control" id="kt_select2_6">
                                    <option value="">Sila Pilih Kategori Slot</option>
                                    @foreach($data['slot'] as $f)
                                        <option value="{{ $f->id }}">{{ $f->lsc_slot_desc }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori Hari :</label>
                            <div class="col-lg-10">
                                <select required name="hari" class="form-control" id="kt_select2_7">
                                    <option value="">Sila Pilih Kategori Hari</option>
                                    <option value="1">Isnin - Jumaat</option>
                                    <option value="2">Sabtu - Ahad / Cuti Umum</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Harga (RM):</label>
                            <div class="col-lg-10">
                                <input required type="text" class="form-control" name="harga" id="harga" placeholder="Harga" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Harga (RM)<span class="font-size-xs text-danger">(selepas cukai):</span></label>
                            <div class="col-lg-10">
                                <input disabled type="text" class="form-control" name="" id="hargaselepas" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    <label class="ml-2 text-muted" id="textbox1"></label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/sport/admin/facilityprice') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.facilityprice.js.form')
@endsection
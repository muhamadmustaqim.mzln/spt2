<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.2.9') }}"></script>
<script>
    $('#checkbox1').on('change', function() { 
        // From the other examples
        if (!this.checked) {
            $('#textbox1').text("Tidak Aktif");
        }else{
            $('#textbox1').text("Aktif");
        }
    });
</script>
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $('#kt_select2_1').select2({
        placeholder: 'Sila Pilih Fasiliti'
    });
    $('#kt_select2_2').select2({
        placeholder: 'Sila Pilih Fungsi Penggunaan'
    });
    $('#kt_select2_3').select2({
        placeholder: 'Sila Pilih Slot Masa'
    });
    $('#kt_select2_4').select2({
        placeholder: 'Sila Pilih Kategori GST'
    });
    $('#kt_select2_5').select2({
        placeholder: 'Sila Pilih Kategori Pakej'
    });
    $('#kt_select2_6').select2({
        placeholder: 'Sila Pilih Kategori Slot'
    });
    $('#kt_select2_7').select2({
        placeholder: 'Sila Pilih Kategori Hari'
    });
    $(document).ready(function() {
        function calculateTotal() {
            var selectedOption = $('#gst').find('option:selected'); // Get the selected option from #gst
            var taxValue = parseFloat(selectedOption.data('taxvalue')) || 0; // Get the tax value or default to 0
            var price = parseFloat($('#harga').val()); // Get the price and parse to float
            console.log(taxValue, price);
            if (!isNaN(price) && !isNaN(taxValue)) {
                var totalafter = (price + (price * taxValue)).toFixed(2); // Calculate the total after tax and format to 2 decimals
                $('#hargaselepas').val(totalafter); // Set the value of #hargaselepas
            } else {
                console.log('Invalid price or tax value');
            }
        }

        $('#gst').on('change', function(e) {
            calculateTotal();
        });

        $('#harga').on('input', function(e) { // Use 'input' event for real-time changes
            calculateTotal();
        });
    });
</script>
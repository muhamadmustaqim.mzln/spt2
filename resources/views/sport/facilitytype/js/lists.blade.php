<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $('#checkbox1').on('change', function() { 
        // From the other examples
        if (!this.checked) {
            $('#textbox1').text("Tidak Aktif");
        }else{
            $('#textbox1').text("Aktif");
        }
    });
</script>
<script>
    $(document).ready(function() {
    var table = $('table').DataTable({
        dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
			<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
        buttons: [
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                  columns: ':visible'
                },
                customize: function(doc) {
                    doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    //doc.defaultStyle.alignment = 'center';
                    //doc.styles.tableHeader.alignment = 'center';
                },
            },
            {
                extend: 'colvis',
                text: 'Pilihan Lajur'
            },
            
        ],
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        }
    } );
});
</script>
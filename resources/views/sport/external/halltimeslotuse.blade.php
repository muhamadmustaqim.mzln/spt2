@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    {{-- <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5> --}}
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Pengurusan Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Tempahan Luaran</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="" method="post" id="formSukan">
                @csrf
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                        <td>{{ $data['user']->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                        <td>{{ $data['main']->bmb_booking_no }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                        <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                        <td>
                                            {{ $data['user_detail']->bud_phone_no }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                        <td>{{ $data['user']->email }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                        <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                        <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Tempahan Fasiliti</span>
                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                        </h3>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <input hidden type="text" name="efVal" value="{{ $data['ef'] }}" id="efVal">
                            @if ($data['ef'] == 1)
                                <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">
                                    <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                </button>
                                {{-- <button type="button" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2" id="modalBtn" data-toggle="modal" data-target="#staticBackdrop">
                                    <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                </button> --}}
                            @else
                                <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">
                                    <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                </button>
                            @endif
                            {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button> --}}
                            {{-- <a href="{{ url('/hall/batal', [Crypt::encrypt($data['book'])]) }}" class="btn btn-light-danger font-weight-bolder mt-2"> --}}
                            <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                <i class="flaticon-circle"></i> Batal Tempahan
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered tableJS" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Lokasi</th>
                                        @if ($data['et_hall_book'] != null && $data['et_hall_book']->fk_et_function != 13)
                                            <th>Jenis Penggunaan</th>
                                        @endif
                                        <th>Tarikh Kegunaan Dari</th>
                                        <th>Tarikh Kegunaan Hingga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach($data['et_booking_facility'] as $s) --}}
                                        <tr>
                                            <td>{{ Helper::tempahanSportType($data['et_booking_facility']->fk_et_facility_type) }}</td>
                                            <td>{{ Helper::location($data['et_facility_type']->fk_lkp_location) }}</td>
                                            @if ($data['et_hall_book'] != null && $data['et_hall_book']->fk_et_function != 13)
                                                <td>{{ Helper::typeFunction($data['et_hall_book']->fk_et_function) }}</td>
                                            @endif
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_start_date) }}</td>
                                            <td>{{ Helper::date_format($data['et_booking_facility']->ebf_end_date) }}</td>
                                        </tr>
                                    {{-- @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Kelengkapan Dewan</span>
                        </h3>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <div class="col-12">
                            <div class="card-body pt-0">
                                <!--begin: Datatable-->
                                <div class="table-responsive">
                                    <table class="table table-bordered tableJS" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Kegunaan</th>
                                                <th>Item</th>
                                                <th>Kuantiti</th>
                                                <th>Tarikh Penggunaan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['et_equipment_book'] as $s)
                                                <tr>
                                                    <td>{{ Helper::typeFunction($s->fk_et_function) }}</td>
                                                    <td>{{ Helper::get_equipment($s->fk_et_equipment) }}</td>
                                                    <td>{{ $s->eeb_quantity }}</td>
                                                    <td>{{ Helper::date_format($s->eeb_booking_date) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </div>
                    </div>
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Pilihan Slot</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="row d-flex justify-content-center">
                            <div class="col-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered tableJS" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Bil.</th>
                                                <th>Waktu</th>
                                                <th>
                                                    <label class="checkbox checkbox-primary checkbox-lg">
                                                        <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                                        <span class="mr-3"></span>Pilihan Slot
                                                    </label>
                                                </th>
                                                {{-- <th>Penggunaan</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody id="slot">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @if(count($data['slot']) > 0)
                                                @foreach($data['slot'] as $s)
                                                    <tr>
                                                        <td style="width: 5%;">{{ $i++ }}</td>
                                                        <td style="width: 33%;">{{ ($s->masa) }}</td>
                                                        <td style="width: 33%;">
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" class="chk {{ $s->id }}" name="slot[]" value="{{ $s->id }},{{ $s->fk_et_slot_time }},{{ $s->efp_unit_price }},{{ $s->fk_lkp_gst_rate }}"/>
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                @foreach($data['slot2'] as $s)
                                                    <tr>
                                                        <td style="width: 5%;">{{ $i++ }}</td>
                                                        <td style="width: 33%;">{{ ($s->masa) }}</td>
                                                        <td style="width: 33%;">
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" class="chk {{ $s->id }}" name="slot[]" value="{{ $s->id }},{{ $s->efpid }},{{ $s->efp_unit_price }},null"/>
                                                                <span></span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Maklumat Perbankan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close m-2"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-12">
                                            <div class="card-body">
                                                <!--begin: Bank Info-->
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">Nama Penuh <span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="namaPenuh" value="{{$data['user']->fullname}}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">No. Kad Pengenalan<span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="kadPengenalan" value="{{$data['user_detail']->bud_reference_id}}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">Email <span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="emel" value="{{$data['user']->email}}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">No. Telefon Bimbit <span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="phoneNo" value="{{$data['user_detail']->bud_phone_no}}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">Nama Bank <span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="namaBank"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label  class="col-3 col-form-label">No. Akaun Bank <span class="text-danger">*</span></label>
                                                    <div class="col-9">
                                                        <input  class="form-control" type="text" name="akaunBank"/>
                                                    </div>
                                                </div>
                                                <!--end: Bank Info-->
                                            </div>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-check-circle"></i> Teruskan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

{{-- <div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <h3 class="card-title align-items-start flex-column mb-5">
            <span class="card-label font-weight-bolder text-dark">Maklumat Perbankan</span>
        </h3>
    </div>
    <div class="card-body pt-0">
        <!--begin: Datatable-->
        <div class="row d-flex justify-content-center">
            <div class="col-12">
                <div class="card-body">
                    <!--begin: Bank Info-->
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Nama Penuh <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="namaPenuh"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">No. Kad Pengenalan<span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="kadPengenalan"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="emel"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">No. Telefon Bimbit <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="phoneNo"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Nama Bank <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="namaBank"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">No. Akaun Bank <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input  class="form-control" type="text" name="akaunBank"/>
                        </div>
                    </div>
                    <!--end: Bank Info-->
                </div>
            </div>
        </div>
        <!--end: Datatable-->
    </div>
</div> --}}

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    {{-- @include('hall.public.js.slot') --}}
    {{-- @include('sport.public.js.slot') --}}
    @include('sport.external.js.halltimeslot')
@endsection




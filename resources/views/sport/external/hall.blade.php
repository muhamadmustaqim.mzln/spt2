@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Luaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Sila Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}"@if(isset($data['id'])) @if($l->id == $data['id']) selected @endif @endif>{{ $l->lc_description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-icon">
                                                <input type="text" class="form-control  font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" required value="@if(isset($data['date'])) {{ $data['date'] }} @endif" />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                        <div class="float-left">
                                            <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                        </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-12">
                        @if ($data['post'] == true)
                        <!--begin::Card-->
                        {{-- <form action="{{ url('sport/external/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
                            @csrf --}}
                            <div class="card card-custom gutter-b">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <h3 class="card-title align-items-start flex-column mb-5">
                                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ Helper::date_format($data['date']) }}</span>
                                    </h3>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button> --}}
                                        {{-- <a href="{{ url('hall/external/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date']), Crypt::encrypt($data['userId'])]) }}" class="btn btn-primary font-weight-bolder mr-2 mt-2">
                                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                        </a>
                                        <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                            <i class="flaticon-circle"></i> Batal Tempahan
                                        </a> --}}
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive mb-5">
                                        <table class="table table-bordered" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th>Senarai Dewan Telah Ditempah</th>
                                                    <th>Nama Penempah</th>
                                                    <th>No Tempahan</th>
                                                    <th style="width: 15%">Tindakan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data['bookedList'] as $s)
                                                    <tr>
                                                        <td style="width: 25%">{{ Helper::getSportFacility($s->fk_et_facility_type) }}</td>
                                                        <td style="width: 25%">{{ $s->fullname }}</td>
                                                        <td style="width: 20%"><a href="{{ url('sport/maklumatbayaran', Crypt::encrypt($s->id)) }}" target="_blank" rel="noopener noreferrer">{{ Helper::get_noTempahan($s->fk_main_booking) }}</a></td>
                                                        <td class="text-center">
                                                            <a target="_blank" href="{{ url('sport/reschedule/sport', Crypt::encrypt(Helper::get_noTempahan($s->fk_main_booking))) }}" class="btn btn-sm font-weight-bold btn-light-primary m-1">Penjadualan Semula</a>
                                                            <button type="button" class="btn btn-light-warning font-weight-bold btn-sm m-1" data-toggle="modal" data-target="#staticBackdrop">
                                                                Batal Tempahan
                                                            </button>
                                                        </td>
                                                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Pembatalan Tempahan</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <i aria-hidden="true" class="ki ki-close"></i>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{url('/sport/cancel/submit',Crypt::encrypt($s->id))}}" method="post">
                                                                        @csrf
                                                                        <div class="modal-body">
                                                                            <textarea class="form-control" name="reason" id="exampleTextarea" rows="5"></textarea>
                                                                            <span class="form-text text-muted" >Sila nyatakan alasan pembatalan anda.</span>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                                                                            <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <th>Senarai Dewan Kosong</th>
                                                    <th class="w-25">Lokasi</th>
                                                    <th style="width: 15%">Pilih</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data['availableList'] as $s)
                                                    <tr>
                                                        <td>{{ $s->nama }}</td>
                                                        <td>{{ $s->lokasi }}</td>
                                                        <td>
                                                            <a href="{{ url('/sport/external/hallpurpose', [Crypt::encrypt($s->id), Crypt::encrypt($data['date']), Crypt::encrypt($data['userId'])]) }}" class="btn btn-primary">Buat Tempahan</a>
                                                        </td>                                                                
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        {{-- </form> --}}
                        <!--end::Card-->
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        	

@endsection

@section('js_content')
    @include('sport.external.js.hall')
@endsection
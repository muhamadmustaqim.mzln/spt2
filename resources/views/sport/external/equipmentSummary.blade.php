@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Papan Pemuka</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Sukan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <span class="mt-2">Borang Carian Telah Dinyah-Aktif<br><br></span>
                                <span class="mt-2">Sila Batalkan Tempahan Semasa Untuk Memilih Semula Tarikh atau Lokasi.<br><br></span>
                                <span class="mt-2">Anda juga boleh kemaskini tempahan ini di menu 'Tempahan Peralatan' dengan menggunakan nombor tempahan yang dipaparkan.<br><br></span>
                                <span class="mt-2">Nota:<br></span>
                                <span class="mt-2">Peralatan yang boleh ditempah hanya peralatan untuk kegunaan luaran sahaja.</span>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                <form action="" method="post">
                                    @csrf
                                    {{-- <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" name="nama" class="form-control mt-3" autocomplete="off" required value="{{ strtoupper($data['id']) }}">
                                        </div>
                                    </div> --}}
                                    {{-- <div class="float-right">
                                        <button type="submit" id="submit_form" class="btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                    </div> --}}
                                </form>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-8">
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5 pb-3 bg-secondary m-5">
                                <div class="h4">Tempahan bagi {{ strtoupper($data['user']->fullname) }}<br>
                                    <span class="font-size-sm">No Tempahan : {{ $data['main']->bmb_booking_no }} / Tarikh Tempahan : {{ date('d-m-Y') }}</span><br>
                                    <span class="font-size-sm">No Telefon : {{ $data['userProfile']->bud_phone_no }} / Email : {{ $data['user']->email }}</span>
                                </div>
                            </div>
                            <!--end::Header-->
                            @php
                                $gstStatus = 0;
                            @endphp
                            @foreach ($data['equipment_book'] as $getGstStatus)
                                @php
                                    $rate = Helper::kadar_gst($getGstStatus->fk_lkp_gst_rate);
                                    if($rate > 0){
                                        $gstStatus = 1;
                                    }
                                @endphp
                            @endforeach
                            <div class="card-body pt-2">
                                <div class="font-weight-bold">Senarai Peralatan Yang Ditempah</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Tarikh Kegunaan</td>
                                            <td>Penggunaan</td>
                                            <td>Item</td>
                                            <td class="text-center">Kuantiti</td>
                                            <td class="text-right">Jumlah (RM) *Selepas Diskaun</td>
                                            @if ($gstStatus == 1)
                                                <td>Kod GST</td>
                                                <td>GST (6%)</td>
                                            @endif
                                            <td class="text-right">Jumlah Sewaan (RM)</td>
                                        </tr>
                                    </thead>
                                    @php
                                        $totalPrice = 0;
                                    @endphp
                                    <tbody>
                                        @foreach ($data['equipment_book'] as $eb)
                                        @php
                                            $totalPrice += $eb->eeb_subtotal
                                        @endphp
                                            <tr>
                                                <td>{{ Helper::date_format($eb->eeb_booking_date) }}</td>
                                                <td>{{ Helper::typeFunction($eb->fk_et_function) }}</td>
                                                <td>{{ Helper::get_equipment($eb->fk_et_equipment) }}</td>
                                                <td class="text-center">{{ $eb->eeb_quantity }}</td>
                                                <td class="text-right">{{ $eb->eeb_subtotal }}</td>
                                                @if ($gstStatus == 1)
                                                    <td>{{ Helper::tempahanSportGST($eb->fk_lkp_gst_rate) }}</td>
                                                    <td>{{ Helper::kadar_gst($eb->fk_lkp_gst_rate) }}</td>
                                                @endif
                                                <td class="text-right">{{ $eb->eeb_subtotal }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="font-weight-bold mt-7">Rumusan</div>
                                <table class="table table-bordered">
                                    <tbody>
                                        @if ($gstStatus == 1)
                                            <tr>
                                                <td>GST (6%)</td>
                                                <td class="text-right">0.00</td>
                                            </tr>
                                            <tr>
                                                <td>GST (6%)</td>
                                                <td class="text-right">0.00</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>Jumlah Keseluruhan</td>
                                            <td class="text-right">{{ Helper::moneyhelper($totalPrice) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Penggenapan</td>
                                            <td class="text-right">0.00</td>
                                        </tr>
                                        <tr>
                                            <td><b>Jumlah Keseluruhan Perlu Dibayar (RM)</b></td>
                                            <td class="text-right">{{ Helper::moneyhelper($totalPrice) }}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="font-weight-bold mt-7"></div>
                                <button type="button" class="btn btn-primary float-right font-weight-bolder mt-2" data-toggle="modal" data-target="#staticBackdrop">
                                    <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                </button>
                                <!-- Modal-->
                                <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Perakuan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <label class="checkbox checkbox-success checkbox-sm d-flex align-items-start">
                                                    <input type="checkbox" id="pengakuan" class="group-checkable"/>
                                                    <span></span>
                                                    <div class="pl-5">
                                                        Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun yang dikenakan
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="modal-footer">
                                                <form action="{{ url('sport/external/bayaran', Crypt::encrypt($data['main']->id)) }}" method="post">
                                                    @csrf
                                                    <button disabled type="submit" id="submitBtn" class="btn btn-success"><i class="
                                                        fas fa-check-circle"></i> Buat Pembayaran</button>
                                                        <input type="hidden" name="total" value="{{ $totalPrice }}">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->	

@endsection

@section('js_content')
    @include('sport.external.js.equipment')
@endsection

<script>
        // $('#kt_select2_4').on('change', function(){
        // var location_id = $(this).val();
        // if (location_id == ''){
        //     $('#kt_select2_5').prop('disabled', true);
        // }
        // else{
        //     $('#kt_select2_5').prop('disabled', false);
        //     $.ajax({
        //         url:"{{ url('sport/ajax') }}",
        //         type: "POST",
        //         data: {'location_id' : location_id},
        //         dataType: 'json',
        //         success: function(data){
        //             $('#kt_select2_5').html(data);
        //         }
        //     });
        // }
    // });
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    // var table = $('table').DataTable({
    //     "bPaginate": false,
    //     "bLengthChange": false,
    //     "bFilter": false,
    //     "bInfo": false,
    //     language: {
    //         "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
    //         "zeroRecords": "Harap maaf, tiada rekod ditemui",
    //         "info": "Halaman _PAGE_ dari _PAGES_",
    //         "infoEmpty": "Tiada rekod dalam sistem",
    //         "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
    //         // "sSearch": "Carian:"
    //     },
    //     // rowGroup: {
    //     //     dataSrc: 1,
    //     // },
    // } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function() {
        $('#kt_select2_4').change(function() {
            var selectedValue = $(this).val();

            // Show the corresponding select element based on the selected value
            if(selectedValue == 1) {
                $('#dalaman').prop('hidden', false);
                $('#luaran').prop('hidden', true);
                $('#kuantiti').prop('hidden', false);
                // Set max attribute for kuantiti input based on max value from equipment_int
                var max = parseInt($('select[name="dalaman"] option:selected').data('max'));
                $('#kuantiti').attr('max', max);
            } else if(selectedValue == 2) {
                $('#luaran').prop('hidden', false);
                $('#dalaman').prop('hidden', true);
                $('#kuantiti').prop('hidden', false);
                // Set max attribute for kuantiti input based on max value from equipment_ext
                var max = parseInt($('select[name="luaran"] option:selected').data('max'));
                $('#kuantiti').attr('max', max);
            }
        });
        $('#dalaman').change(function() {
            var selectedValue = $(this).val();
            var max = parseInt(selectedOption.data('max'));
            console.log(selectedValue, max);

        })

        $('#staticBackdrop1').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var itemId = button.data('item-id'); // Extract info from data-* attributes
            $('input[name="itemId"]').val(itemId); // Set the value of the input field
        });
        $('#staticBackdrop2').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var itemId = button.data('item-id'); // Extract info from data-* attributes
            $('input[name="itemId"]').val(itemId); // Set the value of the input field
        });

        $('.quantity-input').on('input', function() {
            var max = parseInt($(this).data('max'));
            var value = parseInt($(this).val());
            console.log(value)
            if (!isNaN(value)) {
                if (value > max) {
                    $(this).val(max); 
                }
            } 
        });

        var groupColumn = 4;
        var table = $('#example').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );

        $('#tempahanDewan-tab').on('click', function() {
            $('#tempahanDewanContent').removeAttr('hidden');
            $('#tempahanSukanContent').prop('hidden', true);
        })
        $('#tempahanSukan-tab').on('click', function() {
            $('#tempahanSukanContent').removeAttr('hidden');
            $('#tempahanDewanContent').prop('hidden', true);
        })
    
        // Order by the grouping
        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );

        $('#kategori').on('change', function() {
            var type = $(this).val();
            console.log(type);
            if(type == 8){
                $('.kjCls').prop('hidden', false);
                $('.sajCls').prop('hidden', true);
            } else if(type == 6 || type == 7) {
                $('.sajCls').prop('hidden', false);
                $('.kjCls').prop('hidden', true);
            } else {
                $('.sajCls').prop('hidden', true);
                $('.kjCls').prop('hidden', true);
            }
        })
    });
</script>
<script>
    $(document).ready(function(){
        $('#pengakuan').on('change', function() {
            if (this.checked) {
                $('#submitBtn').removeAttr('disabled');
            } else {
                $('#submitBtn').attr('disabled', true);
            }
        })
        
        var maxDate = moment().add(12, 'months'); // Calculate the maximum date
        $('#kt_daterangepicker_5').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $("#form").submit(function( event ) {
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }else{
                $("#form").submit();
            }

        });
    });
    
</script>
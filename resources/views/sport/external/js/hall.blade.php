<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        var table = $('table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'><'col-sm-12 col-md-7 dataTables_pager'>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
        var maxDate = moment().add(12, 'months');
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            minDate: new Date(),
            maxDate: maxDate,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
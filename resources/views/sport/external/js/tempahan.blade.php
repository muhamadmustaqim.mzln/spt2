<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        // $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('DD-MM-YYYY'));
        //     var tarikh = picker.startDate.format('DD-MM-YYYY');
        //     var id = $('#eft').val();
        //     var csrfToken = $('meta[name="csrf-token"]').attr('content');

        //     $.ajax({
        //         url:"{{ url('sport/ajax/getSportTimeSlot') }}",
        //         type: "POST",
        //         headers: {
        //             'X-CSRF-TOKEN': csrfToken
        //         },
        //         data: {
        //             'id': id,
        //             'date': tarikh
        //         },
        //         success: function(data){
        //             // $('#kt_select2_5').html(data);
        //         }
        //     });
        // });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
        $('#kt_select2_4').on('change', function(){
        var location_id = $(this).val();
        var kaunter = 1;
        if (location_id == ''){
            $('#kt_select2_5').prop('disabled', true);
        }
        else{
            $('#kt_select2_5').prop('disabled', false);
            $.ajax({
                url:"{{ url('sport/ajax') }}",
                type: "POST",
                data: {
                    'location_id': location_id,
                    'kaunter': kaunter
                },
                dataType: 'json',
                success: function(data){
                    $('#kt_select2_5').html(data);
                }
            });
        }
    });
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        var groupColumn = 4;
        var table = $('#example').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );
    
        // Order by the grouping
        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
        } );
</script>
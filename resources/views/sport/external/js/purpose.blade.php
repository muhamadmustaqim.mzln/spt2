<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        $('#kembali').on('click', function() {
            $('#mainForm').submit()
        })

        var table = $('#table').DataTable({
            dom: `
                <'row'<'col-sm-12'tr>>
               `,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('#JenisAcaraTempDewan').on('change', function() {
            var type = $(this).val();
            console.log(type);
            if(type == 8){
                // $('.kjCls').prop('hidden', false).find('input').prop('required', true); // If nak convert jadi required after unhide
                $('.kjCls').prop('hidden', false);
                $('.sajCls').prop('hidden', true);
                $('#fileUpload1').prop('required', true);
            } else if(type == 6 || type == 7) {
                $('.sajCls').prop('hidden', false);
                $('.kjCls').prop('hidden', true);
                $('#fileUpload1').prop('required', true);
            } else {
                $('.sajCls').prop('hidden', true);
                $('.kjCls').prop('hidden', true);
                $('#fileUpload1').prop('required', false);
            }
        })
        // $('#form').submit(function(event) {
        //     var checkboxes = $('input[type="checkbox"][name="gelanggang[]"]:checked');
        //     if (checkboxes.length === 0) {
        //         event.preventDefault(); // Prevent form submission
        //         alert('Please select at least one option.');
        //     }
        // });
    });
</script>
@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Tempahan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPS</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Luaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                {{-- <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Carian Pengguna</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                    
                                </h3>
                                <span class="mt-2 font-weight-bold font-size-sm">Masukkan No. Kad Pengenalan Pelanggan Atau No. Pendaftaran Syarikat Atau Nama:</span> --}}
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <select name="kategori" id="kategori" class="form-control mt-3" required>
                                            <option disabled selected value="">Pilih Kategori</option>
                                            @foreach ($data['discount'] as $l)
                                                <option value="{{ $l->id }}" @if(isset($data['kategori']) && Crypt::decrypt($data['kategori']) == $l->id) selected @endif>{{ $l->ldt_user_cat }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row kjCls" id="" hidden>
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Kementerian / Jabatan</span></div>
                                            <input type="text" class="form-control" name="kementerianjabatan" placeholder="" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row sajCls" id="" hidden>
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Id Staf</span></div>
                                            <input type="text" class="form-control" name="staffid" placeholder="" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row sajCls" id="staffidDiv" hidden>
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Agensi / Jabatan</span></div>
                                            <input type="text" class="form-control" name="agensijabatan" placeholder="" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                            <option disabled selected value="">Pilih Lokasi Peralatan</option>
                                            @foreach ($data['location'] as $l)
                                                <option value="{{ $l->id }}" @if(isset($data['lokasi']) && Crypt::decrypt($data['lokasi']) == $l->id) selected @endif>{{ $l->lc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <div class="input-icon">
                                            <input type="text" class="form-control form-control-md font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" value="@if(isset($data['date'])) {{ Helper::date_format($data['date']) }} @endif" required />
                                            <span>
                                                <i class="flaticon-calendar-3 icon-xl"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-1"></div>
                                    <div class="col-lg-10">
                                        <div class="float-left">
                                            <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Mula Menempah</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-8">
                        <!--begin::List Widget 21-->
                        @if ($data['post'] == 1 || $data['post'] == 2)
                            <div class="card card-custom gutter-b pb-8">
                                @if($data['post'] == 2)
                                    <div class="alert alert-danger">
                                        Tiada data peralatan bagi lokasi ini
                                    </div>
                                @endif
                                <!--begin::Header-->
                                <div class="card-header">
                                    <div class="card-title">Maklumat Tempahan Peralatan</div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <form action="{{ url('/sport/external/equipmentSubmit', [Crypt::encrypt($data['id']), Crypt::encrypt($data['user']->id)]) }}" method="post">
                                    @csrf
                                    <input hidden type="text" class="form-control" name="kategori" placeholder="" value="{{ $data['kategori'] }}" />
                                    <input hidden type="text" class="form-control" name="lokasi" placeholder="" value="{{ $data['lokasi'] }}" />
                                    <input hidden type="text" class="form-control" name="date" placeholder="" value="{{ $data['date'] }}" />
                                    <input hidden type="text" class="form-control" name="kementerianjabatan" placeholder="" value="{{ $data['kj'] }}" />
                                    <input hidden type="text" class="form-control" name="staffid" placeholder="" value="{{ $data['staffid'] }}" />
                                    <input hidden type="text" class="form-control" name="agensijabatan" placeholder="" value="{{ $data['aj'] }}" />
                                    <div class="card-body">
                                        <table class="table table-bordered table-hover table-checkable" id="" style="">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Lokasi</th>
                                                    <th>Tarikh Kegunaan Dari</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tempahan Peralatan</td>
                                                    <td>{{ $data['lokasiData']->lc_description }}</td>
                                                    <td>{{ Helper::date_format($data['date']) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Tempat Kegunaan Peralatan: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <select @if($data['post'] == 2) disabled @endif name="presint" id="" class="form-control mt-3" required>
                                                    <option @if($data['post'] == 1) disabled selected @endif value="">Pilih Tempat Kegunaan</option>
                                                    @if($data['post'] == 2)
                                                        <option @if($data['post'] == 2) selected @endif value=""></option>
                                                    @endif
                                                    @foreach ($data['presint'] as $l)
                                                        <option value="{{ $l->id }}">{{ $l->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Alamat Tempat Kegunaan: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <textarea @if($data['post'] == 2) disabled @endif name="address" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Nama Pemohon: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <input @if($data['post'] == 2) disabled @endif type="text" class="form-control" name="nama" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">No Telefon: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <input @if($data['post'] == 2) disabled @endif type="text" class="form-control" name="noTel" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <hr class=""></hr>
                                        <div class="font-weight-bold h4 my-8">Senarai Peralatan</div>
                                        <div class="py-3 h6"><u class="">Kegunaan Dalaman</u></div>
                                        @foreach ($data['equipment_int'] as $ee)
                                            <div class="row py-1">
                                                <div class="col-5 my-auto">
                                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }})</div>
                                                </div>
                                                <div class="col-7">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                                        <input type="number" name="dalaman_{{ $ee->id }}" class="form-control quantity-input" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="py-3 h6"><u class="">Kegunaan Luaran</u></div>
                                        @foreach ($data['equipment_ext'] as $ee)
                                            <div class="row py-1">
                                                <div class="col-5 my-auto">
                                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }})</div>
                                                </div>
                                                <div class="col-7">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                                        <input type="number" name="luaran_{{ $ee->id }}" class="form-control quantity-input" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <button @if($data['post'] == 2) hidden @endif class="mt-5 btn btn-primary font-weight-bolder float-right">
                                            Teruskan Tempahan 
                                        </button>
                                        {{-- <table class="table table-bordered table-hover table-checkable" id="" style="margin-top: 13px !important">
                                            <thead>
                                                <tr>
                                                    <th>Tarikh Kegunaan</th>
                                                    <th>Lokasi</th>
                                                    <th>Tarikh Kegunaan Dari</th>
                                                    <th>Tindakan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table> --}}
                                    </div>
                                </form>
                                <!--end::Body-->
                            </div>
                        {{-- @else --}}
                            {{-- <div class="card card-custom gutter-b p-8">
                                <div class="card-header">
                                    <div class="card-title">Maklumat Tempahan Peralatan</div>
                                </div>
                                <div class="card-body">
                                    <div class="">
                                        <table class="table table-bordered table-hover table-checkable" id="" style="">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Lokasi</th>
                                                    <th>Tarikh Kegunaan Dari</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tempahan Peralatan</td>
                                                    <td>{{ $data['lokasiData']->lc_description }}</td>
                                                    <td>{{ Helper::date_format($data['date']) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Tempat Kegunaan Peralatan: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <select name="presint" id="" class="form-control mt-3" required>
                                                    <option disabled selected value="">Pilih Tempat Kegunaan</option>
                                                    @foreach ($data['presint'] as $l)
                                                        <option value="{{ $l->id }}">{{ $l->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Alamat Tempat Kegunaan: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <textarea name="address" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">Nama Pemohon: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="nama" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label text-lg">No Telefon: <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="noTel" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <hr class=""></hr>
                                        <div class="font-weight-bold h4 my-8">Senarai Peralatan</div>
                                        <div class="py-3 h6"><u class="">Kegunaan Dalaman</u></div>
                                        @foreach ($data['equipment_int'] as $ee)
                                            <div class="row py-1">
                                                <div class="col-5 my-auto">
                                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }})</div>
                                                </div>
                                                <div class="col-7">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                                        <input type="number" name="dalaman_{{ $ee->id }}" class="form-control quantity-input" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="py-3 h6"><u class="">Kegunaan Luaran</u></div>
                                        @foreach ($data['equipment_ext'] as $ee)
                                            <div class="row py-1">
                                                <div class="col-5 my-auto">
                                                    <div class="text-left">{{ $ee->item }} (Maksimum Kuantiti = {{ $ee->max }})</div>
                                                </div>
                                                <div class="col-7">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">Kuantiti</span></div>
                                                        <input type="number" name="luaran_{{ $ee->id }}" class="form-control quantity-input" onkeydown="return event.keyCode !== 69 && event.keyCode !== 189" data-max="{{ $ee->max }}" placeholder="" min="0" max="{{ (int)$ee->max }}" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div> --}}
                        @endif
                        <!--end::List Widget 21-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->	
    </div>
@endsection

@section('js_content')
    @include('sport.external.js.equipment')
@endsection
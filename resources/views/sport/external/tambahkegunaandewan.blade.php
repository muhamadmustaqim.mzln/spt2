@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    {{-- <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5> --}}
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">SPS</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Pengurusan Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Tempahan Luaran</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-body pt-6">
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                            <td>{{ strtoupper($data['user']->fullname) }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                            <td>{{ $data['user']->email }}</td>
                        </tr>
                    </table>
                    <hr>
                    <form action="" method="post">
                        @csrf
                        <input type="text" name="type" value="1" hidden>
                        <div class="form-group row">
                            <div class="col-lg-7">
                            </div>
                            <div class="col-lg-3">
                                <input type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh" autocomplete="off" value="@if(isset($data['date'])){{ Helper::date_format($data['date']) }}@endif" required>
                            </div>
                            <div class="col-lg-1">
                                <div class="float-right">
                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mr-2 mt-3">Carian</button>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="float-right">
                                    <a href="{{ url('/sport/external/rumusandewan', Crypt::encrypt($data['main']->id)) }}" class="form-control btn btn-light-warning font-weight-bold mr-2 mt-3">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                </div>
                {{-- <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>
                                        {{ $data['user_detail']->bud_phone_no }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> --}}
            </div>
            @if ($data['post'] == 1)
                <div class="card card-custom gutter-b">
                    @if ($data['type'] == 3)
                        <form action="" method="post">
                            @csrf
                            <input type="text" name="type" value="3" hidden>
                            <input type="text" name="tarikh" value="{{ $data['date'] }}" hidden>
                            <div class="row d-flex justify-content-center my-15">
                                <div class="col-8">
                                    <button type="submit" class="btn btn-primary float-right font-weight-bolder mr-2 my-2"><i class="fas fa-check-circle"></i> Pilih</button>
                                    @if ($data['result'] == 20)
                                        <div class="h6 mt-15"><b>Pilih Kegunaan {{ $data['checkroom'][0]->eft_type_desc }}</b></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <th>Kegunaan</th>
                                                        <th style="width: 15%">Pilih</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <input hidden type="text" name="et_facility_type_id" value="{{ $data['checkroom'][0]->id }}">
                                                    @foreach ($data['slot'] as $s)
                                                        <tr>
                                                            <td>{{ $s['efd_name'] }}</td>
                                                            <td class="text-center"><input required value="{{ $s['id'] }}" type="radio" id="star5" name="fn[]" /></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="h6 mt-15"><b>Pilih {{ $data['checkroom'][0]->eft_type_desc }}</b></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <th>Kegunaan</th>
                                                        <th style="width: 15%">Pilih</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <input hidden type="text" name="et_facility_type_id" value="{{ $data['checkroom'][0]->id }}">
                                                    @foreach ($data['slot3'] as $s)
                                                        <tr>
                                                            <td>{{ $s['efd_name'] }}</td>
                                                            <td class="text-center"><input value="{{ $s['id'] }}" type="checkbox" id="star5" name="gelanggang[]" /></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <th>Kegunaan</th>
                                                        <th>Pilih</th>
                                                    </tr>
                                                </thead>
                                                <tbody >
                                                    @foreach($data['kegunaan'] as $s)
                                                        <input hidden type="text" name="et_facility_type_id" value="{{ $data['checkroom'][0]->id }}">
                                                        <tr>
                                                            <td>{{ $s['efd_name'] }}</td>
                                                            <td class="text-center"><input required value="{{ $s['id'] }}" type="radio" id="star5" name="et_function_id" /></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </form>
                    @else
                        <form action="" method="post" id="formSukan">
                            @csrf
                            @if (isset($data['gelanggang']))
                                <input type="text" hidden name="gelanggang" value="{{$data['gelanggang']}}">
                            @endif
                            <input type="text" name="type" value="2" hidden>
                            <input type="text" name="tarikh" value="{{ $data['date'] }}" hidden>
                            <input type="text" name="et_function_id" value="{{ $data['et_function_id'] }}" hidden>
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark">Tempahan Fasiliti</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                                </h3>
                                <div class="card-toolbar float-right">
                                    <!--begin::Button-->
                                    <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button>
                                    <a href="{{ url('/sport') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                        <i class="flaticon-circle"></i> Batal Tempahan
                                    </a>
                                    <!--end::Button-->
                                </div>
                            </div>
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark">Senarai Pilihan Slot</span>
                                </h3>
                            </div>
                            <div class="card-body pt-0">
                                <!--begin: Datatable-->
                                <div class="row d-flex justify-content-center mt-5">
                                    <div class="col-8">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <th>Bil.</th>
                                                        <th>Waktu</th>
                                                        <th>Tarikh</th>
                                                        <th>
                                                            <label class="checkbox checkbox-primary checkbox-lg">
                                                                <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                                                <span class="mr-3"></span>Pilihan Slot
                                                            </label>
                                                        </th>
                                                        {{-- <th>Penggunaan</th> --}}
                                                    </tr>
                                                </thead>
                                                <tbody id="slot">
                                                    @php
                                                        $i = 1;
                                                    @endphp
                                                    @if ($data['et_function_id'] == 6)
                                                        @foreach($data['slot'] as $s)
                                                            <tr>
                                                                <td style="width: 5%;">{{ $i++ }}</td>
                                                                <td style="width: 33%;">{{ ($s->masa) }}</td>
                                                                <td style="width: 29">{{ Helper::date_format($data['date']) }}</td>
                                                                <td style="width: 33%;">
                                                                    <label class="checkbox checkbox-primary checkbox-lg">
                                                                        <input type="checkbox" class="chk {{ $s->id }}" name="slot[]" value="{{ $s->id }},{{ $s->fk_et_slot_time }},{{ $s->efp_unit_price }},{{ $s->fk_lkp_gst_rate }}"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif ($data['result'] == 20)
                                                        @foreach($data['slot2'] as $s)
                                                            <tr>
                                                                <td style="width: 5%;">{{ $i++ }}</td>
                                                                <td style="width: 33%;">{{ ($s->masa) }}</td>
                                                                <td style="width: 29">{{ Helper::date_format($data['date']) }}</td>
                                                                <td style="width: 33%;">
                                                                    <label class="checkbox checkbox-primary checkbox-lg">
                                                                        <input type="checkbox" class="chk {{ $s->id }}" name="slot[]" value="{{ $s->id }},{{ $s->efpid }},{{ $s->efp_unit_price }},null"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        @foreach($data['slot'] as $s)
                                                            <tr>
                                                                <td style="width: 5%;">{{ $i++ }}</td>
                                                                <td style="width: 33%;">{{ ($s->masa) }}</td>
                                                                <td style="width: 29">{{ Helper::date_format($data['date']) }}</td>
                                                                <td style="width: 33%;">
                                                                    <label class="checkbox checkbox-primary checkbox-lg">
                                                                        <input type="checkbox" class="chk {{ $s->id }}" name="slot[]" value="{{ $s->id }},{{ $s->fk_et_slot_time }},{{ $s->efp_unit_price }},{{ $s->fk_lkp_gst_rate }}"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Datatable-->
                            </div>
                        </form>
                    @endif
                </div>
            @endif
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    {{-- @include('hall.public.js.slot') --}}
    {{-- @include('sport.public.js.slot') --}}
    @include('sport.internal.js.tambahkegunaandewan')
@endsection




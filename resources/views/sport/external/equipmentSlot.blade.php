@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sukan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Papan Pemuka</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Sukan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <span class="mt-2">Borang Carian Telah Dinyah-Aktif<br><br></span>
                                <span class="mt-2">Sila Batalkan Tempahan Semasa Untuk Memilih Semula Tarikh atau Lokasi.<br><br></span>
                                <span class="mt-2">Anda juga boleh kemaskini tempahan ini di menu 'Tempahan Peralatan' dengan menggunakan nombor tempahan yang dipaparkan.<br><br></span>
                                <span class="mt-2">Nota:<br></span>
                                <span class="mt-2">Peralatan yang boleh ditempah hanya peralatan untuk kegunaan luaran sahaja.</span>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                <form action="" method="post">
                                    @csrf
                                    {{-- <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" name="nama" class="form-control mt-3" autocomplete="off" required value="{{ strtoupper($data['id']) }}">
                                        </div>
                                    </div> --}}
                                    {{-- <div class="float-right">
                                        <button type="submit" id="submit_form" class="btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                    </div> --}}
                                </form>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-8">
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5 pb-3 bg-secondary m-5">
                                <div class="h4">Tempahan bagi {{ strtoupper($data['user']->fullname) }}<br>
                                    <span class="font-size-sm">No Tempahan {{ $data['main']->bmb_booking_no }} : / Tarikh Tempahan : {{ date('d-m-Y') }}</span><br>
                                    <span class="font-size-sm">No Telefon : {{ $data['userProfile']->bud_phone_no }} / Email : {{ $data['user']->email }}</span>
                                </div>
                            </div>
                            <!--end::Header-->
                            <div class="card-body pt-2">
                                <div class="font-weight-bold">Maklumat Acara</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Tempat Kegunaan Peralatan</td>
                                            <td>Alamat Tempat Kegunaan</td>
                                            <td>Nama Pemohon</td>
                                            <td>No Telefon</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['booking_facility_detail'] as $item)
                                            <tr>
                                                <td>{{ $item->ebfd_venue }}</td>
                                                <td>{{ $item->ebfd_address }}</td>
                                                <td>{{ $item->ebfd_user_apply }}</td>
                                                <td>{{ $item->ebfd_contact_no }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="font-weight-bold mt-7">Senarai Fasiliti Yang Ditempah</div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Nama</td>
                                            <td>Lokasi</td>
                                            <td>Tarikh Kegunaan Dari</td>
                                            <td>Tindakan</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['booking_facility'] as $item)
                                            <tr>
                                                <td>{{ Helper::getSportFacility($item->fk_et_facility_type) }}</td>
                                                <td>{{ Helper::location($data['main']->fk_lkp_location) }}</td>
                                                <td>{{ Helper::date_format($item->ebf_start_date) }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Tindakan
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                      <button type="button" class="dropdown-item text-primary " data-toggle="modal" data-target="#staticBackdrop2" data-item-id="{{ $item->id }}">Tambah Peralatan</button>
                                                      <button type="button" class="dropdown-item text-primary " data-toggle="modal" data-target="#staticBackdrop1" data-item-id="{{ $item->id }}">Kemaskini Maklumat Acara</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="font-weight-bold mt-7">Senarai Peralatan Yang Ditempah</div>
                                <table class="table table-bordered"s>
                                    <thead>
                                        <tr>
                                            <td>Item</td>
                                            <td>Tarikh Kegunaan</td>
                                            <td>Kegunaan</td>
                                            <td class="text-center">Kuantiti</td>
                                            {{-- <td>Tindakan</td> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data['equipment_book'] as $eb)
                                            <tr>
                                                <td><div class="font-weight-bold">{{ Helper::get_equipment($eb->fk_et_equipment) }}</div>(Maximum Kuantiti = {{ Helper::get_equipment_max_quantity($eb->fk_et_equipment) }})</td>
                                                <td>{{ Helper::date_format($eb->eeb_booking_date) }}</td>
                                                <td>{{ Helper::typeFunction($eb->fk_et_function) }}</td>
                                                <td class="text-center">{{ $eb->eeb_quantity }}</td>
                                                {{-- <td>
                                                    <button class="btn btn-outline-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Tindakan
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item text-primary" href="#">Kemaskini</a>
                                                        <a class="dropdown-item text-danger btn-delete" href="{{ url('/sport/external/equipmentdelete', [Crypt::encrypt($data['main']->id), Crypt::encrypt($eb->id)]) }}">Padam</a>
                                                    </div>
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <a href="{{ url('/sport/external/equipmentsummary', Crypt::encrypt($data['main']->id)) }}" class="btn btn-primary float-right font-weight-bolder mt-2">
                                    <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                </a>
                                {{-- <a href="{{ url('/sport/external/equipmentadd', Crypt::encrypt($data['main']->id)) }}" class="btn btn-warning float-right font-weight-bolder mt-2 mx-2">
                                    Tambah Kegunaan
                                </a> --}}
                                <button type="button" class="btn btn-warning float-right font-weight-bolder m-2" data-toggle="modal" data-target="#staticBackdrop">
                                    Tambah Kegunaan
                                </button>
                            </div>
                            <!-- Modal-->
                            <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Tambah Tarikh Kegunaan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form action="" method="post">
                                            @csrf
                                            <input type="text" name="type" value="1" hidden>
                                            <div class="modal-body">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-md font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikh" placeholder="Tarikh" autocomplete="off" value="@if(isset($data['date'])) {{ Helper::date_format($data['date']) }} @endif" required />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                                <button type="submit" class="btn btn-warning float-right font-weight-bolder m-5" data-toggle="modal" data-target="#staticBackdrop">
                                                    Tambah Tarikh
                                                </button>
                                            </div>
                                        </form>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="staticBackdrop1" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Kemaskini Maklumat Acara Dewan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form action="" method="post">
                                            @csrf
                                            <input type="text" name="type" value="2" hidden>
                                            <div class="modal-body">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Tempat Kegunaan Peralatan</span></div>
                                                    <select name="lokasi" id="" class="form-control mt-3" required>
                                                        <option disabled selected value="">Pilih Lokasi Peralatan</option>
                                                        @foreach ($data['presint'] as $l)
                                                            <option value="{{ $l->description }}">{{ $l->description }}</option>
                                                        @endforeach
                                                    </select>                                                
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Alamat Tempat Kegunaan</span></div>
                                                    <input type="text" name="kegunaan" class="form-control" placeholder="" value="" />
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Nama Pemohon</span></div>
                                                    <input type="text" name="namapemohon" class="form-control" placeholder="" value="" />
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">No. Telefon</span></div>
                                                    <input type="text" name="notelefon" class="form-control" placeholder="" value="" />
                                                </div>
                                                <input type="text" name="itemId" id="itemId" class="form-control" hidden>
                                                <button type="submit" class="btn btn-warning float-right font-weight-bolder m-5" data-toggle="modal" data-target="#staticBackdrop">
                                                    Simpan
                                                </button>
                                            </div>
                                        </form>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="staticBackdrop2" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop2" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Tambah Tempahan Peralatan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form action="" method="post">
                                            @csrf
                                            <input type="text" name="type" value="3" hidden>
                                            <div class="modal-body">
                                                <select name="kegunaan" id="kt_select2_4" class="form-control mt-3" required>
                                                    <option disabled selected value="">Pilih Kegunaan Peralatan</option>
                                                    <option value="1">Kegunaan Dalaman</option>
                                                    <option value="2">Kegunaan Luaran</option>
                                                </select>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <select hidden name="dalaman" id="dalaman" class="form-control mt-3" required>
                                                            @foreach ($data['equipment_int'] as $l)
                                                                <option value="{{ $l->eid }}">{{ $l->item }}</option>
                                                            @endforeach
                                                        </select>
                                                        <select hidden name="luaran" id="luaran" class="form-control mt-3" required>
                                                            @foreach ($data['equipment_ext'] as $l)
                                                                <option value="{{ $l->eid }}" data-max="">{{ $l->item }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div hidden id="kuantiti" class="col-6">
                                                        <input type="number" class="form-control mt-3" name="kuantiti" min="0">
                                                    </div>
                                                </div>
                                                <input type="text" name="itemId" id="itemId" class="form-control" hidden>
                                                <button type="submit" class="btn btn-success float-right font-weight-bolder m-5" data-toggle="modal" data-target="#staticBackdrop">
                                                    Tambah Peralatan
                                                </button>
                                            </div>
                                        </form>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->	

@endsection

@section('js_content')
    @include('sport.external.js.equipment')
@endsection
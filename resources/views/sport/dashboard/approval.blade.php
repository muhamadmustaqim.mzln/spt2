@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		                    				
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    {{-- <h5 class="text-dark font-weight-bolder my-1 mr-5">{{ $data['fasility']->bh_name }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}</h5> --}}
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Kelulusan Tempahan Sukan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('/sport/dashboard/report_kelulusan') }}" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengesahan Tempahan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <form action="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="text" name="type" value="1" hidden>
                                    <div class="">
                                        <h4>Senarai Fail</h4>
                                    </div>
                                    <hr>
                                    <div class="row my-2">
                                        {{-- <div class="col-4">Nama Fail</div>
                                        <div class="col-8">Tindakan</div> --}}
                                        <div class="col-12">
                                            <table class="table border" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th style="max-width: 60%;">Nama Fail</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($data['bh_attachment']) > 0)
                                                    @foreach ($data['bh_attachment'] as $val)
                                                        <tr>
                                                            <td>{{ $val->ba_file_name }}</td>
                                                            <td style="width: 40%;">
                                                                <div class="dropdown">
                                                                    <button class="btn btn-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                      Tindakan
                                                                    </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                        {{-- <a class="dropdown-item text-primary" href="">Muat Turun</a> --}}
                                                                        <a href="{{ url('/sport/dashboard/removefile', Crypt::encrypt($data['mbid'])) }}" class="dropdown-item text-danger btn-delete" href="">Hapus</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    @else
                                                        <tr>
                                                            <td class="text-center" colspan="2">No Data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <input name="fail" type="file" class="form-control" id="fileUpload" accept=".pdf, image/*" style="display:none;">
                                        <label for="fileUpload" class="btn btn-secondary btn-file-upload">Muat Naik</label>
                                        <button @if($data['main']->fk_lkp_status != 13) disabled @endif class="btn btn-secondary btn-confirm-custom">Simpan</button>
                                    </div>
                                    <div class="my-auto" id="selectedFile"></div>
                                </form>
                            </div>
                            <div class="col-6">
                                <form action="" method="post">
                                    @csrf
                                    <input type="text" name="type" value="2" hidden>
                                    <div>
                                        <h4>Tindakan Kelulusan</h4>
                                    </div>
                                    <hr>
                                    <div class="row my-2">
                                        <div class="col-4">
                                            Tindakan<span class="text-danger">*</span>
                                        </div>
                                        <div class="col-8">
                                            <select @if($data['main']->fk_lkp_status != 13) disabled @endif name="tindakan" class="form-control" name="" id="" required>
                                                <option value="" disabled selected>Sila Pilih Tindakan</option>
                                                <option value="1">Lulus</option>
                                                <option value="2">Tidak Lulus</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            Keterangan Tindakan
                                        </div>
                                        <div class="col-8">
                                            <textarea @if($data['main']->fk_lkp_status != 13) disabled @endif class="form-control" name="keterangan" id="" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <a href="{{ url('/sport/janasebutharga', Crypt::encrypt($data['main']->id)) }}" target="_blank" class="btn btn-success">Jana Sebut Harga</a>
                                            {{-- <a href="{{ url('/sport/resitonline', Crypt::encrypt($data['main']->id)) }}" target="_blank" class="btn btn-success mx-2">Jana Borang</a> --}}
                                        </div>
                                        <div>
                                            <button @if($data['main']->fk_lkp_status != 13) disabled @endif class="btn btn-primary btn-confirm-custom">Hantar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Kelulusan Tempahan</span>
                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                        </h3>
                        <form action="" method="post" id="form">
                            @csrf
                            <div class="card-toolbar">
                            </div>
                        </form>
                    </div>
                    <!--start::Senarai Slot Masa -->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Slot Masa</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="row d-flex justify-content-center">
                            {{-- <div class="col-8"> --}}
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Lokasi</th>
                                                <th>Nama Fasiliti</th>
                                                <th>Tarikh Kegunaan</th>
                                                <th>Jenis Kegunaan</th>
                                                <th class="text-right">Slot Kegunaan</th>
                                                <th class="text-right">Harga (RM)</th>
                                                <th class="text-right">Jumlah (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 0;
                                                $totalAll = 0;
                                                $subtotalAll = 0;
                                                $totalHall = 0;
                                                $totalEq = 0;
                                            @endphp
                                            @foreach ($data['et_booking_facility'] as $item1)
                                                @php
                                                    $ebfid = $item1->id;
                                                    $count = 0;
                                                @endphp
                                                @foreach ($data['et_hall_book'] as $item)
                                                    @if($item->fk_et_booking_facility == $ebfid)
                                                        @php
                                                            $i = 0;
                                                            $ehbid = $item->id;
                                                        @endphp
                                                        @foreach ($data['et_hall_book'] as $w)
                                                            @foreach ($data['et_hall_time'] as $e)
                                                                @if ($e->fk_et_hall_book == $w->id)
                                                                    @if ($w->ehb_booking_date == $item1->ebf_start_date)
                                                                        @php
                                                                            $count++;
                                                                        @endphp
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                        @foreach($data['et_hall_time'] as $s)
                                                            @if ($s->fk_et_hall_book == $ehbid)
                                                                @php
                                                                    $totalHall += $s->eht_subtotal;
                                                                    $totalAll += $s->eht_total;
                                                                    $subtotalAll += $s->eht_subtotal;
                                                                @endphp
                                                                <tr>
                                                                    {{-- <td>{{ Helper::tempahanSportType($item1->fk_et_facility_type) }}  {{ $count }}</td>
                                                                    <td>{{ Helper::date_format($item1->ebf_start_date) }}</td> --}}
                                                                    @if ($i == 0)
                                                                        <td rowspan="{{$count}}">{{ Helper::location($data['main']->fk_lkp_location) }}</td>
                                                                        <td rowspan="{{$count}}">{{ Helper::tempahanSportType($item1->fk_et_facility_type) }}</td>
                                                                        <td rowspan="{{$count}}">{{ Helper::date_format($item1->ebf_start_date) }}</td>
                                                                    @endif
                                                                    <td>{{ Helper::typeFunction($item->fk_et_function) }}</td>
                                                                    <td>
                                                                        {{ Helper::get_timeslot($s->fk_et_slot_time) }}<br>
                                                                    </td>
                                                                    <td class="text-right">{{ $s->eht_total }}</td>
                                                                    <td class="text-right">{{ $s->eht_subtotal }}</td>
                                                                    {{-- @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                                        <td class="text-right">
                                                                            <a href="{{ url('/sport/external/delete/slotdewan', [Crypt::encrypt($data['mbid']), Crypt::encrypt($s->id)]) }}" class="btn btn-primary btn-sm mx-auto">Hapus</button>
                                                                        </td>
                                                                    @endif --}}
                                                                </tr>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            {{-- @foreach ($data['et_hall_book'] as $item)
                                                @php
                                                    $ehbid = $item->id
                                                @endphp
                                                @foreach($data['et_hall_time'] as $s)
                                                    @if ($s->fk_et_hall_book == $ehbid)
                                                        @php
                                                            $totalHall += $s->eht_subtotal;
                                                            $totalAll += $s->eht_total;
                                                            $subtotalAll += $s->eht_subtotal
                                                        @endphp
                                                        <tr>
                                                            @if ($i == 0)
                                                                <td rowspan="{{count($data['et_hall_time'])}}">{{ Helper::location($data['main']->fk_lkp_location) }}</td>
                                                                <td rowspan="{{count($data['et_hall_time'])}}">{{ Helper::tempahanSportType($data['et_booking_facility'][0]->fk_et_facility_type) }}</td>
                                                                <td rowspan="{{count($data['et_hall_time'])}}">{{ Helper::date_format($data['et_booking_facility'][0]->ebf_start_date) }}</td>
                                                            @endif
                                                            <td>{{ Helper::typeFunction($item->fk_et_function) }}</td>
                                                            <td>
                                                                {{ Helper::get_timeslot($s->fk_et_slot_time) }}<br>
                                                            </td>
                                                            <td class="text-right">{{ $s->eht_total }}</td>
                                                            <td class="text-right">{{ $s->eht_subtotal }}</td>
                                                            @if ($data['et_hall_book'][0]->fk_et_function != 10)
                                                                <td class="text-right">
                                                                    <a href="{{ url('/sport/external/delete/slotdewan', [Crypt::encrypt($data['mbid']), Crypt::encrypt($s->id)]) }}" class="btn btn-primary btn-sm mx-auto">Hapus</button>
                                                                </td>
                                                            @endif
                                                        </tr>
                                                        @php
                                                            $i++
                                                        @endphp
                                                    @endif
                                                @endforeach
                                            @endforeach --}}
                                            
                                            <tr>
                                                <td colspan="5" class="text-right"><b>Jumlah</b></td>
                                                <td class="text-right"><b>{{ Helper::moneyhelper($totalAll) }}</b></td>
                                                <td class="text-right"><b>{{ Helper::moneyhelper($subtotalAll) }}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            {{-- </div> --}}
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Senarai Slot Masa -->
                    <!--start::Senarai Kelengkapan -->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Kelengkapan</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Penggunaan</th>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Item</th>
                                        <th class="text-right">Kuantiti</th>
                                        <th class="text-right">Harga Seunit (RM)</th>
                                        <th class="text-right">Jumlah (RM)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $eq_total = 0;
                                        $eq_subtotal = 0;
                                    @endphp
                                    @foreach($data['et_equipment_book'] as $s)
                                        @php
                                            $eq_total += $s->eeb_unit_price;
                                            $eq_subtotal += $s->eeb_subtotal;
                                        @endphp
                                        <tr>
                                            <td>{{ Helper::typeFunction($s->fk_et_function) }}</td>
                                            <td>{{ Helper::date_format($s->eeb_booking_date) }}</td>
                                            <td>{{ Helper::get_equipment($s->fk_et_equipment) }}</td>
                                            <td class="text-right">{{ $s->eeb_quantity }}</td>
                                            <td class="text-right">{{ $s->eeb_unit_price }}</td>
                                            <td class="text-right">{{ $s->eeb_subtotal }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-right" colspan="4"><b>Jumlah</b></td>
                                        <td class="text-right"><b>{{ Helper::moneyhelper($eq_total) }}</b></td>
                                        <td class="text-right"><b>{{ Helper::moneyhelper($eq_subtotal) }}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ url('sport/external') }}" class="btn btn-outline-danger float-right font-weight-bolder mt-2 mr-2">Kembali</a>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Senarai Kelengkapan -->
                </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->

@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    @include('sport.dashboard.js.approval')
@endsection




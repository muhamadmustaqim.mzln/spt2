@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-custom gutter-b">
                        <div class="card-header pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder">Kutipan Bayaran</span>
                            </h3>
                        </div>
                        <form action="" method="post">
                            @csrf
                            <div class="card-body pt-5">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">No. Sebutharga</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="sebutharga" placeholder="No Sebutharga" value="{{ $data['bh_quotation']->bq_quotation_no }}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jumlah Tempahan (RM)</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="jumlahTempahan" placeholder="Jumlah Tempahan(RM)" value="{{ $data['main']->bmb_subtotal }}" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jumlah Deposit (RM)</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="deposit" id="deposit" placeholder="Deposit(RM)" value="{{ $data['main']->bmb_deposit_rm }}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jumlah Telah Dibayar (RM)</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="jumlahtelahdibayar" placeholder="Jumlah Telah Dibayar(RM)" value="{{ Helper::moneyhelper($data['paid']) }}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jumlah Perlu Dibayar (RM)</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="jumlahPerluDibayar" id="jumlahperludibayar" placeholder="Jumlah Perlu Dibayar(RM)" value="{{ Helper::moneyhelper($data['payNeeded']) }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jenis Bayaran</label>
                                            <div class="col-lg-8">
                                                <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4, 19])) disabled @endif name="jenis_bayaran" id="jenis_bayaran" class="form-control" required>
                                                    <option value="" @if(in_array($data['main']->fk_lkp_status, [2, 3, 4, 19])) selected @else hidden @endif disabled>Sila Pilih</option>
                                                    @if ($data['depoStatus'] == 1)
                                                        <option value="1">DEPOSIT</option>
                                                    @elseif ($data['depoStatus'] == 0)
                                                        <option value="2">PENUH</option>
                                                    {{-- @else
                                                        <option value="2">PENUH</option> --}}
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Cara Bayaran</label>
                                            <div class="col-lg-8">
                                                <select @if(!in_array($data['main']->fk_lkp_status, [2, 3, 4, 19])) disabled @endif required name="carabayaran" id="carabayaran" class="form-control" required>
                                                    <option value="" @if(in_array($data['main']->fk_lkp_status, [2, 3, 4, 19])) selected @else hidden @endif disabled>Sila Pilih</option>
                                                    @foreach ($data['payment_mode'] as $pm)
                                                        <option value="{{ $pm->id }}">{{ $pm->lpm_description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div id="nocekDiv" class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">No Cek</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="nocek" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <div id="namabankDiv" class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Nama Bank</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="namabank" placeholder="" value="" required/>
                                            </div>
                                        </div>
                                        <div id="lopo" class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Nombor LO/PO</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="lopo" placeholder="" value=""/>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="norujukanbayaran">
                                            <label class="col-lg-4 col-form-label font-weight-bold">No Rujukan Bayaran</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="rujukanBayaran" placeholder="No Rujukan Bayaran" value=""/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label font-weight-bold">Jumlah Bayaran (RM)</label>
                                            <div class="col-lg-8">
                                                <input disabled type="text" class="form-control" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value="{{ Helper::moneyhelper($data['payNeeded']) }}"/>
                                                <input hidden type="text" class="form-control" name="jumlahBayaran" id="jumlahbayaran" placeholder="Jumlah Bayaran" value="{{ $data['payNeeded'] }}"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                <button type="submit" class="btn btn-primary float-right font-weight-bolder" ><i class="fas fa-check-circle"></i> Teruskan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12">
                    <!--begin::Maklumat Pemohon-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <div class="card-header pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder">Maklumat Pemohon</span>
                            </h3>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2">
                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                            <td>{{ $data['user']->fullname }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                            <td>{{ $data['main']->bmb_booking_no }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                            <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                            <td>{{ $data['user']->email }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                            <td>{{ $data['main']->bmb_subtotal }}</td>
                                        </tr>
                                        <tr>
                                            <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                            <td>
                                                @if($data['main']->bmb_deposit_rm == null)
                                                    0.00
                                                @else 
                                                    {{ ($data['main']->bmb_deposit_rm) }}
                                                @endif
                                            </td>                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                            <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 d-flex justify-content-end">
                                    {{-- <a href="{{ url('hall/borangbpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Borang BPSK</a> --}}
                                </div>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Maklumat Pemohon-->
                    
                    <!--start::Maklumat Acara-->
                    {{-- <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>Item</th>
                                            <th>Tarikh Mula</th>
                                            <th>Tarikh Akhir</th>
                                            <th>Tujuan Tempahan</th>
                                            <th>Maklumat Acara</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $i = 0;
                                        $j = 1;
                                    @endphp
                                    <tbody>
                                        @if(count($data['booking_detail']) > 0)
                                            @foreach($data['booking_detail'] as $p)
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>{{ Helper::getHall($data['bh_booking'][$i++]->fk_bh_hall) }}</td>
                                                <td>{{ Helper::date_format($p->bbd_start_date) }}</td>
                                                <td>{{ Helper::date_format($p->bbd_end_date) }}</td>
                                                <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                                <td>{{ $p->bbd_event_name }}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">Tiada Maklumat Acara</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div> --}}
                    <!--end::Maklumat Acara-->
                    <!--start::Kadar Sewaan Fasiliti-->
                    {{-- <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>Nama Fasiliti</th>
                                            <th>Slot Masa</th>
                                            <th>Tarikh Penggunaan</th>
                                            <th>Lokasi</th>
                                            <th>Harga Sewaan (RM)</th>
                                            <th>Kod GST</th>
                                            <th>GST (RM)</th>
                                            <th>Jumlah (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                            $total = 0.00;
                                        @endphp
                                        @foreach($data['slot'] as $s)
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                            <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                            <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                            <td>{{$s->ebf_start_date}}</td>
                                            <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                            <td style="text-align: end">{{number_format($s->est_total, 2)}}</td>
                                            <td style="text-align: end">{{ Helper::tempahanSportGST($s->est_gst_code)}}</td>
                                            <td style="text-align: end">{{$s->est_gst_rm}}</td>
                                            <td style="text-align: end">{{number_format($s->est_subtotal, 2)}}</td>
                                            @php
                                                $total = $total + number_format($s->est_subtotal, 2)
                                            @endphp
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="8" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan (RM)</td>
                                            <td class="font-weight-bolder" style="text-align: end">{{ $total_v = number_format($total, 2) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div> --}}
                    <!--end::Kadar Sewaan Fasiliti-->
                </div>
                <div class="col-lg-12">
                    <!--start::Senarai Resit-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>Jenis Bayaran</th>
                                            <th>No. Resit</th>
                                            @if(count($data['online']) > 0)
                                            <th>No. SAP</th>
                                            @endif
                                            <th>Tarikh Transaksi</th>
                                            <th>Jumlah Bayaran (RM)</th>
                                            {{-- <th>Jumlah Baki Pembayaran (RM)</th> --}}
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $j = 1;
                                        $paid = 0.00;
                                    @endphp
                                    <tbody>
                                        @if(count($data['kaunter']) > 0)
                                            @foreach($data['kaunter'] as $p)
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                <td>{{ $p->bp_receipt_number }}</td>
                                                <td>{{ $p->bp_receipt_date }}</td>
                                                <td>{{ $p->bp_paid_amount }}</td>
                                                <td style="width: 15%;">
                                                    <a href="{{ url('sport/resitonline', Crypt::encrypt($data['main']->id)) }}" target="_blank" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                </td>
                                            </tr>
                                            @php
                                                $paid = $paid + $p->bp_paid_amount
                                            @endphp
                                            @endforeach
                                        @elseif(count($data['online']) > 0)
                                            @foreach($data['online'] as $p)
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                                <td>{{ $p->fpx_trans_id }}</td>
                                                <td>{{ $data['main']->sap_no ?? "" }}</td>
                                                <td>{{ $p->fpx_date }}</td>
                                                <td>{{ $p->amount_paid }}</td>
                                                <td><?php ?>6625.00<?php ?></td>
                                                <td style="width: 15%;">
                                                    <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                                    <a href="https://spt2.ppj.gov.my/hall/bayaran/eyJpdiI6InN0dEFyNXI2T3p6SnJKK01jdWgrRmc9PSIsInZhbHVlIjoiRkFRMmJHSUZQc1BMNGNkejFWQUMydz09IiwibWFjIjoiYzJlMjgzOTJhOWNjMzg5MzdiZTE1YzQwMjAyM2NmODAwNDllNjNhMTg0NmFjZjdkNDY2NzgyZGIxMGUxMmJkZiIsInRhZyI6IiJ9" class="btn btn-sm btn-primary">Bayar Baki</a>
                                                </td>
                                            </tr>
                                            @php
                                                $paid = $paid + $p->amount_paid
                                            @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="7">Tiada Resit Bayaran</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                <!--end::Senarai Resit-->
                <!--start::Maklumat Acara-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Fasiliti</th>
                                        <th>Tarikh Mula</th>
                                        <th>Tarikh Akhir</th>
                                        <th>Tujuan</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                @endphp
                                <tbody>
                                    @foreach($data['ebf'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::tempahanSportDetail($p->fk_et_facility_detail) }}</td>
                                        <td>{{ Helper::date_format($p->ebf_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->ebf_end_date) }}</td>
                                        <td>{{ Helper::typeFunction($data['ehb'][0]->fk_et_function) }}</td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Maklumat Acara-->
                <!--start::Kadar Sewaan Dewan-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Dewan/ Fasiliti</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Nama Dewan/ Fasiliti</th>
                                        <th>Tarikh</th>
                                        <th>Jenis Penggunaan</th>
                                        <th>Slot Masa</th>
                                        <th class="text-center">Kuantiti</th>
                                        <th class="text-right">Harga (RM)</th>
                                        <th class="text-right">Jumlah Sewaan (RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                    $i = 1;
                                    $total = 0;
                                    $subtotal = 0;
                                @endphp
                                <tbody>
                                    @foreach ($data['slot'] as $s)
                                        @php
                                            $total += $s->eht_total;
                                            $subtotal += $s->eht_subtotal;
                                        @endphp
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                            <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                            <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                            <td>{{ Helper::date_format($s->ebf_start_date) }}</td>
                                       
                                            <td>{{ Helper::tempahanHallTime($s->fk_et_facility_price) }}</td>
                                            {{-- <td style="text-align: end">{{number_format($s->eht_total, 2)}}</td> --}}
                                            {{-- <td style="text-align: end">{{ Helper::tempahanSportGST($s->eht_gst_code)}}</td>
                                            <td style="text-align: end">{{$s->eht_gst_rm}}</td> --}}
                                            <td style="text-align: end">{{number_format($s->eht_subtotal, 2)}}</td>
                                            @php
                                                $total = $total + number_format($s->eht_subtotal, 2)
                                            @endphp
                                            <td class="text-right">{{ $s->eht_total }}</td>
                                            <td class="text-right">{{ $s->eht_subtotal }}</td>
                                            {{-- @if ($data['main']->fk_lkp_status )
                                                @php
                                                    $paid = $total + number_format($s->eht_subtotal, 2)
                                                @endphp
                                            @endif --}}
                                        </tr>
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="6">Jumlah(RM)</td>
                                        <td class="text-right">{{ Helper::moneyhelper($subtotal) }}</td>
                                        <td class="text-right">{{ Helper::moneyhelper($subtotal) }}</td>
                                    </tr> 
                                    {{-- @foreach($data['eht'] as $p)
                                        @php
                                            $total += $p->eht_total;
                                            $subtotal += $p->eht_subtotal;
                                        @endphp
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                            <td>{{ Helper::tempahanSportDetail($data['ebf'][0]->fk_et_facility_detail) }}</td>
                                            <td>{{ Helper::date_format($data['ehb'][0]->ehb_booking_date) }}</td>
                                            <td>{{ Helper::typeFunction(Helper::get_harga_data($p->fk_et_facility_price)->fk_et_function) }}</td>
                                            <td>{{ Helper::get_slot_masa($p->fk_et_slot_time) }}</td>
                                            <td class="text-center">1</td>
                                            <td class="text-right">{{ $p->eht_total }}</td>
                                            <td class="text-right">{{ $p->eht_subtotal }}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="6">Jumlah(RM)</td>
                                        <td class="text-right">{{ Helper::moneyhelper($total) }}</td>
                                        <td class="text-right">{{ Helper::moneyhelper($subtotal) }}</td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Kadar Sewaan Dewan-->
                <!--start::Kadar Sewaan Peralatan-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Nama Dewan/ Fasiliti</th>
                                        <th>Item</th>
                                        <th>Tarikh</th>
                                        <th>Jenis Penggunaan</th>
                                        <th class="text-center">Kuantiti</th>
                                        <th class="text-right">Harga (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th>
                                        <th class="text-right">Jumlah Sewaan (RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                    $totaleq = 0;
                                    $subtotaleq = 0;
                                @endphp
                                <tbody>
                                    @if(count($data['kelengkapan']) > 0)
                                        @foreach($data['kelengkapan'] as $p)
                                            @php
                                                $totaleq += $p->eeb_total;
                                                $subtotaleq += $p->eeb_subtotal;
                                            @endphp
                                            <tr>
                                                <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                                <td>{{ Helper::tempahanSportDetail($data['ebf'][0]->fk_et_facility_detail) }}</td>
                                                <td>{{ Helper::get_equipment($p->fk_et_equipment) }}</td>
                                                <td>{{ Helper::date_format($p->eeb_booking_date) }}</td>
                                                <td>{{ Helper::typeFunction($p->fk_et_function) }}</td>
                                                <td class="text-center">{{ $p->eeb_quantity }}</td>
                                                <td class="text-right">{{ Helper::moneyhelper($p->eeb_total) }}</td>
                                                <td class="text-right">{{ Helper::moneyhelper($p->eeb_subtotal) }}</td>
                                            </tr>
                                        @endforeach
                                        <tr class="font-weight-bolder text-right">
                                            <td colspan="6">Jumlah(RM)</td>
                                            <td class="text-right">{{ Helper::moneyhelper($totaleq) }}</td>
                                            <td class="text-right">{{ Helper::moneyhelper($subtotaleq) }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="10">Tiada Data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Kadar Sewaan Peralatan-->
                <!--start::Jumlah Keseluruhan-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                        <td class="text-center">E</td>
                                        <td class="text-right">0.00</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                        <td class="text-center">I</td>
                                        <td class="text-right">0.00</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                        <td></td>
                                        <td class="text-right">{{ $data['main']->bmb_subtotal }}</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                        <td></td>
                                        <td class="text-right">{{ $data['main']->bmb_rounding }}</td>
                                    </tr>
                                    <tr>
                                        <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                        <td></td>
                                        <td class="text-right">{{ number_format($paid, 2) }}</td>
                                    </tr>
                                </table>
                            </div>
                            <!--end: Datatable-->
                        </div>
                    </div>
                    <!--end::Jumlah Keseluruhan-->
                    <div class="float-right">
                        {{-- <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</button> --}}
                        {{-- <a href="" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</a> --}}
                        {{-- <a href="{{ url('hall/dashboard/maklumatkaunter', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Teruskan</a> --}}
                        {{-- <a href="{{ url('hall/bayaran', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Bayar Online</a> --}}
                        <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('hall.dashboard.js.pengesahan')
@endsection				



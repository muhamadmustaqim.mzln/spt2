<script>
    $(document).ready(function() {
        $('#fileUpload').on('change', function() {
            var filename = $(this).val().replace(/C:\\fakepath\\/i, ''); // Extract filename
            $('#selectedFile').text('Selected file: ' + filename);
        });
        $(".btn-confirm-custom").on('click', function(event) {
            event.preventDefault();
            const form = $(this).closest('form');
            const type = form.find('input[name="type"]').val();

            // Check which form is being submitted based on the "type" value
            if (type === '1') {
                // Form type is 1 (Senarai Fail)
                const fileInput = form.find('input[name="fail"]');
                
                // Check if a file is selected
                if (fileInput[0].files.length === 0) {
                    // No file selected, show an error message
                    Swal.fire({
                        title: 'Harap Maaf!',
                        text: 'Sila pilih fail yang hendak dimuat naik.',
                        icon: 'error'
                    });
                    return; // Prevent form submission
                }
            }
            
            
            if (form[0].checkValidity()) {
                Swal.fire({
                    title: 'Anda pasti?',
                    text: "Maklumat dihantar adalah benar!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Kembali',
                    confirmButtonText: 'Saya Pasti'
                }).then((result) => {
                    if (result.value) {
                        // console.log(result, result.value);
                        form.submit();
                    }
                });
            } else {
                // If the form is not valid, focus on the first invalid input field
                Swal.fire({
                    title: 'Harap Maaf!',
                    text: 'Sila isi ruangan diperlukan.',
                    icon: 'error'
                })
            }
        });
    })
</script>
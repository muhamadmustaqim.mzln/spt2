@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Semak Kekosongan Fasiliti</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Sukan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Semak Kekosongan Fasiliti</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!-- Navigation Bar::Start -->
                            <ul class="nav nav-tabs nav-fill mx-2" id="myTab" role="tablist">
                                <li class="nav-item position-relative" id="tempahanSukanNavItem">
                                    <a class="nav-link @if($data['tab'] == 1) active @endif pr-1" id="tempahanSukan-tab" data-toggle="tab" href="#tempahanSukan" aria-controls="tempahanSukan">
                                        <span class="nav-text">Semak Kekosongan Sukan</span>
                                    </a>
                                </li>
                                <li class="nav-item position-relative" id="tempahanDewanNavItem">
                                    <a class="nav-link @if($data['tab'] == 2) active @endif pr-1" id="tempahanDewan-tab" data-toggle="tab" href="#tempahanDewan">
                                        <span class="nav-text">Semak Kekosongan Dewan</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content mt-5" id="myTabContent">
                                <div class="tab-pane fade show @if($data['tab'] == 1) active @endif" id="tempahanSukan" role="tabpanel" aria-labelledby="tempahanSukan-tab">
                                    <div class="row px-4">
                                        <div class="col-lg-4">
                                            <!--begin::List Widget 21-->
                                            <div class="card card-custom gutter-b">
                                                <!--begin::Body-->
                                                <div class="card-body pt-2">
                                                    <form action="" method="post">
                                                        @csrf
                                                        <input hidden type="text" name="tab" value="1">
                                                        <div class="form-group row">
                                                            <div class="col-lg-12">
                                                                <select name="" id="kt_select2_4" class="form-control mt-3" required>
                                                                    <option value="">Sila Pilih Lokasi</option>
                                                                    @foreach ($data['location'] as $l)
                                                                        <option value="{{ $l->id }}">{{ $l->lc_description }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select name="lokasi" id="kt_select2_5" class="form-control mt-3" required disabled>
                                                                    <option value="">Sila Pilih Fasiliti</option>
                                                                </select>
                                                                <input type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh" autocomplete="off" required>
                                                            </div>
                                                        </div>
                                                        <div class="float-right">
                                                            <button type="submit" id="submit_form" class="btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--end::Body-->
                                            </div>
                                            <!--end::List Widget 21-->
                                        </div>
                                        <div class="col-lg-8">
                                            <!--begin::List Widget 21-->
                                            @if ($data['post'] == true)
                                                <div class="card card-custom gutter-b">
                                                    <div class="card-header border-0 pt-5">
                                                        <h3 class="card-title align-items-start flex-column mb-5">
                                                            <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Kekosongan </span>
                                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Fasiliti : {{ Helper::getSportFacility($data['id']) }}</span>
                                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Kekosongan : {{ Helper::date_format2($data['date']) }}</span>
                                                        </h3>
                                                    </div>
                                                    <!--begin::Body-->
                                                    <div class="card-body pt-2">
                                                        @php
                                                        $fasiliti = "";   
                                                        @endphp
                                                        @foreach ($data['facility'] as $f)
                                                        <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                                            <thead>
                                                                <tr>
                                                                    <th>Fasiliti</th>
                                                                    <th>Slot Masa</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($data['slot'] as $s)
                                                                @if ($f->efd_name == $s->efd_name)
                                                                <tr>
                                                                    <td>{{ $s->efd_name }}</td>
                                                                    <td>{{ $s->est_slot_time }}</td>
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                        @endforeach
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                            @endif
                                            <!--end::List Widget 21-->
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade show @if($data['tab'] == 2) active @endif" id="tempahanDewan" role="tabpanel" aria-labelledby="tempahanDewan-tab">
                                    <div class="row px-4">
                                        <div class="col-lg-4">
                                            <!--begin::List Widget 21-->
                                            <div class="card card-custom gutter-b">
                                                <!--begin::Header-->
                                                {{-- <div class="card-header border-0 pt-5">
                                                    <h3 class="card-title align-items-start flex-column mb-5">
                                                        <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                                    </h3>
                                                </div> --}}
                                                <!--end::Header-->
                                                <!--begin::Body-->
                                                <div class="card-body pt-2">
                                                    <form action="" method="post">
                                                        @csrf
                                                        <input hidden type="text" name="tab" value="2">
                                                        <div class="form-group row">
                                                            <div class="col-lg-12">
                                                                <select name="presint" id="kt_select2_6" class="form-control mt-3" required>
                                                                    <option value="">Sila Pilih Lokasi</option>
                                                                    @foreach ($data['location'] as $l)
                                                                        <option value="{{ $l->id }}" >{{ $l->lc_description }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select name="lokasi" id="kt_select2_7" class="form-control mt-3" required disabled>
                                                                    <option value="">Sila Pilih Fasiliti</option>
                                                                </select>
                                                                <input value="@if(isset($data['date'])){{ Helper::date_format($data['date']) }} @endif" type="text" name="tarikh" class="form-control mt-3" id="kt_daterangepicker_6" placeholder="Tarikh" autocomplete="off" required>
                                                            </div>
                                                        </div>
                                                        <div class="float-right">
                                                            <button type="submit" id="submit_form_dewan" class="btn btn-light-primary  font-weight-bold mr-2">Carian</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--end::Body-->
                                            </div>
                                            <!--end::List Widget 21-->
                                        </div>
                                        <div class="col-lg-8">
                                            <!--begin::List Widget 21-->
                                            @if ($data['postdewan'] == true)
                                                <div class="card card-custom gutter-b">
                                                    <div class="card-header border-0 pt-5">
                                                        <h3 class="card-title align-items-start flex-column mb-5">
                                                            <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Kekosongan </span>
                                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Fasiliti : {{ Helper::getSportFacility($data['id']) }}</span>
                                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Kekosongan : {{ Helper::date_format2($data['date']) }}</span>
                                                        </h3>
                                                    </div>
                                                    <!--begin::Body-->
                                                    <div class="card-body pt-2">
                                                        @php
                                                        $fasiliti = "";   
                                                        @endphp
                                                        @foreach ($data['facilitydewan'] as $f)
                                                        <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                                            <thead>
                                                                <tr>
                                                                    <th>Fasiliti </th>
                                                                    <th>Slot Masa</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($data['slotdewan'] as $s)
                                                                @if (strpos($s->efd_name, $f->eft_type_desc) !== false)
                                                                <tr>
                                                                    <td>{{ $s->efd_name }}</td>
                                                                    <td>{{ $s->est_slot_time }}</td>
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                        @endforeach
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                            @endif
                                            <!--end::List Widget 21-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

@section('js_content')
    @include('sport.available.js.index')
@endsection
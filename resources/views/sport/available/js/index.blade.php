<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('#kt_daterangepicker_6').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    var csrfToken = "{{ csrf_token() }}";
        $('#kt_select2_4').on('change', function(){
            var location_id = $(this).val();
            if (location_id == ''){
                $('#kt_select2_5').prop('disabled', true);
            }
            else{
                $('#kt_select2_5').prop('disabled', false);
                $.ajax({
                    url:"{{ url('sport/ajax') }}",
                    type: "POST",
                    data: {'location_id' : location_id},
                    dataType: 'json',
                    success: function(data){
                        $('#kt_select2_5').html(data);
                    }
                });
            }
        });
        $('#kt_select2_6').on('change', function(){
            var location_id = $(this).val();
            if (location_id == ''){
                $('#kt_select2_7').prop('disabled', true);
            }
            else{
                $('#kt_select2_7').prop('disabled', false);
                $.ajax({
                    url:"{{ url('sport/ajaxhall') }}",
                    type: "POST",
                    data: {
                        '_token': csrfToken,
                        'location_id' : location_id},
                    dataType: 'json',
                    success: function(data){
                        if (data.includes('Tiada')) {
                            $('#submit_form_dewan').prop('disabled', true);
                        } else {
                            $('#kt_select2_7').prop('disabled', false);
                            $('#submit_form_dewan').prop('disabled', false);
                        }
                        $('#kt_select2_7').html(data);
                        // $('#kt_select2_7').html(data);
                    }
                });
            }
        });
</script>
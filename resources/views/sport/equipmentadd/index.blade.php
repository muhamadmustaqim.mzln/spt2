@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Gabung Tempahan Peralatan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Peralatan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="row">
                <div class="col-3">
                    <form action="" id="" method="post">
                        @csrf
                        <input type="text" name="type" value="1" hidden>
                        <div class="card card-custom">
                            <div class="card-body">
                                <b class="h6">Semakan Tempahan</b>
                                <p class="my-3">Masukkan No Tempahan:</p>
                                <input type="text" value="{{ $data['booking_no'] }}" class="form-control my-3" name="bmb_booking_no" required>
                                <button class="btn btn-primary my-3">Carian Tempahan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-9">
                    @if ($data['post'] == 1)
                        <div class="card card-custom">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <h3 class="card-label">Tambah Peralatan & Kelengkapan</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="my-3">Maklumat Tempahan:</p>
                                <div class="table-responsive">
                                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Kemudahan</th>
                                                <th>Dari</th>
                                                <th>Hingga</th>
                                                <th>Status</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($data['mbebf'] as $val)
                                                <tr>
                                                    <td>{{ Helper::get_nama($val->fk_users) }}</td>
                                                    <td>{{ Helper::get_email($val->fk_users) }}</td>
                                                    <td>{{ Helper::getSportFacility($val->fk_et_facility_type) }}</td>
                                                    <td>{{ Helper::date_format($val->ebf_start_date) }}</td>
                                                    <td>{{ Helper::date_format($val->ebf_end_date) }}</td>
                                                    <td>{{ Helper::get_status_tempahan($val->fk_lkp_status) }}</td>
                                                    <td>
                                                        <a class="btn btn-primary btn-sm" href="{{ url('/sport/equipment_edit', Crypt::encrypt($data['mb']->id)) }}"></a>
                                                        {{-- <form action="" method="post">
                                                            @csrf
                                                            <input hidden type="text" value="{{ $data['booking_no'] }}" class="form-control my-3" name="bmb_booking_no" required>
                                                            <input type="text" name="type" value="2" hidden>
                                                            <button type="submit" class="btn btn-primary btn-sm">Tambah Peralatan</button>
                                                        </form> --}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- <div class="card-footer">
                                <div class="float-right">
                                    <a href="{{ url('/sport/admin/equipment') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                                    <button class="btn btn-primary font-weight-bold">Simpan</button>
                                </div>
                            </div> --}}
                        </div>

                    @elseif ($data['post'] == 1)
                        <div class="card card-custom">
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                {{-- <div class="card-title">
                                    <h3 class="card-label">Tempahan </h3>
                                </div> --}}
                                {{-- @foreach ($data['mb'] as $val) --}}
                                    <div class="row">
                                        <div class="col-12 h4"><p>Tempahan bagi <b>{{ Helper::get_nama($data['mb']->fk_users) }}</b></p></div>
                                        <div class="col-12"><p>No Tempahan : {{ $data['mb']->bmb_booking_no }} / Tarikh Tempahan : {{ $data['mb']->created_at }}</p></div>
                                        <div class="col-12"><p>No Telefon : {{ Helper::get_no($data['mb']->fk_users) }} / Email : {{ Helper::get_email($data['mb']->fk_users) }}</p></div>
                                    </div>
                                {{-- @endforeach --}}
                            </div>
                            <div class="card-body">
                                <p class="my-3"><b>Senarai Fasiliti Ditempah</b></p>
                                <div class="table-responsive">
                                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Lokasi</th>
                                                <th>Tarikh Kegunaan Dari</th>
                                                <th>Tarikh Kegunaan Hingga</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['ebf'] as $val)
                                                <tr>
                                                    <td>{{ Helper::tempahanSportDetail($val->fk_et_facility_detail) }}</td>
                                                    <td>{{ Helper::location($data['mb']->fk_lkp_location) }}</td>
                                                    <td>{{ Helper::date_format($val->ebf_start_date) }}</td>
                                                    <td>{{ Helper::date_format($val->ebf_end_date) }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#staticBackdrop2" data-item-id="{{ $val->id }}">Tambah Peralatan</button>
                                                    </td>
                                                    {{-- <td><button class="btn btn-primary btn-sm">Tambah Peralatan</button></td> --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                                <p class="my-3"><b>Senarai Peralatan Ditempah</b></p>
                                Penggunaan di kompleks
                                <div class="table-responsive">
                                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Tarikh Kegunaan</th>
                                                <th>Item</th>
                                                <th>Kuantiti</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['eebdalaman'] as $val)
                                                @php
                                                    $paid= DB::table('et_payment_detail')
                                                        ->select('id','created_at')
                                                        ->where('fk_et_booking_facility',$val->fk_et_booking_facility)
                                                        ->where('fk_et_equipment',$val->fk_et_equipment)
                                                        ->where('booking_date',$val->eeb_booking_date)
                                                        ->where('deleted_at', NULL)
                                                        ->where('created_at','>', date('Y-m-d H:i:s', strtotime($val->created_at)))
                                                        ->first();
                                                @endphp
                                                <tr>
                                                    <td>{{ Helper::date_format($val->eeb_booking_date) }}</td>
                                                    <td>{{ Helper::get_equipment($val->fk_et_equipment) }}</td>
                                                    <td>{{ $val->eeb_quantity }} {{ $val->created_at }}</td>
                                                    <td>
                                                        @if ($paid != null)
                                                            Telah Dibayar
                                                        @else
                                                            <a href="#" class="btn btn-primary btn-sm delete-equipment" data-id="{{ Crypt::encrypt($val->id) }}">Hapus</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                                Penggunaan di luar kompleks
                                <div class="table-responsive">
                                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                                        <thead>
                                            <tr>
                                                <th>Tarikh Kegunaan</th>
                                                <th>Item</th>
                                                <th>Kuantiti</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data['eebluaran'] as $val)
                                            @php
                                                $paid= DB::table('et_payment_detail')
                                                    ->select('id','created_at')
                                                    ->where('fk_et_booking_facility',$val->fk_et_booking_facility)
                                                    ->where('fk_et_equipment',$val->fk_et_equipment)
                                                    ->where('booking_date',$val->eeb_booking_date)
                                                    ->where('deleted_at', NULL)
                                                    ->where('created_at','>', date('Y-m-d H:i:s', strtotime($val->created_at)))
                                                    ->first();
                                            @endphp
                                            <tr>
                                                <td>{{ Helper::date_format($val->eeb_booking_date) }}</td>
                                                <td>{{ Helper::get_equipment($val->fk_et_equipment) }}</td>
                                                <td>{{ $val->eeb_quantity }}</td>
                                                <td>
                                                    @if ($paid != null)
                                                        Telah Dibayar
                                                    @else
                                                        <a href="#" class="btn btn-primary btn-sm delete-equipment" data-id="{{ Crypt::encrypt($val->id) }}">Hapus</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                    </div>
                                </div>
                                <div class="modal fade" id="staticBackdrop2" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop2" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Tambah Tempahan Peralatan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <form action="" method="post">
                                                @csrf
                                                <input type="text" name="type" value="3" hidden>
                                                <div class="modal-body">
                                                    <select name="kegunaan" id="kt_select2_4" class="form-control mt-3" required>
                                                        <option disabled selected value="">Pilih Kegunaan Peralatan</option>
                                                        <option value="1">Kegunaan Dalaman</option>
                                                        <option value="2">Kegunaan Luaran</option>
                                                    </select>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <select hidden name="dalaman" id="dalaman" class="form-control mt-3" required>
                                                                @foreach ($data['equipment_int'] as $l)
                                                                    <option value="{{ $l->eid }}">{{ $l->item }}</option>
                                                                @endforeach
                                                            </select>
                                                            <select hidden name="luaran" id="luaran" class="form-control mt-3" required>
                                                                @foreach ($data['equipment_ext'] as $l)
                                                                    <option value="{{ $l->eid }}" data-max="">{{ $l->item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div hidden id="kuantiti" class="col-6">
                                                            <input type="number" class="form-control mt-3" name="kuantiti" min="0">
                                                        </div>
                                                    </div>
                                                    <input hidden type="text" value="{{ $data['booking_no'] }}" class="form-control my-3" name="bmb_booking_no" required>
                                                    <input type="text" name="itemId" id="itemId" class="form-control" hidden>
                                                    <button type="submit" class="btn btn-success float-right font-weight-bolder m-5" data-toggle="modal" data-target="#staticBackdrop">
                                                        Tambah Peralatan
                                                    </button>
                                                </div>
                                            </form>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button class="btn btn-primary btn-sm float-right mx-1 btn-confirm">Teruskan Tempahan</button>
                                    <button class="btn btn-warning btn-sm float-right mx-1">Tambah Kegunaan</button>
                                    {{-- <a href="{{ url('/sport/admin/equipment') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                                    <button class="btn btn-primary font-weight-bold">Simpan</button> --}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.equipmentadd.js.index')
@endsection
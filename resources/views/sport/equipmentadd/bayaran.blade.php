@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Bayaran Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Bayaran Kaunter</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <form action="" method="post">
                        @csrf
                        <input type="text" name="type" value="1" hidden>
                        <input type="hidden" name="ext_eq" value="{{ json_encode($data['ext_eq']) }}">
                        <input type="hidden" name="int_eq" value="{{ json_encode($data['int_eq']) }}">
                        <table class="table table-bordered">
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                <td>{{ $data['user']->fullname }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                <td>{{ $data['main']->bmb_booking_no }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                <td>{{ date('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                <td>{{ $data['user_detail']->bud_phone_no }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                <td>{{ $data['user']->email }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Bayaran (RM)</th>
                                <th class="text-danger">{{ Helper::moneyhelper($data['total']) }}</th>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jenis Bayaran</th>
                                <td>Bayaran Penuh</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Cara Bayaran</th>
                                <td>
                                    <select required name="bayaran" id="carabayaran" class="form-control" required>
                                        <option value="" disabled>Sila Pilih</option>
                                        @foreach ($data['bayaran'] as $pm)
                                            <option value="{{ $pm->id }}">{{ $pm->lpm_description }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr id="nocekDiv" hidden>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No Cek</th>
                                <td>
                                    <input type="text" class="form-control" name="nocek" placeholder="" value="" />
                                </td>
                            </tr>
                            <tr id="namabankDiv" hidden>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Nama Bank</th>
                                <td>
                                    <input type="text" class="form-control" name="namabank" placeholder="" value="" />
                                </td>
                            </tr>
                            <tr id="lopo" hidden>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Nombor LO/PO</th>
                                <td>
                                    <input type="text" class="form-control" name="lopo" placeholder="" value="" />
                                </td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">No. Rujukan Bayaran <span class="text-danger">*</span></th>
                                <td><input required type="text" class="form-control" name="ref" placeholder="No. Rujukan Bayaran" /></td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2"><i class="fas fa-check-circle"></i>Simpan</button>
                        <input type="hidden" name="total" value="{{ $data['total'] }}">
                    </form>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('sport.equipmentadd.js.index')
@endsection				

					



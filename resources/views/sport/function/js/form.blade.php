<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.2.9') }}"></script>
<script>
    var state = 1;
    $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
        if($('#TheCheckBox').bootstrapSwitch('state') == true){
            state = 1
        }else{
            state = 0
        }
    });
    $('input[name="status"]').val(state);
</script>
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $('#kt_select2_1').select2({
        placeholder: 'Sila Pilih Fungsi Penggunaan'
    });

    $('#kt_select2_2').select2({
        placeholder: 'Sila Pilih Fasiliti'
    });

</script>
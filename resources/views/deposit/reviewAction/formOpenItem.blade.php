@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Permohonan Pemulangan Deposit - Tindakan Penyemak</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Breadcrumb-->
            <div class="d-flex ml-auto">
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Pemulangan Deposit</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href=" {{ url('/department/review') }}" class="text-muted">Senarai Tindakan Penyemak</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Permohonan Pemulangan Deposit - Tindakan Penyemak</a>
                    </li>
                </ul>
            </div>
            <!--end::Breadcrumb-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/user/form/add') }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Borang Pemulangan Deposit</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Maklumat Pelanggan</span>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pelanggan</th>
                                        <td>Mustaqim</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Kad Pengenalan / No. Rujukan</th>
                                        <td>booking nombor</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Emel</th>
                                        <td>mustaqim@gmail.com</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                        <td>
                                            phone number
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Akaun</th>
                                        <td>email</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Nama Bank</th>
                                        <td>Maybank</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">Status</th>
                                        <td>Permohonan Baru Pemulangan Deposit</td>
                                    </tr>
                                    <tr>
                                        <th width="30%" class="text-light" style="background-color: #242a4c">No. Permohonan</th>
                                        <td>RFD2021007090085</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Maklumat Tempahan</span>
                        </div>
                        <div class="row">
                            <table class="table table-bordered">
                                <thead class="text-center text-white" style="background-color: #242a4c">
                                    <tr>
                                        <th>No Tempahan</th>
                                        <th>Keterangan</th>
                                        <th>No OTC</th>
                                        <th>No Resit</th>
                                        <th>Tarikh Resit</th>
                                        <th>Jumlah Deposit (RM)</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Laporan</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Report :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="reportdate" placeholder="Tarikh Report"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Sinopsis Laporan :</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="sinopsis" placeholder="Sinopsis Laporan" rows="7"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Status Pemulangan<span class="text-danger">*</span> :</label>
                            <div class="col-8 col-form-label">
                                <div class="radio-list">
                                    <label class="radio radio-success">
                                        <input type="radio" name="radios18" value="1"/>
                                        <span></span>
                                        Pemulangan Deposit Penuh
                                    </label>
                                    <!-- <label class="radio radio-success">
                                        <input type="radio" name="radios18" checked="checked" value="2"/>
                                        <span></span>
                                        Pemulangan Deposit Dengan Kerosakan
                                    </label> -->
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jumlah Pemulangan Deposit (RM) :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="jumlahrefunddeposit" placeholder="Jumlah Pemulangan Deposit (RM)"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Lampiran :</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <input name="lampiran" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="">
                                    <!-- {{-- <label for="fileUpload1" class="btn btn-primary btn-file-upload" >Muat Naik</label> --}} -->
                                </div>
                            </div>
                        </div>
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Maklumat Penyokong</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Nama Penyokong :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="supportdate" placeholder="Nama Penyokong"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Sokong :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="supportdate" placeholder="Tarikh Sokong"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Ulasan Sokong :</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="sinopsis" placeholder="Sinopsis Laporan" rows="7"></textarea>
                            </div>
                        </div>
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Tindakan</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jumlah Deposit Dibayar (RM) :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="totalpaid" placeholder="Jumlah Deposit Dibayar (RM)"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jumlah Caj Kerosakan Disyorkan :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="repaircharge" placeholder="Jumlah Caj Kerosakan Disyorkan"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Baki Jumlah Deposit Selepas Caj Kerosakan (RM) :</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="remainder" placeholder="Baki Jumlah Deposit Selepas Caj Kerosakan (RM)"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Ulasan<span class="text-danger">*</span> :</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="reviewulasan" placeholder="Ulasan" rows="7"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Catatan Text SAP :</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="textSAP" placeholder="Catatan" rows="1"></textarea>
                            </div>
                        </div>
                        <div class="row gutter-b" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Senarai Open Item</span>
                        </div>
                        <div class="row">
                            <table class="table table-bordered">
                                <thead class="text-center text-white" style="background-color: #242a4c">
                                    <tr>
                                        <th>No Dokumen</th>
                                        <th>No Caj</th>
                                        <th>Jumlah Kasar (RM)</th>
                                        <th>Jumlah Penjelasan (RM)</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                        <td>something</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Status Pemulangan Deposit<span class="text-danger">*</span> :</label>
                            <div class="col-8 col-form-label">
                                <div class="radio-list">
                                    <label class="radio radio-success">
                                        <input type="radio" name="radios18" value="1"/>
                                        <span></span>
                                        Pemulangan Deposit Penuh
                                    </label>
                                    <label class="radio radio-success">
                                        <input type="radio" name="radios18" value="2"/>
                                        <span></span>
                                        Pemulangan Deposit Dengan Hutang Di Perbadanan
                                    </label>
                                    <label class="radio radio-success">
                                        <input type="radio" name="radios18" checked="checked" value="3"/>
                                        <span></span>
                                        Pemulangan Deposit Dengan Lebihan Bayaran
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/department/review') }}" class="btn btn-outline-danger font-weight-bold">Batal</a>
                            <button class="btn btn-warning font-weight-bold">Kuiri</button>
                            <button class="btn btn-success font-weight-bold">Semak</button>
                            <button class="btn btn-light-primary font-weight-bold">Dokumen EFT</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection
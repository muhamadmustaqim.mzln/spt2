@extends('layouts.master')

@section('container')
<style>
	@media (max-width: 991.98px){
		.header-mobile-fixed .content {
			padding-top: 0;
		}
	}
</style>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h3 class="text-dark font-weight-bolder my-1 mr-5">Senarai Tindakan Penyemak</h3>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Breadcrumb-->
            <div class="d-flex ml-auto">
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-muted">Laman Utama</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Senarai Tindakan Penyemak</a>
                    </li>
                </ul>
            </div>
            <!--end::Breadcrumb-->
        </div>
    </div>
    <!--end::Subheader-->
	<div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Tindakan Penyemak</h3>
                    </div>
                </div>
				<hr>
				<form action="" method="get">
            		<div class="row m-2">
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="id" id="id" placeholder="Carian" value="">
                        </div>
                        <div class="col-md-3">
                        <button id="btnSearch" class="btn btn-primary font-weight-bolder px-5">
                            Cari
                            </button>
                        </div>
                    </div>
                </form>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead class="bg-dark text-center text-white">
                                <tr>
                                    <th>Bil</th>
                                    <th>No. Permohonan</th>
                                    <th>Nama Pemohon</th>
                                    <th>Tarikh Laporan</th>
                                    <th>Status</th>
                                    <th>Tarikh Mohon</th>
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @php
                                    $i = 1;
                                @endphp
                                <tr>
                                    <td style="width: 5%;">{{ $i++ }}</td>
                                    <td>RFD201201050081</td>
                                    <td>Mustaqim</td>
                                    <td>23-01-05</td>
                                    <td>
                                        <span class="label label-inline label-light-primary font-weight-bold">
                                            Permohonan Baru Pemulangan Deposit
                                        </span>
                                    </td>
                                    <td>2023-01-01</td>
                                    <td style="width: 10%; text-align: center;">
                                        <a class="btn btn-outline-primary btn-sm m-1" href="{{ url('/list_action_review/form') }}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Tindakan
                                        </a>
                                        <a class="btn btn-outline-warning btn-sm m-1" href="{{ url('/list_action_review/formOpenItem') }}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Berhutang
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <span class="font-weight-bold">Jumlah Keseluruhan : 1</span>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->	
</div>

<!--end::Content-->
@endsection

@section('js_content')
    @include('deposit.js.list')
@endsection
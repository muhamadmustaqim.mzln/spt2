@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Borang Pemulangan Deposit</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Breadcrumb-->
            <div class="d-flex ml-auto">
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Pemulangan Deposit</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Senarai Pemulangan Deposit</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Borang Pemulangan Deposit</a>
                    </li>
                </ul>
            </div>
            <!--end::Breadcrumb-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="" id="" method="post">
                @csrf
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Borang Pemulangan Deposit</h3>
                        </div>
                    </div>
                        @csrf
                        <div class="card-body">
                            {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                            <div class="row gutter-b" style="background-color: #242a4c">
                                <span class="text-white m-1 p-2">Maklumat Pelanggan</span>
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pelanggan</th>
                                            <td>{{ $data['deposit_']->name }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Kad Pengenalan / No. Rujukan</th>
                                            <td>{{ $data['deposit_']->subsystem_booking_no }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Emel</th>
                                            <td>{{ $data['deposit_']->email }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                            <td>
                                                {{ $data['deposit_']->mobile_no }}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">No Akaun</th>
                                            <td>{{ $data['deposit_']->bank_account }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c">Nama Bank</th>
                                            <td>{{ $data['deposit_']->bank_name }}</td>
                                        </tr>
                                        <!-- <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c"></th>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="text-light" style="background-color: #242a4c"></th>
                                            <td></td>
                                        </tr> -->
                                    </table>
                                </div>
                                <div class="col d-flex flex-row-reverse gutter-b">
                                    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                        Kemaskini
                                    </button>
                                </div>
                            </div>
                            <div class="row gutter-b" style="background-color: #242a4c">
                                <span class="text-white m-1 p-2">Maklumat Tempahan</span>
                            </div>
                            <div class="row">
                                <table class="table table-bordered">
                                    <thead class="text-center text-white" style="background-color: #242a4c">
                                        <tr>
                                            <th>No. Tempahan</th>
                                            <th>Keterangan</th>
                                            <th>No OTC</th>
                                            <th>No. Resit</th>
                                            <th>Tarikh Resit</th>
                                            <th>Jumlah Deposit (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr>
                                            <td>SPS251251312</td>
                                            <td>Tempahan Dewan Seri Melati</td>
                                            <td>OTC2000063</td>
                                            <td><a class="font-weight-bolder" href="{{ url('/list_action_support') }}"><u>8810129121</u></a></td>
                                            <td>04-06-2024</td>
                                            <td>300.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row gutter-b" style="background-color: #242a4c">
                                <span class="text-white m-1 p-2">Laporan</span>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Report<span class="text-danger">*</span> :</label>
                                <div class="col-lg-8">
                                    <input type="text" name="reportdate" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Report" autocomplete="off" required value="">
                                    <!-- <input type="text" name="reportdate" class="form-control mt-3" id="kt_daterangepicker_5" placeholder="Tarikh Report" autocomplete="off" required value="{{ isset($data['mula']) ? \Carbon\Carbon::createFromFormat('Y-m-d', $data['mula'])->format('d-m-Y') : '' }}"> -->
                                    <!-- <input type="text" class="form-control" name="reportdate" placeholder="Tarikh Report"/> -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Sinopsis Laporan<span class="text-danger">*</span> :</label>
                                <div class="col-lg-8">
                                    <textarea type="text" class="form-control" name="reportsynopsis" placeholder="Sinopsis Laporan" rows="7"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Status Pemulangan</label>
                                <div class="col-8 col-form-label">
                                    <div class="radio-list">
                                        <label class="radio radio-success">
                                            <input type="radio" name="radios18" value="1"/>
                                            <span></span>
                                            Pemulangan Deposit Penuh
                                        </label>
                                        <label class="radio radio-success">
                                            <input type="radio" name="radios18" checked="checked" value="2"/>
                                            <span></span>
                                            Pemulangan Deposit Dengan Kerosakan
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Jumlah Pulangan Deposit(RM)<span class="text-danger">*</span> :</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="depo_amount" placeholder="Jumlah Pulangan Deposit(RM)"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Lampiran :</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <input name="lampiran" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="">
                                        <!-- {{-- <label for="fileUpload1" class="btn btn-primary btn-file-upload" >Muat Naik</label> --}} -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <a href="{{ url('/') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                                <button class="btn btn-primary font-weight-bold">Simpan</button>
                            </div>
                        </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <form action="{{ url('/deposit/kemaskini/pengguna', Crypt::encrypt($data['id'])) }}" method="post">
                        @csrf
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Kemaskini Maklumat Pengguna</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="border: none; background: transparent;">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Nama Pelanggan :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Pelanggan" value="{{ $data['deposit_']->name }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">No Kad Pengenalan/No Rujukan :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="ref_id" placeholder="No Kad Pengenalan/No Rujukan" value="{{ $data['deposit_']->ref_id }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Email :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $data['deposit_']->email }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">No Telefon :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="mobile_no" placeholder="No Telefon" value="{{ $data['deposit_']->mobile_no }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">No Akaun :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="bank_account" placeholder="No Akaun" value="{{ $data['deposit_']->bank_account }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Nama Bank :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="bank_name" placeholder="Nama Bank" value="{{ $data['deposit_']->bank_name }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Lampiran :</label>
                                <div class="col-6">
                                    <div class="input-group">
                                        <input name="lampiran" type="file" class="form-control" id="fileUpload1" accept=".pdf" style="">
                                        <!-- {{-- <label for="fileUpload1" class="btn btn-primary btn-file-upload" >Muat Naik</label> --}} -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col d-flex justify-content-center">
                                    <div class="form-check">
                                        <input required class="form-check-input" type="checkbox" name="sah" id="sah">
                                        <label class="form-check-label" for="sah">
                                            Saya mengesahkan maklumat yang diberikan adalah benar dan telah disahkan oleh pelanggan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Kemaskini</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('javascript')
    <script>
        const myModal = document.getElementById('staticBackdrop');
        const myInput = document.getElementById('myInput');

        myModal.addEventListener('shown.bs.modal', () => {
            myInput.focus();
        });
    </script>
    <script>
</script>   
@endsection

@section('js_content')
    @include('deposit.js.list')
@endsection
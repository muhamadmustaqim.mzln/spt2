@extends('layouts.master')

@section('container')
<style>
	@media (max-width: 991.98px){
		.header-mobile-fixed .content {
			padding-top: 0;
		}
	}
</style>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h3 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Bayaran</h3>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Breadcrumb-->
            <div class="d-flex ml-auto">
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-muted">Laman Utama</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="" class="text-muted">Maklumat Bayaran</a>
                    </li>
                </ul>
            </div>
            <!--end::Breadcrumb-->
        </div>
    </div>
    <!--end::Subheader-->
	<div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body">
                <div class="row" style="background-color: #242a4c">
                            <span class="text-white m-1 p-2">Maklumat Permohonan</span>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="50%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                        <td>{{ $data['deposit_']->name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="50%" class="text-light" style="background-color: #242a4c">Alamat</th>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <th width="50%" class="text-light" style="background-color: #242a4c">No. Tel (P)</th>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <th width="50%" class="text-light" style="background-color: #242a4c">No. Faks</th>
                                        <td>
                                            -
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="50%" class="text-light" style="background-color: #242a4c">No. Bil</th>
                                        <td>
                                            -
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="60%" class="text-light" style="background-color: #242a4c">No. Kad Pengenalan / Pendaftaran Syarikat</th>
                                        <td>{{ $data['deposit_']->ref_id }}</td>
                                    </tr>
                                    <tr>
                                        <th width="60%" class="text-light" style="background-color: #242a4c">Emel</th>
                                        <td>{{ $data['deposit_']->email }}</td>
                                    </tr>
                                    <tr>
                                        <th width="60%" class="text-light" style="background-color: #242a4c">No. Tel (H/P)</th>
                                        <td>{{ $data['deposit_']->mobile_no }}</td>
                                    </tr>
                                    <tr>
                                        <th width="60%" class="text-light" style="background-color: #242a4c">No. Permohonan</th>
                                        <td>{{ $data['deposit_']->subsystem_booking_no }}</td>
                                    </tr>
                                    <tr>
                                        <th width="60%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                        <td>{{ $data['deposit_']->deposit_amount }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead class="bg-dark text-center text-white">
                                <tr>
                                    <th>Bil</th>
                                    <th>No. Transaksi</th>
                                    <th>Tarikh Transaksi</th>
                                    <th>Jenis Bayaran</th>
                                    <th>Cara Bayaran</th>
                                    <th>Perkara</th>
                                    <th>Jumlah Amaun (RM)</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @php
                                    $i = 1;
                                @endphp
                                <tr>
                                    <td style="width: 5%;">{{ $i++ }}</td>
                                    <td>{{ $data['deposit_']->subsystem_booking_no }}</td>
                                    <td>{{ $data['deposit_']->created_at }}</td>
                                    <td>DEPOSIT</td>
                                    <td>FPX</td>
                                    <td>test</td>
                                    <td>{{ $data['deposit_']->deposit_amount }}</td>
                                </tr>
                            </tbody>
                            <tfoot class="border border-white">
                                <tr class="fw-bold fs-6 bg-dark">
                                    <td colspan="6" class="text-right text-white font-weight-bolder">
                                        Jumlah Amaun Dibayar (RM)
                                    </td>
                                    <td class="text-center text-white font-weight-bolder">
                                        {{ $data['deposit_']->deposit_amount }}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
                <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/eksportdeposit') }}" class="btn btn-warning font-weight-bold">Jana Resit Deposit</a>
                            <button class="btn btn-secondary font-weight-bold">Jana Resit Pendahuluan</button>
                            <button class="btn btn-primary font-weight-bold">Kembali ke Aplikasi</button>
                            <a href="{{ url('/') }}" class="btn btn-outline-dark font-weight-bold">Kembali ke Dashboard Bayaran</a>
                        </div>
                    </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->	
</div>

<!--end::Content-->
@endsection

{{-- @section('js_content')
    @include('deposit.js.list')
@endsection --}}
<!DOCTYPE html>
<html>
    <head>
        <title>Resit Online</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>

        @page {
                margin-top: 25px;
            }
            body{
                font-family: arial, sans-serif;
            }

            .kosong{
                margin-bottom: 0;
                font-size: 9px;
            }

            .standard{
                font-size: 11px;
                margin-bottom: 0;
            }
            .tajuk{
                font-size: 14px;
            }

            .table td, .table th{
                border-top: 0;
            }
            .buang{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
                padding-left: 3px !important;
            }

            .buang1{
                padding-top: 3px !important;
                padding-bottom: 3px !important;
            }

            tfoot tr td {
                margin-bottom: 0;
                border-bottom: 0;
                font-size: 10px; 
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <main> 
            <table class="table">
                <tbody>
                    <tr>
                        <td style="text-align: center; width: 10%">
                            <img src="https://elesen.ppj.gov.my/uploads/threef/entree/20190124/Logo_PPj-Tulisan%20Biru-.png" alt="" style="width: 60px; height: 70px">
                        </td>
                        <td style="font-size: small;">
                            <p class="kosong"><b>PERBADANAN PUTRAJAYA</b> </p>
                            <p class="kosong" style="text-transform: uppercase;"><b>KOMPLEKS KEJIRANAN </b> </p>
                            <p class="kosong">62300, WP-PUTRAJAYA</p>
                            <p class="kosong">Tel: </p>
                            <p class="kosong">Fax: </p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <table class="table table-bordered" style="background-color:#dee2e6">
                <tbody>
                    <tr>
                        <th class="tajuk" style="text-align: center">
                            RESIT RASMI DEPOSIT
                        </th>
                    </tr>
                </tbody>
            </table>
            <table class="table">
                <tbody>
                    <tr class="standard">
                        <th class="buang" style="width: 20%">MOD :</th>
                        <td class="buang"></td>
                        <th class="buang" style="width: 20%">Tarikh Transaksi :</th>
                        <td class="buang"></td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">BANK :</th>
                        <td class="buang"></td>
                        <th class="buang">No Transaksi </th>
                        <td class="buang"></td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">Nama Pelanggan :</th>
                        <td class="buang"></td>
                        <th class="buang">No K/P Pelanggan :</th>
                        <td class="buang"></td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">No. Telefon :</th>
                        <td class="buang"></td>
                        <th class="buang">Jumlah Bayaran :</th>
                        <td class="buang">RM </td>
                    </tr>
                    <tr class="standard">
                        <th class="buang">No. Tempahan :</th>
                        <td class="buang"></td>
                    </tr>
                </tbody>
            </table>
            <p class="standard">KETERANGAN: </p>
            <p class="standard">BAYARAN PENUH BAGI PENGGUNAAN DEWAN / FASILITI KOMPLEKS KEJIRANAN </p>
            <table class="table table-bordered" style="font-size: 6px; padding: 0px; margin: 0px">
                <thead>
                    <tr class="standard">
                        <th class="text-center">Bil</th>
                        <th class="text-center">Nama Fasiliti</th>
                        <th class="text-center">Tarikh Penggunaan</th>
                        <th class="text-center">Slot Masa</th>
                        <th class="text-center">Harga (RM) <br><span class="text-primary">*selepas diskaun</span></th>
                        <th class="text-center">Jumlah (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                        $total = 0.00;
                    @endphp
                    <td>
                        test
                    </td>
                </tbody>
            </table>
            <div style="font-size: 10px; margin-bottom: 15px">* Selepas diskaun </div>
            <table class="table table-bordered" style="float: right">
                <tbody>
                    <tr class="standard">
                        <th colspan="2">Rumusan Harga</th>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Jumlah Keseluruhan (RM)</th>
                        <td class="buang1" style="text-align: right"></td>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Pengenapan (RM)</th>
                        <td class="buang1" style="text-align: right"></td>
                    </tr>
                    <tr class="standard">
                        <th class="buang1">Jumlah Keseluruhan Telah Dibayar (RM)</th>
                        <td class="buang1" style="text-align: right"></td>
                    </tr>
                </tbody>
            </table>
            <p class="kosong">NOTA : </p>
            <ol class="kosong">
                <li>Kegagalan membayar jumlah tempahan dalam tempoh empat belas (14) hari sebelum tarikh penggunaan akan menyebabkan tempahan anda dibatalkan.</li>
                <li>Resit Deposit/ Cagaran hendaklah disimpan dengan selamat dan dikemukakan semula apabila membuat tuntutan balik deposit/ cagaran.
                </li>
            </ol>
            <table id="footer" width="100%" border="0" cellpadding="10">
                <tr>
                    <td align="center" style="text-align:center"></td>
                    <td align="center" style="text-align:center; font-size: 10px">Resit ini cetakan komputer, tiada tandatangan diperlukan.</td>
                </tr>
            </table>
        </main>
    </body>
</html>
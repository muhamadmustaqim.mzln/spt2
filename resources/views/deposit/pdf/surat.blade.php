
<style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:10px;
    }
     #footer{
      font-size:8px;
    }
    #back{

       width:100%;
       height:99%;
       background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
       background-repeat: no-repeat;
       background-position: center;
       /*background-attachment:fixed;*/
    }

    /* Force all children to have a specified font-size */
</style>

<div id="back">
  <table  width="100%" border="0" cellpadding="0">
    <tr >
      <th align="right" id="footer">BPSK-1/2015</th>
    </tr>
    <tr>
      <th align="left" width="80%" rowspan="5" scope="col"><img src="{{public_path('assets/media/logos/logo_PPj.png')}}" width="330" height="127" /></th>
    </tr>

      </table>
      <table id="title" width="100%" border="0" cellpadding="0">
        <tr>
          <td><center><strong>BORANG PERMOHONAN TEMPAHAN PERKHIDMATAN/PROGRAM/AKTIVITI/MAJLIS/SEWAAN DAN LAIN-LAIN</strong></center></td>
        </tr>
          <tr>
          <td><center>(Tempahan melalui: Surat / Telefon / Faks / E-Mel / Kaunter)</center></td>
        </tr>
      </table>
      <table id="title" width="100%" border="1" cellpadding="5" cellspacing="0">
                      <tr>
                        <td align="center">
                          <strong>A. BUTIRAN PEMOHON</strong>
                        </td>
                      </tr>
      </table>
      <br>
      <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
          <tr>
              <td width="15%"><b>No Tempahan</b></td>
              <td>:&nbsp;&nbsp;{{$main->bmb_booking_no}}</td>
              {{-- <td class="text-left">&nbsp;{{$main->bmb_booking_no}}</td> --}}
          </tr>
          <tr>
              <td width="15%"><b>Nama Pemohon</b></td>
              <td>:&nbsp;&nbsp;{{$user_detail->bud_name}}</td>
              {{-- <td>&nbsp;{{$user_detail->bud_name}}</td> --}}
          </tr>
          <tr>
              <td width="15%"><b>No.Kad Pengenalan / No. Pendaftaran Syarikat</b></td>
              <td>:&nbsp;&nbsp;{{$user_detail->bud_reference_id}}</td>
              {{-- <td>&nbsp;{{$user_detail->bud_reference_id}}</td> --}}
          </tr>
          <tr>
              <td width="15%"><b>Alamat Pemohon</b></td> <!--try commit-->
              {{-- <td>:</td> --}}
              <td>:&nbsp;&nbsp;{{$user_detail->bud_address}},<br>
              &nbsp;&nbsp;&nbsp;{{$user_detail->bud_town}},
              {{$user_detail->bud_poscode}},
              {{-- {{$user_detail->lkp_state->ls_description}},
              {{$user_detail->lkp_country->lc_description}} --}}
              </td>
          </tr>
          <tr>
              <td width="15%"><b>Jenis Tempahan</b></td>
              {{-- <td>:</td> --}}
              @if($main->fk_lkp_discount_type =='')
              <td>:&nbsp;&nbsp;</td>
              @else
              <td>:&nbsp;&nbsp;{{Helper::getJenisTempahan($main->fk_lkp_discount_type)}}</td>
              {{-- <td>&nbsp;{{$main->fk_lkp_discount_type->ldt_user_cat}}</td> --}}
              @endif
          </tr>
          <tr>
              <td width="15%"><b>Email</b></td>
              {{-- <td>:</td> --}}
              <td>:&nbsp;&nbsp;{{$user->email}}</td>
          </tr>
          <tr>
              <td width="15%"><b>No. Telefon (P)</b></td>
              {{-- <td>:</td> --}}
              <td>:&nbsp;&nbsp;{{$user_detail->bud_office_no}}</td>
          </tr>
          <tr>
              <td width="15%"><b>No. Telefon (H/P)</b></td>
              {{-- <td>:</td> --}}
              <td>:&nbsp;&nbsp;{{$user_detail->bud_phone_no}}</td>
          </tr>
          <tr>
              <td width="15%"><b>No. Faks</b></td>
              {{-- <td>:</td> --}}
              <td>:&nbsp;&nbsp;{{$user_detail->bud_fax_no}}</td>
          </tr>
      </table>
              <br>
      <table id="title" width="100%" border="1" cellpadding="5" cellspacing="0">
          <tr>
            <td align="center">
            <strong>B. SENARAI PERKHIDMATAN / TAPAK / PERALATAN / KELENGKAPAN YANG DIPERLUKAN</strong>
            </td>
          </tr>
      </table>
      <br>
      <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
           <thead>
                <th width="1%" align="center"><b>Bil.</b></th>
                <th width="17%"align="center"><b>Item</b></th>
                <th width="20%" align="center"><b>Tarikh Mula</b></th>
                <th width="20%" align="center"><b>Tarikh Tamat</b></th>
                <th width="20%" align="center"><b>Tujuan Tempahan</b></th>
            </thead>
              <?php
                      $i=1;

                      ?>

           @forelse($bh_booking as $value)
           <tr>
            <td width="" align="center"><?php echo $i ?></a></td>
             <td width="" align="center">{{ Helper::getHall($value->fk_bh_hall) }}</a></td>
             <td width="" align="center">{{ Helper::date_format($value->bb_start_date) }}</a></td>
             <td width="" align="center">{{ Helper::date_format($value->bb_end_date) }}</a></td>
             @if($booking_detail[$i-1]->fk_lkp_event =='')
             <td width="" align="left"></a></td>
             @else
             <td width="" align="left"></a></td>
             {{-- <td width="50px" align="left">{{ $value->bhbookingdetail->lkpevent->le_description }}</a></td> --}}
             @endif

           </tr>
               <?php $i++;?>
             @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
                 @endforelse
   </table>
   <br>
    <div id="title"><b>&nbsp;KADAR SEWAAN DEWAN</b></div>
    <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
        <thead>
            <tr style="page-break-inside: avoid">
            <th width="4%" align="center"><b>Bil.</b></th>
            <th width="17%"align="center"><b>Nama Dewan</b></th>
            <th width="12%" align="center"><b>Tarikh Mula</b></th>
            <th width="12%" align="center"><b>Tarikh Tamat</b></th>
            <th width="12%" align="center"><b>Masa Mula</b></th>
            <th width="12%" align="center"><b>Masa Tamat</b></th>
            <th width="13%" align="center"><b>Kuantiti</b></th>
            <th width="22%" align="center"><b>Harga (RM)</b></th>
            <th width="22%" align="center"><b>Kod GST</b></th>
            <!--  <th class="th-sortable active"><b>{{ trans('asset::asset.daftarasset.tindakan') }}</b></th> -->
        </thead>
          <?php
            $i=1;
            $sumselepas=0;
          ?>
          @forelse($bh_booking as $value)
            <tr style="page-break-inside:avoid;">
                <td width="" align="center"><?php echo $i ?></a></td>
                @if($value->fk_bh_hall=='')
                <td width=""></td>
                @else
                <td width="">{{ Helper::getHall($value->fk_bh_hall) }}</td>
                @endif
                <td width="" align="center"> {{ Helper::date_format($value->bb_start_date) }}</td>
                <td width="" align="center"> {{ Helper::date_format($value->bb_end_date) }}</td>
                <td width="" align="center">{{ $booking_detail[$i-1]->bbd_start_time }}</td>
                <td width="" align="center">{{ $booking_detail[$i-1]->bbd_end_time }}</td>
                <td width="" align="center">{{ $kelengkapan[$i-1]->bbe_quantity }}</td>
                <td width="" align="center">{{ $value->bb_subtotal }}</td>
                <td width="" align="center">{{ Helper::tempahanSportGST($value->fk_lkp_gst_rate) }}</td>

                {{-- <?php 
                $totalselepas=$online->total_amount-0;
                ?>
                <td width="" align="right"><?php //echo number_format($totalselepas,2);?></td> --}}
                {{-- @if($value->lkpGstRate=='') --}}
                {{-- @else --}}
                {{-- <td width="200px" align="center">{{ $value->lkpGstRate->lgr_gst_code }}</td> --}}
              {{-- @endif --}}
          </tr>
          <?php
          $i++;
          // $sumselepas+= $totalselepas;
          ?>
         @empty
            <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
          @endforelse
    </table>
            {{-- @if($diskaun->fk_lkp_discount_type !=1)
             <div id="footer">* Selepas diskaun {{$diskaun->lkpdiskauntype->ldt_discount_rate}} %</div>
            @else
             <div>&nbsp;</div>
             @endif --}}
             
      <br>
      <?php 
        //  if($gsteqpSR->gst==''){
        //       $gsteqpSR=0.00;
        //  }else{
        //       $gsteqpSR=$gsteqpSR->gst;
        //  }
            //  if($gsteqpSRI->gst==''){
        //       $gsteqpSRI=0.00;
        //  }else{
            //      $gsteqpSRI=$gsteqpSRI->gst;
        //  }
        $gstSR = 0;
        $gstSRI = 0;
        ?>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
              {{-- <tr style="page-break-inside:avoid;">
                <th width="50%" scope="col">{{ trans('sales::sales.bayarandetails.gst') }} </th>
                <th width="36%" scope="col" align="right">{{$totalsumSRdewan ?? ''}}</th>
                <th width="20%" scope="col" align="center">E</th>
              </tr>
                <tr style="page-break-inside:avoid;">
                <th width="50%" scope="col">{{ trans('sales::sales.bayarandetails.gst') }} </th>
                <th width="36%" scope="col" align="right">{{$totalsumSRIdewan}}</th>
                <th width="20%" scope="col" align="center">I</th>

              </tr> --}}
              <tr>
                <th scope="row">Jumlah Sewaan Dewan</th>
                <td align="right"><b><?php echo number_format($sumselepas,2);?></b></td>
                <td>&nbsp;</td>
                {{-- <th scope="row">{{trans('sales::sales.resitpdf.jumdewan')}}</th> --}}
              </tr>
                 <tr>
                <th scope="row">Jumlah Sewaan Dewan + GST (0%)(RM)</th>
                <td align="right"><b><?php echo number_format($sumselepas+$gstSR+$gstSRI,2);?></b></td>
                <td>&nbsp;</td>
              </tr>
               <tr>
                @if($main->fk_lkp_status==9)
                <th scope="row">Jumlah Deposit telah dibayar (RM)</th>
                  @else
                      <th scope="row">Jumlah Deposit (RM)</th>
                  @endif
                <td align="right"><b><?php echo number_format($main->bmb_deposit_rounding,2);?></b></td>
                <td>&nbsp;</td>
              </tr>
        </table>
        <br>
        <br>
        <br>
        <br>
        <br>
    {{-- @if($data['checkfacility']==1) --}}
        <div style="page-break-after:always;">&nbsp;</div>
            <div id="title"><b>&nbsp;KADAR SEWAAN PERALATAN</b></div>
            <br>
            <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
             <tr style="page-break-inside: avoid">
             <th width="4%" align="center"><b>Bil.</b></th>
             <th width="17%" align="center"><b>Nama Dewan</b></th>
             <th width="12%" align="center"><b>Item</b></th>
             <th width="12%" align="center"><b>Tarikh</b></th>
             <th width="8%" align="center"><b>Masa Mula</b></th>
             <th width="8%" align="center"><b>Masa Mula</b></th>
             <th width="8%" align="center"><b>Kos Seunit (RM)</b></th>
             <th style="text-align:center" width="5%"><b>Jumlah Unit</b></th>
             <th style="text-align:center" width="5%"><b>Kod GST</b></th>
             <th style="text-align:center" width="8%"><b>Jumlah sewaan (RM)</b></th>

             {{-- <th width="17%" align="center"><b>{{ trans('sales::sales.bayarandetails.dewan') }}</b></th>
             <th width="17%" align="center"><b>{{ trans('sales::sales.bayarandetails.item') }}</b></th>
             <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.tarikh') }}</b></th>
             <th width="10%" align="center"><b>{{ trans('sales::sales.bayarandetails.masamula') }}</b></th>
             <th width="10%" align="center"><b>{{ trans('sales::sales.bayarandetails.masatamat') }}</b></th>             
             <th width="13%" align="center"><b>Kos Seunit (RM)</b></th>
             <th width="13%" align="center"><b>{{ trans('sales::sales.bayarandetails.unit') }}</b></th>
             <th width="10%" align="right"><b>{{ trans('sales::sales.bayarandetails.harga') }}</b></th>
             <th width="22%" align="center"><b>{{ trans('sales::sales.bayarandetails.kodgst') }}</b></th> --}}

              </tr>
               <?php
                      $i=1;
                      $sumselepaseqp=0;
                      $totalsumeqp=0;
                      $gsteqpSR = 0;
                      $gsteqpSRI = 0;
                      ?>
               @forelse($datatambahaneqp as $key => $value)
                      <tr style="page-break-inside:avoid;">
                      <td width=""><?php echo $i ?></a></td>
                      <td width="">{{ Helper::getHall(Helper::getBhBooking($value->fk_bh_booking)->fk_bh_hall) }}</td>
                      <td width="">{{ Helper::get_equipment_hall($value->fk_bh_equipment) }}</td>
                      <td width="" align="center">{{ ($value->bbe_booking_date) }}</td>
                      <td width="" align="center"></td>
                      <td width="" align="center"></td>
                      <td width="" align="center">{{ $value->bbe_total }}</td>
                      <td width="" align="center">{{ $value->bbe_quantity }}</td>
                      <?php
                      // if($value->fk_lkp_gst_rate==1){
                      //   $totalselepaseqp=$value->total_amount-$value->gst_amount;

                      // }else{
                      //   $totalselepaseqp=$value->total_amount;
                      // }

                      // if($totalselepaseqp==''){
                      //   $totalselepaseqp=0.00;
                      // }else{
                      //   $totalselepaseqp=$totalselepaseqp;
                      // }
                      ?>
                      {{-- <td width="100px" align="right"><?php //echo number_format($totalselepaseqp,2);?></td> --}}
                      <td width="" align="center">{{ Helper::tempahanSportGST($value->fk_lkp_gst_rate) }}</td>
                      <td width="" align="right">{{ $value->bbe_subtotal }}</td>
                       {{-- @if($value->lkpGstRate=='')
                       <td width="200px"></td>
                      @else
                       <td width="200px" align="center">{{ $value->lkpGstRate->lgr_gst_code }}</td>
                      @endif --}}

                      </tr>

                      <?php $i++;?>
                      <?php 
                          $sumselepaseqp+= $value->bbe_total; 
                      ?>
                 @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
                @endforelse
        
        {{-- @endif --}}
        </table>
        <br>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
          <tr style="page-break-inside:avoid;">
            <th align="left" width="50%"><b>GST (0%) </b></th>
            <th width="36%" align="right">{{$gsteqpSR}}</th>
            <th width="20%" align="center"><b>E</b></th>
          </tr>
          <tr style="page-break-inside:avoid;">
                <th align="left" width="50%"><b>GST (0%) </b></th>
                <th width="36%" align="right">{{$gsteqpSRI}}</th>
                <th width="20%" align="center"><b>I</b></th>
          </tr>
          <tr style="page-break-inside:avoid;">
            <th align="left"scope="row">Jumlah Sewaan Peralatan (RM)</th>
            <td align="right"><b><?php echo number_format(($sumselepaseqp),2)?></b></td>
            <td>&nbsp;</td>
          </tr>
          <tr style="page-break-inside:avoid;">
            <th align="left" scope="row">Jumlah Sewaan Peralatan + GST (0%)(RM)</th>
                <td align="right"><b><?php echo number_format($sumselepaseqp+$gsteqpSR+$gsteqpSRI,2);?></b></td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <br>
        <table id="title" width="100%" border="0" align="right"  cellpadding="2" cellspacing="0">
            <tr style="page-break-inside: avoid">
            <td>
            </td>
            </tr>
        </table>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
          <tr style="page-break-inside: avoid">
            <th width="50%" align="left"><b>Jumlah Keseluruhan ( RM )</b></th>
            <th width="36%" align="right">{{ number_format($main->bmb_subtotal,2) }}</th>
            <td width="20%"></td>
        </tr>
            <tr style="page-break-inside:avoid;">
                      <td width="100px"><b>Pengenapan</b></td>
                      <?php $pengenapan=($main->bmb_rounding)?>
                      <td width="100px" align="right"><b><?php echo number_format($pengenapan,2)?></b></td>
                      <td width="100px"></td>
            </tr>

            @if($main->fk_lkp_status==9)
                    <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Telah Dibayar(RM)</b></td>
                        <td width="36%" align="right"><b><?php echo number_format($amount_paid,2)?></b></td>
                        <td width="20%"></td>
                        </tr>
                        <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                        <?php $jumbayar=($main->bmb_subtotal-$main->bmb_rounding)?>
                        <td width="36%" align="right"><b><?php echo number_format($bayaranpenuh,2)?></b></td>
                        <td width="20%"></td>
                    </tr>
                @else
                    <tr style="page-break-inside:avoid;">
                        <td width="50%"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                        <?php $jumbayar=($main->bmb_subtotal-$main->bmb_rounding)?>
                        <td width="36%" align="right"><b><?php echo number_format($jumbayar,2)?></b></td>
                        <td width="20%"></td>
                    </tr>
                @endif
      </table>


      <table align="right" width="100%">
        <tr><td>&nbsp;</td></tr>
      </table>
      <div style='page-break-after:always;'>&nbsp;</div>
              <table id="title" width="100%" border="1" cellpadding="10" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>C. PERAKUAN TEMPAHAN</strong>
                  </td>
                </tr>
              </table>

              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                  <td colspan="2">Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun yang dikenakan dan akan <b>dijelaskan tidak lewat daripada 14 hari sebelum </b> perkhidmatan / program / aktiviti / majis / Acara / Sewaan yang ditempah berlangsung<br><br>Tandatangan :</tr>
                <tr><td align="left">Nama & Jawatan Pemohon :<br>Tarikh :</td>
              </table>
              <br>

          <table id="title" width="100%" border="1" cellpadding="10" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>D. KEGUNAAN PEJABAT PERBADANAN PUTRAJAYA</strong>
                  </td>
                </tr>
              </table>

              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                <td colspan="2"><b>Diisi oleh Jabatan Pelaksana</b></td>
                <tr><td align="left">Nama Pegawai Melulus:<br>Bahagian/ Jabatan :<br>Tandatangan :<br>Tarikh :</td>
              </table>

              <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <br>
                <tr>
                <td colspan="2"><b>Diisi oleh Jabatan Kewangan</b></td>
                <tr><td align="left">No. BP :<br>No. Kontrak :<br>No. Bil :<br>Tandatangan :<br>Tarikh :</td>
              </table>


</div>


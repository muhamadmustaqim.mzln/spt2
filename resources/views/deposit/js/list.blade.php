<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

</body>
<script>
    // dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
    //     <'row'<'col-sm-12'tr>>
    //     <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    $(document).ready(function() {
        var table = $('#table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                // {
                //     extend: 'pdf',
                //     exportOptions: {
                //     columns: ':visible'
                //     },
                //     customize: function(doc) {
                //         doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                //         //doc.defaultStyle.alignment = 'center';
                //         //doc.styles.tableHeader.alignment = 'center';
                //     },
                // },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
    });
</script>
<script>
    $(document).ready(function() {
        $('#btnSearch').on('click', function() {
            var query = $('#searchInput').val().toLowerCase();
            if (query.trim() === '') {
                location.reload(); 
            } else {
                searchTable(query);
            }
        });

        function searchTable(query) {
            $.ajax({
                url: '/admin/user/search', 
                method: 'GET',
                data: { query: query },
                success: function(response) {
                    updateTable(response);
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        }

        function updateTable(data) {
            console.log(data)
            if (!Array.isArray(data)) {
                console.error('Invalid data format:', typeof(data));
                return;
            }
            $('#kt_datatable_2 tbody').empty();

            data.forEach(function(record, index) {
                let newRowId = `${index+1}`;
                $('#kt_datatable_2 tbody').append(`<tr>
                    <td>${newRowId}</td>
                    <td>${record.email}</td>
                    <td>${record.fullname}</td>
                    <td>${record.fullname}</td>
                    <td>${record.isAdmin === 1 ? 'Administrator' : 'User'}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-outline-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Tindakan
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item text-primary" href="/admin/user/edit/${record.id}">Kemaskini</a>
                              <a class="dropdown-item text-danger btn-delete" href="/admin/user/delete/${record.id}">Padam</a>
                            </div>
                          </div>
                    </td>
                </tr>`);
            });
        }
    });
    $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_daterangepicker_5').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), 
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        }
    });

    $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    
    $('#kt_datepicker').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
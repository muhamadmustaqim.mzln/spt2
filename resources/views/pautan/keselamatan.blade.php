@extends('layouts.master')

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Dasar Keselamatan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Dasar Keselamatan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body">
                    <p class="font-size-h6" style="text-align: justify;">
                        <b>Perlindungan Data</b><br><br>
                        Teknologi-teknologi terkini termasuk perisian penyulitan digunakan untuk melindungi sebarang data yang diberikan kepada kami dan piawaian keselamatan ketat dikekalkan untuk menghalang akses yang tidak dibenarkan. 
                        <br><br> 
                        <b>Keselamatan Penyimpanan</b><br><br>
                        Bagi melindungi data peribadi anda, semua simpanan elektronik dan penghantaran data peribadi adalah selamat dan disimpan dengan teknologi-teknologi sekuriti yang bersesuaian.
                        <br><br>
                    </p>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection
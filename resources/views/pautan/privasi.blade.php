@extends('layouts.master')

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Dasar Privasi</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Dasar Privasi</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body">
                    <p class="font-size-h6" style="text-align: justify;">
                        <b>Kenyataan Privasi </b><br><br>
                        Perbadanan Putrajaya komited untuk melindungi maklumat privasi pengguna Portal Rasmi kami. Laman Perbadanan Putrajaya juga cuba untuk menyediakan satu persekitaran yang selamat dan terjamin. Kenyataan Privasi ini menerangkan pengumpulan data dalam talian dan dasar penggunaan dan amalan-amalan yang digunakan terhadap Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>. Dengan menggunakan atau mendaftar sebagai seorang pengguna Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>, anda mengesahkan yang anda telah membaca dan memahami Kenyataan Privasi Portal Rasmi Perbadanan Putrajaya, dan anda bersetuju untuk mematuhi dasar dan amalan yang diterangkan dalam Kenyataan ini.  
                        <br><br> 
                        Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> mungkin mempunyai pautan kepada tapak-tapak Web lain yang tiada dalam kawalan Perbadanan Putrajaya. Perbadanan Putrajaya tidak bertanggung jawab terhadap dasar-dasar privasi atau amalan-amalan laman Web lain yang mungkin dipautkan daripada Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>. Kenyataan Privasi ini hanya digunakan semata-mata untuk maklumat yang dikumpulkan oleh Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>. 
                        <br><br>
                        Data anda akan disimpan dan diproses di Malaysia. Jika anda mengakses <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> daripada mana-mana negara lain, penggunaan anda dalam tapak ini memberi persetujuan untuk pemindahan data daripada negara luar dan ke dalam Malaysia. 
                        <br><br>
                        <b>Pengumpulan Dan Penyimpanan Maklumat </b><br><br>
                        Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> meminta anda menyediakan maklumat peribadi, termasuk nama, alamat, alamat e-mel, nombor telefon, maklumat hubungan, maklumat bil termasuk nombor kad kredit dan maklumat lain. Di bahagian lain, <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> mungkin mengumpul maklumat demografi yang bukan unik untuk anda seperti poskod anda, umur, kesukaan dan jantina. Kami mungkin juga mengumpul maklumat tertentu tentang penggunaan laman kami oleh anda, seperti bahagian-bahagian apa anda lawati dan perkhidmatan-perkhidmatan apa yang anda akses. Selain itu, terdapat maklumat tentang perkakasan dan perisian komputer anda iaitu atau mungkin diambil oleh Portal Rasmi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>. Maklumat ini mungkin termasuk alamat IP anda, jenis pelayar, nama-nama domain, waktu-waktu akses dan alamat-alamat tapak Web yang dirujuk. 
                        <br><br>
                        Sila ingat bahawa jika anda menghantar mana-mana maklumat peribadi anda dalam kawasan awam, seperti di papan mesej atau forum-forum dalam talian, maklumat seperti itu boleh diambil dan digunakan oleh orang lain yang mana <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> tidak mempunyai kawalan. <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> adalah tidak bertanggung jawab untuk penggunaan yang dibuat oleh maklumat pihak ketiga yang anda hantar atau sebaliknya dibuat dan didapati di kawasan awam.
                        <br><br>
                        Kami mungkin menyimpan maklumat yang kami kumpul mengenai anda dalam satu usaha bagi menjadikan lawatan ulangan anda kepada kami lebih efisien, praktikal, dan relevan. Anda boleh membetulkan atau mengemaskinikan profil akaun pada bila-bila masa. Tambahan pula,, anda boleh memadamkan maklumat peribadi daripada pangkalan data kami atau menutup akaun <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> anda pada  bila-bila masa, di mana kami akan memindahkan semua salinan-salinan profil akaun maklumat anda kecuali untuk satu arkib salinan yang tidak boleh dicapai melalui Internet. 
                        <br><br>
                        Perbadanan Putrajaya menggunakan maklumat perhubungan anda bagi menghantar e-mel atau komunikasi lain mengenai perniagaan yang anda jalankan di laman kami. Anda mempunyai pilihan bagi memilih untuk menerima maklumat tambahan seperti maklumat surat berita percuma daripada Perbadanan Putrajaya berkaitan topik-topik yang mungkin anda minati, seperti jadual mesyuarat awam atau acara-acara lain.
                        <br><br>
                        <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> mempunyai satu bahagian di mana anda boleh mengemukakan maklum balas. Mana-mana maklum balas yang anda serahkan di bahagian ini menjadi hakmilik kami, dan kami akan menggunakan maklum balas itu(seperti maklum balas positif atau negatif) untuk meningkatkan perkhidmatan-perkhidmatan kami untuk anda. Kami juga akan menghubungi anda untuk keterangan lanjut. 
                        <br><br>
                        <b>Pendedahan Maklumat </b><br><br>
                        Perbadanan Putrajaya tidak akan mendedahkan kepada pihak ketiga maklumat peribadi dan / atau demografi anda kecuali seperti yang dinyatakan dalam dua subperenggan berikut.
                        <br><br>
                        <ol class="font-size-h6" style="text-align: justify;">
                            <li>
                                <div class="ml-7">
                                    Bagi membantu tujuan-tujuan yang dinyatakan di atas dan penyediaan produk dan perkhidmatan kepada anda, kami mungkin akan mendedahkan Maklumat Peribadi anda kepada pihak ketiga mengikut pengkelasan seperti berikut :
                                    <ul>
                                        <li>Gabungan syarikat-syarikat kami dan anak syarikat kami di dalam atau luar Malaysia;</li>
                                        <li>Sebarang institusi kewangan dan syarikat-syarikat yang mengeluarkan kad caj atau kad kredit.</li>
                                        <li>Penasihat, juruaudit, perunding, kontraktor, pembekal produk dan perkhidmatan kami sekadar makluman yang diperlukan sahaja;</li>
                                        <li>Pihak berkuasakawal selia, agensi-agensi penguatkuasaan dan mana-mana pihak yang berkaitan dengan prosiding undang-undang;</li>
                                        <li>Mana-mana pemegang serah hak atau bakal penerima pindah milik atau pemeroleh syarikat atau perniagaan kami atau berkenaan dengan perlaksanaan korporat. </li>
                                    </ul>
                                <br>
                                </div>
                            </li>
                            <li>
                                <div class="ml-7">
                                    Perbadanan Putrajaya mungkin mendedahkan maklumat seperti itu kepada syarikat-syarikat dan individu-individu yang bekerja dengan kami untuk menjalankan tugas sebagai wakil kami. Contohnya termasuk menjadi hos pelayan Web kami, menganalisis data, memproses bayaran kad kredit, dan memberi perkhidmatan kepada pelanggan. Syarikat-syarikat ini dan individu-individu akan mempunyai akses kepada maklumat peribadi anda sebagai keperluan bagi melaksanakan tugas mereka, tetapi mereka tidak boleh berkongsi maklumat tersebut dengan mana-mana pihak ketiga lain.
                                <br>
                                </div>
                                <br>
                            </li>
                            <li>
                                <div class="ml-7">
                                    Perbadanan Putrajaya mungkin mendedahkan maklumat seperti itu jika dikehendaki oleh undang-undang dan, jika diminta berbuat demikian oleh satu entiti kerajaan atau jika kita meyakini dengan niat baik tindakan seumpama itu adalah perlu untuk:
                                    <ul>
                                        <li>mematuhi keperluan-keperluan undang-undang atau mematuhi proses undang-undang;</li>
                                        <li>melindungi hak atau harta Perbadanan Putrajaya;</li>
                                        <li>menghalang satu jenayah atau mempertahankan keselamatan negara; atau</li>
                                        <li>melindungi keselamatan diri pengguna-pengguna atau orang awam.</li>
                                    </ul>
                                </div>
                            </li>
                        </ol>
                        <br>
                        <b class="font-size-h6" style="text-align: justify;">Akses, pembetulan atau menghadkan proses-proses Maklumat Peribadi Anda. </b><br><br>
                        <ol class="font-size-h6" style="text-align: justify;">
                            <li>
                                <div class="ml-7">
                                    Pada bila-bila masa anda boleh mengemukakan permintaan untuk akses, membuat pembetulan, mengemaskini atau menghadkan proses-proses maklumat peribadi anda dengan memaklumkan kepada kami secara bertulis kepada butir-butir hubungan seperti yang tertera di bawah :
                                    <ul style="list-style: none">
                                    <br>
                                        <li>Perbadanan Putrajaya,</li>
                                        <li>Kompleks Perbadanan Putrajaya,</li>
                                        <li>24, Persiaran Perdana, Presint 3,</li>
                                        <li>62675 Putrajaya.</li>
                                        <li>Emel : ppjonline@ppj.gov.my</li>
                                        <li>Telefon : 03-8000 8000</li>
                                        <li>Faksimili : 03-88875000</li>
                                    </ul>
                                <br>
                                </div>
                            </li>
                            <li>
                                <div class="ml-7">
                                Sekiranya anda tidak bersetuju dengan kami untuk memproses Maklumat Peribadi anda seperti di atas, kami mungkin tidak dapat :
                                    <ul>
                                        <li>Memproses permohonan anda;</li>
                                        <li>Memberikan maklumat, maklumat terkini, produk-produk atau perkhidmatan yang diminta oleh anda yang berkenaan dengan produk dan perkhidmatan kami; atau</li>
                                        <li>Memproses atau melaksanakan transaksi perniagaan yang releven.</li>
                                    </ul>
                                </div>
                                <br>
                            </li>
                        </ol>
                    </p>
                    <p class="font-size-h6" style="text-align: justify;">
                        <b>Notis  </b><br><br>
                        Jika kami tidak menerima sebarang maklumbalas anda sebelum 31 Julai 2014 atau anda masih berurusan dengan kami selepas penerimaan Notis Privasi ini, anda dianggap telah bersetuju dengan syarat-syarat di atas yang telah dinyatakan di dalam Notis Privasi ini.
                        <br><br>
                        <b>Penggunaan Cookies</b><br><br>
                        <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> akan menggunakan "cookies" untuk memperibadikan dan memaksimumkan pengalaman dalam talian anda. ‘Cookie' adalah satu fail teks yang diletakkan dalam cakera keras anda oleh halaman Web pelayan. ‘Cookie' tidak mengendalikan program-program atau menyampaikan virus-virus kepada komputer anda. ‘Cookie' ditugaskan secara unik kepada komputer anda, dan hanya boleh dibaca oleh satu pelayan Web dalam domain yang mengeluarkan ‘cookie' kepada anda.
                        <br><br>
                        Satu daripada tujuan utama ‘cookie' adalah bagi memberi satu ciri kemudahan untuk anda menjimatkan masa. Tujuan ‘cookie' adalah bagi memberitahu pelayan Web yang anda telah kembali ke satu halaman khusus. Sebagai contoh, jika anda memperibadikan halaman- <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>, atau mendaftar untuk perkhidmatan, ‘cookie' membantu kami untuk mengingat kembali maklumat tertentu anda (seperti nama pengguna, kata laluan dan kesukaan). Disebabkan oleh penggunaan ‘cookie', kita boleh menghantar keputusan dengan cepat dan lebih tepat dan tapak yang lebih diperibadikan. Apabila anda kembali kepada tapak <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>, maklumat yang anda berikan dahulu boleh didapatkan kembali, supaya anda boleh menggunakan ciri yang telah anda pilih. Kami juga menggunakan ‘cookie'  untuk mengesan klik ‘stream' dan untuk keseimbangan beban.
                        <br><br>
                        Anda mempunyai pilihan untuk menerima atau menolak ‘cookie'. Kebanyakan pelayar Web menerima ‘cookie' secara automatik, tetapi biasanya anda boleh mengubah suai persekitaran pelayar anda untuk menolak semua ‘cookie' jika anda suka. Secara alternatif, anda boleh mengubah suai persekitaran pelayar anda untuk memaklumkan setiap kali ‘cookie' diberi dan memberi pilihan kepada anda untuk menerima atau menolak ‘cookie' secara individu. Jika anda memilih untuk menolak ‘cookie', bagaimanapun,ia mungkin menghalang prestasi dan memberi kesan negatif terhadap pengalaman anda di tapak web <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a>
                        <br><br>
                        <b>Keselamatan</b><br><br>
                        <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> telah melaksanakan langkah-langkah teknikal dan organisasi munasabah yang direka untuk melindungi maklumat peribadi anda daripada kerugian tak sengaja dan akses tidak sah, penggunaan, perubahan atau pendedahan. Bagaimanapun, kami tidak boleh menjamin bahawa pihak ketiga yang tidak dibenarkan tidak akan berupaya untuk melepasi langkah-langkah atau menggunakan maklumat peribadi anda untuk tujuan tidak sepatutnya.
                        <br><br>
                        <b>Perubahan Dalam Kenyataan Privasi</b><br><br>
                        Jika kami memutuskan untuk mengubah Kenyataan Privasi kami, kami akan menghantar perubahan tersebut di sini supaya anda akan sentiasa tahu maklumat apa yang kami kumpul, bagaimana kami menggunakan maklumat itu, dan kepada siapa kami akan mendedahkannya. Jika pada bila-bila masa, anda mempunyai soalan-soalan atau kebimbangan tentang Kenyataan Privasi <a href="https://www.ppj.gov.my">https://www.ppj.gov.my</a> , sila hubungi kami.
                        <br><br>
                        <b>Maklumat Hubungan</b><br><br>
                        Kami mengalu-alukan komen anda berkenaan Kenyataan Privasi ini. Jika anda percaya bahawa kami tidak mematuhi Kenyataan ini, sila maklumkan dan kami akan berusaha sebaik mungkin bagi mengenalpasti dan menyelesaikan masalah.
                        <br><br>
                    </p>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection
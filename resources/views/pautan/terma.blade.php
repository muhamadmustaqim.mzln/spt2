@extends('layouts.master')

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Terma dan Syarat</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Terma dan Syarat</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-body">
                    <p class="font-size-h6" style="text-align: justify;">
                        Selamat Datang Ke Portal Perbadanan Putrajaya ("Laman"). Laman ini dicipta oleh Perbadanan Putrajaya, pihak berkuasa tempatan untuk Wilayah Persekutuan Putrajaya, Malaysia. Dengan menggunakan Laman ini, anda bersetuju untuk mematuhi dan terikat dengan terma dan syarat-syarat yang berikut berkenaan penggunaan Laman ("Syarat-syarat Penggunaan") dan Polisi Sulit kami. Kami mungkin menyemak Terma Penggunaan dan Polisi Sulit pada bila-bila masa tanpa sebarang notis kepada anda.
                        <br><br> 
                        Bahagian-bahagian Laman mungkin mempunyai syarat-syarat penggunaan berbeza yang berlainan disiarkan. Jika terdapat konflik antara Terma Penggunaan dan syarat-syarat penggunaan yang disiarkan untuk bahagian tertentu dalam Laman, syarat penggunaan yang terkini merupakan keutamaan terhadap penggunaan anda dalam Laman tersebut.
                        <br><br>
                        <ol class="font-size-h6" style="text-align: justify;">
                            <li>
                                <b class="ml-7">Had Usia</b>
                                <ul>
                                    <li>
                                    Bagi pengguna berumur 18 tahun dan kebawah perlu mendapat kebenaran ibu bapa atau penjaga bagi menggunakan perkhidmatan PPj Online. Perkhidmatan PPj Online melibatkan pembayaran secara online menggunakan kad kredit. Langkah ini diambil bagi menjamin agar tiada penyalahgunaan kad kredit dan ianya adalah milik persendirian.
                                    </li>
                                </ul>
                                <br>
                            </li>
                            <li>
                                <b class="ml-7">Penggunaan Maklumat Portal Rasmi</b>
                                <ul>
                                    <li>
                                    Kandungan hanya digunakan untuk diri sendiri, makluman, tujuan bukan untuk perdagangan; dan
                                    </li>
                                    <li>
                                    Kandungan tidak boleh diubah suai atau dipinda dalam apa-apa cara. Kecuali penggunaan anda menyumbang kepada "penggunaan yang adil" di bawah undang-undang hak cipta, anda sebaliknya tidak boleh menggunakannya, memuat turun, memuat naik, menyalin, mencetak, memapaparkan, menjalankan, menghasilkan semula, menerbitkan, melesenkan, menyiarkan, menghantar atau mengagihkan apa-apa maklumat daripada lamanWeb ini sepenuhnya atau sebahagian tanpa kebenaran Perbadanan Putrajaya.
                                    </li>
                                </ul>
                                <br>
                            </li>
                            <li>
                                <div class="ml-7">
                                <b>Penggunaan Perisian</b> Sebarang penggunaan perisian dan dokumentasi yang anda muat turun daripada Laman adalah tertakluk kepada syarat-syarat perjanjian lesen perisian antara anda dan Perbadanan Putrajaya. Anda mesti membaca perjanjian lesen dan memberikan persetujuan terhadap syarat-syarat untuk memasang atau menggunakan perisian. Semua hak, tajuk dan kepentingan yang tidak dibenarkan adalah dilarang.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Resolusi Pertikaian Alternatif</b> ika ada pertikaian di antara Perbadanan Putrajaya (PPj) dan pengguna, pihak PPj akan merujuk kes tersebut ke Kementerian Wilayah Persekutuan (KWP) melalui Jawatankuasa Pengurusan Aduan PPj memandangkan PPj adalah agensi di bawah Kementerian Wilayah Persekutuan (KWP).
                                </div>
                            </li>
                            <br>
                            <li>
                                <b class="ml-7">Polisi Bayaran Balik</b>
                                <ul>
                                    <li>
                                    Perkhidmatan PPj Online beroperasi atas polisi tiada bayaran balik tunai.
                                    </li>
                                    <li>
                                    Kesemua bayaran tidak akan dibayar balik kecuali dalam keadaan tertentu yang akan ditentukan oleh Pihak Perbadanan Putrajaya (contohnya masalah teknikal, masalah dengan sistem perbankan). Kredit hanya akan dikeluarkan atas budi bicara Perbadanan Putrajaya sepenuhnya. Proses bayaran balik adalah dalam tempoh 14 hari dari tarikh masalah dikenalpasti.
                                    </li>
                                </ul>
                                <br>
                            </li>
                            <li>
                                <div class="ml-7">
                                <b>Penggunaan Forum dan Komunikasi Awam </b> "Forum" bermaksud satu kumpulan perbincangan, ruang berbual, papan buletin, kumpulan berita, komunikasi (surat, emel atau sebarang bentuk komunikasi) kepada Perbadanan Putrajaya, penyelia laman webnya atau pekerjanya, atau fungsi emel yang ditawarkan sebagai sebahagian daripada Laman. Anda bersetuju supaya tidak muat naik, emel, menghantar, menerbitkan atau sebaliknya menghantar melalui Forum sebarang kandungan yang:
                                </div>
                                <ul>
                                    <li>
                                    palsu atau mengelirukan;
                                    </li>
                                    <li>
                                    fitnah;
                                    </li>
                                    <li>
                                    menggangu atau menyerang peribadi orang lain, atau menyebabkan ketaksuban, perkauman, kebencian atau kemarahan mana-mana kumpulan atau individu;
                                    </li>
                                    <li>
                                    lucah;
                                    </li>
                                    <li>
                                    melanggar hak-hak orang lain, termasuk tetapi tidak terhad untuk hak harta intelek;
                                    </li>
                                    <li>
                                    menyumbang emel pukal yang tidak diminta, "mel sarap," "spam" atau surat berantai; atau 
                                    </li>
                                    <li>
                                    melanggar mana-mana undang-undang atau peraturan-peraturan yang berkenaan.<br>  
                                    Kecuali dinyatakan sebaliknya dengan jelas oleh Perbadanan Putrajaya, Forums hanya akan digunakan untuk tujuan bukan perdagangan sahaja. Anda tidak boleh mengedar atau sebaliknya menerbitkan sebarang maklumat yang mengandungi kutipan wang, promosi, pengiklanan, permintaan untuk barangan atau perkhidmatan, atau perkara perdagangan lain. Anda bersetuju supaya tidak mendapatkan pengguna Laman lain untuk menggunakan atau menyertai atau menjadi ahli-ahli mana-mana perkhidmatan perdagangan dalam talian atau luar talian atau organisasi lain. Kecuali dibenarkan dengan jelas oleh Perbadanan Putrajaya, anda bersetuju untuk tidak mengutip atau menyimpan data peribadi tentang pengguna lain. Dengan memuat naik, menghantar emel, menyiarkan, menerbitkan atau sebaliknya menghantar kandungan kepada mana-mana Forum atau menyerahkan sebarang kandungan kepada Perbadanan Putrajaya, anda secara automatik memberi kebenaran (atau waran menyatakan kebenaran hak pemilik) kepada Perbadanan Putrajaya sebuah badan kekal, royalti bebas, muktamad, hak tidak eksklusif dan lesen untuk digunakan, menghasilkan semula, mengubahsuai, menyesuaikan, menerbitkan, menghantar dan mengedarkan kandungan dalam sebarang bentuk, cara, atau teknologi sekarang atau dihasilkan kemudian. Seperkara lagi, anda menjamin semua yang dikenali hak-hak moral dalam kandungan telah diketepikan.
                                    </li>
                                </ul>
                                <br>
                            </li>
                            <li>
                                <div class="ml-7">
                                <b>Kata Laluan Dan Keselamatan</b> Anda bertanggungjawab memelihara maklumat sulit kata laluan anda yang diberi untuk memasuki Laman, dan adalah bertanggungjawab sepenuhnya untuk semua aktiviti yang berlaku di bawah kata laluan anda. Anda bersetuju untuk memberitahu Perbadanan Putrajaya dengan segera mana-mana penggunaan tanpa kebenaran kata laluan anda. Perbadanan Putrajaya mengambil berat tentang keselamatan maklumat peribadi yang kami telah ambil daripada anda dan telah mengambil langkah-langkah munasabah untuk menghalang akses yang tidak sah kepada maklumat itu.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Penamatan Penggunaan</b> Anda bersetuju bahawa Perbadanan Putrajaya mungkin, dengan pertimbangan sendiri, pada bila-bila masa menamatkan akses anda kepada Laman dan mana-mana akaun anda yang mungkin berkaitan dengan Laman. Akses kepada Laman yang diawasi oleh Perbadanan Putrajaya.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Laman-laman Web Pihak Ketiga, Kandungan, Produk Dan Perkhidmatan</b> Laman ini memberi pautan kepada laman Web dan akses untuk isikandungan, produk dan perkhidmatan dari pihak ketiga, termasuk pengguna, pengiklan, sekutu-sekutu dan penaja-penaja Laman. Anda bersetuju bahawa Perbadanan Putrajaya tidak bertanggungjawab untuk ketersediaan, dan kandungan yang disediakan , laman Web pihak ketiga. Anda hendaklah merujuk polisi yang disiarkan oleh laman Web lain mengenai polisi privasi dan topik lain sebelum anda menggunakannya. Anda bersetuju bahawa Perbadanan Putrajaya tidak bertanggungjawab terhadap kandungan pihak ketiga yang boleh diakses melalui Tapak, termasuk pendapat-, nasihat, kenyataan- dan iklan-, dan memahami yang anda menanggung risiko yang berkaitan dengan penggunaan kandungan tersebut. Jika anda memilih untuk membeli mana-mana produk atau perkhidmatan- daripada pihak ketiga, perhubungan anda adalah secara terus dengan pihak ketiga. Anda bersetuju yang Perbadanan Putrajaya tidak bertanggungjawab terhadap: (a) kualiti produk atau perkhidmatan pihak ketiga; dan (b) mematuhi mana-mana syarat-syarat bagi perjanjian anda dengan penjual, termasuk penghantaran produk atau perkhidmatan dan syarat-syarat jaminan yang berkaitan dengan produk atau perkhidmatan yang dibeli. Anda bersetuju bahawa Perbadanan Putrajaya adalah tidak bertanggungjawab terhadap sebarang kehilangan atau kerosakan yang anda mungkin hadapi semasa berurusan dengan mana-mana pihak ketiga.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Penafian</b> Kecuali di mana dengan nyata sebaliknya, Laman, dan semua isi kandungan, bahan-bahan, maklumat, perisian, produk dan perkhidmatan yang disediakan di laman, disediakan atas berdasar "sebagaimana ia" dan "sebagai tersedia" . Perbadanan putrajaya dengan nyata menafikan semua jenis jaminan, sama ada dinyatakan atau tertakluk, termasuk, tetapi tidak terhad untuk, jaminan bagi kebolehniagaan, kesesuaian untuk tujuan tertentu dan bukan pelanggaran. Perbadanan Putrajaya tidak menjamin yang: (A) Laman ini akan memenuhi keperluan anda; (B) Laman ini boleh digunakan tanpa gangguan, pada masa tertentu, selamat, atau bebas daripada kesilapan; (C) keputusan yang mungkin diperolehi daripada penggunaan laman ini atau apa-apa perkhidmatan yang ditawarkan melalui laman ini adalah tepat atau boleh dipercayai; atau (D) kualiti mana-mana produk, perkhidmatan, maklumat, atau bahan lain yang dibeli atau diperolehi oleh anda melalui laman akan memenuhi jangkaan anda. Sebarang kandungan, bahan-bahan, maklumat atau perisian yang dimuat turun atau sebaliknya diperolehi melalui penggunaan laman ini adalah dilakukan dengan budi bicara dan risiko anda sendiri. Perbadanan Putrajaya tidak akan bertanggungjawab terhadap sebarang kerosakan kepada sistem komputer anda atau kehilangan data yang disebabkan daripada muat turun sebarang kandungan, bahan-bahan, maklumat atau perisian. Perbadanan Putrajaya mempunyai hak untuk membuat perubahan atau kemaskini pada laman ini pada bila-bila masa tanpa notis.  
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Had Liabiliti </b> Perbadanan Putrajaya tidak akan bertanggungjawab sama ada secara langsung, tidak langsung, sampingan, ganti rugi khas atau akibat, atau ganti rugi terhadap kehilangan keuntungan, pendapatan, data atau penggunaan, ditanggung oleh anda atau mana-mana pihak ketiga, sama ada dalam satu tindakan dalam kontrak atau tort, muncul daripada akses anda kepada, atau disebabkan penggunaan tapak ini. Beberapa bidang kuasa tidak membenarkan had atau pengecualian liabiliti. Maka, beberapa had-had di atas mungkin tidak berkaitan dengan anda.  
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Ganti Rugi </b> Anda bersetuju untuk mempertahankan, memberi jaminan ganti rugi dan tidak mempertanggungjawabkan Perbadanan Putrajaya, pegawai-pegawainya, pengarah-pengarah, pekerja-pekerja dan ejen-ejennya daripada dan dari sebarang dan semua tuntutan, liabiliti, kerosakan, kerugian atau perbelanjaan, termasuk bayaran yuran dan kos guaman, yang disebabkan daripada atau apa-apa cara yang berkaitan dengan akses anda kepada atau disebabkan penggunaan Laman ini. 
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Polisi Privasi </b> Perbadanan Putrajaya mengambil berat terhadap privasi anda dan telah membuat satu dasar mengenai kepentingan privasi. Anda boleh mendapat dasar privasi terkini. 
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Sekatan Eksport / Pematuhan Undang-undang </b> Anda tidak dibenarkan untuk akses, muat turun, menggunakan atau mengeksport Laman ini, atau kandungannya, perisian, produk atau perkhidmatan yang disediakan di Laman ini yang melanggar undang-undang eksport Malaysia atau peraturan-peraturan, atau melanggar mana-mana undang-undang atau peraturan-peraturan yang berkenaan. Anda bersetuju untuk mematuhi semua undang-undang eksport dan sekatan serta peraturan-peraturan daripada agensi Malaysia atau asing atau pihak berkuasa, dan tidak secara langsung atau tidak langsung memberi atau menyediakan perkhidmatan dan produk Perbadanan Putrajaya yang melanggar mana-mana sekatan sebegitu, undang-undang atau peraturan-peraturan, atau dengan tanpa kelulusan-kelulusan yang diperlukan , termasuk, tidak terhad, untuk pembangunan, reka bentuk, pengeluaran atau pembuatan senjata nuklear, kimia atau biologi untuk kemusnahan besar-besaran. Sebagaimana yang berkenaan, anda hendaklah mendapatkan dan menanggung semua perbelanjaan berkaitan dengan mana-mana lesen perlu dan / atau pengecualian terhadap penggunaan sendiri perkhidmatan Perbadanan Putrajaya di luar Malaysia.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Undang-undang yang Diguna Pakai </b> Semua hal berkaitan dengan akses anda kepada, dan penggunaan, Laman ini hendaklah merujuk kepada Undang-Undang Malaysia. Sebarang tindakan undang-undang atau prosiding yang berkaitan dengan akses anda kepada, atau penggunaan, Laman ini akan didengar dalam mahkamah negeri atau persekutuan di Kuala Lumpur, Malaysia. Anda dan Perbadanan Putrajaya bersetuju untuk mematuhi bidang kuasa, dan bersetuju bahawa tempat yang wajar adalah di mahkamah-mahkamah ini untuk mana-mana tindakan undang-undang atau prosiding.
                                </div>
                            </li>
                            <br>
                            <li>
                                <div class="ml-7">
                                <b>Hak cipta / Maklumat Cap Dagangan </b> Hak Cipta © 2013 , Perbadanan Putrajaya. Hakcipta terpelihara. Nama lain yang muncul pada Laman mungkin cap dagangan bagi pemilik masing-masing.
                                </div>
                            </li>
                            <br>
                        </ol>
                    </p>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection
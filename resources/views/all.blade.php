<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PPJ Virtual Tour</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.css" crossorigin="anonymous"/>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
    <style>
        .panorama {
            width: 100%;
            height: 400px;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="panorama" id="panorama"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama1"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama2"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama3"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama4"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama5"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama6"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama7"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama8"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama9"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama10"></div>
            </div>
            <div class="col-lg-4">
                <div class="panorama" id="panorama11"></div>
            </div>
        </div>
    </div>
    <script>
        pannellum.viewer('panorama', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Auditorium Tun Azizan Zainal Abidin",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 10,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu masuk Auditorium TAZA.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 10, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "2"
                        }
                    ]
                },
                "2": {
                    "title": "Ruang Legar Bawah Auditorium Tun Azizan Zainal Abidin",
                    "hfov": 510,
                    "pitch": 10,
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Ruang Legar TAZA.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 150, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "1"
                        },
                        {
                            "pitch": 20.1,
                            "yaw": 50, //melintang
                            "type": "scene",
                            "text": "Ruang Legar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "3"
                        },
                        {
                            "pitch": 20.1,
                            "yaw": -50, //melintang
                            "type": "scene",
                            "text": "Ruang Legar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "3"
                        }
                    ]
                },
                "3": {
                    "title": "Ruang Legar Auditorium Tun Azizan Zainal Abidin",
                    "pitch": 10,
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Ruang Legar Auditorium TAZA.jpg",
                    "hotSpots": [
                        {
                            "pitch": -10.1,
                            "yaw": 3, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "4"
                        },
                        {
                            "pitch": -10.1,
                            "yaw": 235, //melintang
                            "type": "scene",
                            "text": "Ruang Legar Bawah Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "2"
                        },
                        {
                            "pitch": -10.1,
                            "yaw": 125, //melintang
                            "type": "scene",
                            "text": "Ruang Legar Bawah Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "2"
                        }
                    ]
                },

                "4": {
                    "title": "Auditorium Tun Azizan Zainal Abidin",
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Auditorium TAZA -1.jpg",
                    "hotSpots": [
                        {
                            "pitch": -10.1,
                            "yaw": 90, //melintang
                            "type": "scene",
                            "text": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "6"
                        },
                        {
                            "pitch": -10.1,
                            "yaw": -90, //melintang
                            "type": "scene",
                            "text": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "5"
                        },
                        {
                            "pitch": 10.1,
                            "yaw": 180, //melintang
                            "type": "scene",
                            "text": "Pandangan Atas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "7"
                        }
                    ]
                },

                "5": {
                    "title": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Auditorium TAZA-4.jpg",
                    "hotSpots": [
                        {
                            "pitch": -10.1,
                            "yaw": 55, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "3"
                        },
                        {
                            "yaw": -70, //melintang
                            "type": "scene",
                            "text": "Pandangan Pentas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "4"
                        },
                        {
                            "pitch": 10.1,
                            "yaw": 10, //melintang
                            "type": "scene",
                            "text": "Pandangan Atas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "7"
                        }
                    ]
                },

                "6": {
                    "title": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                    "hfov": 250,
                    "yaw": 170,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Auditorium TAZA-5.jpg",
                    "hotSpots": [
                        {
                            "pitch": -10.1,
                            "yaw": 140, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "3"
                        },
                        {
                            "yaw": -100, //melintang
                            "type": "scene",
                            "text": "Pandangan Pentas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "4"
                        },
                        {
                            "pitch": 10.1,
                            "yaw": 170, //melintang
                            "type": "scene",
                            "text": "Pandangan Atas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "7"
                        }
                    ]
                },

                "7": {
                    "title": "Pandangan Atas Auditorium Tun Azizan Zainal Abidin",
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Auditorium TAZA-2.jpg",
                    "hotSpots": [
                        {
                            "pitch": -15.1,
                            "yaw": 30, //melintang
                            "type": "scene",
                            "text": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "6"
                        },
                        {
                            "pitch": -15.1,
                            "yaw": -30, //melintang
                            "type": "scene",
                            "text": "Pandangan Sisi Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "5"
                        },
                        {
                            "pitch": -10.1,
                            "yaw": 0, //melintang
                            "type": "scene",
                            "text": "Pandangan Pentas Auditorium Tun Azizan Zainal Abidin",
                            "sceneId": "4"
                        }
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama1', {
            "type": "equirectangular",
            "panorama": "assets/media/dewan/Panggung Eksperimen Mawar Sari 1,2.jpg",
            "autoLoad": true,
            "title": "Panggung Eksperimen Mawar Sari",
            "author": "Perbadanan Putrajaya (PPJ)"
        });
    </script>
    <script>
        pannellum.viewer('panorama2', {
            "type": "equirectangular",
            "panorama": "assets/media/dewan/PAnggung Eksperimen Mawar Sari-1.jpg",
            "autoLoad": true,
            "title": "Panggung Eksperimen Mawar 1",
            "author": "Perbadanan Putrajaya (PPJ)"
        });
    </script>
    <script>
        pannellum.viewer('panorama3', {
            "type": "equirectangular",
            "panorama": "assets/media/dewan/Panggung Eksperimen Mawar Sari-2.jpg",
            "autoLoad": true,
            "title": "Panggung Eksperimen Mawar 2",
            "author": "Perbadanan Putrajaya (PPJ)",
            "yaw": 170,
        });
    </script>
    <script>
        pannellum.viewer('panorama4', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Seri Melati",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 150,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu masuk Dewan Seri Melati.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 175, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Seri Melati",
                            "sceneId": "2"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 212, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Seri Melati",
                            "sceneId": "2"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 117, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Seri Melati",
                            "sceneId": "2"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 78, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Seri Melati",
                            "sceneId": "2"
                        }
                    ]
                },
                "2": {
                    "title": "Dewan Seri Melati",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Seri Melati.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 143, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Seri Melati",
                            "sceneId": "1"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 108, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Seri Melati",
                            "sceneId": "1"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 78, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Seri Melati",
                            "sceneId": "1"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 46, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Seri Melati",
                            "sceneId": "1"
                        }
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama5', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Melati 1",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Dewan MElati 1.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 278, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Melati 1",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Melati 1",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Melati 1,2,3.jpg",
                    "hotSpots": [
                        {
                            "pitch": -4.9,
                            "yaw": 249, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Melati 1",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama6', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Melati 2",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Dewan Melati 2.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 208, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Melati 2",
                            "sceneId": "2"
                        },
                        {
                            "pitch": -2.1,
                            "yaw": 150, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Melati 2",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Melati 2",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Melati 1,2,3.jpg",
                    "hotSpots": [
                        {
                            "pitch": -4.9,
                            "yaw": 249, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Melati 2",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama7', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Melati 3",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Dewan Melati 3.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 160, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Melati 3",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Melati 3",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Melati 1,2,3.jpg",
                    "hotSpots": [
                        {
                            "pitch": -4.9,
                            "yaw": 249, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Melati 3",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama8', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Ruang Legar Utama Dewan Persidangan",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 10,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Ruang Legar Utama Dewan Persidangan.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 0, //melintang
                            "type": "scene",
                            "text": "Ruang Legar Utama Dewan Persidangan",
                            "sceneId": "2"
                        }
                    ]
                },
                "2": {
                    "title": "Ruang Legar Utama Dewan Persidangan",
                    "hfov": 510,
                    "pitch": 0,
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Ruang Legar Dewan Persidangan.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 120, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Ruang Legar Utama Dewan Persidangan",
                            "sceneId": "1"
                        }
                    ]
                }
            }
        });
    </script>
    <script>
        pannellum.viewer('panorama9', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Seri Siantan",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 0,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Dewan Seri Siantan.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 0, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Seri Siantan",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Seri Siantan",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Seri Siantan.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 175, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Seri Siantan",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama10', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Siantan 1",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk Siantan 1.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 180, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Siantan 1",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Seri Siantan",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Siantan 1,2.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 175, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Siantan 1",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
    <script>
        pannellum.viewer('panorama11', {
            "default": {
                "firstScene": "1",
                "author": "Perbadanan Putrajaya (PPJ)",
                "sceneFadeDuration": 1000,
                "autoLoad": true,
            },

            "scenes": {
                "1": {
                    "title": "Dewan Siantan 2",
                    "hfov": 510,
                    "pitch": -3,
                    "yaw": 180,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Pintu Masuk  Siantan 2.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 180, //melintang
                            "type": "scene",
                            "text": "Pintu Masuk Dewan Siantan 2",
                            "sceneId": "2"
                        },
                    ]
                },
                "2": {
                    "title": "Dewan Seri Siantan",
                    "hfov": 510,
                    "pitch": -10,
                    "yaw": 270,
                    "type": "equirectangular",
                    "panorama": "assets/media/dewan/Dewan Siantan 1,2.jpg",
                    "hotSpots": [
                        {
                            "pitch": -2.1,
                            "yaw": 175, //melintang
                            "type": "scene",
                            "text": "Pintu Keluar Dewan Siantan 2",
                            "sceneId": "1"
                        },
                    ]
                },

            }
        });
    </script>
</body>

</html>
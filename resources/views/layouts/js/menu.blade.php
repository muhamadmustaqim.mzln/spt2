<script>
    document.addEventListener('DOMContentLoaded', function() {
    const toggles = document.querySelectorAll('.menu-item-submenu.menu-item-rel > a.menu-link');
    const submenus = document.querySelectorAll('.menu-submenu');

    toggles.forEach(toggle => {
        toggle.addEventListener('click', function(e) {
            e.preventDefault();
            const submenu = this.nextElementSibling;

            // Toggle the 'active' class on the submenu
            submenu.classList.toggle('active');

            // Close other submenus
            submenus.forEach(otherSubmenu => {
                if (otherSubmenu !== submenu) {
                    otherSubmenu.classList.remove('active');
                }
            });
        });
    });

    // Prevent the submenu from closing when clicking inside it
    submenus.forEach(submenu => {
        submenu.addEventListener('click', function(e) {
            e.stopPropagation();
        });
    });

    // Close submenu on outside click
    document.addEventListener('click', function(e) {
        let isClickInside = false;
        toggles.forEach(toggle => {
            if (toggle.contains(e.target) || toggle.nextElementSibling.contains(e.target)) {
                isClickInside = true;
            }
        });

        if (!isClickInside) {
            submenus.forEach(submenu => {
                submenu.classList.remove('active');
            });
        }
    });

    // Close submenu on scroll
    window.addEventListener('scroll', function() {
        submenus.forEach(submenu => {
            submenu.classList.remove('active');
        });
    });
});
</script>
<!DOCTYPE html>
<html lang="en">
	<!-- 
			#########        ##########
			###      ###            ###
			###        ###          ###
			###         ###         ###
			###         ###         ###
			###        ###    ###   ###
			###      ###      ###   ###
			#########         #########    Created By: DJ🤖
	-->
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title>PPJ | Sistem Pengurusan Tempahan</title>
		<meta name="description" content="Sistem Tempahan untuk Pengguna Awam bagi Fasiliti-fasiliti di Perbadanan Putrajaya (PPj)" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" crossorigin="anonymous"/>
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="{{ asset('assets/plugins/custom/leaflet/leaflet.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/leaflet/MarkerCluster.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/leaflet/MarkerCluster.Default.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.2.9') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/pages/wizard/wizard-4.css?v=7.2.9') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/owlcarousel/assets/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.css" crossorigin="anonymous"/>
		<link href="{{ asset('assets/plugins/custom/lightgallery/css/lightgallery.css') }}" rel="stylesheet">
		
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="{{ asset('assets/media/logos/ppj.png') }}" />

		<meta name="csrf-token" content="{{ csrf_token() }}">
	</head>
	<style>
		.popup {
			display: none;
			position: fixed;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			z-index: 9999;
			background-color: rgba(0, 0, 0, 0.8);
		}

		.popup-content {
			position: relative;
			max-width: 100%;
			max-height: 90%;
			padding: 10px;
			text-align: center;
		}

		.close {
			position: absolute;
			top: 0;
			right: 0;
			color: #fff;
			font-size: 30px;
			cursor: pointer;
		}

		.nav {
			list-style-type: none;
		}

		.nav-item {
			display: inline-block;
			margin-right: 10px;
		}

		.btn {
			display: inline-block;
			padding: 8px 12px;
			text-decoration: none;
			color: #fff;
			border: none;
			cursor: pointer;
		}

		.btn-icon {
			padding: 0;
		}

		.btn-bg-light {
			background-color: #f0f0f0;
		}

		.btn-hover-info:hover {
			background-color: #17a2b8;
		}

		.fa-video {
			color: #17a2b8;
			font-size: 20px;
		}
		
	</style>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="page-loading-enabled page-loading header-fixed header-mobile-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		<!--begin::Page loader-->
		<div class="page-loader page-loader-logo">
			<img alt="Logo" class="max-h-75px" src="{{ asset('assets/media/logos/ppj-dark.png') }}" />
			<div class="spinner spinner-dark spinner-lg"></div>
		</div>
		<!--end::Page Loader-->
		<!--begin::Header Mobile-->
		<div id="kt_header_mobile" class="header-mobile header-mobile-fixed">
			<div class="d-flex align-items-center">
				<!--begin::Logo-->
				<a href="#" class="mr-7 mt-2">
					<img alt="Logo" src="{{ asset('assets/media/logos/ppj-light.png') }}" class="max-h-50px" />
				</a>
				<!--end::Logo-->
			</div>
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center">
				<button class="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle">
					<span></span>
				</button>
				<button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-3x">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24"/>
								<rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="4" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="4" y="16" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="16" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="16" width="4" height="4" rx="2"/>
							</g>
						</svg>
					</span>
				</button>
			</div>
			<!--end::Toolbar-->
		</div>
		<!--end::Header Mobile-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header flex-column header-fixed ">
						<!--begin::Top-->
						<div class="header-top">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Left-->
								<div class="d-none d-lg-flex align-items-center mr-3">
									<!--begin::Logo-->
									<a href="{{ url('/') }}" class="mr-10">
										<img alt="Logo" src="{{ asset('assets/media/logos/ppj-light.png') }}" class="max-h-75px" />
									</a>
									<!--end::Logo-->
								</div>
								<!--end::Left-->
								<!--begin::Topbar-->
								<div class="topbar">
									@if(Session::has('user'))  
										<!--begin::Notifications-->

										<!--end::Notifications-->
										<div class="dropdown">
											<!--begin::Toggle-->
											<div class="topbar-item" data-toggle="dropdown" data-offset="0px,0px">
												<!--<div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto" id="kt_quick_user_toggle">-->
												<div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto" >
													<span class="text-white opacity-70 font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
													<span class="text-white opacity-90 font-weight-bolder font-size-base d-none d-md-inline mr-4">{{ Session::get('user')['name'] }}</span>
													<span class="symbol symbol-35">
														<span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-15">{{ mb_substr(Session::get('user')['name'], 0, 1) }}</span>
													</span>
												</div>
											</div>
											<!--end::Toggle-->
											<!--begin::Dropdown-->
											<div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg p-0">
												<!--begin::Header-->
												<div class="d-flex align-items-center py-10 px-8 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url({{asset('assets/media/bg/bg-1.jpg')}})">
													<!--begin::Symbol-->
													<span class="symbol symbol-35 mr-5">
														<span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-15">{{ mb_substr(Session::get('user')['name'], 0, 1) }}</span>
													</span>
													<!--end::Symbol-->
													<!--begin::Text-->
													<div class="text-white m-0 flex-grow-1 mr-3 font-size-h5">{{ Session::get('user')['name'] }}<br>({{ Helper::get_role_name(Session::get('user.role')) }})</div>
													<!--end::Text-->
												</div>
												<div class="separator separator-solid"></div>
												<!--end::Header-->
												<!--begin::Nav-->
												<div class="navi navi-spacer-x-0 pt-5">
													<!--begin::Item-->
													<a href="https://user.ppj.gov.my/profile" class="navi-item px-8">
														<div class="navi-link">
															<div class="navi-icon mr-2">
																<i class="fas fa-user text-info"></i>
															</div>
															<div class="navi-text">
																<div class="font-weight-bold">Profil Saya</div>
															</div>
														</div>
													</a>
													<!--end::Item-->
													<!--end::Item-->
													<!--begin::Footer-->
													<div class="navi-separator mt-3"></div>
													<div class="navi-footer px-8 py-5">
														<a href="{{ url('/logout') }}" class="btn btn-light-primary font-weight-bold">Log Keluar</a>
													</div>
													<!--end::Footer-->
												</div>
												<!--end::Nav-->
											</div>
											<!--end::Dropdown-->
										</div>
										<!--end::User-->
									@else
										<div class="topbar-item">
											<a href="{{ url('/auth') }}" class="btn btn-sm btn-transparent-white font-weight-bold"><i class="fas fa-sign-in-alt"></i> Log Masuk</a>
										</div>
									@endif
								</div>
								<!--end::Topbar-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Top-->
						<!--begin::Bottom-->
						<div class="header-bottom">
							@include('layouts.menu')
						</div>
						<!--end::Bottom-->
					</div>
					<!--end::Header-->
					@yield('container')
                    <!--begin::Footer-->
					<div class="footer kt-grid__item bgi-position-center-center bgi-size-cover bgi-no-cover" id="kt_footer" style="background-image: url('{{ asset('assets/media/bg/bg-2.jpg') }}');">
						<!--begin::Container-->
						<div class="container py-lg-18 py-8">
							<!--begin::Row-->
							<div class="row">
								<!--begin::Col-->
								<div class="col-lg-4 my-lg-0 my-5">
									<h4 class="text-white pb-3">Alamat</h4>
									<p class="m-0 text-white opacity-45">Perbadanan Putrajaya, 
									<br>Kompleks Perbadanan Putrajaya, 
									<br>24, Persiaran Perdana, Presint 3, 
									<br>62675 Putrajaya, Malaysia</p>
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-lg-4 my-lg-0 my-5">
									<h4 class="text-white pb-3">Hubungi Kami</h4>
									<p class="m-0 text-white opacity-45"><i class="fas fa-phone-alt mr-2 mb-2"></i> 603 8000 8000
										<br><i class="fas fa-fax mr-2 mb-2"></i> 603 8887 5000 (Fax)
										<br><i class="fas fa-phone-alt mr-2 mb-2"></i> 603 8887 3000 (Aduan)
										<br><i class="fas fa-envelope mr-2 mb-2"></i> <a href="mailto: ppjonline@ppj.gov.my" class="text-white text-hover-success">ppjonline@ppj.gov.my</a> 
										<br><i class="fas fa-link mr-2 mb-2"></i> <a href="https://ppj.spab.gov.my/" target="_blank" class="text-white text-hover-success">Pautan Aduan</a>
									</p>
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-lg-4 my-lg-0 my-5">
									<h4 class="text-white pb-3">Pautan</h4>
									<div class="d-flex align-item1s-center">
										<div class="d-flex flex-column mr-18">
											<a href="{{ url('/terma') }}" class="text-white opacity-55 text-hover-success">Terma dan Syarat</a>
											<a href="{{ url('/privasi') }}" class="py-2 text-white opacity-55 text-hover-success">Dasar Privasi</a>
											<a href="{{ url('/keselamatan') }}" class="text-white opacity-50 text-hover-success">Dasar Keselamatan</a>
										</div>
										<div class="d-flex flex-column">
											<a href="{{ url('/penafian') }}" class="text-white opacity-55 text-hover-success">Penafian</a>
											<a href="#" class="py-2 text-white opacity-55 text-hover-success">Peta Laman</a>
											<a href="#" class="text-white opacity-55 text-hover-success">FAQs</a>
										</div>
									</div>
								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
						</div>
						<!--end::Container-->
						<!--begin::Container-->
						<div class="separator separator-solid opacity-7"></div>
						<!--end::Container-->
						<!--begin::Container-->
						<div class="container py-8">
							<div class="d-flex align-items-center justify-content-between flex-lg-row flex-column">
								<!--begin::Copyright-->
								<div class="d-flex align-items-center order-lg-1 order-2">
									<img alt="Logo" src="{{ asset('assets/media/logos/ppj.png') }}" class="logo-sticky max-h-90px">
									<span class="text-muted font-weight-bold mx-2"> Hakcipta Terpelihara © {{date('Y')}} </span>
									<a href="https://www.ppj.gov.my/" target="_blank" class="text-primary text-hover-success">Perbadanan Putrajaya</a>
								</div>
								<!--end::Copyright-->
								<!--begin::Nav-->
								<div class="d-flex align-items-center order-lg-2 order-1 mb-lg-0 mb-5">
									<a href="https://twitter.com/putrajayacorp" target="_blank" class="text-white pl-0 text-hover-success"><i class="socicon-twitter text-light icon-xl mr-4"></i></a>
									<a href="https://www.instagram.com/perbadananputrajaya/" target="_blank" class="text-white pl-0 text-hover-success"><i class="socicon-instagram text-light icon-xl mr-4"></i></a>
									<a href="https://www.facebook.com/putrajaya" target="_blank" class="text-white pl-0 text-hover-success"><i class="socicon-facebook text-light icon-xl mr-4"></i></a>
								</div>
								<!--end::Nav-->
							</div>
						</div>
						<!--end::Container-->
					</div>
					<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop">
			<span class="svg-icon">
				<!--begin::Svg Icon | path:{{ asset('assets/media/svg/icons/Navigation/Up-2.svg') }}-->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon points="0 0 24 0 24 24 0 24" />
						<rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
						<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
					</g>
				</svg>
				<!--end::Svg Icon-->
			</span>
		</div>
		<!--end::Scrolltop-->
		<!--begin::Sticky Toolbar-->
		<ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
			<!--begin::Item-->
			<li class="nav-item mb-2" id="kt_demo_panel_toggle" data-toggle="tooltip" title="ppjonline@ppj.gov.my" data-placement="right">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-success btn-hover-success" href="#">
				<i class="far fa-envelope"></i>
				</a>
			</li>
			<!--end::Item-->
			<!--begin::Item-->
			<li class="nav-item mb-2" data-toggle="tooltip" title="+603 8887 3000 (Aduan)" data-placement="left">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" href="#" target="_blank">
					<i class="fas fa-phone-alt"></i>
				</a>
			</li>
			<!--end::Item-->
			<!--begin::Item-->
			<li class="nav-item mb-2" data-toggle="tooltip" title="Manual Pengguna (Video)" data-placement="left">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-info btn-hover-info" href="#" id="openVideo">
					<i class="fas fa-video"></i>
				</a>
			</li>
			<!-- Popup Video -->
			<div class="popup" id="videoPopup">
				<div class="popup-content">
					<span class="close float-right" id="closeVideo">&times;</span>
					<video id="videoFrame" width="1500" height="750" controls>
						<source src="https://spt2.ppj.gov.my/assets/media/Tutorial Tempahan Sukan.mp4" type="video/mp4">
						Your browser does not support the video tag.
					</video>
				</div>
			</div>
			{{-- <div class="popup" id="videoPopup">
				<div class="popup-content">
					<span class="close float-right" id="closeVideo">&times;</span>
					<iframe width="1500" height="750" src="https://spt2.ppj.gov.my/assets/media/Tutorial Tempahan Sukan.mp4" frameborder="0" allowfullscreen></iframe>
				</div>
			</div> --}}
			<!-- Popup Video -->
			{{-- <li class="nav-item mb-2" data-toggle="tooltip" title="Manual Pengguna (Video)" data-placement="left">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-info btn-hover-info" href="#" target="_blank">
				<i class="fas fa-video"></i>
				</a>
			</li> --}}
			<!--end::Item-->
			<!--begin::Item-->
			<li class="nav-item" id="kt_sticky_toolbar_chat_toggler" data-toggle="tooltip" title="Manual Pengguna (PDF)" data-placement="left">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-danger btn-hover-danger" href="#" data-toggle="modal" data-target="#kt_chat_modal">
					<i class="far fa-file-alt"></i>
				</a>
			</li>
			<!--end::Item-->
		</ul>
		<!--end::Sticky Toolbar-->
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#0BB783", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#D7F9EF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
		<script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
		<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
		<!--end::Page Scripts-->
		<script src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js?v=7.2.9') }}"></script>
		<script src="{{ asset('assets/js/my.js') }}"></script>
		<script type="text/javascript" src="https://botsrv.com/qb/widget/MY12wr8Wlnmd8ARO/pXNoar1wBgblBOn4" async defer></script>
		<style>
			.quriobotWidgetButton{
				bottom: 88px !important;
			}
		</style>
		@yield('js_content')
		<script>
			$(".btn-delete").on('click', function(event) {
				event.preventDefault();
				const href = $(this).attr('href');
				Swal.fire({
					title: 'Anda pasti?',
					text: "Rekod yang dipadam tidak dapat dikembalikan!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'Kembali',
					confirmButtonText: 'Saya Pasti'
					}).then((result) => {
					if (result.value) {
						document.location.href = href;
					}
				})
			});

			$(".btn-confirm").on('click', function(event) {
				event.preventDefault();
				const fform = document.getElementById('form');
				Swal.fire({
					title: 'Anda pasti?',
					text: "Maklumat dihantar adalah benar!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					cancelButtonText: 'Kembali',
					confirmButtonText: 'Saya Pasti'
					}).then((result) => {
					if (result.value) {
						fform.submit(); // Submit form
					}
				})
			});

			$(document).ready(function() {
				$('#openVideo').click(function() {
					$('#videoPopup').fadeIn();
					// Play the video when the popup is opened
					document.getElementById('videoFrame').play();
				});

				$('#closeVideo, #backButton').click(function() {
					// Pause the video when the popup is closed
					document.getElementById('videoFrame').pause();
					$('#videoPopup').fadeOut();
				});

				// Stop video playback when leaving the page or closing the window
				$(window).on('beforeunload', function() {
					document.getElementById('videoFrame').pause();
				});
				// $('#openVideo').click(function() {
        		// 	$('#videoFrame').attr('src', 'https://spt2.ppj.gov.my/assets/media/Tutorial Tempahan Sukan.mp4');

				// 	$('#videoPopup').fadeIn();
				// });

				// $('#closeVideo, #backButton').click(function() {
				// 	// Find the iframe element within the popup content
				// 	var iframe = $('#videoPopup').find('iframe')[0];
				// 	// Set the source of the iframe to an empty string to stop the video
				// 	iframe.src = '';
					
				// 	$('#videoPopup').fadeOut();
				// });

				// // Stop video playback when leaving the page or closing the window
				// $(window).on('beforeunload', function() {
				// 	// Find the iframe element within the popup content
				// 	var iframe = $('#videoPopup').find('iframe')[0];
				// 	// Set the source of the iframe to an empty string to stop the video
				// 	iframe.src = '';
				// });
			});
		</script>
        </body>
	<!--end::Body-->
</html>
<style>
    .menu-submenu {
		display: none;
		position: absolute; /* Change to fixed if needed */
		top: 100%;
		left: 0;
		z-index: 1000;
		/* Other styling as needed */
    }

    .menu-submenu.active {
        display: block;
    }
    /* .container {
        width: 100%;
        margin: auto;
        padding: 0;
    } */

    /* .container a {
        white-space: nowrap;
    } */
    .menu-submenu::-webkit-scrollbar {
        width: 0; /* Hide vertical scrollbar in WebKit */
    }
</style>
<!--begin::Container-->
<div class="container">
    <!--begin::Header Menu Wrapper-->
    <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
        <!--begin::Header Menu-->
        <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
            <!--begin::Header Nav-->
            <ul class="menu-nav" data-bs-auto-close="inside" aria-expanded="false">
                <li class="menu-item" aria-haspopup="true">
                    <a href="{{ url('/') }}" class="menu-link" data-toggle="tooltip" data-placement="bottom" title="Laman Utama">
                        <span class="menu-text"><i class="fas fa-home"></i></span>
                    </a>
                </li>
                <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true" >
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="menu-text">Tempahan</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="{{ url('/hall') }}" class="menu-link">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tempahan Dewan</span>
                                </a>
                            </li>
                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="{{ url('/sport') }}" class="menu-link">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tempahan Sukan</span>
                                </a>
                            </li>
                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="{{ url('/event') }}" class="menu-link">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tempahan Acara</span>
                                </a>
                            </li>
                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                <a href="{{ url('dashboard') }}" class="menu-link">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tempahan Saya</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                @if(Session::has('user') && !empty(Session::get('user.roles'))) 
                    @if ((Session::get('user.role')) != 10)
                        {{-- begin::Papan Pemuka --}}
                        @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 16, 23, 24])) > 0)
                            <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Papan Pemuka</span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                    <ul class="menu-subnav">
                                        {{-- @if ((Session::get('user.role')) != 10) --}}
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 24])) > 0)
                                                <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                    <a href="{{ url('hall/dashboard') }}" class="menu-link">
                                                        <i class="menu-bullet menu-bullet-line">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Pengurusan Dewan</span>
                                                    </a>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 23])) > 0)
                                                <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                    <a href="{{ url('sport/dashboard') }}" class="menu-link">
                                                        <i class="menu-bullet menu-bullet-line">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Pengurusan Sukan</span>
                                                    </a>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                                <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                    <a href="{{ url('event/dashboard') }}" class="menu-link">
                                                        <i class="menu-bullet menu-bullet-line">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Pengurusan Acara</span>
                                                    </a>
                                                </li>
                                            @endif
                                        {{-- @endif --}}
                                        {{-- <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ url('dashboard') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-line">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Tempahan Saya</span>
                                            </a>
                                        </li> --}}
                                    </ul>
                                </div>
                            </li>
                        @endif
                        {{-- end::Papan Pemuka --}}
                        @php
                            $width = '950';
                            $widthReport = '950';
                            $submenu = 'menu-submenu-center';
                            // if(Helper::get_role(Session::get('user.id')) != 1){
                            $countUnderSport = count(array_intersect(Session::get('user.roles'), [5, 6, 7, 8, 18]));
                            $countIf23 = count(array_intersect(Session::get('user.roles'), [23]));
                            $count = count(array_intersect(Session::get('user.roles'), [16, 23, 24]));
                            $admin = count(array_intersect(Session::get('user.roles'), [1]));
                            if($admin == 1){
                                $width = '950';
                                $widthReport = '950';
                                $submenu = 'menu-submenu-center';
                            } else {
                                if($count == 1){
                                    $width = '300';
                                    $widthReport = '700';
                                    $submenu = '';
                                } else if($count == 2){
                                    $width = '625';
                                    $widthReport = '825';
                                    $submenu = 'menu-submenu-center';
                                } else {
                                    $width = '950';
                                    $widthReport = '950';
                                    $submenu = 'menu-submenu-center';
                                }
                            }
                        @endphp
                        @if (count(array_intersect(Session::get('user.roles'), [1, 16, 23, 24])) > 0)
                            <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Pengurusan Sistem </span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                    <ul class="menu-subnav">
                                        @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/user/list') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Pengguna</span>
                                                </a>
                                            </li>
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/role/list') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Peranan</span>
                                                </a>
                                            </li>
                                            {{-- <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="#" class="menu-link" onclick="return false;">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Tempahan (Umum)</span>
                                                </a>
                                                <div class="menu-submenu menu-submenu-classic menu-submenu-right">
                                                    <ul class="menu-subnav">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('/hall') }}" class="menu-link">
                                                                <span class="menu-text">Tempahan Dewan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('/sport') }}" class="menu-link">
                                                                <span class="menu-text">Tempahan Sukan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('/event') }}" class="menu-link">
                                                                <span class="menu-text">Tempahan Acara</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li> --}}
                                            {{-- <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/list_maklumat') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Maklumat Sistem</span>
                                                </a>
                                            </li> --}}
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/banner/list') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Banner</span>
                                                </a>
                                            </li>
                                        @endif
                                        <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ url('/admin/announcement/list') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-line">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Pengurusan Pengumuman</span>
                                            </a>
                                        </li>
                                        <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                            <a href="{{ url('/takwim') }}" class="menu-link">
                                                <i class="menu-bullet menu-bullet-line">
                                                    <span></span>
                                                </i>
                                                <span class="menu-text">Pengurusan Kalendar (Umum)</span>
                                            </a>
                                        </li>
                                        @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/package/list') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Pakej Perkahwinan</span>
                                                </a>
                                            </li>
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/taxmanagement') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Cukai</span>
                                                </a>
                                            </li>
                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                <a href="{{ url('/admin/systemconfiguration') }}" class="menu-link">
                                                    <i class="menu-bullet menu-bullet-line">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Konfigurasi Fungsi</span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                        @endif
                        {{-- begin::Konfigurasi Tempahan --}}
                        @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 16, 23, 24])) > 0)
                            <li class="menu-item menu-item-submenu" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Konfigurasi Tempahan</span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-fixed {{ $submenu }}" style="width:{{ $width }}px; overflow-y: scroll;">
                                    <div class="menu-subnav">
                                        <ul class="menu-content">
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 24])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Pengurusan Dewan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/location') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Lokasi</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/facility') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/equipment') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Peralatan & Kelengkapan</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="#" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Fungsi Fasiliti</span>
                                                            </a>
                                                        </li> --}}
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/facilityprice') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Harga Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/equipmentprice') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Harga Peralatan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/discount') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Diskaun</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/admin/deposit') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Deposit</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="#" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Polisi</span>
                                                            </a>
                                                        </li> --}}
                                                    </ul>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 23])) > 0)
                                                <li class="menu-item " style="right: 0;">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Pengurusan Sukan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                        <div class="menu menu-column">
                                                            <ul class="menu-inner">
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/location') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Lokasi</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/facilitytype') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Jenis Kemudahan</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/facility') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Fasiliti</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/facilitydetail') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Fasiliti Terperinci</span>
                                                                    </a>
                                                                </li>
                                                                @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/slot') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Slot Masa</span>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/equipment') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Peralatan & Kelengkapan</span>
                                                                    </a>
                                                                </li>
                                                                @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                                                    <li class="menu-item" aria-haspopup="true">
                                                                        <a href="{{ url('sport/admin/functiontype') }}" class="menu-link">
                                                                            <i class="menu-bullet menu-bullet-line">
                                                                                <span></span>
                                                                            </i>
                                                                            <span class="menu-text">Pengurusan Jenis Fungsi Fasiliti</span>
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                        <div class="menu menu-column">
                                                            <ul class="menu-inner">
                                                                @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                                                    <li class="menu-item" aria-haspopup="true">
                                                                        <a href="{{ url('sport/admin/function') }}" class="menu-link">
                                                                            <i class="menu-bullet menu-bullet-line">
                                                                                <span></span>
                                                                            </i>
                                                                            <span class="menu-text">Pengurusan Fungsi Fasiliti</span>
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/facilityprice') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Harga Fasiliti</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/equipmentprice') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Harga Peralatan</span>
                                                                    </a>
                                                                </li>
                                                                @if (count(array_intersect(Session::get('user.roles'), [1])) > 0)
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/packagetype') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Jenis Pakej</span>
                                                                    </a>
                                                                </li>
                                                                @endif
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/package') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Pakej</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/closed') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Penutupan Fasiliti</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item" aria-haspopup="true">
                                                                    <a href="{{ url('sport/admin/policy') }}" class="menu-link">
                                                                        <i class="menu-bullet menu-bullet-line">
                                                                            <span></span>
                                                                        </i>
                                                                        <span class="menu-text">Pengurusan Polisi</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Pengurusan Acara</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/location') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Lokasi</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/facility') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Lokasi</span>
                                                            </a>
                                                        </li> --}}
                                                        <!--
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/facilitytype') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Jenis Kemudahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/slot') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Slot Masa</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/equipment') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Peralatan & Kelengkapan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/function') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Fungsi Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/facilityprice') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Harga Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/equipmentprice') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Harga Peralatan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/equipmentprice') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Pakej</span>
                                                            </a>
                                                        </li>
                                                        
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/closed') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Penutupan Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/policy') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pengurusan Polisi</span>
                                                            </a>
                                                        </li>
                                                        -->
                                                    </ul>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endif
                        {{-- end::Konfigurasi Tempahan --}}
                        {{-- begin::Pengurusan Tempahan --}}
                        @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 16, 23, 24])) > 0)
                            <li class="menu-item menu-item-submenu" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Pengurusan Tempahan</span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-fixed {{ $submenu }}" style="width:{{ $width }}px;">
                                    <div class="menu-subnav">
                                        <ul class="menu-content">
                                            {{-- Dewan --}}
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 24])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tempahan Dewan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/internal/book') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Dalaman</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/external') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Luaran</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/reschedule') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Penjadualan Semula</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/cancel') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pembatalan Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/duplicate') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Bertindan</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/available') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Semak Kekosongan Fasiliti</span>
                                                            </a>
                                                        </li> --}}
                                                        <li class="menu-item" aria-haspopup="true">
                                                            {{-- <a href="https://spt2.ppj.gov.my/tunggakan" class="menu-link"> --}}
                                                            <a href="{{ url('hall/refund')}}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pemulangan Bayaran Deposit</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                            {{-- Dewan --}}
                                            {{-- Sukan --}}
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 23])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tempahan Sukan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/internal') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Dalaman</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/external') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Luaran</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/reschedule') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Penjadualan Semula</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/cancel') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pembatalan Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/duplicate') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Bertindan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/available') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Semak Kekosongan Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="#" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pemulangan Bayaran</span>
                                                            </a>
                                                        </li> --}}
                                                    </ul>
                                                </li>
                                            @endif
                                            {{-- Sukan --}}
                                            {{-- Acara --}}
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Tempahan Acara</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/list/reservation') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Senarai Tempahan ( Dalaman / Luaran )</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/internal') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Dalaman</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/external') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Luaran</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/reschedule') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Penjadualan Semula</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/cancel') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pembatalan Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/duplicate') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Tempahan Bertindan</span>
                                                            </a>
                                                        </li>
                                                        <!--
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/available') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Semak Kekosongan Fasiliti</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/facility') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Pemulangan Bayaran</span>
                                                            </a>
                                                        </li>
                                                        -->
                                                    </ul>
                                                </li>
                                            @endif
                                            {{-- Acara --}}
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endif
                        {{-- end::Pengurusan Tempahan --}}
                        {{-- begin::Laporan --}}
                        @if (count(array_intersect(Session::get('user.roles'), [1, 5, 6, 7, 8, 16, 23, 24])) > 0)
                            <li class="menu-item menu-item-submenu" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Laporan</span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-fixed {{ $submenu }}" style="max-width:{{ $width }}px;">
                                    <div class="menu-subnav">
                                        <ul class="menu-content">
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 24])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tempahan Dewan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/tempahan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/pelanggan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Pelanggan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/hasil') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Hasil</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/harian') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Harian</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/deposit') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Bayaran Deposit</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/bayaran') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Bayaran Tertunggak</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/pemulangan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Pemulangan Bayaran</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/auditlog') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Audit</span>
                                                            </a>
                                                        </li> --}}
                                                    </ul>
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Audit Dewan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/penggunaan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Log Audit</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('hall/report/auditlog') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Audit</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 23])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tempahan Sukan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/pelanggan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Pelanggan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/hasil') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Hasil</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/harian') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Harian</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/bayarandepo') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Bayaran Deposit</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/aging') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Bayaran Tertunggak</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/usage') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Penggunaan</span>
                                                            </a>
                                                        </li>
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/refund') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Pemulangan Bayaran</span>
                                                            </a>
                                                        </li> --}}
                                                        
                                                    </ul>
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Audit Sukan</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/penggunaan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Log Audit</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('sport/report/auditlog') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Audit</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif
                                            @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                                <li class="menu-item">
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Tempahan Acara</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        {{-- <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Tempahan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/pelanggan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Pelanggan</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/hasil') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Kutipan Hasil</span>
                                                            </a>
                                                        </li> --}}
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/acara') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Senarai Acara</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/jumlahpengunjung') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Jumlah Pengunjung</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/jumlahacara') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Jumlah Acara</span>
                                                            </a>
                                                        </li>
                                                        <!--
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/admin/facility') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Pemulangan Bayaran</span>
                                                            </a>
                                                        </li>
                                                        -->
                                                    </ul>
                                                    <h3 class="menu-heading menu-toggle">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Audit Acara</span>
                                                        <i class="menu-arrow"></i>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/penggunaan') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Log Audit</span>
                                                            </a>
                                                        </li>
                                                        <li class="menu-item" aria-haspopup="true">
                                                            <a href="{{ url('event/report/auditlog') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Laporan Maklumat Audit</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endif                                            
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endif
                        {{-- end::Laporan --}}
                        {{-- <li class="menu-item menu-item-submenu" data-menu-toggle="click" aria-haspopup="true">
                            <a href="javascript:;" class="menu-link menu-toggle">
                                <span class="menu-text">Audit</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu menu-submenu-fixed menu-submenu-center {{ $submenu }}" style="max-width:{{ $widthReport }}px;">
                                <div class="menu-subnav">
                                    <ul class="menu-content">
                                        @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                            <li class="menu-item">
                                                <h3 class="menu-heading menu-toggle">
                                                    <i class="menu-bullet menu-bullet-dot">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Dewan</span>
                                                    <i class="menu-arrow"></i>
                                                </h3>
                                                <ul class="menu-inner">
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('hall/report/penggunaan') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Log Audit</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('hall/report/auditlog') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Maklumat Audit</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endif
                                        @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                            <li class="menu-item">
                                                <h3 class="menu-heading menu-toggle">
                                                    <i class="menu-bullet menu-bullet-dot">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Sukan</span>
                                                    <i class="menu-arrow"></i>
                                                </h3>
                                                <ul class="menu-inner">
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('sport/report/penggunaan') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Log Audit</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('sport/report/auditlog') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Maklumat Audit</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endif
                                        @if (count(array_intersect(Session::get('user.roles'), [1, 16])) > 0)
                                            <li class="menu-item">
                                                <h3 class="menu-heading menu-toggle">
                                                    <i class="menu-bullet menu-bullet-dot">
                                                        <span></span>
                                                    </i>
                                                    <span class="menu-text">Pengurusan Acara</span>
                                                    <i class="menu-arrow"></i>
                                                </h3>
                                                <ul class="menu-inner">
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('event/report/penggunaan') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Log Audit</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item" aria-haspopup="true">
                                                        <a href="{{ url('event/report/auditlog') }}" class="menu-link">
                                                            <i class="menu-bullet menu-bullet-line">
                                                                <span></span>
                                                            </i>
                                                            <span class="menu-text">Laporan Maklumat Audit</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </li> --}}
                        {{-- begin::Deposit --}}
                        @php
                            $i = 1;
                        @endphp
                        {{-- @if($i == 1) --}}
                        {{-- @if (Session::has('user.deposit_status')) 
                            @if (Session::get('user.deposit_status') == 1) --}}
                        @if (count(array_intersect(Session::get('user.roles'), [1, 21, 22, 26, 27])) > 0)
                            <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                                <a href="javascript:;" class="menu-link menu-toggle">
                                    <span class="menu-text">Deposit</span>
                                    <span class="menu-desc"></span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="menu-submenu menu-submenu-classic menu-submenu-right py-0">
                                {{-- <div class="menu-submenu menu-submenu-classic menu-submenu-right {{ $submenu }} py-0"> --}}
                                    <ul class="menu-subnav">
                                        <ul class="menu-content">
                                            <li class="menu-item">
                                                @if (count(array_intersect(Session::get('user.roles'), [1, 21, 22])) > 0)
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tindakan Jabatan</span>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        @if (count(array_intersect(Session::get('user.roles'), [1, 21])) > 0)
                                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                                <a href="{{ url('/list_deposit') }}" class="menu-link">
                                                                    <i class="menu-bullet menu-bullet-line">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="menu-text">Senarai Pemulangan Deposit</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                        {{-- <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                            <a href="{{ url('/deposit_payment_info') }}" class="menu-link">
                                                                <i class="menu-bullet menu-bullet-line">
                                                                    <span></span>
                                                                </i>
                                                                <span class="menu-text">Maklumat Bayaran</span>
                                                            </a>
                                                        </li> --}}
                                                        @if (count(array_intersect(Session::get('user.roles'), [1, 22])) > 0)
                                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                                <a href="{{ url('/list_action_support') }}" class="menu-link">
                                                                    <i class="menu-bullet menu-bullet-line">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="menu-text">Senarai Tindakan Penyokong</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                @endif
                                                @if (count(array_intersect(Session::get('user.roles'), [1, 26, 27])) > 0)
                                                    <h3 class="menu-heading menu-toggle">
                                                        <span class="menu-text">Tindakan Pegawai Kewangan BPSK</span>
                                                    </h3>
                                                    <ul class="menu-inner">
                                                        @if (count(array_intersect(Session::get('user.roles'), [1, 26])) > 0)
                                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                                <a href="{{ url('/list_action_review') }}" class="menu-link">
                                                                    <i class="menu-bullet menu-bullet-line">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="menu-text">Senarai Tindakan Penyemak</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                        @if (count(array_intersect(Session::get('user.roles'), [1, 27])) > 0)
                                                            <li class="menu-item menu-item-submenu" data-menu-toggle="hover" aria-haspopup="true">
                                                                <a href="{{ url('/list_action_verify') }}" class="menu-link">
                                                                    <i class="menu-bullet menu-bullet-line">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="menu-text">Senarai Tindakan Pelulus</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                @endif
                                            </li>
                                        </ul>
                                    </ul>
                                </div>
                            </li>
                        @endif
                            {{-- @endif
                        @endif --}}
                        {{-- end::Deposit --}}
                    @endif
                @endif
            </ul>
            <!--end::Header Nav-->
        </div>
        <!--end::Header Menu-->
    </div>
    <!--end::Header Menu Wrapper-->
</div>
<!--end::Container-->
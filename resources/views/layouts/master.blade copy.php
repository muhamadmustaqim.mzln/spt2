<!DOCTYPE html>
<html lang="en">
	<!-- 
			#########        ##########
			###      ###            ###
			###        ###          ###
			###         ###         ###
			###         ###         ###
			###        ###    ###   ###
			###      ###      ###   ###
			#########         #########    Created By: DJ🤖
	-->
	<!--begin::Head-->
	<head><base href="">
		<meta charset="utf-8" />
		<title>PPJ | Sistem Pengurusan Tempahan</title>
		<meta name="description" content="Sistem Tempahan untuk Pengguna Awam bagi Fasiliti-fasiliti di Perbadanan Putrajaya (PPj)" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" crossorigin="anonymous"/>
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="{{ asset('assets/plugins/custom/leaflet/leaflet.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/leaflet/MarkerCluster.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/leaflet/MarkerCluster.Default.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.2.9') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/pages/wizard/wizard-4.css?v=7.2.9') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/owlcarousel/assets/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.css" crossorigin="anonymous"/>
		<link href="{{ asset('assets/plugins/custom/lightgallery/css/lightgallery.css') }}" rel="stylesheet">
		
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="{{ asset('assets/media/logos/ppj.png') }}" />

		<meta name="csrf-token" content="{{ csrf_token() }}">
	</head>
	<style>
		.popup {
			display: none;
			position: fixed;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			z-index: 9999;
			background-color: rgba(0, 0, 0, 0.8);
		}

		.popup-content {
			position: relative;
			max-width: 100%;
			max-height: 90%;
			padding: 10px;
			text-align: center;
		}

		.close {
			position: absolute;
			top: 0;
			right: 0;
			color: #fff;
			font-size: 30px;
			cursor: pointer;
		}

		.nav {
			list-style-type: none;
		}

		.nav-item {
			display: inline-block;
			margin-right: 10px;
		}

		.btn {
			display: inline-block;
			padding: 8px 12px;
			text-decoration: none;
			color: #fff;
			border: none;
			cursor: pointer;
		}

		.btn-icon {
			padding: 0;
		}

		.btn-bg-light {
			background-color: #f0f0f0;
		}

		.btn-hover-info:hover {
			background-color: #17a2b8;
		}

		.fa-video {
			color: #17a2b8;
			font-size: 20px;
		}

		/* Sticky Header and Footer */
		.header-fixed {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			z-index: 1000;
			background-color: #fff; /* Add a background color if needed */
		}

		.footer-fixed {
			position: fixed;
			bottom: 0;
			left: 0;
			width: 100%;
			z-index: 1000;
			background-color: #fff; /* Add a background color if needed */
		}

		.main-content {
			padding-top: 120px; /* Adjust according to header height */
			padding-bottom: 100px; /* Adjust according to footer height */
		}
	</style>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="page-loading-enabled page-loading header-fixed header-mobile-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		<!--begin::Page loader-->
		<div class="page-loader page-loader-logo">
			<img alt="Logo" class="max-h-75px" src="{{ asset('assets/media/logos/ppj-dark.png') }}" />
			<div class="spinner spinner-dark spinner-lg"></div>
		</div>
		<!--end::Page Loader-->
		<!--begin::Header Mobile-->
		<div id="kt_header_mobile" class="header-mobile header-mobile-fixed">
			<div class="d-flex align-items-center">
				<!--begin::Logo-->
				<a href="#" class="mr-7 mt-2">
					<img alt="Logo" src="{{ asset('assets/media/logos/ppj-light.png') }}" class="max-h-50px" />
				</a>
				<!--end::Logo-->
			</div>
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center">
				<button class="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle">
					<span></span>
				</button>
				<button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-3x">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24"/>
								<rect fill="#000000" opacity="0.3" x="4" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="4" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="4" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="10" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="4" y="16" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="10" y="16" width="4" height="4" rx="2"/>
								<rect fill="#000000" x="16" y="16" width="4" height="4" rx="2"/>
							</g>
						</svg>
					</span>
				</button>
			</div>
			<!--end::Toolbar-->
		</div>
		<!--end::Header Mobile-->
		<div class="d-flex flex-column flex-root main-content">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header header-fixed">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Header Menu Wrapper-->
							<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
								<!--begin::Header Menu-->
								<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
									<!--begin::Header Nav-->
									<ul class="menu-nav">
										<li class="menu-item menu-item-rel">
											<a href="{{ route('home') }}" class="menu-link">
												<span class="menu-text">Laman Utama</span>
												<span class="menu-desc"></span>
											</a>
										</li>
										<li class="menu-item menu-item-rel">
											<a href="#" class="menu-link">
												<span class="menu-text">Tempahan</span>
												<span class="menu-desc"></span>
											</a>
										</li>
										<li class="menu-item menu-item-rel">
											<a href="#" class="menu-link">
												<span class="menu-text">Bayaran</span>
												<span class="menu-desc"></span>
											</a>
										</li>
										<li class="menu-item menu-item-rel">
											<a href="#" class="menu-link">
												<span class="menu-text">Pertanyaan</span>
												<span class="menu-desc"></span>
											</a>
										</li>
									</ul>
									<!--end::Header Nav-->
								</div>
								<!--end::Header Menu-->
							</div>
							<!--end::Header Menu Wrapper-->
							<!--begin::Topbar-->
							<div class="topbar">
								<div class="dropdown">
									<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
										<div class="btn btn-icon btn-hover-transparent-white d-flex align-items-center btn-lg px-md-2 w-md-auto">
											<div class="d-flex flex-column text-right pr-md-3">
												<span class="text-white opacity-50 font-weight-bold font-size-sm d-none d-md-inline">{{ Auth::user()->name }}</span>
											</div>
											<span class="symbol symbol-35">
												<span class="symbol-label font-size-h5 font-weight-bold text-white bg-white-o-30">{{ Auth::user()->initials }}</span>
											</span>
										</div>
									</div>
									<div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
										<ul class="navi navi-hover py-4">
											<li class="navi-item">
												<a href="{{ route('profile') }}" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<i class="fas fa-user"></i>
													</span>
													<span class="navi-text">Profil</span>
												</a>
											</li>
											<li class="navi-item">
												<a href="{{ route('logout') }}" class="navi-link">
													<span class="symbol symbol-20 mr-3">
														<i class="fas fa-sign-out-alt"></i>
													</span>
													<span class="navi-text">Log Keluar</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<!--end::Topbar-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!-- Your main content here -->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer footer-fixed" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-center justify-content-between">
							<div class="text-dark">
								<span>&copy; {{ date('Y') }} Perbadanan Putrajaya</span>
							</div>
							<div class="nav nav-dark">
								<a href="#" class="nav-link pl-0 pr-5">About</a>
								<a href="#" class="nav-link pl-0 pr-5">Privacy</a>
								<a href="#" class="nav-link pl-0 pr-0">Contact</a>
							</div>
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
		<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
	</body>
	<!--end::Body-->
</html>

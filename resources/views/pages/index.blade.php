@if(count($data['services']) > 0)
    <ul>
        @foreach($data['services'] as $service)
        <li>{{$service}}</li>
        @endforeach
    </ul>
    @lang('passwords.sent')
@endif
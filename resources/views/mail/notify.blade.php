Tuan/ Puan,<p>

@if($type==1)
	Adalah dimaklumkan bayaran deposit tuan/ puan telah diterima dan dengan ini tempahan telah  berjaya. Nombor tempahan tuan/ puan adalah:{{$notempahan}} </p>
@elseif($type == 2)
	Adalah dimaklumkan bayaran penuh tuan/ puan telah diterima dan dengan ini tempahan telah  berjaya. Nombor tempahan tuan/ puan adalah:{{$notempahan}} </p>
@elseif($type == 3)
	Adalah dimaklumkan bayaran tuan/ puan telah gagal dan tempahan ini telah dibatalkan. Nombor tempahan yang dibatalkan adalah:{{$notempahan}} </p>
@elseif($type == 4)
	Adalah dimaklumkan bahawa penjadualan tempahan anda tuan/ puan telah telah berjaya. Nombor tempahan  tuan/ puan adalah:{{$notempahan}} </p>
@endif
Berikut adalah maklumat tempahan:<p></p>
<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<table class="gridtable" style="cellspacing:0px;cellpadding;0px">
					<thead>
						<tr>
							<th width="2%" style="border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;">No</th>
							<th style="border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;" width="20%"><div style="text-align:left">Tarikh Tempahan</div></th>
							<th style="border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;"width="30%"><div style="text-align:left">Nama Dewan/ Fasiliti</div></th>							
							<th style="border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;" width="30%"><div style="text-align:left">Nama Gelanggang</div></th>
							<th style="border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;" width="30%"><div style="text-align:left">Slot Masa</div></th>
						</tr>
					</thead>	
					<tbody>	
		            <?php
                  
                      $i=1; 
                  
                      ?>
                      @foreach($email as $key => $value)
	                  		<tr>
								<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><?php echo $i ?></td>
								@if($checkfacility==1)
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left"><?php echo date('d-m-Y',strtotime($value->ehb_booking_date))?></div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left">{{ $value->eft_type_desc }},{{ $value->lc_description }}</div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left">{{ $value->efd_name }}</div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left"></div>{{ $value->est_slot_time}}</td>
								@else
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left"><?php echo date('d-m-Y',strtotime($value->esb_booking_date))?></div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left">{{ $value->eft_type_desc }},{{ $value->lc_description }}</div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left">{{ $value->efd_name }}</div></td>
									<td style = "border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;"><div class="text-left"></div>{{ $value->est_slot_time}}</td>
								@endif
						    </tr>
					    </tbody>
						<?php $i++;?>
				
				@endforeach
		
			</table><br>


<p>Sila log masuk di <a href="{{ $url }}"> https://spt.ppj.gov.my </a> untuk masuk ke dalam sistem</p>


Sila berurusan di Bahagian Perkhidmatan Komuniti, Perbadanan Putrajaya, Kompleks Perbadanan Putrajaya, 24, Persiaran Perdana, Presint 3, 62675 Putrajaya, Malaysia atau hubungi kami di : <p>

Dewan Serbaguna Presint    8     :  03-8887 7665 <br>
Kompleks Kejiranan Presint 9     :  03-8887 7668 <br>
Kompleks Kejiranan Presint 11    :  03-8887 7664 <br>
Kompleks Kejiranan Presint 16    :  03-8887 7669 <br>

<br>
Terima kasih kerana menggunakan khidmat kami, segala kesulitan amat dikesali.</p>

Sekian,<br>
<br>

<font color="green">Inisiatif ICT Hijau Perbadanan Putrajaya:<br>
           - Gunakan Perkhidmatan Atas Talian (On-Line)<br>
<br>
_____________________________________________________<br>
<br>

SAVE A TREE<br>
PLEASE DON'T PRINT THIS E-MAIL UNLESS IT'S NECESSARY</font>


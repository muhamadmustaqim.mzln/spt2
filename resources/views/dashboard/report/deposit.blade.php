@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Senarai Pemulangan Deposit</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('/hall/dashboard') }}" class="text-muted">Papan Pemuka</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('/hall/dashboard') }}" class="text-muted">Pengurusan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Senarai Pemulangan Deposit</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                            <!--begin::List Widget 21-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Body-->
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h3 class="card-label">Senarai Pemulangan Deposit</h3>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    <form action="" method="post">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-lg-1 col-form-label text-lg-left font-weight-bold my-auto" style="white-space: nowrap">Jenis Bayaran</label>
                                            <div class="col-lg-2">
                                                <select name="jenis" id="kt_select2_4" class="form-control" required>
                                                    <option value="1" {{ ($data['type'] == 1) ? 'selected' : ''}}>Kaunter</option>
                                                    <option value="2" {{ ($data['type'] == 2) ? 'selected' : ''}}>Online</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="float-left">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mr-2">Carian</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <table class="table table-bordered table-hover table-checkable" id="table" style="margin-top: 13px !important">
                                        <thead> 
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pelanggan</th>
                                                <th>No Kad Pengenalan</th>
                                                <th>No Tempahan</th>
                                                <th>No Resit</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;  
                                            @endphp
                                                @if ($data['type'] == 1)
                                                    @foreach ($data['lists'] as $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama($s->fk_users) }}</td>
                                                            <td>{{ Helper::get_ic_number($s->fk_users) }}</td>
                                                            <td>{{ Helper::get_noTempahan($s->fk_main_booking) }}</td>
                                                            <td>{{ $s->bp_receipt_number }}</td>
                                                            <td>
                                                                @if ($s->fk_lkp_payment_type == 1)
                                                                    <a href="#">Pemulangan Deposit</a>
                                                                @else
                                                                    @if ((Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) == 10)
                                                                        Pemulangan Penuh
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    @foreach ($data['lists'] as $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_ic_number(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_noTempahan($s->fk_main_booking) }}</td>
                                                            <td>{{ $s->fpx_serial_no }}</td>
                                                            <td>
                                                                @if ($s->fk_lkp_payment_type == 1)
                                                                    <a href="#">Pemulangan Deposit</a>
                                                                @else
                                                                    @if ((Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) == 10)
                                                                        Pemulangan Penuh
                                                                    @else
                                                                        {{ Helper::get_status_tempahan(Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) }}
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-body pt-5">
                                    <table class="table table-bordered table-hover table-checkable" id="table2" style="margin-top: 13px !important">
                                        <thead> 
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pelanggan</th>
                                                <th>No Kad Pengenalan</th>
                                                <th>No Tempahan</th>
                                                <th>No Resit</th>
                                                <th>Nama Bank</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;  
                                            @endphp
                                                {{-- @if ($data['type'] == 1)
                                                    @foreach ($data['bh_refund_req'] as $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama($s->fk_users) }}</td>
                                                            <td>{{ Helper::get_ic_number($s->fk_users) }}</td>
                                                            <td>{{ Helper::get_noTempahan($s->fk_main_booking) }}</td>
                                                            <td>{{ $s->bp_receipt_number }}</td>
                                                            <td>
                                                                @if ($s->fk_lkp_payment_type == 1)
                                                                    <a href="#">Pemulangan Deposit</a>
                                                                @else
                                                                    @if ((Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) == 10)
                                                                        Pemulangan Penuh
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else --}}
                                                    @foreach ($data['bh_refund_req'] as $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_ic_number(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_noTempahan($s->fk_main_booking) }}</td>
                                                            <td></td>
                                                            <td>{{ $s->bank_name }}</td>
                                                            <td>
                                                                {{-- <form action="{{ url('/hall/dashboard/depositcancel', Crypt::encrypt($s->id)) }}" method="post">
                                                                    @csrf
                                                                    <button type="submit" class="form-control btn btn-danger btn-confirm font-weight-bold mr-2">Batal Permohonan</button> --}}
                                                                    <a href="{{ url('/hall/dashboard/depositcancel', Crypt::encrypt($s->id)) }}" class="form-control btn btn-danger font-weight-bold mr-2">Batal Permohonan</a>
                                                                {{-- </form> --}}
                                                            </td>
                                                            {{-- <td>{{ $s->fpx_serial_no }}</td> --}}
                                                            {{-- <td>
                                                                @if ($s->fk_lkp_payment_type == 1)
                                                                    <a href="#">Pemulangan Deposit</a>
                                                                @else
                                                                    @if ((Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) == 10)
                                                                        Pemulangan Penuh
                                                                    @else
                                                                        {{ Helper::get_status_tempahan(Helper::get_main_booking($s->fk_main_booking)->fk_lkp_status) }}
                                                                    @endif
                                                                @endif
                                                            </td> --}}
                                                        </tr>
                                                    @endforeach
                                                {{-- @endif --}}
                                        </tbody>
                                    </table>
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::List Widget 21-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('dashboard.js.deposit')
@endsection
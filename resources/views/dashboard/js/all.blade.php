<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        var table = $('#table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
        var table = $('#table2').DataTable({
            dom: `<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>>
                <'row'<'col-sm-12'tr>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
        $('#carianSukan').on('change', function () {
            
        })
    });
</script>
<script>
    $(document).on('submit', '#searchForm', function(event) {
        event.preventDefault(); // Prevent form submission
        var formData = $(this).serialize(); // Serialize form data
        // AJAX request to submit search query
        $.ajax({
            type: 'GET',
            url: 'hall/dashboard/search',
            data: formData,
            success: function(response) {
                // Handle success response (e.g., update search results)
                $('#searchResults').html(response);
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error(error);
            }
        });
    });
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_5').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_daterangepicker_5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_daterangepicker_5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    // date picker
    $(function() {
        $('#kt_daterangepicker_6').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_daterangepicker_6').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_daterangepicker_6').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    $('#searchTerm').on('input', function(e) {
        e.preventDefault();
        var searchTerm = $(this).val();
        var csrfToken = "{{ csrf_token() }}";
        $.ajax({
            type: 'POST',
            url: '/sport/dashboard/report_keseluruhan',
            data: 
                { 
                    '_token': csrfToken,
                    'searchTerm': searchTerm 
                },
            success: function(response) {
                $('#table2 tbody').empty(); // Clear existing table rows
                $.each(response['test'], function(index, item) {
                    var row = $('<tr>');
                    row.append($('<td>').text(index + 1));
                    row.append($('<td>').html('<a href="{{ url("sport/maklumatbayaran") }}/' + item.encryptedId + '">' + item.bmb_booking_no + '</a>'));
                    row.append($('<td>').text(item.nama_pemohon));
                    row.append($('<td>').text(item.no_tel));
                    row.append($('<td>').text(item.nama_kemudahan));
                    row.append($('<td>').text(item.lokasi));
                    row.append($('<td>').text(item.status));
                    row.append($('<td>').text(item.bmb_booking_date));
                    $('#table2 tbody').append(row);
                });
                // console.log(response)
                // $('#table2 tbody').html(response);
            }
        });
    })
</script>
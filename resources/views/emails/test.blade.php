@component('mail::message')
## Tuan/ Puan,
@component('mail::subcopy')
Adalah dimaklumkan bayaran penuh tuan/ puan telah diterima dan dengan ini tempahan telah berjaya. Berikut merupakan maklumat tempahan anda:
@endcomponent

<table cellspacing="0" cellpadding="6" border="1" style="width:100%; border-collapse: collapse;">
    <tr><td width="40%" style="background-color: #2d3748; color: white"><b>No. Tempahan</b></td><td width="60%">SPS22082687157 </td></tr>
    <tr><td width="40%" style="background-color: #2d3748; color: white"><b>No. Tempahan</b></td><td width="60%">SPS22082687157 </td></tr>
    <tr><td width="40%" style="background-color: #2d3748; color: white"><b>No. Tempahan</b></td><td width="60%">SPS22082687157 </td></tr>
    <tr><td width="40%" style="background-color: #2d3748; color: white"><b>No. Tempahan</b></td><td width="60%">SPS22082687157 </td></tr>
</table>

@component('mail::subcopy')
Untuk maklumat lanjut berkenaan tempahan, sila <a href="{{ url('') }}">klik sini</a> untuk log masuk {{ config('app.name') }}. Terima kasih kerana menggunakan perkhidmatan kami, segala kesulitan amat dikesali.
@endcomponent

@endcomponent
  
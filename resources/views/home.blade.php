@extends('layouts.master')

@section('container')
<style>
	@media (max-width: 991.98px){
		.header-mobile-fixed .content {
			padding-top: 0;
		}
	}
</style>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">	
    <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach ($data['banner'] as $index => $b)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}"></li>
            @endforeach
            {{-- <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
        </ol>
        {{-- <img class="d-block w-100" height="420px" src="{{ URL::asset("{$b->bb_location}/{$b->bb_filename}") }}" alt="First slide"> --}}
        <div class="carousel-inner" style="z-index: -1">
            @foreach ($data['banner'] as $index => $b)
                <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                    <img class="d-block w-100" height="420px" src="{{ asset($b->bb_location . '/' . $b->bb_filename) }}" alt="">
                    <div class="carousel-caption">
                        <h1 class="@if(isset($b->text_color)) {{ $b->text_color }} @endif">{{ $b->bb_name }}</h1>
                        <p class="pb-0 mb-0 @if(isset($b->text_color)) {{ $b->text_color }} @endif">@if(isset($b->bb_text)) {{ $b->bb_text }} @endif</p>
                        @if (isset($b->bb_start_date) && isset($b->bb_end_date))
                            <p class="py-0 my-0 @if(isset($b->text_color)) {{ $b->text_color }} @endif">Dari {{Helper::date_format($b->bb_start_date)}} sehingga {{Helper::date_format($b->bb_end_date)}}</p>
                        @endif
                    </div>
                </div>
            @endforeach
            {{-- <div class="carousel-item active">
                <img class="d-block w-100" height="420px" src="{{ asset('assets/media/logos/pic.jpg') }}" alt="First slide">
                <div class="carousel-caption">
                    <h1>Selamat Datang ke Sistem Pengurusan Tempahan</h1>
                    <p>Sistem Tempahan Dewan</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" height="420px" src="{{ asset('assets/media/logos/pic.jpg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h1>Selamat Datang ke Sistem Pengurusan Tempahan</h1>
                    <p>Sistem Tempahan Sukan</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" height="420px" src="{{ asset('assets/media/logos/pic.jpg') }}" alt="Third slide">
                <div class="carousel-caption">
                    <h1>Selamat Datang ke Sistem Pengurusan Tempahan</h1>
                    <p>Sistem Permohonan Acara </p>
                </div>
            </div> --}}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>					
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <!--begin::Nav Panel Widget 3-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Wrapper-->
                            <div class="d-flex justify-content-between flex-column h-100">
                                <!--begin::Container-->
                                <div class="h-100">
                                    <!--begin::Header-->
                                    <div class="d-flex flex-column flex-center">
                                        <!--begin::Image-->
                                        <div class="bgi-no-repeat bgi-size-cover rounded h-300px w-100" style="background-image: url(assets/media/dewan.jpg)"></div>
                                        <!--end::Image-->
                                        <!--begin::Title-->
                                        <a href="{{ url('/hall') }}" class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">Tempahan Dewan</a>
                                        <!--end::Title-->
                                        <!--begin::Text-->
                                        <div class="font-weight-bold text-dark-50 font-size-sm pb-7 text-center">Tempahan Dewan-Dewan di Perbadanan Putrajaya dan Kompleks Kejiranan Presint</div>
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Header-->
                                </div>
                                <!--eng::Container-->
                                <!--begin::Footer-->
                                <div class="d-flex flex-center">
                                    <a href="{{ url('/hall') }}" class="btn btn-primary font-weight-bolder font-size-sm py-3 px-14">Lihat Lebih Lanjut</a>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Nav Panel Widget 3-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Nav Panel Widget 3-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Wrapper-->
                            <div class="d-flex justify-content-between flex-column h-100">
                                <!--begin::Container-->
                                <div class="h-100">
                                    <!--begin::Header-->
                                    <div class="d-flex flex-column flex-center">
                                        <!--begin::Image-->
                                        <div class="bgi-no-repeat bgi-size-cover rounded h-300px w-100" style="background-image: url(assets/media/Sukan2.jpg)"></div>
                                        <!--end::Image-->
                                        <!--begin::Title-->
                                        <a href="{{ url('/sport') }}" class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">Tempahan Sukan</a>
                                        <!--end::Title-->
                                        <!--begin::Text-->
                                        <div class="font-weight-bold text-dark-50 font-size-sm pb-7 text-center">Tempahan Sukan di Kompleks Kejiranan Presint</div>
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Header-->
                                </div>
                                <!--eng::Container-->
                                <!--begin::Footer-->
                                <div class="d-flex flex-center">
                                    <a href="{{ url('/sport') }}" class="btn btn-primary font-weight-bolder font-size-sm py-3 px-14">Lihat Lebih Lanjut</a>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Nav Panel Widget 3-->
                </div>
                <div class="col-xl-4">
                    <!--begin::Nav Panel Widget 3-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Wrapper-->
                            <div class="d-flex justify-content-between flex-column h-100">
                                <!--begin::Container-->
                                <div class="h-100">
                                    <!--begin::Header-->
                                    <div class="d-flex flex-column flex-center">
                                        <!--begin::Image-->
                                        <div class="bgi-no-repeat bgi-size-cover rounded h-300px w-100" style="background-image: url(assets/media/dataran.jpg)"></div>
                                        <!--end::Image-->
                                        <!--begin::Title-->
                                        <a href="{{ url('/event') }}" class="card-title font-weight-bolder text-dark-75 text-hover-primary font-size-h4 m-0 pt-7 pb-1">Permohonan Acara</a>
                                        <!--end::Title-->
                                        <!--begin::Text-->
                                        <div class="font-weight-bold text-dark-50 font-size-sm pb-7 text-center">Permohonan Acara di seluruh Putrajaya</div>
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Header-->
                                </div>
                                <!--eng::Container-->
                                <!--begin::Footer-->
                                <div class="d-flex flex-center">
                                    <a href="{{ url('/event') }}" class="btn btn-primary font-weight-bolder font-size-sm py-3 px-14">Lihat Lebih Lanjut</a>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Nav Panel Widget 3-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection


@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Takwim</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Takwim</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/takwim/edit/update',Crypt::encrypt($data['takwim']->id)) }}" id="form" method="post" enctype="multipart/form-data">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Takwim</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Takwim :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="takwim" placeholder="Nama Takwim" value="{{ $data['takwim']->lh_name }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Mula :</label>
                            <div class="col-sm-2">
                                <div class="input-icon">
                                <input type="text" class="form-control text-center" id="kt_datepicker" name="tarikhmula" value="{{ $data['takwim']->lh_start_date }}" placeholder="Tarikh Mula" required/>
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                </div>
                            </div>
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Tamat :</label>
                            <div class="col-sm-2">
                                <div class="input-icon">
                                <input type="text" class="form-control text-center" id="kt_datepicker1" name="tarikhtamat" value="{{ $data['takwim']->lh_end_date }}" placeholder="Tarikh Tamat" required/>
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" {{($data['takwim']->lh_status == 1) ? 'checked="checked"' : ''}} id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    <label class="ml-2 text-muted" id="textbox1">{{ Helper::status($data['takwim']->lh_status) }}</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/takwim') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary btn-confirm font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('location.js.form')
@endsection
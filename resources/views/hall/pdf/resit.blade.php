<style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:10px;
    }
      #title2{
      font-size:8px;
    }
     #footer{
      font-size:8px;
    }
    #back{

   width:100%; 
   height:99%; 
   background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }

    /* Force all children to have a specified font-size */
</style>
<div id="back">
  <table  width="100%" border="0" cellpadding="0">
  <tr>
  <th width="80%" rowspan="5" scope="col"><img src="{{public_path('themes/default/logo_PPj.png')}}" width="323" height="127" />
    <!-- <th width="80%" rowspan="5" scope="col"><img src="{{asset('packages/threef/entree/img/logo_PPj.png')}}" width="323" height="127" /></th> -->
    <th width="20%">&nbsp;</th>
  </tr>
 
</table>
<table id="title" width="100%" border="0" cellpadding="0">
  <tr>
    <td><center><strong><font size="3">PENGESAHAN TEMPAHAN</font></strong></center></td>
  </tr>
</table>
 @if($data->bp_receipt_date < '2018-06-01 00:00::00')
<table id="title" width="100%" border="0" cellpadding="0">
  <tr>
    <td width="9%"><strong><font size="-2">NO. GST :</font></strong></td>
    
    <td width="90%"><strong><font size="-2">00139960320</td>
  </tr>
</table>
@endif
<table id="title" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center">
          <strong>BUTIRAN PENYEWAAN<em></em></strong>
        </td>
      </tr>
</table>
   <br>
    <table id="title" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="15%"><b>{{trans('sales::sales.resitpdf.nosebutharga')}}</b></td>
        <td width="0%">:</td>
        <td width="10%" colspan="2">&nbsp;{{$data->bhQuotation->bq_quotation_no}}</td>

   
        <td width="10%">&nbsp;</td>
        <td width="9%"><b>{{trans('sales::sales.resitpdf.noresit')}}</b></td>
        <td width="1%">:</td>
        <td width="26%">&nbsp;RN{{$data->bp_receipt_number}}</td>

      </tr>
      <tr>
        <td width="15%"><b>{{trans('sales::sales.resitpdf.tarikh')}}</b></td>
        <td>:</td>
        <td colspan="2">&nbsp;{{$data->bhQuotation->bq_quotation_date}}</td>
        <td width="10%">&nbsp;</td>
       <!--  <td width="20%"><b>Tarikh Transaksi</b></td>
        <td width="1%">:</td>
        <td width="26%">&nbsp;{{$data->bp_receipt_date}}</td> -->
        
       
    <!--     <td width="12%">&nbsp;</td>
        <td width="11%"><b>{{trans('sales::sales.resitpdf.online')}}</b></td>
        <td width="1%">:</td> -->
         <!--  @if($fpx=='')
        <td width="26%"></td>
        @else
        <td width="26%">{{$fpx->fpx_serial_no}}</td>
        @endif -->
      </tr>

      <tr>
        <td width="15%"><b>{{trans('sales::sales.resitpdf.notempahan')}}</b></td>
        <td>:</td>
        <td>&nbsp;{{$data->bhQuotation->bhMainbooking->bmb_booking_no}}</td>
        <td width="10%">&nbsp;</td>
        <td width="0%">&nbsp;</td>
        
      </tr>

      <tr>
        <td width="15%"><b>{{trans('sales::sales.resitpdf.nama')}}</b></td>
        <td>:</td>
        <td colspan="4">&nbsp;{{$data->bhMainbooking->bhuser->bhuserdetails->bud_name}} (No.Kp: {{$data->bhMainbooking->bhuser->bhuserdetails->bud_reference_id}})</td>
       
      </tr>
      <tr width="15%">
        <td width="20%"><b>No Tel.</b></td>
        <td width="1%">:</td>
        <td width="26%">&nbsp;{{$data->bhMainbooking->bhuser->bhuserdetails->bud_phone_no}}</td>
      </tr>
  
      <tr>
        <td><b>{{trans('sales::sales.resitpdf.alamat')}}</b></td>
        <td>:</td>
         <td colspan="5">&nbsp;{{$data->bhMainbooking->bhuser->bhuserdetails->bud_address}},<br>
                      &nbsp;{{$data->bhMainbooking->bhuser->bhuserdetails->bud_town}},
                    {{$data->bhMainbooking->bhuser->bhuserdetails->bud_poscode}},
                    {{$data->bhMainbooking->bhuser->bhuserdetails->lkp_state->ls_description}},
                    {{$data->bhMainbooking->bhuser->bhuserdetails->lkp_country->lc_description}}
                  </td>
        
      </tr>
 <tr>
  <td width="20%"><b>Jumlah Bayaran (RM)</b></td>
      <td width="1%">:</td>
      <td width="10%">&nbsp;<?php echo number_format($amount_paid_resit->bp_paid_amount,2);?></td>
 </tr>
      <!-- <tr>
      <td width="13%"><b>{{trans('sales::sales.resitpdf.deposit')}}</b></td>
      <td width="0%">:</td>
      @if($type_payment!=1)
          @if($fpx=='')
          @if($kaunter=='')
          <td width="30%">&nbsp;{{$mainbooking->bmb_deposit_rounding}}</td>
          @else
          <td width="30%">&nbsp;{{$mainbooking->bmb_deposit_rounding}} (No.Rujukan : {{$kaunter->bp_receipt_number}})</td>
          @endif
         
        @else
        <td width="30%">&nbsp;{{$mainbooking->bmb_deposit_rounding}} (No.Rujukan : {{$fpx->fpx_serial_no}})</td>
        @endif
     
      @else
      <td width="10%">&nbsp;{{$mainbooking->bmb_deposit_rounding}}</td>
      @endif
      </tr> -->
      <tr>
         <td width="12%"><b>{{trans('sales::sales.resitpdf.jenistempahan')}}</b></td>
        <td>:</td>
        @if($data->bhQuotation->bhMainbooking->lkpdiskauntype=='')
        <td>&nbsp;</td>
        @else
        <td colspan="4">&nbsp;{{$data->bhQuotation->bhMainbooking->lkpdiskauntype->ldt_user_cat}}</td>
        @endif
      </tr>
      
      <tr>
      <td width="33%"><b>{{trans('sales::sales.resitpdf.namastaff')}}<b></td>
      <td width="1%">:</td>
      <td width="15%" colspan="4">&nbsp;{{$data->bhuser->bhuserdetails->bud_name}}</td>
      </tr>
        <tr>
     <!--  <td width="33%"><b>Cara Bayaran<b></td>
      <td width="1%">:</td>
      @if($carabayaran->id==3)
       <td width="15%" colspan="4">{{$carabayaran->lpm_description}}(Nama Bank : {{$data->nama_bank}}, No Cek : {{$data->no_cek}})</td>
      @else
       <td width="15%" colspan="4">{{$carabayaran->lpm_description}}</td>
      @endif -->
     
      </tr>
    </table>
     @if($data->paymenttype->id ==1)
      <div id="title"><br><b>KETERANGAN :</b></br><b>BAYARAN {{$data->paymenttype->lpt_description}} BAGI PENGGUNAAN DEWAN KOMPLEKS PERBADANAN PUTRAJAYA</b><br>
      <div id="title2"><b>PERHATIAN: Resit Deposit hendaklah disimpan dengan Selamat dan dikemukakan semula apabila membuat tuntutan balik deposit.<br>
        Deposit hendaklah dibayar dikaunter Bayaran Khidmat Pelanggan Perbadanan Putrajaya, Aras 1, Bangunan Annexe, Presint 3, Putrajaya.</b></div>
      @else
      <div id="title"><br><b>KETERANGAN :</b></br><b>BAYARAN {{$data->paymenttype->lpt_description}} BAGI PENGGUNAAN DEWAN KOMPLEKS PERBADANAN PUTRAJAYA</b><br>
      <div id="title2"><b>Bayaran penuh hendaklah dibayar dikaunter Bayaran Khidmat Pelanggan Perbadanan Putrajaya, Aras 1, Bangunan Annexe, Presint 3, Putrajaya.</b></div>
   
      @endif
       <br>
       <div id="title"><b>TUJUAN TEMPAHAN</b></div>
        <br>
         <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
           <thead>
         <th width="2%" align="center"><b>Bil.</b></th>
         <th width="17%"align="center"><b>Item</b></th>
         <th width="12%" align="center"><b>Tarikh Mula</b></th>
         <th width="12%" align="center"><b>Tarikh Tamat</b></th>
         <th width="12%" align="center"><b>Tujuan Tempahan</b></th>

            </thead>
              <?php  
                      $i=1; 
                     
                      ?>
      
           @forelse($tujuan as $key => $value)
           <tr>
            <td width="50px" align="left"><?php echo $i ?></a></td>
             <td width="50px" align="left">{{ $value->bhHall->bh_name }}</a></td>
             <td width="50px" align="left">{{ $value->bb_start_date }}</a></td>
             <td width="50px" align="left">{{ $value->bb_end_date }}</a></td>
              @if($value->bhbookingdetail->lkpevent =='')
             <td width="50px" align="left"></a></td>
             @else
             <td width="50px" align="left">{{ $value->bhbookingdetail->lkpevent->le_description }} - {{$value->bhbookingdetail->bbd_event_name}}</a></td>
             @endif
           </tr>
               <?php $i++;?>
          @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
        @endforelse
   </table>
   <br>
   @if(($check_current_pay_hall->count_hall>0 || $check_current_pay_sport->count_sport>0) 
 || ($check_current_pay_eqp->count_eqp>0 && $check_current_pay_hall->count_hall>0)) 
 
    <div id="title"><b>&nbsp;KADAR SEWAAN DEWAN</b></div>
    <br>
         <table id="title" idth="100%" border="1" cellpadding="2" cellspacing="0">
          <thead>
      <tr style="page-break-inside: avoid">
       
     <th width="4%" align="center"><b>Bil.</b></th>
     <th width="17%" align="center"><b>{{ trans('sales::sales.bayarandetails.item') }}</b></th>
     <th width="12%" align="center"><b>Tarikh Mula</b></th>
     <th width="12%" align="center"><b>Tarikh Tamat</b></th>
     <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.masamula') }}</b></th>
     <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.masatamat') }}</b></th>
     <th width="13%" align="center"><b>{{ trans('sales::sales.bayarandetails.unit') }}</b></th>
     <th width="15%" align="right"><b>{{ trans('sales::sales.bayarandetails.harga') }}</b></th>
    @if($data->bp_receipt_date < '2018-06-01 00:00::00')
     <th width="22%"align="center"><b>{{ trans('sales::sales.bayarandetails.kodgst') }}</b></th>
    @endif
     <!--  <th class="th-sortable active"><b>{{ trans('asset::asset.daftarasset.tindakan') }}</b></th> -->
   </tr>
                  
               </thead>


                    <?php  
                      $i=1;
                      $sumselepas=0;
                      ?>

                        @forelse($listdetaildewan as $key => $value)
                      <tr style="page-break-inside:avoid;">
                      <td width="50px" align="center"><?php echo $i ?></a></td>
                       @if($value->bhHall=='')
                      <td width="200px"></td>
                      @else
                      <td width="200px">{{ $value->bhHall->bh_name }}</td>
                      @endif
                     
                      <td width="200px" align="center"> {{ $value->bhBooking->bb_start_date }}</td>
                      <td width="200px" align="center"> {{ $value->bhBooking->bb_end_date }}</td>
                      <td width="200px" align="center">{{ $value->start_time }}</td>
                      <td width="200px" align="center">{{ $value->end_time }}</td>
                      <td width="200px" align="center">{{ $value->quantity }}</td>
                       <?php $totalselepas=$value->total_amount-$value->gst_amount;
                             
                      ?>
           <td width="200px" align="right"><?php echo number_format($totalselepas,2);?></td>
                      @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                      @if($value->lkpGstRate=='')
                       <td width="200px"></td>
                      @else
                       <td width="200px" align="center">{{ $value->lkpGstRate->lgr_gst_code }}</td>
                      @endif
                      @endif
                     
                     
                     
                      </tr>

                    <?php $i++;?>

         <?php $sumselepas+= $totalselepas;?>

                 @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
                 @endforelse

          

          </table>
            @if($diskaun->fk_lkp_discount_type !=1) 
             <div id="footer">* Selepas diskaun {{$diskaun->lkpdiskauntype->ldt_discount_rate}} %</div>
            @else
             <div>&nbsp;</div>
             @endif      
      
        <table width="60%" id="title"  border="1" align="right"  cellpadding="2" cellspacing="0">
              @if($data->bp_receipt_date < '2018-06-01 00:00::00')
              <tr style="page-break-inside: avoid">
                <td width="50%"><b>{{ trans('sales::sales.bayarandetails.gst') }} </b></td>
                <td width="35%"  align="right"><b>{{$totalsumSRdewan}}</b></td>
                <td width="46%"  align="center"><b>E</b></td>
              </tr>
                <tr style="page-break-inside: avoid">
                <td width="40%" ><b>{{ trans('sales::sales.bayarandetails.gst') }} <b></td>
                <td width="20%" align="right"><b>{{$totalsumSRIdewan}}</b></td>
                <td width="20%" align="center"><b>I</b></td>
                
              </tr>
              @endif
              <tr>
                
                <th width="395px">{{trans('sales::sales.resitpdf.jumdewan')}}</th>
                <td align="right"><b><?php echo number_format($sumselepas,2);?></b></td>
                @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                <td>&nbsp;</td>
                @endif
              </tr>
                    <?php $totalsum= $sumselepas+$totalsumSRdewan ;
                      
                    ?>
              @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                 <tr>
                <th>Jumlah Sewaan Dewan + GST (0%)(RM)</th>
                <td align="right"><b><?php echo number_format($totalsum,2);?></b></td>
                <td>&nbsp;</td>
              </tr>
              @endif
               <tr>
                <th scope="row" width="395px">Jumlah Deposit telah dibayar (RM)</th>
                <td align="right"><b><?php echo number_format($mainbooking->bmb_deposit_rounding,2);?></b></td>
                @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                <td>&nbsp;</td>
                @endif
              </tr>
           <tr>
                <th scope="row">&nbsp;</th>
                <td align="right"><b>&nbsp;</b></td>
                @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                <td>&nbsp;</td>
                @endif
              </tr>
        </table>
           @endif
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
         @if($data->fk_lkp_payment_type!=1)<!--start kalau deposit peraltan x keluar-->
      
       @if(($check_current_pay_hall->count_hall>0 && $check_current_pay_eqp->count_eqp>0)
    ||($check_current_pay_hall->count_hall==0 && $check_current_pay_eqp->count_eqp>0)
    ||($check_current_pay_sport->count_sport>0 && $check_current_pay_eqp->count_eqp>0)
    ||($check_current_pay_sport->count_sport==0 && $check_current_pay_eqp->count_eqp>0))

   <div id="title"><b>&nbsp;KADAR SEWAAN PERALATAN</b></div>
            <br>
            <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr style="page-break-inside: avoid">
             <th width="4%" align="center"><b>Bil.</b></th>
             <th width="17%" align="center"><b>{{ trans('sales::sales.bayarandetails.item') }}</b></th>
             <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.tarikh') }}</b></th>
             <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.masamula') }}</b></th>
             <th width="12%" align="center"><b>{{ trans('sales::sales.bayarandetails.masatamat') }}</b></th>
             <th width="13%" align="center"><b>Kos Seunit (RM)</b></th>
             <th width="13%" align="center"><b>{{ trans('sales::sales.bayarandetails.unit') }}</b></th>
             <th width="15%" align="right"><b>{{ trans('sales::sales.bayarandetails.harga') }}</b></th>
             @if($data->bp_receipt_date < '2018-06-01 00:00::00')
             <th width="22%"align="center"><b>{{ trans('sales::sales.bayarandetails.kodgst') }}</b></th>
             @endif
             
              </tr>
               <?php  
                      $i=1; 
                      $sumselepaseqp=0;
                      ?>
        @forelse($listdetailequep as $key => $value)
                      <tr style="page-break-inside:avoid;">
                      <td width="50px" align="center"><?php echo $i ?></a></td>
                      @if($value->bhEquipment=='')
                      <td width="200px"></td>
                      @else
                      <td width="200px" align="left">{{ $value->bhEquipment->be_name }}</td>
                      @endif
                      
                      <td width="200px" align="center"> {{ $value->booking_date }}</td>
                      <td width="200px" align="center">{{ $value->start_time }}</td>
                      <td width="200px" align="center">{{ $value->end_time }}</td>
                      <td width="200px" align="center">{{ $value->harga }}</td>
                      <td width="200px" align="center">{{ $value->quantity }}</td>
                     <?php  if($value->fk_lkp_gst_rate==1){
                        $totalselepaseqp=$value->total_amount-$value->gst_amount;

                      }else{
                        $totalselepaseqp=$value->total_amount;
                      } ?>
                      <td width="200px" align="right"><?php echo number_format($totalselepaseqp,2);?></td>
                      
                      @if($data->bp_receipt_date < '2018-06-01 00:00::00')
                       @if($value->lkpGstRate=='')
                       <td width="200px"></td>
                      @else
                       <td width="200px" align="center">{{ $value->lkpGstRate->lgr_gst_code }}</td>
                      @endif
                      @endif
                      
                      </tr>

                    <?php $i++;?>
                     <?php $sumselepaseqp+= $totalselepaseqp;?>

                 @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
                 @endforelse
        </table>

         <br>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
          @if($data->bp_receipt_date < '2018-06-01 00:00::00')
          <tr style="page-break-inside: avoid">
                <th width="50%"><b>{{ trans('sales::sales.bayarandetails.gst') }} </b></th>
                  @if($totalsumSReqp=='')
                    <th width="36%" align="right">0.00</th>
                @else
                    <th width="36%" align="right">{{$totalsumSReqp}}</th>
                @endif
                <th width="46%" align="center"><b>E</b></th>
              </tr>
                <tr style="page-break-inside: avoid">
                <th width="50%" ><b>{{ trans('sales::sales.bayarandetails.gst') }} </b></th>
                @if($totalsumSRIeqp=='')
                     <th width="36%" align="right">0.00</th>
                  @else
                     <th width="36%" align="right">{{$totalsumSRIeqp}}</th>
                  @endif
                <th width="46%" align="center"><b>I</b></th>
                
              </tr>
        @endif
          <tr style="page-break-inside:avoid;">
     
            <th scope="row" width="375px">{{trans('sales::sales.resitpdf.jumitem')}}</th>

               @forelse($listdetailequep as $key => $value)
                <?php $totalselepaseqp=$totalselepaseqp;?>

                 @empty
                      <?php $totalselepaseqp=0.00;?>
                 @endforelse

            <td align="right"><b><?php echo number_format(($sumselepaseqp),2)?></b></td>
           @if($data->bp_receipt_date < '2018-06-01 00:00::00')
            <td>&nbsp;</td>
           @endif
          </tr>
                     <?php $totalsumeqp= $sumselepaseqp+$totalsumSReqp;
                

           ?>
           @if($data->bp_receipt_date < '2018-06-01 00:00::00')
            <tr>
                <th scope="row">Jumlah Sewaan Peralatan + GST (0%)(RM)</th>
                 @if($totalsumeqp=='')
                <td align="right"><b>0.00</b></td>
                @else
                <td align="right"><b><?php echo number_format($totalsumeqp,2);?></b></td>
                @endif
                <td>&nbsp;</td>
              </tr>
            @endif
               <tr><td colspan="3">&nbsp;</td></tr>
          <tr style="page-break-inside:avoid;">

        </table>


        @endif

       @endif <!--start kalau deposit peraltan x keluar-->
         <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
   
          <tr>
          <td width="375px"><b>Jumlah Kesuluruhan ( RM )</b></td>

           @if($data->fk_lkp_payment_type!=1)
             @if($amount_paid_resit=='')
              <td align="right" colspan="2"><b></b></td>
             @else
              <td align="right" colspan="2"><b><?php echo number_format(($amount_paid_resit->bp_paid_amount),2)?></b></td>
             @endif

           @else<!--depposit-->
              <td align="right" colspan="2"><b>{{$mainbooking->bmb_total_book_hall}}</b></td>
          
              @endif
            

          </tr>
             <tr style="page-break-inside:avoid;">
                      <td width="100px"><b>Pengenapan</b></td>
                      <?php $pengenapan=($mainbooking->bmb_subtotal-$mainbooking->bmb_rounding)?>
                      <td  align="right" colspan="2"><b><?php echo number_format($pengenapan,2)?></b></td>
                    
            </tr>

              @if($data->fk_lkp_payment_type!=1)
         <tr style="page-break-inside:avoid;">
                      <td width="88px"><b>Jumlah Keseluruhan Telah Dibayar (RM)</b></td>
                      <?php $jumlahseluruh=$amount_paid-$bhpaymentquo->bp_paid_amount;?>
                      <td width="100px" align="right" colspan="2"><b><?php echo number_format($amount_paid_resit->bp_paid_amount,2);?></b></td>

                  
                      </tr>
               @endif

             </table>
                 <p>&nbsp;</p>

         <!--      <div style='page-break-after:always;'>&nbsp;</div> -->

    <table id="title" width="100%" border="0" align="right"  cellpadding="2" cellspacing="0">
    <tr>
      <td>
      </td>
    </tr>
    </table>

               <?php 

                      if($gstSR==''){

                           $gstSR=0.00;

                      }else{
                           
                           $gstSR=$gstSR->gst;

                      }


                       if($gstSRI==''){

                           $gstSRI=0.00;

                      }else{
                           
                           $gstSRI=$gstSRI->gst;

                      }
                          

                      ?>

                  <?php 

                      if($gsteqpSR==''){

                           $gsteqpSR=0.00;

                      }else{
                           
                           $gsteqpSR=$gsteqpSR->gst;

                      }


                       if($gsteqpSRI==''){

                           $gsteqpSRI=0.00;

                      }else{
                           
                           $gsteqpSRI=$gsteqpSRI->gst;

                      }
                          

                      ?>

 

 @if($data->bp_receipt_date < '2018-06-01 00:00::00')
           <table id="title" width="55%" border="1" align="right"  cellpadding="2" cellspacing="0">
          <tr style="page-break-inside: avoid">
                <th width="46%" align="center">Analisis GST</th>
                <th width="27%" scope="col"></th>
                <th width="46%" scope="col"></th>
          </tr>
          <tr style="page-break-inside: avoid">
                <th width="46%" align="center">Kod Cukai</th>
                <th width="27%" align="right">Jumlah Nilai (RM)</th>
                <th width="46%" align="right">Jumlah GST (RM)</th>
          </tr>
            <tr style="page-break-inside: avoid">
            <th align="center">E</th>
             @if($data->fk_lkp_payment_type==1)
            <td align="right"><b><?php echo number_format(($gsteqpSR_resit_depo),2)?></b></td>
            <td align="right"><b><?php echo number_format(($sumeqpSR_resit_depo),2)?></b></td>
            @else
            <td align="right"><b><?php echo number_format(($gsteqpSR_resit),2)?></b></td>
            <td align="right"><b><?php echo number_format(($sumeqpSR_resit),2)?></b></td>
            @endif
            

               </tr>
                 <tr style="page-break-inside: avoid">
            <th align="center">I</th>

             @if($data->fk_lkp_payment_type==1)
             <td align="right"><b><?php echo number_format(($gsteqpSRI_resit_depo),2)?></b></td>
              <td align="right"><b><?php echo number_format(($sumeqpSRI_resit_depo),2)?></b></td>
             @else
             <td align="right"><b><?php echo number_format(($gsteqpSRI_resit),2)?></b></td>
             <td align="right"><b><?php echo number_format(($sumeqpSRI_resit),2)?></b></td>
             @endif
            

          </tr> 
        </table>
        @endif
            


         
         @if($data->fk_lkp_payment_type==1)
       
          <table id="title"width="100%" border="0" cellpadding="0">
          <tr style="page-break-inside: avoid">
            <td colspan="2">NOTA :</td>
          </tr>
          <tr style="page-break-inside: avoid">
            <td width="2%">a)</td>
            <td width="98%">Bayaran Deposit sebanyak {{$data->bp_deposit}} adalah TIDAK TERMASUK jumlah bayaran tempahan.</td>
          </tr>
          <tr style="page-break-inside: avoid">
            <td>b)</td>
            <td>Kegagalan membayar jumlah tempahan dalam tempoh empat belas (14) hari sebelum tarikh penggunaan akan menyebabkan tempahan anda dibatalkan.</td>
          </tr>
          <tr style="page-break-inside: avoid">
            <td>c)</td>
            <td>Resit Deposit/ Cagaran hendaklah disimpan dengan selamat dan dikemukakan semula apabila membuat tuntutan balik deposit/ cagaran.</td>
          </tr>
          @if($data->fk_lkp_payment_type == 1)
           <tr style="page-break-inside: avoid">
            <td>d)</td>
            <td>Bayaran penuh sebanyak RM {{$data->bp_total_amount}} hendaklah dibuat selepas bayaran deposit dibuat dan sekurang-kurangnya satu (1) Bulan sebelum tarikh pengggunaan dewan</td>
          </tr>
          @endif
        </table>
         @endif
        <p>&nbsp;</p>
        <table id="footer" width="100%" border="0" cellpadding="10">
          <tr>
            <td><div align="center" style="text-align:center">Resit ini cetakan komputer, tiada tandatangan diperlukan.</div></td>
          </tr>
        </table>

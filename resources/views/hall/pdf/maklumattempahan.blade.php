<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN MAKLUMAT TEMPAHAN KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN MAKLUMAT TEMPAHAN KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                {{-- <th class="th-sortable active"><b>Jenis Tempahan</b></th> --}}
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Nama Dewan</b></th>
                <th class="th-sortable active"><b>Acara</b></th>
                <th class="th-sortable active"><b>Tarikh Tempah Dari</b></th>
                <th class="th-sortable active"><b>Tarikh Tempah Hingga</b></th>
                <th class="th-sortable active"><b>Masa Dari</b></th>
                <th class="th-sortable active"><b>Masa Hingga</b></th>
                <th class="th-sortable active"><b>Peralatan</b></th>
            </tr>
        </thead>
        <tbody>
          @php
              $i = 1; 
              $total = 0;
          @endphp
          @if (count($data['slot']) > 0)
          @for ($j = 0; $j < count($data['slot']); $j++)
              <tr>
                  <td style="width: 5%;">{{ $i++ }}</td>
                  <td>{{ Helper::get_nama(Helper::get_userTempahan($data['slot'][$j]->fk_main_booking)) }}</td>
                  <td>{{ Helper::get_noTempahan($data['slot'][$j]->fk_main_booking) }}</td>
                  <td>{{ Helper::getHall($data['slot'][$j]->fk_bh_hall) }}</td>
                  <td>{{ ($data['bh_booking_detail'][$j]->bbd_event_name) }}</td>
                  <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_start_date) }}</td>
                  <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_end_date) }}</td>
                  <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_start_time) }}</td>
                  <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_end_time) }}</td>
                  <td style="white-space: nowrap">
                      @if (isset($data['bh_booking_equipment'][$data['slot'][$j]->id]))
                          @foreach ($data['bh_booking_equipment'][$data['slot'][$j]->id] as $bbe)
                              {{ Helper::get_equipment_hall($bbe->fk_bh_equipment) }}<br>
                          @endforeach
                      @endif
                  </td>
              </tr>
          @endfor
      @endif
          {{-- @foreach ($data['slot'] as $s)
              <tr> --}}
                  {{-- <td style="width: 5%;">{{ $i++ }}</td>
                  <td>{{ $s->fullname }} <br>({{ $s->bud_phone_no }}) <br>{{ $s->email }}</td>
                  <td><a target="_blank" href="{{ url('sport/maklumatbayaran', Crypt::encrypt($s->fk_main_booking)) }}">{{ $s->bmb_booking_no }}</a></td>
                  <td>{{ $s->ebf_start_date }}</td>
                  <td>{{ $s->efd_name }}</td>
                  <td>
                      @foreach ($data['umum'] as $u)
                          @if ($u->fk_et_booking_facility == $s->id)
                              <li>{{ $u->est_slot_time }}</li>
                          @endif
                      @endforeach
                  </td> --}}
                  {{-- <td>{{ $i++ }}</td>
                  <td>{{ $s->bud_name }} <br>@if ($s->bud_phone_no != '' && $s->bud_phone_no != null) ({{ $s->bud_phone_no }}) @endif</td>
                  <td>{{ Helper::get_nama(Helper::get_userTempahan($data['slot']->fk_main_booking)) }}</td>
                  <td>{{ Helper::get_noTempahan($data['slot']->fk_main_booking) }}</td> --}}

                  {{-- <td>
                      @if($s->internal_indi == 1)
                          Tempahan Rasmi
                      @elseif($s->internal_indi == 2)
                          Sekatan Tempahan
                      @else
                          Tempahan Awam
                      @endif
                  </td>
                  <td>{{ $s->bmb_booking_no }}</td>
                  <td>{{ $s->booking_date }}</td>
                  <td>{{ $s->efd_name }}</td>
                  @php
                      $eqData = Helper::get_equipment_book($s->id);
                  @endphp
                  <td>
                      @foreach ($eqData as $val)
                          {{  $val->ee_name  }} - {{  $val->eeb_quantity  }} ( RM {{$val->eeb_subtotal}} )<br>
                      @endforeach
                  </td>
                  <td>
                      @php
                          $slotData = Helper::report($s)
                      @endphp
                      @foreach ($slotData as $slot)
                          {{ $slot->est_slot_time }}<br>
                      @endforeach
                  </td> --}}
              {{-- </tr>
          @endforeach --}}
      </tbody>
    </table>
</body>
</html>

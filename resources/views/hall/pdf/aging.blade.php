<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        @page {
            size: landscape;
            margin: 1cm;
        }
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    

    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN KUTIPAN BAYARAN TERTUNGGAK KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN KUTIPAN BAYARAN TERTUNGGAK KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Nama Dewan</b></th>
                <th class="th-sortable active"><b>Acara</b></th>
                <th class="th-sortable active"><b>Tarikh Tempahan Dari</b></th>
                <th class="th-sortable active"><b>Tarikh Tempahan Hingga</b></th>
                <th class="th-sortable active"><b>Masa Dari</b></th>
                <th class="th-sortable active"><b>Masa Hingga</b></th>
                <th class="th-sortable active"><b>Peralatan</b></th>
                <th class="th-sortable active"><b>Status</b></th>
                <th class="th-sortable active"><b>Jumlah Bayaran (RM)</b></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;  
                $total = 0;
            @endphp
            @if (count($data['slot']) > 0)
                @for ($j = 0; $j < count($data['slot']); $j++)
                    <tr>
                        <td style="width: 5%;">{{ $i++ }}</td>
                        {{-- <td>{{ Helper::get_nama(Helper::get_userTempahan($data['slot'][$j]->fk_main_booking)) }}</td> --}}
                        <td>{{ Helper::get_nama(Helper::get_userTempahan($data['slot'][$j]->id)) }}</td>
                        {{-- <td>{{ Helper::get_noTempahan($data['slot'][$j]->fk_main_booking) }}</td> --}}
                        <td>{{ Helper::get_noTempahan($data['slot'][$j]->id) }}</td>
                        {{-- <td>{{ Helper::getHall($data['slot'][$j]->fk_bh_hall) }}</td> --}}
                        <td>{{ Helper::getHall($data['bh_booking'][$j]->fk_bh_hall) }}</td>
                        <td>{{ ($data['bh_booking_detail'][$j]->bbd_event_name) }}</td>
                        {{-- <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_start_date) }}</td> --}}
                        <td class="text-center">{{ Helper::date_format($data['bh_booking'][$j]->bb_start_date) }}</td>
                        {{-- <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_end_date) }}</td> --}}
                        <td class="text-center">{{ Helper::date_format($data['bh_booking'][$j]->bb_end_date) }}</td>
                        <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_start_time) }}</td>
                        <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_end_time) }}</td>
                        <td style="white-space: nowrap">
                            @if (isset($data['bh_booking_equipment'][$data['slot'][$j]->id]))
                                @foreach ($data['bh_booking_equipment'][$data['slot'][$j]->id] as $bbe)
                                    {{ Helper::get_equipment_hall($bbe->fk_bh_equipment) }}<br>
                                @endforeach
                            @endif
                        </td>
                        <td class="text-center">{{ Helper::get_status_tempahan(Helper::get_main_booking($data['slot'][$j]->id)->fk_lkp_status) }}</td>
                        <td align="right" class="text-right">{{ $data['slot'][$j]->subtotal }}</td>
                    </tr>
                    @php
                    $total += ($data['slot'][$j]->subtotal);
                    @endphp
                @endfor
            @else
            <tr>
                <td colspan="12" class="text-center text-danger">Harap Maaf,data tiada dalam sistem bagi tarikh tersebut!</td>
            </tr>
            @endif
            <tr class="bg-dark">
                <td align="right" colspan="11" class="text-right font-weight-bolder text-white">Jumlah Keseluruhan (RM)</td>
                {{-- <td class="text-right font-weight-bolder text-white">{{ number_format($total , 2, '.', '') }}</td> --}}
                <td align="right" class="text-right font-weight-bolder text-white">{{ number_format($total , 2, '.', ',') }}</td>
            </tr>
        </tbody>
        {{-- <tfoot>
            <tr class="bg-dark">
                <td colspan="11" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                {{-- <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td> 
                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', ',') }}</td>
            </tr>
        </tfoot> --}}
    </table>
</body>
</html>

<?php use App\Helpers\Helper as HP;?>
<style>
    /*Assumed everything is inheriting font-size*/
    #title{
      font-size:12px;
    }
    #footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            text-align: center;
            padding: 10px;
            border: 0;
            font-size: 10px;
        }
    #back{
       width:100%; 
       height:99%;
       background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
       background-repeat: no-repeat;
       background-position: center;
       /*background-attachment:fixed;*/
    }
    /* Force all children to have a specified font-size */
</style>
          <div id="back">
            <table  width="100%" border="0" cellpadding="0">
              <tr>
                <th width="20%" rowspan="5" scope="col"><img src="{{public_path('assets/media/logos/logo_PPj.png')}}" width="323" height="127" /></th>
                <!--<th width="80%" rowspan="5" scope="col"><img src="{{asset('packages/threef/entree/img/logo_PPj.png')}}" width="323" height="127" /></th>-->
                <th width="80%">&nbsp;</th>
              </tr>
            </table>
            <table id="title" width="100%" border="0" cellpadding="0">
              <tr>
                <td><center><strong>SEBUTHARGA / QUOTATIONS</strong></center></td>
              </tr>
            </table>
            <table id="title" width="100%" border="0" cellpadding="0">
              <tr>
                <td width="9%"><strong><font size="-2">NO. GST :</font></strong></td>
                <td width="90%"><strong><font size="-2">00139960320</td>
              </tr>
            </table>
              <table id="title" width="100%" border="1" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center">
                    <strong>BUTIRAN PENYEWAAN<em></em></strong>
                  </td>
                </tr>
              </table>
               <br>
                <table id="title" width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="10%"><b>No. Sebutharga</b></td>
                  <td width="2%">:</td>
                  <td width="28%">&nbsp;{{$data['bh_quotation']->bq_quotation_no}}</td>
                </tr>
                <tr>
                  <td width="12%"><b>Tarikh Sebutharga</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['bh_quotation']->bq_quotation_date}}</td>
                </tr>
                <tr>
                  <td width="12%"><b>No Tempahan</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['getbmbno']->bmb_booking_no}}</td>
                </tr>
                <tr>
                  <td width="12%"><b>Nama Pemohon</b></td>
                  <td>:</td>
                @if($data['getbmbno']->internal_indi=='')
                 <td>&nbsp;{{$data['user_detail']->bud_name}}</td>
                @else
                 <td>&nbsp;{{$data['bh_booking_detail'][0]->bbd_user_apply}}</td>
                @endif
                </tr>
                @if($data['getbmbno']->internal_indi=='')
                <tr>
                  <td width="12%"><b>Alamat Pemohon</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['user_detail']->bud_address}},<br>
                      &nbsp;{{$data['user_detail']->bud_town}},
                    {{$data['user_detail']->bud_poscode}},
                    {{HP::negeri($data['user_detail']->fk_lkp_state)}},
                    {{HP::negara($data['user_detail']->fk_lkp_country)}}
                  </td>
                </tr>
               @endif
                 <tr>
                  <td width="12%"><b>Jenis Tempahan</b></td>
                  <td>:</td>
                   @if($data['getbmbno']->fk_lkp_discount_type=='')
                  <td>&nbsp;</td>
                  @else
                  <td>&nbsp;{{HP::getJenisTempahan($data['getbmbno']->fk_lkp_discount_type)}}</td>
                  @endif
                </tr>
                  <tr>
                  <td width="12%"><b>Jumlah Tempahan (RM)</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['getbmbno']->bmb_subtotal}}</td>
                </tr>
                  <tr>
                  <td width="12%" ><b>Deposit (RM)</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['getbmbno']->bmb_deposit_rm}}</td>
                </tr>
                 @if($data['getbmbno']->internal_indi!='')
                  <tr>
                  <td width="12%" ><b>Nama Pegawai</b></td>
                  <td>:</td>
                  <td>&nbsp;{{$data['bh_detail'][0]->bud_name}}</td>
                </tr>
                 @endif
              </table>
              <hr>
        <div id="title"><b>KETERANGAN :</b><br><b>SEBUTHARGA BAGI PENGGUNAAN DEWAN KOMPLEKS PERBADANAN PUTRAJAYA</b></div>
        <br>
        <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
           <thead>
         <th width="1%" align="center"><b>Bil.</b></th>
         <th width="17%"align="center"><b>Item</b></th>
        <th width="12%" align="center"><b>Tarikh Mula</b></th>
         <th width="12%" align="center"><b>Tarikh Tamat</b></th>
         <th width="12%" align="center"><b>Tujuan Tempahan</b></th>
            </thead>
              <?php  
                      $i=1; 
                      ?>
           @forelse($data['bh_booking'] as $value)
           <tr>
            <td width="5px" align="center"><?php echo $i ?></a></td>
             <td width="50px" align="left">{{ HP::getHall($value->fk_bh_hall) }}</a></td>
             <td width="50px" align="center">{{ HP::date_format($value->bb_start_date) }}</a></td>
             <td width="50px" align="center">{{ HP::date_format($value->bb_end_date) }}</a></td>
             @if($data['bh_booking_detail'][0]->bbd_event_name =='')
             <td width="50px" align="left"></a></td>
             @else
             <td width="50px" align="left">{{ Helper::get_lkp_event($data['bh_booking_detail'][0]->fk_lkp_event) }}</a></td>
             @endif
           </tr>
               <?php $i++;?>
             @empty
                      <tr><td colspan='10'>{{ trans('sales::sales.nodata') }}</td></tr>
                 @endforelse
   </table>
   <br>
   <hr>
    <div id="title"><b>&nbsp;KADAR SEWAAN DEWAN</b></div>
    <br>
         <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
          <thead>
    <tr style="page-break-inside: avoid">
     <th width="4%" align="center"><b>Bil.</b></th>
     <th width="17%"align="center"><b>Nama Dewan</b></th>
     <th width="12%" align="center"><b>Tarikh Mula</b></th>
     <th width="12%" align="center"><b>Tarikh Tamat</b></th>
     <th width="12%" align="center"><b>Masa Mula</b></th>
     <th width="12%" align="center"><b>Masa Tamat</b></th>
     <th width="13%" align="center"><b>Kuantiti</b></th>
     <th width="22%" align="right"><b>Harga (RM)</b></th>
   </tr>
                </thead>
                  @php 
                    $i=1; 
                    $sumselepas=0;
                  @endphp
                  @foreach ($data['bh_booking'] as $value)
                    <tr style="page-break-inside:avoid;">
                      <td>{{ $i++ }}</td>
                      <td>{{ HP::getHall($value->fk_bh_hall) }}</td>
                      <td align="center">{{ HP::date_format($value->bb_start_date) }}</td>
                      <td align="center">{{ HP::date_format($value->bb_end_date) }}</td>
                      <td align="center">{{ ($data['bh_booking_detail'][$i-2]->bbd_start_time) }}</td>
                      <td align="center">{{ ($data['bh_booking_detail'][$i-2]->bbd_end_time) }}</td>
                      <td align="center">1</td>
                      <td align="right">{{ ($value->hall_price) }}</td>
                    </tr>
                  @endforeach
              </table>
            @if($data['getbmbno']->fk_lkp_discount_type !=1) 
              {{-- <!-- <div id="footer">* Selepas diskaun {{$diskaun->lkpdiskauntype->ldt_discount_rate}} %</div>--> --}}
              <div>&nbsp;</div>
            @else
              <div>&nbsp;</div>
            @endif      
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
              <tr>
                <th align="left" scope="row">Jumlah Sewaan Dewan (RM)</th>
                <td align="right"><b>{{ number_format($data['getbmbno']->bmb_total_book_hall,2) }}</b></td>
              </tr>
                    <?php // $totalsum= $sumselepas+$totalsumSRdewan ; ?>
                 <!--<tr>
                <th scope="row">Jumlah Sewaan Dewan + GST (0%)(RM)</th>
                <td align="right"><b><?php // echo number_format($totalsum,2);?></b></td>
                <td>&nbsp;</td>
              </tr>-->
               <tr>
                @if($data['getbmbno']->fk_lkp_status==9)
                  <th scope="row">Jumlah Deposit telah dibayar (RM)</th>
                @else
                  <th align="left" scope="row">Jumlah Deposit (RM)</th>
                @endif
                <td align="right"><b>{{number_format($data['getbmbno']->bmb_deposit_rm,2)}}</b></td>
              </tr>
        </table>
        <br>
   <hr>
            <div id="title"><b>&nbsp;KADAR SEWAAN PERALATAN</b></div>
            <br>
            <table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
             <tr style="page-break-inside: avoid">
             <th width="2%" align="center"><b>Bil.</b></th>
             <th width="17%" align="center"><b>Nama Dewan</b></th>
             <th width="17%" align="center"><b>Item</b></th>
             <th width="12%" align="center"><b>Tarikh</b></th>
             {{-- <th width="10%" align="center"><b>Masa Mula</b></th>
             <th width="10%" align="center"><b>Masa Tamat</b></th> --}}
             <th width="13%" align="center"><b>Kos Seunit (RM)</b></th>
             <th width="10%" align="center"><b>Kuantiti</b></th>
             <th width="10%" align="center"><b>Harga (RM)</b></th>
             <!--<th width="22%" align="center"><b>{{ trans('sales::sales.bayarandetails.kodgst') }}</b></th>-->
              </tr>
               <?php  
                      $i=1;
                      $sumselepaseqp=0;
                      $totalsumeqp=0;
                      ?>
               @forelse($data['bh_booking_equipment'] as $value)
                      <tr style="page-break-inside:avoid;">
                      <td width="1%" style="text-align: center"><?php echo $i ?></a></td>
                       @if($data['bh_booking'][0]->fk_bh_hall =='')
                      <td width="10%"></td>
                      @else
                      <td width="10%">{{ HP::getHall($data['bh_booking'][0]->fk_bh_hall) }}</td>
                      @endif
                      @if($value->fk_bh_equipment=='')
                      <td width="10%"></td>
                      @else
                      <td width="10%">{{ HP::get_equipment_hall($value->fk_bh_equipment) }}</td>
                      @endif
                      <td width="10%" align="center"> {{ $value->bbe_booking_date }}</td>
                      <td width="10%" align="center">{{ number_format($value->bbe_price,2) }}</td>
                      {{-- <td width="100px" align="center">{{ $value->bbd_start_time }}</td>
                      <td width="100px" align="center">{{ $value->bbd_end_time }}</td> --}}
                      {{-- <td width="200px" align="center">{{ number_format($value->total_amount/$value->bbe_quantity,2) }}</td> --}}
                      <td width="10%" align="center">{{ $value->bbe_quantity }}</td>
                      <?php 
                        // if($value->fk_lkp_gst_rate==1){
                        //   $totalselepaseqp=$value->total_amount;
                        // }else{
                          $totalselepaseqp= $value->bbe_price * $value->bbe_quantity;
                          $totalsumeqp += $totalselepaseqp;
                        // } 
                        // if($totalselepaseqp==''){
                        //   $totalselepaseqp=0.00;
                        // }else{
                        //   $totalselepaseqp=$totalselepaseqp;
                        // }
                      ?>
                      <td width="100px" align="right"><?php echo number_format($totalselepaseqp,2);?></td>
                       {{-- @if($value->lkpGstRate=='')
                       <!--<td width="200px"></td>-->
                      @else
                       <!--<td width="200px" align="center">{{ $value->lkpGstRate->lgr_gst_code }}</td>-->
                      @endif --}}
                      </tr>
                    <?php $i++;?>
                     <?php // $sumselepaseqp+= $totalselepaseqp;?>
                 @empty
                      <tr><td colspan='10'></td></tr>
                 @endforelse
        </table>
        <br>
        <table id="title" width="60%" border="1" align="right"  cellpadding="2" cellspacing="0">
          <tr style="page-break-inside:avoid;">
            <th scope="row" align="left">Jumlah Sewaan Peralatan (RM)</th>
              <td align="right"><b><?php echo number_format(($totalsumeqp),2)?></b></td>
           <td>&nbsp;</td>
          </tr>
            <tr style="page-break-inside:avoid;">
                      <td width="100px"><b>Pengenapan</b></td>
                      <?php $pengenapan=($data['getbmbno']->bmb_rounding)?>
                      <td width="100px" align="right"><b><?php echo number_format($pengenapan ?? 0,2)?></b></td>
                      <td width="100px"></td>
                      </tr>
          @if($data['getbmbno']->fk_lkp_status==9)
                <tr style="page-break-inside:avoid;">
                  <td width="100px"><b>Jumlah Telah Dibayar (RM)</b></td>
                  <td width="100px" align="right"><b>{{ number_format($data['amount_paid_fpx'] ?? 0, 2) }}</b></td>
                  <td width="100px"></td>
              </tr>
              <tr style="page-break-inside:avoid;">
                <td width="100px" style="white-space: nowrap"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                <?php $jumbayar=($data['getbmbno']->bmb_subtotal)?>
                <td width="100px" align="right"><awb><?php echo number_format($bayaranpenuh,2)?></b></td>
                <td width="100px"></td>
              </tr>
          @else
          <tr style="page-break-inside:avoid;">
            <td width="100px"><b>Jumlah Telah Dibayar(RM)</b></td>
            <td width="100px" align="right"><b><?php echo number_format($data['amount_paid_fpx'] ?? 0, 2)?></b></td>
            <td width="100px"></td>
          </tr>
             <tr style="page-break-inside:avoid;">
                      <td width="200px" style="white-space: nowrap"><b>Jumlah Keseluruhan Perlu Dibayar(RM)</b></td>
                      <?php $jumbayar=($data['getbmbno']->bmb_subtotal)?>
                      <td width="100px" align="right"><b><?php echo number_format($jumbayar,2)?></b></td>
                      <td width="100px"></td>
                      </tr>
          @endif
        </table>
        <br>
      <div align="center" id="footer" width="100%" border="0" cellpadding="10">
        <hr>
          <h2 style="text-align: center;">*<u>Sebut harga ini cetakan komputer, tiada tandatangan diperlukan.</u>*</h2>
      </div>

  
    

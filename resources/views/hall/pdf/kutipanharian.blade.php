<!DOCTYPE html>
<html>
<head>
    <style>
        #body {
            font-size: 10px;
        }
    </style>
</head>
<body>
    <div align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div align="center">
        @if(isset($data['id']) && $data['id']!='')
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN {{ strtoupper(Helper::location($data['id'])) }}   </b>
        @else
        <b>LAPORAN KUTIPAN HARIAN KOMPLEKS SUKAN</b>
        @endif
    </div>
    <div>&nbsp;</div>
    <table style="width:100%;padding-top:50px;text-align:center">
        <tr>
            <td style="width:48%;border:0px" id="body">
                @if(isset($data['mula'])) 
                @if($data['mula']!='')
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d F Y', strtotime($data['mula']))) }}</b>
                @endif
                @else
                <b>TARIKH KUTIPAN : {{ strtoupper(date('d/m/Y')) }}</b>
                @endif
            </td>
            <td style="width:48%;text-align:center;border:0px" id="body">
                @if(isset($data['mula'])) 
                @if($data['mula']!='')
                <b>HARI KUTIPAN : {{ strtoupper($data['dayOfWeek']) }}</b>
                @endif
                @else
                <b>HARI KUTIPAN : {{ "" }}</b>
                @endif
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body"> 
        <thead>
            <tr>
                <th class="th-sortable active"><b>Bil.</b></th>
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>Nama Dewan / Kelengkapan</b></th>
                <th class="th-sortable active"><b>Kod Fee</b></th>
                <th class="th-sortable active"><b>Tarikh Transaksi</b></th>
                <th class="th-sortable active"><b>No. Resit</b></th>
                <th class="th-sortable active"><b>No. Ruj Transaksi</b></th>
                <th class="th-sortable active"><b>Cara Bayaran</b></th>
                <th class="th-sortable active">Jumlah (RM)</th>
                <th class="th-sortable active">Jumlah Keseluruhan (RM)</th>
            </tr>
        </thead>
        <tbody>
          @php
              $i = 1;  
              $total = 0;
          @endphp
          @if (count($data['slot']) > 0)
          @foreach ($data['slot'] as $s)
              <tr>
                  <td style="width: 5%;" align="center">{{ $i++ }}</td>
                  <td class="mx-2">{{ $s->bmb_booking_no }}</td>
                  <td>{{ $s->fullname }}</td>
                  <td>{{ $s->bh_name }}</td>
                  <td>{{ $s->bh_code }}</td>
                  <td>{{ $s->bp_receipt_date }}</td>
                  <td>{{ $s->bp_receipt_number }}</td>
                  <td>{{ $s->bp_payment_ref_no }}</td>
                  <td>{{ $s->lpm_description }}</td>
                  <td align="right">{{ $s->bp_total_amount }}</td>
                  <td align="right">{{ $s->bp_total_amount }}</td>
              </tr>
              @php
                  $total += $s->bp_total_amount;
              @endphp
          @endforeach
          @else
          <tr>
              <td colspan="11" class="text-center text-danger">Harap Maaf, tiada data untuk tarikh tersebut!</td>
          </tr>
          @endif
      </tbody>
          <tfoot>
              <tr class="fw-bold fs-6 bg-dark">
                  <td colspan="9" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                  <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', ',') }}</td>
                  <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', ',') }}</td>
              </tr>
          </tfoot>
      </table>
  </body>
  </html>
  
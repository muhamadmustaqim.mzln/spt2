<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        @page {
            size: landscape;
            margin: 1cm;
        }
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    

    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN MAKLUMAT PELANGGAN KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN MAKLUMAT PELANGGAN KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>Alamat Pelanggan</b></th>
                <th class="th-sortable active"><b>No. Telefon Rumah</b></th>
                <th class="th-sortable active"><b>No. Telefon Pejabat</b></th>
                <th class="th-sortable active"><b>Email</b></th>
                <th class="th-sortable active"><b>Dewan</b></th>
                <th class="th-sortable active"><b>Tarikh Tempah Dari</b></th>
                <th class="th-sortable active"><b>Tarikh Tempah Hingga</b></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;  
                $total = 0;
            @endphp
            @if (count($data['slot']) > 0)
            @foreach ($data['slot'] as $s)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ Helper::get_nama(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                    <td>{{ Helper::get_address(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                    <td>{{ Helper::get_no(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                    <td>{{ Helper::get_no_office(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                    <td>{{ Helper::get_email(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                    <td>{{ Helper::getHall($s->fk_bh_hall) }}</td>
                    <td>{{ Helper::date_format($s->bb_start_date) }}</td>
                    <td>{{ Helper::date_format($s->bb_end_date) }}</td>
                </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        @page {
            size: landscape;
            margin: 1cm;
        }
        #body {
            font-size: 10px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div align="right" width="15%" id="body">
      <div class="row">
        <div class="col" align="left">
          @if($data['location'] == '')
          <header class="panel-heading"></header>
          @else
              <header class="panel-heading"><strong>Lokasi : </strong>{{ strtoupper(Helper::location($data['id'])) }}</header>
          @endif
        </div>
        <div class="col" align="right">
          <strong>Tarikh</strong>
          <span>:</span>
          <span>@php echo date("d-m-Y"); @endphp</span>
        </div>
      </div>
      <div class="row">
        <div class="col" align="right">
            <strong>Masa</strong>
            <span>:</span>
            <span>@php echo date("H:i"); @endphp</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
    <br>
    

    <div>&nbsp;</div>
    <div class="col-md-12" align="center">
        <b>PERBADANAN PUTRAJAYA</b>
    </div>
    <div class="col-md-12" align="center">
            @if(isset($data['location']) && $data['location'] != '')
                <b>LAPORAN BAYARAN DEPOSIT KOMPLEKS KEJIRANAN {{ strtoupper(Helper::location($data['id'])) }}</b>
            @else
                <b>LAPORAN BAYARAN DEPOSIT KOMPLEKS KEJIRANAN</b>
            @endif
    </div>
    <div class="col-md-12" align="center">
        <?php if(isset($data['mula'])): ?>
            @if($data['mula'] != '' && $data['tamat'] != '')
                <b>Dari : <?php echo $data['mula']; ?></b>
                <b>Hingga : <?php echo $data['tamat']; ?></b>
            @endif
        <?php endif; ?>
    </div>
    <div>&nbsp;</div>

    <table width="100%" border="1" cellpadding="0" cellspacing="0" id="body">
        <thead>
            <tr>
                <th class="th-sortable active"><b>No.</b></th>
                <th class="th-sortable active"><b>No. Tempahan</b></th>
                <th class="th-sortable active"><b>Nama Pelanggan</b></th>
                <th class="th-sortable active"><b>Nama Dewan / Kelengkapan</b></th>
                <th class="th-sortable active"><b>Kod Fee</b></th>
                <th class="th-sortable active"><b>Tarikh Transaksi</b></th>
                <th class="th-sortable active"><b>No. Resit</b></th>
                <th class="th-sortable active"><b>Tujuan</b></th>
                @if ($data['jeniskutipan'] == 1)
                    <th class="th-sortable active"><b>Jenis Bayaran</b></th>
                @endif
                <th class="th-sortable active"><b>Jumlah (RM)</b></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;  
                $total = 0;
            @endphp
            @if (count($data['slot']) > 0)
                @if ($data['jeniskutipan'] == 2)
                    @for ($j = 0; $j < count($data['slot']); $j++)
                        <tr>
                            <td style="width: 5%;">{{ $i++ }}</td>
                            <td>{{ Helper::get_noTempahan(Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->fk_main_booking) }}</td>
                            <td>{{ Helper::get_nama(Helper::get_userTempahan(Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->fk_main_booking)) }}</td>
                            <td>{{ Helper::getHall($data['slot'][$j]->fk_bh_hall) }}</td>
                            <td>{{ Helper::getBhCodeDepositOnline($data['slot'][$j]->fk_bh_hall) }}</td>
                            <td>{{ ($data['slot'][$j]->created_at) }}</td>
                            {{-- <td>{{ Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->bp_receipt_number }}</td> --}}
                            <td>{{ Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->fpx_serial_no }}</td>
                            <td>{{ Helper::getBhBookingDetail(Helper::getBhBookingFromMBooking(Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->fk_main_booking)->id)->bbd_event_name }}</td>
                            {{-- <td>{{ Helper::get_kutipan_bayaran(Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->fk_lkp_payment_mode) }}</td> --}}
                            <td>{{ Helper::getBhPaymentFpx($data['slot'][$j]->fk_bh_payment_fpx)->amount_paid }}</td>
                        </tr>
                    @endfor
                @else
                    @for ($j = 0; $j < count($data['slot']); $j++)
                        <tr>
                            <td style="width: 5%;">{{ $i++ }}</td>
                            <td>{{ Helper::get_noTempahan(Helper::getBhPayment($data['slot'][$j]->fk_bh_payment)->fk_main_booking) }}</td>
                            <td>{{ Helper::get_nama(Helper::get_userTempahan(Helper::getBhPayment($data['slot'][$j]->fk_bh_payment)->fk_main_booking)) }}</td>
                            <td>{{ Helper::getHall($data['slot'][$j]->fk_bh_hall) }}</td>
                            <td>{{ Helper::getBhCodeDeposit($data['slot'][$j]->fk_bh_hall) }}</td>
                            <td>{{ ($data['slot'][$j]->created_at) }}</td>
                            <td>{{ Helper::getBhPayment($data['slot'][$j]->fk_bh_payment)->bp_receipt_number }}</td>
                            <td>{{ Helper::get_lkp_event(Helper::getBhBookingDetail(Helper::getBhBooking(Helper::getBhQuotationDetail(Helper::getBhQuotation(Helper::getQuotationFromBhPayment($data['slot'][$j]->fk_bh_payment))->id)->fk_bh_booking)->id)->fk_lkp_event) }}</td>
                            <td>{{ Helper::get_kutipan_bayaran(Helper::getBhPayment($data['slot'][$j]->fk_bh_payment)->fk_lkp_payment_mode) }}</td>
                            <td>{{ Helper::getBhPayment($data['slot'][$j]->fk_bh_payment)->bp_paid_amount }}</td>
                        </tr>
                    @endfor
                @endif
            @endif
        </tbody>
        {{-- <tfoot>
            <tr class="bg-dark">
                <td colspan="8" class="text-right text-white font-weight-bolder">Jumlah Keseluruhan (RM)</td>
                <td class="text-right text-white font-weight-bolder">{{ number_format($total , 2, '.', '') }}</td>
            </tr>
        </tfoot> --}}
    </table>
</body>
</html>

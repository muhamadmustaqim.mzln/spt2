@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Harga Peralatan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Harga Peralatan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/hall/admin/equipmentprice/add') }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Tambah Harga Peralatan</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Peralatan :</label>
                            <div class="col-lg-10">
                                <select name="peralatan" class="form-control" id="kt_select2_1">
                                    <option disabled selected value="">Pilih Peralatan</option>
                                    @foreach ($data['equipment'] as $s)
                                        <option value="{{ $s->id }}">{{ $s->be_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori Diskaun :</label>
                            <div class="col-lg-10">
                                <select name="discountcategory" class="form-control" id="">
                                    <option disabled selected value="">Pilih Kategori Diskaun</option>
                                    @foreach ($data['discount_type'] as $s)
                                        <option value="{{ $s->id }}">{{ $s->ldt_user_cat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori Hari :</label>
                            <div class="col-lg-10">
                                <select name="daycategory" class="form-control" id="kt_select2_4">
                                    <option value="">Sila Pilih Kategori GST</option>
                                    <option value="1">Isnin-Jumaat</option>
                                    <option value="2">Sabtu-Ahad/Cuti Umum</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Harga Seunit (RM) :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="hargaRM" name="hargaRM" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kadar Diskaun (%) :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="diskaunPer" name="diskaunPer" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Diskaun (RM) :</label>
                            <div class="col-lg-10">
                                <input type="text" readonly class="form-control form-control-solid" id="diskaunRM" name="diskaunRM" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kadar GST :</label>
                            <div class="col-lg-10">
                                <select name="kadargst" class="form-control" id="kadarGST">
                                    <option value="" disabled selected>Sila Pilih Kadar GST</option>
                                    @foreach($data['gst'] as $f)
                                        <option value="{{ $f->id }}" data-rate="{{ $f->lgr_rate }}">{{ $f->lgr_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">GST (4350)(RM) :</label>
                            <div class="col-lg-10">
                                <input type="text" readonly class="form-control form-control-solid" id="gst4350" name="gst4350" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jumlah Harga (RM) :</label>
                            <div class="col-lg-10">
                                <input type="text" readonly class="form-control form-control-solid" id="jumlahhargarm" name="jumlahharga" placeholder="" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10 my-auto">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    <label class="ml-2 text-muted" id="textbox1"></label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/hall/admin/equipmentprice') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('hall.facilityprice.js.form')
@endsection
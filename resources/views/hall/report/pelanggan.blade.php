@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Maklumat Pelanggan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Maklumat Pelanggan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                        <div class="form-group row d-flex justify-content-center">
                                            <div class="col-lg-3">
                                                <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                    <option value="" disabled selected>Sila Pilih Lokasi</option>
                                                    <option value="1" {{ isset($data['id']) && $data['id'] == 1 ? 'selected' : '' }}>PPj</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_4" name="mula" placeholder="Tarikh Tempah Dari" autocomplete="off" required value="{{ isset($data['mula']) ? Helper::date_format($data['mula']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_5" name="tamat" placeholder="Tarikh Tempah Hingga" autocomplete="off" required value="{{ isset($data['tamat']) ? Helper::date_format($data['tamat']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            {{-- <div class="col-lg-3">
                                                <div class="float-right">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="form-group row d-flex justify-content-center">
                                            <div class="col"></div>
                                            <div class="col">
                                                <div class="">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div>
                                            <div class="col"></div>
                                        </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                        <div class="col-lg-12">
                            <!--begin::List Widget 21-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Body-->
                                <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title text-center">
                                        <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN MAKLUMAT PELANGGAN KOMPLEKS PERBADANAN PUTRAJAYA <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Dari : {{ Helper::date_format($data['mula']) }} Hingga : {{ Helper::date_format($data['tamat']) }}</span></span>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                            <thead> 
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Nama Pelanggan</th>
                                                    <th>Alamat Pelanggan</th>
                                                    <th>No. Telefon Rumah</th>
                                                    <th>No. Telefon Pejabat</th>
                                                    <th>Email</th>
                                                    <th>Dewan</th>
                                                    <th>Tarikh Tempah Dari</th>
                                                    <th>Tarikh Tempah Hingga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;  
                                                    $total = 0;
                                                @endphp
                                                @if (count($data['slot']) > 0)
                                                    @foreach ($data['slot'] as $s)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_address(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_no(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_no_office(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_email(Helper::get_userTempahan($s->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::getHall($s->fk_bh_hall) }}</td>
                                                            <td>{{ Helper::date_format($s->bb_start_date) }}</td>
                                                            <td>{{ Helper::date_format($s->bb_end_date) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--end::Body-->
                                <div class="row">
                                    <div class="col-lg-12 d-flex justify-content-center gutter-b">
                                        <a href="{{ url('hall/eksportpdfmaklumatpelanggan',[Crypt::encrypt($data['id']),Crypt::encrypt($data['mula']),Crypt::encrypt($data['tamat'])]) }}" class="btn btn-sm btn-primary">Cetak PDF</a>
                                    </div>
                                </div>
                            </div>
                            <!--end::List Widget 21-->
                        </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('hall.report.js.pelanggan')
@endsection
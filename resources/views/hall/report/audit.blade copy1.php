@extends('layouts.master')
@section('container')
<style>
    .step-progress {
        display: -webkit-box;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .step-progress-list {
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    .step-progress-item {
        display: flex;
        align-items: center;
        min-width: 170px;
        margin-bottom: 10px;
        position: relative;
    }

    .step-progress-item + .step-progress-item::after {
        content: "";
        position: absolute;
        left: 50%;
        top: 0;
        background: #90ee90;
        width: 2px;
        height: calc(100% - 40px);
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
        z-index: -1; /* Adjusted z-index to make sure the line is behind the progress counts */
    }

    .progress-count {
        height: 40px;
        width: 40px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        font-weight: 600;
        position: relative;
        margin-right: 10px;
        z-index: 1; /* Ensure the progress count is above the line */
        color: transparent;
    }

    .progress-count::after {
        content: "";
        height: 40px;
        width: 40px;
        background: #90ee90;
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        border-radius: 50%;
        z-index: -1; /* Adjusted z-index to make sure the circle is behind the progress count text */
    }

    .progress-count:before {
        content: "";
        height: 10px;
        width: 20px;
        border-left: 3px solid #fff;
        border-bottom: 3px solid #fff;
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -60%) rotate(-45deg);
        transform: translate(-50%, -60%) rotate(-45deg);
        transform-origin: center center;
    }

    .progress-label {
        font-size: 14px;
        font-weight: 600;
    }

    .current-item .progress-count:before,
    .current-item ~ .step-progress-item .progress-count:before {
        display: none;
    }

    .current-item ~ .step-progress-item .progress-label {
        opacity: 0.5;
    }

    .current-item .progress-count::after {
        background: #fff;
        border: 2px solid #90ee90;
    }

    .current-item .progress-count {
        color: #90ee90;
    }
</style>
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Maklumat Audit</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Maklumat Audit</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <!--begin::List Widget 21-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Header-->
                                    <div class="card-header border-0 pt-5">
                                        <h3 class="card-title align-items-start flex-column mb-5">
                                            <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                        </h3>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Body-->
                                    <form action="" method="post">
                                        @csrf
                                        <div class="card-body pt-2">
                                            <h4 class="mt-2">No. Tempahan</h4>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <input required class="form-control" type="text" name="id" placeholder="No. Tempahan" autocomplete="off" required value="{{ $data['id'] }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <div class="float-right">
                                                        <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 21-->
                            </div>
                        </div>
                        @if ($data['post'] == true)
                        <div class="row">
                            <div class="col-lg-6">
                                <!--begin::List Widget 21-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Body-->
                                    <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                        <div class="card-title text-center">
                                            <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Audit </span>
                                        </div>
                                    </div>
                                    <div class="card-body pt-1">
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Nama Pengguna
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ Helper::get_nama($data['main']->fk_users) }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    No. Tempahan
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ $data['main']->bmb_booking_no }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Emel
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ Helper::get_email($data['main']->fk_users) }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    No. Telefon
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ $data['user_detail']->bud_phone_no }}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Nama Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ $data['bh_booking_detail'][0]->bbd_event_name }}</div>
                                                <div></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Keterangan Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ $data['bh_booking_detail'][0]->bbd_event_description }}</div>
                                                <div></div>
                                            </div>
                                        </div>
                                    @if(count($data['bh_booking']) > 0)
                                        {{-- @foreach ($data['bh_booking'] as $p) --}}
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Lokasi Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>{{ Helper::getHall($data['bh_booking'][0]->fk_bh_hall) }}, PPJ</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Tarikh Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col">
                                                        {{ Helper::date_format($data['bh_booking'][0]->bb_start_date) }}
                                                    </div>
                                                    <div class="col">
                                                        sehingga
                                                    </div>
                                                    <div class="col">
                                                        {{ Helper::date_format($data['bh_booking'][0]->bb_end_date) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- @endforeach --}}
                                        @else
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Lokasi Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>Tiada Maklumat</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <div class="font-weight-bolder text-dark">
                                                    Tarikh Acara
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div>Tiada Maklumat</div>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 21-->
                            </div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <!--begin::Body-->
                                    <div class="card-body">
                                        <section class="step-progress">
                                            <ul class="step-progress-list">
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">1</span>
                                                    <span class="progress-label">Baharu</span>
                                                </li>
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">2</span>
                                                    <span class="progress-label">Tempahan Diproses - Sedia untuk Bayaran</span>
                                                </li>
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">3</span>
                                                    <span class="progress-label">Tempahan Diproses (LO/PO)</span>
                                                </li>
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">4</span>
                                                    <span class="progress-label">Tempahan Berjaya (Bayaran Deposit)</span>
                                                </li>
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">5</span>
                                                    <span class="progress-label">Acara Selesai - Menunggu Pemeriksaan</span>
                                                </li>
                                                <li class="step-progress-item mb-5 current-item">
                                                    <span class="progress-count">6</span>
                                                    <span class="progress-label">Pemeriksaan Jabatan</span>
                                                </li>
                                                <li class="step-progress-item mb-5">
                                                    <span class="progress-count">7</span>
                                                    <span class="progress-label">Selesai</span>
                                                </li>
                                            </ul>
                                        </section>
                                    </div>
                                    @endif
                                    <!--end::Body-->
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('hall.report.js.pelanggan')
@endsection
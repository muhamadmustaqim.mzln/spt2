@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Kutipan Hasil</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Kutipan Hasil</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                        <div class="form-group row">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-3">
                                                <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                    <option value="" disabled selected>Sila Pilih Lokasi</option>
                                                    @foreach ($data['location'] as $l)
                                                        <option value="{{ $l->id }}" {{ ($l->id == $data['id']) ? 'selected' : '' }}>{{ $l->lc_description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <select name="bayaran" id="kt_select2_4" class="form-control mt-3" required>
                                                    <option value="" disabled selected>Cara Bayaran</option>
                                                    @foreach ($data['bayaran'] as $b)
                                                        <option value="{{ $b->id }}" {{ ($b->id == $data['mode']) ? 'selected' : '' }}>{{ $b->lpm_description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>                                            
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3"></div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_4" name="mula" placeholder="Tarikh Transaksi Dari" autocomplete="off" required value="{{ isset($data['mula']) ? Helper::date_format($data['mula']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_5" name="tamat" placeholder="Tarikh Transaksi Hingga" autocomplete="off" required value="{{ isset($data['tamat']) ? Helper::date_format($data['tamat']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row d-flex justify-content-center">
                                            <div class="col"></div>
                                            <div class="col">
                                                <div class="">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div>
                                            <div class="col"></div>
                                        </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title text-center">
                                    <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN MAKLUMAT KUTIPAN HASIL DEWAN KOMPLEKS PERBADANAN PUTRAJAYA {{ strtoupper(Helper::location($data['id'])) }} <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Dari : {{ Helper::date_format($data['mula']) }} Hingga : {{ Helper::date_format($data['tamat']) }}</span></span>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                        <thead>
                                            <tr>
                                                <th>Bil</th>
                                                <th>No Tempahan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Nama Dewan/Kelengkapan</th>
                                                <th>Kod Fee</th>
                                                <th>Tarikh Transaksi</th>
                                                <th>No Resit</th>
                                                <th>Tujuan</th>
                                                <th>Lokasi/Fasiliti</th>
                                                <th>Jenis Bayaran</th>
                                                <th>Jumlah (RM)</th>
                                                {{-- <th>Nilai GST (RM)</th>
                                                <th>Kod GST</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                                $total = 0;
                                            @endphp
                                            @if (count($data['slot']) > 0)
                                                @foreach ($data['slot'] as $slot)
                                                    <tr>
                                                        <td style="width: 5%;">{{ $i++ }}</td>
                                                        <td>{{ $slot->bmb_booking_no }}</td>
                                                        <td>{{ $slot->fullname }}</td>
                                                        <td>{{ $slot->bh_name }}</td>
                                                        <td>{{ $slot->bh_code }}</td>
                                                        <td>{{ $slot->bp_receipt_date }}</td>
                                                        <td>{{ $slot->bp_receipt_number }}</td>
                                                        <td>{{ Helper::get_lkp_event($slot->fk_lkp_event)}}</td>
                                                        <td>{{ Helper::location($slot->fk_lkp_location) }}</td>
                                                        <td>{{ $slot->lpm_description }}</td>
                                                        <td>{{ $slot->bp_total_amount }}</td>
                                                    </tr>
                                                    @php
                                                    $total += ($slot->bp_total_amount);
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="11" class="text-center text-danger">Harap Maaf, tiada data untuk jarak tarikh tersebut!</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr class="bg-dark">
                                                <td colspan="10" class="text-right font-weight-bolder text-white">Jumlah Keseluruhan (RM)</td>
                                                <td class="text-right font-weight-bolder text-white">{{ number_format($total, 2, '.', ',') }}</td>
                                                {{-- <td class="text-right font-weight-bolder text-white">{{ number_format($total, 2, '.', '') }}</td> --}}
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <!--end::Body-->
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center gutter-b">
                                    <a href="{{ url('hall/eksportpdfhasil',[Crypt::encrypt($data['id']),Crypt::encrypt($data['mode']),Crypt::encrypt($data['start']),Crypt::encrypt($data['end'])]) }}" class="btn btn-sm btn-primary">Cetak PDF</a>
                                </div>
                            </div>
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('hall.report.js.pelanggan')
@endsection
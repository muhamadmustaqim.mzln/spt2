@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Pemulangan Bayaran</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Pemulangan Bayaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                        <div class="form-group row d-flex justify-content-center">
                                            <div class="col-lg-3">
                                                <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                    <option value="" disabled selected>Sila Pilih Lokasi</option>
                                                    <option value="1" {{ isset($data['id']) && $data['id'] == 1 ? 'selected' : '' }}>PPj</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_4" name="tarikhMula" placeholder="Tarikh Tempah Dari" autocomplete="off" required value="{{ isset($data['tarikhMula']) ? Helper::date_format($data['tarikhMula']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control form-control-lg font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikhAkhir" placeholder="Tarikh Tempah Hingga" autocomplete="off" required value="{{ isset($data['tarikhAkhir']) ? Helper::date_format($data['tarikhAkhir']) : '' }}" />
                                                    <span>
                                                        <i class="flaticon-calendar-3 icon-xl"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            {{-- <div class="col-lg-3">
                                                <div class="float-right">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="form-group row d-flex justify-content-center">
                                            <div class="col"></div>
                                            <div class="col">
                                                <div class="">
                                                    <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                                </div>
                                            </div>
                                            <div class="col"></div>
                                        </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                        <div class="col-lg-12">
                            <!--begin::List Widget 21-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Body-->
                                <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title text-center">
                                        <span class="card-label font-weight-bolder text-dark mb-1">PERBADANAN PUTRAJAYA <br>LAPORAN MAKLUMAT TEMPAHAN DEWAN KOMPLEKS PERBADANAN PUTRAJAYA <br><span class="text-muted mt-2 font-weight-bold font-size-sm">Dari : {{ Helper::date_format($data['tarikhMula']) }} Hingga : {{ Helper::date_format($data['tarikhAkhir']) }}</span></span>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                            <thead> 
                                                <tr>
                                                    <th>Bil</th>
                                                    <th>Nama Pelanggan</th>
                                                    <th>No. Tempahan</th>
                                                    <th>Nama Dewan</th>
                                                    <th>Acara</th>
                                                    <th>Tarikh Tempah Dari</th>
                                                    <th>Tarikh Tempah Hingga</th>
                                                    <th>Masa Dari</th>
                                                    <th>Masa Hingga</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 @php
                                                    $i = 1;  
                                                    $total = 0;
                                                @endphp
                                                {{--                                 
                                                @if (count($data['slot']) > 0)
                                                    @for ($j = 0; $j < count($data['slot']); $j++)
                                                        <tr>
                                                            <td style="width: 5%;">{{ $i++ }}</td>
                                                            <td>{{ Helper::get_nama(Helper::get_userTempahan($data['slot'][$j]->fk_main_booking)) }}</td>
                                                            <td>{{ Helper::get_noTempahan($data['slot'][$j]->fk_main_booking) }}</td>
                                                            <td>{{ Helper::getHall($data['slot'][$j]->fk_bh_hall) }}</td>
                                                            <td>{{ ($data['bh_booking_detail'][$j]->bbd_event_name) }}</td>
                                                            <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_start_date) }}</td>
                                                            <td class="text-center">{{ Helper::date_format($data['slot'][$j]->bb_end_date) }}</td>
                                                            <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_start_time) }}</td>
                                                            <td class="text-center">{{ ($data['bh_booking_detail'][$j]->bbd_end_time) }}</td>
                                                            <td style="white-space: nowrap">
                                                                @if (isset($data['bh_booking_equipment'][$data['slot'][$j]->id]))
                                                                    @foreach ($data['bh_booking_equipment'][$data['slot'][$j]->id] as $bbe)
                                                                        {{ Helper::get_equipment_hall($bbe->fk_bh_equipment) }}<br>
                                                                    @endforeach
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endfor
                                                @else--}}
                                                <tr>
                                                    <td colspan="10" class="text-center">Harap Maaf, data tiada dalam sistem!</td>
                                                </tr>
                                                {{--@endif --}}
                                            </tbody>
                                            <tfoot>
                                                <tr class="bg-dark">
                                                    <td colspan="9" class="text-right font-weight-bolder text-white">Jumlah Keseluruhan (RM)</td>
                                                    {{-- <td class="text-right font-weight-bolder text-white">{{ number_format($total , 2, '.', '') }}</td> --}}
                                                    <td class="text-right font-weight-bolder text-white">{{ number_format($total , 2, '.', '') }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::List Widget 21-->
                        </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    @include('hall.report.js.tempahan')
@endsection
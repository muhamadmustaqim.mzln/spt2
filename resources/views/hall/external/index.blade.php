@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Luaran</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Luaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                            <div class="card card-custom gutter-b">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <h3 class="card-title align-items-start flex-column mb-5">
                                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                        {{-- <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ Helper::date_format_between($data['date']) }}</span> --}}
                                    </h3>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button> --}}
                                        {{-- <a href="{{ url('hall/internal/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" class="btn btn-primary font-weight-bolder mr-2 mt-2">
                                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                        </a>--}}
                                        <a href="{{ url('/hall/external/user') }}" class="btn btn-primary font-weight-bolder">
                                            <i class="flaticon-add-circular-button"></i> Tambah Tempahan
                                        </a>
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    <td>Bil</td>
                                                    <th>No Tempahan</th>
                                                    <th>Tempahan Dibuat Oleh</th>
                                                    <th>No. Tel</th>
                                                    <th>Nama Dewan</th>
                                                    <th>Tarikh Dari</th>
                                                    <th>Tarikh Hingga</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($data['list'] as $hall)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $hall->notempahan}}</td>
                                                        <td>{{ $hall->nama}}</td>
                                                        <td style="white-space: nowrap">{{ $hall->bud_phone_no}}</td>
                                                        <td>{{ $hall->bh_name}}</td>
                                                        <td style="white-space: nowrap">{{ Helper::date_format($hall->start)}}</td>
                                                        <td style="white-space: nowrap">{{ Helper::date_format($hall->end)}}</td>
                                                        <td>{{ $hall->status}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        <!--end::Card-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('js_content')
    @include('hall.internal.js.index')
@endsection



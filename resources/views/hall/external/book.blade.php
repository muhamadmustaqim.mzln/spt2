@extends('layouts.master')

@section('container')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Luaran</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Luaran</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                                <div class="card-body pt-2">
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-lg-3">
                                            <select name="jenisTempahan" id="" class="form-control mt-3" required>
                                                <option value="">Pilih Jenis Tempahan</option>
                                                <option value="1" @if(isset($data['jenis'])) @if($data['jenis'] == 1) selected @endif @endif>Tempahan Rasmi</option>
                                                <option value="2" @if(isset($data['jenis'])) @if($data['jenis'] == 2) selected @endif @endif>Sekatan Tempahan</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <select name="lokasi" id="kt_select2_4" class="form-control mt-3" required>
                                                <option value="">Pilih Lokasi</option>
                                                @foreach ($data['location'] as $l)
                                                    <option value="{{ $l->id }}"@if(isset($data['id'])) @if($l->id == $data['id']) selected @endif @endif>{{ $l->bh_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="input-icon">
                                                <input type="text" class="form-control font-weight-bold mt-3" id="kt_daterangepicker_5" value="{{ $data['start'] }}" name="tarikhMula" placeholder="Tarikh" autocomplete="off" required />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="input-icon">
                                                <input type="text" class="form-control font-weight-bold mt-3" id="kt_daterangepicker_6" value="{{ $data['end'] }}" name="tarikhAkhir" placeholder="Tarikh" autocomplete="off" required />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="float-right">
                                                <button type="submit" id="submit_form" class="form-control btn btn-primary font-weight-bold mr-2 mt-3">Carian</button>
                                            </div>
                                        </div>
										<!--
                                        <div class="col-lg-2">
                                            <select name="lokasi" class="form-control mt-3" id="kt_select2_5" required disabled>
                                                <option value="">Sila Pilih Fasiliti</option>
                                            </select>
                                        </div>
										!-->
                                        {{-- <div class="col-lg-3">
                                            <div class='input-group' id="kt_daterangepicker_5">
                                                <input value="@if(isset($data['date'])) {{ Helper::date_format_between($data['date']) }} @endif" type="text" name="tarikh" class="form-control mt-3"  placeholder="Tarikh Penggunaan" autocomplete="off" readonly required>
                                                <div class="input-group-append mt-3">
                                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                    <hr>
                                </div>
                            </form>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <div class="col-lg-12">
                        @if ($data['post'] == true)
                        <!--begin::Card-->
                        <form action="{{ url('hall/external/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" method="post" id="form">
                            @csrf
                            <div class="card card-custom gutter-b">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <h3 class="card-title align-items-start flex-column mb-5">
                                        <span class="card-label font-weight-bolder text-dark">Kekosongan Fasiliti</span>
                                        <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ ($data['date']) }}</span>
                                        {{-- <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh : {{ Helper::date_format_between($data['date']) }}</span> --}}
                                    </h3>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        {{-- <button type="submit" class="btn btn-primary font-weight-bolder mr-2 mt-2"><i class="fas fa-check-circle"></i> Teruskan Tempahan</button> --}}
                                        <a href="{{ url('hall/external/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date']), Crypt::encrypt($data['userId'])]) }}" class="btn btn-primary font-weight-bolder mr-2 mt-2">
                                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                                        </a>
                                        <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                                            <i class="flaticon-circle"></i> Batal Tempahan
                                        </a>
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="kt_datatable_2">
                                            <thead>
                                                <tr>
                                                    {{-- <th>
                                                        <label class="checkbox checkbox-primary checkbox-lg">
                                                            <input type="checkbox" id="example-select-all" class="group-checkable"/>
                                                            <span></span>
                                                        </label>
                                                    </th> --}}
                                                    <th>Bil</th>
                                                    <th>No Tempahan</th>
                                                    <th>Nama Pemohon</th>
                                                    <th>No. Tel</th>
                                                    <th>Nama Dewan</th>
                                                    <th>Tarikh Dari</th>
                                                    <th>Tarikh Hingga</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach($data['bh_booking'] as $hall)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $hall->bmb_booking_no }}</td>
                                                        <td>{{ Helper::get_no($hall->fk_users) }}</td>
                                                        <td>{{ Helper::getHall($hall->fk_bh_hall) }}</td>
                                                        <td>{{ Helper::date_format($hall->bb_start_date) }}</td>
                                                        <td>{{ Helper::date_format($hall->bb_end_date) }}</td>
                                                        <td>{{ $hall->bmb_booking_no }}</td>
                                                        <td>{{ Helper::get_status_tempahan($hall->fk_lkp_status) }}</td>
                                                    </tr>
                                                @endforeach
                                                {{-- @foreach($data['bh_booking'] as $hall) --}}
                                                    {{-- @foreach ($hall as $item) --}}
                                                    {{-- <tr>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td>
                                                        <td>{{ \Carbon\Carbon::parse($hall->bb_start_date)->format('d-M-Y')}}</td>
                                                        <td>{{ \Carbon\Carbon::parse($hall->bb_end_date)->format('d-M-Y')}}</td>
                                                        <td>{{ $hall->bmb_booking_no}}</td> --}}
                                                        {{-- <td>{{ $hall->est_slot_time }}</td>
                                                        <td>{{ date("d-m-Y", strtotime($s)) }}</td>
                                                        <td>{{ $data['fasility']->eft_type_desc }}</td>
                                                        <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td> --}}
                                                    {{-- </tr> --}}
                                                    {{-- @endforeach --}}
                                                {{-- @endforeach --}}
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        </form>
                        <!--end::Card-->
                        @endif
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@section('js_content')
    @include('hall.internal.js.index')
@endsection



<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
    var table = $('.table').DataTable({
        dom: `<'row'<'col-sm-12'tr>>`,
        buttons: [
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                  columns: ':visible'
                },
                customize: function(doc) {
                    doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    //doc.defaultStyle.alignment = 'center';
                    //doc.styles.tableHeader.alignment = 'center';
                },
            },
            {
                extend: 'colvis',
                text: 'Pilihan Lajur'
            },
            
        ],
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        }
    } );
    var table = $('#table2').DataTable({
        dom: `<'row'<'col-sm-12'tr>>`,
        buttons: [
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                  columns: ':visible'
                },
                customize: function(doc) {
                    doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    //doc.defaultStyle.alignment = 'center';
                    //doc.styles.tableHeader.alignment = 'center';
                },
            },
            {
                extend: 'colvis',
                text: 'Pilihan Lajur'
            },
            
        ],
        language: {
            "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
            "zeroRecords": "Harap maaf, tiada rekod ditemui",
            "info": "Halaman _PAGE_ dari _PAGES_",
            "infoEmpty": "Tiada rekod dalam sistem",
            "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
            "sSearch": "Carian:"
        }
    } );
    $('#carianSukan').on('change', function () {
        
    })
});
</script>
<script>
    $('input[type="checkbox"][name="pegawaichkbx"]').change(function() {
        if ($(this).prop('checked')) {
            $('#pegawaiInput').removeAttr('disabled');
        } else {
            $('#pegawaiInput').prop('disabled', true).val('');
        }
    });
    $('[name="dewan"]').change(function() {
        $('[name="dewan"]').not(this).prop('checked', false);
    });
    $('.checkbox-group').change(function() {
        var groupName = $(this).attr('name');
        $('[name="' + groupName + '"]').not(this).prop('checked', false);
    });
    $("#form").submit(function( event ) {
        var atLeastOneIsChecked = $('input[name="pegawai[]"]:checked').length > 0;
        if(atLeastOneIsChecked == false){
            event.preventDefault();
            Swal.fire(
            'Harap Maaf!',
            'Sila pilih sekurang-kurangnya 1 pegawai terlibat dalam pemeriksaan dewan.',
            'error'
            );
        }else{
            $("#form").submit();
        }

    });
</script>

<script>
    $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
    // Class definition
var KTBootstrapSelect = function () {
// Private functions
    var demos = function () {
        // minimum setup
        $('.kt-selectpicker').selectpicker();
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
KTBootstrapSelect.init();
});
</script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
{{-- <script>
    $(document).ready(function() {
        var table = $('#kt_datatable_1').DataTable({
            dom: `<'row'<'col-sm-12'tr>>`,
            language: {
                "lengthMenu": "Paparan _MENU_ rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        });
    });
</script> --}}
<script>
    $('input[type="checkbox"][name="pegawaichkbx"]').change(function() {
        if ($(this).prop('checked')) {
            $('#pegawaiInput').removeAttr('disabled');
        } else {
            $('#pegawaiInput').prop('disabled', true).val('');
        }
    });
    $('[name="dewan"]').change(function() {
        $('[name="dewan"]').not(this).prop('checked', false);
    });
    $('.checkbox-group').change(function() {
        var groupName = $(this).attr('name');
        $('[name="' + groupName + '"]').not(this).prop('checked', false);
    });
    $("#form").submit(function( event ) {
        var atLeastOneIsChecked = $('input[name="pegawai[]"]:checked').length > 0;
        if(atLeastOneIsChecked == false){
            event.preventDefault();
            Swal.fire(
            'Harap Maaf!',
            'Sila pilih sekurang-kurangnya 1 pegawai terlibat dalam pemeriksaan dewan.',
            'error'
            );
        }else{
            $("#form").submit();
        }

    });
</script>
<script>
    $(document).ready(function() {
        // $('#customModal').modal('show'); // Pelarasan

        function updateSubtotal(val, isPercentage, j) {
            var total = parseFloat($('#total_' + j).text());
            var subtotal = 0;
            var discount_val = 0;
            if(isPercentage){
                if(val > 100){
                    val = 100;
                    $('.per_' + j).val(val);
                } 
                val = val / 100;
                discount_val = total * val;
                subtotal = total - discount_val;
                console.log(subtotal)
                $('.val_' + j).val(discount_val.toFixed(2));
                $('.subtotal_'+j).val(subtotal.toFixed(2));
                $('.hdsubtotal_'+j).val(subtotal.toFixed(2));
            } else {
                if(val > total){
                    val = total;
                    $('.val_' + j).val(val);
                } 
                percent_val = ((val / total) * 100).toFixed(2);
                subtotal = (total - val);
                console.log(subtotal)
                $('.per_' + j).val(percent_val.toFixed(2));
                $('.subtotal_'+j).val(subtotal.toFixed(2));
                $('.hdsubtotal_'+j).val(subtotal.toFixed(2));
            }
        }
        function updateTotal() {
            var total = 0;
            $('[class*="hdsubtotal_"]').each(function() {
                console.log('dewan: ' + $(this).val());
                total += parseFloat($(this).val() || 0);
            });
            $('#subtotal').text(total.toFixed(2));
        }
        $('.percentage-input, .fixed-value-input').on('input', function() {
            var row = $(this).closest('tr');
            var val = $(this).val();
            var isPercentage = $(this).hasClass('percentage-input');
            var j = $(this).data(isPercentage ? 'perctr' : 'valctr');
            isPercentage ? 'per' : 'val';
            updateSubtotal(val, isPercentage, j);
            updateTotal();
        });
        
        function updateEQSubtotal(val, isPercentage, j) {
            var total = parseFloat($('#eqtotal_' + j).text());
            console.log('total: ' + total, 'j: ' + j)
            var subtotal = 0;
            var discount_val = 0;
            if(isPercentage){
                if(val > 100){
                    val = 100;
                    $('.eqper_' + j).val(val);
                } 
                val = val / 100;
                discount_val = total * val;
                subtotal = total - discount_val;
                console.log(subtotal)
                $('.eqval_' + j).val(discount_val.toFixed(2));
                $('.eqsubtotal_'+j).val(subtotal.toFixed(2));
                $('.hdeqsubtotal_'+j).val(subtotal.toFixed(2));
            } else {
                if(val > total){
                    val = total;
                    $('.eqval_' + j).val(val);
                } 
                percent_val = ((val / total) * 100).toFixed(2);
                subtotal = (total - val);
                console.log(subtotal)
                $('.eqper_' + j).val(percent_val);
                $('.eqsubtotal_'+j).val(subtotal.toFixed(2));
                $('.hdeqsubtotal_'+j).val(subtotal.toFixed(2));
            }
        }
        function updateTotaleq() {
            var total = 0;
            $('[class*="hdeqsubtotal_"]').each(function() {
                console.log('eq: ' + $(this).val());
                total += parseFloat($(this).val() || 0);
            });
            $('#eqsubtotal').text(total.toFixed(2));
        }
        $('.eqpercentage-input, .eqfixed-value-input').on('input', function() {
            var row = $(this).closest('tr');
            var val = $(this).val();
            var isPercentage = $(this).hasClass('eqpercentage-input');
            console.log('isPercentage = ' + isPercentage);
            var j = $(this).data(isPercentage ? 'eqperctr' : 'eqvalctr');
            isPercentage ? 'eqper' : 'eqval';
            console.log('val = ' + val);
            console.log('j = ' + j);
            updateEQSubtotal(val, isPercentage, j);
            updateTotaleq();
        });
        
        function moneyHelper(id) {
            return parseFloat(id).toFixed(2);
        }
        function numberFormat(value, decimals, decimalSeparator, thousandsSeparator) {
            // Default to 2 decimal places, '.' as decimal separator, and ',' as thousands separator if not provided
            decimals = typeof decimals !== 'undefined' ? decimals : 2;
            decimalSeparator = typeof decimalSeparator !== 'undefined' ? decimalSeparator : '.';
            thousandsSeparator = typeof thousandsSeparator !== 'undefined' ? thousandsSeparator : ',';

            // Convert the value to a float and round to the specified number of decimal places
            var floatValue = parseFloat(value);
            var roundedValue = floatValue.toFixed(decimals);

            // Split the rounded value into integer and fractional parts
            var parts = roundedValue.split('.');

            // Add thousands separator to the integer part
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);

            // Join integer and fractional parts with the decimal separator
            return parts.join(decimalSeparator);
        }

        // Hide lopo element when the document is ready
        $('#lopo').attr('hidden', true);
        $('#norujukanbayaran').attr('hidden', true);
        $('#nocekDiv').attr('hidden', true);
        $('#nocek').attr('hidden', true);
        $('#namabankDiv').attr('hidden', true);
        $('#namabank').attr('hidden', true);

        $('#carabayaran').on('change', function() {
            var selectedDescription = $(this).find(':selected').text().toLowerCase();
            var caraBayaranVal = $(this).val();
            console.log(caraBayaranVal)
            if (caraBayaranVal == 2){           // LO/PO
                $('#lopo').removeAttr('hidden');
                $('input[name="lopo"]').attr('required', true);
                $('#nocekDiv').attr('hidden', true);
                $('input[name="nocek"]').removeAttr('required');
                $('#namabankDiv').attr('hidden', true);
                $('input[name="namabank"]').removeAttr('required');
                $('#norujukanbayaran').removeAttr('hidden');
            } else if (caraBayaranVal == 3){    // Bank Draft
                $('#lopo').attr('hidden', true);
                $('input[name="lopo"]').removeAttr('required');
                $('#nocekDiv').removeAttr('hidden');
                $('input[name="nocek"]').attr('required', true);
                $('#namabankDiv').removeAttr('hidden');
                $('input[name="namabank"]').attr('required', true);
                $('#norujukanbayaran').removeAttr('hidden');
                $('#lopo').attr('hidden', true);
            } else {                            // Others
                $('#lopo').attr('hidden', true);
                $('input[name="lopo"]').removeAttr('required');
                $('#nocekDiv').attr('hidden', true);
                $('input[name="nocek"]').removeAttr('required');
                $('#namabankDiv').attr('hidden', true);
                $('input[name="namabank"]').removeAttr('required');
                $('#norujukanbayaran').removeAttr('hidden');
                $('#lopo').attr('hidden', true);
            }
            // if (selectedDescription == 'fpx') {
            //     $('#norujukanbayaran').attr('hidden', true);
            //     $('#lopo').attr('hidden', true);
            //     $('input[name="lopo"]').removeAttr('required');
            //     $('input[name="rujukanBayaran"]').removeAttr('required');
            // } else if (selectedDescription == 'lo/po'){
            //     $('#norujukanbayaran').removeAttr('hidden');
            //     $('#lopo').removeAttr('hidden');
            //     $('input[name="lopo"]').attr('required', true);
            //     $('input[name="rujukanBayaran"]').attr('required', true);
            // } else {
            //     $('#norujukanbayaran').removeAttr('hidden');
            //     $('#lopo').attr('hidden', true);
            // }
        });
        $('#jenis_bayaran').on('change', function() {
            var value = $(this).val();
            var lkp_status = $('#lkp_status').val();
            if (lkp_status == 2){
                if (value == 1) {
                    var depo = parseFloat($('#deposit').val());
                    var pendahuluan = parseFloat($('#pendahuluan').val());
                    var total = depo + pendahuluan;
                    $('#jumlahperludibayar').val(numberFormat(total));
                    $('.jumlahBayaran').val(numberFormat(total));
                } else if (value == 2) {
                    var depo = parseFloat($('#deposit').val());
                    var penuh = parseFloat(($('#hall_val').val())) + parseFloat(($('#equipment_val').val()));
                    var total = depo + penuh
                    $('#jumlahperludibayar').val(numberFormat(total));
                    $('.jumlahBayaran').val(numberFormat(total));
                }
            } else if (lkp_status == 4){
                    var total = (parseFloat(($('#hall_val').val())) / 2) + parseFloat(($('#equipment_val').val()));
                    $('#jumlahperludibayar').val(numberFormat(total));
                    $('.jumlahBayaran').val(numberFormat(total));
            }
            // if (value == 1) {
            //     var depo = parseFloat($('#deposit').val());
            //     var pendahuluan = parseFloat($('#pendahuluan').val());
            //     var depoPendahuluan = depo + pendahuluan;
            //     $('#jumlahperludibayar').val(depoPendahuluan);
            //     $('.jumlahBayaran').val(depoPendahuluan);
            // } else if (value == 2) {
            //     var penuh = parseFloat(($('#jumlahTempahanId').val()));
            //     var pendahuluan = parseFloat(($('#pendahuluan').val()));
            //     console.log(penuh, pendahuluan)
            //     $('#jumlahperludibayar').val(moneyHelper(penuh - pendahuluan));
            //     $('.jumlahBayaran').val(moneyHelper(penuh - pendahuluan));
            // }
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('#fileUpload').on('change', function() {
            var filename = $(this).val().replace(/C:\\fakepath\\/i, ''); // Extract filename
            $('#selectedFile').text('Selected file: ' + filename);
        });
        $(".btn-confirm-custom").on('click', function(event) {
            event.preventDefault();
            const form = $(this).closest('form');
            const type = form.find('input[name="type"]').val();

            // Check which form is being submitted based on the "type" value
            if (type === '1') {
                // Form type is 1 (Senarai Fail)
                const fileInput = form.find('input[name="fail"]');
                
                // Check if a file is selected
                if (fileInput[0].files.length === 0) {
                    // No file selected, show an error message
                    Swal.fire({
                        title: 'Harap Maaf!',
                        text: 'Sila pilih fail yang hendak dimuat naik.',
                        icon: 'error'
                    });
                    return; // Prevent form submission
                }
            }
            
            
            if (form[0].checkValidity()) {
                Swal.fire({
                    title: 'Anda pasti?',
                    text: "Maklumat dihantar adalah benar!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Kembali',
                    confirmButtonText: 'Saya Pasti'
                }).then((result) => {
                    if (result.value) {
                        // console.log(result, result.value);
                        form.submit();
                    }
                });
            } else {
                // If the form is not valid, focus on the first invalid input field
                Swal.fire({
                    title: 'Harap Maaf!',
                    text: 'Sila isi ruangan diperlukan.',
                    icon: 'error'
                })
            }
        });
    })
</script>
<script>   
    var options = {
          series: [{
          name: 'Tempahan',
          type: 'column',
          data: [440, 505, 414, 671, 227, 413, 0, 0, 0, 0, 0]
        }],
          chart: {
          height: 350,
          type: 'line',
        },
        stroke: {
          width: [0, 4]
        },
        dataLabels: {
          enabled: true,
          enabledOnSeries: [1]
        },
        labels: ['Januari 2023', 'Februari 2023', 'Mac 2023', 'April 2023', 'Jun 2023', 'Julai 2023', 'Ogos 2023', 'September 2023', 'Oktober 2023', 'November 2023', 'Disember 2023'],
        xaxis: {
          type: 'Month'
        },
        yaxis: [{
          title: {
            text: 'Jumlah Tempahan',
          },
        
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart_dd"), options);
        chart.render();
      
</script>
<script>
  var options = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
            bar: {
              horizontal: true,
              dataLabels: {
                position: 'top',
              },
            }
          },
    dataLabels: {
        enabled: false
    },
    series: [],
    noData: {
      text: 'Loading...'
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val + " tempahan"
        }
      }
    }
  }

var chart2 = new ApexCharts(
document.querySelector("#chart_dd2"),
options
);

chart2.render();


$.ajax({
url: "{{ url('sport/dashboard/top_tempahan') }}", 
dataType: "json",
success: function(result) {
chart2.updateSeries([{
name: 'Jumlah',
data: result 
}]);
}
});
</script>
<script>
      var options = {
        chart: {
            height: 350,
            type: 'bar',
        },
        plotOptions: {
                bar: {
                  horizontal: true,
                  dataLabels: {
                    position: 'top',
                  },
                }
              },
        dataLabels: {
            enabled: false
        },
        series: [],
        noData: {
          text: 'Loading...'
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + " tempahan"
            }
          }
        }
      }

var chart1 = new ApexCharts(
  document.querySelector("#chart_dd1"),
  options
);

chart1.render();


$.ajax({
  url: "{{ url('sport/dashboard/top_pengguna') }}", 
  dataType: "json",
  success: function(result) {
    chart1.updateSeries([{
    name: 'Jumlah',
    data: result 
  }]);
  }
});
</script>
<script>
      
      var options = {
          series: [44, 55, 67, 83],
          chart: {
          height: 350,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            dataLabels: {
              name: {
                fontSize: '22px',
              },
              value: {
                fontSize: '16px',
              },
              total: {
                show: true,
                label: 'Total',
                formatter: function (w) {
                  // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                  return 249
                }
              }
            }
          }
        },
        labels: ['Apples', 'Oranges', 'Bananas', 'Berries'],
        };

        var chart = new ApexCharts(document.querySelector("#chart_dd3"), options);
        chart.render();
</script>
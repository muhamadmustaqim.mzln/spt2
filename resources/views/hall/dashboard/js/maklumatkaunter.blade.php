
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('#customModal').modal('show');
        $('#customModal').on('hide.bs.modal', function (e) {
            // Perform the same action as the "Tidak" (Cancel) button click
            $('#nocek').prop('hidden', true);
            $('#namabank').prop('hidden', true);
            $('#lopo').prop('hidden', true);
        });
        // Optional: Focus on the "Tidak" (Cancel) button when the modal is shown
        $('#customModal').on('shown.bs.modal', function (e) {
            $('#modalTidak').focus();
        });
        $('#carabayaran').on('change', function() {
            var selectedDescription = $(this).find(':selected').text().toLowerCase();
            if (selectedDescription == 'fpx') {
                $('#norujukanbayaran').attr('hidden', true);
                $('#lopo').attr('hidden', true);
            } else {
                $('#norujukanbayaran').removeAttr('hidden');
                $('#lopo').removeAttr('hidden');
            }
        });
        $('#jenis_bayaran').on('change', function() {
            var value = $(this).val();
            if (value == 1) {
                var depo = parseFloat($('#deposit').val());
                var pendahuluan = parseFloat($('#pendahuluan').val());
                var depoPendahuluan = depo + pendahuluan;
                $('#jumlahperludibayar').val(depoPendahuluan);
                $('#jumlahbayaran').val(depoPendahuluan);
            } else if (value == 2) {
                var penuh = parseFloat(($('#jumlahtempahan').val())/2);
                $('#jumlahperludibayar').val(penuh);
                $('#jumlahbayaran').val(penuh);
            }
        })
    });
</script>
@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body flex-wrap py-5">
                        <h3 class="card-label">Makluman Pengesahan Tempahan Dewan</h3>
                        <ul>
                            <li>Pengesahan Tempahan perlu disahkan dalam tempoh <span class="text-danger">3 hari</span> dari tarikh permohonan.</li>
                            <li>Permohonan yang tidak disahkan dalam tempoh 3 hari akan disahkan secara automatik oleh sistem.</li>
                        </ul>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <form action="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="text" name="type" value="1" hidden>
                                    <div class="">
                                        <h4>Senarai Fail</h4>
                                    </div>
                                    <hr>
                                    <div class="row my-2">
                                        {{-- <div class="col-4">Nama Fail</div>
                                        <div class="col-8">Tindakan</div> --}}
                                        <div class="col-12">
                                            <table class="table border" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th style="max-width: 60%;">Nama Fail</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($data['bh_attachment']) > 0)
                                                    @foreach ($data['bh_attachment'] as $val)
                                                        <tr>
                                                            <td>{{ $val->ba_file_name }}</td>
                                                            <td style="width: 40%;">
                                                                <div class="dropdown">
                                                                    <button class="btn btn-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                      Tindakan
                                                                    </button>
                                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                        {{-- <a class="dropdown-item text-primary" href="">Muat Turun</a> --}}
                                                                        <a href="{{ url('/hall/dashboard/removefile', Crypt::encrypt($data['main']->id)) }}" class="dropdown-item text-danger btn-delete" href="">Hapus</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    @else
                                                        <tr>
                                                            <td class="text-center" colspan="2">No Data</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <input name="fail" type="file" class="form-control" id="fileUpload" accept=".pdf, image/*" style="display:none;">
                                        <label for="fileUpload" class="btn btn-secondary btn-file-upload">Muat Naik</label>
                                        <button @if($data['main']->fk_lkp_status != 13) disabled @endif class="btn btn-secondary btn-confirm-custom">Simpan</button>
                                    </div>
                                    <div class="my-auto" id="selectedFile"></div>
                                </form>
                            </div>
                            <div class="col-6">
                                <form action="" method="post">
                                    @csrf
                                    <input type="text" name="type" value="2" hidden>
                                    <div>
                                        <h4>Tindakan Kelulusan</h4>
                                    </div>
                                    <hr>
                                    <div class="row my-2">
                                        <div class="col-4">
                                            Tindakan<span class="text-danger">*</span>
                                        </div>
                                        <div class="col-8">
                                            <select @if($data['main']->fk_lkp_status != 19) disabled @endif name="tindakan" class="form-control" name="" id="" required>
                                                <option value="" disabled selected>Sila Pilih Tindakan</option>
                                                <option value="1">Lulus</option>
                                                <option value="2">Tidak Lulus</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            Keterangan Tindakan
                                        </div>
                                        <div class="col-8">
                                            <textarea @if($data['main']->fk_lkp_status != 19) disabled @endif class="form-control" name="keterangan" id="" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <a href="{{ url('/hall/janasebutharga', Crypt::encrypt($data['main']->id)) }}" target="_blank" class="btn btn-success">Jana Sebut Harga</a>
                                            {{-- <a href="{{ url('/sport/resitonline', Crypt::encrypt($data['main']->id)) }}" target="_blank" class="btn btn-success mx-2">Jana Borang</a> --}}
                                        </div>
                                        <div>
                                            <button @if($data['main']->fk_lkp_status != 19) disabled @endif class="btn btn-primary btn-confirm-custom">Hantar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Kelulusan Tempahan</span>
                            <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Harini : {{ date("d F Y") }}</span>
                        </h3>
                        <form action="" method="post" id="form">
                            @csrf
                            <div class="card-toolbar">
                            </div>
                        </form>
                    </div>
                    <!--start::Senarai Slot Masa -->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Slot Masa</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        {{-- <div class="row d-flex justify-content-center"> --}}
                            {{-- <div class="col-8"> --}}
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="kt_datatable_1">
                                        <thead>
                                            <tr>
                                                {{-- <th>Bil</th> --}}
                                                <th>Lokasi</th>
                                                <th>Nama Fasiliti</th>
                                                <th>Tarikh Kegunaan</th>
                                                <th>Bilangan Hari</th>
                                                <th>Jenis Kegunaan</th>
                                                <th>Slot Kegunaan</th>
                                                <th class="text-right">Harga (RM)</th>
                                                {{-- <th class="text-right">Jumlah (RM)</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @php
                                                $i = 1;
                                                $totalAll = 0;
                                                $subtotalAll = 0;
                                                $totalHall = 0;
                                                $totalEq = 0;
                                            @endphp
                                            @if(count($data['bh_booking']) > 0)
                                                @foreach($data['bh_booking'] as $p)
                                                <tr>
                                                    {{-- <td style="width: 2%;">{{ $i++ }}</td> --}}
                                                    <td>PPJ</td>
                                                    <td>{{ Helper::getHall($p->fk_bh_hall) }}</td>
                                                    <td><b>{{ Helper::date_format($p->bb_start_date) }}</b> sehingga <b>{{ Helper::date_format($p->bb_end_date) }}</b></td>
                                                    <td>{{ $p->bb_no_of_day }}</td>
                                                    <td>{{ Helper::get_lkp_event($data['booking_detail'][0]->fk_lkp_event) }}</td>
                                                    {{-- <td>{{ Helper::date_format($p->bb_end_date) }}</td> --}}
                                                    <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }} - {{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                                    <td class="text-right">{{ Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)) }}</td>
                                                    {{-- <td>{{ ($p->bb_no_of_day) }}</td> --}}
                                                    {{-- <td class="text-right">{{ Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)*$p->bb_no_of_day) }}</td> --}}
                                                </tr>
                                                @endforeach
                                                {{-- <tr class="font-weight-bolder text-right">
                                                    <td colspan="7">Jumlah(RM)</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr> --}}
                                            @else
                                                <tr>
                                                    <td colspan="7">Tiada Maklumat Dewan</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td colspan="6" class="text-right"><b>Jumlah Keseluruhan</b></td>
                                                <td class="text-right"><b>{{ Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)*$p->bb_no_of_day) }}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            {{-- </div> --}}
                        {{-- </div> --}}
                        <!--end: Datatable-->
                    </div>
                    <!--end::Senarai Slot Masa -->
                    <!--start::Senarai Kelengkapan -->
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Senarai Kelengkapan</span>
                        </h3>
                    </div>
                    <div class="card-body pt-0">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Penggunaan</th>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Item</th>
                                        <th class="text-right">Kuantiti</th>
                                        <th class="text-right">Harga Seunit(RM)</th>
                                        <th class="text-right">Jumlah (RM)</th>
                                    </tr>
                                </thead>
                                @php
                                    $j = 1;
                                @endphp
                                <tbody>
                                    @if(count($data['kelengkapan']) > 0)
                                        @php
                                            $eq_total = 0;
                                            $eq_subtotal = 0;
                                        @endphp
                                        @foreach($data['kelengkapan'] as $p)
                                            @php
                                                // $eq_total += $p->bbe_total;
                                                $eq_total += $p->bbe_price;
                                                $eq_subtotal += $p->bbe_total;
                                                // $eq_subtotal += $p->bbe_subtotal;
                                            @endphp
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                            {{-- <td>{{ $data['booking_detail']->bbd_event_name }}</td> --}}
                                            <td>{{ Helper::get_lkp_event($data['booking_detail'][0]->fk_lkp_event) }}</td>
                                            <td>{{ $p->bbe_booking_date }}</td>
                                            <td>{{ Helper::get_equipment_hall($p->fk_bh_equipment) }}</td>
                                            <td class="text-right">{{ $p->bbe_quantity }}</td>
                                            <td class="text-right">{{ $p->bbe_price }}</td>
                                            <td class="text-right">{{ $p->bbe_total }}</td>
                                        </tr>
                                        @endforeach
                                        <tr class="font-weight-bolder text-right">
                                            <td colspan="6">Jumlah Keseluruhan(RM)</td>
                                            {{-- <td class="text-right"><b>{{ Helper::moneyhelper($eq_total) }}</b></td> --}}
                                            <td class="text-right"><b>{{ Helper::moneyhelper($eq_subtotal) }}</b></td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="10">Tiada Data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                    </div>
                    <!--end::Senarai Kelengkapan -->
                </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
@include('hall.dashboard.js.pengesahan')
@endsection
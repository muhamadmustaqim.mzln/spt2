@extends('layouts.master')
@section('container')

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Laporan Pemeriksaan Dan Pemantauan Dewan Dan Auditorium PPJ</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Laporan Pemeriksaan Dan Pemantauan Dewan dan Auditorium PPj</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->		
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <!--begin::List Widget 21-->
                                <div class="card card-custom gutter-b">
                                    <!--begin::Body-->
                                    <div class="card-header d-flex justify-content-center flex-wrap border-0 pt-6 pb-0">
                                        <div class="card-title text-center">
                                            <span class="card-label font-weight-bolder text-dark mb-1 h3">Laporan Pemeriksaan Dan Pemantauan Dewan Dan Auditorium PPJ</span>
                                        </div>
                                    </div>
                                    <div class="card-body pt-1">
                                        <hr>
                                        <div class="card-body pt-2">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                                    <td colspan="3">{{ $data['user']->fullname }}</td>
                                                </tr>
                                                <tr>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                                </tr>
                                                <tr>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                                    <td>{{ date('d-m-Y') }}</td>
                                                    <th colspan="2" class="text-center text-light" style="background-color: #242a4c">Tempoh Penggunaan</th>
                                                </tr>
                                                <tr>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Lokasi</th>
                                                    <td>{{ Helper::getHall($data['bh_booking']->fk_bh_hall) }}</td>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Dari</th>
                                                    <td>{{ Helper::date_format($data['bh_booking_detail']->bbd_start_date) }}</td>
                                                </tr>
                                                <tr>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Tujuan Penggunaan</th>
                                                    <td>{{ $data['bh_booking_detail']->bbd_event_description }}</td>
                                                    <th width="20%" class="text-light" style="background-color: #242a4c">Hingga</th>
                                                    <td>{{ Helper::date_format($data['bh_booking_detail']->bbd_end_date) }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <hr>
                                        <form action="" method="post" id="form">
                                            @csrf
                                            {{-- <div class="mb-10 font-weight-bolder text-dark bg-secondary px-8 py-3">Pemeriksaan</div> --}}
                                            <div class="px-7" data-wizard-type="step-content" data-wizard-state="current">
                                                <!--begin::Jenis Pemohon-->
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="font-weight-bolder my-auto">Tarikh <span class="text-danger">*</span></label>
                                                        <div class="col-3">
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control font-weight-bold mt-3" id="kt_datepicker" name="tarikhPemeriksaan" placeholder="Tarikh Pemeriksaan" autocomplete="off" required value="{{ isset($data['tarikhMula']) ? Helper::date_format($data['tarikhMula']) : '' }}" />
                                                                <span>
                                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <label class="font-weight-bolder my-auto">Masa Pemeriksaan<span class="text-danger">*</span></label>
                                                        <div class="col-3">
                                                            <div class="input-group">
                                                                <input class="form-control font-weight-bold mt-3" id="kt_timepicker_1" placeholder="Masa Pemeriksaan" readonly="readonly" name="masaPemeriksaan" value="" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-2 my-auto">
                                                            <label class="font-weight-bolder">Pegawai Terlibat <span class="text-danger">*</span></label>
                                                        </div>
                                                        <div class="col-8">
                                                            <select class="form-control selectpicker" name="pegawai[]" title="Sila Pilih Pegawai Yang Terlibat" multiple>
                                                                <option value="NIK ABDULLAH RAHIMI BIN NIK MOHD DIN">NIK ABDULLAH RAHIMI BIN NIK MOHD DIN</option>
                                                                <option value="MAZLAN BIN MAT ISA">MAZLAN BIN MAT ISA</option>
                                                                <option value="MAZLAN BIN MASRUPI">MAZLAN BIN MASRUPI</option>
                                                                <option value="SAHRIN BIN SELAMAT">SAHRIN BIN SELAMAT</option>
                                                                <option value="KAHIRUL FAIZI BIN MOHD KHORISH">KAHIRUL FAIZI BIN MOHD KHORISH</option>
                                                                <option value="ROZILAWATI BINTI ISMAIL">ROZILAWATI BINTI ISMAIL</option>
                                                                <option value="FARRAH WAHIDA BT HARIS">FARRAH WAHIDA BT HARIS</option>
                                                                <option value="MOHAMMAD AFIQ BIN MD ZAINI">MOHAMMAD AFIQ BIN MD ZAINI</option>
                                                                <option value="MOHD FAREEZ IZHAN BIN ABD RAHMAN">MOHD FAREEZ IZHAN BIN ABD RAHMAN</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5 class="mt-2 font-weight-bolder bg-dark text-white text-center px-8 py-3">Borang Pemeriksaan</h5>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Dewan Persidangan</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios1" checked="checked"/>
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios1"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Auditorium / Panggung Mawar Sari</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios2" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios2"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Lokasi Penyediaan (Dapur dan Siar Kaki)</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios3" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios3"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Kebersihan</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios4" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios4"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Peralatan Audio Visual dan Kelengkapan</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios5" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios5"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed mb-2" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Peralatan (Pentas/Meja/Kerusi dll)</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios6" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios6"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="card card-custom card-collapsed" data-card="true" id="kt_card_4">
                                                    <div class="card-header">
                                                        <div class="card-title">
                                                            <h3 class="card-label">Lain-lain</h3>
                                                        </div>
                                                        <div class="card-toolbar">
                                                            <a href="#" class="btn btn-icon btn-sm btn-light-primary" data-card-tool="toggle">
                                                            <i class="ki ki-arrow-down icon-nm"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="" class="font-weight-bolder">Keadaan</label>
                                                        <div class="row m-2">
                                                            <div class="radio-inline">
                                                                <label class="radio radio-success">
                                                                    <input type="radio" name="radios7" checked="checked" />
                                                                    <span></span>
                                                                    Baik
                                                                </label>
                                                                <label class="radio radio-danger">
                                                                    <input type="radio" name="radios7"/>
                                                                    <span></span>
                                                                    Tidak Baik
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label for="" class="font-weight-bolder">Catatan</label>
                                                        <textarea name="catatanDewan" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                {{-- <div class="table-responsive">
                                                    <table class="table table-bordered mt-5" id="kt_datatable_2">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center" colspan="2">Perkara</th>
                                                                <th class="text-center" style="width: 10%">Baik</th>
                                                                <th class="text-center" style="width: 10%">Tidak</th>
                                                                <th class="text-center" style="width: 40%">Catatan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1.</td>
                                                                <td>Dewan Persidangan</td>
                                                                <td class="">
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="dewan" class="checkbox-group" value="1" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="dewan" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanDewan" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2.</td>
                                                                <td>Auditorium / Panggung Mawar Sari</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="auditorium" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="auditorium" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanAuditorium" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3.</td>
                                                                <td>Lokasi Penyediaan (Dapur dan Siar Kaki)</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="penyediaan" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="penyediaan" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanPenyediaan" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>4.</td>
                                                                <td>Kebersihan</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="kebersihan" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="kebersihan" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanKebersihan" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>5.</td>
                                                                <td>Peralatan Audio Visual dan Kelengkapan</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="kelengkapan" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="kelengkapan" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanKelengkapan" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>6.</td>
                                                                <td>Peralatan (Pentas/Meja/Kerusi dll)</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="peralatan" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="peralatan" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanPeralatan" class="form-control"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td>7.</td>
                                                                <td>Lain-lain</td>
                                                                <td>
                                                                    <label class="checkbox checkbox-success checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="lainlain" class="checkbox-group" value="1"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label class="checkbox checkbox-danger checkbox-lg d-flex justify-content-center">
                                                                        <input type="checkbox" name="lainlain" class="checkbox-group" value="2"/>
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td><textarea name="catatanLainlain" class="form-control"></textarea></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div> --}}
                                            </div>
                                            <hr>
                                            <br>
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bolder" >Ulasan Keseluruhan</label>
                                                <div class="col-lg-6">
                                                    <textarea name="overall" class="form-control" rows="7"></textarea>
                                                    <span class="form-text text-muted">*Ulasan Terperinci.</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bolder" >Disediakan Oleh<span class="text-danger">*</span> :</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="namaPemeriksa" name="namaPemeriksa" autocomplete="off" placeholder="Nama Pemeriksa" value="" required/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label text-right col-lg-2 col-sm-12 text-lg-right font-weight-bolder">Disahkan Oleh<span class="text-danger">*</span> :</label>
                                                <div class="col-lg-4 col-md-9 col-sm-12">
                                                    <select class="form-control selectpicker" title="Sila Pilih Pengesah">
                                                        <optgroup label="Pegawai" data-max-options="2">
                                                            <option>ROZILAWATI BINTI ISMAIL</option>
                                                            <option>FARRAH WAHIDA BT HARIS</option>
                                                            <option>MOHAMMAD AFIQ BIN MD ZAINI</option>
                                                        </optgroup>
                                                        {{-- <optgroup label="Camping" data-max-options="2">
                                                            <option>Tent</option>
                                                            <option>Flashlight</option>
                                                            <option>Toilet Paper</option>
                                                        </optgroup> --}}
                                                    </select>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bolder" >Disahkan Oleh<span class="text-danger">*</span> :</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="Pengesah" name="Pengesah" autocomplete="off" placeholder="Nama Pengesah" value="" required/>
                                                </div>
                                            </div> --}}
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bolder" >Pemulangan Deposit<span class="text-danger">*</span> :</label>
                                                <div class="col-lg-6" required>
                                                    <input type="radio" id="penuh" name="deporeturn" value="Penuh">
                                                    <label for="penuh" class="mx-3">Penuh</label><br>
                                                    <input type="radio" class="my-auto" id="separuh" name="deporeturn" value="Separuh">
                                                    <label for="separuh" class="mx-3">Separuh</label><br>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2 float-right">Simpan</button>
                                        </form>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 21-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
@endsection

@section('js_content')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
    @include('hall.dashboard.js.pemeriksaan')
@endsection
@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                            <td>{{ $data['user']->fullname }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                            <td>{{ $data['main']->bmb_booking_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                            <td>{{ date('d-m-Y') }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                            <td>{{ $data['user']->email }}</td>
                        </tr>
                    </table>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom mb-5 pb-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        {{-- <a href="{{ url('hall/slot', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}" class="btn btn-light-info font-weight-bolder mr-2 mt-2"><i class="flaticon-add-circular-button"></i> Tambah Kegunaan
                        </a> --}}
                        <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <form action="" method="post">
                    @csrf
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="table-responsive">
                            <table class="table table-bordered" id="kt_datatable_2">
                                <thead>
                                    <tr>
                                        <th>Bil</th>
                                        <th>Nama Fasiliti</th>
                                        <th>Slot Masa</th>
                                        <th>Tarikh Penggunaan</th>
                                        <th>Lokasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($data['datesBetween'] as $s)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $data['fasility']->bh_name }}</td>
                                            <td>08:00 am - 04:00 pm</td>
                                            <td>{{ Helper::date_format($s) }}</td>
                                            <td>{{ Helper::location($data['fasility']->fk_lkp_location) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable-->
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <h3 class="card-title align-items-start flex-column mb-5">
                            <span class="card-label font-weight-bolder text-dark">Maklumat Tempahan</span>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-3 col-form-label">Kategori Kapasiti Dewan</label>
                            <div class="col-9 col-form-label">
                                <div class="radio-list">
                                <label  class="col-8 col-form-label">@if ($data['bh_booking'][0]->fk_bh_hall_usage != null){{ Helper::get_hall_usage_capacity($data['bh_booking'][0]->fk_bh_hall_usage) }}@endif</label>									
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Jenis Acara</label>
                            <div class="col-9">
                            <label  class="col-8 col-form-label">{{ Helper::get_lkp_event($data['booking_detail'][0]->fk_lkp_event) }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Nama Acara</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">{{ $data['booking_detail'][0]->bbd_event_name }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Keterangan Acara</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">{{ $data['booking_detail'][0]->bbd_event_description }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Sesi</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">Siang - (08:00 am - 04:00 pm)</label>									
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Masa Mula</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">{{ date('H:i', strtotime($data['booking_detail'][0]->bbd_start_time)) }}</label>									
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Masa Tamat</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">{{ date('H:i', strtotime($data['booking_detail'][0]->bbd_end_time)) }}</label>									
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-2 col-form-label">Bilangan Peserta</label>
                            <div class="col-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">VVIP</span></div>
                                    <input type="number" class="form-control" placeholder="Jumlah" value="{{ $data['booking_detail'][0]->bbd_vvip }}" readonly/>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">VIP</span></div>
                                    <input type="number" class="form-control" placeholder="Jumlah" value="{{ $data['booking_detail'][0]->bbd_vip }}" readonly/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">Peserta Lain</span></div>
                                    <input required type="number" class="form-control" placeholder="Jumlah" value="{{ $data['booking_detail'][0]->bbd_participant }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Jenis Tempahan</label>
                            <div class="col-9">
                                <label  class="col-8 col-form-label">{{ Helper::getJenisTempahan($data['main']->fk_lkp_discount_type) }}</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">Nama Pemohon</label>
                            <div class="col-9">
                                <label class="col-8 col-form-label" type="text" name="namaPemohon">{{ $data['user']->fullname }}</label>
                                {{-- <input readonly type="text" class="form-control border-0" name="namaPemohon" value="{{ $data['user']->fullname }}"> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-3 col-form-label">No. Telefon</label>
                            <div class="col-9">
                                <label class="col-8 col-form-label" type="tel" pattern="[0-9]{10-11}" placeholder="Enter Phone Number" name="noTelefonInput">{{ $data['user_detail']->bud_phone_no }}</label>
                                {{-- <small id="noTelefonHelp" class="form-text text-muted">Format: 0123456789</small> --}}
                            </div>
                        </div>
                        @if ($data['numberOfDays'] > 1)
                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="button" id="tambahKelengkapan" class="btn btn-warning font-weight-bolder mt-2 float-right">
                                        <i class="flaticon-plus mr-2"></i>Kelengkapan Harian
                                    </button>
                                </div>
                            </div>
                        @endif
                        {{-- 1 = keseluruhan, 2 = harian --}}
                        <input hidden class="form-group row" value="{{$data['numberOfDays']}}" id="numberOfDays" name="numberOfDays"/>
                        <input hidden type="text" id="statusKelengkapan" name="statusKelengkapan" value="1"/>
                        <div class="form-group row" id="kelengkapanKeseluruhan">
                            <label  class="col-3 col-form-label">Tambah Kelengkapan Dewan</label>
                            <div class="col-9">	
                                <table class="table-bordered w-100">
                                    <thead>
                                        <tr>
                                            <td class="text-center" style="width: 20%"></td>
                                            <td style="" class="text-center py-3">Sepanjang Program</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Meja Bulat (Kuantiti Maksima 100)</div></td>
                                            <td class="text-center"><input type="number" class="form-control" min="0" max="100" name="jumlahMejaBulat[]" value="0"/></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Meja Panjang (Kuantiti Maksima 100)</div></td>
                                            <td class="text-center"><input type="number" class="form-control" min="0" max="100" name="jumlahMejaPanjang[]" value="0"/></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Kerusi Banquet (Kuantiti Maksima 1000)</div></td>
                                            <td class="text-center"><input type="number" class="form-control" min="0" max="1000" name="jumlahKerusi[]" value="0"/></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">LCD Projector dan Layar Putih (Kuantiti Maksima 3)</div></td>
                                            <td class="text-center"><input type="number" class="form-control" min="0" max="3" name="jumlahLCDlayar[]" value="0"/></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="kelengkapanHarian">
                            <label class="col-3 col-form-label">Tambah Kelengkapan Dewan</label>
                            <div class="col-9">	
                                <table class="table-bordered w-100">
                                    <thead>
                                        @php
                                            $initial = 80;
                                            $divided = $initial / $data['numberOfDays'];
                                        @endphp
                                        <tr>
                                            <td class="text-center" style="width: 20%"></td>
                                            @for ($i = 1; $i <= $data['numberOfDays']; $i++)
                                                <td style="width: {{ $divided }}%" class="text-center py-3">Hari - {{ $i }}</td>
                                            @endfor
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Meja Bulat (Kuantiti Maksima 100)</div></td>
                                            @for ($i = 1; $i <= $data['numberOfDays']; $i++)
                                                <td class="text-center"><input type="number" class="form-control" min="0" max="100" name="jumlahMejaBulatHarian[]" value="0"/></td>
                                            @endfor
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Meja Panjang (Kuantiti Maksima 100)</div></td>
                                            @for ($i = 1; $i <= $data['numberOfDays']; $i++)
                                                <td class="text-center"><input type="number" class="form-control" min="0" max="100" name="jumlahMejaPanjangHarian[]" value="0"/></td>
                                            @endfor
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">Kerusi Banquet (Kuantiti Maksima 1000)</div></td>
                                            @for ($i = 1; $i <= $data['numberOfDays']; $i++)
                                                <td class="text-center"><input type="number" class="form-control" min="0" max="1000" name="jumlahKerusiHarian[]" value="0"/></td>
                                            @endfor
                                        </tr>
                                        <tr>
                                            <td class="text-center" style="width: 20%"><div class="input-group-text">LCD Projector dan Layar Putih (Kuantiti Maksima 3)</div></td>
                                            @for ($i = 1; $i <= $data['numberOfDays']; $i++)
                                                <td class="text-center"><input type="number" class="form-control" min="0" max="3" name="jumlahLCDlayarHarian[]" value="0"/></td>
                                            @endfor
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-primary font-weight-bolder mt-2 float-right">
                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                        </button>
                        {{-- <a href="{{ url('/hall/rumusan')/*, Crypt::encrypt($data['booking']))*/ }}" class="btn btn-primary font-weight-bolder mt-2 float-right">
                            <i class="fas fa-check-circle"></i> Teruskan Tempahan
                        </a> --}}
                    </div>
                </form>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('hall.public.js.tempahan')
@endsection

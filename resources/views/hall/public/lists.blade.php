@extends('layouts.master')
<style>
.active1 {
	background-color: #3445E5 !important;
	color: #FFFFFF !important;
	border-color: transparent;
}
.column {
	display: none; /* Hide all elements by default */
}
.show {
	display: block;
}
</style>
@section('container')

<!--begin::Content-->
	@if(Session::has('flash'))
		<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
	@endif
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
		<!--begin::Subheader-->
		<div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
			<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Tempahan Dewan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Tempahan Dewan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
			</div>
		</div>
		<!--end::Subheader-->
		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="owl-carousel owl-theme">
							@foreach($data['pengumuman'] as $p)
							<div class="item">
								<!--begin::Notice-->
								<div class="alert alert-custom alert-primary" style="min-height:" role="alert">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">
									<h3 class="card-label font-weight-bolder text-light mb-1">{{ $p->ba_name }}</h3>
									<hr>
									{{ $p->ba_detail }}
									</div>
								</div>
								<!--end::Notice-->
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b" style="background-color: #20274d; background-position: right bottom; background-size: auto 50%; background-repeat: no-repeat; background-image: url({{ asset('assets/media/svg/patterns/rhone-2.svg') }})">
							<!--begin::Header-->
							<div class="card-header border-0 pt-5">
								<h3 class="card-title align-items-start flex-column mb-5">
									<span class="card-label font-weight-bolder text-white mb-1">Tapisan Data</span>
									<span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
								</h3>
							</div>
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body pt-2">
								<form action="" method="post">
								@csrf
								<div class="form-group row">
									<div class="col-lg-12">
										<div class="input-icon">
											<select name="fasiliti" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="fasiliti" required>
												<option value="1" @if($data['fasiliti'] && $data['fasiliti'] == 1) selected @endif>PPJ</option>
												{{-- <option value="2">Marina</option>
												<option value="5">PICC</option> --}}
												{{-- <option value="5" @if($data['fasiliti'] && $data['fasiliti'] == 5) selected @endif>Presint 8</option>
												<option value="6" @if($data['fasiliti'] && $data['fasiliti'] == 6) selected @endif>Presint 9</option>
												<option value="7" @if($data['fasiliti'] && $data['fasiliti'] == 7) selected @endif>Presint 11</option>
												<option value="8" @if($data['fasiliti'] && $data['fasiliti'] == 8) selected @endif>Presint 16</option> --}}
											</select>
											<span>
												<i class="flaticon-search-1 icon-xl"></i>
											</span>
										</div>
									</div>
									{{-- <div class="col-lg-12">
										<div class="input-icon">
											<select name="lokasi" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="lokasi" required>
												<option value=""> Pilih Fasiliti Dewan</option>
												@foreach ($data['hall'] as $l)
													<option value="{{ $l->id }}">{{ $l->bh_name }}</option>
												@endforeach
											</select>
											<span>
												<i class="flaticon-search-1 icon-xl"></i>
											</span>
										</div>
									</div> --}}
									<div class="col-lg-12">
										<div class="input-icon">
											<input type="text" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_daterangepicker_4" name="tarikhMula" placeholder="Tarikh Mula" autocomplete="off" required value="@if($data['carian'] != null){{ $data['carian']->tarikhMula }}@endif" />
											<span>
												<i class="flaticon-calendar-3 icon-xl"></i>
											</span>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input-icon">
											<input @if($data['carian'] == null) disabled @endif type="text" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="kt_daterangepicker_5" name="tarikhAkhir" placeholder="Tarikh Akhir" autocomplete="off" required value="@if($data['carian'] != null){{ $data['carian']->tarikhAkhir }}@endif" />
											<span>
												<i class="flaticon-calendar-3 icon-xl"></i>
											</span>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input-icon">
											<select name="kapasiti" class="form-control form-control-lg border-0 font-weight-bold mt-3" id="" >
												<option value="" selected disabled> Pilih Kapasiti</option>
												<option value=".200" @if($data['carian'] && $data['carian']->kapasiti == '.200') selected @endif>200 ke bawah</option>
												<option value=".400" @if($data['carian'] && $data['carian']->kapasiti == '.400') selected @endif>201 hingga 400</option>
												<option value=".600" @if($data['carian'] && $data['carian']->kapasiti == '.600') selected @endif>401 hingga 600</option>
												<option value=".800" @if($data['carian'] && $data['carian']->kapasiti == '.800') selected @endif>601 ke atas</option>
											</select>
											<span>
												<i class="flaticon-search-1 icon-xl"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="float-right">
									<button type="submit" id="submit_form" class="btn btn-light-primary  font-weight-bold mr-2">Carian</button>
								</div>
							</form>
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b">
							<!--begin::Header-->
							<div class="card-header border-0 pt-5">
								<h3 class="card-title align-items-start flex-column mb-5">
									<span class="card-label font-weight-bolder text-dark mb-1">Kalendar Takwim</span>
								</h3>
							</div>
							<!--end::Header-->
							<!--begin::Body-->
							<div class="card-body pt-2">
								<div class="row">
									<div class="col-lg-12">
										<div id="kt_calendar"></div>
									</div>
								</div>
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
					</div>
					<div class="col-lg-8">
						<!--begin::List Widget 21-->
						<div class="card card-custom gutter-b">
							<div class="card-header">
								<div class="card-title">
									<h3 class="card-label">Senarai Tempahan Dewan</h3>
								</div>
								<div class="card-toolbar">
									<ul class="nav nav-success nav-bold nav-pills">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1"  data-toggle="tooltip" data-theme="dark" title="Grid"><i class="flaticon-grid-menu text-dark"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2" data-toggle="tooltip" data-theme="dark" title="List"><i class="la la-list text-dark icon-xl"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3"  data-toggle="tooltip" data-theme="dark" title="Peta"><i class="flaticon2-map text-dark"></i></a>
										</li>
									</ul>
								</div>
							</div>
							<!--begin::Body-->
							<div class="card-body pt-2">
								<div class="row mb-8 mt-6">
									<div class="col-lg-12">
										<p>
											<div id="myBtnContainer">
												<button type="button" class="btn btn-light-primary font-weight-bold mr-2 mt-2 active1" onclick="filterSelection('all')">Semua</button>
												@if(count($data['komuniti']) > 0)
													<button type="button" class="btn btn-light-primary font-weight-bold mr-2 mt-2" onclick="filterSelection('komuniti')">Dewan Komuniti</button>
												@endif
												@foreach ($data['location'] as $l)
													<button type="button" class="btn btn-light-primary font-weight-bold mr-2 mt-2" onclick="filterSelection('<?= $l->lc_description ?>')">{{ $l->lc_description }}</button>									
												@endforeach
											</div>
										</p>
									</div>
									{{-- <div class="col-lg-3">
										<div class="input-icon">
											<input type="text" class="form-control mt-2" placeholder="Carian..." />
											<span>
												<i class="flaticon2-search-1 icon-md"></i>
											</span>
										</div>
									</div> --}}
								</div>
								<div class="tab-content">
									@if ($data['ppj'] == true)
										<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">
											<div class="row">
												@foreach ($data['hall'] as $f)
													<!--begin::Product-->
													<div class="col-md-4 col-xxl-4 col-lg-12 column {{ Helper::location($f->fk_lkp_location) }}">
														<!--begin::Card-->
														<div class="card card-custom gutter-b" style="box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 50%)">
															<div class="overlay">
																<div class="overlay-wrapper bg-light text-center">
																	<img src="{{ URL::asset("{$f->bh_file_location}/{$f->bh_filename}") }}" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />
																</div>
																<div class="overlay-layer">
																	<a href="{{ url('hall/details', ['id' => Crypt::encrypt($f->id)] + ($data['carian'] ? ['dateStart' => Crypt::encrypt($data['carian']->tarikhMula), 'dateEnd' => Crypt::encrypt($data['carian']->tarikhAkhir), 'fasility' => Crypt::encrypt($data['carian']->fasiliti)] : [])) }}" class="btn font-weight-bolder btn-sm btn-light-primary">Teruskan Tempahan </a>
																</div>
															</div>
															<div class="card-body">
																<a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}" class="font-size-h6 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{ $f->bh_name }}</a>
																<p class="text-muted font-weight-bold">{{ $f->bh_percint }}</p>
																<p><span class="text-center"><b>Harga:</b> bermula dari <span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::get_harga_dewan($f->id) }}</span> / Hari</span></p>
																<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->bh_status) }} label-inline">{{ Helper::status($f->bh_status) }}</span></p>
																<br>
																<p class="text-center"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
																<p class="text-center"><a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-outline-primary mt-4">Teruskan Tempahan</a></p>
															</div>
														</div>
														<!--end::Card-->
													</div>
													<!--end::Product-->
												@endforeach
											</div>
										</div>
										<div class="tab-pane fade" id="kt_tab_pane_7_2" role="tabpanel" aria-labelledby="kt_tab_pane_7_2">
											<div class="row">
												@foreach ($data['hall'] as $f)
													<div class="col-lg-12 column {{ Helper::location($f->fk_lkp_location) }}">
													<!--begin::Item-->
													<div class="d-flex align-items-center pb-9">
														<!--begin::Symbol-->
														<div class="symbol symbol-150 symbol-2by3 flex-shrink-0 mr-4">
															<a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}"><div class="symbol-label" style="background-image: url('{{ URL::asset("{$f->bh_file_location}/{$f->bh_filename}") }}')"></div></a>
														</div>
														<!--end::Symbol-->
														<!--begin::Section-->
														<div class="d-flex flex-column flex-grow-1">
															<!--begin::Title-->
															<a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}" class="text-dark-75 font-weight-bolder font-size-lg text-hover-primary mb-1">{{ $f->bh_name }}, {{ $f->bh_percint }}</a>
															<p><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
															{{ $f->bh_description }}
															<p class="mb-0"><b>Harga:</b> <span class="float-right">bermula dari <span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::get_harga_dewan($f->id) }}</span> / Hari</span></span></p>
															<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->bh_status) }} label-inline">{{ Helper::status($f->bh_status) }}</span></p>
															<!--end::Title-->
														</div>
														<!--end::Section-->
													</div>
													<!--end::Item-->
													</div>
												@endforeach
											</div>
										</div>
									@else
										<div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">
											<div class="row">
												@foreach ($data['hall'] as $f)
													<!--begin::Product-->
													<div class="col-md-4 col-xxl-4 col-lg-12 column {{ Helper::location($f->fk_lkp_location) }}">
														<!--begin::Card-->
														<div class="card card-custom gutter-b" style="box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 50%)">
															<div class="overlay">
																<div class="overlay-wrapper bg-light text-center">
																	<img src="{{ ("assets/upload/main/{$f->cover_img}") }}" alt="" style="height: 220px" class="mw-100 w-100 card-img-top" />
																</div>
																<div class="overlay-layer">
																	<a href="{{ url('hall/details',['id' => Crypt::encrypt($f->id), 'dateStart' => $data['carian']->tarikhMula]) }}" class="btn font-weight-bolder btn-sm btn-light-primary">Info </a>
																</div>
															</div>
															<div class="card-body">
																<a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}" class="font-size-h6 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{ $f->eft_type_desc }}</a>
																<p class="text-muted font-weight-bold">{{ Helper::location($f->fk_lkp_location) }}</p>
																<p><span class="text-center"><b>Harga:</b> bermula dari <span class="font-weight-boldest text-danger font-size-h6">RM {{ Helper::get_harga_dewan($f->id) }}</span> / Hari</span></p>
																<p><b>Status:</b> <span class="label font-weight-bolder float-right {{ Helper::status_color($f->eft_status) }} label-inline">{{ Helper::status($f->eft_status) }}</span></p>
																<br>
																<p class="text-center"><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i><i class="flaticon-star text-warning mr-2"></i></p>
																<p class="text-center"><a href="{{ url('hall/details',Crypt::encrypt($f->id)) }}" class="btn font-weight-bolder btn-outline-primary mt-4">Info</a></p>
															</div>
														</div>
														<!--end::Card-->
													</div>
													<!--end::Product-->
												@endforeach
											</div>
										</div>
									@endif
									<div class="tab-pane fade" id="kt_tab_pane_7_3" role="tabpanel" aria-labelledby="kt_tab_pane_7_3">
										<div id="map" style="height:500px;"></div>
									</div>
								</div>											
							</div>
							<!--end::Body-->
						</div>
						<!--end::List Widget 21-->
					</div>
				</div>
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
		
	</div>
	<!--end::Content-->
	<!-- Button trigger modal-->
	<!-- Modal-->
	<div class="modal fade" id="exampleModalCustomScrollable" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
			<div class="modal-content" style="background-position: right top; background-size: auto 50%; background-repeat: no-repeat; background-image: url({{ asset('assets/media/svg/shapes/abstract-2.svg') }})">
				<div class="modal-header border-0">
					<h5 class="modal-title font-weight-bolder" id="exampleModalLabel">Pengumuman</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i aria-hidden="true" class="ki ki-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<div data-scroll="true" data-height="200">
						<ul>						
							@foreach($data['pengumuman'] as $p)
							<li>
								<b>{{ $p->ba_name }}</b> <br> {{ $p->ba_detail }}
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js_content')
    @include('hall.public.js.lists')
@endsection
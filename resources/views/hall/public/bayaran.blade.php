@extends('layouts.master')
@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Bayaran Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Bayaran FPX</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Transaksi FPX</th>
                            <td>{{ $data['oderNo'] }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Bayaran (RM)</th>
                            <td>{{ Helper::moneyhelper($data['total']) }}</td>
                        </tr>
                    </table>
                    <form action="https://fpxuat.ppj.gov.my/pay/pay" method="post">

                        <button type="submit" class="btn btn-primary float-right font-weight-bolder mt-2"><i class="
                            fas fa-check-circle"></i>Buat Pembayaran</button>
                        <input type="hidden" name="token" value="{{ $response['token'] }}">
                    </form>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->


        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

					



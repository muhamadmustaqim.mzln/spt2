@extends('layouts.master')
<style>
    .img-container{
        position: relative;
        text-align: center;
        color: white;
        background-color: rgba(0,0,0,1);
    }

    .img-container img {
        object-fit: cover;
        filter: brightness(.5);
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

    }

    #chat-bot-launcher-container{
        z-index: 1000 !important;
    }
    
    .highlight {
        background-color: #F1F3FF; /* Set your desired background color for highlighting */
    }
    
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@section('container')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid"  id="kt_content">		
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mobile-ver" style="display:none">
                <div id="carouselExampleIndicators" class="carousel slide gutter-b" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" height="370px" src="{{ asset('assets/media/default-image.jpg') }}" alt="First slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>	
                </div>
                <div class="col-lg-12">	
                    <div class="card card-custom gutter-b">
                        <form action="" method="post">
                            @csrf
                            <div class="card-body pb-5">
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="card-title font-weight-bolder">Tempahan {{ Helper::getHall($data['id']) }}, {{ Helper::location($data['lkp_location']) }}</h3>
                                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                                        <input type="hidden" name="lkp_location" value="{{ $data['lkp_location'] }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3"><label>1. Sahkan Tarikh Tempahan</label></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <div class="input-icon">
                                                <input readonly type="text" class="form-control form-control-md form-control-solid font-weight-bold mb-3" id="" name="bbd_start_date" placeholder="" autocomplete="off" required value="{{ $data['tarikhMula'] }}" />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-4 col-xl-3">
                                            <div class="input-icon">
                                                <input readonly type="text" class="form-control form-control-md form-control-solid font-weight-bold mb-3" id="" name="bbd_end_date" placeholder="" autocomplete="off" required value="{{ $data['tarikhAkhir'] }}" />
                                                <span>
                                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>2. Pilih Kategori Anda <span class="text-danger">*</span></label></div>
                                        <div class="col-9">
                                            <select name="kategoriTempahan" id="" class="form-control form-control-md mt-3" required>
                                                <option value="1">Permohonan Luar Biasa</option>
                                                <option value="2">Permohonan Anggota PPj, K/Tangan & Pesara PPj, Anggota PPj, Pegawai Kader & Kontrak PPj</option>
                                                <option value="3">Pemohon Warga Kerja Kementerian Wilayah Persekutuan</option>
                                                <option value="4">Permohonan Kementerian & Agensi Kerajaan IPTA/ Sekolah</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3"><label>3. Pilih Kegunaan Dewan </label></div>
                                        <div class="col-9">
                                            <table class="table table-bordered" id="kt_datatable_2">
                                                <thead>
                                                    <tr>
                                                        <td>Bankuet</td>
                                                        <td>Harga**</td>
                                                        <td><input checked type="radio" id="html" name="kegunaanDewan" value="1"></td>
                                                    </tr>
                                                </thead>
                                                <thead>
                                                    <tr>
                                                        <td>Seminar</td>
                                                        <td>Harga**</td>
                                                        <td><input type="radio" id="html" name="kegunaanDewan" value="2"></td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3 my-auto"><label>4. Pilih Jenis Acara <span class="text-danger">*</span></label></div>
                                        <div class="col-9">
                                            <select name="fk_lkp_event" id="" class="form-control form-control-md mt-3" >
                                                <option value="1">Persidangan</option>
                                                <option value="2">Konvensyen/ Kongres</option>
                                                <option value="3">Hiburan (Pertunjukan/ Makan Malam/ Majlis Anugerah/ Pertandingan)</option>
                                                <option value="4">Pameran</option>
                                                <option value="5">Penggambaran/ Pemfileman</option>
                                                <option value="6">Mesyuarat</option>
                                                <option value="7">Seminar</option>
                                                <option value="8">Majlis Perkahwinan</option>
                                                <option value="9">Lain-lain</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 my-auto"><label>5. Maklumat Acara</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" autocomplete="off" style="width: 150px" type="button">Nama Acara</div>
                                                </div>
                                                <input name="bbd_event_name" type="text" class="form-control" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" autocomplete="off" style="width: 150px" type="button">Keterangan Acara</div>
                                                </div>
                                                <input name="bbd_event_description" type="text" class="form-control" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" style="width: 150px" type="button">Sesi</div>
                                                </div>
                                                <select name="fk_lkp_time_session" id="" class="form-control form-control-md" >
                                                    <option value="1">Siang - (08:00 am - 04:00 pm)</option>
                                                    <option value="2">Malam - (04:00 pm - 12:00 pm)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" autocomplete="off" style="width: 150px" type="button">Nama Pemohon</div>
                                                </div>
                                                <input name="bbd_user_apply" type="text" class="form-control" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" autocomplete="off" style="width: 150px" type="button">No Telefon</div>
                                                </div>
                                                <input name="bbd_contact_no" type="text" class="form-control" required />
                                            </div>
                                        </div>
                                    </div>
                                </div><div class="form-group">
                                    <div class="row">
                                        <div class="col-12 mt-5"><label>6. Maklumat Bilangan Peserta</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 py-2">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" style="width: 110px" type="button">VVIP</div>
                                                </div>
                                                <input name="bbd_vvip" type="number" min="0" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 py-2">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" style="width: 110px" type="button">VIP</div>
                                                </div>
                                                <input name="bbd_vip" type="number" min="0" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-4 py-2">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="btn btn-secondary btn-hover-secondary text-left" style="width: 110px" type="button">Peserta Lain</div>
                                                </div>
                                                <input name="bbd_participant" type="number" min="1" class="form-control" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-end mt-10">
                                    <div class="row">
                                        <button type="button" class="btn border-dark font-weight-bold mb-7 ml-2">Padam</button>
                                        <button type="submit" class="btn btn-primary font-weight-bold mb-7 ml-2">Teruskan Tempahan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
    
</div>
<!--end::Content-->

@endsection

@section('js_content')
    {{-- @include('hall.public.js.details') --}}
    {{-- @include('event.public.js.details') --}}
    @include('hall.public.js.reservation')
@endsection

<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
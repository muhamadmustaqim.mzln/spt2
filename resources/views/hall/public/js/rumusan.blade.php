
<script>
    $(document).ready(function(){
        $('#JenisAcaraTempDewan').on('change', function() {
            var selectedText = $(this).find('option:selected').text();
            var selectedValue = $(this).val();
            if (selectedText.trim() === 'Majlis Perkahwinan') {
                $('#pakejPerkahwinan').prop('hidden', false);
                $('#pakejPerkahwinanSelect').attr('required', true);
            } else {
                $('#pakejPerkahwinan').prop('hidden', true);
                $('#pakejPerkahwinanSelect').prop('required', false).removeAttr('required');
            }
        });
        $('#jenisTempahan').on('change', function() {
            var jenisTempahan = $(this).val();
            console.log(jenisTempahan)
            if(jenisTempahan != 1 && jenisTempahan != 5) {
                $('#docSokongan').removeAttr('hidden');
            } else {
                $('#docSokongan').attr('hidden', true);
            }
        });
        $('#pengakuan').on('change', function() {
            if (this.checked) {
                $('#submitBtn').removeAttr('disabled');
            } else {
                $('#submitBtn').attr('disabled', true);
            }
        })
        $("#myForm").submit(function( event ) {
            event.preventDefault();
            var mbid = $('#mbid').val();
            var csrfToken = '{{ csrf_token() }}';
            var form = $(this);
            console.log('test')
            $.ajax({
                url:"{{ url('sport/validateSportSlot') }}",
                type: "POST",
                data: {'id' : mbid, _token: csrfToken},
                dataType: 'json',
                success: function(response) {
                    event.preventDefault();
                    // form.off('submit').submit();
                },
                error: function(error) {
                    event.preventDefault();
                    Swal.fire(
                        'Harap Maaf!',
                        error.responseJSON.message,
                        'error'
                    );
                }
            });
            var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
            if(atLeastOneIsChecked == false){
                event.preventDefault();
                Swal.fire(
                'Harap Maaf!',
                'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
                'error'
                );
            }
        });
    });
</script>
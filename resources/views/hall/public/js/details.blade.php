<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js?v=7.2.9') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-thumbnail.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-share.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-fullscreen.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-zoom.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-autoplay.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/lightgallery/js/lg-hash.js') }}"></script>
<script>
    lightGallery(document.getElementById('aniimated-thumbnials'), {
        selector: 'a',
        thumbnail:true
    });
</script>
<script>
    var events = "{{ url('takwim/hall', $data['fasility']->id) }}";
    console.log(events);
    "use strict";

    var KTCalendarBasic = function() {

    return {
        //main function to initiate the module
        init: function() {
            var todayDate = moment().startOf('day');
            var YM = todayDate.format('YYYY-MM');
            var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
            var TODAY = todayDate.format('YYYY-MM-DD');
            var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

            var calendarEl = document.getElementById('kt_calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                themeSystem: 'bootstrap',

                isRTL: KTUtil.isRTL(),

                header: {
                    left: 'prev,next ',
                    center: 'title',
                    right: 'today'
                },

                height: 800,
                contentHeight: 780,
                aspectRatio: 3, 

                nowIndicator: true,
                now: TODAY + 'T09:25:00', // just for demo

                views: {
                    dayGridMonth: { buttonText: 'month' }
                },

                defaultView: 'dayGridMonth',
                defaultDate: TODAY,
                locale: 'ms-my',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: events,

                eventRender: function(info) {
                    var element = $(info.el);

                    if (info.event.extendedProps && info.event.extendedProps.description) {
                        if (element.hasClass('fc-day-grid-event')) {
                            element.data('content', info.event.extendedProps.description);
                            element.data('placement', 'top');
                            KTApp.initPopover(element);
                        } else if (element.hasClass('fc-time-grid-event')) {
                            element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        } else if (element.find('.fc-list-item-title').lenght !== 0) {
                            element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
                        }
                    }
                }
            });

            calendar.render();
        }
    };
}();

jQuery(document).ready(function() {
    KTCalendarBasic.init();
});
</script>
<script>
    pannellum.viewer('panorama', {
        "type": "equirectangular",
        "panorama": "{{ URL::asset($data['fasility']->bh_file_location.'/'.$data['fasility']->bh_filename) }}",
        "preview": "{{ URL::asset($data['fasility']->bh_file_location.'/'.$data['fasility']->bh_filename) }}",
        "title": "{!! $data['fasility']->bh_name !!}, {!! Helper::location($data['fasility']->fk_lkp_location) !!}",
        "autoLoad" : true,
        "autoRotate" : 2
    });
</script>
<script>
    $('#kt_select2_4').on('change', function(){
    var location_id = $(this).val();
    if (location_id == ''){
        $('#kt_select2_5').prop('disabled', true);
    }
    else{
        $('#kt_select2_5').prop('disabled', false);
        $.ajax({
            url:"{{ url('sport/ajax') }}",
            type: "POST",
            data: {'location_id' : location_id},
            dataType: 'json',
            success: function(data){
                $('#kt_select2_5').html(data);
            }
        });
    }
});
</script>
<script>
    $(function() {

        // var blockedDates = ['2024-07-10', '2024-07-15', '2024-07-20'];
        var blockedDates = <?php echo $data['confirmed_booking'] ?>;
        console.log(blockedDates)
        $('#kt_daterangepicker_5').daterangepicker({
            autoApply: true,
            autoclose: true,
            // minDate: new Date(),
            minDate: moment().add(3, 'months'), 
            isInvalidDate: function(date) {
                // Convert the date to the format DD-MM-YYYY for comparison
            var formattedDate = date.format('YYYY-MM-DD');
                
                // Check if the date is in the blockedDates array
                return blockedDates.indexOf(formattedDate) !== -1;
            }
        }, function(start, end, label) {
        var isValidRange = true;

        // Iterate through each day in the selected range
        for (var d = start.clone(); d.isBefore(end); d.add(1, 'day')) {
            var formattedDate = d.format('YYYY-MM-DD');
            if (blockedDates.indexOf(formattedDate) !== -1) {
                isValidRange = false; // Range contains a blocked date
                break;
            }
        }

        if (!isValidRange) {
            // Show an error message or take any other action
            Swal.fire(
                'Harap Maaf!',
                'Tarikh yang dipilih telah ditempah. Sila pilih tarikh yang berlainan',
                'error'
                );
            $('#kt_daterangepicker_5').data('daterangepicker').setStartDate(moment());
            $('#kt_daterangepicker_5').data('daterangepicker').setEndDate(moment());
        } else {
            // Handle the valid range here
            $('#kt_daterangepicker_5 .form-control').val(start.format('DD-MM-YYYY') + ' / ' + end.format('DD-MM-YYYY'));
        }
    });
});
</script>
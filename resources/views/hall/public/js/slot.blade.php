<script>
    // set the dropzone container id
    var id = '#kt_dropzone_4';

    // set the preview element template
    var previewNode = $(id + " .dropzone-item");
    previewNode.id = "";
    var previewTemplate = previewNode.parent('.dropzone-items').html();
    previewNode.remove();

    var myDropzone4 = new Dropzone(id, { // Make the whole body a dropzone
        url: "{{ url('file', $data['book']) }}", // Set the url for your upload script location
        //parallelUploads: 20,
        acceptedFiles:"application/pdf, image/*",
        previewTemplate: previewTemplate,
        maxFileSize: 1024,
        //autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: id + " .dropzone-items", // Define the container to display the previews
        clickable: id + " .dropzone-select", // Define the element that should be used as click trigger to select files.
        success: function(file, response) {
            console.log('file: ', file);
            console.log('response: ', response);
            // Parse the JSON response from the server
            var responseData = typeof response === 'string' ? JSON.parse(response) : response;

            // Extract filename, size, and type from the response data
            var filename = responseData.filename;
            var size = responseData.size;
            var extension = responseData.extension;

            $('#filename').val(filename);
            $('#filesize').val(size);
            $('#filetype').val(extension);

        },
        error: function(file, errorMessage) {
            console.log(errorMessage);
        }
    });

    myDropzone4.on("addedfile", function(file) {
        // Hookup the start button
        file.previewElement.querySelector(id + " .dropzone-start").onclick = function() { myDropzone4.enqueueFile(file); };
        $(document).find( id + ' .dropzone-item').css('display', '');
        $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'inline-block');
    });

    // Update the total progress bar
    myDropzone4.on("totaluploadprogress", function(progress) {
        $(this).find( id + " .progress-bar").css('width', progress + "%");
    });

    myDropzone4.on("sending", function(file) {
        // Show the total progress bar when upload starts
        $( id + " .progress-bar").css('opacity', '1');
        // And disable the start button
        file.previewElement.querySelector(id + " .dropzone-start").setAttribute("disabled", "disabled");
    });

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone4.on("complete", function(progress) {
        var thisProgressBar = id + " .dz-complete";
        setTimeout(function(){
            $( thisProgressBar + " .progress-bar, " + thisProgressBar + " .progress, " + thisProgressBar + " .dropzone-start").css('opacity', '0');
        }, 300)

    });

    // Setup the buttons for all transfers
    document.querySelector( id + " .dropzone-upload").onclick = function() {
        myDropzone4.enqueueFiles(myDropzone4.getFilesWithStatus(Dropzone.ADDED));
    };

    // Setup the button for remove all files
    document.querySelector(id + " .dropzone-remove-all").onclick = function() {
        $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
        myDropzone4.removeAllFiles(true);
    };

    // On all files completed upload
    myDropzone4.on("queuecomplete", function(progress){
        $( id + " .dropzone-upload").css('display', 'none');
    });

    // On all files removed
    myDropzone4.on("removedfile", function(file){
        var name = file.name; 
        $.ajax({
            type: 'POST',
            //url: "{{ url('sport/slotbook', [Crypt::encrypt($data['id']), Crypt::encrypt($data['date'])]) }}",
            url: "{{ url('file', $data['book']) }}",
            data: {name: name},
            sucess: function(data){
                console.log('success: ' + data);
            }
        });
        if(myDropzone4.files.length < 1){
            $( id + " .dropzone-upload, " + id + " .dropzone-remove-all").css('display', 'none');
        }
    });
</script>

<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script>
    // var table = $('table').DataTable({
    //     "bPaginate": false,
    //     "bLengthChange": false,
    //     "bFilter": true,
    //     "bInfo": false,
    //     language: {
    //         "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
    //         "zeroRecords": "Harap maaf, tiada rekod ditemui",
    //         "info": "Halaman _PAGE_ dari _PAGES_",
    //         "infoEmpty": "Tiada rekod dalam sistem",
    //         "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
    //         "sSearch": "Carian:"
    //     },
    //     rowGroup: {
    //         dataSrc: 1,
    //     },
    // } );

    $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            el.checked = false;
         }
      }
   });
</script>
<script>
    $(document).ready(function(){
        $('#JenisAcaraTempDewan').on('change', function() {
            var selectedText = $(this).find('option:selected').text();
            var selectedValue = $(this).val();
            if (selectedText.trim() === 'Majlis Perkahwinan') {
                $('#pakejPerkahwinan').prop('hidden', false);
                $('#pakejPerkahwinanSelect').attr('required', true);
            } else {
                $('#pakejPerkahwinan').prop('hidden', true);
                $('#pakejPerkahwinanSelect').prop('required', false).removeAttr('required');
            }
        });
        $('#jenisTempahan').on('change', function() {
            var jenisTempahan = $(this).val();
            console.log(jenisTempahan)
            if(jenisTempahan != 1 && jenisTempahan != 5) {
                $('#docSokongan').removeAttr('hidden');
            } else {
                $('#docSokongan').attr('hidden', true);
            }
        });
        function isInArray(array, value) {
            return (array.find(item => {return item == value}) || []).length > 0;
        }
        $('[name="masaTamat"]').on('change', function() {
            var timePicked = $(this).val();
                    
            var selectedTime = new Date('2024-01-01 ' + timePicked); 
            var sevenPM = new Date('2024-01-01 19:00:00');
            
            if (selectedTime > sevenPM) {
                var isBooked = false;
                var blockedDates = <?php echo json_encode($data['confirmed_booking']); ?>;
                var endJson = <?php echo json_encode($data['endJson']); ?>;
                isBooked = isInArray(blockedDates, endJson);

                if (isBooked) {
                    Swal.fire(
                        'Harap Maaf!',
                        'Masa yang dipilih tidak dibenarkan bagi memberi ruang acara hari ke hadapan. Sila pilih tarikh atau waktu yang berlainan',
                        'error'
                        );
                    $('[name="masaTamat"]').val('07:00 PM');
                }
                console.log("The selected time is greater than 7 PM", isBooked);
            }
        });
        // $("#form").submit(function( event ) {
        //     var $form = $(this);
        //     var atLeastOneIsChecked = $('input[name="slot[]"]:checked').length > 0;
        //     if(atLeastOneIsChecked == false){
        //         event.preventDefault();
        //         Swal.fire(
        //         'Harap Maaf!',
        //         'Sila pilih sekurang-kurangnya 1 slot untuk teruskan tempahan',
        //         'error'
        //         );
        //     }
        // });
    });
    
    var maxAllowed = 2;
    var current = 0;
    
$('.chk').on('change', function () {
    var checkedCount = $('.chk:checked').length;

    if (checkedCount > maxAllowed) {
        $(this).prop('checked', false);
        Swal.fire(
            'Harap Maaf!',
            'Anda telah memilih terlalu banyak slot tempahan. Sila pilih maksimum ' + maxAllowed + ' slot.',
            'error'
        );
    }
});
</script>
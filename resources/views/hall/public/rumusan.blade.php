@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Rumusan Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                {{-- <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr> --}}
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Jenis Tempahan</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                    <td>RM 1000</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                            <a href="{{ url('hall/borangbpsk', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Borang BPSK</a>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--start::Maklumat Acara-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        {{-- <a href="{{ url('/hall/tempahan', Crypt::encrypt($data['booking'])) }}" class="btn btn-light-warning font-weight-bolder mr-2 mt-2">
                            <i class="flaticon-edit"></i> Kemaskini Tempahan
                        </a> --}}
                        <a href="{{ url('/hall') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Item</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Akhir</th>
                                    <th>Tujuan Tempahan</th>
                                    <th>Maklumat Acara</th>
                                </tr>
                            </thead>
                            @php
                                $i = 0;
                                $j = 1;
                            @endphp
                            <tbody>
                                @if(count($data['booking_detail']) > 0)
                                    @foreach($data['booking_detail'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($data['bh_booking'][$i++]->fk_bh_hall) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_end_date) }}</td>
                                        <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                        <td>{{ $p->bbd_event_name }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Tiada Maklumat Acara</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kos Dewan</span>
                    </h3>
                    
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        @php
                            $jumlahdeposit = 0;
                            
                        @endphp
						
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Dewan</th>
                                    <th>Lokasi</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Slot Masa</th>
                                    <th class="text-right">Harga Sewaan (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th>
                                    {{-- <th>Kod GST</th>
                                    <th>GST (0%)</th> --}}
                                    <th class="text-right">Jumlah Sewaan (RM)</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahdewan = 0;
                                $jumlahkeseluruhan = 0;
                            @endphp
                            <tbody>
                                @if(count($data['bh_booking']) > 0)
                                    @foreach($data['bh_booking'] as $p)
                                    @php
                                        $hargaDewan = Helper::get_harga_dewan($p->fk_bh_hall);
                                    @endphp
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($p->fk_bh_hall) }}</td>
                                        <td>PPj</td>
                                        <td>{{ Helper::date_format($p->bb_start_date) }} sehingga {{ Helper::date_format($p->bb_end_date)}}</td>
                                        <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }} - {{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                        {{-- <td class="text-right">{{ Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)) }}</td> --}}
                                        {{-- <td></td>
                                        <td></td> --}}
                                        <td class="text-right">{{ Helper::moneyhelper($hargaDewan * $data['lkp_discount_rate']) }}</td>
                                        <td class="text-right">{{ Helper::moneyhelper($hargaDewan * $data['lkp_discount_rate'] *$p->bb_no_of_day) }}</td>
                                    </tr>
                                    @php
                                        $jumlahdewan += $hargaDewan * $data['lkp_discount_rate'];
                                        $discountRate = Helper::getDepositRate($data['main']->fk_lkp_location);
                                        $jumlahdeposit += 1000.00;
                                        // $jumlahdeposit += $hargaDewan * ($discountRate/100);
                                    @endphp
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="6">Jumlah(RM)</td>
                                        {{-- <td>{{ Helper::moneyhelper($jumlahdewan * $data['lkp_discount_rate']) }}</td> --}}
                                        <td>{{ Helper::moneyhelper($jumlahdewan) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="11">Tiada Maklumat Dewan</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            {{-- Maklumat Kelengkapan Dewan --}}
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Maklumat Kelengkapan Dewan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Dewan</th>
                                    <th>Item</th>
									<th>Kuantiti</th>
									<th>Masa Mula</th>
									<th>Masa Akhir</th>
                                    {{-- <th>Jumlah (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th> --}}
                                    <th>Tarikh Penggunaan</th>

                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $jumlahperalatan = 0;
                                    $jumlahunit = 0;
                                @endphp
                                @foreach ($data['kelengkapan'] as $s)
                                    @if ($s->bbe_quantity != null)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td style="white-space: nowrap">{{ Helper::getHall($data['bh_booking'][0]->fk_bh_hall) }}</td>
                                            <td>{{ Helper::get_equipment_hall($s->fk_bh_equipment) }}</td>
                                            <td>{{ $s->bbe_quantity }}</td>
                                            <td>08:00 am</td>
                                            <td>04:00 pm</td>
                                            {{-- <td style="text-align: end">{{ Helper::moneyhelper($s->bbe_price * $s->bbe_quantity) }}</td> --}}
                                            <td>{{ Helper::date_format($s->bbe_booking_date) }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--start::Kos Kelengkapan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kos Kelengkapan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Item</th>
									<th>Kuantiti</th>                        
									<th class="text-right">Kos Seunit (RM)</th>
                                    <th class="text-right">Jumlah (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th>
                                    <th class="text-right">Jumlah Sewaan (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $jumlahperalatan = 0;
                                    $jumlahunit = 0;
                                @endphp
                                @foreach ($data['kelengkapan'] as $s)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ Helper::get_equipment_hall($s->fk_bh_equipment) }}</td>
                                        <td>{{ $s->bbe_quantity }}</td>         
                                        <td style="text-align: end">{{ $s->bbe_price }}</td>
                                        <td style="text-align: end">@if($s->bbe_total != 0.00) {{ Helper::moneyhelper(($s->bbe_total) ) }} @else - @endif</td>
                                        <td style="text-align: end">@if($s->bbe_total != 0.00) {{ Helper::moneyhelper(($s->bbe_total) ) }} @else - @endif</td>
                                    </tr>
                                    @php
                                        $jumlahperalatan += $s->bbe_total;
                                    @endphp
                                @endforeach
                                <tr>
                                    <td colspan="5" class="font-weight-bolder" style="text-align: end">Jumlah (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">{{ Helper::moneyhelper(($jumlahperalatan)) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
                    <!--start::Jumlah Keseluruhan-->
                    <div class="card card-custom gutter-b">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <!--begin: Datatable-->
                            @php
                                $jumlahdewan = $jumlahdewan * $data['lkp_discount_rate'];
                                $jumlahkeseluruhan = $jumlahdewan + $jumlahperalatan;
                                $jumlahpendahuluan = $jumlahdewan / 2;
                            @endphp
                            <div class="table-responsive">
                                <table class="table table-bordered" id="kt_datatable_2">
                                    <tbody>						
                                        <tr>
                                            <td>Jumlah Dewan (RM)</td>
                                            <td style="text-align: end">{{ Helper::moneyhelper($jumlahdewan) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Peralatan (RM)</td>
                                            <td style="text-align: end">{{ Helper::moneyhelper($jumlahperalatan) }}</td>
                                        </tr>				
                                        <tr>
                                            <td>Jumlah Pendahuluan Perlu Dibayar (RM) <span class="text-danger mt-2 font-weight-bold font-size-xs"> *(50% Jumlah Dewan)</span></td>
                                            <td style="text-align: end">{{ Helper::moneyhelper(($jumlahpendahuluan)) }}</td>                                
                                        </tr>				
                                        <tr>
                                            <td>Jumlah Deposit(RM)</td>
                                            <td style="text-align: end">{{ Helper::moneyhelper($jumlahdeposit) }}</th>                                
                                        </tr>					
                                        <tr>
                                            <td colspan = "2">&nbsp;</td>                  
                                        </tr>				
                                        <tr>
                                            <td><b>Jumlah Deposit dan Pendahuluan Perlu Dibayar (RM)</b></td>
                                            <td style="text-align: end"><b>{{ Helper::moneyhelper($jumlahpendahuluan + 1000) }}</b></th>                                
                                        </tr>		
                                        <tr>
                                            <td><b>Jumlah Keseluruhan Perlu Dibayar (RM)</b></td>
                                            <td style="text-align: end"><b>{{ Helper::moneyhelper($jumlahdewan + $jumlahperalatan + $jumlahdeposit) }}</b></td>                                
                                        </tr>	
                                        <tr>
                                            <td colspan = "2">&nbsp;</td>                  
                                        </tr>
                                        {{-- <tr>
                                            <td>Jumlah Bayaran Awalan (RM)</td>
                                            <td style="text-align: end">200.00</th>                                
                                        </tr>	
                                        <tr>
                                            <td><b>Jumlah Perlu Dibayar Hari Ini(RM)</b></td>
                                            <td style="text-align: end"><b>6822.00</b></th>                                
                                        </tr> --}}
                                    </tbody>
                                </table>
                            </div>
                            <!--end: Datatable-->
                            <!-- Button trigger modal-->
                            <button type="button" class="btn btn-primary float-right font-weight-bolder mt-2" data-toggle="modal" data-target="#staticBackdrop">
                                <i class="fas fa-check-circle"></i> Teruskan Tempahan
                            </button>
                            <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>

                        </div>
                    </div>

        </div>
        <!-- Modal-->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Perakuan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <b>PERBADANAN PUTRAJAYA<br><br>

                        PERATURAN DAN SYARAT-SYARAT PENGGUNAAN DEWAN
                        PERSIDANGAN DAN AUDITORIUM CEMPAKA SARI,<br>
                        KOMPLEKS PERBADANAN PUTRAJAYA, PRESINT 3,<br>
                        WILAYAH PERSEKUTUAN PUTRAJAYA</b>
                        <hr>

                        1.0 	PERMOHONAN<br>	  	 	 
                            1.1	 Permohonan untuk menyewa Dewan Persidangan dan Auditorium Cempaka Sari, Kompleks Perbadanan Putrajaya Presint 3, Putrajaya hendaklah dibuat melalui Borang Tempahan Sewaan yang
                                boleh didapati di Kaunter Khidmat Pelanggan atau pejabat Unit Sewaan Premis dan Kompleks, Bahagian Pentadbiran, Jabatan Perkhidmatan Korporat , Aras 8, Blok A dan dikemukakan secara
                                serahan tangan atau emel.<br><br>
                            1.2 	Semua permohonan penyewaan hendaklah dibuat kepada :-<br>
                                Naib Presiden (Perkhidmatan Korporat)<br>
                                Perbadanan Putrajaya<br>
                                Kompleks Perbadanan Putrajaya.<br>
                                (u.p : Unit Sewaan Premis & Kompleks)<br>
                                No. Tel : 03 - 8000 8000 (1MOCC) / 03 - 8887 7000 (PPj)<br>
                                No. Faks : 03 - 8889 5001/5009<br><br> 	 	 
                            1.3 	Semua urusan perohonan hendaklah dibuat dalam waktu pejabat sahaja.<br>
                                Isnin - Khamis :             8.30 Pagi - 12.00 Tengahari<br>
                                                                        2.00 Petang - 4.00 Petang<br>
                                Jumaat              :            8.30 Pagi - 12.00 Tengahari<br>
                                                                        2.45 Petang - 4.00 Petang<br>
                                
                                Pengesahan penempahan hendaklah dibuat dalam tempoh 1 bulan sebelum tarikh penggunaan. Walau bagaimanapun tempahan yang dibuat dalam tempoh kurang dari 1 bulan, masih boleh dipertimbangkan oleh Perbadanan Putrajaya (PPj) tertakluk kepada kekosongan.<br><br>
                                
                            1.4 	Sebarang pembatalan atau perubahan tarikh hendaklah dibuat 1 bulan sebelum tarikh sewa dan ini tertakluk kepada kekosongan.<br><br>
                                
                            1.5	PPj berhak untuk membenarkan atau menolak sesuatu permohonan sewaan tanpa memberikan sebarang sebab atau pun menarik balik tempahan serta tawaran penggunaan ruang/tempat di atas
                                kepentingan dan keperluan PPj tanpa notis atau bayar ganti rugi.<br><br>
                                
                        2.0 	PEMBAYARAN	 <br><br>
                                
                            2.1 	Deposit sewaan adalah sebanyak sepuluh peratus (10%) daripada bayaran sewaan semasa tempahan, tertakluk kepada amaun minimum sebanyak rm200.00. Deposit ini akan dikembalikan jika
                                tiada tanggungan dialami. Penyewa dikehendaki membayar sekiranya terdapat tanggungan melebihi nilai deposit.<br><br>
                                
                            2.2 	Penyewa hendaklah membayar kepada PPj bayaran penuh dalam tempoh empat belas (14) hari sebelum tarikh penggunaan. Sekiranya pembayaran tidak dibuat dalam tempoh tersebut, pihak PPj
                                berhak untuk membatalkan tempahan tanpa sebarang notis kepada pemohon.<br><br>
                                
                            2.3 	Pembayaran hendaklah dibuat di Kaunter Bayaran (Pusat Khidmat Pelanggan) Aras 1, Bangunan Annexe, Kompleks Perbadanan Putrajaya Presint 3, Putrajaya atau secara Atas Talian
                                mengikut mana berkenaan dengan mengguna mod bayaran seperti berikut :<br>
                                
                                (a) Tunai<br>
                                (b) Deraf Bank<br>
                                (c) Kad Kredit<br>
                                (d) Perbankan Internet
                                <br><br>
                            2.4 	Penyewa dibenarkan membuat tuntutan wang deposit setelah selesai penggunaan dengan mengemukakan resit rasmi bayaran deposit.
                                <br><br>
                        3.0 	PENGGUNAAN	 <br><br>
                                
                            3.1 	Waktu penggunaan ruang/tempat adalah seperti berikut :-<br>
                                
                                Isnin - Jumaat :             8.00 Pagi - 4.00 Petang<br>
                                                                        4.00 Petang - 12.00 Malam<br>
                                
                                Sabtu                :            8.00 Pagi - 4.00 Petang<br>
                                                                    4.00 Petang - 12.00 Malam<br>
                                
                                Ahad/                :            8.00 Pagi - 4.00 Petang<br>
                                Cuti Umum      :            4.00 Petang - 12.00 Malam<br><br>
                                
                        4.0 	KADAR SEWA<br><br>
                                
                            4.1 	Kadar sewa adalah seperti yang dinyatakan di dalam lampiran Borang BPSK-01/2015 (Borang Permohonan Tempahan Perkhidmatan/Program/Aktiviti/Majlis/Sewaan dan Lain-lain)<br><br>
                                
                        5.0 	SYARAT-SYARAT PENGGUNAAN<br><br>
                                    
                            5.1	Tanggungjawab Perbadanan Putrajaya<br>
                            5.1.1	 	PPj akan menyediakan keperluan teknikal iaitu sistem lampu, sistem bunyi dan hawa dingin dan peralatan lain yang sedia ada di ruang/tempat berkenaan.<br>
                            5.1.2	 	PPj akan menempatkan sekurang-kurangnya dua (2) orang Penolong Jurutera/Juruteknik untuk bertanggungjawab ke atas penyelenggaraan dan penyeliaan peralatan teknikal milik PPj sahaja
                                    dan penyeliaan sepanjang tempoh penggunaan.<br>
                            5.1.3	 	PPj boleh dan berhak untuk mengambil gambar atau membuat rakaman audio visual yang bertujuan untuk simpanan/dokumentasi pihak PPj yang bukan untuk tujuan dagangan.<br>
                                    
                            5.1.4	 	PPj tidak bertanggungjawab ke atas keselamatan peralatan, props dan lain-lain barang ataupun peralatan teknikal milik Penyewa yang disimpan di dalam kawasan PPj sekiranya berlaku
                                    kehilangan dan kerosakan kepada peralatan tersebut.<br>
                                    
                            5.1.5	 	PPj tidak bertanggungjawab terhadap sebarang kecederaan, kemalangan, kematian penyewa, pekerja-pekerja, agen-agen, atau tetamunya atau sesiapa jua sebelum, smeasa dan selepas
                                    penggunaan ruang/tempat.<br>
                                    
                            5.1.6	 	PPj tidak menyediakan tenaga pekerja bagi membantu persiapan pihak penyewa.<br>
                                    
                            5.1.7	 	PPj berhak mengambil tindakan atau membatalkan tempahan jika didapati penyewa gagal mematuhi garis panduan atau melanggar peraturan yang disediakan dan tidak akan membayar
                                    sebarang ganti rugi dengan sebab pembatalan sesuatu tempahan.<br>
                                    
                            5.1.8	 	Tempat letak kereta di kompleks Perbadanan Putrajaya adalah terhad. Walau bagaimanapun, para jemputan dibenar menggunakan tempat-tempat letak kereta ini (kecuali bagi tempat letak
                                    kereta yang telah dikhaskan untuk kenderaan PPj serta pegawai dan kakitangannya). Segala kerosakan, kehilangan atau pun sesuatu perkara berlaku ke atas kenderaan adalah di bawah
                                    tanggungjawab sendiri.<br>
                                    
                            5.1.9	 	PPj atau pegawainya yang diberikan kuasa hendaklah dibenarkan keluar atau masuk ruang/tempat pada bila-bila masa untuk menjalankan pemeriksaan atau tugas-tugas sekali pun
                                    semasa ianya sedang dijalankan.<br>
                                    
                            5.2	Tanggungjawab Penyewa<br>
                            5.2.1	 	Penyewa yang menggunakan segala peralatan dan kemudahan milik PPj adalah bertanggungjawab bagi menentukan ianya diselenggarakan dengan sebaik-baiknya . Sekiranya berlaku
                                    kerosakan, kemusnahan atau kehilangan kepada perlatan tersebut akibat kecuaian Penyewa, PPj berhak menuntut gantirugi dengan nilai harga kerosakan atau nilai harga peralatan
                                    tersebut. PPj berhak menggunakan wang deposit bagi memperbaiki kerosakan atau mengganti kemusnahan berkenaan dan sekiranya wang deposit tersebut tidak dapat menampung kos
                                    pembaikan atau penggantian, PPj boleh menuntut bakinya daripada Penyewa.<br>
                                    
                            5.2.2	 	Penyewa tidak dibenarkan menambah atau mengurangkan sebarang peralatan teknikal sedia ada di ruang/tempat yang disewa. Sebarang penambahan perlulah mendapat kelulusan
                                    bertulis dari PPj.<br>
                                    
                            5.2.3	 	Penyewa boleh dengan persetujuan PPj untuk membawa perabot dan perkakas-perkakas lain milik Penyewa selain dari yang disediakan oeh PPj.<br>
                                    
                            5.2.4	 	Semua peralatan, harta milik Penyewa atau barangan lain seumpamanya yang hendak dibawa masuk lebih awal iaitu sebelum tempoh masa penyewaan perlulah mendapat persetujuan PPj
                                    telebih dahulu. PPj tidak akan bertanggungjawab ke atas sebarang kehilangan atau kerosakan peralatan, harta benda atau barangan tersebut.<br>
                                    
                            5.2.5	 	Penyewa adalah bertanggungjawab untuk menjaga kebersihan di semua bahagian ruang/tempat yang digunakan.<br>
                                    
                            5.2.6	 	Pemasangan backdrop pada dinding atas pentas hendaklah dilakukan tanpa meggunakan paku. Backdrop hendaklah ditanggalkan selepas sahaja setiap persembahan selesai
                                    dan membersihkan segala kesan yang tertinggal serta memastikan tiada sebarang kecacatan yang berlaku. Sekiranya berlaku kekotoran atau kerosakan, penyewa adalah bertanggungjawab
                                    untuk membaiki kerosakan tersebut.<br>
                                    
                            5.2.7	 	Penyewa tidak dibenarkan menampal/melekat bahan-bahan hebahan seperti poster, brosur atau gambar dimana-mana bahagian dinding, cermin bangunan melainkan setelah mendapatkan
                                    kelulusan PPj terlebih dahulu.<br>
                                    
                            5.2.8	 	Aktiviti yang berbentuk persembahan, Penyewa hendaklah menyediakan Penolong Jurutera/Juruteknik untuk bertindak sebagai penyelia peralatan dan ruang/tempat.<br>
                                    
                            5.2.9	 	Segala penggunaan/penyewaan ruang/tempat dan kemudahan PPj dihadkan penggunaannya sehingga masa yang telah ditetapkan seperti yang dinyatakan perenggan 3.1 di atas.<br>
                                    
                            5.2.10	 	Penyewa dikehendaki mengeluarkan segala peralatan persendirian, termasuk props, set dan semua barang-barang kepunyaan Penyewa daripada ruang/tempat selepas tamat
                                    masa penggunaan.<br>
                                    
                            5.2.11	 	Penyewa tidak boleh menggunakan mana-mana bahagian ruang/tempat yang tidak ditempah tanpa kebenaran PPj.<br>
                                    
                            5.2.12	 	'Backdrop' boleh diuruskan oleh PPj dengan bayaran berasingan berdasarkan kepada harga, konsep, rekacipta, pemasangan dan tanggal semula. Satu senarai pembekal boleh didapati
                                    daripada pihak pengurusan.<br>
                                    
                        6.0 	SYARAT-SYARAT AM<br><br>
                                
                            6.1 	Setiap persembahan/pertunjukan/majlis yang memerlukan surat kelulusan atau permit dari pihak polis atau mana-mana pihak berkuasa, perlulah dikemukakan salinan permit tersebut
                                kepada PPj dan Penyewa dikehendaki mempamerkannya di pintu masuk ruang/ tempat yang disewa.<br>
                            6.2	Semua kawasan dalam bangunan Kompleks Perbadanan Putrajaya Presint 3, Wilayah Persekutuan Putrajaya adalah kawasan larangan merokok.<br>
                                
                            6.3	Makan dan minum dalam apa jua bentuk sekalipun adalah tidak dibenarkan di dalam ruang/tempat yang disewa melainkan di tempat yang dikhaskan sahaja.<br>
                                
                            6.4	Minuman keras adalah dilarang sama sekali.<br>
                                
                            6.5	Sebarang aktiviti jualan adalah tidak dibenarkan semasa persembahan/pertunjukan/majlis sedang diadakan.<br>
                                
                            6.6	'Rehearsal' ringkas selama 2 jam boleh dibuat dalam waktu pejabat sahaja tanpa hawa dingin.<br>
                                
                            6.7	Persembahan/pertunjukan/majlis tidak bercanggah dengan hukum Islam atau dasar PPj.<br>
                                
                            6.8	Persembahan/pertunjukan/majlis yang melibatkan permainan muzik hendaklah dihentikan apabila masuk waktu solat ketika azan dilaungkan di masjid.<br>

                    </div>
                    <div class="modal-footer">
                        <form action="{{ url('hall/rumusan', [Crypt::encrypt($data['booking']), Crypt::encrypt($jumlahdewan + $jumlahperalatan), Crypt::encrypt($jumlahdeposit)]) }}" method="post">
                            @csrf
                            <input hidden type="text" name="jumlahkelengkapan" value="{{ $jumlahperalatan }}">
                            <input hidden type="text" name="jumlahdewan" value="{{ $jumlahdewan }}">
                            <label class="checkbox checkbox-success checkbox-sm d-flex align-items-start">
                                <input type="checkbox" id="pengakuan" class="group-checkable"/>
                                <span></span>
                                <div class="pl-5">Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun dan syarat yang dikenakan dan akan dijelaskan tidak lewat 14 hari sebelum perkhidmatan/ program/ aktiviti/ majlis/ acara/ sewaan yang ditempah berlangsung.</div>
                            </label>
                            <button disabled id="submitBtn" type="submit" class="btn btn-primary float-right"><i class="fas fa-check-circle"></i> Hantar Tempahan</button>
                            {{-- <button type="submit" class="btn btn-primary"><i class="
                                fas fa-check-circle"></i> Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun dan syarat yang dikenakan dan akan dijelaskan tidak lewat 14 hari sebelum perkhidmatan/ program/ aktiviti/ majlis/ acara/ sewaan yang ditempah berlangsung.
                            </button> --}}
                        </form>
                        {{-- <a href="{{ url('hall/maklumatbayaran', Crypt::encrypt($data['booking'])) }}" class="btn btn-primary"><i class="
                            fas fa-check-circle"></i> Dengan ini saya mengesahkan maklumat yang diberikan adalah benar dan bersetuju dengan amaun dan syarat yang dikenakan dan akan dijelaskan tidak lewat 14 hari sebelum perkhidmatan/ program/ aktiviti/ majlis/ acara/ sewaan yang ditempah berlangsung.
                        </a> --}}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

@section('js_content')
    @include('hall.public.js.rumusan')
@endsection





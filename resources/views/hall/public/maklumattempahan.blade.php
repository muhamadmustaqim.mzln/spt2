@extends('layouts.master')
@section('container')

<style>
    .rate {
        float:left;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:40px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #ffc700;    
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;  
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }
    /** end rating **/
    lottie-player {
        margin: 0 auto;
    }
</style>
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Maklumat Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            @if ($data['main']->fk_lkp_status == 19 || $data['main']->fk_lkp_status == 2 || $data['main']->fk_lkp_status == 3 || $data['main']->fk_lkp_status == 4)
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        @if ($data['main']->fk_lkp_status == 19)
                            <div class="card-title h3">Tempahan ini telah diproses dan dihantar kepada pihak Perbadanan Putrajaya untuk pengesahan.</div>
                            <ul>						
                                <li>
                                    Pengesahan akan mengambil masa 3-5 hari bekerja. <br> 
                                </li>	
                                <li>
                                    Notifikasi emel akan akan dihantar setelah tempahan disahkan. <br> 
                                </li>
                                <li>
                                    Pembayaran Deposit dan Pendahuluan mesti dilakukan dalam tempoh 3 hari selepas pengesahan. <br> 
                                </li>
                            </ul>
                        @elseif ($data['main']->fk_lkp_status == 2)
                            <div class="card-title h3">Tempahan ini telah disahkan dan diterima. Sila buat pembayaran deposit dan pendahuluan dalam tempoh 3 hari.</div>
                            <ul>
                                <li>
                                    Pembayaran yang tidak dijelaskan sebelum tempoh 3 hari selepas pengesahan akan dibatalkan secara automatik oleh sistem. <br> 
                                </li>
                            </ul>
                        @elseif ($data['main']->fk_lkp_status == 4)
                            <div class="card-title h3">Pembayaran deposit dan pendahuluan telah diterima. Sila buat pembayaran penuh 14 hari sebelum hari acara.</div>
                        @endif
                    </div>
                </div>
            @endif
            <!--begin::Maklumat Pemohon-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    {{-- <td>{{ $data['main']->bmb_subtotal }}</td> --}}
                                    <td>RM {{ number_format($data['main']->bmb_subtotal, 2) }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                    {{-- <td>{{ Helper::getDepositRate($data['main']->fk_lkp_deposit_rate) }}</td> --}}
                                    <td>RM 1000</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    @if ($data['main']->fk_lkp_status == 19)
                        <button disabled href="{{ url('/hall/janasebutharga', [Crypt::encrypt($data['booking'])]) }}" class="btn btn-success float-right font-weight-bolder mt-2 mr-2">Jana Sebut Harga</button>
                    @elseif ($data['main']->fk_lkp_status != 1 && $data['main']->fk_lkp_status != 5)
                        <a href="{{ url('/hall/janasebutharga', [Crypt::encrypt($data['booking'])]) }}" class="btn btn-success float-right font-weight-bolder mt-2 mr-2">Jana Sebut Harga</a>
                    @endif
                </div>
                <!--end::Body-->
            </div>
            <!--end::Maklumat Pemohon-->
            <!--start::Senarai Resit-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Jenis Bayaran</th>
                                    <th>No. Resit</th>
                                    @if(count($data['online']) > 0)
                                    <th>No. SAP</th>
                                    @endif
                                    <th>Tarikh Transaksi</th>
                                    <th>Jumlah Bayaran (RM)</th>
									{{-- <th>Jumlah Baki Pembayaran (RM)</th> --}}
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahtelahdibayar = 0;
                                $selesai = false;
                            @endphp
                            <tbody>
                                @if(count($data['kaunter']) > 0)
                                    @foreach($data['kaunter'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>{{ $p->bp_receipt_number }}</td>
                                        <td>{{ $p->bp_receipt_date }}</td>
                                        <td>{{ $p->bp_paid_amount }}</td>
                                        <td style="width: 15%;">
                                            <a href="#" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @elseif(count($data['online']) > 0)
                                    @foreach($data['online'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>{{ $p->fpx_trans_id }}</td>
                                        <td>{{ $data['main']->sap_no ?? "" }}</td>
                                        <td>{{ $p->fpx_date }}</td>
                                        <td>{{ $p->amount_paid }}</td>
										{{-- <td></td> --}}
                                        <td style="width: 15%;"> 
                                            <a href="{{ url('hall/resitonline', [Crypt::encrypt($data['booking']),Crypt::encrypt($data['kelengkapan'])]) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
											{{-- <a href="https://spt2.ppj.gov.my/hall/bayaran/eyJpdiI6InN0dEFyNXI2T3p6SnJKK01jdWgrRmc9PSIsInZhbHVlIjoiRkFRMmJHSUZQc1BMNGNkejFWQUMydz09IiwibWFjIjoiYzJlMjgzOTJhOWNjMzg5MzdiZTE1YzQwMjAyM2NmODAwNDllNjNhMTg0NmFjZjdkNDY2NzgyZGIxMGUxMmJkZiIsInRhZyI6IiJ9" class="btn btn-sm btn-primary">Bayar Baki</a> --}}
                                        </td>
                                    </tr>
                                    @if ($p->fk_lkp_payment_type != 1)
                                        @php
                                            $jumlahtelahdibayar += $p->amount_paid;
                                        @endphp
                                    @endif
                                    @if ($p->fk_lkp_payment_type == 3)
                                        @php
                                            $selesai = true;
                                        @endphp
                                    @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Tiada Resit Bayaran</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Senarai Resit-->
            <!--start::Maklumat Acara-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Item</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Akhir</th>
                                    <th>Tujuan Tempahan</th>
                                    <th>Maklumat Acara</th>
                                </tr>
                            </thead>
                            @php
                                $i = 0;
                                $j = 1;
                            @endphp
                            <tbody>
                                @if(count($data['booking_detail']) > 0)
                                    @foreach($data['booking_detail'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($data['bh_booking'][$i++]->fk_bh_hall) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_end_date) }}</td>
                                        <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                        <td>{{ $p->bbd_event_name }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Tiada Maklumat Acara</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Maklumat Acara-->
            <!--start::Kadar Sewaan Dewan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Dewan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Dewan</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Akhir</th>
                                    <th>Masa Mula</th>
                                    <th>Masa Akhir</th>
                                    <th>Bil. Hari</th>
                                    <th class="text-right">Harga (RM)</th>
                                    {{-- <th>Kod GST</th>
                                    <th>GST (0%)</th> --}}
                                    <th class="text-right">Jumlah Sewaan (RM)</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahdewan = 0;
                                $jumlahkeseluruhan = 0;
                            @endphp
                            <tbody>
                                @if(count($data['bh_booking']) > 0)
                                    @foreach($data['bh_booking'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($p->fk_bh_hall) }}</td>
                                        <td>{{ Helper::date_format($p->bb_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->bb_end_date) }}</td>
                                        <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }}</td>
                                        <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                        <td>{{ ($p->bb_no_of_day) }}</td>
                                        <td class="text-right">{{ Helper::get_harga_dewan($p->fk_bh_hall) }}</td>
                                        {{-- <td></td>
                                        <td></td> --}}
                                        <td class="text-right">{{ $temp = Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall) *$p->bb_no_of_day)  }}</td>
                                        {{-- <td class="text-right">{{ $temp = Helper::moneyhelper($p->bb_subtotal) }}</td> --}}
                                    </tr>
                                    @php
                                        $tempFloat = (float) str_replace(',', '', $temp);
                                        $jumlahdewan += $tempFloat; 
                                    @endphp
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="8">Jumlah Keseluruhan(RM)</td>
                                        <td>{{ Helper::moneyhelper($jumlahdewan) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="11">Tiada Maklumat Dewan</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Kadar Sewaan Dewan-->
            <!--start::Kadar Sewaan Peralatan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Dewan</th>
                                    <th>Item</th>
                                    <th>Tarikh</th>
                                    <th>Masa Mula</th>
                                    <th>Masa Akhir</th>
                                    <th>Kuantiti</th>
                                    <th>Harga (RM) Seunit</th>
                                    {{-- <th>Jumlah (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th> --}}
                                    {{-- <th>Kod GST</th>
                                    <th>GST (0%)</th> --}}
                                    <th>Jumlah Sewaan (RM)</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahperalatan = 0;
                                $jumlahunit = 0;
                            @endphp
                            <tbody>
                                @if(count($data['kelengkapan']) > 0)
                                    @foreach($data['kelengkapan'] as $p)
                                    @if($p->bbe_quantity != null)
                                        @php
                                            $jumlahperalatan += ($p->bbe_total);
                                            $jumlahunit += $p->bbe_price;
                                        @endphp
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                            <td>{{ Helper::getHall($data['bh_booking'][0]->fk_bh_hall) }}</td>
                                            <td>{{ Helper::get_equipment_hall($p->fk_bh_equipment) }}</td>
                                            <td>{{ $p->bbe_booking_date }}</td>
                                            <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }}</td>
                                            <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                            <td class="text-center">{{ $p->bbe_quantity }}</td>
                                            <td class="text-right">{{ $p->bbe_price }}</td>
                                            {{-- <td class="text-right">{{ $p->bbe_total }}</td> --}}
                                            <td class="text-right">{{ Helper::moneyhelper($p->bbe_total ) }}</td>
                                        </tr>
                                        @php
                                            $jumlahkeseluruhan += $jumlahperalatan; 
                                        @endphp
                                    @endif
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="8">Jumlah(RM)</td>
                                        <td>{{ Helper::moneyhelper($jumlahperalatan ) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="10">Tiada Data</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Kadar Sewaan Peralatan-->
            <!--start::Jumlah Keseluruhan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        @php
                            $jumlah50dewan = $jumlahdewan / 2;
                            
                        @endphp
                        <table class="table table-bordered">
                            {{-- <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">E</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">I</td>
                                <td class="text-right">0.00</td>
                            </tr> --}}
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Dewan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ Helper::moneyhelper($jumlahdewan) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">50% dari Jumlah Dewan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ Helper::moneyhelper($jumlah50dewan) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Peralatan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ Helper::moneyhelper($jumlahperalatan) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Deposit (RM)</th>
                                <td></td>
                                <td class="text-right">{{ Helper::moneyhelper($data['main']->bmb_deposit_rm) }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                <td></td>
                                <td class="text-right">{{ number_format($jumlahtelahdibayar, 2) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Deposit dan Pendahuluan Perlu Dibayar (RM)</th>
                                <td></td>
                                <td style="text-align: end">{{ Helper::moneyhelper($jumlah50dewan + 1000) }}</th>                                
                            </tr>	
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Perlu Dibayar (RM)</th>
                                <td></td>
                                @if ($data['main']->fk_lkp_status == 2 || $data['main']->fk_lkp_status == 19)
                                    <td class="text-right"><b>{{ Helper::moneyhelper((($jumlahdewan + $data['main']->bmb_deposit_rm + $jumlahperalatan))) }}</b></td>
                                @elseif ($data['main']->fk_lkp_status == 4)
                                    <td class="text-right"><b>{{ Helper::moneyhelper((($jumlahperalatan + $jumlahdewan)/2)) }}</b></td>
                                @else
                                    <td class="text-right">0.00</td>
                                @endif
                            </tr> 	
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Jumlah Keseluruhan-->
            <div class="float-right">
                @if ($data['main']->fk_lkp_status == 2)
                    <a href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), Crypt::encrypt($jumlah50dewan + $data['main']->bmb_deposit_rm)]) }}" class="btn btn-primary float-right font-weight-bolder mt-2">
                        <i class="fas fa-check-circle"></i> Bayar Online
                    </a>
                @elseif ($data['main']->fk_lkp_status == 4)
                    <a href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), Crypt::encrypt((($jumlahperalatan + $jumlah50dewan)))]) }}" class="btn btn-primary float-right font-weight-bolder mt-2">
                        <i class="fas fa-check-circle"></i> Bayar Online
                    </a>
                @elseif ($data['main']->fk_lkp_status == 19)
                    <button type="button" title ="Menunggu Pengesahan Admin" disabled href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), Crypt::encrypt($jumlahdewan + $data['main']->bmb_deposit_rm + $jumlahperalatan)]) }}" class="btn btn-primary float-right font-weight-bolder mt-2">
                        <i class="fas fa-check-circle"></i> Bayar Online
                    </button>
                @else
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input hidden type="text" id="bookingId" value="{{ Crypt::encrypt($data['booking']) }}">
                    @if (strtotime($data['bh_booking'][0]->bb_end_date) >= strtotime('today'))
                        @if ($data['main']->fk_lkp_status == 5)
                            @if (count($data['bh_rating']) == 0)
                                <button type="button" class="btn btn-primary float-right font-weight-bolder mt-2" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fas fa-check-circle"></i> Mohon Pemulangan Deposit
                                </button>
                            @else
                                <button @if(count($data['bh_refund_request']) > 0) disabled @endif type="button" class="btn btn-primary float-right font-weight-bolder mt-2" data-toggle="modal" data-target="#staticBackdrop">
                                    <i class="fas fa-check-circle"></i> Mohon Pemulangan Deposit
                                </button>
                            @endif
                        @endif
                    @endif
                    {{-- <button id="mohonpemulangan" class="btn btn-primary mohonpemulangan float-right font-weight-bolder mt-2"><i class="fas fa-check-circle"></i> Mohon Pemulangan Deposit</button> --}}
                    {{-- <a href="{{ url('hall/pemulangandeposit', [Crypt::encrypt($data['booking'])]) }}" class="btn btn-primary float-right font-weight-bolder mt-2">
                        <i class="fas fa-check-circle"></i> Mohon Pemulangan Deposit
                    </a> --}}
                @endif
                {{-- <a href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), (($jumlahperalatan + $jumlahdewan)/2)+($jumlahdewan/10))]) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Bayar Online</a> --}}
                <a href="{{ url('hall/refund') }}" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Rating Tempahan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="rate">
                                    <input type="radio" id="star5" name="rate" value="5" />
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="rate" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="rate" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="rate" value="2" />
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="rate" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4 mb-1">
                            <textarea class="form-control" id="highlight" rows="5"></textarea>
                            <span class="form-text text-muted">Jika ada sebarang maklumbalas, pandangan ataupun penambahbaikan berkenaan tempahan. Sila isi pada ruangan disediakan</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-dark font-weight-bold" id="simpanRating" data-dismiss="modal">Simpan</button>
                        {{-- <a href="{{ url('/hall/refund/form', Crypt::encrypt($data['main']->id)) }}" class="btn btn-outline-dark font-weight-bold">Simpan</a> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-2" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Permohonan Pemulangan Deposit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close text-white"></i>
                        </button>
                    </div>
                    <form action="{{ url('hall/pemulangandeposit', [Crypt::encrypt($data['booking'])]) }}" method="post">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <h5 class="col font-weight-bold">Tujuan Permohonan:</h5>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                    <div class="form-check my-2 p-2 ml-5">
                                        <input class="form-check-input" type="radio" name="tujuan_permohonan" id="permohonanbaru" value="option1">
                                        <label class="form-check-label ml-4" for="permohonanbaru">
                                            Permohonan Baru
                                        </label>
                                        </div>
                                    <div class="form-check my-2 p-2 ml-5">
                                        <input class="form-check-input" type="radio" name="tujuan_permohonan" id="kemaskini" value="option2">
                                        <label class="form-check-label ml-4" for="kemaskini">
                                            Kemaskini Maklumat
                                            </label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">No. Pendaftaran Syarikat Dengan SSM</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="ssm" id="ssm" placeholder="No. Pendaftaran Syarikat Dengan SSM" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">No. Pendaftaran Cukai Barangan Dan Perkhidmatan (GST)</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="pendaftarangst" id="pendaftarangst" placeholder="No. Pendaftaran Cukai Barangan Dan Perkhidmatan (GST)" value="">
                                    <span class="form-text text-danger" for="ic" >(Jika Ada)</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">Nama Syarikat</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="namasyarikat" id="namasyarikat" placeholder="Nama Syarikat" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">Nama Pemilik</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="namapemilik" id="namapemilik" placeholder="Nama Pemilik" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">Alamat Syarikat</label>
                                <div class="col-lg-9">
                                    <textarea type="text" class="form-control my-2" name="alamatsyarikat" id="alamatsyarikat" placeholder="Alamat Syarikat" value=""></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">No. Telefon Pejabat</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="notelpejabat" id="notelpejabat" placeholder="No. Telefon Pejabat" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">No. Telefon Bimbit</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="notelbimbit" id="notelbimbit" placeholder="No. Telefon Bimbit" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">No. Telefon Faks</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="notelfaks" id="notelfaks" placeholder="No. Telefon Faks" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label font-weight-bold">Alamat E-mail</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control my-2" name="email" id="email" placeholder="Alamat E-mail" value="">
                                    <span class="form-text text-danger" for="email" >(Sila gunakan email rasmi untuk syarikat)</span>
                                </div>
                            </div>
                            <br>
                            <div class="row d-flex justify-content-center">
                                <h5 class="text-danger text-center" >*<span class="text-muted">Sila pastikan maklumat yang diisi adalah benar.</span></h5>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Tutup</button>
                            <button type="" id="submitrefund" class="btn btn-primary font-weight-bold">Mohon</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection
					
@section('js_content')
@include('hall.public.js.maklumattempahan')
@endsection




@extends('layouts.master')

@section('container')

    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Fasiliti</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Dewan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Tambah Fasiliti</h3>
                    </div>
                    <div class="card-toolbar">
                        <ul class="nav nav-dark nav-bold nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_4_1">
                                    <span class="nav-icon"><i class="flaticon-file-2"></i></span>
                                    <span class="nav-text">Maklumat Fasiliti</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_4_2">
                                    <span class="nav-icon"><i class="flaticon-file-2"></i></span>
                                    <span class="nav-text">Gambar & Lokasi Fasiliti</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_4_3">
                                    <span class="nav-icon"><i class="flaticon-file-2"></i></span>
                                    <span class="nav-text">Maklumat Fasiliti Terperinci</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_4_4">
                                    <span class="nav-icon"><i class="flaticon-file-2"></i></span>
                                    <span class="nav-text">Polisi Fasiliti</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="kt_tab_pane_4_1" role="tabpanel" aria-labelledby="kt_tab_pane_4_1">
                            <input type="hidden" name="uuidKey" value="{{ $data['uuid'] }}">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Fasiliti :</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="fasiliti" placeholder="Nama Fasiliti" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jenis Fasiliti :</label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" id="kt_select2_1" name="jenis">
                                        <option value="">Sila Pilih</option>
                                        @foreach($data['facility'] as $f)
                                            <option value="{{ $f->id }}">{{ $f->ef_desc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lokasi :</label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" id="kt_select2_2" name="lokasi">
                                        <option value="">Sila Pilih</option>
                                        @foreach($data['location'] as $l)
                                            <option value="{{ $l->id }}">{{ $l->lc_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Sorotan :</label>
                                <div class="col-lg-10">
                                    <textarea name="highlight" class="form-control" rows="7"></textarea>
                                    <span class="form-text text-muted">*Penerangan Fasiliti secara terperinci.</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Gambar Muka Hadapan :</label>
                                <div class="col-lg-10">
                                    <div class="dropzone dropzone-default" id="kt_dropzone_2">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Letakkan fail di sini atau klik untuk memuat naik.</h3>
                                            <span class="dropzone-msg-desc"><strong>Fail dalam bentuk gambar sahaja. Hanya satu(1) lampiran dibenarkan</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lawatan Maya :</label>
                                <div class="col-lg-10">
                                    <div class="dropzone dropzone-default" id="kt_dropzone_1">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Letakkan fail di sini atau klik untuk memuat naik.</h3>
                                            <span class="dropzone-msg-desc"><strong>Fail dalam bentuk gambar sahaja. Hanya satu(1) lampiran dibenarkan</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Fasiliti Berkait :</label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" id="kt_select2_4" multiple="multiple" name="jenis">
                                        <option value="">Sila Pilih</option>
                                        @foreach($data['facility_type'] as $f)
                                            <option value="{{ $f->id }}">{{ Helper::getSportFacility($f->id) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tempahan Atas Talian :</label>
                                <div class="col-lg-10">
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="checkbox" checked="checked" id="checkbox1" name="status"/>
                                            <span></span>
                                        </label>
                                        <label class="ml-2 text-muted" id="textbox1">Aktif</label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status Fasiliti :</label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" id="kt_select2_3" name="status">
                                        <option value="">Sila Pilih Status</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                        <option value="2">Tutup Sementara</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_tab_pane_4_2" role="tabpanel" aria-labelledby="kt_tab_pane_4_2">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Gambar Fasiliti :</label>
                                <div class="col-lg-10">
                                    <div class="dropzone dropzone-default" id="kt_dropzone_3">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Letakkan fail di sini atau klik untuk memuat naik.</h3>
                                            <span class="dropzone-msg-desc"><strong>Fail dalam bentuk gambar sahaja. Minimum enam(6) lampiran gambar</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lokasi Fasiliti :</label>
                                <div class="col-lg-10">
                                    <div id="map" style="height:400px;"></div>
                                    <span class="form-text text-muted">*Klik pada peta unutk mengisi latitud dan longitud ataupun isi latitud dan longitud pada tempat yang disediakan.</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Latitud :</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="latitud" id="latitude" placeholder="Latitud" />
                                </div>
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Longitud :</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="longitud" id="longitude" placeholder="Longitud" />
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_tab_pane_4_3" role="tabpanel" aria-labelledby="kt_tab_pane_4_3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <style>
                                        table
                                        {
                                            counter-reset: rowNumber;
                                        }

                                        table tr > td:first-child
                                        {
                                            counter-increment: rowNumber;
                                        }

                                        table tr td:first-child::before
                                        {
                                            content: counter(rowNumber);
                                        }
                                    </style>
                                    <div class="form-group">
                                        <label class="col-form-label text-lg-right font-weight-bold">Maklumat Fasiliti Terperinci: </label>
                                        <span class="form-text text-muted">*Sila isi sekurang-kurangnya satu(1) maklumat fasiliti terperinci.</span>
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="table_details">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th style="text-align: center">Bil</th>
                                                        <th style="width: 25%">Nama Fasiliti Terperinci</th>
                                                        <th>Kategori Bayaran Sewa</th>
                                                        <th style="width: 10%">Kod Hasil</th>
                                                        <th style="width: 13%">Kod Hasil Deposit</th>
                                                        <th style="width: 10%">Deposit (%)</th>
                                                        <th>Status</th>
                                                        <th style="text-align: center"><button type="button" class="btn btn-success btn-sm btn-icon" id="add_details"><i style="margin: auto;" class="fas fa-plus"></i></button></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center"></td>
                                                        <td><input type="text" name="fasiliti[]" class="form-control" placeholder="Nama Fisiliti Terperinci" ></td>
                                                        <td>
                                                            <select class="form-control" name="sewa[]">
                                                                <option value="">Sila Pilih Kategori Bayaran Sewa</option>
                                                                @foreach($data['rent'] as $l)
                                                                    <option value="{{ $l->id }}">{{ $l->lsc_slot_desc }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="text" name="kod_hasil[]" class="form-control" placeholder="Kod Hasil"></td>
                                                        <td><input type="text" name="kod_deposit[]" class="form-control" placeholder="Kod Hasil Deposit"></td>
                                                        <td><input type="text" name="deposit[]" class="form-control" placeholder="Deposit (%)"></td>
                                                        <td>
                                                            <select class="form-control" name="fasiliti_status[]">
                                                                <option value="">Sila Pilih Status</option>
                                                                <option value="1">Aktif</option>
                                                                <option value="0">Tidak Aktif</option>
                                                                <option value="2">Tutup Sementara</option>
                                                            </select>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_tab_pane_4_4" role="tabpanel" aria-labelledby="kt_tab_pane_4_4">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jumlah Slot Masa :</label>
                                <div class="col-lg-10">
                                    <input type="number" class="form-control" name="fasiliti" />
                                    <span class="form-text text-muted">*Jumlah slot masa untuk satu(1) tempahan</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jumlah Fasiliti Terperinci :</label>
                                <div class="col-lg-10">
                                    <input type="number" class="form-control" name="fasiliti" />
                                    <span class="form-text text-muted">*Jumlah fasiliti terperinci / Gelanggang untuk satu(1) tempahan</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Seminggu :</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <input type="number" class="form-control" aria-describedby="basic-addon2"/>
                                        <div class="input-group-append"><span class="input-group-text">Tempahan</span></div>
                                    </div>
                                    <span class="form-text text-muted">*Jumlah tempahan dalam semninggu</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Buka Tempahan :</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <input type="number" class="form-control" aria-describedby="basic-addon2"/>
                                        <div class="input-group-append"><span class="input-group-text">Hari</span></div>
                                    </div>
                                    <span class="form-text text-muted">*Tarikh yang dibenarkan untuk membuat tempahan</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right bg-gray-300 border-top-0">
                    <button type="reset" class="btn btn-primary">Simpan</button>
                    <span class="mx-2">atau</span>
                    <a href="#" class="btn btn-link btn-link-primary">Batal</a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('sport.facility.js.form')
@endsection
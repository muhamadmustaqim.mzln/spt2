@extends('layouts.master')

@section('container')

    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Dewan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Dewan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/hall/admin/facility/update', Crypt::encrypt($data['list']->id)) }}" id="form" method="post" enctype="multipart/form-data">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Fasiliti</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Nama Dewan :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="dewan" placeholder="Nama Dewan" value="{{ $data['list']->bh_name }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Dewan :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kod" placeholder="Kod Dewan" value="{{ $data['list']->bh_code }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kodfee" placeholder="Kod Dewan" value="{{ $data['list']->bh_code_fee }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran Deposit:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kodfeedepo" placeholder="Kod Dewan" value="{{ $data['list']->bh_code_deposit }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran Deposit (Online):</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="koddepoonline" placeholder="Kod Dewan" value="{{ $data['list']->feecode_depo_online }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Lokasi :</label>
                            <div class="col-lg-9">
                                <select class="form-control" id="" name="lokasi">
                                    <option value="">Sila Pilih</option>
                                    @foreach($data['location'] as $l)
                                        <option value="{{ $l->id }}" {{($data['list']->fk_lkp_location == $l->id) ? 'selected' : ''}} >{{ $l->lc_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Presint :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="presint" placeholder="Kod Dewan" value="{{ $data['list']->bh_percint }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Dewan Dibawah Pengurusan :</label>
                            <div class="col-lg-9">
                                <select class="form-control" id="" name="bawahpengurusan">
                                    <option value="">Sila Pilih</option>
                                    <option value="0" {{ $data['list']->bh_hall_owner == 0 ? 'selected' : '0' }}>PPJ</option>
                                    <option value="2" {{ $data['list']->bh_hall_owner == 2 ? 'selected' : '2' }}>Marina</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Pilihan Dewan Utama :</label>
                            <div class="col-lg-9">
                                <select class="form-control " id="" name="dewanutama">
                                    <option value="">Sila Pilih</option>
                                    @foreach($data['bh_hall'] as $l)
                                        <option value="{{ $l->id }}" {{ $data['list']->bh_parent_id == $l->id ? 'selected' : '' }} >{{ $l->bh_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Keterangan :</label>
                            <div class="col-lg-9">
                                <textarea name="keterangan" class="form-control" rows="7">{{ $data['list']->bh_description }}</textarea>
                                <span class="form-text text-muted">*Penerangan Fasiliti secara terperinci.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-9">
                                <select class="form-control select2" id="kt_select2_3" name="status">
                                    <option value="">Sila Pilih Status</option>
                                    <option value="1" {{($data['list']->bh_status == 1) ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{($data['list']->bh_status == 0) ? 'selected' : ''}}>Tidak Aktif</option>
                                    <option value="2" {{($data['list']->bh_status == 2) ? 'selected' : ''}}>Tutup Sementara</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Saiz Dewan:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="bh_size" placeholder="Saiz Dewan" value="{{ $data['list']->bh_size }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Pecahan Kapasiti/Penggunaan :</label>
                            <div class="col-lg-9">
                                <input hidden type="text" name="" id="pecahanValue" value="{{$data['details']->bhd_option}}">
                                <select class="form-control " id="pecahan" name="pecahan">
                                    <option value="" >Sila Pilih Pecahan Kapasiti</option>
                                    <option value="0" {{($data['details']->bhd_option == 0) ? 'selected' : ''}}>Tidak</option>
                                    <option value="1" {{($data['details']->bhd_option == 1) ? 'selected' : ''}}>Ya</option>
                                    <option value="2" {{($data['details']->bhd_option == 2) ? 'selected' : ''}}>Pameran</option>
                                </select>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="banquet">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti Banquet:</label>
                            <div class="col-lg-9">
                                {{-- <input type="text" class="form-control" name="kapasitibanquet" placeholder="Kapasiti Banquet" value="@if(count($data['details'])){{  $data['details'][0]->bhd_cap_banquet }} @endif"/> --}}
                                <input type="text" class="form-control" name="kapasitibanquet" placeholder="Kapasiti Banquet" value="{{ $data['details']->bhd_cap_banquet }} "/>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="seminar">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti Seminar:</label>
                            <div class="col-lg-9">
                                {{-- <input type="text" class="form-control" name="kapasitiseminar" placeholder="Kapasiti Seminar" value="@if(count($data['details'])){{ $data['details'][1]->bhd_cap_seminar }} @endif"/> --}}
                                <input type="text" class="form-control" name="kapasitiseminar" placeholder="Kapasiti Seminar" value="{{ $data['details']->bhd_cap_seminar }}"/>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="tiada">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kapasitibiasa" placeholder="Kapasiti" value="{{ $data['details']->bhd_capasity }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kemudahan Wifi:</label>
                            <div class="col-lg-9">
                                <select class="form-control select2" id="kt_select2_5" name="wifi">
                                    <option value="">Sila Pilih Status</option>
                                    <option value="1" {{($data['details']->bhd_wifi == 1) ? 'selected' : ''}}>Ya</option>
                                    <option value="0" {{($data['details']->bhd_wifi == 0) ? 'selected' : ''}}>Tidak</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Gambar Muka Hadapan :</label>
                        <div class="col-lg-10">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="file" accept="image/*" onchange="loadFile(event)"/>
                                <label class="custom-file-label" for="customFile">Pilih Fail</label>
                            </div>
                            <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                            <img id="output" src="{{ URL::asset("assets/upload/main/{$data['list']->eft_uuid}/{$data['list']->cover_img}") }}" style="width: 250px; height: 200px;"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lawatan Maya :</label>
                        <div class="col-lg-10">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="file1" accept="image/*" onchange="loadFile1(event)"/>
                                <label class="custom-file-label" for="customFile">Pilih Fail</label>
                            </div>
                            <span class="form-text text-muted">*Fail dalam bentuk gambar sahaja.</span>
                            <img id="output1" src="{{ URL::asset("assets/upload/virtual/{$data['list']->eft_uuid}/{$data['list']->virtual_img}") }}" style="width: 250px; height: 200px;"/>
                        </div>
                    </div> --}}
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('hall/admin/facility') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary btn-confirm font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

<!--end::Content-->
@endsection

@section('js_content')
    @include('hall.facility.js.edit')
    {{-- @include('hall.facility.js.form') --}}
@endsection
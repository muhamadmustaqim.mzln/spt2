@extends('layouts.master')

@section('container')

    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Fasiliti</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Dewan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/hall/admin/facility/add') }}" id="form" method="post" enctype="multipart/form-data">
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">Tambah Fasiliti</h3>
                        </div>
                    </div>
                    @csrf
                    <div class="card-body">
                        {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Nama Dewan :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="dewan" placeholder="Nama Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Dewan :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kod" placeholder="Kod Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kodfee" placeholder="Kod Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran Deposit:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="kodfeedepo" placeholder="Kod Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kod Pembayaran Deposit (Online):</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="koddepoonline" placeholder="Kod Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Lokasi :</label>
                            <div class="col-lg-9">
                                <select class="form-control" id="" name="lokasi">
                                    <option value="">Sila Pilih</option>
                                    @foreach($data['location'] as $l)
                                        <option value="{{ $l->id }}">{{ $l->lc_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Presint :</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="presint" placeholder="Kod Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Dewan Dibawah Pengurusan :</label>
                            <div class="col-lg-9">
                                <select class="form-control" id="" name="bawahpengurusan">
                                    <option value="">Sila Pilih</option>
                                    <option value="0">PPJ</option>
                                    <option value="2">Marina</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Pilihan Dewan Utama :</label>
                            <div class="col-lg-9">
                                <select class="form-control " id="" name="dewanutama">
                                    <option value="">Sila Pilih</option>
                                    @foreach($data['bh_hall'] as $l)
                                        <option value="{{ $l->id }}">{{ $l->bh_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Keterangan :</label>
                            <div class="col-lg-9">
                                <textarea name="keterangan" class="form-control" rows="7"></textarea>
                                <span class="form-text text-muted">*Penerangan Fasiliti secara terperinci.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-9">
                                <select class="form-control select2" id="kt_select2_3" name="status">
                                    <option value="">Sila Pilih Status</option>
                                    <option value="1" >Aktif</option>
                                    <option value="0" >Tidak Aktif</option>
                                    <option value="2" >Tutup Sementara</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Saiz Dewan:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="bh_size" placeholder="Saiz Dewan" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Pecahan Kapasiti/Penggunaan :</label>
                            <div class="col-lg-9">
                                <input hidden type="text" name="" id="pecahanValue" value="">
                                <select class="form-control " id="pecahan" name="pecahan">
                                    <option value="" >Sila Pilih </option>
                                    <option value="0" >Tidak</option>
                                    <option value="1" >Ya</option>
                                    <option value="2" >Pameran</option>
                                </select>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="banquet">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti Banquet:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="kapasitibanquet" placeholder="Kapasiti Banquet" value=""/>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="seminar">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti Seminar:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="kapasitiseminar" placeholder="Kapasiti Seminar" value=""/>
                            </div>
                        </div>
                        <div hidden class="form-group row" id="tiada">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kapasiti:</label>
                            <div class="col-lg-9">
                                <input type="number" class="form-control" name="kapasitibiasa" placeholder="Kapasiti" value=""/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Kemudahan Wifi:</label>
                            <div class="col-lg-9">
                                <select class="form-control " id="" name="wifi">
                                    <option value="">Sila Pilih </option>
                                    <option value="1">Ya</option>
                                    <option value="0">Tidak</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('hall/admin/facility') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('hall.facility.js.form')
@endsection
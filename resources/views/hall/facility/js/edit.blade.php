<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset('assets/plugins/custom/leaflet/leaflet.bundle.js') }}"></script>
<!-- Load Esri Leaflet from CDN -->
<script src="https://unpkg.com/esri-leaflet@3.0.3/dist/esri-leaflet.js"
    integrity="sha512-kuYkbOFCV/SsxrpmaCRMEFmqU08n6vc+TfAVlIKjR1BPVgt75pmtU9nbQll+4M9PN2tmZSAgD1kGUCKL88CscA=="
    crossorigin=""></script>
<!-- Load Esri Leaflet Vector from CDN -->
<script src="https://unpkg.com/esri-leaflet-vector@3.1.0/dist/esri-leaflet-vector.js"
    integrity="sha512-AAcPGWoYOQ7VoaC13L/rv6GwvzEzyknHQlrtdJSGD6cSzuKXDYILZqUhugbJFZhM+bEXH2Ah7mA1OxPFElQmNQ=="
    crossorigin=""></script>
    
<!--end::Page Vendors-->
<script>
        $('#kt_select2_1').select2({
            placeholder: 'Sila Pilih Fasiliti'
        });

        $('#kt_select2_2').select2({
            placeholder: 'Sila Pilih Lokasi'
        });

        $('#kt_select2_3').select2({
            placeholder: 'Sila Pilih Status'
        });

        $('#kt_select2_4').select2({
            placeholder: 'Sila Pilih Pecahan Kapasiti'
        });

        $('#kt_select2_5').select2({
            placeholder: 'Sila Pilih Kesediaan Wifi'
        });
</script>


<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        $("#output").css('display', 'block');
      }
    };
</script>
<script>
    var loadFile1 = function(event) {
      var output1 = document.getElementById('output1');
      output1.src = URL.createObjectURL(event.target.files[0]);
      output1.onload = function() {
        URL.revokeObjectURL(output1.src) // free memory
        $("#output1").css('display', 'block');
      }
    };
</script>
<script>
  $('#checkbox1').on('change', function() { 
      // From the other examples
      if (!this.checked) {
          $('#textbox1').text("Tidak Aktif");
      }else{
          $('#textbox1').text("Aktif");
      }
  });
</script>
<script>
          $('#kt_dropzone_1').dropzone({
            url: "{{ url('/upload/file/2') }}", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 10, // MB
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            init: function() {
                /*let myDropzone = this;
                $.getJSON("{{ url('/upload/scan/2') }}", function(data) {
                    $.each(data, function(index, val) {
                        var file = { name: val.name, size: val.size };
                        var path = "{{ asset('assets/upload/main/2') }}";
                        myDropzone.displayExistingFile(file, path+"/"+val.name);
                        let fileCountOnServer = data.length; // The number of files already uploaded
                        myDropzone.options.maxFiles = myDropzone.options.maxFiles - fileCountOnServer;
                    });
                });*/
                

                this.on("addedfile", file => {
                    var test = URL.createObjectURL(file);
                    var a = document.createElement('a');
                        a.setAttribute('href', test);
                        a.setAttribute('class',"dz-remove");
                        a.setAttribute('download', "download");
                        a.innerHTML = "Download";
                        file.previewTemplate.appendChild(a);
                });
            },
            removedfile: function(file) {
            var name = file.name; 
              $.ajax({
                  type: 'POST',
                  url: "{{ url('/upload/file/2') }}",
                  data: {name: name},
                  success: function(data){
                      console.log('success');
                  }
              });

              var previewElement;
              return(previewElement = file.previewElement) != null ? (
                  previewElement.parentNode.removeChild(file.previewElement)
              ) : (void 0);
          }
        });
        
</script>
<script>
    $('#kt_dropzone_2').dropzone({
      url: "{{ url('/upload/file/2') }}", // Set the url for your upload script location
      paramName: "file", // The name that will be used to transfer the file
      maxFiles: 1,
      maxFilesize: 10, // MB
      acceptedFiles: "image/*",
      addRemoveLinks: true,
      init: function() {
          this.on("addedfile", file => {
              var test = URL.createObjectURL(file);
              var a = document.createElement('a');
                  a.setAttribute('href', test);
                  a.setAttribute('class',"dz-remove");
                  a.setAttribute('download', "download");
                  a.innerHTML = "Download";
                  file.previewTemplate.appendChild(a);
          });
      },
      removedfile: function(file) {
      var name = file.name; 
        $.ajax({
            type: 'POST',
            url: "{{ url('/upload/file/2') }}",
            data: {name: name},
            success: function(data){
                console.log('success');
            }
        });

        var previewElement;
        return(previewElement = file.previewElement) != null ? (
            previewElement.parentNode.removeChild(file.previewElement)
        ) : (void 0);
    }
  });
  
</script>
<script>
    $('#kt_dropzone_3').dropzone({
      url: "{{ url('/upload/file/2') }}", // Set the url for your upload script location
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 10, // MB
      acceptedFiles: "image/*",
      addRemoveLinks: true,
      init: function() {
          this.on("addedfile", file => {
              var test = URL.createObjectURL(file);
              var a = document.createElement('a');
                  a.setAttribute('href', test);
                  a.setAttribute('class',"dz-remove");
                  a.setAttribute('download', "download");
                  a.innerHTML = "Download";
                  file.previewTemplate.appendChild(a);
          });
      },
      removedfile: function(file) {
      var name = file.name; 
        $.ajax({
            type: 'POST',
            url: "{{ url('/upload/file/2') }}",
            data: {name: name},
            success: function(data){
                console.log('success');
            }
        });

        var previewElement;
        return(previewElement = file.previewElement) != null ? (
            previewElement.parentNode.removeChild(file.previewElement)
        ) : (void 0);
    }
  });
  
</script>
<script>

    var map = L.map('map', {
        center: [2.9264, 101.6964],
        zoom: 14,
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    //Get Coordinate
    var curLocation = [2.9264, 101.6964];

    map.attributionControl.setPrefix(false);

    var leafletIcon = L.divIcon({
        html: `<span class="svg-icon svg-icon-danger svg-icon-3x"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="24" width="24" height="0"/><path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/></g></svg></span>`,
        bgPos: [10, 10],
        iconAnchor: [20, 37],
        popupAnchor: [0, -37],
        className: 'leaflet-marker'
    });

    var marker = new L.marker(curLocation, {
        draggable: 'true',
        icon: leafletIcon
    });

    marker.on('dragend', function(event){
        var position = marker.getLatLng();
        marker.setLatLng(position, {
            draggable: 'true'
        }).bindPopup(position).update();
        $("#latitude").val(position.lat.toFixed(5));
        $("#longitude").val(position.lng.toFixed(5));
    });
    map.addLayer(marker);

    map.on('click', function(e){
        var lat = e.latlng.lat;
        var lng = e.latlng.lng;
        if(!marker){
            marker = L.marker(e.latlng).addTo(map);
        } else {
            marker.setLatLng(e.latlng);
        }
        $("#latitude").val(lat.toFixed(5));
        $("#longitude").val(lng.toFixed(5));
    })


    var mapTab = document.getElementById('kt_tab_pane_4_2');
    var observer1 = new MutationObserver(function(){
        if(mapTab.style.display != 'none'){
            map.invalidateSize();
        }
    });
    observer1.observe(mapTab, {attributes: true}); 
</script>
<script>
    $("#add_details").click(function(){
        var html = '';
        html += '<tr>';
        html += '<td align="center"></td>';
        html += '<td><input type="text" name="fasiliti[]" class="form-control" placeholder="Nama Fisiliti Terperinci" ></td>';
        html += '<td>';
        html += '<select class="form-control" name="sewa[]">';
        html += '<option value="">Sila Pilih Kategori Bayaran Sewa</option>';
        @foreach($data['rent'] as $l)
        html += '<option value="{{ $l->id }}">{{ $l->lsc_slot_desc }}</option>';
        @endforeach
        html += '</select>';
        html += '</td>';
        html += '<td><input type="text" name="kod_hasil[]" class="form-control" placeholder="Kod Hasil"></td>';
        html += '<td><input type="text" name="kod_deposit[]" class="form-control" placeholder="Kod Hasil Deposit"></td>';
        html += '<td><input type="text" name="deposit[]" class="form-control" placeholder="Deposit (%)"></td>';
        html += '<td>';
        html += '<select class="form-control" name="fasiliti_status[]">';
        html += '<option value="">Sila Pilih Status</option>';
        html += '<option value="1">Aktif</option>';
        html += '<option value="0">Tidak Aktif</option>';
        html += '<option value="2">Tutup Sementara</option>';
        html += '</select>';
        html += '</td>';
        html += '<td style="text-align: center"><button type="button" class="btn btn-danger btn-sm btn-icon" id="remove"><i style="margin: auto;" class="fas fa-trash"></i></button></td>';
        $('#table_details').append(html);
    });

    $(document).on('click','#remove', function(){
        $(this).closest('tr').remove();
        q--;
    })
</script>
<script>
    $(document).ready(function() { 
        var initialPecahanVal = $('#pecahanValue').val();
        if(initialPecahanVal == 0){
            $('#banquet').prop('hidden', true);
            $('#seminar').prop('hidden', true);
            $('#tiada').prop('hidden', false);
        }else if(initialPecahanVal == 1){
            $('#banquet').prop('hidden', false);
            $('#seminar').prop('hidden', false);
            $('#tiada').prop('hidden', true);
        }else if(initialPecahanVal == 2){
            $('#banquet').prop('hidden', true);
            $('#seminar').prop('hidden', true);
            $('#tiada').prop('hidden', true);
        }
        $('#pecahan').on('change', function() {
            var val = ($(this).val());
            if(val == 0){
                $('#banquet').prop('hidden', true);
                $('#seminar').prop('hidden', true);
                $('#tiada').prop('hidden', false);
            }else if(val == 1){
                $('#banquet').prop('hidden', false);
                $('#seminar').prop('hidden', false);
                $('#tiada').prop('hidden', true);
            }else if(val == 2){
                $('#banquet').prop('hidden', true);
                $('#seminar').prop('hidden', true);
                $('#tiada').prop('hidden', true);
            }
        })
    })
</script>
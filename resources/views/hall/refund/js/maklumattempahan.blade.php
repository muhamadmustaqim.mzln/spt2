
<script>
     $(document).ready(function(){
        $('#tambahKelengkapan').on('click', function() {
            var kelengkapanHarian = $('#kelengkapanHarian').prop('hidden');
            if (kelengkapanHarian == true) {
                $('#kelengkapanHarian').removeAttr('hidden');
                $('#kelengkapanKeseluruhan').prop('hidden', true);
                $('#statusKelengkapan').val(2);
            } else {
                $('#kelengkapanHarian').prop('hidden', true);
                $('#kelengkapanKeseluruhan').removeAttr('hidden');
                $('#statusKelengkapan').val(1);
            }
        })
        $('#simpanRating').on('click', function() {
            $('#exampleModal').modal('hide');
            var highlight = $('#highlight').val();
            var rating = $('input[name="rate"]:checked').val();
            var token = $('#token').val();
            var bookingId = $('#bookingId').val();
            $.ajax({
                url: 'submitrating',
                type: 'POST', 
                data: {
                    _token: token,
                    rating: rating,
                    highlight: highlight,
                    id: bookingId
                },
                success: function(response) {
                    console.log(response);
                    $('#staticBackdrop').modal('show');
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        })
        $('#submitrefund').on('click', function() {
            $('#staticBackdrop').modal('hide');
            var name = $('#name').val();
            var id = $('#bookingId').val();
            var ic = $('#ic').val();
            var email = $('#email').val();
            var phoneno = $('#phoneno').val();
            var bankname = $('#bankname').val();
            var bankaccount = $('#bankaccount').val();
            var token = $('#token').val();
            console.log(id);
            try {
                $.ajax({
                    type: 'POST',
                    url: "{{ url('/hall/maklumatbayaran/pemulangandeposit') }}/" + id,
                    data: {
                        _token: token,
                        id: id,
                        name: name, ic: ic, email: email, phoneno: phoneno, bankname: bankname, bankaccount: bankaccount

                    },
                    success: function(response){
                        Swal.fire({
                            title: 'Berjaya',
                            icon: 'success',
                            html:
                                '<hr>Data telah berjaya disimpan dalam rekod<hr>',
                            showCloseButton: true
                        });
                    },
                    error: function(xhr, status, error) {
                        Swal.fire(
                            'Harap Maaf!',
                            'Data anda tidak berjaya disimpan dalam rekod',
                            'error'
                        );
                    }
                });
            } catch (error) {
                console.log(error)
            }
        })
    });
</script>
<script>
    function showTextarea() {
        var input_separa = document.getElementById("input_pemulangan_separa");
        var textarea_alasan = document.getElementById("alasantolak");
        if (document.getElementById("pemulangan_separa").checked) {
            input_separa.style.display = "block";
            textarea_alasan.style.display = "block";
        }else if(document.getElementById("tiada_pemulangan").checked){
            input_separa.style.display = "none";
            textarea_alasan.style.display = "block";
        }else {
            input_separa.style.display = "none";
            textarea_alasan.style.display = "none";
        }
    }

    function hideTextarea() {
        document.getElementById("input_pemulangan_separa").style.display = "none";
        document.getElementById("alasantolak").style.display = "none";
    }
</script>
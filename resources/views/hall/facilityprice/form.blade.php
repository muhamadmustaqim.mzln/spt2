@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Harga Fasiliti</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ url('') }}" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Konfigurasi Tempahan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sukan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Harga Fasiliti</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/hall/admin/facilityprice/add') }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Tambah Harga Fasiliti</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama Dewan :</label>
                            <div class="col-lg-10">
                                <select name="dewan" class="form-control">
                                    <option value="">Sila Pilih Fasiliti</option>
                                    @foreach($data['bh_hall'] as $f)
                                        <option value="{{ $f->id }}" >{{ $f->bh_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori Hari :</label>
                            <div class="col-lg-10">
                                <select name="kategorihari" class="form-control">
                                    <option value="">Sila Pilih Fungsi Penggunaan</option>
                                    <option value="1">Isnin-Jumaat</option>
                                    <option value="2">Sabtu-Ahad/Cuti Umum</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Bermula Pada Jam :</label>
                            <div class="col-lg-10">
                                <select name="jam" class="form-control">
                                    <option value="">Sila Pilih Slot Masa</option>
                                    <option value="0">12 Tengah Malam</option>
                                    <option value="1">1 Pagi</option>
                                    <option value="2">2 Pagi</option>
                                    <option value="3">3 Pagi</option>
                                    <option value="4">4 Pagi</option>
                                    <option value="5">5 Pagi</option>
                                    <option value="6">6 Pagi</option>
                                    <option value="7">7 Pagi</option>
                                    <option value="8">8 Pagi</option>
                                    <option value="9">9 Pagi</option>
                                    <option value="10">10 Pagi</option>
                                    <option value="11">11 Pagi</option>
                                    <option value="12">12 Tengah Hari</option>
                                    <option value="13">1 Petang</option>
                                    <option value="14">2 Petang</option>
                                    <option value="15">3 Petang</option>
                                    <option value="16">4 Petang</option>
                                    <option value="17">5 Petang</option>
                                    <option value="18">6 Petang</option>
                                    <option value="19">7 Malam</option>
                                    <option value="20">8 Malam</option>
                                    <option value="21">9 Malam</option>
                                    <option value="22">10 Malam</option>
                                    <option value="23">11 Malam</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Harga (RM) :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="hargaRM" name="hargaRM" placeholder="Harga (RM)" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Diskaun (%) :</label>
                            <div class="col-lg-10">
                                <input type="number" min="0" max="100" class="form-control" id="diskaunPer" name="diskaunPer" placeholder="Diskaun (%)" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Diskaun (RM) :</label>
                            <div class="col-lg-10">
                                <input readonly type="text" class="form-control form-control-solid" id="diskaunRM" name="diskaunRM" placeholder="Diskaun (RM)" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kadar GST :</label>
                            <div class="col-lg-10">
                                <select name="kadargst" class="form-control" id="kadarGST">
                                    <option value="" disabled selected>Sila Pilih Kadar GST</option>
                                    @foreach($data['lkp_gst_rate'] as $f)
                                        <option value="{{ $f->id }}" data-rate="{{ $f->lgr_rate }}">{{ $f->lgr_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">GST (RM) :</label>
                            <div class="col-lg-10">
                                <input readonly type="text" class="form-control form-control-solid" id="gstrm" name="gstrm" placeholder="GST (RM)" value="" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jumlah Harga (RM) :</label>
                            <div class="col-lg-10">
                                <input readonly type="text" class="form-control form-control-solid" id="jumlahhargarm" name="jumlahhargarm" placeholder="Jumlah Harga" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/hall/admin/facilityprice') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('hall.facilityprice.js.form')
@endsection
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-switch.js?v=7.2.9') }}"></script>
<script>
    $(document).ready(function() {
        $('#kadarGST, #diskaunPer, #hargaRM').on('input', function() {
            var totalPrice = $('#jumlahhargarm');
            var hargaRM = $('#hargaRM').val();
            var diskaunPer = $('#diskaunPer').val();
            var diskaunRM = $('#diskaunRM').val();
            var kadarGST = $('#kadarGST').val();
            var rate = $('#kadarGST option:selected').data('rate');

            diskaunPer = diskaunPer.replace(/\D/g, '');

            var diskaunPer = parseInt(diskaunPer || 0); 
            if (diskaunPer < 0) {
                diskaunPer = 0;
            } else if (diskaunPer > 100) {
                diskaunPer = 100;
            }
            $('#diskaunPer').val(diskaunPer);

            totalPrice = hargaRM - (hargaRM * (diskaunPer / 100));

            diskaunRM = hargaRM * (diskaunPer / 100)

            $('#jumlahhargarm').val(totalPrice.toFixed(2));
            $('#diskaunRM').val(diskaunRM.toFixed(2));

            if(rate >= 0){
                kadarGST = hargaRM * rate;
                $('#gstrm').val(kadarGST.toFixed(2));
            }
        })
    })
</script>
{{-- <script>
    var state = 1;
    $('#TheCheckBox').on('switchChange.bootstrapSwitch', function () {
        if($('#TheCheckBox').bootstrapSwitch('state') == true){
            state = 1
        }else{
            state = 0
        }
    });
    $('input[name="status"]').val(state);
</script>
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $('#kt_select2_1').select2({
        placeholder: 'Sila Pilih Fasiliti'
    });
    $('#kt_select2_2').select2({
        placeholder: 'Sila Pilih Fungsi Penggunaan'
    });
    $('#kt_select2_3').select2({
        placeholder: 'Sila Pilih Slot Masa'
    });
    $('#kt_select2_4').select2({
        placeholder: 'Sila Pilih Kategori GST'
    });
    $('#kt_select2_5').select2({
        placeholder: 'Sila Pilih Kategori Pakej'
    });
    $('#kt_select2_6').select2({
        placeholder: 'Sila Pilih Kategori Slot'
    });
    $('#kt_select2_7').select2({
        placeholder: 'Sila Pilih Kategori Hari'
    });
</script> --}}
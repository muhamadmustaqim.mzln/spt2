@extends('layouts.master')

@section('container')
    <!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bolder my-1 mr-5">Pembatalan Tempahan</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ url('') }}" class="text-muted">SPT</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pengurusan Tempahan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Tempahan Dewan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="" class="text-muted">Pembatalan Tempahan</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->	
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-5">
                                    <span class="card-label font-weight-bolder text-dark mb-1">Tapisan Data</span>
                                    <span class="text-muted mt-2 font-weight-bold font-size-sm">Tarikh Hari Ini : <?= date("d F Y") ?></span>
                                </h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <form action="" method="post">
                                @csrf
                            <div class="card-body pt-2">
                                <hr>
                                <div class="form-group row">
                                    <div class="col-lg-5">
                                        <input type="text" name="id" class="form-control mt-3" placeholder="No. Tempahan" autocomplete="off" required value="{{ $data['id'] }}">
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="float-right">
                                            <button type="submit" id="submit_form" class="form-control btn btn-light-primary font-weight-bold mt-3">Carian</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <hr>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    @if ($data['post'] == true)
                    <div class="col-lg-12">
                        <!--begin::List Widget 21-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                <div class="card-title">
                                    <h3 class="card-label">Senarai Tempahan</h3>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <table class="table table-bordered table-hover table-checkable" id="myTable" style="margin-top: 13px !important">
                                    <thead> 
                                        <tr>
                                            <th>No. Tempahan</th>
                                            <th>Nama </th>
                                            <th>Email</th>
                                            <th>Status Tempahan</th>
                                            <th>Operasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($data['main'] == null)
                                            
                                        @else
                                        <tr>
                                            <td>{{ $data['main']->bmb_booking_no }}</td>
                                            <td>{{ Helper::get_nama($data['main']->fk_users) }}</td>
                                            <td>{{ Helper::get_email($data['main']->fk_users) }}</td>
                                            <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                            <td style="width: 20%; text-align: center;">
                                                <a target="_blank" class="btn btn-outline-primary btn-sm btn-primary m-1" href="{{url('/hall/maklumatbayaran',Crypt::encrypt($data['main']->id))}}">
                                                    Maklumat Tempahan
                                                 </a>
                                                @if ($data['main']->fk_lkp_status != 10)
                                                <button type="button" class="btn btn-outline-danger btn-sm m-1" data-toggle="modal" data-target="#staticBackdrop">
                                                    Pembatalan Tempahan
                                                </button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 21-->
                    </div>
                    <!-- Modal-->
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Pembatalan Tempahan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close"></i>
                                    </button>
                                </div>
                                <form action="{{url('/hall/cancel/submit',Crypt::encrypt($data['main']->id))}}" method="post">
                                    @csrf
                                <div class="modal-body">
                                    <textarea class="form-control" name="reason" id="exampleTextarea" rows="5"></textarea>
                                    <span class="form-text text-muted" >Sila nyatakan alasan pembatalan anda.</span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        
    </div>
    <!--end::Content-->
    <!-- Button trigger modal-->	

@endsection

@section('js_content')
    @include('hall.cancel.js.index')
@endsection
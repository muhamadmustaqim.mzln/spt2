@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Penjadualan Semula</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Maklumat Pemohon-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                                    <td>{{ $data['user']->fullname }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                                    <td>{{ $data['main']->bmb_booking_no }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                                    <td>{{ date("d-m-Y", strtotime($data['main']->bmb_booking_date)) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                                    <td>{{ $data['user_detail']->bud_phone_no }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                                    <td>{{ $data['user']->email }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan</th>
                                    <td>{{ $data['main']->bmb_subtotal }}</td>
                                </tr>
                                <tr>
                                    <th width="35%" class="text-light" style="background-color: #242a4c">Amaun Cagaran / Deposit</th>
                                    <td>{{ Helper::get_permohonan($data['main']->fk_lkp_discount_type) }}</td>
                                </tr>
                                <tr>
                                    <th width="30%" class="text-light" style="background-color: #242a4c">Status Tempahan</th>
                                    <td>{{ Helper::get_status_tempahan($data['main']->fk_lkp_status) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Maklumat Pemohon-->
            {{-- <!--start::Senarai Resit-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Resit</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Jenis Bayaran</th>
                                    <th>No. Resit</th>
                                    @if(count($data['online']) > 0)
                                    <th>No. SAP</th>
                                    @endif
                                    <th>Tarikh Transaksi</th>
                                    <th>Jumlah Bayaran (RM)</th>
									<th>Jumlah Baki Pembayaran (RM)</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $paid = 0.00;
                            @endphp
                            <tbody>
                                @if(count($data['kaunter']) > 0)
                                    @foreach($data['kaunter'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>{{ $p->bp_receipt_number }}</td>
                                        <td>{{ $p->bp_receipt_date }}</td>
                                        <td>{{ $p->bp_paid_amount }}</td>
                                        <td style="width: 15%;">
                                            <a href="#" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
                                        </td>
                                    </tr>
                                    @php
                                        $paid = $paid + $p->bp_paid_amount
                                    @endphp
                                    @endforeach
                                @elseif(count($data['online']) > 0)
                                    @foreach($data['online'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>BAYARAN {{ Helper::get_jenis_bayaran($p->fk_lkp_payment_type) }}</td>
                                        <td>{{ $p->fpx_trans_id }}</td>
                                        <td>{{ $data['main']->sap_no ?? "" }}</td>
                                        <td>{{ $p->fpx_date }}</td>
                                        <td>{{ $p->amount_paid }}</td>
										<td><?php ?>6625.00<?php ?></td>
                                        <td style="width: 15%;">
                                            <a href="{{ url('sport/resitonline', Crypt::encrypt($data['booking'])) }}" class="btn btn-sm btn-primary">Cetak Resit Bayaran</a>
											<a href="https://spt2.ppj.gov.my/hall/bayaran/eyJpdiI6InN0dEFyNXI2T3p6SnJKK01jdWgrRmc9PSIsInZhbHVlIjoiRkFRMmJHSUZQc1BMNGNkejFWQUMydz09IiwibWFjIjoiYzJlMjgzOTJhOWNjMzg5MzdiZTE1YzQwMjAyM2NmODAwNDllNjNhMTg0NmFjZjdkNDY2NzgyZGIxMGUxMmJkZiIsInRhZyI6IiJ9" class="btn btn-sm btn-primary">Bayar Baki</a>
                                        </td>
                                    </tr>
                                    @php
                                        $paid = $paid + $p->amount_paid
                                    @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Tiada Resit Bayaran</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div> --}}
            <!--end::Senarai Resit-->
            <!--start::Maklumat Acara-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Maklumat Acara</span>
                    </h3>
                    <div class="aligh-items-end flex-column">
                        @if ($data['main']->fk_lkp_status != 10)
                            <button type="button" class="btn btn-primary btn-sm m-1" data-toggle="modal" data-target="#tarikh">
                                Kemaskini Tarikh
                            </button>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Item</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Akhir</th>
                                    <th>Tujuan Tempahan</th>
                                    <th>Maklumat Acara</th>
                                </tr>
                            </thead>
                            @php
                                $i = 0;
                                $j = 1;
                            @endphp
                            <tbody>
                                @if(count($data['booking_detail']) > 0)
                                    @foreach($data['booking_detail'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($data['bh_booking'][$i++]->fk_bh_hall) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->bbd_end_date) }}</td>
                                        <td>{{ Helper::get_lkp_event($p->fk_lkp_event) }}</td>
                                        <td>{{ $p->bbd_event_name }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Tiada Maklumat Acara</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Maklumat Acara-->
            <!--start::Kadar Sewaan Dewan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Dewan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Dewan</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Akhir</th>
                                    <th>Masa Mula</th>
                                    <th>Masa Akhir</th>
                                    <th>Bil. Hari</th>
                                    <th>Jumlah(RM)</th>
                                    {{-- <th>Kod GST</th>
                                    <th>GST (0%)</th> --}}
                                    <th>Jumlah Sewaan (RM)</th>
                                    {{-- <th class="text-center">Tindakan</th> --}}
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahdewan = 0;
                                $jumlahkeseluruhan = 0;
                            @endphp
                            <tbody>
                                @if(count($data['bh_booking']) > 0)
                                    @foreach($data['bh_booking'] as $p)
                                    <tr>
                                        <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                        <td>{{ Helper::getHall($p->fk_bh_hall) }}</td>
                                        <td>{{ Helper::date_format($p->bb_start_date) }}</td>
                                        <td>{{ Helper::date_format($p->bb_end_date) }}</td>
                                        <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }}</td>
                                        <td class="text-right">{{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                        <td class="text-right">{{ ($p->bb_no_of_day) }}</td>
                                        <td class="text-right">{{ Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)) }}</td>
                                        {{-- <td></td>
                                        <td></td> --}}
                                        <td class="text-right">{{ $temp = Helper::moneyhelper(Helper::get_harga_dewan($p->fk_bh_hall)) }}</td>
                                        {{-- <td class="text-right">
                                            @if ($data['main']->fk_lkp_status != 10)
                                            <button type="button" class="btn btn-outline-info btn-sm m-1" data-toggle="modal" data-target="#staticBackdrop">
                                                Kemaskini Dewan
                                            </button>
                                            @endif
                                        </td> --}}
                                    </tr>
                                    @php
                                        $tempFloat = (float) str_replace(',', '', $temp);
                                        $jumlahdewan += $tempFloat; 
                                    @endphp
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="7">Jumlah(RM)</td>
                                        <td></td>
                                        <td>{{ Helper::moneyhelper($jumlahdewan) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="11">Tiada Maklumat Dewan</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!-- Modal-->
            {{-- <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Kemaskini Dewan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <form action="{{url('/hall/reschedule/dewan/submit',Crypt::encrypt($data['main']->id))}}" method="post">
                            @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Dewan :</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="" name="Dewan" placeholder="Dewan" value="{{ Helper::getHall($p->fk_bh_hall) }}"/>
                                    <select name="lokasi" class="form-control form-control-lg border-0 font-weight-bold" id="kt_select2_5" required>
                                        <option value=""> Pilih Fasiliti Dewan</option>
                                        @foreach ($data['location'] as $l)
                                        
                                            $varSelected = " ";
                                            if ($data['fasility'] && $data['fasility']->bh_name == $l->bh_name)
                                            {
                                                $varSelected = "selected";
                                            }	
                                        ?>							
                                        <option value="{{ $l->id }}" {{ $varSelected }}>{{ $l->bh_name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!--end::Kadar Sewaan Dewan-->
            <!--start::Kadar Sewaan Peralatan-->
            <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Kadar Sewaan Peralatan</span>
                    </h3>
                    {{-- <div class="align-items-end flex-column">
                        @if ($data['main']->fk_lkp_status != 10)
                            <button type="button" class="btn btn-primary btn-sm m-1" data-toggle="modal" data-target="#peralatan">
                                Kemaskini Peralatan
                            </button>
                        @endif
                    </div> --}}
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Dewan</th>
                                    <th>Item</th>
                                    <th>Tarikh</th>
                                    <th>Masa Mula</th>
                                    <th>Masa Akhir</th>
                                    <th>Kuantiti</th>
                                    <th>Harga (RM) Seunit</th>
                                    <th>Jumlah (RM) <br><span class="text-danger mt-2 font-weight-bold font-size-xs">*selepas diskaun</span></th>
                                    {{-- <th>Kod GST</th>
                                    <th>GST (0%)</th> --}}
                                    <th>Jumlah Sewaan (RM)</th>
                                </tr>
                            </thead>
                            @php
                                $j = 1;
                                $jumlahkelengkapan = 0;
                            @endphp
                            <tbody>
                                @if(count($data['kelengkapan']) > 0)
                                    @foreach($data['kelengkapan'] as $p)
                                        <tr>
                                            <td style="width: 2%; text-align: center">{{ $j++ }}</td>
                                            <td>{{ Helper::getHall($data['bh_booking'][0]->fk_bh_hall) }}</td>
                                            <td>{{ Helper::get_equipment_hall($p->fk_bh_equipment) }}</td>
                                            <td>{{ $p->bbe_booking_date }}</td>
                                            <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_start_time) }}</td>
                                            <td>{{ Helper::time_format($data['booking_detail'][0]->bbd_end_time) }}</td>
                                            <td>{{ $p->bbe_quantity }}</td>
                                            <td class="text-right">{{ $p->bbe_price }}</td>
                                            <td class="text-right">{{ Helper::moneyhelper($p->bbe_total) }}</td>
                                            <td class="text-right">{{ Helper::moneyhelper($p->bbe_total) }}</td>
                                        </tr>
                                        @php
                                            $jumlahkelengkapan += $p->bbe_total; 
                                        @endphp
                                    @endforeach
                                    <tr class="font-weight-bolder text-right">
                                        <td colspan="9">Jumlah(RM)</td>
                                        <td>{{ Helper::moneyhelper($jumlahkelengkapan) }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="10">Tiada Data</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!-- Modal-->
            {{-- <div class="modal fade" id="peralatan" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="peralatan" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Kemaskini Peralatan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button>
                        </div>
                        <form action="{{url('/hall/reschedule/submit',Crypt::encrypt($data['main']->id))}}" method="post">
                            @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-lg-right font-weight-bold">Meja Bulat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="mejabulat" placeholder="Kuantiti" value="{{ $p->bbe_quantity }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-lg-right font-weight-bold">Meja Panjang</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="mejapanjang" placeholder="Kuantiti" value="{{ $p->bbe_quantity }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-lg-right font-weight-bold">Kerusi Banquet</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="kerusibanquet" placeholder="Kuantiti" value="{{ $p->bbe_quantity }}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label text-lg-right font-weight-bold">LCD Projektor dan Layar Putih</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="lcdLayarputih" placeholder="Kuantiti" value="{{ $p->    bbe_quantity }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            <!-- Modal-->
            <div class="modal fade" id="tarikh" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="tarikh" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Kemaskini Tarikh</h5>
                            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i aria-hidden="true" class="ki ki-close"></i>
                            </button> --}}
                        </div>
                        <form action="{{ url('/hall/reschedule/date/submit', Crypt::encrypt($data['main']->id)) }}" method="post">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Mula :</label>
                                    <div class="col-sm-6">
                                        <div class="input-icon">
                                            <input type="text" class="form-control text-center" id="kt_datepicker" name="tarikhmula" placeholder="Tarikh Mula" value="{{ Helper::date_format1($data['bh_booking'][0]->bb_start_date) }}"/>
                                            <span>
                                                <i class="flaticon-calendar-3 icon-xl"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Tarikh Tamat :</label>
                                    <div class="col-sm-6">
                                        <div class="input-icon">
                                            <input type="text" class="form-control text-center" id="kt_datepicker1" name="tarikhtamat" placeholder="Tarikh Tamat" value="{{ Helper::date_format1($data['bh_booking'][0]->bb_end_date) }}" readonly style="background: #D3D3D3; cursor: not-allowed;"/>
                                            <span>
                                                <i class="flaticon-calendar-3 icon-xl text-white"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-lg-right font-weight-bold">Bil. Hari :</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control text-center" id="" name="bilanganHari" value="{{ !empty($data['bh_booking']) ? $data['bh_booking'][0]->bb_no_of_day : '' }}" readonly style="background: #D3D3D3; cursor: not-allowed;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end::Kadar Sewaan Peralatan-->
            <!--start::Kadar Sewaan Fasiliti-->
            {{-- <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Lokasi</th>
                                    <th>Harga Sewaan (RM)</th>
                                    <th>Kod GST</th>
                                    <th>GST (RM)</th>
                                    <th>Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $total = 0.00;
                                @endphp
                                @foreach($data['slot'] as $s)
                                <tr>
                                    <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                    <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                    <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                    <td>{{$s->ebf_start_date}}</td>
                                    <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                    <td style="text-align: end">{{number_format($s->est_total, 2)}}</td>
                                    <td style="text-align: end">{{ Helper::tempahanSportGST($s->est_gst_code)}}</td>
                                    <td style="text-align: end">{{$s->est_gst_rm}}</td>
                                    <td style="text-align: end">{{number_format($s->est_subtotal, 2)}}</td>
                                    @php
                                        $total = $total + number_format($s->est_subtotal, 2)
                                    @endphp
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="8" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">{{ $total_v = number_format($total, 2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div> --}}
            <!--end::Kadar Sewaan Fasiliti-->
            <!--start::Jumlah Keseluruhan-->
            {{-- <div class="card card-custom gutter-b">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Jumlah Keseluruhan</span>
                    </h3>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered"> --}}
                            {{-- <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">E</td>
                                <td class="text-right">0.00</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">GST (0%)</th>
                                <td class="text-center">I</td>
                                <td class="text-right">0.00</td>
                            </tr> --}}
                            {{-- <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan (RM)</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_subtotal }}</td>
                                <td class="text-right">{{ Helper::moneyhelper($jumlahkelengkapan + $jumlahdewan) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                <td></td>
                                <td class="text-right"></td>
                                <td class="text-right">{{ $data['main']->bmb_rounding }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Keseluruhan Telah Dibayar</th>
                                <td></td>
                                <td class="text-right">{{ number_format($paid, 2) }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Deposit (RM)</th>
                                <td></td>
                                <td class="text-right">{{ Helper::moneyhelper($jumlahdewan/10) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">50% dari Jumlah Amaun Tempahan(RM)</th>
                                <td></td>
                                <td class="text-right">{{ $data['main']->bmb_subtotal }}</td>
                                <td class="text-right">{{ Helper::moneyhelper(($jumlahkelengkapan + $jumlahdewan)/2) }}</td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Penggenapan</th>
                                <td></td>
                                <td class="text-right"></td>
                            </tr>
                            <tr>
                                <th width="20%" class="text-light" style="background-color: #242a4c">Jumlah Deposit Perlu DIbayar (RM)</th>
                                <td></td>
                                <td class="text-right"><b>{{ Helper::moneyhelper((($jumlahkelengkapan + $jumlahdewan)/2)+($jumlahdewan/10)) }}</b></td>
                            </tr>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div> --}}
            <!--end::Jumlah Keseluruhan-->
            <div class="float-right">
                {{-- <a href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), Crypt::encrypt((($jumlahkelengkapan + $jumlahdewan)/2)+($jumlahdewan/10))]) }}" class="btn btn-primary float-right font-weight-bolder mt-2"> --}}
                <a href="" class="btn btn-primary float-right font-weight-bolder mt-2">
                    <i class="fas fa-check-circle"></i> Simpan
                </a>
                {{-- <a href="{{ url('hall/bayaran', [Crypt::encrypt($data['booking']), (($jumlahkelengkapan + $jumlahdewan)/2)+($jumlahdewan/10))]) }}" class="btn btn-primary float-right font-weight-bolder mt-2" ><i class="fas fa-check-circle"></i> Bayar Online</a> --}}
                <a href="#" class="btn btn-primary float-right font-weight-bolder mt-2 mr-2">Kembali</a>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

					
@section('js_content')
    @include('hall.reschedule.js.penjadualan')
@endsection


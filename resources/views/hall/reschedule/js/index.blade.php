<script>
    $(function() {
        // Function to calculate and set the end date based on the fixed number of days
        function setEndDate(startDate) {
            var days = parseInt($('input[name="bilanganHari"]').val());
            if (!isNaN(days)) {
                var endDate;
                if (days === 1) {
                    endDate = startDate.clone();
                } else {
                    endDate = startDate.clone().add(days - 1, 'days');
                }
                $('#kt_datepicker1').val(endDate.format('YYYY-MM-DD'));
            }
        }

        $('#kt_datepicker').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: moment().add(3, 'months').toDate(), // Calculate the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
            // Set the end date
            setEndDate(picker.startDate);
        });

        $('#kt_datepicker').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            $('#kt_datepicker1').val('');
        });

        // Event listener for changes in the start date
        $('#kt_datepicker').on('change', function() {
            var startDate = moment($(this).val(), 'YYYY-MM-DD');
            // Set the end date
            setEndDate(startDate);
        });
    });
</script>
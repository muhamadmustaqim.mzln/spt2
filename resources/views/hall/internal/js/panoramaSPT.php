<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>

<script>
    pannellum.viewer('panorama', {
        "type": "equirectangular",
        "panorama": "{{ asset('assets/media/panaroma_2.jpg') }}",
        "preview": "{{ asset('assets/media/panaroma_2.jpg') }}",
        "title": "{{ $data['fasility']->eft_type_desc }}, {{ Helper::location($data['fasility']->fk_lkp_location) }}",
    });
    </script>
@extends('layouts.master')
@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
<div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">						
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Rumusan Tempahan</h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::List Widget 21-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column mb-5">
                        <span class="card-label font-weight-bolder text-dark mb-1">Maklumat Pemohon</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-2">
                    <table class="table table-bordered">
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Nama Pemohon</th>
                            <td>{{ $data['user']->fullname }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Tempahan</th>
                            <td>{{ $data['main']->bmb_booking_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Tarikh Tempahan</th>
                            <td>{{ date('d-m-Y') }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">No. Telefon</th>
                            <td>{{ $data['user_detail']->bud_phone_no }}</td>
                        </tr>
                        <tr>
                            <th width="20%" class="text-light" style="background-color: #242a4c">Email Pemohon</th>
                            <td>{{ $data['user']->email }}</td>
                        </tr>
                    </table>
                </div>
                <!--end::Body-->
            </div>
            <!--end::List Widget 21-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Senarai Tempahan Fasiliti</span>
                    </h3>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/sport/internal/tempahan', Crypt::encrypt($data['booking'])) }}" class="btn btn-light-warning font-weight-bolder mr-2 mt-2">
                            <i class="flaticon-edit"></i> Kemaskini Tempahan
                        </a>
                        <a href="{{ url('/sport/internal') }}" class="btn btn-light-danger font-weight-bolder mt-2">
                            <i class="flaticon-circle"></i> Batal Tempahan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Fasiliti</th>
                                    <th>Slot Masa</th>
                                    <th>Tarikh Penggunaan</th>
                                    <th>Lokasi</th>
                                    <th>Harga Sewaan (RM)</th>
                                    <th>Kod GST</th>
                                    <th>GST (RM)</th>
                                    <th>Jumlah (RM)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                    $total = 0.00;
                                @endphp
                                @foreach($data['slot'] as $s)
                                <tr>
                                    <td style="width: 2%; text-align: center">{{ $i++ }}</td>
                                    <td>{{ Helper::tempahanSportDetail($s->fk_et_facility_detail) }}</td>
                                    <td>{{ Helper::tempahanSportTime($s->fk_et_slot_price) }}</td>
                                    <td>{{$s->ebf_start_date}}</td>
                                    <td>{{ Helper::tempahanSportLocation($s->fk_et_facility_type) }}</td>
                                    <td style="text-align: end">{{$s->est_total}}</td>
                                    <td style="text-align: end">{{$s->est_gst_code}}</td>
                                    <td style="text-align: end">{{$s->est_gst_rm}}</td>
                                    <td style="text-align: end">{{$s->est_subtotal}}</td>
                                    @php
                                        $total = $total + $s->est_subtotal
                                    @endphp
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="8" class="font-weight-bolder" style="text-align: end">Jumlah Keseluruhan Perlu Dibayar (RM)</td>
                                    <td class="font-weight-bolder" style="text-align: end">{{ $total_v = number_format($total, 2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                    <!-- Button trigger modal-->
                    <button type="button" class="btn btn-primary float-right font-weight-bolder mt-2" data-toggle="modal" data-target="#staticBackdrop">
                        <i class="fas fa-check-circle"></i> Teruskan Tempahan
                    </button>

                    <!-- Modal-->
                    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Perakuan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i aria-hidden="true" class="ki ki-close"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Dengan ini saya mengesahkan maklumat yang diberikan adalahbenar dan bersetuju dengan amaun yang dikenakan
                                </div>
                                <div class="modal-footer">
                                    <form action="{{ url('sport/internal/bayaran', Crypt::encrypt($data['booking'])) }}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-primary"><i class="
                                            fas fa-check-circle"></i> Saya pasti</button>
                                            <input type="hidden" name="total" value="{{ $total_v }}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
@endsection

					



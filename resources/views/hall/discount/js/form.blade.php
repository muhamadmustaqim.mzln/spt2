
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    $(document).ready(function() {
        $('#checkbox1').on('change', function() { 
            // From the other examples
            if (!this.checked) {
                $('#textbox1').text("Tidak Aktif");
            }else{
                $('#textbox1').text("Aktif");
            }
        });
        $('#kadardiskaun').on('input', function() {
            this.value = this.value.replace(/\D/g, '');

            // Ensure the value is within the allowed range (0 to 100)
            var value = parseInt(this.value || 0); // Convert input value to an integer
            if (value < 0) {
                value = 0;
            } else if (value > 100) {
                value = 100;
            }
            this.value = value; 
        })
    })
</script>
@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Pengumuman</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Pengumuman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/announcement/add') }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Tambah Pengumuman</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Pengumuman :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="umum" placeholder="Pengumuman" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Butiran :</label>
                            <div class="col-lg-10">
                                <textarea name="butiran" class="form-control" rows="7"></textarea>
                                <span class="form-text text-muted">*Butiran Pengumuman.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori :</label>
                            <div class="col-lg-10">
                                <select name="kategori" class="form-control" id="" required>
                                    <option value="" selected disabled><div class="text-muted">Pilih Kategori</div></option>
                                    <option value="0">Acara</option>
                                    <option value="1">Dewan</option>
                                    <option value="2">Sukan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Mula :</label>
                            <div class="col-sm-2">
                                <div class="input-icon">
                                <input autocomplete="off" type="text" class="form-control text-center" id="kt_datepicker" name="tarikhmula" placeholder="Tarikh Mula"/>
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                </div>
                            </div>
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Tamat :</label>
                            <div class="col-sm-2">
                                <div class="input-icon">
                                <input autocomplete="off" type="text" class="form-control text-center" id="kt_datepicker1" name="tarikhtamat" placeholder="Tarikh Tamat"/>
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                </div>
                            </div>
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Tarikh Luput :</label>
                            <div class="col-sm-2">
                                <div class="input-icon">
                                <input autocomplete="off" type="text" class="form-control text-center" id="kt_datepicker2" name="tarikhluput" placeholder="Tarikh Luput"/>
                                <span>
                                    <i class="flaticon-calendar-3 icon-xl"></i>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    <label class="ml-2 text-muted" id="textbox1"></label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/admin/announcement/list') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('admin.announcement.js.form')
@endsection
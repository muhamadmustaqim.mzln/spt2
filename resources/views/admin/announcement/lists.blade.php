@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Pengumuman</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Pengumuman</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Pengumuman</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/admin/announcement/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i> Tambah
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Sistem</th>
                                    <th>Pengumuman</th>
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Tamat</th>
                                    <th>Tarikh Luput</th>
                                    <th width="15%">Status</th>
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($data['umum'] as $u)
                                <tr>
                                    <td style="width: 5%;">{{ $i++ }}</td>
                                    <td>{{ $u->ba_name }}</td>
                                    <td>
                                        @if ($u->ba_type == 0)
                                            Acara
                                        @elseif ($u->ba_type == 1)
                                            Dewan
                                        @elseif ($u->ba_type == 2)
                                            Sukan
                                        @endif
                                    </td>
                                    <td>{!! empty($u->ba_start_date) ? date("d-m-Y") : date("d-m-Y", strtotime($u->ba_start_date)) !!}</td>
                                    <td>{!! empty($u->ba_end_date) ? date("d-m-Y") : date("d-m-Y", strtotime($u->ba_end_date)) !!}</td>
                                    <td>{!! empty($u->ba_expired_date) ? date("d-m-Y") : date("d-m-Y", strtotime($u->ba_expired_date)) !!}</td>
                                    @if($u->ba_status == 1)
                                        <td><span class="label label-lg label-light-success label-inline">{{ Helper::status($u->ba_status) }}</span></td>
                                    @elseif($u->ba_status == 2)
                                        <td><span class="label label-lg label-light-warning label-inline">{{ Helper::status($u->ba_status) }}</span></td>
                                    @elseif($u->ba_status == 3)
                                        <td><span class="label label-lg label-light-info label-inline">{{ Helper::status($u->ba_status) }}</span></td>
                                    @else
                                        <td><span class="label label-lg label-light-danger label-inline">{{ Helper::status($u->ba_status) }}</span></td>
                                    @endif
                                    <td style="width: 20%; text-align: center;">
                                        <a type="button" class="btn btn-outline-info btn-sm m-1" data-bs-toggle="modal" data-bs-target="#umum_{{ str_replace(' ', '_', $u->ba_name) }}">
                                            <i class="fas fa-images">
                                            </i>
                                            Butiran Pengumuman
                                        </a>
                                        <a class="btn btn-outline-primary btn-sm m-1" href="{{url('/admin/announcement/edit',Crypt::encrypt($u->id))}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Kemaskini
                                        </a>
                                        <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('/admin/announcement/delete',Crypt::encrypt($u->id))}}">
                                            <i class="fas fa-trash">
                                            </i>
                                            Padam
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @foreach($data['umum'] as $u)
                    <div class="modal fade" id="umum_{{ str_replace(' ', '_', $u->ba_name) }}"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h1 class="modal-title" id="exampleModalLabel">Butiran</h1>
                              <a type="button" class="btn btn-outline-danger btn-sm m-1" data-bs-dismiss="modal"><i class="fas note-icon-close p-0"></i></a>
                            </div>
                            <div class="modal-body" style="background-color: rgb(226 232 240)">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 style="font-weight: 700;">{{ $u->ba_name }}</h5>
                                    </div>
                                    <div class="card-body">
                                        <h6>{{ $u->ba_detail }}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-outline-primary btn-sm m-1 p-3" href="{{url('/admin/announcement/edit/butiran',Crypt::encrypt($u->id))}}">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Kemaskini
                                </a>
                              </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('admin.announcement.js.list')
@endsection
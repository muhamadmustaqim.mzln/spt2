@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sistem</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Banner</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Banner</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/admin/banner/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i> Tambah Banner
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Banner</th>
                                    <th>Penerangan</th>
                                    {{-- <th>Banner</th> --}}
                                    <th>Tarikh Mula</th>
                                    <th>Tarikh Tamat</th>
                                    <th>Tarikh Luput</th>
                                    <th>Status</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @if(count($data['banner']) > 0)
                                    @foreach($data['banner'] as $b)
                                    <tr>
                                        <td style="width: 5%;">{{ $i++ }}</td>
                                        <td>{!! $b->bb_name !!}</td>
                                        <td style="width: 5%; white-space: pre-wrap;">{!! !empty($b->bb_text) ? $b->bb_text : '' !!}</td>
                                        {{-- <td>{!! HTML::image(asset($b->bb_location.'/'.$b->bb_filename), '', array('class' => 'photo', 'height' => 100, 'width' => 200)) !!}</td> --}}
                                        {{-- <td>
                                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#banner_{{ str_replace(' ', '_', $b->bb_name) }}">
                                                Papar Banner
                                            </button>
                                        </td> --}}
                                        <td>{!! empty($b->bb_start_date) ? date("d-m-Y") : date("d-m-Y", strtotime($b->bb_start_date)) !!}</td>
                                        <td>{!! empty($b->bb_end_date) ? date("d-m-Y") : date("d-m-Y", strtotime($b->bb_end_date)) !!}</td>
                                        <td>{!! empty($b->bb_expired_date) ? date("d-m-Y") : date("d-m-Y", strtotime($b->bb_expired_date)) !!}</td>
                                        <td>{!! $b->bb_status == 1 ?  'Aktif' : 'Tidak Aktif' !!}</td>
                                        <td style="width: 15%; text-align: center;">
                                            <a class="btn btn-outline-primary btn-sm m-1" href="{{url('/admin/banner/edit',Crypt::encrypt($b->id))}}">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                                Kemaskini Banner
                                            </a>
                                            <a type="button" class="btn btn-outline-info btn-sm m-1" data-bs-toggle="modal" data-bs-target="{{ !empty($b->bb_name) ? '#banner_'.str_replace(' ', '_', $b->bb_name) : '' }}">
                                                <i class="fas fa-images">
                                                </i>
                                                Gambar Banner
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('/admin/banner/delete',Crypt::encrypt($b->id))}}">
                                                <i class="fas fa-trash"></i>
                                                Padam Banner
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                        @foreach($data['banner'] as $b)
                        <div class="modal fade" id="{{ !empty($b->bb_name) ? 'banner_'.str_replace(' ', '_', $b->bb_name) : '' }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ !empty($b->bb_name) ? $b->bb_name : '' }}</h5>
                                <a type="button" class="btn btn-outline-danger btn-sm m-1" data-bs-dismiss="modal"><i class="fas note-icon-close p-0"></i></a>
                                </div>
                                <div class="modal-body" style="background-color: rgb(226 232 240)">
                                    @if ($b->bb_filename == null)
                                        <span class="form-text text-danger">*Fail dalam bentuk gambar sahaja.</span>
                                        <span class="form-text text-danger">*Saiz Imej mestilah tidak melebihi 15MB dan berskala 1200px x 200px.</span>
                                        <a href="{{ url('/admin/bannerimageform', Crypt::encrypt($b->id)) }}" class="btn btn-primary font-weight-bolder my-2">
                                            <i class="flaticon-add-circular-button"></i>Tambah Banner
                                        </a>
                                    @endif
                                    <div class="card">
                                        <img src="{{ URL::asset("{$b->bb_location}/{$b->bb_filename}") }}" alt="img">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-outline-danger btn-sm btn-delete m-1" href="{{url('/admin/banner/image/delete',Crypt::encrypt($b->id))}}">
                                        <i class="fas fa-trash"></i>
                                        Padam Banner
                                    </a>
                                    <a class="btn btn-outline-primary btn-sm m-1" href="{{url('/admin/banner/edit/image',Crypt::encrypt($b->id))}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Kemaskini
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>
                        @endforeach
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('location.js.lists')
@endsection
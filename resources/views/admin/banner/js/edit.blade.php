<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js?v=7.2.9') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

</body>
<script>
    // dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
    //     <'row'<'col-sm-12'tr>>
    //     <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    $(document).ready(function() {
        $('#form').on('submit', function(event) {
            event.preventDefault();
            var status = $('#checkbox1').prop('checked') ? 1 : 0; 
            $.ajax({
                url: '/sport/ajax/bannerStatus', 
                method: 'GET',
                success: function(response) {
                    if(status == 0){
                        $('#form').off('submit').submit();
                    } else {
                        if(response.data == 1){
                            $('#form').off('submit').submit();
                        } else {
                            Swal.fire(
                            'Harap Maaf!',
                            'Banner yang aktif telah mencapai had kapasiti (3 banner). Sila pilih aktif atau deaktif banner sebelum membuat banner aktif baharu.',
                            'error'
                            );
                        }
                    }
                },
                // error: function(xhr, status, error) {
                //     console.log('response1: ', xhr)
                //     console.log('response2: ', status)
                //     console.log('response3: ', error)
                //     Swal.fire(
                //     'Harap Maaf!',
                //     'Banner yang aktif telah mencapai had kapasiti (3 banner). Sila pilih aktif atau deaktif banner sebelum membuat banner aktif baharu.',
                //     'error'
                //     );
                // }
            });
        })
        var table = $('table').DataTable({
            dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
                <'row'<'col-sm-12'tr>>
                <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            buttons: [
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                    columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.content[1].table.widths =Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        //doc.defaultStyle.alignment = 'center';
                        //doc.styles.tableHeader.alignment = 'center';
                    },
                },
                {
                    extend: 'colvis',
                    text: 'Pilihan Lajur'
                },
                
            ],
            language: {
                "lengthMenu": "Paparan _MENU_  rekod setiap halaman",
                "zeroRecords": "Harap maaf, tiada rekod ditemui",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Tiada rekod dalam sistem",
                "infoFiltered": "(Tapisan daripada _MAX_ jumlah rekod)",
                "sSearch": "Carian:"
            }
        } );
    });
</script>
<script>
    $(document).ready(function() {
        $('#btnSearch').on('click', function() {
            var query = $('#searchInput').val().toLowerCase();
            if (query.trim() === '') {
                location.reload(); 
            } else {
                searchTable(query);
            }
        });

        function searchTable(query) {
            $.ajax({
                url: '/admin/user/search', 
                method: 'GET',
                data: { query: query },
                success: function(response) {
                    updateTable(response);
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        }

        function updateTable(data) {
            console.log(data)
            if (!Array.isArray(data)) {
                console.error('Invalid data format:', typeof(data));
                return;
            }
            $('#kt_datatable_2 tbody').empty();

            data.forEach(function(record, index) {
                let newRowId = `${index+1}`;
                $('#kt_datatable_2 tbody').append(`<tr>
                    <td>${newRowId}</td>
                    <td>${record.email}</td>
                    <td>${record.fullname}</td>
                    <td>${record.fullname}</td>
                    <td>${record.isAdmin === 1 ? 'Administrator' : 'User'}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-outline-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Tindakan
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item text-primary" href="/admin/user/edit/${record.id}">Kemaskini</a>
                              <a class="dropdown-item text-danger btn-delete" href="/admin/user/delete/${record.id}">Padam</a>
                            </div>
                          </div>
                    </td>
                </tr>`);
            });
        }
    });
    $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
   $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker1').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
    $(function() {
        var maxDate = moment().add(3, 'months'); // Calculate the maximum date

        $('#kt_datepicker2').daterangepicker({
            autoApply: true,
            autoclose: true,
            showDropdowns: true,
            autoUpdateInput: false,
            singleDatePicker: true,
            minDate: new Date(),
            maxDate: maxDate.toDate(), // Set the maximum date
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });

        $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD'));
        });

        $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });
</script>
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        $("#output").css('display', 'block');
      }
    };
</script>



@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Pakej Perkahwinan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Pakej Perkahwinan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Pakej Perkahwinan</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ url('/admin/package/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i>Pakej Perkahwinan
                        </a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama</th>
                                    <th>Harga Pakej (RM)</th>
                                    <th>Harga GST(RM)</th>
                                    <th>Keterangan</th>
                                    {{-- <th>Peralatan</th> --}}
                                    <th>Status</th>
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                    @foreach($data['pakej'] as $package)
                                    <tr>
                                        <td style="width: 5%;">{{ $i++ }}</td>
                                        <td>{{ $package->bp_package_name }}</td>
                                        <td>{{ $package->bp_package_price }}</td>
                                        <td>{{ $package->bp_gst_rm }}</td>
                                        <td>
                                            @foreach ($data['detail'] as $d)
                                                @if ($d->fk_bh_package == $package->id)
                                                    {{ $d->bpd_description }}<br>
                                                @endif
                                            @endforeach
                                        </td>
                                        {{-- <td>{{ $package->bpd_description }}</td> --}}
                                        @if($package->bp_status == 1)
                                            <td><span class="label label-lg label-light-success label-inline">{{ Helper::status($package->bp_status) }}</span></td>
                                        @else
                                            <td><span class="label label-lg label-light-danger label-inline">{{ Helper::status($package->bp_status) }}</span></td>
                                        @endif
                                        <td style="width: 10%; text-align: center;">
                                            <div class="dropdown">
                                                <button class="btn btn-outline-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Tindakan
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item text-primary" href="{{ url('/admin/package/form/edit', Crypt::encrypt($package->id)) }}">Kemaskini</a>
                                                <a class="dropdown-item text-danger btn-delete" href="{{ url('/admin/package/delete', Crypt::encrypt($package->id)) }}">Padam</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
@include('admin.package.js.lists')

    {{-- @include('package.js.lists') --}}
@endsection
@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Peranan</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Peranan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/role/update',Crypt::encrypt($data['role']->id)) }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Peranan</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Peranan :</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="name" placeholder="Peranan" value="{{ $data['role']->name }}" />
                            </div>
                        </div>  
                        {{-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Jenis Admin</label>
                            <div class="col-9 col-form-label">
                                <div class="radio-inline">
                                    <label class="radio">
                                        <input type="radio" name="radios5"/>
                                        <span></span>
                                        Dewan
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="radios5"/>
                                        <span></span>
                                        Sukan
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="radios5"/>
                                        <span></span>
                                        Acara
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        {{--<div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Lokasi :</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" id="kt_select2_2" name="lokasi">
                                    <option value="">Sila Pilih</option>
                                    @foreach($data['location'] as $l)
                                        <option value="{{ $l->id }}">{{ $l->lc_description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold pt-0 mt-0">Akses Menu</label>
                            <div class="col">
                                <div class="checkbox-list">
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Tempahan" class="parent-checkbox" value="1"/>
                                        <span></span>
                                        Semua
                                    </label>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Tempahan" class="parent-checkbox" data-group="group1"/>
                                        <span></span>
                                        Tempahan
                                    </label>
                                    <div class="child-checkboxes group1" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Papan_Pemuka" class="parent-checkbox" data-group="group2"/>
                                        <span></span>
                                        Papan Pemuka
                                    </label>
                                    <div class="child-checkboxes group2" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Pengurusan_Sistem" class="parent-checkbox" data-group="group3"/>
                                        <span></span>
                                        Pengurusan Sistem
                                    </label>
                                    <div class="child-checkboxes group3" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Kofigurasi_Tempahan" class="parent-checkbox" data-group="group4"/>
                                        <span></span>
                                        Kofigurasi Tempahan
                                    </label>
                                    <div class="child-checkboxes group4" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="checkbox-list">
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Pengurusan_Tempahan" class="parent-checkbox" data-group="group5"/>
                                        <span></span>
                                        Pengurusan Tempahan
                                    </label>
                                    <div class="child-checkboxes group5" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Laporan" class="parent-checkbox" data-group="group6"/>
                                        <span></span>
                                        Laporan
                                    </label>
                                    <div class="child-checkboxes group6" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Audit" class="parent-checkbox" data-group="group7"/>
                                        <span></span>
                                        Audit
                                    </label>
                                    <div class="child-checkboxes group7" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                    <label class="checkbox checkbox-lg">
                                        <input type="checkbox" name="Deposit" class="parent-checkbox" data-group="group8"/>
                                        <span></span>
                                        Deposit
                                    </label>
                                    <div class="child-checkboxes group8" style="display: none; margin-left: 20px;">
                                        <div class="checkbox-list">
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Lihat Sahaja
                                            </label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="Checkboxes5"/>
                                                <span></span>
                                                Semua (Tambah. Kemaskini, Padam)
                                            </label>
                                        </div>
                                        <span class="form-text text-danger mb-2">Sila Pilih Tahap Akses</span>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/admin/role/list') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary btn-confirm font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('location.js.form')
@endsection
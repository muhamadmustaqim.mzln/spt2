@extends('layouts.master')

@section('container')
    <!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Cukai</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Cukai</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/taxmanagement/update',Crypt::encrypt($data['tax']->id)) }}" id="form" method="post">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Cukai</h3>
                    </div>
                </div>
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Cukai :</label>
                            <div class="col-lg-10">
                                {{-- <input type="text" class="form-control" name="cukai" placeholder="Cukai" value="{{ $data['tax']->lt_name }}" autocomplete="off"/> --}}
                                <input type="text" class="form-control" name="cukai" placeholder="Cukai" value="{{ $data['tax']->lgr_description }}" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kadar Cukai :</label>
                            <div class="col-lg-10">
                                {{-- <input type="number" class="form-control" name="ratecukai" placeholder="Kadar Cukai" min="0" max="1" value="{{ $data['tax']->lt_rate }}" autocomplete="off"/> --}}
                                <input type="number" class="form-control" name="ratecukai" placeholder="Kadar Cukai" min="0" max="1" value="{{ $data['tax']->lgr_rate }}" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kod Cukai :</label>
                            <div class="col-lg-10">
                                {{-- <input type="text" class="form-control" name="kodcukai" placeholder="Kod Cukai" value="{{ $data['tax']->lt_code}}" autocomplete="off"/> --}}
                                <input type="text" class="form-control" name="kodcukai" placeholder="Kod Cukai" value="{{ $data['tax']->lgr_gst_code}}" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                            <div class="col-lg-10">
                                <span class="switch switch-icon">
                                    <label>
                                        {{-- <input type="checkbox" {{($data['tax']->lt_status == 1) ? 'checked="checked"' : ''}}  id="checkbox1" name="status"/> --}}
                                        <input type="checkbox" {{($data['tax']->lgr_status == 1) ? 'checked="checked"' : ''}}  id="checkbox1" name="status"/>
                                        <span></span>
                                    </label>
                                    {{-- <label class="ml-2 text-muted" id="textbox1">{{ Helper::status($data['tax']->lt_status) }}</label> --}}
                                    <label class="ml-2 text-muted" id="textbox1">{{ Helper::status($data['tax']->lgr_status) }}</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <a href="{{ url('/admin/taxmanagement') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                            <button class="btn btn-primary btn-confirm font-weight-bold">Simpan</button>
                        </div>
                    </div>
            </div>
        </form>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('admin.tax.js.edit')
@endsection
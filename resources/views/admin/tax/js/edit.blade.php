
<script>
    const form = document.getElementById('form');
    var validator = FormValidation.formValidation(
        form,
        {
            fields: {
                lokasi: {
                    validators: {
                        notEmpty: {
                            message: 'Sila Masukkan Nama Lokasi'
                        }
                    }
                }
            },
            plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
            },
        }
    );
</script>
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        $("#output").css('display', 'block');
      }
    };
</script>
<script>
    $('#checkbox1').on('change', function() { 
        // From the other examples
        if (!this.checked) {
            $('#textbox1').text("Tidak Aktif");
        }else{
            $('#textbox1').text("Aktif");
        }
    });
</script>

<script>
    $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
   $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker1').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker1').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
<script>
    $(function() {
    var maxDate = moment().add(3, 'months'); // Calculate the maximum date

    $('#kt_datepicker2').daterangepicker({
        autoApply: true,
        autoclose: true,
        showDropdowns: true,
        autoUpdateInput: false,
        singleDatePicker: true,
        minDate: new Date(),
        maxDate: maxDate.toDate(), // Set the maximum date
        locale: {
            cancelLabel: 'Clear',
            format: 'YYYY-MM-DD'
        }
    });

    $('#kt_datepicker2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });

    $('#kt_datepicker2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
});
</script>
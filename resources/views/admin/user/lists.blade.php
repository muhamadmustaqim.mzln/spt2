@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sistem</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Pengguna</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Senarai Pengguna</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        {{-- <a href="{{ url('/admin/user/form') }}" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon-add-circular-button"></i> Tambah Pengguna
                        </a> --}}
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    {{-- <div class="row">
                        <div class="col-md-3">
                            <input class="form-control " type="text" id="searchInput" placeholder="Carian">
                        </div>
                        <div class="col-md-3">
                            <button id="btnSearch" class="btn btn-primary font-weight-bolder px-5">
                                Cari
                            </button>
                        </div>
                    </div>
                    <form action="" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <input class="form-control " type="text" name="id" id="id" placeholder="Carian">
                            </div>
                            <div class="col-md-3">
                                <button id="btnSearch" class="btn btn-primary font-weight-bolder px-5">
                                    Cari
                                </button>
                            </div>
                        </div>
                    </form> --}}
                    <form action="{{ url('admin/user/list/' . urlencode(request()->input('id'))) }}" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <input class="form-control" type="text" name="id" id="id" placeholder="Carian" value="{{ ($data['search'] != null) ? $data['search']['id'] : ''}}">
                            </div>
                            <div class="col-md-3">
                                <button id="btnSearch" class="btn btn-primary font-weight-bolder px-5">
                                    Cari
                                </button>
                            </div>
                        </div>
                    </form>
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_2">
                            <thead>
                                <tr>
                                    <th>Bil</th>
									<th>E-mel</th>
                                    <th>Nama</th>
                                    {{-- <th>No Pengenalan</th> --}}
                                    <th>Peranan</th>
                                    <th width="10%">Status</th>
                                    <th>Operasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($data['pengguna'] as $p)
                                <tr>
                                    <td style="width: 5%;">{{ $loop->iteration + $data['pengguna']->firstItem() - 1 }}</td>
                                    {{-- <td style="width: 5%;">{{ $i++ }}</td> --}}
                                    <td>{{ $p->email }}</td>
                                    <td>{{ $p->fullname }}</td>
									{{-- <td>{{ $p->fullname }}</td> --}}
                                    <td>{{ Helper::getUserType($p->isAdmin) }}</td>
                                    @if($p->status == 1)
                                        <td class="text-center"><span class="label label-lg label-light-success label-inline">{{ Helper::status($p->status) }}</span></td>
                                    @else
                                        <td class="text-center"><span class="label label-lg label-light-danger label-inline">{{ Helper::status($p->status) }}</span></td>
                                    @endif
                                    <td style="width: 10%; text-align: center;">
                                        <div class="dropdown">
                                            <button class="btn btn-light-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              Tindakan
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                              <a class="dropdown-item text-primary" href="{{ url('/admin/user/edit', Crypt::encrypt($p->id)) }}">Kemaskini</a>
                                              <a class="dropdown-item text-danger btn-delete" href="{{ url('/admin/user/delete', Crypt::encrypt($p->id)) }}">Padam</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col d-flex justify-content-end" style="size: 20px;">
                                {{ $data['pengguna']->appends($data['search'])->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('admin.user.js.lists')
@endsection
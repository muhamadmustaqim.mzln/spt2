@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<style>
    .tagify--custom-dropdown{
        border: 1;
    }
    .tagify--custom-dropdown .tagify__tag{
        background-color: rgba(52, 229, 52, 0.12);
    }
    .tagify--custom-dropdown .tagify__tag > div{
        size: 10px;
        border-radius: 25px;
    }
    .tagify--custom-dropdown .tagify__tag div .tagify__tag-text{
        margin: 0.3em;
        color: #4BB543;
        font-weight: 500;
    }

    .tagify--custom-dropdown .tagify__tag div .tagify__tag-text:hover{
        color: red;
    }

    .tagify--custom-dropdown .tagify__tag:not(:only-of-type):not(.tagify__tag--editable):hover .tagify__tag-text{
        color: red;
    }

    /* Do not show the "remove tag" (x) button when only a single tag remains */
    .tagify--custom-dropdown .tagify__tag:only-of-type .tagify__tag__removeBtn{
    display: none;
    }

    .tagify--custom-dropdown .tagify__tag__removeBtn{
    opacity: 0;
    transform: translateX(-100%) scale(.5);
    margin-inline: -20px 6px; /* very specific on purpose  */
    text-align: right;
    transition: .12s;
    }

    .tagify--custom-dropdown .tagify__tag:not(.tagify__tag--editable):hover .tagify__tag__removeBtn{
    transform: none;
    opacity: 1;
    color: red;
    }

    .tags-look .tagify__dropdown__item{
    display: inline-block;
    vertical-align: middle;
    border-radius: 3px;
    padding: .3em .5em;
    border: 1px solid black;
    background: white;
    opacity: 0.5;
    margin: .5em;
    font-size: 1em;
    font-weight: bold;
    color: black;
    transition: 0s;
    }

    .tags-look .tagify__dropdown__item--active{
        border-color: black;
    }

    .tags-look .tagify__dropdown__item:hover{
        background: lightyellow;
        border-color: gold;
        color: orangered;
    }

    .tags-look .tagify__dropdown__item--hidden {
        max-width: 0;
        max-height: initial;
        padding: .3em 0;
        margin: .2em 0;
        white-space: nowrap;
        text-indent: -20px;
        border: 0;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sistem</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Pengguna</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Kemaskini Pengguna</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/user/edit/update', Crypt::encrypt($data['list']->id)) }}" id="form" method="post" enctype="multipart/form-data">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Pengguna</h3>
                    </div>
                </div>
                @csrf
                <div class="card-body">
                    {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Email :</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $data['list']->email }}"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Nama :</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="nama" placeholder="nama" value="{{ $data['list']->fullname }}"/>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">No. Pengenalan :</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="noPengenalan" placeholder="No. Pengenalan" value="{{ $data['list']->fullname }}"/>
                        </div>
                    </div> --}}
                    {{-- <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Peranan :</label>
                        <div class="col-lg-2">
                            <select class="form-control" id="kt_select2_3" name="isAdmin">
                                <option value="" disabled>Sila Pilih Peranan</option>
                                <option value="1" {{($data['list']->isAdmin == 1) ? 'selected' : ''}}>Administrator</option>
                                <option value="0" {{($data['list']->isAdmin == 0) ? 'selected' : ''}}>User</option>
                            </select>
                        </div>
                    </div> --}}
                    {{-- <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Peranan :</label>
                        <div class="col-lg-2">
                            <select class="form-control" id="kt_select2_3" name="isAdmin1">
                                <option value="" disabled>Sila Pilih Peranan</option>
                                @foreach ($data['roles'] as $r)
                                    <option value="{{ $r->id }}" {{($data['list']->isAdmin === 0 && $r->id == 10) ? 'selected' : ($data['role_id']->role_id != null && $r->id === $data['role_id']->role_id) ? 'selected' : ''}}>{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Peranan :</label>
                        <div class="col-lg-10">
                            {{-- class = tagify--custom-dropdown --}}
                            <input name="peranan" id="perananId" class="tagify--custom-dropdown" placeholder="Pilih Peranan User" value="">
                            <span class="text-muted">Maksimum 10 peranan sahaja.</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Status :</label>
                        <div class="col-lg-10">
                            <span class="switch switch-icon">
                                <label>
                                    <input type="checkbox" {{($data['list']->status == 1) ? 'checked="checked"' : ''}}  id="checkbox1" name="status"/>
                                    <span></span>
                                </label>
                                <label class="ml-2 text-muted" id="textbox1">{{ Helper::status($data['list']->status) }}</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <a href="{{ url('/admin/user/list') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                        <button class="btn btn-primary font-weight-bold">Simpan</button>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('admin.user.js.edit')
@endsection
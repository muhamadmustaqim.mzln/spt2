@extends('layouts.master')

@section('container')
<!--begin::Content-->
@if(Session::has('flash'))
    <div class="flash-data" data-flashdata="{{Session::get('flash')}}"></div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bolder my-1 mr-5">Pengurusan Sistem</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">SPT</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Sistem</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Pengurusan Soalan</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Kemaskini Soalan</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <form action="{{ url('/admin/updateMaklumat', Crypt::encrypt($data['question']->id)) }}" id="form" method="post" enctype="multipart/form-data">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Kemaskini Soalan</h3>
                    </div>
                </div>
                @csrf
                <div class="card-body">
                    {{-- <input type="hidden" name="uuidKey" value="{{ $data['list']->eft_uuid }}"> --}}
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Soalan :</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="bq_question" placeholder="" value="{{ $data['question']->bq_question }}"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-lg-right font-weight-bold">Kategori :</label>
                        <div class="col-lg-10">
                            <select class="form-control" id="kt_select2_3" name="fk_lkp_question_cat">
                                <option value="" disabled selected>Sila Pilih Kategori</option>
                                @foreach ($data['lkp_question_cat'] as $bq)
                                    <option value="{{ $bq->id }}" @if($bq->id == $data['question']->fk_lkp_question_cat) selected @endif>{{ $bq->description }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <a href="{{ url('/admin/list_maklumat') }}" class="btn btn-outline-dark font-weight-bold">Batal</a>
                        <button class="btn btn-primary font-weight-bold">Simpan</button>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

@section('js_content')
    @include('location.js.lists')
@endsection